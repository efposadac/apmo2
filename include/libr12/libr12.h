#ifndef _psi3_libr12_h
#define _psi3_libr12_h

#include <libint/libint.h>
/* Maximum angular momentum of functions in a basis set plus 1 */
#define LIBR12_MAX_AM 6
#define LIBR12_OPT_AM 4
#define NUM_TE_TYPES 4

typedef struct {
  REALTYPE AB[3];
  REALTYPE CD[3];
  REALTYPE AC[3];
  REALTYPE ABdotAC, CDdotCA;
  } contr_data;

typedef struct {
  REALTYPE *int_stack;
  prim_data *PrimQuartet;
  contr_data ShellQuartet;
  REALTYPE *te_ptr[NUM_TE_TYPES];
  REALTYPE *t1vrr_classes[11][11];
  REALTYPE *t2vrr_classes[11][11];
  REALTYPE *rvrr_classes[11][11];
  REALTYPE *gvrr_classes[12][12];
  REALTYPE *r12vrr_stack;

  } Libr12_t;

#ifdef __cplusplus
extern "C" {
#endif
extern void (*build_r12_gr[6][6][6][6])(Libr12_t *, int);
extern void (*build_r12_grt[6][6][6][6])(Libr12_t *, int);
void init_libr12_base();

int  init_libr12(Libr12_t *, int max_am, int max_num_prim_quartets);
void free_libr12(Libr12_t *);
int  libr12_storage_required(int max_am, int max_num_prim_quartets);

#ifdef __cplusplus
}
#endif

#endif
