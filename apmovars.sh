#!/bin/sh

APMO_HOME=$HOME/.apmo
APMO_SCRT=/scratch/$USER/apmo

if [ -z "$APMO_HOME" ]
then
    export APMO_HOME
fi

if [ -z "$APMO_DATA" ]
then
    APMO_DATA="$APMO_HOME/data"
    export APMO_DATA
fi

if [ -z "$PATH" ]
then
    PATH="$APMO_HOME/bin:$APMO_HOME/utils:"
    export PATH
else
    PATH="${PATH}:$APMO_HOME/bin:$APMO_HOME/utils"
    export PATH
fi

