# README #

Any Particle Molecular Orbital (APMO) Optimised Version.

### What is this repository for? ###

This is an optimised version of APMO, as result of M.Sc. thesis by Fernando Posada:

* Molecular integrals up to `l = 7`. It uses LIBINT library for respulsion integrals.
* The `l` has been un-vectorized.
* Other performances in compilation and algorithms.
* NOTE: It does not have the geometry optimization system.

### How do I get set up? ###

You must have installed in you computer:

* MKL or LAPACK
* libBLAS
* exPat
* LIBINT library 1.1.4
* LibFourIndexTransformation (provided along with this code)

Compilation instructions:

* make -f Makefile.cvs
* ./configure
* make
* make install

Optional:

* make binary (for portability to a match-machine)
* make uninstall
* make clean
* make distclean

### Contributions ###

* This software has been developed from Sergio's APMO
* All optimisations have been developed by Fernando Posada

### Who do I talk to? ###

* efposadac@unal.edu.co