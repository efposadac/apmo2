##Arreglar lo del allocatable para que lo maneje desde afuera, y se ahorra un monton de elementos en el arreglo (aunque tocaría dar vueltas...)
##Falta arreglarlo para que funciona cuando se declaran varias variables en la misma linea (e.g. integer(8) :: factorial_ , i en FMath.f90)
##Falta arreglar posibles problemas con el uso de mayúsculas (e.g. OPTIONAL en FAttractionIntegrals.f90)
##Falta meter otras declaraciones de variables en el arreglo fort o fort1. Por ejemplo, en FContractedGaussian.f90 tenemos:
## real(8)    contractionOrigin(3)
## integer(8) contractionAngularMomentIndex(3)
## integer    lengthContractedFunction
##Falta definir si va a manejar dos arreglos separados, fort y fort1, o si los va a unir
##Si es el caso, verificar la pila de problemas que nos podría generar el unir los arreglos
##Falta mirar cómo se manejan las funciones recursivas (e.g. recursive function ssMomentIntegral( primitiveA , PrimitiveB ,&		momentIndex ) result( momentValue ) en FMomentIntegrals.f90)
##Otras declaraciones de variables para meter, aunque tal vez es más bien un problema de comentarios: En FPrimitiveGaussian.f90 tenemos:
## real(8) :: originGaussian(3)     !< Coordenadas x,y,z donde se centra la gausiana
##Podemos tener problemas con esta declaración de variable (vea e.g. FPrimitiveGaussian.f90):
## real( 8 ) :: origin( 3 )
##La toma como variable double, pero eso puede ser un error

BEGIN{
#   fort["module"]="class {";
#   fort["end module"]="}";
#   fort["use"]="#include <";
  fort["type[\(]([^\)]+)[\)] *, *(optional *, *)?(allocatable|intent[\(][^\)]+[\)]) +:: *([^\(]+).*"]="FArray<F\\1> \\4;";
  fort["type[\(]([^\)]+)[\)](* , *optional)? *:: *([^\(]+).*"]="\\1 \\3;";
  fort["real[\(] *8 *[\)] *, *(optional *, *)?(allocatable|intent[\(][^\)]+[\)]) +:: *([^\(]+).*"]="FArray<double> \\3;";
  fort["real[\(] *8 *[\)]( *, *optional)? *:: *([^\(]+).*"]="double \\2;";
  fort["integer[\(] *8 *[\)] *, *(optional *, *)?(allocatable|intent[\(][^\)]+[\)]) +:: *([^\(]+).*"]="FArray<long long> \\3;";
  fort["integer[\(] *8 *[\)]( *, *optional)? *:: *([^\(]+).*"]="long long \\2;";
  fort["integer *, *(optional *, *)?(allocatable|intent[\(][^\)]+[\)]) +:: *([^\(]+).*"]="FArray<int> \\3;";
  fort["integer( *, *optional)? *:: *([^\(]+).*"]="int \\2;";
  fort1["^[[:blank:]]*integer"]="int";
  
}

($0~/^[[:blank:]]*!\*\*[^*]*$/ && contrl==0){
  if (uso!=""){
    print uso;
  }
  if (coment!=""){
    print coment;
  }
  if (clase!=""){
    print clase "\nprivate:";
  }
  if (tipo!=""){
    print tipo;
  }
#   for (i=1;i<=nsubr;i++){
#     print subr[i];
#   }
#   delete subr;
  if (subr!=""){
    print subr ";";
  }
  if (fun!=""){
    if (fun~/result[\(]/){
#      print fun;
      print gensub(/(.*)result[\(]([^\)]+) +[^\)]+[\)]/,"\\2 \\1","1",fun);
    }
    else{
      print fun ";";
    }
  }
  fun="";
  subr="";
  uso="";
  coment="";
  clase="";
  tipo="";
  contrl=1;
  coment="\n/**";
  next;
}

($0~/^[[:blank:]]*!\*\*[^*]*$/ && contrl==1){
  contrl=0;
  coment=coment "\n */";
  next;
}

(contrl==1){
  sub ("[[:blank:]]*![[:blank:]]*"," * ",$0);
  coment=coment "\n" $0;
}

(contrl==0 && $0~/^[[:blank:]]*module/){
  gsub("module","class",$0);
  clase= $0 " {";
}

(contrl==0 && $0~/^[[:blank:]]*use[[:blank:]]/){
  gsub (/[[:blank:]]*use[[:blank:]]*/,"#include <",$0);
  uso=uso $0 ".h>\n";
}

(contrl==0 && $0~/^[[:blank:]]*type[[:blank:],]/){
  typcontrl=1;
  next;
}

(contrl==0 && $0~/^[[:blank:]]*end *type[[:blank:]]*/){
  typcontrl=0;
  next;
}

(typcontrl==1){
  for (coso in fort){
    if ($0~coso){
      tipo=tipo gensub(coso,fort[coso],"g",$0) "\n";
      next;
    }
  }
#  tipo=tipo gensub(/type[\(]([^\)]+)[\)] *, *allocatable *:: *([^\(]+).*/,"VFArray<\\1> \\2","g",$0) "\n";
#    tipo=tipo $0 "\n";
}

(contrl==0 && $0~/^[[:blank:]]*subroutine[[:blank:]]*/){
#  nsubr++;
#  subr[nsubr]=$0;
  subr=$0;
  while ($0~/&[[:blank:]]*$/){
    getline;
#    subr[nsubr]=subr[nsubr] " " $0;
    subr=subr " " $0;
  }
  gsub(/&/,"",subr);
  gsub(/[[:blank:]]+/," ",subr);
  sub(/subroutine/,"void",subr);
  subrcontrl=1;
}

(contrl==0 && $0~/end *subroutine/){
  subrcontrl=0;
}

(subrcontrl==1){
# #  print "vamos en " $0;
#   for (coso in fort){
#     if ($0~coso){
#       replace=gensub(/;/,"","g",gensub(/^[[:blank:]]+/,"","g",gensub(coso,fort[coso],"g",$0)));
# #      print replace;
# #      print subr;
#       n=split(replace,arr);
# #      print substr(arr[n],1,length(arr[n])-1);
#       sub(arr[n],replace,subr);
#       next;
#     }
#   }
#      subr=subr "\n" gensub(coso,fort[coso],"g",$0);
  for (coso in fort){
    if ($0~coso){
#     print coso;
#     print $0;
      replace= "\\1" gensub(/;/,"","g",gensub(/^[[:blank:]]+/,"","g",gensub(coso,fort[coso],"g",$0)));
#      print replace;
#      print subr;
      n=split(replace,arr);
#      print replace;
#      print substr(arr[n],1,length(arr[n])-1);
#      sub(arr[n],replace,fun);
      target= "([\(].*)" arr[n] "([,\)[:blank:]])";
      replace=replace "\\2";
#      print target;
      subr=gensub(target,replace,"g",subr);
      next;
    }
  }
#      subr=subr "\n" gensub(coso,fort[coso],"g",$0);
}

(contrl==0 && $0~/end *function/){
#  print "vamos";
  funcontrl=0;
  next;
}

(contrl==0 && $0~/^[[:blank:]]*[[:alnum:]]*[[:blank:]]*function[[:blank:]]*/){
#  print "vamos";
#  nsubr++;
#  subr[nsubr]=$0;
  fun=$0;
  while ($0~/&[[:blank:]]*$/){
    getline;
#    subr[nsubr]=subr[nsubr] " " $0;
    fun=fun " " $0;
  }
  gsub(/&/,"",fun);
  gsub(/[[:blank:]]+/," ",fun);
  sub(/function/,"",fun);
  funcontrl=1;
  for (coso in fort1){
    if (fun~/coso/){
      sub(coso,fort1[coso],fun);
      next;
    }
  }
}

(funcontrl==1){
#  print "vamos en " $0;
#  print fun;
  for (coso in fort){
#    print coso;
    if ($0~coso){
      replace= "\\1" gensub(/;/,"","g",gensub(/^[[:blank:]]+/,"","g",gensub(coso,fort[coso],"g",$0)));
#      print replace;
#      print subr;
      n=split(replace,arr);
#      print replace;
#      print substr(arr[n],1,length(arr[n])-1);
#      sub(arr[n],replace,fun);
      target= "([\(].*)" arr[n] "([,\)[:blank:]])";
      replace=replace "\\2";
#      print target;
      fun=gensub(target,replace,"g",fun);
      next;
    }
  }
#      subr=subr "\n" gensub(coso,fort[coso],"g",$0);
}

(contrl==0 && $0~/^[[:blank:]]*end *module/){
    if (uso!=""){
    print uso;
  }
  if (coment!=""){
    print coment;
  }
  if (clase!=""){
    print clase;
  }
  if (subr!=""){
    print subr ";";
  }
  if (fun!=""){
    if (fun~/result[\(]/){
#      print fun;
      print gensub(/(.*)result[\(]([^\)]+) +[^\)]+[\)]/,"\\2 \\1","1",fun);
    }
    else{
      print fun ";";
    }
  }
  print "}";
}
