#!/usr/bin/gawk -f
#**************************************************************************
#   Copyright (C) 2007 by Universidad Nacional de Colombia                *
#   http://www.unal.edu.co                                                *
#                                                                         *
#   This program is free software; you can redistribute it and/or modify  *
#   it under the terms of the GNU General Public License as published by  *
#   the Free Software Foundation; either version 2 of the License, or     *
#   (at your option) any later version.                                   *
#                                                                         *
#   This program is distributed in the hope that it will be useful,       *
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#   GNU General Public License for more details.                          *
#                                                                         *
#   You should have received a copy of the GNU General Public License     *
#   along with this program; if not, write to the                         *
#   Free Software Foundation, Inc.,                                       *
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#**************************************************************************
#Actinium Ac
#Aluminum Al
#Barium Ba
#Beryllium Be
#Bismuth Bi
#Bohrium Bh
#Cadmium Cd
#Calcium Ca
#Cesium Cs
#Chromium Cr
#Cobalt Co
#Copper Cu
#Darmstadtium Ds
#Dubnium Db
#Francium Fr
#Gallium Ga
#Gold Au
#Hafnium Hf
#Hahnium Ha
#Hassium Hs
#Iron Fe
#Indium In
#Iridium Ir
#Lanthanum La
#Lead Pb
#Lithium Li
#Magnesium Mg
#Manganese Mn
#Meitnerium Mt
#Mercury Hg
#Molybdenum Mo
#Nickel Ni
#Niobium Nb
#Osmium Os
#Palladium Pd
#Platinum Pt
#Potassium K
#Radium Ra
#Rhenium Re
#Rhodium Rh
#Rubidium Rb
#Ruthenium Ru
#Rutherfordium Rf
#Scandium Sc
#Seaborgium Sg
#Silver Ag
#Sodium Na
#Strontium Sr
#Tantalum Ta
#Technetium Tc
#Thallium Tl
#Tin Sn
#Titanium Ti
#Tungsten W
#Ununbium Uub
#Ununhexium Uuh
#Ununquadium Uuq
#Ununumium Uuu
#Vanadium V
#Yttrium Y
#Zinc Zn
#Zirconium Zr
#Antimony Sb
#Arsenic As
#Astatine At
#Boron B
#Germanium Ge
#Polonium Po
#Silicon Si
#Tellurium Te
#Bromine Br
#Carbon C
#Chlorine Cl
#Fluorine F
#Hydrogen H
#Iodine I
#Nitrogen N
#Oxygen O
#Phosphorus P
#Selenium Se
#Sulfur S
#Argon Ar
#Helium He
#Krypton Kr
#Neon Ne
#Radon Rn
#Xenon Xe
#Americium Am
#Berkelium Bk
#Californium Cf
#Cerium Ce
#Curium Cm
#Dysprosium Dy
#Einsteinium Es
#Erbium Er
#Europium Eu
#Fermium Fm
#Gadolinium Gd
#Holmium Ho
#Lawrencium Lr
#Lutetium Lu
#Mendelevium Md
#Neodymium Nd
#Neptunium Np
#Nobelium No
#Plutonium Pu
#Prasendymium Pr
#Promethium Pm
#Protactinium Pa
#Samarium Sm
#Terbium Tb
#Thorium Th
#Thulium Tm
#Uranium U
#Ytterbium Yb

#**
#  @brief Convierte archivos en formato .kv del proyecto MPQC ( http://www.mpqc.org/ )
#  @author Andr�s Bernal
#
#  Lee un archivo de bases en formato .kv y genera un documento xml estructurado.
#  Requiere gawk.
#
#  La especificaci�n del documento xml es la siguiente:
#
#  <Basis name>
#    <Atom name symbol contraction>
#       <ContractedGaussian size shellCode pure>
#          <PrimitiveGaussian exponent coefficient/>
#       </ContractedGaussian>
#    </Atom>
#  </Basis>
#
#  <b> Fecha de creaci�n : </b> 2007-09-23
#**


BEGIN{
  ##******************************************************
  # Lee los nombres y s�mbolos correspondientes a cada 
  # uno de los elementos qu�micos, archivados en el mismo 
  # c�digo fuente de este script (i.e. el archivo que est�
  # leyendo ahora mismo), y los carga en el arreglo symbol. 
  # La llave es el nombre del elemento, el valor es el 
  # s�mbolo correspondiente
  getline < "kv2xml.awk";
  while ($0~/^\#/){
    getline < "kv2xml.awk";
    if($0!=""){
      symbol[substr($1,2)]=$2;
    }
  }
  close("kv2xml");
  ##******************************************************

  ##******************************************************
  # Establece una expresi�n regular que empata con la 
  # primera l�nea de cada especificaci�n de un conjunto 
  # base (corresponde al objeto <Atom> en el xml) en el 
  # archivo .kv como el nuevo separador de registros (RS), 
  # y un cambio de l�nea como el nuevo separador de 
  # campos (FS)
  RS="(\n%)+ *BASIS *SET:[[:punct:][:alnum:][:blank:]]+.";
  FS="\n";
  ##******************************************************
}

#**
# El primer registro del archivo .kv est� compuesto por 
# el nombre y la lista de referencias bibliogr�ficas 
# de la base especificada
#**
(NR==1){
   ##******************************************************
   # Imprime la lista de referencias bibliogr�ficas de la 
   # base como comentarios xml
   for (i=3;i<=NF;i++){
     sub(/%/,"",$i);
     if ($i!~/^[-[:blank:]]*$/){
       print "<!-- " $i " -->";
     }
   }
   print "\n";
   ##******************************************************
   ## Genera e imprime la etiqueta de apertura del objeto <Basis>
   print "<Basis name=" substr($0,match($0,"\"[^\"]+\""),RLENGTH) ">\n";
   ##******************************************************

   ##******************************************************
   # Lee el valor del atributo contraction del primer 
   # objeto <Atom>. Esta valor se extrae de RT, que es
   # el texto que empat� con RS
   contract=gensub(/ /,"","g",substr(RT,match(RT,": ")+2,length(RT)-match(RT,":")-2));
   next;
   ##******************************************************
}


##**
# A continuaci�n el programa va leyendo fragmento a 
# fragmento cada entrada del archivo .kv, genera la 
# etiqueta del objeto xml correspondiente y elimina el 
# fragmento leido.
##**
{
  ##******************************************************
  # Elimina los arreglos pos1 y pos2
  delete pos1;
  delete pos2;
  ##******************************************************

  ##******************************************************
  # Extrae el nombre del �tomo del primer campo del 
  # registro actual y lo almacena en la variable nome. 
  # Cambia la primera letra de ese nombre por may�scula 
  # antes de almacenarlo
  nome=toupper(gensub(/^ *([^:]).*/,"\\1","g",$1)) gensub(/^ *[^:]([^:]+).*/,"\\1","g",$1);
  ##******************************************************

  ##******************************************************
  # Genera e imprime la etiqueta de apertura del objeto 
  # <Atom> correspondiente al registro actual, tomando el 
  # nombre de la variable nome, el valor de symbol del 
  # elemento correspondiente a nome en el arreglo symbol y 
  # el atributo contraction de la variable contract. Note 
  # que contraction se debe sacar del RT correspondiente 
  # al registro anterior.
  print "   <Atom name=\"" nome "\" symbol=\"" symbol[nome] "\" contraction=\"" contract "\">";
  ##******************************************************

  ##******************************************************
  # Actualiza contract al nuevo RT. Este valor ser� usado 
  # para generar el atributo contraction del pr�ximo 
  # objeto <Atom>
  ##******************************************************
  contract=gensub(/ /,"","g",substr(RT,match(RT,": ")+2,length(RT)-match(RT,":")-2));
  ##******************************************************

  ##******************************************************
  # Elimina la parte del registro actual correspondiente 
  # al nombre del �tomo
  sub(/^[^\[]*/,"",$0);
  ##******************************************************

  ##******************************************************
  # El siguiente ciclo corre sobre cada conjunto de datos 
  # correspondiente a los coeficientes y exponentes de una
  # gaussiana contraida en el archivo .kv. Se hace notar 
  # que cuando distintas gaussianas comparten los mismos 
  # exponentes, los datos de esas gaussianas se presentan 
  # simult�neamente, por lo cual un solo ciclo while puede
  # generar varios objetos <ContractedGaussian>
  while(match($0,/\(type: *\[\(?am *= */)){
    ##******************************************************
    # Lee el valor que asignar� al atributo shellcode de 
    # la(s) gaussiana(s) contraida(s) (e.g. s, p, d) y lo 
    # almacena en la variable type
    type=gensub(/[^\(]*type: *\[\(?(am *= *[^\]]+).*/,"\\1","g",$0);
    ##******************************************************

    ##******************************************************
    # Busca el string "puream = 1" (o similares) en type, si
    # lo encuentra lo elimina, asegurando que la variable 
    # type contenga lo que debe contener, y asigna un valor 
    # diferente a cero a la variable pure. El valor de esta 
    # variable determinar� el valor del atributo pure del 
    # objeto <ContractedGaussian>
    pure=gsub(/ *puream *= *1\)/,"",type);
    #******************************************************
    
    ##******************************************************
    # Divide type en el arreglo types, usando / *am *= */ 
    # como separador. Cada elemento del arreglo types tiene 
    # como valor el tipo (atributo shellCode) de una y solo
    # una gaussiana contraida. El n�mero de elementos de 
    # types corresponde al n�mero de gaussianas contraidas 
    # que comparten el mismo exponente, cuyos datos se 
    # encuentran en los campos subsecuentes del archivo .kv;
    # este valor se almacena en la variable n.
    n=split(type,types," *am *= *");
    ##******************************************************

    ##******************************************************
    # Esta l�nea y el ciclo subsecuente tienen como objeto 
    # identificar el orden en que se listan los coeficientes 
    # de las gaussianas contraidas y archivar esos datos en
    # los arreglos pos1 y pos2.
    o=split(gensub(/[^\{]*\{(exp[^\}]*).*/,"\\1","g",$0),pos1," *coef:");
    for (i=2;i<=o;i++){
      pos2[pos1[i]+1]=i;
    }
    #******************************************************

    ##******************************************************
    # Elimina el fragmento del registro actual que tiene que
    # ver con los objetos pure y shellCode del objeto 
    # <ContractedGaussian>
    sub(/[^\{]+/,"",$0);
    ##******************************************************

    ##******************************************************
    # Identifica el n�mero de gaussianas primitivas en la 
    # gaussiana contraida y almacena ese valor en la 
    # variable m
    m=split(substr($0,1,match($0,/[[:blank:]]*\}\)/)-2),expcoefs,"\n");
    ##******************************************************

    ##******************************************************
    # Imprime todos los objetos <ContractedGaussian> 
    # correspondientes al actual conjunto de datos (vea el
    # ciclo while m�s arriba) del resgistro (objeto <Atom>)
    # actual
    for (i=n;i>1;i--){
	##******************************************************
	# Si pure es diferente de cero, asigna el valor 1 al
	# atributo pure; en caso contrario no lo imprime, lo
	# cual significa que tiene asignado el valor cero por
	# omisi�n
	if (pure){
		print "      <ContractedGaussian size=\"" m-1 "\" shellCode=\"" types[i] "\" pure=\"1\">";
      	}
      	else{
		print "      <ContractedGaussian size=\"" m-1 "\" shellCode=\"" types[i] "\">";
      	}
	##******************************************************
	
	##******************************************************
	# Imprime todos los objetos <PrimitiveGaussian> del
	# objeto <ContractedGaussian> actual (vea el ciclo for
	# m�s arriba)
	for (j=2;j<=m;j++){
		split(gensub(/^[[:blank:]]+/,"","g",expcoefs[j]),coefs,"[[:blank:]]+");
		print "         <PrimitiveGaussian exponent=\"" coefs[1] "\" coefficient=\"" coefs[pos2[i-1]] "\"/>";
	}
	##******************************************************
	print "      </ContractedGaussian>";
    }
    ##******************************************************

    ##******************************************************
    # Elimina el fragmento del registro actual que
    # corresponde a todos los objetos <ContractedGaussian>
    # que acaban de ser impresos
    sub(/\{[^\{]*\{[^\(]*/,"",$0);
    #******************************************************
  }
  ##******************************************************

  ##******************************************************
  # Imprime la etiqueta de cierre del objeto <Atom>
  print "   </Atom>\n";
  ##******************************************************
}

END{
  ##******************************************************
  # Imprime la etiqueta de cierre del objeto <Basis>
  print "</Basis>";
  ##******************************************************
}
