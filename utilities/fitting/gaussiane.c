#include "gaussiane.h"

void InputData_get( char *fileName){
  
  FILE *InputFile;
  float value[2];

  InputFile=fopen(fileName,"r");
  if( InputFile==NULL){
    printf("\n");
    printf("The input file can't be open. Aborting fitting... \n");
    printf("\n");
    exit(0);
  };
  int i=-1;
  while(!feof(InputFile) && (fscanf(InputFile,"%f",&value[1])!=NULL) ){
    i++;
  };

  numOfInputData=i/2;
  InputData.y=(double *)malloc(numOfInputData*sizeof(double));
  InputData.sigma=(double *)malloc(numOfInputData*sizeof(double));
  InputData.r_i=(double *)malloc(numOfInputData*sizeof(double));
  InputData.n=numOfInputData;
  printf("File name for input data:      %s\n",fileName);
  printf("Number of input points:        %i\n",numOfInputData);
  rewind(InputFile);

  for(i=0;i<numOfInputData;i++){
    fscanf(InputFile,"%f",&value[0]);
    fscanf(InputFile,"%f",&value[1]);
    InputData.r_i[i]=value[0];
    InputData.y[i]=value[1];
    InputData.sigma[i]=gaussianError;
    // printf("%i %f %f \n",i,InputData.r_i[i],InputData.y[i]);
  };
  
  fclose(InputFile);
  
};

struct gaussFunc * buildGaussArray(int numOfComponents ){
  struct gaussFunc *gP;
  double origin;
  double exponent;
  double factor;
  double b;
  int arraySize;
  int k;

  numOfParams=2*numOfComponents;
  if(optimizeCenter==1){
    numOfParams=3*numOfComponents;
  };
  
  numOfParams=numOfParams+1;

  arraySize=sizeof(struct gaussFunc)*numOfComponents;

  factor=1.0;  
  exponent=0.0;
  origin=0.0;
  b=1.0;
  printf("Num. of parameters to adjust:  %i\n",numOfParams);
  x_init=(double *) malloc(sizeof(double)*numOfParams);

  if(numOfParams>numOfInputData){
    printf("\n");
    printf("The number of input data has to be, minimum %i\n",numOfParams);
    printf("Aborting fitting... \n");
    printf("\n");
    exit(0);
  };

  gP=(struct gaussFunc *) malloc(arraySize);
  
  k=0;
  for(int i=0;i<numOfComponents;i++){

    // Adjust initial origin
    gP[i].factor=factor;
    gP[i].exponent=exponent;
    gP[i].origin=origin;
    
    
    x_init[k]=gP[i].factor;
    x_init[k+1]=gP[i].exponent;

    if(optimizeCenter==1){
      x_init[k+2]=gP[i].origin;
      k=k+3;
    }
    else{
      k=k+2;
    };
  };

  x_init[numOfParams-1]=b;
  functionOffset=x_init[numOfParams-1];
  return gP;
};

void fittingFunction_set(const gsl_vector *x){
  int offset;
  int k;

  k=2;
  if(optimizeCenter==1){
    k=3;
  };

  for(int i=0; i<numOfComponents; i++){
    offset=i*k;
    
    fittingFunc[i].factor=gsl_vector_get(x, offset);
    fittingFunc[i].exponent=gsl_vector_get(x, offset+1);

    if(optimizeCenter==1){
      fittingFunc[i].origin=gsl_vector_get(x, offset+2);
    };
    
  };
  functionOffset=gsl_vector_get(x, numOfParams-1);

};

double fittingFunction_eval_radial(double r){

  double y=0.0;
  double R;

  for(int i=0; i<numOfComponents; i++){

    R=fittingFunc[i].origin;    

    y=y+( fittingFunc[i].factor
    	  * exp(-fittingFunc[i].exponent
    		* ( (r-R)*(r-R) ) ) );
    
    /* y=y+( fittingFunc[i].factor */
    /* 	  * exp(-fittingFunc[i].exponent */
    /* 		* ( (r-R) ) ) ); */

  };
  y=y+functionOffset;
  return y;
};


double fittingFunction_df_eval_radial(double r,int componentNum, int parameterNum){

  double dy,out;
  double R;
  R=fittingFunc[componentNum].origin;

  dy=exp(-fittingFunc[componentNum].exponent*(r-R)*(r-R) );

  switch(parameterNum){
  case 0:
    out=dy;
    break;
  case 1:
    out=-fittingFunc[componentNum].factor*(r-R)*(r-R)*dy;
    break;
  case 2:
    out=fittingFunc[componentNum].factor*dy*2.0*fittingFunc[componentNum].exponent*(r-R);
    break;
  };

  /* dy=exp(-fittingFunc[componentNum].exponent*(r-R) ); */
  /* switch(parameterNum){ */
  /* case 0: */
  /*   out=dy; */
  /*   break; */
  /* case 1: */
  /*   out=-fittingFunc[componentNum].factor*(r-R)*dy; */
  /*   break; */
  /* case 2: */
  /*   out=fittingFunc[componentNum].factor*dy*2.0*fittingFunc[componentNum].exponent*(r-R); */
  /*   break; */
  /* }; */

  return out;
};

int fittingFunction(const gsl_vector * x, void *dataToAdjust, 
		     gsl_vector * f){
  double *r_i = ((struct dataToAdjust *)dataToAdjust)->r_i;
  double *y = ((struct dataToAdjust *)dataToAdjust)->y;
  double *sigma = ((struct dataToAdjust *) dataToAdjust)->sigma;

  fittingFunction_set(x);

  for (int i = 0; i < numOfInputData; i++){
    // Model Yi =SUM_{i}_{numOfComponents}( F_i * exp(-lambda * (r-R)^2 ) ) + b

    double Yi = fittingFunction_eval_radial( r_i[i] );
    gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
  };

  return GSL_SUCCESS;
}


int fittingFunction_df (const gsl_vector * x, void *dataToAdjust, 
			 gsl_matrix * J){
  double *sigma = ((struct dataToAdjust *) dataToAdjust)->sigma;
  double *r_i = ((struct dataToAdjust *)dataToAdjust)->r_i;
  double df;
  int i,k,j,offset;

  fittingFunction_set(x);

  k=2;
  if(optimizeCenter==1){
    k=3; 
  };
  for(i=0;i<numOfInputData;i++){
    
    for(j=0; j<numOfComponents; j++){
      offset=j*k;
 
      df=fittingFunction_df_eval_radial(r_i[i],j,0);
      gsl_matrix_set (J, i, offset, df/sigma[i]);
 
      df=fittingFunction_df_eval_radial(r_i[i],j,1);
      gsl_matrix_set (J, i, offset+1, df/sigma[i]);

      if(optimizeCenter==1){
  	df=fittingFunction_df_eval_radial(r_i[i],j,2);
  	gsl_matrix_set (J, i, offset+2, df/sigma[i]);
      };
    };  
    gsl_matrix_set (J, i, numOfParams-1, 1/sigma[i]);
  };
  return GSL_SUCCESS;
}


int fittingFunction_fdf (const gsl_vector * x, void *dataToAdjust,
			 gsl_vector * f, gsl_matrix * J){
  fittingFunction(x, dataToAdjust, f);
  fittingFunction_df(x, dataToAdjust, J);
  return GSL_SUCCESS;
};

void print_state (size_t iter, gsl_multifit_fdfsolver * s){

  printf("\n");
  printf("===================\n");
  printf(" ITERATION No. %i\n",iter);
  printf("===================\n");

  int k=2;
  if(optimizeCenter==1){
    k=3;
  };
  
  for(int j=0; j<numOfComponents; j++){
    int offset=j*k;
  
    printf("\tCOMPONENT %i:\n",j+1);
    printf("\t\tFactor(%i):  \t %15.6f\n",j+1,gsl_vector_get (s->x, offset));
    printf("\t\tExponent(%i):\t %15.6f\n",j+1,gsl_vector_get (s->x, offset+1));
    
    if(optimizeCenter==1){
      printf("\tCenter(%i):  \t %15.6f\n",j+1,gsl_vector_get (s->x, offset+2));
    };
  
  };
  printf("\tb:\t\t\t %15.6f\n",gsl_vector_get (s->x, numOfParams-1));
  printf ("\t|f(x)| = %g\n", gsl_blas_dnrm2 (s->f));

}


void print_finalState(gsl_multifit_fdfsolver *s,char *fileName, int status,int iter){
  int offset;
  char *gnuplotFile;
  char *psFile;
  char *xmlFile;
  gsl_matrix *covar = gsl_matrix_alloc (numOfParams,numOfParams);
  gsl_multifit_covar (s->J, 0.0, covar);


  xmlFile=(char *)malloc( (strlen(fileName)+4)*( sizeof(char) ) );
  sprintf(xmlFile,"%s%s",fileName,".xml");
  
#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
  
  printf("\n");
  printf("Fitting model: \n");
  printf("\t_n_\n");
  printf("\t\\   F_i exp(-a_i(r-R_i)^2 ) + b\n");
  printf("\t/__\n");
  printf("\t i\n");




  printf("\n");
  printf("PARAMETERS ADJUSTED IN %i  ITERATIONS: \n",iter);
  printf("=========================================\n");
  double chi = gsl_blas_dnrm2(s->f);
  double dof = numOfInputData - numOfParams;
  double c = GSL_MAX_DBL(1, chi / sqrt(dof));
  //    printf("chisq/dof = %g\n",  pow(chi, 2.0) / dof);
  int k=2;
  if(optimizeCenter==1){
    k=3;
  };

  FILE *outputFile;
  outputFile=fopen(xmlFile,"w");

  fprintf(outputFile,"<!-- Potential fit  to gaussian model -->\n");
  fprintf(outputFile,"<!-- Generated by fitting utility     -->\n\n");
  fprintf(outputFile,"<Potential name=\"%s\" type=\"gaussian\" coordinate=\"radial\">\n",fileName);
  fprintf(outputFile,"\t<Interaction name=\"undefined\" size=\"%i\">\n",numOfComponents);
  for(int j=0;j<numOfComponents;j++){  
    offset=j*k;
    printf ("COMPONENT %i\n", j+1);
    //    printf ("\t Factor    = %15.5e +/- %.5f\n", FIT(offset), c*ERR(offset));
    //    printf ("\t Exponent  = %.5f +/- %.5f\n", FIT(offset+1), c*ERR(offset+1));
    printf ("\t Factor    = %15.5e \n", FIT(offset));
    printf ("\t Exponent  = %.5f \n", FIT(offset+1));
    
    fprintf(outputFile,"\t\t<GaussianComponent exponent=\"%f\" factor=\"%f\"",FIT(offset+1),FIT(offset));


    if(optimizeCenter==1){
      //      printf ("\t center  = %.5f +/- %.5f\n", FIT(offset+2), c*ERR(offset+2));
      printf ("\t center  = %.5f \n", FIT(offset+2));
      fprintf(outputFile," x=\"%f\" y=\"0.0000\" z=\"0.0000\" />\n",FIT(offset+2));
    }
    else{
      fprintf(outputFile," x=\"0.0000\" y=\"0.0000\" z=\"0.0000\"/>\n");
    };
    
  }

  printf ("COMPONENT %i\n", numOfComponents+1);
  printf ("\t Factor    = %15.5e +/- %.5f\n", FIT(numOfParams-1), c*ERR(numOfParams-1));
  printf ("\t Exponent  = %.5f\n", 0.0 );
  if(optimizeCenter==1){
    printf ("\t center  = %.5f \n", 0.000);
  };

  //  printf ("b         = %.5f +/- %.5f\n", FIT(numOfParams-1), c*ERR(numOfParams-1));
  fprintf(outputFile,"\t\t<GaussianComponent exponent=\"%f\" factor=\"%f\"",0.0,FIT(numOfParams-1));
  fprintf(outputFile," x=\"0.0000\" y=\"0.0000\" z=\"0.0000\"/>\n");

  fprintf(outputFile,"\t</Interaction>\n");
  fprintf(outputFile,"</Potential>\n");

  fclose(outputFile);
  printf ("status = %s\n", gsl_strerror (status));
  
  double *rr = InputData.r_i;  
  double *yi = InputData.y;  

  outputFile=fopen(fileName,"w");
  if( outputFile==NULL){
    printf("\n");
    printf("Error writing the output file...\n");
    printf("\n");
    exit(0);
  };

  fprintf(outputFile,"# Outfile from fitting \n");
  for(int l=0;l<numOfInputData;l++){
    double out=0.0;
    double R=0.0;
    for(int j=0;j<numOfComponents;j++){  
      offset=j*k;
    
      if(optimizeCenter==1){
	R= FIT(offset+2);
      }
    
      out=out+(FIT(offset)*exp(-FIT(offset+1)*(rr[l]-R)*(rr[l]-R) ) );
    
    };
    out=out+FIT(numOfParams-1);
    fprintf(outputFile,"%f %f %f\n",rr[l],yi[l],out);
  };
  fclose(outputFile);

  
  gnuplotFile=(char *)malloc( (strlen(fileName)+4)*( sizeof(char) ) );
  sprintf(gnuplotFile,"%s%s",fileName,".gnp");

  psFile=(char *)malloc( (strlen(fileName)+3)*( sizeof(char) ) );
  sprintf(psFile,"%s%s",fileName,".ps");

  outputFile=fopen(gnuplotFile,"w");
  if( outputFile==NULL){
    printf("\n");
    printf("Error writing the gnuplot  file...\n");
    printf("\n");
    exit(0);
  };

  fprintf(outputFile,"set term post eps enh color dashed rounded dl 4 \"Times-Bold\" 15\n");
  fprintf(outputFile,"set output \"%s\" \n",psFile);
  fprintf(outputFile,"set encoding iso_8859_1 \n");
  fprintf(outputFile,"set title \"NONLINEAR LEAST-SQUARES FITTING TO GAUSSIAN MODEL {/Symbol S}$^{n+1}(f_i e^{-{/Symbol a}|r-R_i|^2} ) \" \n");
  fprintf(outputFile,"set xlabel \" r /{\\305} \" \n");
  fprintf(outputFile,"set format x \" %s.2f \" \n","%");
  fprintf(outputFile,"set ylabel \" U /Hartree \" \n");
  fprintf(outputFile,"set format y \" %s.2f \" \n","%");
  fprintf(outputFile,"plot \"%s\" using 1:3 w l lt rgb \"black\" title \"Fitting model\", ",fileName);
  fprintf(outputFile,"\"%s\" using 1:2 with points pointtype 7 pointsize 0.5 lt rgb \"blue\" title \"InputData\"\n",fileName);
  fprintf(outputFile,"set output \n");
  fclose(outputFile);
  char command[256];
  sprintf(command,"gnuplot %s",gnuplotFile);
  system(command);
  //  remove(gnuplotFile);

  gsl_matrix_free (covar);
};
