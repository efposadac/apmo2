program generateData
  implicit none
  real(8) :: factor
  real(8) :: exponent
  real(8) :: center
  integer :: i
    
  factor= 5.33190_8
  exponent  = 0.00195
  center  = 14.40444


  do i=0,10000
     print *,i*0.01,factor*exp(-exponent*(0.01*i-center)**2)
  end do
  print *,""


  do i=0,10000
     print *,i*0.01,exp(exponent*0.01*i*center)
  end do


end program generateData
