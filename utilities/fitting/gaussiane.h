struct gaussFunc *fittingFunc;

int optimizeCenter;
int numOfParams;
int numOfComponents;
int numOfExpComponents;
int numOfInputData;
int printCycles;
double gaussianError;
double functionOffset;
double absoluteError;
double relativeError;
double *x_init;


struct gaussFunc{
  double factor;
  double exponent;
  double origin;
};

struct dataToAdjust{
  size_t n;
  double * y;
  double * sigma;
  double * r_i;
};

struct dataToAdjust InputData;
