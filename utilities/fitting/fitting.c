#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include "gaussiane.c"

int main (int argc, char *argv[])
{
  unsigned int i,maximumIteration;
  char *fileName;
  char *outputFileName;
  /*******************************************
        Ajuste de parametros por omisión
  ********************************************/

  numOfExpComponents=3;
  numOfComponents=3;
  gaussianError=0.1;
  maximumIteration=500;
  optimizeCenter=0;
  printCycles=0;
  absoluteError=1e-4;
  relativeError=1e-4;
  
  
  fileName=(char *)malloc( strlen(argv[argc-1])*( sizeof(char) ) );
  fileName=argv[argc-1];
  outputFileName=(char *)malloc( (strlen(argv[argc-1])+4)*( sizeof(char) ) );
  sprintf(outputFileName,"%s%s",fileName,".fit");
  
  if( argc == 2 ){
    
    if( ( strlen(argv[1]) == strlen("--help") )
	&& !strncmp(argv[1], "--help",7)){
      printf("Fitting  is an application to fit radial functions with a linear combination \n");
      printf("of gaussians functions. Fitting is an utility for APMO code and is part of it.\n");
      printf("Fitting use nonlinear least-squares fitting algorithms from GSL library.\n");
      printf("[Cited online]: http://www.gnu.org/software/gsl/ \n");

      printf("\n");
      printf("Use: fitting [OPTION]... file \n");
      printf("\n");
      printf("FITTING OPTIONS \n");
      printf("\t -n #    \t Number of gaussian functions in the linear combination. \n");
      printf("\t -m #    \t Number of gaussian functions in the exponential term. \n");
      printf("\t -iter # \t Maximum number of iterations in the fitting process. \n");
      printf("\t -c      \t Optimize gaussian centers. \n");
      printf("\t -p      \t Print parameters at every optimization step. \n");
      printf("\t -o file \t Name of the output file. \n");
      printf("\t -ae aErr\t Absolute error (aErr) to the current parameter respect to the last step. \n");
      printf("\t -s gErr \t Gassian error (gErr) associated to input data. \n");
      printf("\t -re rErr\t Relative error (rErr) to the current parameter respect to the last step. \n");
      printf("\t --help  \t Show this output. \n");
      printf("\n");
      printf("BUG REPORTS \n");
      printf("If you find a bug in fitting, please send electronic mail to sergmonic@gmail.com \n");
       
      return 0;
    };
  }
  else if(argc>2){
    
    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-n") )
	  && !strncmp(argv[i], "-n",2)){
	numOfComponents=atoi(argv[i+1]);
	if(numOfComponents==0)numOfComponents=1;
	break;
      };
    };

    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-m") )
	  && !strncmp(argv[i], "-m",2)){
	numOfExpComponents=atoi(argv[i+1]);
	if(numOfExpComponents==0)numOfExpComponents=1;
	break;
      };
    };
    
    for(i=1;i<argc-1;i++){
      if( ( strlen(argv[i]) == strlen("-s") )
	  && !strncmp(argv[i], "-s",2)){
	gaussianError=atof(argv[i+1]);
	break;
	
      };
    };
    
    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-iter") )
	  && !strncmp(argv[i], "-iter",5)){
	maximumIteration=atoi(argv[i+1]);
	if(maximumIteration==0) maximumIteration=500;
	break;
      };
    };
    
    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-c") )
	  && !strncmp(argv[i], "-c",2)){
	optimizeCenter=1;
	break;
      };
    };

    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-p") )
	  && !strncmp(argv[i], "-p",2)){
	printCycles=1;
	break;
      };
    };

    for(i=1;i<argc-1;i++){

      if( ( strlen(argv[i]) == strlen("-o") )
	  && !strncmp(argv[i], "-o",2)){
	free(outputFileName);
	outputFileName=(char *)malloc( (strlen(argv[i+1]))*( sizeof(char) ) );
	outputFileName=argv[i+1];
	break;
      };
    };


    for(i=1;i<argc-1;i++){
      if( ( strlen(argv[i]) == strlen("-ae") )
	  && !strncmp(argv[i], "-ae",3)){
        absoluteError=atof(argv[i+1]);
	break;
	
      };
    };

    for(i=1;i<argc-1;i++){
      if( ( strlen(argv[i]) == strlen("-re") )
	  && !strncmp(argv[i], "-re",3)){
        relativeError=atof(argv[i+1]);
	break;
	
      };
    };

  }
  else{
    printf("Use: fitting [OPTION]... file.fit \n");
    printf("Try `fitting --help' for more information. \n");
    return 0;
  };
  

  printf("\n");
  printf("***********************************************\n");
  printf("* FITTING, this utilty is part of APMO code.  *\n");
  printf("* Written by Sergio A. Gonzalez UNAL-GQT/2010 *\n");
  printf("***********************************************\n");
  printf("\n");
  
  printf("CONTROL PARAMETERS: \n");
  printf("==================\n");
  printf("\n");
  printf("Num. of gaussian comps. (n):   %i\n",numOfComponents);
  printf("Gaussian error:                %f\n",gaussianError);
  printf("Absolute error:                %f\n",absoluteError);
  printf("Relative error:                %f\n",relativeError);
  printf("Maximum number of iteration:   %i\n",maximumIteration);
  printf("Optimize gaussian centers:     %i\n",optimizeCenter);


  /*******************************************
  Obtiene los datosque deben ser ajustados 
  ********************************************/
  InputData_get(fileName);
  fittingFunc=buildGaussArray(numOfComponents);
  
  
  /***********************************************
       AJUSTE DEL MINIMIZADOR
  ***********************************************/
  const size_t n = numOfInputData; 
  const size_t p = numOfParams; 
  const gsl_multifit_fdfsolver_type *T;
  gsl_multifit_fdfsolver *s;
  unsigned int  iter = 0;
  int status;
  gsl_vector_view xv = gsl_vector_view_array (x_init, p);
  gsl_multifit_function_fdf f; 
  
  f.f = &fittingFunction;
  f.df = &fittingFunction_df; 
  f.fdf = &fittingFunction_fdf;
  f.n = n;
  f.p = p;
  f.params = &InputData;
  
  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T, n, p);
  gsl_multifit_fdfsolver_set(s, &f, &xv.vector);


  /***********************************************
       INICIA PROCESO DE AJUSTE
   ***********************************************/
  
  if(printCycles ) print_state (iter, s);
  
  do{
    iter++;
    status = gsl_multifit_fdfsolver_iterate (s);
    

    
    if( printCycles ){
      printf ("status = %s\n", gsl_strerror (status));
      print_state (iter, s);
    };
    
    if (status)  break;
    
    status = gsl_multifit_test_delta (s->dx, s->x,
				      absoluteError,relativeError);
  }
  while (status == GSL_CONTINUE && iter < maximumIteration );
  

  /***********************************************
     MUESTRA ESTADO FINAL DEL PROCESO DE AJUSTE
   ***********************************************/
  print_finalState(s,outputFileName,status,iter);

    gsl_multifit_fdfsolver_free (s);
  return 0;


}
