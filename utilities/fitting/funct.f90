program fucnt
  implicit none
  integer :: j
  
  do j=-200,200,1
     print *,j*0.1,getPot(j*0.1_8),getWF(j*0.1_8)*0.01-4.5
  end do

contains

  function getPot(r) result(out)
    implicit none
    real(8) :: exps(3),facts(3), centers(3), out,r
    integer :: i
    
    ! exps=[0.88286,2.04211,0.00]*(0.52917724924_8**2)
    ! facts=[3.25482e-01,1.83062e-01,-2.27384e+03]
    ! centers=[3.26827,3.07380,0.0]/0.52917724924_8

    exps=[0.002794,0.01023,0.08386]*(0.52917724924_8**2)
    facts=[-1.56824,-1.493112,-1.682048]
    centers=[0.00,0.00,0.0]
    
    out=0.0
    do i=1,3
       out=out+(facts(i)*exp(-exps(i) *( ( r-centers(i) )**2 ) ))
    end do
    
  end function getPot


  function getWF(r) result(out)
    implicit none
    real(8) :: exps(2),facts(2), centers(2), out,r
    integer :: i
    
    ! exps=[0.88286,2.04211,0.00]*(0.52917724924_8**2)
    ! facts=[3.25482e-01,1.83062e-01,-2.27384e+03]
    ! centers=[3.26827,3.07380,0.0]/0.52917724924_8

    exps=[0.787441,0.216920]
    facts=[29.292661,20.175831]
    centers=[0.00,0.00]
    
    out=0.0
    do i=1,2
       out=out+(facts(i)*exp(-exps(i) *( ( r-centers(i) )**2 ) ))
    end do
    
  end function getWF
  

end program fucnt



