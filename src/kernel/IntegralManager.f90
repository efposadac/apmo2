!!**********************************************************************************!!
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                        !!
!!    http://www.unal.edu.co                                                        !!
!!                                                                                  !!
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>                !!
!!    Keywords:                                                                     !!
!!                                                                                  !!
!!    thisPointer files is part of nonBOA                                           !!
!!                                                                                  !!
!!    thisPointer program is free software; you can redistribute it and/or modify   !!
!!    it under the terms of the GNU General Public License as published by          !!
!!    the Free Software Foundation; either version 2 of the License, or             !!
!!    (at your option) any later version.                                           !!
!!                                                                                  !!
!!    thisPointer program is distributed in the hope that it will be useful,        !!
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of                !!
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 !!
!!    GNU General Public License for more details.                                  !!
!!                                                                                  !!
!!    You should have received a copy of the GNU General Public License             !!
!!    along with thisPointer program. If not, write to the Free Software Foundation !!
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               !!
!!                                                                                  !!
!!**********************************************************************************!!

!**
! @brief Modulo para definicion de datos de elemetos atomicos.
! 
!  Este modulo define una seudoclase para manejo de datos atomicos, correspondientes
!  a elementos atomicos.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see XMLParser_
!
!**
module IntegralManager_
	use ExternalPotential_
	use ParticleManager_
	use LibintInterface_
	use LibintInterface2_
	use IndexMap_
	use Exception_
	use Math_
	use Matrix_
	implicit none
	
	!! IntegralsDestiny {
	integer, parameter :: DISK					 =  0
	integer, parameter :: MEMORY				 =  1
	!! }
	
	!! IntegralManager_instance {
	integer, parameter :: NONE_INTEGRALS		 = -1
	integer, parameter :: OVERLAP_INTEGRALS		 =  1
	integer, parameter :: KINETIC_INTEGRALS		 =  2
	integer, parameter :: ATTRACTION_INTEGRALS	 =  3
	integer, parameter :: MOMENT_INTEGRALS		 =  4
	integer, parameter :: MOMENTUM_INTEGRALS	 =  5
	integer, parameter :: REPULSION_INTEGRALS	 =  6
	
	integer, parameter :: OVERLAP_DERIVATIVES	 =  7
	integer, parameter :: KINETIC_DERIVATIVES	 =  8
	integer, parameter :: ATTRACTION_DERIVATIVES =  9
	integer, parameter :: MOMENT_DERIVATIVES	 =  10
	integer, parameter :: MOMENTUM_DERIVATIVES	 =  11
	integer, parameter :: REPULSION_DERIVATIVES	 =  12
	!! }
	
	
	type , public :: IntegralManager
		character(30) :: name
		integer ::ttype 
		character(255) :: prefix 
		logical :: isOpen
		logical :: isComplete
		
!!		binfstream file
!! 		integer beginPos
!! 		integer currentPos
!! 		integer endPos 
		
		type(Matrix), allocatable :: integralsOfIndependientSpecie(:,:)
		type(Matrix), allocatable :: integralsInterSpecie(:)
		
	end type IntegralManager

	type(IntegralManager), target, public :: IntegralManager_instance
	!! }
	
	public :: &
		IntegralManager_constructor, &
		IntegralManager_destructor, &
		IntegralManager_getVectorElement, &
		IntegralManager_getElement, &
		IntegralManager_getMatrixElement, &
		IntegralManager_getMatrix, &
		IntegralManager_getMatrixPtr, &
		IntegralManager_getMatrixValuesPtr, &
		IntegralManager_getTensorR4Element, &
		IntegralManager_getLabels, &
		IntegralManager_getInterspecieOverlapIntegral,&
		IntegralManager_getInterspeciesOverlapMatrix,&
		IntegralManager_getInteractionBetweenQuantumClassicMtx, &
		IntegralManager_getIntraspecieRepulsionIntegral, &
		IntegralManager_getIntraspecieRepulsionDerivative, &
		IntegralManager_getInterspecieRepulsionIntegral, &
		IntegralManager_getMolecularIntraspecieRepulsionIntegral, &
		IntegralManager_getMolecularInterspecieRepulsionIntegral, &
		IntegralManager_getInteractionWithPotentialMatrix, &
		IntegralManager_buildMatrix, &
		IntegralManager_calculateInteractionMatrix,&
		IntegralManager_reset, &
		IntegralManager_writeIntraspecieRepulsionIntegral, &
		IntegralManager_writeInterspecieRepulsionIntegral


	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine IntegralManager_constructor()
		implicit none
		type(IntegralManager) :: this
				
		integer :: numberOfQuantumSpecies
				
		this%name = "undefined"
		this%ttype = NONE_INTEGRALS
		this%prefix = "undefined"
		this%isOpen = .false.
		

		numberOfQuantumSpecies = ParticleManager_getNumberOfQuantumSpecies()
		allocate(  IntegralManager_instance%integralsOfIndependientSpecie( numberOfQuantumSpecies, 12 ) )
		allocate(  IntegralManager_instance%integralsInterSpecie( numberOfQuantumSpecies * ( numberOfQuantumSpecies-1 ) / 2 ) )

		
	end subroutine IntegralManager_constructor

	
	!**
	! Define el destructor para clase
	!
	! @param thisPointer Funcion base
	!**
	subroutine IntegralManager_destructor()
		implicit none
		integer :: errorNum

		errorNum = system("rm "// trim(APMO_instance%INPUT_FILE)//"*.ints ")

	end subroutine IntegralManager_destructor
	
	!**
	! @brief Retorna valor de integrales
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un vector
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param i Posicion del elemento dentro del vector
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getVectorElement( thisID, i, nameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		integer :: i
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		real(8) :: output
		
		type(Exception) :: ex
		integer :: specieID
		integer(8) :: numberOfContractions
		integer :: aux(2)
		integer :: j
		character(30) :: nameOfSpecieSelected
		real(8) :: auxCharge
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= nameOfSpecie
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
		
		aux = IndexMap_vectorToMatrix( int(i, 8), int(numberOfContractions, 8) )
		
			
		if ( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie(specieID,thisID)%values ) ) then
			
			call Matrix_constructor( IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID ), &
				 numberOfContractions, numberOfContractions, Math_NaN )
				
		end if
	
		if ( .not.isNaN(IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( aux(1), aux(2) ) ) ) then
			
			output = IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( aux(1), aux(2) )
			
		else

			select case (thisID)
			
!				case(OVERLAP_INTEGRALS)
!
!					output = ContractedGaussian_overlapIntegral( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ) )
!
!				case(KINETIC_INTEGRALS)
!
!					output = ContractedGaussian_kineticIntegral( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ) ) / &
!						ParticleManager_getMass( specieID )
!
!				case(ATTRACTION_INTEGRALS)
!
!					output = 0.0_8
!
!					auxCharge = ParticleManager_getCharge( specieID )
!
!					do j = 1 , ParticleManager_getNumberOfPuntualParticles()
!
!						output = output + ContractedGaussian_attractionIntegral( &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ), &
!								ParticleManager_getOriginOfPuntualParticle( j ) ) * &
!								auxCharge * &
!								ParticleManager_getChargeOfPuntualParticle( j )
!
!					end do
!
!				case(MOMENT_INTEGRALS)
!
!					j=1
!					if ( present(component) ) j = component
!
!					output = ContractedGaussian_momentIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ), &
!							[0.0_8, 0.0_8, 0.0_8], cshift( [0_8, 0_8, 1_8], 3 - j ) )
!
!				case(MOMENTUM_INTEGRALS)
!
!					output = ContractedGaussian_momentumIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ) )
!
!				case(REPULSION_INTEGRALS)
!
!					output = 0.0_8
!
!					call Exception_constructor( ex , WARNING )
!					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getMatrixElement(i,j) function" )
!					call Exception_setDescription( ex, "In this case use the getTensor function, returning zero value" )
!					call Exception_show( ex )
!
!					return
!
!				case( OVERLAP_DERIVATIVES )
!
!					output = ContractedGaussian_overlapDerivative( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ), nuclei, component )
!
!				case(KINETIC_DERIVATIVES)
!
!					output = ContractedGaussian_kineticDerivative( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ), nuclei, component ) / &
!						ParticleManager_getMass( specieID )
!
!				case(ATTRACTION_DERIVATIVES)
!
!					output = 0.0_8
!
!					auxCharge = ParticleManager_getCharge( specieID )
!
!					do j=1, ParticleManager_getNumberOfPuntualParticles()
!
!						output = output + ContractedGaussian_attractionDerivative( &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(1) ), &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=aux(2) ), &
!								ParticleManager_getOriginOfPuntualParticle( j ), nuclei, component, &
!								ParticleManager_getOwnerOfPuntualParticle( j ) ) * &
!								auxCharge * &
!								ParticleManager_getChargeOfPuntualParticle( j )
!
!
!					end do
					
				case(REPULSION_DERIVATIVES)
					
					output = 0.0_8
					
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getMatrixElement(i,j) function" )
					call Exception_setDescription( ex, "In this case use the getTensor function, returning zero value" )
					call Exception_show( ex )
					
					return
				
				case default
				
					output = 0.0_8
					
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object IntegralManager in the get(i) function" )
					call Exception_setDescription( ex, "This ID hasn't been defined, returning zero value" )
					call Exception_show( ex )
					
					return
							
			end select
			
			IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( aux(1), aux(2) )  = output
			IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( aux(2), aux(1) )  = output
			
		end if 
	
	end function IntegralManager_getVectorElement
	
	!**
	! @brief Retorna valor de integrales sin almacenar su valor en ningun arreglo
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	!**
	function IntegralManager_getElement( thisID, i, j,k, l, nameOfSpecie, otherNameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		integer :: i
		integer :: j
		integer, optional :: k
		integer, optional :: l
		character(*), optional :: nameOfSpecie
		character(*), optional :: otherNameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		real(8) :: output
		
		type(Exception) :: ex
		integer :: specieID
		integer :: otherSpecieID
		integer :: m
		character(30) :: nameOfSpecieSelected
		real(8) :: auxCharge
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= nameOfSpecie
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )

		
		select case (thisID)
		
!			case(OVERLAP_INTEGRALS)
!
!				output = ContractedGaussian_overlapIntegral( &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) )
!
!			case(KINETIC_INTEGRALS)
!
!				output = ContractedGaussian_kineticIntegral( &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) ) / &
!					ParticleManager_getMass( specieID )
!
!			case(ATTRACTION_INTEGRALS)
!
!				output = 0.0_8
!
!				auxCharge = ParticleManager_getCharge( specieID )
!
!				do m = 1 , ParticleManager_getNumberOfPuntualParticles()
!
!					output = output + ContractedGaussian_attractionIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!							ParticleManager_getOriginOfPuntualParticle( m ) ) * &
!							auxCharge * ParticleManager_getChargeOfPuntualParticle( m )
!
!				end do
!
!			case(MOMENT_INTEGRALS)
!
!				m=1
!				if ( present(component) ) m = component
!
!				output = ContractedGaussian_momentIntegral( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!						[0.0_8, 0.0_8, 0.0_8], cshift( [0_8, 0_8, 1_8], 3 - m ) )
!
!			case(MOMENTUM_INTEGRALS)
!
!				output = ContractedGaussian_momentumIntegral( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) )
!
!			case(REPULSION_INTEGRALS)
!
!				if ( present(k) .and. present(l) ) then
!
!					specieID = ParticleManager_getSpecieID( nameOfSpecie = trim( nameOfSpecie ) )
!					otherSpecieID = ParticleManager_getSpecieID( nameOfSpecie = trim( otherNameOfSpecie ) )
!
!					output = ContractedGaussian_repulsionIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j), &
!							ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=k), &
!							ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=l) )
!
!					output = output * ( ParticleManager_getCharge( specieID ) &
!								* ParticleManager_getCharge( otherSpecieID ) )
!
!				else
!
!					call Exception_constructor( ex , ERROR )
!					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getElement(i,j) function" )
!					call Exception_setDescription( ex, "" )
!					call Exception_show( ex )
!
!				end if
!
!			case( OVERLAP_DERIVATIVES )
!
!				output = ContractedGaussian_overlapDerivative( &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction = i ), &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction = j ), nuclei, component )
!
!			case(KINETIC_DERIVATIVES)
!
!				output = ContractedGaussian_kineticDerivative( &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), nuclei, component ) / &
!					ParticleManager_getMass( specieID )
!
!			case(ATTRACTION_DERIVATIVES)
!
!				output = 0.0_8
!
!				auxCharge = ParticleManager_getCharge( specieID )
!
!				do m=1, ParticleManager_getNumberOfPuntualParticles()
!
!					output = output + ContractedGaussian_attractionDerivative( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!							ParticleManager_getOriginOfPuntualParticle( m ), nuclei, component, &
!							ParticleManager_getOwnerOfPuntualParticle( m ) ) * &
!							auxCharge * ParticleManager_getChargeOfPuntualParticle( m )
!				end do
!
!			case(REPULSION_DERIVATIVES)
!
!				if ( present(k) .and. present(l) ) then
!
!					specieID = ParticleManager_getSpecieID( nameOfSpecie = trim( nameOfSpecie ) )
!
!					otherSpecieID = ParticleManager_getSpecieID( nameOfSpecie = trim( otherNameOfSpecie ) )
!
!					output = ContractedGaussian_repulsionDerivative( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j), &
!							ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=k), &
!							ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=l), &
!							nuclei, component )
!
!					output = output * ( ParticleManager_getCharge( specieID ) &
!								* ParticleManager_getCharge( specieID ) )
!
!				else
!
!					call Exception_constructor( ex , ERROR )
!					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getElement(i,j) function" )
!					call Exception_setDescription( ex, "" )
!					call Exception_show( ex )
!
!				end if
			
			case default
			
				output = 0.0_8
				
				call Exception_constructor( ex , WARNING )
				call Exception_setDebugDescription( ex, "Class object IntegralManager in the get(i) function" )
				call Exception_setDescription( ex, "This ID hasn't been defined, returning zero value" )
				call Exception_show( ex )
				
				return
						
		end select
	
	
	end function IntegralManager_getElement

	
	!**
	! @brief Retorna valor de integrales
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como una matriz
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param i Posicion en las filas del elemento dentro de la matriz
	! @param j Posicion en las columnas del elemento dentro de la matriz
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getMatrixElement( thisID, i, j, nameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		integer :: i
		integer :: j
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		real(8) :: output
		
		
		type(Exception) :: ex
		integer :: specieID
		integer :: k
		integer(8) :: numberOfContractions
		integer :: aux
		real :: auxCharge
		character(30) :: nameOfSpecieSelected


		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= nameOfSpecie

		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie(specieID,thisID)%values ) ) then
			
			numberOfContractions = ParticleManager_getNumberOfContractions( specieID )

			call Matrix_constructor( IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID ), &
				 numberOfContractions, numberOfContractions, Math_NaN )
				
		end if

		
		if ( .not.isNaN(IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( i, j ) ) ) then
			
			output = IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( i, j )
						
		else

			select case (thisID)
			
!				case(OVERLAP_INTEGRALS)
!
! 					output = ContractedGaussian_overlapIntegral( &
! 						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
! 						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) )
!
!				case(KINETIC_INTEGRALS)
!
!					output = ContractedGaussian_kineticIntegral( &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) ) / &
!						ParticleManager_getMass( specieID )
!
!
!				case(ATTRACTION_INTEGRALS)
!
!					output = 0.0_8
!
!					auxCharge = ParticleManager_getCharge( specieID )
!
!					do k=1, ParticleManager_getNumberOfPuntualParticles()
!
!						output = output + ContractedGaussian_attractionIntegral( &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!								ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!								ParticleManager_getOriginOfPuntualParticle( k ) ) * &
!								auxCharge * &
!								ParticleManager_getChargeOfPuntualParticle( k )
!
!					end do
!
!				case(MOMENT_INTEGRALS)
!
!					aux=1
!					if ( present(component) ) aux = component
!
!					output = ContractedGaussian_momentIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!							[0.0_8, 0.0_8, 0.0_8], cshift( [0_8, 0_8, 1_8], 3-aux) )
!
!				case(MOMENTUM_INTEGRALS)
!
!					output = ContractedGaussian_momentumIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ) )

				case(REPULSION_INTEGRALS)
					
					output = 0.0_8
					
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getMatrixElement(i,j) function" )
					call Exception_setDescription( ex, "In this case use the getTensor function , returning zero value" )
					call Exception_show( ex )
					
					return
				
				case( OVERLAP_DERIVATIVES )
					
					output = ContractedGaussian_overlapDerivative( &
						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), & 
						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), nuclei, component )
						
				case(KINETIC_DERIVATIVES)
				
					output = ContractedGaussian_kineticDerivative( &
						ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), & 
						ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), nuclei, component ) / &
						ParticleManager_getMass( specieID )
						
				case(ATTRACTION_DERIVATIVES)
					
					auxCharge = ParticleManager_getCharge( specieID )
					
					do k=1, ParticleManager_getNumberOfPuntualParticles()
						
						output = 0.0_8
						
						output = output + ContractedGaussian_attractionDerivative( &
							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), & 
							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
							ParticleManager_getOriginOfPuntualParticle( k ), nuclei, component, &
							ParticleManager_getOwnerOfPuntualParticle( k ) ) * &
							auxCharge * &
							ParticleManager_getChargeOfPuntualParticle( k )
						
					end do
						
				case(REPULSION_DERIVATIVES)
					
					output = 0.0_8
					
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object IntegralManager in the getMatrixElement(i,j) function" )
					call Exception_setDescription( ex, "In this case use the getTensor function, returning zero value" )
					call Exception_show( ex )
					
					return
				
				case default
				
					output = 0.0_8
					
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object IntegralManager in the get(i) function" )
					call Exception_setDescription( ex, "This ID hasn't been defined, returning zero value" )
					call Exception_show( ex )
					
					return
							
			end select
	
			IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( i, j )  = output
			IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values( j, i )  = output
	
		end if 

	end function IntegralManager_getMatrixElement
	
	
	!**
	! @brief Contruye una matriz de integrales
	! Contruye una matriz de integrales especificadas y la almacena en el administrador de integrales
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	subroutine IntegralManager_buildMatrix( thisID, nameOfSpecie, nuclei, component  ) 
		implicit none
		integer :: thisID
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component

		type(Exception) :: ex
		type(puntualParticle), allocatable :: particles(:)

		integer :: specieID
		integer :: numCartesianOrbitalI
		integer :: numCartesianOrbitalJ
		integer :: i, j, k, l
		integer :: m, n, u
		integer :: puntual
		integer :: aux

		integer, allocatable :: labels(:)

		integer(8) :: a, b
		integer(8) :: totalNumberOfContractions
		integer :: numberOfContractions
		integer :: numberOfIntegrals

		real(8), allocatable :: integralValue(:)

		real(8) :: origin(3)
		real(8) :: auxValue
		real(8) :: auxCharge
		real(8), allocatable :: buffer(:)

		character(30) :: nameOfSpecieSelected

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim(nameOfSpecie)

		specieID= ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )

		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
		totalNumberOfContractions = ParticleManager_getTotalnumberOfContractions( specieID )
		
		if ( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie(specieID,thisID)%values ) ) then
			
				call Matrix_constructor( IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID ), &
					 totalNumberOfContractions, totalNumberOfContractions, Math_NaN )
				
		end if

		if(allocated(labels)) deallocate(labels)
		allocate(labels(numberOfContractions))

		labels = IntegralManager_getLabels(specieID, numberOfContractions)

		numberOfIntegrals = (totalNumberOfContractions * (totalNumberOfContractions + 1)) / 2

		if(allocated(buffer)) deallocate(buffer)
		allocate(buffer(numberOfIntegrals))

		select case (thisID)
		
			case(OVERLAP_INTEGRALS)

				do i=1, numberOfContractions
					numCartesianOrbitalI = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ))
					do j=i, numberOfContractions
						numCartesianOrbitalJ = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ))

						if(allocated(integralValue)) deallocate(integralValue)
						allocate(integralValue(numCartesianOrbitalI * numCartesianOrbitalJ))

						 call ContractedGaussian_overlapIntegral( ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
											ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), integralValue)


						m = 0
						do k = labels(i), labels(i) + (numCartesianOrbitalI - 1)
							do l = labels(j), labels(j) + (numCartesianOrbitalJ - 1)
								m = m + 1

								buffer(IndexMap_transformIndexPair(int(k, 8), int(l, 8), totalNumberOfContractions)) = integralValue(m)

							end do
						end do

					end do
				end do

				m = 0
				do k = 1, totalNumberOfContractions
					do l = k, totalNumberOfContractions
						m = m + 1

						IntegralManager_instance%integralsOfIndependientSpecie( specieID, OVERLAP_INTEGRALS )%values( k, l ) = buffer(m)

						IntegralManager_instance%integralsOfIndependientSpecie( specieID, OVERLAP_INTEGRALS )%values( l, k ) = &
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, OVERLAP_INTEGRALS )%values( k, l )

					end do
				end do

			case(KINETIC_INTEGRALS)
				
				do i=1, numberOfContractions
					numCartesianOrbitalI = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ))
					do j=i, numberOfContractions
						numCartesianOrbitalJ = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ))

						if(allocated(integralValue)) deallocate(integralValue)
						allocate(integralValue(numCartesianOrbitalI * numCartesianOrbitalJ))

						call ContractedGaussian_kineticIntegral( ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
											ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), integralValue)


						m = 0
						do k = labels(i), labels(i) + (numCartesianOrbitalI - 1)
							do l = labels(j), labels(j) + (numCartesianOrbitalJ - 1)
								m = m + 1

								buffer(IndexMap_transformIndexPair(int(k, 8), int(l, 8), totalNumberOfContractions)) = integralValue(m)

							end do
						end do

					end do
				end do

				m = 0
				do k = 1, totalNumberOfContractions
					do l = k, totalNumberOfContractions
						m = m + 1
						
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, KINETIC_INTEGRALS )%values( k, l ) = buffer(m)
						
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, KINETIC_INTEGRALS )%values( l, k ) = &
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, KINETIC_INTEGRALS )%values( k, l )
						
					end do
				end do
				

				IntegralManager_instance%integralsOfIndependientSpecie( specieID, KINETIC_INTEGRALS )%values = &
					IntegralManager_instance%integralsOfIndependientSpecie( specieID, KINETIC_INTEGRALS )%values / &
					ParticleManager_getMass( specieID )
					
			case(ATTRACTION_INTEGRALS)

				puntual = ParticleManager_getNumberOfPuntualParticles()

				if(allocated(particles)) deallocate(particles)
				allocate(particles(0:puntual))

				do i = 0, puntual - 1
					particles(i)%charge = ParticleManager_getChargeOfPuntualParticle( i + 1 )
					origin = ParticleManager_getOriginOfPuntualParticle( i + 1 )
					particles(i)%x = origin(1)
					particles(i)%y = origin(2)
					particles(i)%z = origin(3)
				end do

				do i=1, numberOfContractions
					numCartesianOrbitalI = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ))
					do j=i, numberOfContractions
						numCartesianOrbitalJ = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ))

						if(allocated(integralValue)) deallocate(integralValue)
						allocate(integralValue(numCartesianOrbitalI * numCartesianOrbitalJ))

						call ContractedGaussian_attractionIntegral( ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
												ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
												particles, puntual, integralValue)
						m = 0
						do k = labels(i), labels(i) + (numCartesianOrbitalI - 1)
							do l = labels(j), labels(j) + (numCartesianOrbitalJ - 1)
								m = m + 1

								buffer(IndexMap_transformIndexPair(int(k, 8), int(l, 8), totalNumberOfContractions)) = integralValue(m)

							end do
						end do

					end do
				end do

				m = 0
				do k = 1, totalNumberOfContractions
					do l = k, totalNumberOfContractions
						m = m + 1
						
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, ATTRACTION_INTEGRALS )%values( k, l ) = buffer(m)
						
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, ATTRACTION_INTEGRALS )%values( l, k ) = &
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, ATTRACTION_INTEGRALS )%values( k, l )
						
					end do
				end do
					
				auxCharge = ParticleManager_getCharge( specieID )
				
				IntegralManager_instance%integralsOfIndependientSpecie( specieID, ATTRACTION_INTEGRALS)%values = &
				IntegralManager_instance%integralsOfIndependientSpecie( specieID, ATTRACTION_INTEGRALS)%values * -auxCharge

			case(MOMENT_INTEGRALS)

				IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENT_INTEGRALS)%values=0.0_8
				aux=1
				if ( present(component) ) aux = component
			
				a = 1
				do i=1, numberOfContractions
					numCartesianOrbitalI = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ))
					b = a
					do j=i, numberOfContractions
						numCartesianOrbitalJ = ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ))

						if(allocated(integralValue)) deallocate(integralValue)
						allocate(integralValue(numCartesianOrbitalI * numCartesianOrbitalJ))

						integralValue = ContractedGaussian_momentIntegral( &
									ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
									ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
									[0.0_8, 0.0_8, 0.0_8], aux)

						m = 0
						do k = a, numCartesianOrbitalI + a -1
							do l = b, numCartesianOrbitalJ + b -1
								m = m + 1

								IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENT_INTEGRALS )%values( k, l ) = integralValue(m)

								IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENT_INTEGRALS )%values( l, k ) = &
								IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENT_INTEGRALS )%values( k, l )

							end do
						end do

						b = b + numCartesianOrbitalJ
					end do
					a = a + numCartesianOrbitalI
				end do

			case(MOMENTUM_INTEGRALS)
				
				do i=1, numberOfContractions
					do j=i, numberOfContractions
					
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENTUM_INTEGRALS )%values( i, j ) = &
						ContractedGaussian_momentumIntegral( &
							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), & 
							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j )  &
						)
						
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENTUM_INTEGRALS )%values( j, i ) = &
						IntegralManager_instance%integralsOfIndependientSpecie( specieID, MOMENTUM_DERIVATIVES )%values( i, j )
						
					end do
				end do

			stop "momentum integrals"
			case(REPULSION_INTEGRALS)

				n = 1
				do i = 1 ,  numberOfContractions
					n = i
					do j=i, numberOfContractions
						u = j
						do k = n ,  numberOfContractions
							do l = u,  numberOfContractions
										
								auxValue=IntegralManager_getIntraspecieRepulsionIntegral( i, j, k, l, specieID,&
									 int(numberOfContractions ) )
									
							end do
							u=k+1
						end do
					end do
				end do
		
				stop "repulsion integrals"

			case default
				
				call Exception_constructor( ex , WARNING )
				call Exception_setDebugDescription( ex, "Class object IntegralManager in buildMatrix function" )
				call Exception_setDescription( ex, "In this case use the element functions" )
				call Exception_show( ex )
			
		end select
	
	end subroutine IntegralManager_buildMatrix
	
	
	!>
	!! @brief Retorna el valor de  la integral de overlap entre gausianas contraidas para dos  especies diferentes
	!!
	!! @param i Posicion en la dimension 1 elemento dentro de la matriz, esta dimension se asocia a una especie.
	!! @param j Posicion en la dimension 2 elemento dentro de la matriz  esta dimension de asocia a otra la especie.
	!! @param specieID  Numero de identificacion asociado a una de las especies.
	!! @param specieID  Numero de identificacion asociado a la otra de las especies.
	!!
	!! 
	!<
	function IntegralManager_getInterspecieOverlapIntegral(  i, j, specieID, otherSpecieID ) result ( output )
		implicit none
		integer :: thisID
		integer :: i
		integer :: j
		integer :: specieID
		integer :: otherSpecieID
		real(8), allocatable :: output(:)

!		output = ContractedGaussian_overlapIntegral( &
!				ParticleManager_getContractionPtr( specieID,  numberOfContraction=i), &
!				ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=j) )

	end function IntegralManager_getInterspecieOverlapIntegral
	

	!! @param specieID  Numero de identificacion asociado a una de las especies.
	!! @param specieID  Numero de identificacion asociado a la otra de las especies.
	!! 
	!<
	function IntegralManager_getInterspeciesOverlapMatrix( specieID, otherSpecieID ) result ( output )
		implicit none
		integer :: specieID
		integer :: otherSpecieID
		type(Matrix) :: output

		integer :: i
		integer :: j
		integer :: numberOfContractions
		integer :: otherNumberOfContractions

		numberOfContractions = ParticleManager_getNumberOfContractions( specieID ) 
		otherNumberOfContractions = ParticleManager_getNumberOfContractions( otherSpecieID ) 

		call Matrix_constructor( output, int(numberOfContractions,8), int(otherNumberOfContractions,8), 0.0_8 )



		do  i=1, numberOfContractions
			do j=1, otherNumberOfContractions
				
!				output%values(i,j) = ContractedGaussian_overlapIntegral( &
!					ParticleManager_getContractionPtr( specieID,  numberOfContraction=i), &
!					ParticleManager_getContractionPtr( otherSpecieID,  numberOfContraction=j) )


			end do
		end do
	

	end function IntegralManager_getInterspeciesOverlapMatrix



	!>
	!! @brief Retorna la matriz de interaccion entre una especie cuantica y un nucleo clasico espcificado.
	!!
	!! @param specieID  Numero de identificacion asociado a la especie cunatica.
	!<
	function IntegralManager_getInteractionBetweenQuantumClassicMtx( specieID, classicNucleousID ) result ( output )
	       implicit none
	       integer :: specieID
	       integer :: classicNucleousID
	       type(Matrix) :: output

	       integer :: i
	       integer :: j
	       integer :: numberOfContractions
	       real(8) :: auxCharge
	       real(8) :: auxValue
	       
	       numberOfContractions = ParticleManager_getNumberOfContractions( specieID ) 
	       auxCharge = ParticleManager_getCharge( specieID )
	       print *,"Carga:",ParticleManager_getOriginOfPuntualParticle( classicNucleousID ) 
	       print *,"Origen:",ParticleManager_getChargeOfPuntualParticle( classicNucleousID )
			 
	       call Matrix_constructor( output, int(numberOfContractions,8), int(numberOfContractions,8), 0.0_8 )


!	       do i=1, numberOfContractions
!		    do j=i, numberOfContractions
!
!			 auxValue = ContractedGaussian_attractionIntegral( &
!			 ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!			 ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!			 ParticleManager_getOriginOfPuntualParticle( classicNucleousID ) ) * &
!			 ParticleManager_getChargeOfPuntualParticle( classicNucleousID )
!
!			 output%values( j, i ) = auxValue
!			 output%values( i, j ) = auxValue
!
!		    end do
!	       end do
				
	       output%values = output%values * auxCharge

        end function IntegralManager_getInteractionBetweenQuantumClassicMtx


	!! Debe Arreglarse para evitar acceso directo a particleManager_instance
	subroutine IntegralManager_calculateInteractionMatrix( nameOfSpecie, interactionMatrix ) 
		implicit none
		character(*), optional :: nameOfSpecie
		real(8), allocatable :: interactionMatrix(:,:)
	
		integer :: specieID
		integer :: i
		integer :: j
		integer :: k
		integer(8) :: numberOfContractions
		real(8) :: auxValue
		real(8) :: auxCharge
		character(30) :: nameOfSpecieSelected

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim(nameOfSpecie)

		specieID= ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )

		auxCharge = ParticleManager_getCharge( specieID )
		
		allocate( interactionMatrix( numberOfContractions, numberOfContractions ) )
		interactionMatrix = 0.0
		
!		do i=1, numberOfContractions
!			do j=i, numberOfContractions
!
!
!				auxValue = 0.0_8
!				do k=1, size(ParticleManager_instance%particlesPtr)
!
!					if ( .not.ParticleManager_instance%particlesPtr(k)%isQuantum ) then
!
!
!						auxValue = auxValue + ContractedGaussian_attractionIntegral( &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), &
!							ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), &
!							ParticleManager_instance%particlesPtr(k)%origin ) * &
!							ParticleManager_instance%particlesPtr(k)%charge
!
!					end if
!
!				end do
!
!				interactionMatrix( j, i ) = auxValue
!				interactionMatrix( i, j ) = auxValue
!
!			end do
!		end do

		interactionMatrix=interactionMatrix*auxCharge
	
	end subroutine IntegralManager_calculateInteractionMatrix
	
	!**
	! @brief Retorna arreglo de integrales
	! Retorna El arreglo de datos para la integral especificada, interpretandolos como una matriz. 
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getMatrix( thisID, nameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		type(Matrix) :: output

		integer :: specieID
		character(30) :: nameOfSpecieSelected
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		
		if( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values) ) then
			
			call IntegralManager_buildMatrix( thisID, nameOfSpecieSelected, nuclei, component )
	
		end if
		
		call Matrix_copyConstructor(output, IntegralManager_instance%integralsOfIndependientSpecie(specieID, thisID) )
	
	end function IntegralManager_getMatrix
	
	!**
	! @brief Retorna un opuntero a la matriz de integrales
	! Retorna un puntero a la matrizs, interpretandolos como una matriz. 
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getMatrixPtr( thisID, nameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		type(Matrix), pointer :: output

		integer :: specieID
		character(30) :: nameOfSpecieSelected
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim(nameOfSpecie)
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values) ) then
			
			call IntegralManager_buildMatrix( thisID, nameOfSpecie, nuclei, component )
	
		end if
		
		output => null()
		output => IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )
		
	end function IntegralManager_getMatrixPtr

	function IntegralManager_getInterSpecieRepulsionMatrixPtr( specieID, otherSpecieID,&
		numberOfContractions, otherNumberOfContractions  ) result ( output )
		implicit none
		integer :: specieID
		integer :: otherSpecieID
		integer :: numberOfContractions
		integer :: otherNumberOfContractions
		type(Matrix), pointer :: output

		integer(8) :: auxIndex
		integer(8) :: numberOfIntegrals
		integer :: smallerID
		integer :: numberOfQuantumSpecies
		integer :: matrixNumber

		numberOfQuantumSpecies = ParticleManager_getNumberOfQuantumSpecies()
		
		if ( specieID < otherSpecieID ) then 
			smallerID = specieID
		else
			smallerID = otherSpecieID
		end if
		
		if ( .not.allocated(IntegralManager_instance%integralsInterSpecie ) ) then
		
			allocate(  IntegralManager_instance%integralsInterSpecie( numberOfQuantumSpecies * ( numberOfQuantumSpecies-1 ) / 2) )
			
		end if
		
		!! Determina el indice de la matrix de interaccion inter-especie
		matrixNumber= ( smallerID - 1_8 ) * ( numberOfQuantumSpecies - smallerID / 2.0_8 ) + abs( specieID - otherSpecieID )
		
		if (  .not.allocated(IntegralManager_instance%integralsInterSpecie( matrixNumber )%values ) ) then
				
			!! Calcula el numero de integrales que se almacenaran dentro de la matriz dada 
			numberOfIntegrals = ( numberOfContractions    *  ( ( numberOfContractions + 1.0_8) / 2.0_8 ) ) * &
					( otherNumberOfContractions * ( ( otherNumberOfContractions + 1.0_8 ) / 2.0_8 ) )
						
			call Matrix_constructor( IntegralManager_instance%integralsInterSpecie( matrixNumber) , &
				 numberOfIntegrals, 1_8, Math_NaN )
		end if		
		 
		output => null()
		output => IntegralManager_instance%integralsInterSpecie( matrixNumber)
		
	end function IntegralManager_getInterSpecieRepulsionMatrixPtr

	!**
	! @brief Retorna arreglo de integrales
	! Retorna un puntero al arreglo de datos, interpretandolos como una matriz. 
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: OVERLAP_INTEGRALS,
	!               KINETIC_INTEGRALS, ATTRACTION_INTEGRALS, MOMENT_INTEGRALS, MOMENTUM_INTEGRALS,
	!               OVERLAP_DERIVATIVES, KINETIC_DERIVATIVES, ATTRACTION_DERIVATIVES
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getMatrixValuesPtr( thisID, nameOfSpecie, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		character(*), optional :: nameOfSpecie
		integer, optional :: nuclei
		integer, optional :: component
		real(8), pointer :: output(:,:)

		integer :: specieID
		character(30) :: nameOfSpecieSelected
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim(nameOfSpecie)
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values) ) then
			
			call IntegralManager_buildMatrix( thisID, nameOfSpecie, nuclei, component )
	
		end if
		
		output=> null()
		output => IntegralManager_instance%integralsOfIndependientSpecie( specieID, thisID )%values
		
	end function IntegralManager_getMatrixValuesPtr
	
	
	!**
	! @brief Retorna valor de integrales
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: REPULSION_INTEGRALS
	!               REPULSION_DERIVATIVES
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!
	! @todo Falta implementacion completa
	!**
	function IntegralManager_getTensorR4Element( thisID, i, j, k, l, nameOfSpecieA, nameOfSpecieB, nuclei, component  ) result ( output )
		implicit none
		integer :: thisID
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		character(*), optional :: nameOfSpecieA
		character(*), optional :: nameOfSpecieB
		integer, optional :: nuclei
		integer, optional :: component
		real(8) :: output
		
		output =0.0_8

	end function IntegralManager_getTensorR4Element
	
	function IntegralManager_getLabels(specieID, numberOfContractions) result(labelsOfContractions)
		implicit none
		integer :: specieID
		integer :: numberOfContractions
		integer :: labelsOfContractions(numberOfContractions)

		integer :: auxLabelsOfContractions
		integer :: i

		auxLabelsOfContractions = 1

		do i = 1, numberOfContractions

			!!Posicion real de las contracciones con respecto al numero total de contracciones
			labelsOfContractions(i) = auxLabelsOfContractions

			auxLabelsOfContractions = auxLabelsOfContractions + ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(i)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
									i)%contractionIDInParticle)%numCartesianOrbital
		end do

	end function IntegralManager_getLabels
	!**
	! @brief Retorna valor de integrales intraespecies
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getIntraspecieRepulsionIntegral( i, j, k, l, specieID, numberOfContractions ) result ( output )
		implicit none
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: specieID
		integer :: numberOfContractions
		real(8) :: output

		integer(8) :: auxIndex
		integer(8) :: numberOfIntegrals


		auxIndex = IndexMap_tensorR4ToVector(i,j,k,l, numberOfContractions )
	
		if ( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie(specieID,REPULSION_INTEGRALS)%values ) ) then
		
			
			numberOfIntegrals = int( ( (  numberOfContractions * (  numberOfContractions + 1.0_8 ) / 4.0_8 ) * &
					( (  numberOfContractions * (  numberOfContractions + 1.0_8) / 2.0_8 ) + 1.0_8) ), 8 )
		
			call Matrix_constructor( IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_INTEGRALS ), &
					numberOfIntegrals, 1_8, Math_NaN )
			
		else if ( .not.isNaN( IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_INTEGRALS)%values( &
						auxIndex, 1 ) ) ) then
					
			output = IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_INTEGRALS )%values( auxIndex, 1 )
				
			return
				
		end if
			
!		output = ContractedGaussian_repulsionIntegral( &
!				ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%contractionIDInParticle), &
!				ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%contractionIDInParticle), &
!				ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(k)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(k)%contractionIDInParticle), &
!				ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(l)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(l)%contractionIDInParticle) )

		IntegralManager_instance%integralsOfIndependientSpecie( specieID,REPULSION_INTEGRALS )%values( auxIndex, 1 ) = output

	end function IntegralManager_getIntraspecieRepulsionIntegral
	

	!**
	! @brief Retorna valor de integrales intraespecies
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	subroutine IntegralManager_writeIntraspecieRepulsionIntegral( specieID, nameOfSpecie ) 
		implicit none
		integer :: specieID
		character(*) :: nameOfSpecie	

		integer :: totalNumberOfContractions
		integer :: numberOfContractions
		integer :: numberOfPrimitives
		integer :: maxAngularMoment
		integer :: i,a,b,n,r,s,u

		integer(8) :: sizeTotal

		integer, allocatable :: AOIndex(:)
		integer, allocatable :: labelsOfContractions(:)

		real(8), allocatable :: integralValue(:)
		real(8), allocatable :: buffer(:)
		
        
                call LibintInterface2_diskIntraSpecie(nameOfSpecie, specieID, "ERIS")

                
		! numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
! 		totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

! 		open(UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//".ints", &
! 				STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='BINARY')

! 		if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
! 		allocate(labelsOfContractions(numberOfContractions))

! 		labelsOfContractions = IntegralManager_getLabels(specieID, numberOfContractions)

! 		!! Se construye el objeto de libint (se inicializa la libreria y se calcula la memoria necesaria para el calculo)
! 		maxAngularMoment = ParticleManager_getMaxAngularMoment()
! 		numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

! 		if(.not. LibintInterface_isInstanced()) then
! 			call LibintInterface_constructor(LibintInterface_instance, maxAngularMoment, numberOfPrimitives, "ERIS")
! 			call LibintInterface_show(LibintInterface_instance)
! 		end if

! 		!!Calculo del tamano del buffer (alojamos integrales en memoria y luego a disco)
! 		sizeTotal = (totalNumberOfContractions * (totalNumberOfContractions + 1))/2
! 		sizeTotal = (sizeTotal * (sizeTotal + 1))/2

! 		if(allocated(buffer)) deallocate(buffer)
! 		allocate(buffer(sizeTotal))
! 		buffer = 0.0_8

! 		!! Calcula iterativamente integrales de Repulsion para cada shell
! 		n = 1
! 		do a = 1 ,  numberOfContractions
! 			n = a
! 			do b=a, numberOfContractions
! 				u = b
! 				do r = n , numberOfContractions
! 					do s = u,  numberOfContractions

! 						!!Inicializa datos globales
! 						call LibintInterface_initializeShell( &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(a)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
! 									a)%contractionIDInParticle)%primitives(1), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie( &
! 									specieID)%contractionID(b)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									b)%contractionIDInParticle)%primitives(1), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(r)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									r)%contractionIDInParticle)%primitives(1), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(s)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									s)%contractionIDInParticle)%primitives(1), labelsOfContractions(a), labelsOfContractions(b), &
! 									labelsOfContractions(r), labelsOfContractions(s), totalNumberOfContractions, AOIndex = AOIndex, isInterSpecies=.false.)

! 						!!Calcula integrales de repulsion... segun casos libint.a, libr12.a
! 						call ContractedGaussian_repulsionIntegral( &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(a)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
! 									a)%contractionIDInParticle), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie( &
! 									specieID)%contractionID(b)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									b)%contractionIDInParticle), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(r)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									r)%contractionIDInParticle), &
! 									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
! 									specieID)%contractionID(s)%particleID)%basis%contractions( &
! 									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID( &
! 									s)%contractionIDInParticle),  integralValue )

! 						!!Guarda integrales en buffer
! 						do i = 1, size(integralValue)
! 							buffer(AOIndex(i)) = integralValue(i)
! 						end do

! 					end do
! 					u=r+1
! 				end do
! 			end do
! 		end do


! 		!! Escribe integrales a disco
! 		n = 1
! 		i = 0
! 		do a = 1, totalNumberOfContractions
! 			n = a
! 			do b = a, totalNumberOfContractions
! 				u = b
! 				do r = n, totalNumberOfContractions
! 					do s = u, totalNumberOfContractions
! 						i = i + 1
! 						if ( abs(buffer(i)) > 1.0D-10 ) then
! 							write(34) a,b,r,s,buffer(i)
! !							print*,  a,b,r,s,buffer(i)
! 						end if
! 					end do
! 					u = r + 1
! 				end do
! 			end do
! 		end do

! 		write(34 ) -1,-1,-1,-1,0.0_8
! 		close( 34 )

! 		deallocate(buffer)

	end subroutine IntegralManager_writeIntraspecieRepulsionIntegral

	!**
	! @brief Retorna valor de integrales de repulsion interespecies
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: REPULSION_INTEGRALS
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	!**
	subroutine IntegralManager_writeInterspecieRepulsionIntegral( specieID, otherSpecieID,nameOfSpecie,otherNameOfSpecie  )
		implicit none
		integer :: specieID
		integer :: otherSpecieID
		character(*) :: nameOfSpecie	
		character(*) :: otherNameOfSpecie

		integer :: totalNumberOfContractions
		integer :: numberOfContractions
		integer :: numberOfPrimitives
		integer :: maxAngularMoment
		integer :: otherTotalNumberOfContractions
		integer :: otherNumberOfContractions
		integer :: i,j,k,l,m

		integer(8) :: sizeTotal

		integer, allocatable :: AOIndex(:)
		integer, allocatable :: labelsOfContractions(:)
		integer, allocatable :: otherLabelsOfContractions(:)

		real(8), allocatable :: integralValue(:)
		real(8), allocatable :: buffer(:)

		open(UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//"."//trim(otherNameOfSpecie)//".ints", &
				STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='BINARY')

		!! Specie ID
		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
		totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

		if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
		allocate(labelsOfContractions(numberOfContractions))

		labelsOfContractions = IntegralManager_getLabels(specieID, numberOfContractions)

		!! OtherSpecie ID
		otherNumberOfContractions = ParticleManager_getNumberOfContractions( otherSpecieID )
		otherTotalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( otherSpecieID )

		if (allocated(otherLabelsOfContractions)) deallocate(otherLabelsOfContractions)
		allocate(otherLabelsOfContractions(otherNumberOfContractions))

		otherLabelsOfContractions = IntegralManager_getLabels(otherSpecieID, otherNumberOfContractions)

		!! Calculo del tamano del buffer
		sizeTotal = (totalNumberOfContractions * (totalNumberOfContractions + 1))/2
		sizeTotal = sizeTotal * ((otherTotalNumberOfContractions * (otherTotalNumberOfContractions + 1))/2)

		!! Inicializa LIBINT
		maxAngularMoment = ParticleManager_getMaxAngularMoment()
		numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

		if(.not. LibintInterface_isInstanced()) then
			call LibintInterface_constructor(LibintInterface_instance, maxAngularMoment, numberOfPrimitives, "ERIS")
		end if

		if(allocated(buffer)) deallocate(buffer)
		allocate(buffer(sizeTotal))
		buffer = 0.0_8

		do i=1,numberOfContractions
			do j=i, numberOfContractions
				do k=1,otherNumberOfContractions
					do l=k,otherNumberOfContractions

						!!Inicializa datos globales
						call LibintInterface_initializeShell( &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(i)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%contractionIDInParticle)%primitives(1), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(j)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%contractionIDInParticle)%primitives(1), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									otherSpecieID)%contractionID(k)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(otherSpecieID)%contractionID( &
									k)%contractionIDInParticle)%primitives(1), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie( &
									otherSpecieID)%contractionID(l)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(otherSpecieID)%contractionID( &
									l)%contractionIDInParticle)%primitives(1), labelsOfContractions(i), labelsOfContractions(j), &
									otherLabelsOfContractions(k), otherLabelsOfContractions(l), totalnumberOfContractions, otherTotalNumberOfContractions, AOIndex, isInterSpecies=.true.)

						!!Calcula integrales de repulsion... segun casos libint.a, libr12.a
						call ContractedGaussian_repulsionIntegral( &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(i)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%contractionIDInParticle), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(j)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%contractionIDInParticle), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									otherSpecieID)%contractionID(k)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(otherSpecieID)%contractionID( &
									k)%contractionIDInParticle), &
									ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie( &
									otherSpecieID)%contractionID(l)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(otherSpecieID)%contractionID( &
									l)%contractionIDInParticle) ,  integralValue )

						!!Guarda integrales en buffer
						do m = 1, size(AOIndex)
							buffer(AOIndex(m)) = integralValue(m)
						end do

					end do
				end do
			end do
		end do

		!! Escribe integrales a disco
		m = 1
		do i = 1, totalNumberOfContractions
			do j = i, totalNumberOfContractions
				do k = 1, otherTotalNumberOfContractions
					do l = k, otherTotalNumberOfContractions

						if ( abs(buffer(m)) > 1.0D-10 ) then
							write(34) i, j, k, l,buffer(m)
!							print*,  i,j,k,l,buffer(m)
						end if
						m = m + 1

					end do
				end do
			end do
		end do

		write(34 ) -1,-1,-1,-1,0.0_8
		close( 34 )

		deallocate(buffer)

	end subroutine IntegralManager_writeInterspecieRepulsionIntegral




	!**
	! @brief Retorna valor de la derivada de integrales intraespecies
	! Retorna el valor de la derivada del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	! @param nameOfSpecie String que representa al tipo de particula (i.e. electron, muon, etc. )
	!**
	function IntegralManager_getIntraspecieRepulsionDerivative( i, j, k, l, nuclei, component, nameOfSpecie ) result ( output )
		implicit none
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: nuclei
		integer :: component
		character(*), optional :: nameOfSpecie
		real(8) :: output

		integer :: specieID
		integer(8) :: auxIndex
		integer :: numberOfContractions
		integer(8) :: numberOfIntegrals
		character(30) :: nameOfSpecieSelected

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie )  )  nameOfSpecieSelected = trim(nameOfSpecie)
		
		specieID= ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
		auxIndex = IndexMap_tensorR4ToVector(i,j,k,l, numberOfContractions )
			
		if ( .not.allocated(IntegralManager_instance%integralsOfIndependientSpecie(specieID,REPULSION_DERIVATIVES)%values ) ) then
		
				numberOfIntegrals   =	 int( ( (  numberOfContractions * (  numberOfContractions + 1.0_8 ) / 4.0_8 ) * &
						( (  numberOfContractions * (  numberOfContractions + 1.0_8) / 2.0_8 ) + 1.0_8) ), 8 )
			
				call Matrix_constructor( IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_DERIVATIVES ), &
					 numberOfIntegrals, 1_8, Math_NaN )
			
		else if ( .not.isNaN( IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_DERIVATIVES )%values( &
						auxIndex, 1 ) ) ) then
					
			output = IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_DERIVATIVES )%values( auxIndex, 1 )
				
			return
				
		end if
			
		output = ContractedGaussian_repulsionDerivative( & 
				 ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(i)%contractionIDInParticle), &
				 ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(j)%contractionIDInParticle), &
				 ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(k)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(k)%contractionIDInParticle), &
				 ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(l)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(l)%contractionIDInParticle), &
				 nuclei, component )

		output = output * ( ParticleManager_getCharge( specieID ) )**2.0_8
		
		IntegralManager_instance%integralsOfIndependientSpecie( specieID, REPULSION_DERIVATIVES )%values( auxIndex, 1 ) = output

			
	end function IntegralManager_getIntraspecieRepulsionDerivative


	!**
	! @brief Retorna valor de integrales de repulsion interespecies
	! Retorna el valor de la integral del arreglo de datos, interpretandolos como un tensor de rango 4
	!
	! @param thisID Corresponde al valor de la instancia. Las instancias soportadas son: REPULSION_INTEGRALS
	! @param i Posicion en la dimension 1 elemento dentro de la matriz
	! @param j Posicion en la dimension 2 elemento dentro de la matriz
	! @param k Posicion en la dimension 3 elemento dentro de la matriz
	! @param l Posicion en la dimension 4 elemento dentro de la matriz
	! @author Edwin Posada
	!**
	function IntegralManager_getInterspecieRepulsionIntegral (p, q, r, s,  specieID, otherSpecieID, &
																numberContractions, otherNumberContractions, &
																totalNumberContractions, otherTotalNumberContractions  ) result ( output )
		implicit none
		integer :: thisID
		integer :: specieID
		integer :: otherSpecieID
		integer :: numberContractions
		integer :: otherNumberContractions
		integer :: totalNumberContractions
		integer :: otherTotalNumberContractions
		integer :: p, q, r, s
		real(8) :: output
		
		integer :: i, j, k, l, m
		integer :: numberOfPrimitives
		integer :: maxAngularMoment
		integer :: smallerID
		integer :: numberOfQuantumSpecies
		integer :: matrixNumber

		integer(8) :: auxIndex
		integer(8) :: numberOfIntegrals

		integer, allocatable :: AOIndex(:)
		integer, allocatable :: labelsOfContractions(:)
		integer, allocatable :: otherLabelsOfContractions(:)

		real(8), allocatable :: integralValue(:)
		type(vector), allocatable, save :: buffer(:)

		logical :: couplingA
		logical :: couplingB

		couplingA = .false.
		couplingB = .false.

		numberOfQuantumSpecies = ParticleManager_getNumberOfQuantumSpecies()
		
		if ( specieID < otherSpecieID ) then 
			smallerID = specieID
			auxIndex = IndexMap_tensorR4ToVector(p,q,r,s, totalNumberContractions, otherTotalNumberContractions )
			couplingA = .true.
		else
			smallerID = otherSpecieID
			auxIndex = IndexMap_tensorR4ToVector(r,s,p,q, otherTotalNumberContractions, totalNumberContractions )
			couplingB = .true.
		end if

		if ( .not. allocated( buffer ) ) then

			allocate( buffer( numberOfQuantumSpecies * ( numberOfQuantumSpecies-1 ) / 2) )

		end if

		if ( .not. allocated(IntegralManager_instance%integralsInterSpecie ) ) then
		
			allocate(  IntegralManager_instance%integralsInterSpecie( numberOfQuantumSpecies * ( numberOfQuantumSpecies-1 ) / 2) )
			
		end if
		
		!! Determina el indice de la matrix de interaccion inter-especie
		matrixNumber= ( smallerID - 1_8 ) * ( numberOfQuantumSpecies - smallerID / 2.0_8 ) + abs( specieID - otherSpecieID )

		!! Calcula el numero de integrales que se almacenaran dentro de la matriz dada
		numberOfIntegrals = ( totalNumberContractions * ( ( totalNumberContractions + 1.0_8) / 2.0_8 ) ) * &
							( otherTotalNumberContractions * ( ( otherTotalNumberContractions + 1.0_8 ) / 2.0_8 ) )

		if (  .not.allocated(IntegralManager_instance%integralsInterSpecie( matrixNumber )%values ) ) then
						
			call Matrix_constructor( IntegralManager_instance%integralsInterSpecie( matrixNumber) , &
				 					 numberOfIntegrals, 1_8, Math_NaN )
		end if

		if( .not. allocated (buffer(matrixNumber)%values) ) then

			call Vector_constructor(buffer(matrixNumber), numberOfIntegrals + 1 , 0.0_8)

		end if

		if(buffer(matrixNumber)%values( numberOfIntegrals + 1 ) == 0.0_8) then

			buffer(matrixNumber)%values( numberOfIntegrals + 1) = 1.0_8

			!! Specie ID
			if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
			allocate(labelsOfContractions(numberContractions))

			labelsOfContractions = IntegralManager_getLabels(specieID, numberContractions)
			maxAngularMoment = ParticleManager_getMaxAngularMoment()
			numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

			!! OtherSpecie ID
			if (allocated(otherLabelsOfContractions)) deallocate(otherLabelsOfContractions)
			allocate(otherLabelsOfContractions(otherNumberContractions))

			otherLabelsOfContractions = IntegralManager_getLabels(otherSpecieID, otherNumberContractions)

			!! Inicializa LIBINT
			if(.not. LibintInterface_isInstanced()) then
				call LibintInterface_constructor(LibintInterface_instance, maxAngularMoment, numberOfPrimitives, "ERIS")
			end if

			do i = 1 , numberContractions
				do j = 1 , numberContractions
					do k = 1 , otherNumberContractions
						do l = 1 , otherNumberContractions

							!!Inicializa datos globales
							call LibintInterface_initializeShell(&
									 	ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									 	specieID)%contractionID(i)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
									 	specieID)%contractionID(i)%contractionIDInParticle)%primitives(1), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										specieID)%contractionID(j)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										specieID)%contractionID(j)%contractionIDInParticle)%primitives(1), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(k)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(k)%contractionIDInParticle)%primitives(1), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(l)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(l)%contractionIDInParticle)%primitives(1), &

										labelsOfContractions(i), labelsOfContractions(j), otherLabelsOfContractions(k), otherLabelsOfContractions(l), &
										totalNumberContractions, otherTotalNumberContractions, AOIndex, isCouplingA = couplingA, isCouplingB = couplingB)


							!!Calcula integrales de repulsion... segun casos libint.a, libr12.a
							call ContractedGaussian_repulsionIntegral( &
									 	ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									 	specieID)%contractionID(i)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
									 	specieID)%contractionID(i)%contractionIDInParticle), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										specieID)%contractionID(j)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										specieID)%contractionID(j)%contractionIDInParticle), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(k)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(k)%contractionIDInParticle), &

										ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(l)%particleID)%basis%contractions(ParticleManager_instance%idsOfContractionsForSpecie(&
										otherSpecieID)%contractionID(l)%contractionIDInParticle), integralValue )

										do m = 1, size(AOIndex)
											buffer(matrixNumber)%values(AOIndex(m)) = integralValue(m)
										end do
						end do
					end do
				end do
			end do
		end if

!		print*, IntegralManager_instance%integralsInterSpecie(matrixNumber)%values
!		print*, ""

		if ( .not.isNaN( IntegralManager_instance%integralsInterSpecie(matrixNumber)%values( auxIndex, 1 ) ) ) then

			output = IntegralManager_instance%integralsInterSpecie(matrixNumber)%values( auxIndex, 1 )

		else

			IntegralManager_instance%integralsInterSpecie(matrixNumber)%values( auxIndex, 1 ) = buffer(matrixNumber)%values(auxIndex)
			output = IntegralManager_instance%integralsInterSpecie(matrixNumber)%values( auxIndex, 1 )

		end if

!		print*, IntegralManager_instance%integralsInterSpecie(matrixNumber)%values
!		print*, "======================================================================"

	end function IntegralManager_getInterspecieRepulsionIntegral
	
	!**
	! @brief Retorna la integral de repulsion para orbitales moleculares de la especie especificada 
	!**
	function IntegralManager_getMolecularIntraspecieRepulsionIntegral( a, b, r, s, specieID, numberOfContractions ) result ( output )
		implicit none
		integer :: a
		integer :: b
		integer :: r
		integer :: s
		integer :: specieID
		integer :: numberOfContractions
		real(8) :: output

		type(Exception) :: ex
		type(Matrix) :: eigenVectors
		integer(8) :: auxIndex
		integer(8) :: auxIndexB
		integer(8) :: numberOfIntegrals
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		real(8) :: auxValue
		logical, external :: molecularsystem__mp_molecularsystem_isinstanced
		type(Matrix), external :: molecularsystem__mp_molecularsystem_geteigenvectors

		if  ( molecularsystem__mp_molecularsystem_isinstanced() ) then
			
			auxIndex = IndexMap_tensorR4ToVector(a,b,r,s, numberOfContractions )
			eigenVectors = molecularsystem__mp_molecularsystem_geteigenvectors(specieID)
			output = 0.0_8
		
			do i=1, numberOfContractions
				do j=1, numberOfContractions
					do k=1, numberOfContractions
						do l=1, numberOfContractions
	
							auxIndexB = IndexMap_tensorR4ToVector(i,j,k,l, numberOfContractions )
							auxValue = IntegralManager_instance%integralsOfIndependientSpecie( specieID,&
									REPULSION_INTEGRALS )%values( auxIndexB, 1 )
									
							auxValue= IntegralManager_getIntraspecieRepulsionIntegral( i, j, k, l, specieID,numberOfContractions )
							
							if ( dabs(auxValue) > 1.0E-9_8 ) then
								
								if( dabs( eigenVectors%values( i, a ) ) >  1.0E-9_8 )  then
									auxValue = auxValue *  eigenVectors%values( i, a )
									if( dabs( eigenVectors%values( j, b ) ) >  1.0E-9_8 )  then
										auxValue = auxValue *  eigenVectors%values( j, b )
										if( dabs( eigenVectors%values( k, r ) ) >  1.0E-9_8 )  then
											auxValue = auxValue *  eigenVectors%values( k, r )
											if( dabs( eigenVectors%values( l, s ) ) >  1.0E-9_8 )  then
												auxValue = auxValue *  eigenVectors%values( l, s )
												output = output +auxValue
											end if
										end if
									end if
								end if
										
							end if
							
						end do
					end do
				end do
			end do
			
					
		else
			
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object IntegralManager in intraespeciesRepulsionMolecularIntegral function" )
			call Exception_setDescription( ex, "There aren't a wave function defined in the Molecular System" )
			call Exception_show( ex )
		
		end if
			
	end function IntegralManager_getMolecularIntraspecieRepulsionIntegral
	

	!**
	! @brief 	Retorna la integral de repulsion para orbitales moleculares 
	!		entre las especies especificadas.
	!**
	function IntegralManager_getMolecularInterspecieRepulsionIntegral( a, b, r, s, specieID, otherSpecieID, &
				numberOfContractions, otherNumberOfContractions ) result ( output )
		implicit none
		integer :: a
		integer :: b
		integer :: r
		integer :: s
		integer :: specieID
		integer :: otherSpecieID
		integer :: numberOfContractions
		integer :: otherNumberOfContractions
		real(8) :: output

		type(Exception) :: ex
		type(Matrix) :: eigenVectorsA
		type(Matrix) :: eigenVectorsB
		integer :: smallerID
		integer(8) :: auxIndex
		integer(8) :: auxIndexB
		integer(8) :: numberOfIntegrals
		integer :: numberOfQuantumSpecies
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: matrixNumber
		real(8) :: auxValue
		logical, external :: molecularsystem__mp_molecularsystem_isinstanced
		type(Matrix), external :: molecularsystem__mp_molecularsystem_geteigenvectors

		if  ( molecularsystem__mp_molecularsystem_isinstanced() ) then
			
		
		
			if ( specieID < otherSpecieID ) then 
				smallerID = specieID
				auxIndex = IndexMap_tensorR4ToVector(a, b, r, s, numberOfContractions, otherNumberOfContractions )
			else
				smallerID = otherSpecieID
				auxIndex = IndexMap_tensorR4ToVector(r, s, a, b, otherNumberOfContractions, numberOfContractions )
			end if
		
			eigenVectorsA = molecularsystem__mp_molecularsystem_geteigenvectors(specieID)
			eigenVectorsB = molecularsystem__mp_molecularsystem_geteigenvectors(otherSpecieID)

			output = 0.0_8
		
			do i=1, numberOfContractions
				do j=1, numberOfContractions
					do k=1, otherNumberOfContractions
						do l=1, otherNumberOfContractions
	
							if ( specieID < otherSpecieID ) then 
								auxIndexB = IndexMap_tensorR4ToVector(i,j,k,l, numberOfContractions, otherNumberOfContractions )
							else
								auxIndexB = IndexMap_tensorR4ToVector(k,l,i,j, otherNumberOfContractions, numberOfContractions )
							end if
							
							if ( allocated( IntegralManager_instance%integralsInterSpecie(matrixNumber)%values) ) then
							
								auxValue = IntegralManager_instance%integralsInterSpecie(matrixNumber)%values( auxIndexB, 1 )
															
								if ( isNaN( auxValue ) ) then
						
!									auxValue= IntegralManager_getInterspecieRepulsionIntegral(&
!											specieID, otherSpecieID, numberOfContractions, otherNumberOfContractions )
										print*, "IntegralManager_getMolecularInterspecieRepulsionIntegral"
										stop
					
								end if
							else
							
!								auxValue= IntegralManager_getInterspecieRepulsionIntegral(&
!										specieID, otherSpecieID, numberOfContractions, otherNumberOfContractions )
										print*, "IntegralManager_getMolecularInterspecieRepulsionIntegral"
										stop
							end if


							if ( dabs( auxValue ) > 1.0E-9_8 ) then

								output = output & 
									+ auxValue* ParticleManager_getCharge( specieID ) &
									* ParticleManager_getCharge( otherSpecieID ) &
									* eigenVectorsA%values( i, a ) &
									* eigenVectorsA%values( j, b ) &
									* eigenVectorsB%values( k, r ) &
									* eigenVectorsB%values( l, s )

							end if

						end do
					end do
				end do
			end do
		
			output = output * ParticleManager_getCharge( specieID ) * ParticleManager_getCharge( otherSpecieID )
		
		else
			
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object IntegralManager in IterspecieRepulsionMolecularIntegral function" )
			call Exception_setDescription( ex, "There aren't a wave function defined in the Molecular System" )
			call Exception_show( ex )
		
		end if
			
	end function IntegralManager_getMolecularInterspecieRepulsionIntegral



	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!! @warning esta matriz solo funciona al momento de usar fuciones S si se usan de mayor momento angular se debe usar una overlap de 3 centros (no implementada)
	!<
	function IntegralManager_getInteractionWithPotentialMatrix(potential, sspecieID, interactName) result(output)
		implicit none
		type(ExternalPotential) :: potential(:)
		integer, optional :: sspecieID
		character(*), optional :: interactName
		type(Matrix) :: output
		type(Exception) :: ex

		integer :: specieID
		integer :: i
		integer :: j
		integer :: potID
		integer :: numberOfContractions
		integer :: totalNumberOfContractions
		real(8) :: auxValue
		real(8) :: integralValue(1)
		character(30) :: interactNameSelected
		type(ContractedGaussian) :: auxContract

		interactNameSelected = "e-"
		if ( present( interactName ) )  interactNameSelected= trim(interactName)

		if( present(sspecieID) ) then
			specieID=sspecieID
!			interactNameSelected=trim(ParticleManager_getNameOfSpecie( specieID ))
		else
			specieID= ParticleManager_getSpecieID( nameOfSpecie=trim(interactNameSelected ) )
!			interactNameSelected=trim(ParticleManager_getNameOfSpecie( specieID ))
		end if

		interactNameSelected=trim(ParticleManager_getNameOfSpecie( specieID ))

		totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

		call Matrix_constructor(output,int(totalNumberOfContractions,8),int(totalNumberOfContractions,8),0.0_8)

		potID=0

		print*, "tamaño", size(potential)

		do i=1,size(potential)
			print*, potID, i, trim(potential(i)%interactionName),"  ",  trim(interactNameSelected)
			if( trim(potential(i)%interactionName)==trim(interactNameSelected) ) then
				potID=i
				exit
			end if
		end do

		if( potID>0 ) then
	
	 		do i=1, numberOfContractions
	 
	 			call ContractedGaussian_product(ParticleManager_getContractionPtr( specieID, &
					numberOfContraction=i ), &
	 					potential(potID)%gaussianComponents, auxContract)
	 			
	 			do j=i, numberOfContractions
!
					call ContractedGaussian_overlapIntegral( &
	 					auxContract, ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ), integralValue )

	 				output%values(i,j)= integralValue(1)

					print*, "IntegralManager_getInteractionWithPotentialMatrix"
					stop
	 			end do
	 
	 			call ContractedGaussian_destructor(auxContract)
	 		end do
	 		
	 		call Matrix_symmetrize( output, 'U' )
	
		end if
		
		stop "se uso la rutina"

	end function IntegralManager_getInteractionWithPotentialMatrix


	!**
	! @brief 	Borra los valores de las integrales asociadas a contracciones
	!	 	que han cambiado en alguno de sus parametros
	!
	! @todo Falta incluir el algoritmo que solo borre lintegrales asociadas
	!		a contracciones modificadas
	!**
	subroutine IntegralManager_reset()
		implicit none
		
		integer :: i
		integer :: j
		
		do i=1, size( IntegralManager_instance%integralsOfIndependientSpecie, dim=1 )
			do j=1, size( IntegralManager_instance%integralsOfIndependientSpecie, dim=2 )
				IntegralManager_instance%integralsOfIndependientSpecie(i,j)%values = Math_NaN
			end do
		end do	
			
		do i=1, size(IntegralManager_instance%integralsInterSpecie )
			IntegralManager_instance%integralsInterSpecie(i)%values = Math_NaN
		end do
		
	end subroutine IntegralManager_reset
	
end module IntegralManager_
