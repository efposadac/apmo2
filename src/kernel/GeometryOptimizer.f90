!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module GeometryOptimizer_
	use APMO_
	use Units_
	use CalculateProperties_
	use InternalCoordinates_
	use MecanicProperties_
	use TrustRegionOptimizer_
	use TrustRegionIterative_
	use EmpiricalInformation_
	use RHFSolver_
	use EnergyDerivator_
	use MollerPlesset_
	use Exception_
	use MolecularSystem_
	use InputParser_
        use ExternalSoftware_
        use Solver_

	implicit none

	!>
	!! @brief Modulo para minimizacion multidimencional de funciones
	!! 
	!!
	!! @author Sergio A. Gonzalez Monico
	!!
	!! <b> Fecha de creacion : </b> 2008-10-10
	!!
	!! <b> Historial de modificaciones: </b>
	!!
	!!   - <tt> 2008-10-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
	!!        -# Creacion de modulo y metodos
	!!
	!<	
	type, public :: GeometryOptimizer
		
		character(30) :: name
		integer :: numberOfIterations
		integer :: numberOfIndependVariables
		type(Vector) :: valuesOfIndependentVariables
		type(Vector) :: gradient
		real(8) :: functionValue
		real(8) :: stepSize
		type(InternalCoordinates) :: primitivesCoordinates
		type(TrustRegionOptimizer) :: trustRegion
		type(TrustRegionIterative) :: trustRegionIterative
		logical :: isInitialExecution
		type(APMO) :: APMO_bkp
                type(Solver), pointer :: solverPtr
	end type

	type(GeometryOptimizer), pointer , private :: this_pointer
	type(GeometryOptimizer), public :: apmo_geometryOptimizer

	public :: &
		GeometryOptimizer_constructor, &
		GeometryOptimizer_destructor, &
		GeometryOptimizer_run, &
		GeometryOptimizer_setName, &
		GeometryOptimizer_getNumberOfIterations, &
		GeometryOptimizer_getFunctionValue, &
		GeometryOptimizer_getGradient

	private
contains
		!**
		! @brief Define el constructor
		!**
		subroutine GeometryOptimizer_constructor( this, ssolver )
			implicit none
			type(GeometryOptimizer) :: this
                         type(Solver), target :: ssolver

			type(Matrix) :: initialHessian
			type(Vector) :: initialGeometry
			type(EmpiricalInformation) :: empiricalInfo
! 			print*, " GeometryOptimizer_constructor" 
			
			this.name = ""
			this.isInitialExecution=.true.
			initialGeometry = ParticleManager_getPositionOfCenterOfOptimizacion()
			this.numberOfIndependVariables = size( initialGeometry.values )
			call APMO_copyConstructor(this.APMO_bkp, APMO_instance)
			call EmpiricalInformation_constructor( empiricalInfo)
 			initialHessian =EmpiricalInformation_getCartesianForceConstants( empiricalInfo , MolecularSystem_instance )
                        this.solverPtr => ssolver

			select case ( trim(APMO_instance.MINIMIZATION_METHOD))
				
				case("TR")

					call TrustRegionOptimizer_constructor( this.trustRegion, initialGeometry.values )
					call TrustRegionOptimizer_setInitialHessian( this.trustRegion, initialHessian.values )

				case("TRI")

					call TrustRegionIterative_constructor( this.trustRegionIterative, initialGeometry.values )
					call TrustRegionIterative_setInitialHessian( this.trustRegionIterative, initialHessian.values )

				case default

					call TrustRegionOptimizer_constructor( this.trustRegion, initialGeometry.values )
					call TrustRegionOptimizer_setInitialHessian( this.trustRegion, initialHessian.values )

			end select
			call EnergyDerivator_constructor()
			call Matrix_destructor(initialHessian)
			call Vector_destructor(initialGeometry)
			call EmpiricalInformation_destructor( empiricalInfo)
			
						
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! INICIO	 DE BLOQUE DE DEPURACION PARA PROYECCION DE g Y H !!!!!!!!!!!!!!!!!!!!!
! 			call Vector_constructor(ggradient,9)
! 			call MecanicProperties_constructor( mecProperties ) 
! 			mecProperties.transformationMatrix = MecanicProperties_getTransformationMatrix( mecProperties)
! 			 ggradient.values=[2.247793601009899D-004, 0.850324491434882_8,  6.01352605317418_8,   8.91611277075303_8, &
! 			9.67955701969543_8, 1.89689771826235_8,   5.14975824167475_8,   3.98008388186809_8,   2.62906165450302_8]
! 			ggradientProj = ggradient
! 			ggradientProj=MecanicProperties_getGradientProjectedOnExternalDegrees( mecProperties, ggradient ) 
! 
! 			call Matrix_constructor(hessiane,9_8,9_8)
! 			call Matrix_setIdentity( hessiane )
! 			hessiane = MecanicProperties_getHessianeProjectedOnExternalDegrees( mecProperties, hessiane )
! 			print *,"HESSIANA TRANFORMADA" !!! verificar transformacion para una matriz no unitaria
! 			call Matrix_show(hessiane)
! 
! 			call Vector_show(ggradientProj)
! 			call abort("salida forzada en GeometryOptimizer_constructor")
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FINAL DE BLOQUE DE DEPURACION PARA PROYECCION DE g Y H !!!!!!!!!!!!!!!!!!!!!
			
			
!!!!!!!!!!!!!!!!!!!!!!!!!!  INICIO DE BLOQUE EN DEPURACION PARA OBTENCION DE  COORD. REDUNDANTES!!!!!!!!!!!!!!!!!!!!!!!!!!
! 			!! Se seleccionan las coordenadas primitivas para distancias de enlace
! 			call  InternalCoordinates_constructor( this.primitivesCoordinates )
! 
! 			!! Realiza la seleccion de coordenada primitivas
! 			call InternalCoordinates_primitiveCoordinateSelection(this.primitivesCoordinates)
! 
! 			!!  Se calcula la matriz de Wilson para la distancias de enlace
!  			this.primitivesCoordinates.BWilsonMatrix = &
! 				InternalCoordinates_getBWilsonMatrix(this.primitivesCoordinates )
! 
! 			this.primitivesCoordinates.inverseOfBWilsonMatrix= &
! 				InternalCoordinates_getInverseOfTransposeBWilsonMatrix( this.primitivesCoordinates )
! 
! 
! 				initialHessian=InternalCoordinates_getStartHessian(this.primitivesCoordinates)
! 			call abort( "salida forzada" )
!!!!!!!!!!!!!!!!!!!!!!!!!! FIN  BLOQUE EN DEPURACION PARA OBTENCION DE  COORD. REDUNDANTES!!!!!!!!!!!!!!!!!!!!!!!!!!

! 			!! Calcula  los pesos asociados a las coordenadas primitivas seleccionadas
! 			call InternalCoordinates_selectCoordinatesForWeight( this.primitivesCoordinates )
! 
! 			call abort( "salida forzada" )
!			call Matrix_destructor(initialHessian)


		end subroutine GeometryOptimizer_constructor
		
		!**
		! @brief Define el destructor
		!**
		subroutine GeometryOptimizer_destructor(this)
			implicit none
			type(GeometryOptimizer) :: this

! 			  print*, " GeometryOptimizer_destructor" 
			  
			  this.solverPtr => null()

			select case ( trim(APMO_instance.MINIMIZATION_METHOD))
				case("TR")
					call TrustRegionOptimizer_destructor( this.trustRegion )
				case("TRI")
					call TrustRegionIterative_destructor( this.trustRegionIterative )
			end select
			call EnergyDerivator_destructor()

		end subroutine GeometryOptimizer_destructor

		!>
		!! @brief Muestra en la la  unidad de salida el estimado del minimo
		!!
		!!	@warning Anque no se define como privada de la clase por propositos de conveniencia
		!!			no debe ser empleada fuera de la clase
		!<
		subroutine GeometryOptimizer_showMinimum( evaluationPoint, gradient, functionValue )
			implicit none
			real(8) :: evaluationPoint(:)
			real(8) :: gradient(:)
			real(8) :: functionValue
			
			integer :: i
			integer :: k
			real(8) :: origin(3)

! 			print*, " GeometryOptimizer_showMinimum" 
			
			write (6,*) ""
			write (6,"(T20,A25)") "COORDINATES: "//trim(APMO_instance.UNITS)
			write (6,"(A10,A16,A20,A20)") "Particle","<x>","<y>","<z>"
			do i = 1, ParticleManager_getTotalNumberOfParticles()
				
				if ( ParticleManager_isCenterOfOptimization( i ) ) then
					origin = ParticleManager_getOrigin( iterator = i ) 
					if ( APMO_instance.UNITS=="ANGSTROMS") then
						origin = origin * AMSTRONG
					end if
					
					write (6,"(A10,<3>F20.10)") trim( ParticleManager_getSymbol( iterator = i ) ), origin(1), origin(2), origin(3)
				end if
			
			end do
			
			
			print *,""
			write (6,"(T20,A25)") "GRADIENT: HARTREE/BOHR"
			write (6,"(A10,A16,A20,A20)") "Particle","dE/dx","dE/dy","dE/dz"
			k=1
			do i = 1, ParticleManager_getTotalNumberOfParticles()
				
				if ( ParticleManager_isCenterOfOptimization( i ) .and. k < size(gradient)) then
						
					write (6,"(A10,<3>F20.10)") trim( ParticleManager_getSymbol( iterator = i ) ), &
						gradient(k), gradient(k+1),gradient(k+2)
					k=k+3
						
				end if
			
			end do
			write (6,*) ""
			write (6,"(T10,A24,F20.10)") "TOTAL ENERGY (Hartree) =", functionValue
			

		end subroutine GeometryOptimizer_showMinimum
		
		!>
		!! @brief Ejecuta el minimizador hasta encontrar un minimo de la estructura
		!<
		subroutine GeometryOptimizer_run(this )
			implicit none
			type(GeometryOptimizer), target :: this

			real(8) :: auxValue
			logical :: isMinumum
			type(Matrix) :: initialHessian
			type(EmpiricalInformation) :: empiricalInfo

! 			print*, " GeometryOptimizer_run" 
			
			print	*,""
 			print *, " BEGIN GEOMETRY OPTIMIZATION: "
 			print *,"============================"
			print	*,""
			isMinumum = .false.
 			APMO_instance.SCF_CONVERGENCE_CRITERIUM="energy"
			if( APMO_instance.MINIMIZATION_WITH_SINGLE_POINT ) APMO_instance.SCF_CONVERGENCE_CRITERIUM = "density"
			
			this_pointer => this

			!!************************************************************
			!! Inicia proceso de minimizacion
			!!***

			select case ( trim(APMO_instance.MINIMIZATION_METHOD))

				case("TR")
					do while ( .not.isMinumum )
	
						write (6,*) ""
						call TrustRegionOptimizer_iterate( this.trustRegion, &
								GeometryOptimizer_getFunctionValue, &
								GeometryOptimizer_getGradient, &
								GeometryOptimizer_projectGradient, &
								GeometryOptimizer_projectHessiane, &
								GeometryOptimizer_showMinimum )
				
						isMinumum = TrustRegionOptimizer_isMinimum( this.trustRegion )
					
						if  ( ( TrustRegionOptimizer_getNumberOfIterations(this.trustRegion) > &
							this.numberOfIndependVariables ) .and. (APMO_instance.RESTART_OPTIMIZATION) ) then
						      
							call TrustRegionOptimizer_restart(this.trustRegion)
							call EmpiricalInformation_constructor(empiricalInfo)
							initialHessian =EmpiricalInformation_getCartesianForceConstants( empiricalInfo , MolecularSystem_instance )
							call TrustRegionOptimizer_setInitialHessian( this.trustRegion, initialHessian.values )
							call Matrix_destructor(initialHessian)
							call EmpiricalInformation_destructor(empiricalInfo)
	
						end if
	
					end do
			
					write(6,*) ""
					write (6,"(T20,A30)") " FINAL GEOMETRY: AMSTRONG"	
					write (6,"(T18,A35)") "------------------------------------------"
					write(6,*) ""
					write (6,"(T20,A25,ES20.8)") "FINAL GRADIENT (RMS):",TrustRegionOptimizer_getGradient(this.trustRegion)	

				case("TRI")

					do while ( .not.isMinumum )
	
						write (6,*) ""
	
						call TrustRegionIterative_iterate( this.trustRegionIterative, &
								GeometryOptimizer_getFunctionValue, &
								GeometryOptimizer_getGradient, &
								GeometryOptimizer_projectGradient, &
								GeometryOptimizer_projectHessiane, &
								GeometryOptimizer_showMinimum )
				
						isMinumum = TrustRegionIterative_isMinimum( this.trustRegionIterative )
					
						if  ( ( TrustRegionIterative_getNumberOfIterations(this.trustRegionIterative) > &
							this.numberOfIndependVariables ) .and. (APMO_instance.RESTART_OPTIMIZATION) ) then
						
							call TrustRegionIterative_restart(this.trustRegionIterative)
							call EmpiricalInformation_constructor(empiricalInfo)
							initialHessian =EmpiricalInformation_getCartesianForceConstants( empiricalInfo, &
									MolecularSystem_instance )
							call TrustRegionIterative_setInitialHessian( this.trustRegionIterative, initialHessian.values )
							call Matrix_destructor(initialHessian)
							call EmpiricalInformation_destructor(empiricalInfo)
	
						end if
	
					end do
			
					write(6,*) ""
					write (6,"(T20,A30)") " FINAL GEOMETRY: AMSTRONG"	
					write (6,"(T18,A35)") "------------------------------------------"
					write(6,*) ""
					write (6,"(T20,A25,ES20.8)") "FINAL GRADIENT (RMS):",TrustRegionIterative_getGradient(this.trustRegionIterative)	

			end select

			write(6,*) ""
			call MolecularSystem_showCartesianMatrix()
			call MolecularSystem_showDistanceMatrix()
			call MolecularSystem_showZMatrix( MolecularSystem_instance )

			!! Realiza un calculo final en la geometria de equilibrio
			APMO_instance.OPTIMIZE=.false.
			APMO_instance.SCF_CONVERGENCE_CRITERIUM="density"
			call SCFInterspecies_setStopingThreshold( APMO_instance.ELECTRONIC_DENSITY_MATRIX_TOLERANCE, &
			APMO_instance.NONELECTRONIC_DENSITY_MATRIX_TOLERANCE )

			print	*,""
			print *, "END GEOMETRY OPTIMIZATION "
			print *,""

                        this.solverPtr.withProperties=.true.
                        call Solver_run( this.solverPtr )

			

		end subroutine GeometryOptimizer_run
		
		
		!>
		!! @brief Ajusta el nombre del minimizador empleado
		!!
		!! @todo Falta implementacion completa
		!<
		subroutine GeometryOptimizer_setName(this)
			implicit none
			type(GeometryOptimizer) :: this
		
! 			print*, " GeometryOptimizer_setName"
			
		end subroutine GeometryOptimizer_setName
		

		!>
		!! @brief retorna el numero de iteraciones actual del minimizador
		!!
		!! @todo Falta por implementar
		!<
		function GeometryOptimizer_getNumberOfIterations(this) result(output)
			implicit none
			type(GeometryOptimizer) :: this
			integer :: output
			
! 			print*, " GeometryOptimizer_getNumberOfIterations" 
			
			output = 0

		end function GeometryOptimizer_getNumberOfIterations
		
		!>
		!! @brief retorna el valor la funcion a minimiza en el punto especificado
		!!		esta funcion debe ser llamada privia asigacion de apuntador this_pointer
		!!
		!! @warning Anque no se define como privada de la clase por propositos de conveniencia
		!!			no debe ser empleada fuera de la clase
		!<
		function GeometryOptimizer_getFunctionValue( evaluationPoint, iterator ) result(output)
			implicit none
			real(8):: evaluationPoint(:)
			integer, optional :: iterator
			real(8) :: output

			type(Vector) :: valuesOfIndependentVariables
			
! 			print*, " GeometryOptimizer_getFunctionValue" 
			
			call Vector_constructor( valuesOfIndependentVariables, size(evaluationPoint) )
			valuesOfIndependentVariables.values = evaluationPoint

			!! Ajusta el origen de las particulas presentes en el sistema
			call ParticleManager_setParticlesPositions( valuesOfIndependentVariables)
			call Vector_destructor( valuesOfIndependentVariables )

			 select case ( trim(APMO_instance.ENERGY_CALCULATOR) )

			 case("internal")

			      if ( this_pointer.isInitialExecution ) then
					if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) call EnergyDerivator_setNumeric()
			      else
					call Solver_reset( this_pointer.solverPtr )
			      end if

			      !! Restituye las variables originales para el control de programa
			      call APMO_copyConstructor(APMO_instance, this_pointer.APMO_bkp)

			      this_pointer.solverPtr.withProperties=.false.
			      call Solver_run( this_pointer.solverPtr )

                              output = this_pointer.solverPtr.energy

			      this_pointer.isInitialExecution=.false.
			      print "(A1$)","."

			 case("external")

                              call EnergyDerivator_setNumeric()
			      if ( .not.external_instance.isInstanced ) &
					call ExternalSoftware_constructor( external_instance )
					output = ExternalSoftware_getEnergy( external_instance, &
						withCounterPoiseCorrection = APMO_instance.OPTIMIZE_WITH_CP_CORRECTION , iterador=iterator)
		    end select

		

		end function GeometryOptimizer_getFunctionValue
		
		!>
		!! @brief Retorna el gradiente obtenido en la ultima iteracion
		!!		Esta funcion solo debe emplease previa asignacion del apuntador this_pointer
		!!
		!! @warning Anque no se define como privada de la clase por propositos de conveniencia
		!!			no debe ser empleada fuera de la clase
		!<
		subroutine GeometryOptimizer_getGradient( evaluationPoint, gradientVector )
			implicit none
			real(8) :: evaluationPoint(:)
			real(8) :: gradientVector(:)
			
			type(Vector) :: valuesOfIndependentVariables
			character(100) :: nameOfOutputFile
			real(8), allocatable :: auxVal1(:), auxVal2(:)
			integer :: iterator
			integer :: i, status
			  
! 			print*, " GeometryOptimizer_getGradient"
			
			call Vector_constructor( valuesOfIndependentVariables, size(evaluationPoint) )
			valuesOfIndependentVariables.values = evaluationPoint
			
			if( .not.APMO_instance.ANALYTIC_GRADIENT ) then

				gradientVector=EnergyDerivator_getNumericGradient( valuesOfIndependentVariables, GeometryOptimizer_getFunctionValue )

			else
				if(trim( APMO_instance.EXTERNAL_SOFTWARE_NAME ) == "lowdin") then

					iterator = 0
					do i=1, this_pointer.numberOfIndependVariables
						iterator = iterator + 1
						gradientVector(i) = EnergyDerivator_getDerivative(valuesOfIndependentVariables , [i], &
							GeometryOptimizer_getFunctionValue, iterator=iterator )
						print "(A1$)",":"
					end do
					!!Calcule energias
					status = system("calculateList.sh")
					
					!!Busque energias
					if (allocated(auxVal1)) deallocate(auxVal1)
					if (allocated(auxVal2)) deallocate(auxVal2)
					allocate(auxVal1(iterator), auxVal2(iterator))
					
					do i = 1, iterator
						nameOfOutputFile=trim(APMO_instance.INPUT_FILE)//&
							trim(adjustl(trim(String_convertIntegerToString(0))))//&
							trim(adjustl(trim(String_convertIntegerToString(i))))//".ext.out"
						
						auxVal1(i)=ExternalSoftware_findString( external_instance, trim(nameOfOutputFile) )
						
						status = system ("rm -f "//trim(nameOfOutputFile) )
						
						print*, auxVal1(i)
					end do
					
					do i = 101, iterator+100
						nameOfOutputFile=trim(APMO_instance.INPUT_FILE)//&
							trim(adjustl(trim(String_convertIntegerToString(0))))//&
							trim(adjustl(trim(String_convertIntegerToString(i))))//".ext.out"
						
						auxVal2(i-100)=ExternalSoftware_findString( external_instance, trim(nameOfOutputFile) )
						
						status = system ("rm -f "//trim(nameOfOutputFile) )
						
						print*, auxVal2(i-100)
					end do
					
					!!Calcule gradiente
					 
					do i = 1, iterator
						gradientVector(i) = EnergyDerivator_getNumericDerivative2(auxVal1(i), auxVal2(i))
						print*, gradientVector(i)
					end do
					
					
				else
					do i=1, this_pointer.numberOfIndependVariables
						gradientVector(i) = EnergyDerivator_getDerivative(valuesOfIndependentVariables , [i], &
							GeometryOptimizer_getFunctionValue )
						print "(A1$)",":"
					end do
				end if
			end if
			print *,""
			call Vector_destructor( valuesOfIndependentVariables )
			
		end subroutine GeometryOptimizer_getGradient
		
		
		subroutine GeometryOptimizer_builtInputFile( evaluationPoint )
			real(8) :: evaluationPoint(:)
			
! 			print*, " GeometryOptimizer_builtInputFile"
			
			open( UNIT=37,FILE="tmp.aux",STATUS='REPLACE', &
				ACCESS='SEQUENTIAL', FORM='FORMATTED' )
				
				call InputParser_writeNameList(37,"InputApmoParameters")
				
			close(37)
		
		end subroutine GeometryOptimizer_builtInputFile
		
		!>
		!! @brief Define un proyector para el gradiente cartesiano
		!!
		!!	@warning Anque no se define como privada de la clase por propositos de conveniencia
		!!			no debe ser empleada fuera de la clase
		!<
		subroutine GeometryOptimizer_projectGradient( gradientVector, projectedGradient )
			implicit none
			real(8) :: gradientVector(:)
			real(8) :: projectedGradient(:)
			
			type(vector) :: pprojectedGradient
			
! 			print*, " GeometryOptimizer_projectGradient" 
			
! 			if ( APMO_instance.PROJECT_HESSIANE ) then
! 
! 				MolecularSystem_instance.mecProperties.transformationMatrix = &
! 					MecanicProperties_getTransformationMatrix( MolecularSystem_instance.mecProperties )
! 				call Vector_constructor(pprojectedGradient,size(gradientVector))
! 				pprojectedGradient.values = gradientVector
! 				pprojectedGradient =MecanicProperties_getGradientProjectedOnExternalDegrees( MolecularSystem_instance.mecProperties, &
! 					pprojectedGradient )
! 				projectedGradient=pprojectedGradient.values
! 				call Vector_destructor(pprojectedGradient)
! 
! 			else

			projectedGradient = gradientVector

! 			end if
		

		end subroutine GeometryOptimizer_projectGradient
		
		!>
		!! @brief Define un proyector para la hesiana cartesiano
		!!
		!!	@warning Anque no se define como privada de la clase por propositos de conveniencia
		!!			no debe ser empleada fuera de la clase
		!<
		subroutine GeometryOptimizer_projectHessiane( hessianeMatrix, projectedHessiane)
			implicit none
			real(8) :: hessianeMatrix(:,:)
			real(8) :: projectedHessiane(:,:)
			
			type(Matrix) :: projector
			real(8) :: auxValue
			integer :: infoProcess
			integer :: i
			integer :: j

! 			print*, " GeometryOptimizer_projectHessiane" 
			
			if ( APMO_instance.PROJECT_HESSIANE .or. APMO_instance.MOLLER_PLESSET_CORRECTION >= 2 ) then

				projector=MecanicProperties_getHandyProjector( MolecularSystem_instance.mecProperties,infoProcess)

				if(infoProcess==0) then
					projectedHessiane=matmul(projector.values,matmul(hessianeMatrix,projector.values))
	
					do i=1,size(hessianeMatrix,dim=1)
						do  j=1,i
							auxValue=0.5*( projectedHessiane(i,j) + projectedHessiane(j,i) )
							projectedHessiane(j,i) = auxValue
							projectedHessiane(i,j) = auxValue
						end do
					end do

					

				else
					projectedHessiane = hessianeMatrix
					print *, "HESSIANE WILL NOT PROJECTED, THE INERTIA MATRIX IS BADLY CONDITIONED"
					print *,""

				end if
				call Matrix_destructor(projector)

			else

				projectedHessiane = hessianeMatrix
			end if


		end subroutine GeometryOptimizer_projectHessiane

		!>
		!! @brief  Maneja excepciones de la clase
		!<
		subroutine GeometryOptimizer_exception( this, typeMessage, description, debugDescription)
			implicit none
			type(GeometryOptimizer) :: this
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

! 			print*, " GeometryOptimizer_exception"
			
			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
 
		end subroutine GeometryOptimizer_exception

		
end module GeometryOptimizer_
