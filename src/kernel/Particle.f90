!!**********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de particulas
! 
! Este modulo define una seudoclase para definicion de particulas cuanticas
! o cargas puntuales dentro del marco del metodo Hartree-Fock. En el caso de particulas
! cuanticas se describen al mismo nivel que los electrones en un calculo de estructura
! electronica regular; por su parte las cargas puntuales se tratan de la misma manera
! que los nucleos en tales metodos.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-02-06
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see ContractedGaussian_, PrimitiveGaussian_ , BasisSet_
!**
module Particle_
	use PhysicalConstants_
	use BasisSetManager_
	use BasisSet_
	use ContractedGaussian_
	use Exception_
	use String_
	
	implicit none
	
	type, public :: Particle 
		
		character(10) :: name
		character(10) :: symbol
		character(10) :: nickname
		character(10) :: statistics
		
		
		real(8) :: origin(3)				!< Posicion espacial
		real(8) :: charge 				!< Carga asociada a la particula.
		real(8) :: mass					!< Masa asociada a la particula.
		real(8) :: spin					!< Especifica el espin de la particula
		real(8) :: totalCharge			!< Carga total asociada a la particula.
		logical :: isQuantum			!< Indica comportamiento cuantico/puntual
		logical :: isDummy				!< Permite verificar si se trata de una particula virtual
		logical :: fixComponent(3)		!< Indica cual coordenada sera un parametro.
		integer :: id					!< Indice de particula dentro del sistema
		integer :: internalSize			!< Numero de particulas si se trata de una particula estructurada
                integer :: fragmentNumber               !< Define el numero de fragmento al que pertenece la particula
		integer :: owner				!< asocia un indice a la particula que la indentifica como un centro de referencia.
		integer, allocatable :: childs(:)	!< Cuando la particula es un centro de optimizacion (es padre), almacena los Ids de sus hijas
		logical :: isCenterOfOptimization	!< Especifica si la particula sera centro de optimizacion -Atributo requerido por conveniencia-

		character(20) 	:: basisSetName
		integer 	:: basisSetSize			!< Este atributo es adicionado por conveniencia
		type(BasisSet) :: basis
		
	end type Particle
	
	interface assignment(=)
		module procedure Particle_copyConstructor
	end interface

	public :: &
		Particle_constructor, &
		Particle_destructor, &
		Particle_copyConstructor, &
		Particle_show, &
		Particle_showStructure, &
		Particle_getParticlePtr, &
		Particle_getName, &
		Particle_getId, &
		Particle_getContractionPtr, &
		Particle_getOrigin,&
		Particle_getOriginComponent, &
		Particle_getMass, &
		Particle_getCharge, &
		Particle_getSpin, &
		Particle_getOwner, &
		Particle_isQuantum, &
		Particle_getNumberOfContractions, &
		Particle_setOrigin, &
		Particle_setOwner, &
		Particle_setChild, &
		Particle_setComponentFixed, &
		Particle_hasFixedComponent

	contains
	
	!**
	! Define el metodo para el constructor de la clase.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	subroutine Particle_constructor( this, isQuantum, origin, mass, charge, totalCharge , &
		spin, name, symbol, owner, isDummy, basisSetName, elementName, massNumber, nickname, fragmentNumber )
		implicit none
		type(Particle) , intent(inout) :: this
		logical , optional , intent(in) :: isQuantum
		real(8) , optional , intent(in) :: origin(3)
		real(8) , optional , intent(in) :: mass
		real(8) , optional , intent(in) :: charge 
		real(8) , optional , intent(in) :: totalCharge
		real(8) , optional , intent(in) :: spin
		character(*) , optional , intent(in) :: name
		character(*) , optional , intent(in) :: symbol
		integer , optional , intent(in) :: owner
		logical , optional , intent(in) :: isDummy
		character(*) , optional , intent(in) :: basisSetName
		character(*) , optional , intent(in) :: elementName
		integer, optional, intent(in) :: massNumber
		character(*) , optional , intent(in) :: nickname
                integer, optional, intent(in) :: fragmentNumber

		integer :: i
		character(5) ::  massNumberString
		type(Exception) :: ex

		!!***************************************************************
		!! Inicializa los parametros por omision asociados a la particula
		
		this.isQuantum = .false.
		this.origin = 0.0_8
		this.mass = PhysicalConstants_ELECTRON_MASS
		this.charge = PhysicalConstants_ELECTRON_CHARGE
		this.totalCharge = PhysicalConstants_ELECTRON_CHARGE
		this.name = "electron"
		this.symbol = "e-"
		this.statistics = "fermion"
		this.basisSetName = ""
		this.spin = PhysicalConstants_SPIN_ELECTRON
		this.fixComponent =.false.
		this.owner = 0
		this.isDummy = .false.
		this.internalSize = 0
		this.isCenterOfOptimization = .true.
		this.nickname=""
                this.fragmentNumber=0
		!!***************************************************************
			
		if ( present(origin) ) this.origin = origin
		if ( present( mass ) ) this.mass = mass
		if ( present(charge) ) this.charge = charge
		
		if ( present(totalCharge)) then
			this.totalCharge = totalCharge
		else
			this.totalCharge = this.charge
		end if
		
		if ( present(spin) )  this.spin = spin
		if ( present(isQuantum) ) this.isQuantum = isQuantum
		if ( present(isDummy) ) this.isDummy = isDummy
		if ( present(name) ) this.name=trim(name)
		if ( present(symbol) ) this.symbol=trim(symbol)
		if ( present(owner) ) this.owner=owner
		this.id = this.owner
		if ( present(basisSetName) ) this.basisSetName = trim(basisSetName)
		if ( present(nickname) ) this.nickname=trim(nickname)
                if ( present(fragmentNumber) ) this.fragmentNumber = fragmentNumber

		!!*******************************************************************
		!! Construye y localiza la contracciones asociadas a la particula.
		!!
		if ( .not.present(basisSetName) .and.(this.isQuantum==.true.) ) then 
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object Particle in the constructor function" )
			call Exception_setDescription( ex, "The basis set for this particle wasn't specified" )
			call Exception_show( ex )
					
		else if (this.isQuantum==.true. .and. present(basisSetName) ) then
			
			if ( present(massNumber) ) then
				massNumberString = adjustl( String_convertIntegerToString( massNumber ) )
				call BasisSetManager_loadBasisSet ( this.basis, trim(basisSetName), trim(elementName)//"_"//trim(massNumberString))
			else
				call BasisSetManager_loadBasisSet ( this.basis, trim(basisSetName), trim(elementName))
			end if
			
			
			this.basisSetSize = this.basis.length
			
			call BasisSet_set( this.basis, origin=this.origin, owner=this.owner)
	
			if(this.isDummy) this.isCenterOfOptimization = .false.
			
	
		end if
		!!*******************************************************************
		
	end subroutine Particle_constructor
	
	!**
	! @brief Define el constructor de copia para la clase
	!
	!**
	subroutine Particle_copyConstructor( this, otherThis )
		implicit none
		type(Particle), intent(inout) :: this
		type(Particle), intent(in) :: otherThis
		
		this.name = trim(otherThis.name)
		this.nickname = trim(otherThis.nickname)
		this.symbol = trim(otherThis.symbol)
		this.statistics = trim(otherThis.statistics)
		this.origin = otherThis.origin
		this.charge = otherThis.charge
		this.mass = otherThis.mass
		this.spin = otherThis.spin
		this.totalCharge = otherThis.totalCharge
		this.isQuantum = otherThis.isQuantum
		this.isDummy = otherThis.isDummy
		this.fixComponent = otherThis.fixComponent
		this.id = otherThis.id
		this.internalSize = otherThis.internalSize
                this.fragmentNumber = otherThis.fragmentNumber
		this.owner = otherThis.owner
		this.isCenterOfOptimization = otherThis.isCenterOfOptimization
		
		if( allocated(otherThis.childs) ) then
			allocate( this.childs( size(otherThis.childs ) ) )
			this.childs= otherThis.childs
		end if
		
		this.basisSetName = ""
		this.basisSetSize = 0
		
		if ( allocated( otherThis.basis.contractions ) ) then
		
			this.basisSetName = trim(otherThis.basisSetName)
			this.basisSetSize = otherThis.basisSetSize
			call BasisSet_copyConstructor( this.basis, otherThis.basis )
			
		end if
	
	end subroutine Particle_copyConstructor
	
	
	!**
	! Define el metodo para el destructor de la clase.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	subroutine Particle_destructor(this)
		implicit none
		type(Particle), intent(inout) :: this
		
		
		if ( this.isQuantum ==.true. ) then
			call BasisSet_destructor( this.basis )
		end if
			
		if( allocated(this.childs) ) then
			deallocate( this.childs )
		end if
			
	end subroutine Particle_destructor
	
	
	!**
	! Muestra los atributos de la particula dada (this).
	!
	! @param this Particula cuantica o puntual.
	!**
	subroutine Particle_show( this )
		implicit none
		type(Particle) , intent(in) :: this
		integer :: i
		
		if ( this.isQuantum ) then
			print *," Quantum particle"
		else
			print *," Puntual particle"
		end if
		
		print *,"================="
		print *,""
		write (6,"(T10,A16,A8)")		"Name          : ",trim( this.name )
		write (6,"(T10,A16,A8)")		"Symbol        : ",trim( this.symbol )
		write (6,"(T10,A16,I8)") 	"Particle ID   : ",this.id
		write (6,"(T10,A16,I8)") 	"Owner         : ",this.owner
		write (6,"(T10,A16,F8.2)") 	"Charge        : ",this.charge
		write (6,"(T10,A16,F8.2)") 	"Mass          : ",this.mass
		write (6,"(T10,A16,F8.2)") 	"Spin          : ",this.spin
		write (6,"(T10,A16,F8.2)") 	"Total charge  : ",this.totalCharge
		write (6,"(T10,A16,I8)") 	"Internal size : ",this.internalSize
                write (6,"(T10,A16,I8)")        "Frament number: ",this.fragmentNumber
		write (6,"(T10,A16,L8)") 		"Is dummy      : ",this.isDummy
		write (6,"(T10,A16,A8)")		"Statistics    : ",trim( this.statistics )
		if ( this.isQuantum ) then
			write (6,"(T10,A16,A8)")		"Basis set name: ",trim( this.basisSetName )
			write (6,"(T10,A16,I8)")		"Basis set size: ", this.basisSetSize
		end if
		write (6,"(T10,A16,F8.2,F8.2,F8.2)") 	"Origin        : ",this.origin(1),this.origin(2),this.origin(3)
		
		if ( allocated(this.childs) ) &
		write (6,"(T10,A16,<size(this.childs)>I8)") 	"Childs        : ",( this.childs(i),i=1,size(this.childs) )
		print *,""	
		
	end subroutine Particle_show
	
	!**
	! Muestra los atributos de una lista de particulas
	!
	! @param this lista de particulas
	!**
	subroutine Particle_showStructure( this, numberContraction, numberGaussian )
		implicit none
		type(Particle) , intent(in) :: this
		
		integer , optional , intent(in) :: numberContraction
		integer , optional , intent(in) :: numberGaussian

		call Particle_show( this )
		
		if ( this.isQuantum	) then
			
			print *,""
			print *,"INFORMATION OF BASIS SET: "
			
			call BasisSet_show( this.basis)
			
		end if


	end subroutine Particle_showStructure


	!**
	! Devuelve un apuntador a una particula solicitada.
	!
	! @param this Lista de particulas
	!**
	function Particle_getParticlePtr(this) result(output)
		implicit none
		type(Particle) , target :: this
		type(Particle) , pointer :: output
		
		!! Asegura que el apuntador no se encuentre asociado
!!		output => null() 
		
		!! Devuelve el apuntador a la particula solicitada.
		output => this
	
	end function Particle_getParticlePtr
	
	!**
	! Devuelve el name asignado a una particula solicitada.
	!
	! @param this Lista de particulas
	!**
	function Particle_getName(this) result(output) 
		implicit none
		type(Particle) , intent(in) :: this
		character(30) :: output
		
		output = trim( this.name )
	
	end function Particle_getName
	
	!**
	! Devuelve el indice asignado a la particula
	!
	!**
	function Particle_getOwner(this) result(output) 
		implicit none
		type(Particle) , intent(in) :: this
		integer :: output
		
		output= this.owner
		
	end function Particle_getOwner
	
	
	!**
	! Devuelve la componente cartesiana del origen de la particula considerada
	!
	! @param this Particula cuantica o carga puntual.
	! @param component componente x, y, o z del origen.
	!**
	function Particle_getOrigin(this) result(output)
		implicit none
		type(Particle) , target :: this
		real(8) :: output(3)
		
		output = this.origin
	
	end function Particle_getOrigin
	
	
	!**
	! Devuelve la componente cartesiana del origen de la particula considerada
	!
	! @param this Particula cuantica o carga puntual.
	! @param component componente x, y, o z del origen.
	!**
	function Particle_getOriginComponent(this,component) result(output)
		implicit none
		type(Particle) , target :: this
		integer :: component
		real(8) :: output
		
		output = this.origin(component)
	
	end function Particle_getOriginComponent
	
	
	!**
	! Devuelve el comportamiento asignado a la particula, (TRUE/FALSE).
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_isQuantum(this) result(output)
		implicit none
		type(Particle) , intent(in) :: this
		logical :: output
		
		output = this.isQuantum
	
	end function Particle_isQuantum
		
	!**
	! Devuelve la masa de la particula solicitada.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_getMass(this) result(output)
		implicit none
		type(Particle) , intent(in) :: this
		real(8) :: output
		
		output = this.mass 
	
	end function Particle_getMass
	
	!**
	! Devuelve la carga de la particula solicitada
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_getCharge( this ) result(output)
		implicit none
		type(Particle) , intent(in) :: this
		real(8) :: output
	
		output = this.charge
		
	end function Particle_getCharge
	
	
	!**
	! Devuelve el espin de la particula solicitada.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_getSpin( this ) result( output )
		implicit none
		type(Particle) , intent(in) :: this
		real(8) :: output
		
		if (this.isQuantum == .TRUE.) then
			
			output=this.spin
		end if
		  
	end function Particle_getSpin
	
	!**
	! Devuelve el identificador unico de la particula solicitada.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_getId( this ) result( output )
		implicit none
		type(Particle) , intent(in) :: this
		integer output
		
		output=this.id

	end function Particle_getId
	
	
	!**
	! Devuelve el numero de contracciones asociadas a una particula.
	!
	! @param this Particula cuantica o carga puntual.
	!**
	function Particle_getNumberOfContractions( this ) result( output )
		implicit none
		type(Particle) , intent(in) :: this
		integer :: output
		
		output = 0
 		
		if ( this.isQuantum == .true.)  then
		
 			output = this.basisSetSize
			
 		end if
		
	end function Particle_getNumberOfContractions
	
	!**
	! Devuelve un apuntador asociado a una contraccion, de una particula cuantica.
	!
	! @param this Particula cuantica.
	!**
	function Particle_getContractionPtr( this, id ) result( output )
		implicit none
		type(Particle), target, intent(inout) :: this
		integer :: id
		type(ContractedGaussian) , pointer :: output
		type(Exception) :: ex
	
	
		if ( this.isQuantum ) then
			
!!			output => null()
			
			if ( allocated(this.basis.contractions) ) then
				
				output => this.basis.contractions(id)
			
			else
						
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object Particle in the getContractionPtr function" )
				call Exception_setDescription( ex, "This particle hasn't a basis set assigned" )
				call Exception_show( ex )

		
			end if
			
			
		else
						
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object Particle in the getContractionPtr function" )
			call Exception_setDescription( ex, "This isn't quantum particle and hasn't a basis set assigned" )
			call Exception_show( ex )

		
		end if
		
	
	end function Particle_getContractionPtr
	
	
	!**
	! Indica si la coordenada especificada se ha definido como variable
	! o como parametro.
	!
	! @param this Particula cuantica o carga puntual.
	! @param component Componente cartesiana x=1, y=2, z =3.
	!**
	function Particle_hasFixedComponent(this , component) result( output )
		implicit none
		type(Particle) , intent(inout) :: this
		integer , intent(in) :: component
		logical :: output
		
		output = this.fixComponent(component)
		
	end function Particle_hasFixedComponent
	
	!**
	! Ajusta el origen de la particula especificada, o el origen de una
	! unica  contraccion o gaussina especificadas.
	!
	! @param this Particula cuantica o carga puntual.
	! @param origin Origen de la particula o contraccion especificada.
	! @param contractionNumber Numero de contraccion dentro de una lista.
	! @param gaussianNumber Numero de gausiana dentro de una lista.
	!**
	recursive subroutine Particle_setOrigin( this,origin, contractionID, gaussianID )
		implicit none
		type(Particle)  :: this
		real(8)  :: origin(3)
		integer , optional , intent(in) :: contractionID
		integer , optional , intent(in) :: gaussianID
		
		type(Particle), external :: particlemanager__mp_particlemanager_getparticleptr
		integer :: i 
		
		this.origin = origin
		
		
		if (this.isQuantum == .true.) then

			if ( present(contractionID) ) then
			
				!! Ajusta el origen de la gausiana especificada
				if ( present(gaussianID) ) then  
					
					call ContractedGaussian_set( this.basis.contractions(contractionID) ,&
						origin = this.origin, gaussianID = gaussianID )
						
				else 
				
					!! Ajusta el origen de una contraccin especificada.
					call ContractedGaussian_set(this.basis.contractions(contractionID) ,&
						origin=this.origin )
				end if
				
			else
			
				call BasisSet_setOrigin( this.basis, origin=this.origin )
				
			end if
			
		end if
		
		if ( this.isCenterOfOptimization  .and. allocated(this.childs) ) then
		
			do i=1, size(this.childs)
!!			 	call Particle_setOrigin( particlemanager__mp_particlemanager_getparticleptr( this.childs(i) ), this.origin )
			end do
		
		end if
	
	end subroutine Particle_setOrigin
	
	!**
	! Ajusta el propieario de la particula especificada.
	! @param this Particula cuantica o carga puntual.
	!**
	subroutine Particle_setOwner( this, owner )
		implicit none
		type(Particle) , intent(inout) :: this
		integer  :: owner
		
		if (this.isQuantum == .true.) then

			call BasisSet_set( this.basis, owner=owner ) 
			this.owner = owner
				
		else
			
			this.owner = owner
			
		end if
	
	end subroutine Particle_setOwner
	
	!**
	! Ajusta el id de las particulas hijas.
	! @param this Particula cuantica o carga puntual.
	!**
	subroutine Particle_setChild( this, child )
		implicit none
		type(Particle) , intent(inout) :: this
		integer  :: child
		
		integer, allocatable :: auxChilds(:)
		integer :: ssize
		
		
		if ( allocated(this.childs) ) then
			ssize =size( this.childs )
			allocate( auxChilds( ssize  ) )
			auxChilds=this.childs
			deallocate(this.childs)
			allocate( this.childs( ssize +1  ) )
			this.childs(1:ssize)=auxChilds
			this.childs(ssize+1)=child
			deallocate(auxChilds)
		else
			allocate( this.childs(1))
			this.childs(1) = child
		end if
	
	end subroutine Particle_setChild


	!**
	! Remueve los Id de las particulas hijas
	! @param this Particula cuantica o carga puntual.
	!**
	subroutine Particle_removeChilds( this )
		implicit none
		type(Particle) , intent(inout) :: this
		
		
		if ( allocated(this.childs) ) then
			deallocate(this.childs)
		end if
	
	end subroutine Particle_removeChilds

	
	!**
	! Permiter ajustar la componente de una particula como parametro o como
	! variable durante la optimizacion de geometria. Por defecto lo
	! ajusta como parametro.
	!
	! @param this Particula cuyo atributo se desea modificar
	! @param component Componente cartesiana que se fija como parametro
	!**
	subroutine Particle_setComponentFixed(this , component)
		implicit none
		type(Particle) , intent(inout) :: this
		character(*) , intent(in) :: component
						
		select case ( trim( component ) )

			!!
			!! Ajusta componente x como parametro
			!!			
			case ("x")   
				this.fixComponent(1)= .true.
				
			!!
			!! Ajusta componente y como parametro
			!!			
			case ("y")   
				this.fixComponent(2)= .true.
				
			!!
			!! Ajusta componente z como parametro
			!!			
			case ("z")   
				this.fixComponent(3)= .true.
			
			!!
			!! Ajusta componente x como parametro
			!!			
			case ("xy")   
				this.fixComponent(1)= .true.
				this.fixComponent(2)= .true.
			
			!!
			!! Ajusta componente x como parametro
			!!			
			case ("xz")   
				this.fixComponent(1)= .true.
				this.fixComponent(3)= .true.
			
			!!
			!! Ajusta componente x como parametro
			!!			
			case ("yz")   
				this.fixComponent(2)= .true.
				this.fixComponent(3)= .true.
			
			!!
			!! Ajusta todas las componente cartesianas como parametro
			!!			
			case ("xyz")   
				this.fixComponent= .true.
			
			!!
			!! Deja que las coordenadas de la particula cambien durante
			!! la optimizacion de geometria.
			!!
			case default
				this.fixComponent= .false.
				
		end select
		
	end subroutine Particle_setComponentFixed

end module Particle_	