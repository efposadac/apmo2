!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************
!**
! @brief Modulo para definicoion de funciones de onda RHF
! 
!  Este modulo define funciones de onda del tipo "Restricted Hartree-Fock"
!  (RHF) para diferentes especies cuanticas dentro del formalismo OMNE
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-30
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!
!**
module RHFWaveFunction_
	use Matrix_
	use Vector_
	use ParticleManager_
	use IntegralManager_
	use omp_lib 
	use ExternalPotentialManager_
	use ExternalPotential_
	use ContractedGaussian_
	use String_
	use InputOutput_
	implicit none
	
	
	!< enum Matrix_type {
	integer, parameter :: CANONICAL_ORTHOGONALIZATION	= 1
	integer, parameter :: SYMMETRIC_ORTHOGONALIZATION	= 2
	!< }

	!< enum type of orbital graph {
	integer, parameter, public :: ORBITAL_ALONE = 1
	integer, parameter, public :: ORBITAL_WITH_POTENTIAL = 2
	!< }

	
	type, public :: RHFWaveFunction
		
		character(30) :: name
		
		!!**************************************************************
		!! Matrices asociadas al calculo de funciones de onda RHF
		!!
		type(Matrix) :: waveFunctionCoefficients
		type(Vector) :: molecularOrbitalsEnergy 
		type(Matrix) :: kineticMatrix
		type(Matrix) :: independentParticleMatrix
		type(Matrix) :: puntualParticleMatrix
		type(Matrix) :: densityMatrix 
		type(Matrix) :: transformationMatrix
		type(Matrix) :: twoParticlesMatrix
		type(Matrix) :: couplingMatrix
		type(Matrix) :: externalPotentialMatrix
		type(Matrix) :: fockMatrix
		
		type(Matrix), pointer :: kineticMatrixValuesPtr
		type(Matrix), pointer :: puntualInteractionMatrixValuesPtr
		type(Matrix), pointer :: overlapMatrixValuesPtr
		!!**************************************************************
		
	
		!!**************************************************************
		!! Atributos asociados a los valores de energia al final de 
		!! la funcion de onda RHF
		real(8) :: totalEnergyForSpecie
		real(8) :: independentSpecieEnergy
		real(8) :: kineticEnergy
		real(8) :: puntualInteractionEnergy
		real(8) :: independentParticleEnergy
		real(8) :: repulsionEnergy
		real(8) :: couplingEnergy
		real(8) :: externalPotentialEnergy
		!!**************************************************************
		
		!!**************************************************************
		!!  Variables requeridas por conveniencia
		!!
		logical :: addTwoParticlesMatrix !< Variable por conveniencia
		logical :: addCouplingMatrix 	!< Variable por conveniencia
		logical :: wasBuiltFockMatrix 	!< Variable por conveniencia
		logical :: isThereExternalPotential	!< Variable por conveniencia
		!!**************************************************************
		
	end type RHFWaveFunction
	

	private ::
		type(matrix) :: auxMatrix
	
	
	type(RHFWaveFunction), public, allocatable, target :: RHFWaveFunction_instance(:)
	
	
	public :: &
		RHFWaveFunction_constructor, &
		RHFWaveFunction_destructor, &
		RHFWaveFunction_show, &
		RHFWaveFunction_setDensityMatrix, &
		RHFWaveFunction_buildOverlapMatrix, &
		RHFWaveFunction_buildTransformationMatrix, &
		RHFWaveFunction_buildIndependentParticleMatrix, &
		RHFWaveFunction_buildPuntualParticleMatrix, &
		RHFWaveFunction_buildTwoParticlesMatrix, &
		RHFWaveFunction_buildCouplingMatrix, &
		RHFWaveFunction_buildExternalPotentialMatrix, &
		RHFWaveFunction_buildFockMatrix, &
		RHFWaveFunction_builtDensityMatrix, &
		RHFWaveFunction_obtainTotalEnergyForSpecie,&
		RHFWaveFunction_obtainEnergyComponents, &
		RHFWaveFunction_isInstanced, &		
		RHFWaveFunction_getOverlapMatrix, &
		RHFWaveFunction_getTransformationMatrix, &
		RHFWaveFunction_getIndependentParticleMatrix, &
		RHFWaveFunction_getTwoParticlesMatrix, &
		RHFWaveFunction_getCouplingMatrix, &
		RHFWaveFunction_getPuntualParticleMatrix, &
		RHFWaveFunction_getExternalPotentialMatrix, &
		RHFWaveFunction_getFockMatrix, &
		RHFWaveFunction_getDensityMatrix, &
		RHFWaveFunction_getDensityMatrixPtr, &
		RHFWaveFunction_getKineticMatrix, &
		RHFWaveFunction_getWaveFunctionCoefficients,&
		RHFWaveFunction_getWaveFunctionCoefficientsPtr,&
		RHFWaveFunction_getMolecularOrbitalsEnergy, &
		RHFWaveFunction_getMolecularOrbitalsEnergyPtr, &
		RHFWaveFunction_getNameOfSpeciesPtr,&
		RHFWaveFunction_getKineticEnergyPtr, &
		RHFWaveFunction_getRepulsionEnergyPtr, &
		RHFWaveFunction_getCouplingEnergyPtr, &
		RHFWaveFunction_getQuantPuntualEnergyPtr, &
		RHFWaveFunction_getEnergy, &
		RHFWaveFunction_getValueForOrbitalAt, &
		RHFWaveFunction_draw2DOrbital, &
		RHFWaveFunction_getTotalCoupligEnergy, &
		RHFWaveFunction_reset
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine RHFWaveFunction_constructor( )
		implicit none
				
		integer :: speciesIterator
		integer :: specieID
		integer(8) :: numberOfContractions
		character(30) :: specieName
		
		allocate( RHFWaveFunction_instance( ParticleManager_getNumberOfQuantumSpecies() ) )
		
		RHFWaveFunction_instance(:).isThereExternalPotential = .false.
		if( externalPotentialManager_instance.isInstanced ) &
		RHFWaveFunction_instance(:).isThereExternalPotential = .true.
		
		do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()

			
			specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
			numberOfContractions =  ParticleManager_getTotalNumberOfContractions( specieID )

			RHFWaveFunction_instance( specieID ).name = trim( ParticleManager_getNameOfSpecie( specieID) )
			RHFWaveFunction_instance( specieID ).totalEnergyForSpecie = 0.0_8
			RHFWaveFunction_instance( specieID ).independentSpecieEnergy =0.0_8
			RHFWaveFunction_instance( specieID ).kineticEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).puntualInteractionEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).independentParticleEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).repulsionEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).couplingEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix = .false.
			RHFWaveFunction_instance( specieID ).addCouplingMatrix = .false.
			RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .false.
			
			call Matrix_constructor( RHFWaveFunction_instance(specieID).waveFunctionCoefficients,numberOfContractions, &
				numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).kineticMatrix,numberOfContractions, &
				numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).puntualParticleMatrix,numberOfContractions, &
				numberOfContractions )
			call Vector_constructor( RHFWaveFunction_instance(specieID).molecularOrbitalsEnergy, int(numberOfContractions) )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).independentParticleMatrix, numberOfContractions, &
				numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).densityMatrix, numberOfContractions, numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).transformationMatrix, numberOfContractions, numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).twoParticlesMatrix, numberOfContractions, numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).couplingMatrix, numberOfContractions, numberOfContractions )
			
			if( RHFWaveFunction_instance(specieID).isThereExternalPotential) &
			call Matrix_constructor( RHFWaveFunction_instance(specieID).externalPotentialMatrix, numberOfContractions, numberOfContractions )
			call Matrix_constructor( RHFWaveFunction_instance(specieID).fockMatrix, numberOfContractions, numberOfContractions )
			
		end do
			
	end subroutine RHFWaveFunction_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine RHFWaveFunction_destructor( )
		implicit none
				
		integer :: speciesIterator
		integer :: specieID
				
		if ( allocated( RHFWaveFunction_instance) ) then
		
		
			do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
				
				specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
				
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).waveFunctionCoefficients )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).kineticMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).puntualParticleMatrix )
				call Vector_destructor( RHFWaveFunction_instance( specieID ).molecularOrbitalsEnergy ) 
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).independentParticleMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).densityMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).transformationMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).couplingMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).externalPotentialMatrix )
				call Matrix_destructor( RHFWaveFunction_instance( specieID ).fockMatrix )
				
			
				RHFWaveFunction_instance( specieID ).kineticMatrixValuesPtr => null()
				RHFWaveFunction_instance( specieID ).puntualInteractionMatrixValuesPtr => null()
				RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr => null()
				
			end do
			
			deallocate( RHFWaveFunction_instance )
	
		end if

	end subroutine RHFWaveFunction_destructor
	
	!**
	! @brief 	Retorna el numero de iteraciones realizadas para una especie especificada
	!
	!**
	subroutine RHFWaveFunction_show( nameOfSpecie ) 
		implicit none
		character(*), optional,  intent(in) :: nameOfSpecie

		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		call Matrix_show( RHFWaveFunction_instance( specieID ).waveFunctionCoefficients )

	end subroutine RHFWaveFunction_show

	
	!**
	! @brief 	Ajusta la matriz de densidad para una especie espcificada
	!
	!**
	subroutine RHFWaveFunction_setDensityMatrix( densityMatrix, nameOfSpecie ) 
		implicit none
		type(Matrix), intent(in) :: densityMatrix
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		
		call Matrix_copyConstructor( RHFWaveFunction_instance( specieID ).densityMatrix, densityMatrix )
		
!		print*, "Matriz de densidad inicial"
!		call Matrix_show(RHFWaveFunction_instance( specieID ).densityMatrix)

	end subroutine RHFWaveFunction_setDensityMatrix
	
	
	!**
	! @brief Contruye la matrix de overlap.
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	subroutine RHFWaveFunction_buildOverlapMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
	
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( .not.associated( RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr ) ) then

			call IntegralManager_buildMatrix( OVERLAP_INTEGRALS, nameOfSpecieSelected  )
			
			RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr =>  IntegralManager_getMatrixPtr( OVERLAP_INTEGRALS, &
				nameOfSpecieSelected )

!		print *,"Matriz de overlap: ",trim(nameOfSpecieSelected)
!		call Matrix_show(RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr)
		
		end if
		
		!! Parte para proprositos de prueba
!   	       if( trim(nameOfSpecieSelected)=="H_1" ) then
! 		    RHFWaveFunction_instance( specieID ).waveFunctionCoefficients=&
! 		    InputOutput_getMatrix(20, 20, file="/home/edwin/Calculos/Prueba/geomp2_hf.cofs")
! 		    RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values=&
! 		    transpose(RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values)
! 		    print *,"Matriz de coeficientes: ",trim(nameOfSpecieSelected)
! 			call Matrix_show(RHFWaveFunction_instance( specieID ).waveFunctionCoefficients)
! 		    
! 		    call RHFWaveFunction_draw2DOrbital( trim(nameOfSpecieSelected),1,ORBITAL_ALONE )
! 		    call abort("")
! 	       end if


	end subroutine RHFWaveFunction_buildOverlapMatrix
	

	!**
	! @brief Contruye la matrix de de transformacion.
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	subroutine RHFWaveFunction_buildTransformationMatrix( nameOfSpecie, typeOfOrthogonalization ) 
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie
		integer, optional, intent(in) :: typeOfOrthogonalization
		
		type(Matrix) :: eigenVectors
		type(Vector) :: eigenValues
		character(30) :: nameOfSpecieSelected
		integer(8) :: numberOfContractions
		integer :: i
		integer :: j
		integer :: specieID

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected) )

		if ( .not.associated( RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr ) ) then

			call RHFWaveFunction_buildOverlapMatrix( nameOfSpecie )

		end if

		numberOfContractions = ParticleManager_getTotalnumberOfContractions( specieID )
		
		if ( numberOfContractions > 1) then
			

			call Vector_constructor( eigenValues, int(numberOfContractions) )

			call Matrix_constructor( eigenVectors, numberOfContractions, numberOfContractions )

			!!****************************************************************
			!! diagonaliza la matriz de overlap obteniendo una matriz unitaria
			!!
		
			call Matrix_eigen( RHFWaveFunction_instance( specieID ).overlapMatrixValuesPtr, eigenValues, eigenVectors, SYMMETRIC  )
	
			do i = 1 , numberOfContractions
				do j = 1 , numberOfContractions
					RHFWaveFunction_instance( specieID ).transformationMatrix.values(i,j) = &
								eigenVectors.values(i,j)/sqrt( eigenValues.values(j) )

				end do
			end do

			!!****************************************************************

			!!****************************************************************
			!! Calcula matriz de transformacion
			!!
			select case (typeOfOrthogonalization)
		
				!! Ortogonalizacion canonica
				case (CANONICAL_ORTHOGONALIZATION)
					
					RHFWaveFunction_instance( specieID ).transformationMatrix.values = &
						RHFWaveFunction_instance( specieID ).transformationMatrix.values
		
				!!Ortogonalizacion simetrica
				case (SYMMETRIC_ORTHOGONALIZATION)
				
					RHFWaveFunction_instance( specieID ).transformationMatrix.values  = &
							matmul(RHFWaveFunction_instance( specieID ).transformationMatrix.values, transpose(eigenVectors.values))
										
				case default
	
					RHFWaveFunction_instance( specieID ).transformationMatrix.values  = &
							matmul(RHFWaveFunction_instance( specieID ).transformationMatrix.values, transpose(eigenVectors.values))
			
			end select
			!!****************************************************************
		
		else
				
			RHFWaveFunction_instance( specieID ).transformationMatrix.values= 1.0_8
		end if
		
		!print *,"Matriz de transformacion: ", trim(nameOfSpecieSelected)
		!call Matrix_show( RHFWaveFunction_instance( specieID ).transformationMatrix )

	end subroutine RHFWaveFunction_buildTransformationMatrix

	!**
	! @brief Contruye la matrix de particula independiente.
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	subroutine RHFWaveFunction_buildIndependentParticleMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( .not.associated(RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr ) ) then
		
			call IntegralManager_buildMatrix( KINETIC_INTEGRALS, trim(nameOfSpecieSelected  ) )
			
			RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr => &
					IntegralManager_getMatrixPtr( KINETIC_INTEGRALS,  nameOfSpecieSelected )
			
			RHFWaveFunction_instance(specieID).kineticMatrix = RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr
			
! 			print *,"Matriz de energía cinética", trim(nameOfSpecieSelected), specieID
! 			call Matrix_show( RHFWaveFunction_instance(specieID).kineticMatrix )

			call IntegralManager_buildMatrix( ATTRACTION_INTEGRALS, trim(nameOfSpecieSelected  ) )

			RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr => 	&
				IntegralManager_getMatrixPtr(ATTRACTION_INTEGRALS, nameOfSpecieSelected)

			!print *,"Matriz de interaccion n-e"
			!call Matrix_show( RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr )

			if ( APMO_instance.REMOVE_TRANSLATIONAL_CONTAMINATION ) then

				RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr.values = &
					( 1.0_8 - ( ParticleManager_getMass( specieID ) &
					/ ParticleManager_getTotalMass() ) ) * RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr.values

			end if
			
			RHFWaveFunction_instance(specieID).independentParticleMatrix.values = &
				RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr.values + &
				RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr.values

			!print *,"Matriz de hcore"
			!call Matrix_show( RHFWaveFunction_instance( specieID ).independentParticleMatrix )

			RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .false.
		
		end if

	
	end subroutine RHFWaveFunction_buildIndependentParticleMatrix	
	



	subroutine RHFWaveFunction_buildPuntualParticleMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie

		character(30) :: nameOfSpecieSelected
		integer :: specieID

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )

		call IntegralManager_buildMatrix( ATTRACTION_INTEGRALS, trim(nameOfSpecieSelected  ) )

		RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr => 	&
			IntegralManager_getMatrixPtr(ATTRACTION_INTEGRALS, nameOfSpecieSelected)

		!print *,"Matriz de interaccion n-e"
		!call Matrix_show( RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr )


		RHFWaveFunction_instance(specieID).puntualParticleMatrix.values = &
			RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr.values

	end subroutine RHFWaveFunction_buildPuntualParticleMatrix


	!**
	! @brief 	Contruye la matrix de dos particulas de la especie especificada (G)
	! 		la matrix resultante sera tenida en cuenta en la construccion de la matriz de Fock
	!
	!**
	subroutine RHFWaveFunction_buildTwoParticlesMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie

		type(Exception) :: ex
		character(30) :: nameOfSpecieSelected
		real(8) :: integralValue
		real(8) :: coulomb,exchange
		real(8) :: factor
		integer :: specieID
		integer, target :: numberOfContractions
		integer, pointer :: indexPointer
		integer, target :: u
		integer :: v
		integer :: k
		integer :: l
		
		integer, target :: buff(4)
		integer, pointer :: P1,P2
		
		integer::errorValue
		integer(8) :: auxIndex
		logical :: hadAdded
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		hadAdded=.false.

		if ( RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix ) return

		!! Solo calcula matriz de dos particulas cuando existen dos o mas de ellas para una especie dada
		if ( ParticleManager_getNumberOfParticles( specieID ) > 1 ) then
		
			!! Verifica la existencia de una matriz de densidad para la especie especificada
			if ( allocated( RHFWaveFunction_instance( specieID).densityMatrix.values ) ) then
				
				
				RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values = 0.0_8
				numberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )
				factor = ParticleManager_getFactorOfInterchangeIntegrals( specieID )

				open(unit=34, FILE=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".ints",&
					status='old',access='sequential', form='unformatted' )

				do
					read(UNIT=34,IOSTAT=errorValue) buff(1),buff(2), buff(3),buff(4),integralValue

					if (buff(1)<0) exit
                                        
					coulomb = RHFWaveFunction_instance( specieID).densityMatrix.values(buff(3),buff(4))*integralValue

					!!*****************************************************************************
					!! 	Adiciona aportes debidos al operador de coulomb
					!!*****
					if( buff(1) ==buff(3) .and. buff(2) ==buff(4) ) then

						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) + coulomb

						if( buff(3) /= buff(4) ) then
							
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) + coulomb

						end if 
					else
						
						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) + coulomb

						if( buff(3) /= buff(4)) then
							
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(1),buff(2)) + coulomb

						end if 

						coulomb = RHFWaveFunction_instance(specieID).densityMatrix.values(buff(1),buff(2))*integralValue

						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(3),buff(4)) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(buff(3),buff(4)) + coulomb

						if ( buff(1) /= buff(2) ) then
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(3), buff(4) ) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(3), buff(4)) + coulomb
						end if

					end if
					!!
					!!*****************************************************************************

					!!*****************************************************************************
					!! 	Adiciona aportes debidos al operador de intercambio
					!!*****
					if( buff(3) /= buff(4) ) then

						exchange =RHFWaveFunction_instance( specieID).densityMatrix.values(buff(2),buff(4))*integralValue * factor
						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(3) ) = &
							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(3)) + exchange

						if( buff(1) == buff(3) .and. buff(2) /= buff(4) ) then

							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(3) ) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(3)) + exchange

						end if

					end if

					if ( buff(1) /= buff(2) ) then
			
						exchange=RHFWaveFunction_instance( specieID).densityMatrix.values(buff(1),buff(3))*integralValue * factor
						
						if( buff(2) > buff(4) ) then

							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(4), buff(2) ) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(4), buff(2)) + exchange

						else

							RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(4) ) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(4)) + exchange

							if( buff(2)==buff(4) .and. buff(1)/= buff(3) ) then

								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(4) ) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(4)) + exchange

							end if

						end if

						if ( buff(3) /= buff(4) ) then

							exchange=RHFWaveFunction_instance( specieID).densityMatrix.values(buff(1),buff(4))*integralValue * factor

							if( buff(2) <= buff(3) ) then
	
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(3) ) = &
									RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(3)) + exchange

								if( buff(2) == buff(3) ) then

									RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(3) ) = &
									RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(2), buff(3)) + exchange

								end if

							else

								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(3), buff(2) ) = &
									RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(3), buff(2)) + exchange

								if( buff(1) == buff(3) .and. buff(4) == buff(2) ) goto 20

							end if

						end if

					end if
					
					
						exchange =RHFWaveFunction_instance( specieID).densityMatrix.values(buff(2),buff(3))*integralValue * factor
						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(4) ) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values( buff(1), buff(4)) + exchange
20					continue

				!!
				!!*****************************************************************************

				end do
				P1=>null()
				P2=>null()

				close(34)

				!! Simetriza la matriz de Fock
				do u = 1 , numberOfContractions
					do v = u , numberOfContractions
						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(v,u) = &
								RHFWaveFunction_instance(specieID).twoParticlesMatrix.values(u,v)
					end do	
				end do

				RHFWaveFunction_instance(specieID).twoParticlesMatrix.values=&
						RHFWaveFunction_instance(specieID).twoParticlesMatrix.values &
						* ( ParticleManager_getCharge(specieID=specieID ) )**2.0_8
	
!				print*, "matriz de dos particulas"
!				call matrix_show(RHFWaveFunction_instance(specieID).twoParticlesMatrix)

				RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix =.true.
				RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .false.

			else
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object RHFWaveFunction in the builtTwoParticlesMatrix("// trim(nameOfSpecieSelected) //") function" )
				call Exception_setDescription( ex, "Density matrix for "// trim(nameOfSpecieSelected) //" specie, hasn't been defined." )
				call Exception_show( ex )
			
			end if
			
		else
		
			RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix =.false.
			RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .false.
		
		end if
		
	end subroutine RHFWaveFunction_buildTwoParticlesMatrix

	!**
	! @brief 	Contruye la matrix de acoplamineto para la especie especificada (C)
	! 		la matrix resultante sera tenida en cuenta en la construccion de la matriz de Fock
	!
	!**
	subroutine RHFWaveFunction_buildCouplingMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		type(Exception) :: ex
		character(30) :: nameOfSpecieSelected
		character(30) :: nameOfOtherSpecie
		integer :: currentSpecieID
		integer :: otherSpecieID
		integer :: speciesIterator
		integer :: ssize
		integer :: errorValue
		integer, target :: buff(4)
		integer :: i
		integer :: j
		real(8) :: integralValue
		real(8), allocatable :: auxMatrix(:,:)
		real(8) :: coulomb
		integer,pointer :: u
		integer,pointer :: v
		integer,pointer :: k
		integer,pointer :: l
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
			
		currentSpecieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		RHFWaveFunction_instance( currentSpecieID ).addCouplingMatrix =.false.
		
		if( ParticleManager_getNumberOfQuantumSpecies() > 1 ) then
		
			
			RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values = 0.0_8
			ssize = size(RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values,dim=1)
			allocate(auxMatrix(ssize, ssize))

			
			do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
				
				otherSpecieID = ParticleManager_getSpecieID( iteratorOfSpecie = speciesIterator )
				nameOfOtherSpecie = ParticleManager_getNameOfSpecie( otherSpecieID )
				
				!! Restringe suma de terminos repulsivos de la misma especie.
				if ( otherSpecieID /= currentSpecieID ) then
				
				
					if ( .not.allocated(RHFWaveFunction_instance( otherSpecieID).densityMatrix.values) ) then
				
						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object RHFWaveFunction in the builtCouplingMatrix(" &
														// trim(nameOfOtherSpecie) //") function" )
						call Exception_setDescription( ex, "Density matrix for "// trim(nameOfOtherSpecie) //" specie, hasn't been defined." )
						call Exception_show( ex )
					
					end if

					
					if( currentSpecieID > otherSpecieID) then

						open(unit=34, FILE=trim(APMO_instance.INPUT_FILE)//trim(nameOfOtherSpecie)//"."&
						//trim(nameOfSpecieSelected)//".ints", status='old',access='sequential', form='binary' )

						u=>buff(3)
						v=>buff(4)
						k=>buff(1)
						l=>buff(2)

					else

						open(unit=34, FILE=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecieSelected)//"."&
						//trim(nameOfOtherSpecie)//".ints", status='old',access='sequential', form='binary' )

						u=>buff(1)
						v=>buff(2)
						k=>buff(3)
						l=>buff(4)

					end if
					
					auxMatrix=0.0_8

					!call Matrix_show(RHFWaveFunction_instance( otherSpecieID ).densityMatrix)

					do 
						
						read(UNIT=34,IOSTAT=errorValue) buff(1),buff(2), buff(3),buff(4),integralValue

						if (buff(1)<0) exit


						coulomb = RHFWaveFunction_instance( otherSpecieID ).densityMatrix.values(k,l)*integralValue

						auxMatrix(u,v) = auxMatrix(u,v) + coulomb
						if( k /= l ) auxMatrix(u,v) = auxMatrix(u,v) + coulomb

					end do

					close(34)

					u=>null()
					v=>null()
					k=>null()
					l=>null()

					auxMatrix= auxMatrix * ParticleManager_getCharge( specieID=currentSpecieID ) &
						* ParticleManager_getCharge( specieID=otherSpecieID ) 
					
					RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values= &
						RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values + auxMatrix

				end if
				
			end do

			deallocate(auxMatrix)

			!! Simetriza la matriz de Acoplamineto
			do i = 1 , ssize
				do j = i , ssize
					RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values(j,i) = &
							RHFWaveFunction_instance( currentSpecieID ).couplingMatrix.values(i,j)
				end do	
			end do
			
			RHFWaveFunction_instance( currentSpecieID ).addCouplingMatrix =.true.
			RHFWaveFunction_instance( currentSpecieID ).wasBuiltFockMatrix = .false.
	
		end if
		
!		print *,"Matriz de acoplamiento: ", trim(nameOfSpecieSelected)
!		call Matrix_show( RHFWaveFunction_instance( currentSpecieID ).couplingMatrix )
		
		
	end subroutine RHFWaveFunction_buildCouplingMatrix

	!**
	! @brief Contruye una matriz de interaccion con un potencial externo
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	subroutine RHFWaveFunction_buildExternalPotentialMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
		

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )


		if( RHFWaveFunction_instance(specieID).isThereExternalPotential ) then	  

				RHFWaveFunction_instance(specieID).externalPotentialMatrix = &
				IntegralManager_getInteractionWithPotentialMatrix( &
				externalPotentialManager_instance.externalsPots, specieID )
			
						print *,"MATRIZ DE POTENCIAL EXTERNAL"
			call Matrix_show(RHFWaveFunction_instance(specieID).externalPotentialMatrix)

		end if
		

	
	end subroutine RHFWaveFunction_buildExternalPotentialMatrix

	!**
	! @brief Contruye la matrix de Fock para la especie especificada
	!
	!**
	subroutine RHFWaveFunction_buildFockMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		type(Exception) :: ex
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( allocated( RHFWaveFunction_instance(specieID).independentParticleMatrix.values ) ) then
		
			RHFWaveFunction_instance(specieID).fockMatrix.values = &
				RHFWaveFunction_instance(specieID).independentParticleMatrix.values
				
			if ( RHFWaveFunction_instance( specieID ).isThereExternalPotential ) then
			
				RHFWaveFunction_instance(specieID).fockMatrix.values = RHFWaveFunction_instance(specieID).fockMatrix.values + &
				RHFWaveFunction_instance( specieID ).externalPotentialMatrix.values
				!print *,"MATRIZ DE FOCK 1"
					!call Matrix_show(RHFWaveFunction_instance(specieID).fockMatrix)
			end if				

			if ( RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix ) then
			
				RHFWaveFunction_instance(specieID).fockMatrix.values = RHFWaveFunction_instance(specieID).fockMatrix.values + &
				RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values
				!print *,"MATRIZ DE FOCK 2"
				!call Matrix_show(RHFWaveFunction_instance(specieID).fockMatrix)
			end if
			
			if ( RHFWaveFunction_instance( specieID ).addCouplingMatrix ) then
			
				RHFWaveFunction_instance(specieID).fockMatrix.values = RHFWaveFunction_instance(specieID).fockMatrix.values + &
				RHFWaveFunction_instance( specieID ).couplingMatrix.values 
				!print *,"MATRIZ DE FOCK 3"
					!call Matrix_show(RHFWaveFunction_instance(specieID).fockMatrix)
			end if
			!print *,"MATRIZ DE FOCK"
			!call Matrix_show(RHFWaveFunction_instance(specieID).fockMatrix)
		else
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object RHFWaveFunction in the buidFockMatrix("//trim(nameOfSpecieSelected)//") function" )
			call Exception_setDescription( ex, "You must use at least buildIndependentParticleMatrix("//trim(nameOfSpecieSelected)//") before using this function" )
			call Exception_show( ex )		
		
		end if
		
		RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix =.false.
		RHFWaveFunction_instance( specieID ).addCouplingMatrix =.false.
		RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .true.
			
	end subroutine RHFWaveFunction_buildFockMatrix
	
	!**
	! @brief Calcula la matriz de densidad para una especie especificada
	!
	!**
	subroutine RHFWaveFunction_builtDensityMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
			
		character(30) :: nameOfSpecieSelected
		integer :: orderMatrix
		integer :: specieID
		integer :: ocupationNumber
		integer :: i
		integer :: j
		integer :: k

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		orderMatrix = size( RHFWaveFunction_instance( specieID ).densityMatrix.values, DIM = 1 ) 
		ocupationNumber = ParticleManager_getOcupationNumber( specieID )
		RHFWaveFunction_instance( specieID ).densityMatrix.values = 0.0_8
		
		do i = 1 , orderMatrix 
			do j = 1 , orderMatrix
				do k = 1 , ocupationNumber
					RHFWaveFunction_instance( specieID ).densityMatrix.values(i,j) = &
								RHFWaveFunction_instance( specieID ).densityMatrix.values( i,j ) & 
								+ (	RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values(i,k) &
								*  	RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values(j,k) )
				end do
			end do
		end do
	
		RHFWaveFunction_instance( specieID ).densityMatrix.values =  ParticleManager_getEta( specieID ) &
														* RHFWaveFunction_instance( specieID ).densityMatrix.values
		
				
	end subroutine RHFWaveFunction_builtDensityMatrix
	

	!**
	! @brief calcula la energia total para una especie especificada
	!
	!**					
	subroutine RHFWaveFunction_obtainTotalEnergyForSpecie( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		!! Recalcula matriz de dos particulas (G) Con la nueva matriz de densidad
		call RHFWaveFunction_buildTwoParticlesMatrix( nameOfSpecieSelected)


		if( .not.RHFWaveFunction_instance( specieID ).isThereExternalPotential ) then
			RHFWaveFunction_instance( specieID ).totalEnergyForSpecie = &
				sum(  transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values) &
				*  (  ( RHFWaveFunction_instance( specieID ).independentParticleMatrix.values ) &
				+ 0.5_8 *RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values &
				+ RHFWaveFunction_instance( specieID ).couplingMatrix.values	)  )
		else
		
			RHFWaveFunction_instance( specieID ).totalEnergyForSpecie = &
			sum(  transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values) &
			*  (  ( RHFWaveFunction_instance( specieID ).independentParticleMatrix.values ) &
			+ 0.5_8 *RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values &
			+ RHFWaveFunction_instance( specieID ).couplingMatrix.values &	
			+ RHFWaveFunction_instance( specieID ).externalPotentialMatrix.values )  )
		
		end if
				

		if (  APMO_instance.DEBUG_SCFS) then
			print *,""
			print *,"Total energy for "// trim(nameOfSpecieSelected) //"= ",RHFWaveFunction_instance( specieID ).totalEnergyForSpecie
		end if
	
	end subroutine RHFWaveFunction_obtainTotalEnergyForSpecie
	
	
		
	!**
	! @brief Calcula las componentes de energia para la especie especificada
	!
	! @warning 	Debe garantizarse el llamdo de esta funcion solo si previamente a llamado a 
	!			obtainTotalEnergyForSpecie
	!**
	subroutine RHFWaveFunction_obtainEnergyComponents( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
!		type(Matrix) :: auxMatrix
		
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

			specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )

! 		if( nameOfSpecieSelected == "e-") then
! 		    auxMatrix=IntegralManager_getInteractionBetweenQuantumClassicMtx(specieID,3)
! 		    print *,"ATRACION H-e:",sum( transpose( RHFWaveFunction_instance(specieID).densityMatrix.values )&
! 			 * auxMatrix.values )
! 		end if
		
		!! Calcula energia de repulsion para la especie dada
		RHFWaveFunction_instance( specieID ).repulsionEnergy= 0.5_8 * sum( transpose( RHFWaveFunction_instance( specieID ).densityMatrix.values ) &
			* RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values )
	
		!! Calcula energia de particula independiente
		RHFWaveFunction_instance( specieID ).independentParticleEnergy = sum( transpose( RHFWaveFunction_instance(specieID).densityMatrix.values )&
			* RHFWaveFunction_instance( specieID ).independentParticleMatrix.values )

		!! Calcula energia cinetica para la especie dada
		RHFWaveFunction_instance( specieID ).kineticEnergy = sum( transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values)  &
			* RHFWaveFunction_instance( specieID ).kineticMatrixValuesPtr.values ) 
		
		!! Calcula energia cinetica para la especie dada
		if( RHFWaveFunction_instance( specieID ).isThereExternalPotential ) &
			RHFWaveFunction_instance( specieID ).externalPotentialEnergy = sum( transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values)  &
			* RHFWaveFunction_instance( specieID ).externalPotentialMatrix.values ) 
		
		!! Calcula energia de interaccion entre particulas puntuales y cuanticas 
		RHFWaveFunction_instance( specieID ).puntualInteractionEnergy =  RHFWaveFunction_instance( specieID ).independentParticleEnergy &
		- RHFWaveFunction_instance( specieID ).kineticEnergy
	
		!! Calula enegia de especie independiente (  sin considerar el termino de acoplamiento)
		if( .not.RHFWaveFunction_instance( specieID ).isThereExternalPotential ) then
		
			RHFWaveFunction_instance( specieID ).independentSpecieEnergy = &
				sum(  transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values) &
				*  (  ( RHFWaveFunction_instance( specieID ).independentParticleMatrix.values ) &
				+ 0.5_8 * RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values) )
		else
		
			RHFWaveFunction_instance( specieID ).independentSpecieEnergy = &
			sum(  transpose(RHFWaveFunction_instance( specieID ).densityMatrix.values) &
			*  (  ( RHFWaveFunction_instance( specieID ).independentParticleMatrix.values ) &
			+ 0.5_8 * RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values &
			+ RHFWaveFunction_instance( specieID ).externalPotentialMatrix.values) )
		
		end if
	
		!! Calcula energia de acoplamiento en caso de mas de una especie presente
		if ( ParticleManager_getNumberOfQuantumSpecies() > 1 ) &
			RHFWaveFunction_instance( specieID ).couplingEnergy = sum( transpose( RHFWaveFunction_instance( specieID ).densityMatrix.values ) &
				* RHFWaveFunction_instance( specieID ).couplingMatrix.values )
				
	end subroutine RHFWaveFunction_obtainEnergyComponents

	
	!**
	! @brief indica el objeto ha sido instanciado 
	!
	!**					
	function RHFWaveFunction_isInstanced() result(output)
		implicit none
		logical :: output
		
		if ( allocated( RHFWaveFunction_instance ) ) then
			output = .true.
		else
			output = .false.
		end if
	
	end function RHFWaveFunction_isInstanced
	
	
	!**
	! @brief Retorna la matrix de Overlap
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	function RHFWaveFunction_getOverlapMatrix( nameOfSpecie, sspecieID ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		integer, optional :: sspecieID
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		if ( present(sspecieID) ) then
			
			specieID = sspecieID
		
		else
			
			nameOfSpecieSelected = "e-"
			
			if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
			specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
			
		end if
		
		if ( .not.associated(RHFWaveFunction_instance(specieID).overlapMatrixValuesPtr) ) then

			call RHFWaveFunction_buildOverlapMatrix( nameOfSpecieSelected )		
			
		end if
		
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).overlapMatrixValuesPtr )
			
	end function RHFWaveFunction_getOverlapMatrix
	
	
	!**
	! @brief Retorna la matrix de transformationMatrix
	!
	!**
	function RHFWaveFunction_getTransformationMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
				
		if ( .not.allocated(RHFWaveFunction_instance(specieID).transformationMatrix.values) ) then
				
			call RHFWaveFunction_buildTransformationMatrix( nameOfSpecieSelected )
			
		end if
		
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).transformationMatrix )
			
	end function RHFWaveFunction_getTransformationMatrix
	
	
	!**
	! @brief retorna la matrix de particula independiente.
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!**
	function RHFWaveFunction_getIndependentParticleMatrix( nameOfSpecie ) result(output) 
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
			
		if ( associated(RHFWaveFunction_instance( specieID ).kineticMatrixValuesPtr ) ) then
		
			call RHFWaveFunction_buildIndependentParticleMatrix( nameOfSpecieSelected )
		
		end if
		
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).independentParticleMatrix )
		
! 		print *,"getKineticMatrix", trim(nameOfSpecieSelected), specieID
! 		call Matrix_show( output )
	
	end function RHFWaveFunction_getIndependentParticleMatrix
	
	!**
	! @brief Retorna la matrix de dos particulas para la especie especificada
	!
	!**
	function RHFWaveFunction_getTwoParticlesMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
				
		if ( .not.allocated( RHFWaveFunction_instance(specieID).twoParticlesMatrix.values) ) then		
			
			call RHFWaveFunction_buildTwoParticlesMatrix( nameOfSpecieSelected )
			
		end if
		
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).twoParticlesMatrix )
			
	end function RHFWaveFunction_getTwoParticlesMatrix
	
	!**
	! @brief Retorna la matrix de acoplamiento para la especie especificada
	!
	!**
	function RHFWaveFunction_getCouplingMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( .not.allocated( RHFWaveFunction_instance(specieID).couplingMatrix.values) ) then		
			
			call RHFWaveFunction_buildCouplingMatrix( nameOfSpecieSelected )
			
		end if
				
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).couplingMatrix )
			
	end function RHFWaveFunction_getCouplingMatrix
	
	!**
	! @brief Retorna la matrix de acoplamiento para la especie especificada
	!        respecto a las particula puntuales
	!
	!**
	function RHFWaveFunction_getPuntualParticleMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output

		character(30) :: nameOfSpecieSelected
		integer :: specieID

		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )

		call RHFWaveFunction_buildPuntualParticleMatrix( nameOfSpecieSelected )

		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).puntualParticleMatrix )

	end function RHFWaveFunction_getPuntualParticleMatrix

	!**
	! @brief Retorna la matrix de interaccion con un potencial externo
	!
	!**
	function RHFWaveFunction_getExternalPotentialMatrix( nameOfSpecie ) result( output )
	implicit none
	character(*), optional :: nameOfSpecie
	type(Matrix) ::  output
	
	character(30) :: nameOfSpecieSelected
	integer :: specieID
	
	nameOfSpecieSelected = "e-"
	if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
	
	specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
	
	if ( .not.allocated( RHFWaveFunction_instance(specieID).externalPotentialMatrix.values) ) then		
	
	call RHFWaveFunction_buildExternalPotentialMatrix( nameOfSpecieSelected )
	
	end if
	
	call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).externalPotentialMatrix )
	
	end function RHFWaveFunction_getExternalPotentialMatrix
	

	!**
	! @brief Contruye la matrix de Fock para la especie especificada
	!
	!**
	function RHFWaveFunction_getFockMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( .not.allocated( RHFWaveFunction_instance(specieID).fockMatrix.values) ) then		
			
			call RHFWaveFunction_buildFockMatrix( nameOfSpecieSelected )
			
		end if
				
		call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).fockMatrix )
			
	end function RHFWaveFunction_getFockMatrix
	
	
	!**
	! @brief Retorna la matriz  la ultima matriz de densidad calculada para una especie especificada
	!
	!**
	function RHFWaveFunction_getDensityMatrix( nameOfSpecie, sspecieID ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		integer, optional :: sspecieID
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		if ( present(sspecieID) ) then
			
			specieID = sspecieID
		
		else
			
			nameOfSpecieSelected = "e-"
			
			if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
			specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
			
		end if
		
		if ( allocated( RHFWaveFunction_instance(specieID).densityMatrix.values) ) then		
			
			call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).densityMatrix )
			
		end if
				
	end function RHFWaveFunction_getDensityMatrix
	
	
	function RHFWaveFunction_getKineticMatrix( nameOfSpecie ) result(output)
		implicit none
		
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
	
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
			
		if ( allocated( RHFWaveFunction_instance(specieID).kineticMatrix.values) ) then		
			
			call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).kineticMatrix)
			
		end if
		
	end function RHFWaveFunction_getKineticMatrix
	
	
	!**
	! @brief Retorna apuntador a la matriz  de densidad
	!
	!**
	function RHFWaveFunction_getDensityMatrixPtr() result( output )
		implicit none
		type(Matrix), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output => RHFWaveFunction_instance(:).densityMatrix
			
		end if
				
	end function RHFWaveFunction_getDensityMatrixPtr

	
	!**
	! @brief Retorna la matriz  de coeficientes de combinacion
	!
	!**
	function RHFWaveFunction_getWaveFunctionCoefficients( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( allocated( RHFWaveFunction_instance(specieID).waveFunctionCoefficients.values) ) then		
			
			call Matrix_copyConstructor( output, RHFWaveFunction_instance(specieID).waveFunctionCoefficients )
			
		end if
				
	end function RHFWaveFunction_getWaveFunctionCoefficients
	
	!**
	! @brief Retorna apuntdaor a la matriz  de coeficientes de combinacion
	!
	!**
	function RHFWaveFunction_getWaveFunctionCoefficientsPtr() result( output )
		implicit none
		type(Matrix), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output => RHFWaveFunction_instance(:).waveFunctionCoefficients
			
		end if
				
	end function RHFWaveFunction_getWaveFunctionCoefficientsPtr

	!**
	! @brief Retorna valores propios del sistema molecular
	!
	!**
	function RHFWaveFunction_getMolecularOrbitalsEnergy( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Vector) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( allocated( RHFWaveFunction_instance(specieID).molecularOrbitalsEnergy.values) ) then		
			
			call Vector_copyConstructor( output, RHFWaveFunction_instance(specieID).molecularOrbitalsEnergy )
			
		end if
				
	end function RHFWaveFunction_getMolecularOrbitalsEnergy
	
	!**
	! @brief Retorna valores propios del sistema molecular
	!
	!**
	function RHFWaveFunction_getMolecularOrbitalsEnergyPtr() result( output )
		implicit none
		type(Vector), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).molecularOrbitalsEnergy
			
		end if
				
	end function RHFWaveFunction_getMolecularOrbitalsEnergyPtr

	!**
	! @brief Retorna vector con energia cinetica para cada especie presente
	!
	!**
	function RHFWaveFunction_getNameOfSpeciesPtr() result( output )
		implicit none
		character(30), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).name
			
		end if
				
	end function RHFWaveFunction_getNameOfSpeciesPtr
	
	!**
	! @brief Retorna vector con energia cinetica para cada especie presente
	!
	!**
	function RHFWaveFunction_getKineticEnergyPtr() result( output )
		implicit none
		real(8), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).kineticEnergy
			
		end if
				
	end function RHFWaveFunction_getKineticEnergyPtr
			
	!**
	! @brief Retorna vector con energia de repulsion para cada especie presente
	!
	!**
	function RHFWaveFunction_getRepulsionEnergyPtr() result( output )
		implicit none
		real(8), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).repulsionEnergy
			
		end if
				
	end function RHFWaveFunction_getRepulsionEnergyPtr
			
	!**
	! @brief Retorna vector con energia de acoplamineto para cada especie presente
	!
	!**
	function RHFWaveFunction_getCouplingEnergyPtr() result( output )
		implicit none
		real(8), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).couplingEnergy
			
		end if
				
	end function RHFWaveFunction_getCouplingEnergyPtr
			
	!**
	! @brief Retorna vector con energia de acoplamineto para cada especie presente
	!
	!**
	function RHFWaveFunction_getQuantPuntualEnergyPtr() result( output )
		implicit none
		real(8), pointer ::  output(:)
		
		if ( allocated( RHFWaveFunction_instance ) ) then		
			
			output =>  RHFWaveFunction_instance(:).puntualInteractionEnergy
			
		end if
				
	end function RHFWaveFunction_getQuantPuntualEnergyPtr
			
			
			
	!**
	! @brief Retorna la energia calculada en la ultima iteracion
	!**
	function RHFWaveFunction_getEnergy( nameOfSpecie, typeOfEnergy ) result( output )
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie
		character(*), optional, intent(in) :: typeOfEnergy
		real(8) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		
		select case(trim(typeOfEnergy))
		
			case("specieEnergy")
				output = RHFWaveFunction_instance(specieID).totalEnergyForSpecie
			
			case("independentSpecieEnergy")
				output = RHFWaveFunction_instance(specieID).independentSpecieEnergy
			
			case("kineticEnergy")
				output = RHFWaveFunction_instance(specieID).kineticEnergy
			
			case("puntualInteractionEnergy")
				
				output = RHFWaveFunction_instance(specieID).puntualInteractionEnergy
		
			case("independentParticleEnergy")
				output = RHFWaveFunction_instance(specieID).independentParticleEnergy
						
			case("repulsionEnergy")
				output = RHFWaveFunction_instance(specieID).repulsionEnergy
				
			case("couplingEnergy")
				output = RHFWaveFunction_instance(specieID).couplingEnergy

			case("externalPotentialEnergy")
				output = RHFWaveFunction_instance(specieID).externalPotentialEnergy

			case default

				output = RHFWaveFunction_instance(specieID).totalEnergyForSpecie
		
		end select
		
	end function RHFWaveFunction_getEnergy

	function RHFWaveFunction_getValueForOrbitalAt( nameOfSpecie, orbitalNum, coordinate ) result(output)
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie
		integer :: orbitalNum
		real(8) :: coordinate(3)
		real(8) :: output

		integer :: specieID
		character(30) :: nameOfSpecieSelected
		integer :: numberOfContractions
		integer :: totalNumberOfContractions
		integer :: particleID
		integer :: contractionID
		integer :: i, j
		real(8), allocatable :: auxVal(:)


		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )

		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
	
		output=0.0_8
		do i=1,numberOfContractions

			particleID = ParticleManager_instance.idsOfContractionsForSpecie(specieID).contractionID(i).particleID
			contractionID=ParticleManager_instance.idsOfContractionsForSpecie(specieID).contractionID(i).contractionIDInParticle

			totalNumberOfContractions = ParticleManager_instance.particlesPtr(particleID).basis.contractions(contractionID).numCartesianOrbital

			if( allocated(auxVal)) deallocate(auxVal)
			allocate(auxVal(totalNumberOfContractions))

			auxVal = ContractedGaussian_getValueAt(ParticleManager_getContractionPtr( specieID,  numberOfContraction=i ), coordinate )

			do j = 1, totalNumberOfContractions

				output = output + auxVal(j) * RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values(j,orbitalNum)

			end do

		end do
	
		
	end function RHFWaveFunction_getValueForOrbitalAt

	
	subroutine RHFWaveFunction_draw2DOrbital( nameOfSpecie, orbitalNum, flags )
	implicit none
	character(*), optional, intent(in) :: nameOfSpecie
	integer :: orbitalNum
	integer :: flags

	
	character(30) :: nameOfSpecieSelected
	character(50) :: fileName
	character(50) :: xRange
	integer :: specieID
	integer :: numberOfContractions
	integer :: j
	integer :: i
	integer :: numOfGraphs
	integer :: auxInitOrbitalNum
	integer :: auxLastOrbitalNum

	nameOfSpecieSelected = "e-"
	if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

	specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )

	numberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )
	fileName=trim(APMO_instance.INPUT_FILE)//'orbital.'//trim(String_convertIntegerToString(orbitalNum))//'.'//trim(nameOfSpecieSelected)
	
	select case( flags )

		case(ORBITAL_ALONE)
	
			open ( 5,FILE=trim(fileName)//".dat", STATUS='REPLACE',ACTION='WRITE')
			do j=-APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,&
				APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,1
			write (5,"(F20.10,F20.10)") j*APMO_instance.STEP_OF_GRAPHS, &
			RHFWaveFunction_getValueForOrbitalAt( nameOfSpecieSelected,&
			orbitalNum, [0.0_8,0.0_8,j*APMO_instance.STEP_OF_GRAPHS] ) !&
!			 + (RHFWaveFunction_instance( specieID ).molecularOrbitalsEnergy.values(orbitalNum) * CM_NEG1)
			end do
			close(5)
			call InputOutput_make2DGraph(trim(fileName),&
				"Nuclear Wave Function",&
				"r / Bohr",&
				"U / a.u.", &
				y_format="%.2e")

		case(ORBITAL_WITH_POTENTIAL)

			auxInitOrbitalNum=orbitalNum
			auxLastOrbitalNum=orbitalNum
			numOfGraphs=2
			if(orbitalNum==0) then
				auxInitOrbitalNum=1	
				auxLastOrbitalNum=numberOfContractions
				numOfGraphs=numberOfContractions+1
			end if
		open ( 5,FILE=trim(fileName)//".dat", STATUS='REPLACE',ACTION='WRITE')
		do j=-APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,&
			APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,1
			write (5,"(<2>ES20.10$)") &
			j*APMO_instance.STEP_OF_GRAPHS, &
			ExternalPotential_getPotential(externalPotentialManager_instance.externalsPots(1),&
			[j*APMO_instance.STEP_OF_GRAPHS,0.0_8,0.0_8])*CM_NEG1
			do i=auxInitOrbitalNum,auxLastOrbitalNum
				write (5,"(ES20.10$)") &
				APMO_instance.WAVE_FUNCTION_SCALE&
				*RHFWaveFunction_getValueForOrbitalAt( nameOfSpecieSelected, i,&
					[0.0_8,0.0_8,j*APMO_instance.STEP_OF_GRAPHS] ) &
				+ (RHFWaveFunction_instance( specieID ).molecularOrbitalsEnergy.values(i) * CM_NEG1)
			end do
			write (5,"(A)") ""
		end do
		close(5)

		xRange=trim(adjustl(String_convertRealToString(real(&
			-APMO_instance.MAXIMUM_RANGE_OF_GRAPHS&
			*APMO_instance.STEP_OF_GRAPHS,8))))//':'//trim(adjustl(String_convertRealToString(real(&
			APMO_instance.MAXIMUM_RANGE_OF_GRAPHS&
			*APMO_instance.STEP_OF_GRAPHS,8))))

			call InputOutput_make2DGraph(trim(fileName),&
			"Nuclear Wave Function in potential ",&
			"r / Bohr",&
			"U / cm-1",&
			y_format="%.2e",numOfGraphs=numOfGraphs,x_range=trim(xRange))

	end select

	end subroutine RHFWaveFunction_draw2DOrbital
	
	!**
	! @brief calcula la energia total de acoplamiento para una especie especificada
	!
	!**					
	function RHFWaveFunction_getTotalCoupligEnergy() result( output )
		implicit none
		real(8) :: output
		
		character(30) :: nameOfSpecie
		character(30) :: nameOfOtherSpecie
		real(8) :: auxValue
		integer :: numberOfContractions
		integer :: numberOfContractionsOfOtherSpecie
		integer :: numberOfTotalContractions
		integer :: numberOfTotalContractionsOfOtherSpecie
		integer :: specieID
		integer :: otherSpecieID
		integer :: speciesIteratorA
		integer :: speciesIteratorB
		integer :: u
		integer :: v
		integer :: l
		integer :: k
		
		output =0.0_8		
		
		do speciesIteratorA=1, ParticleManager_getNumberOfQuantumSpecies()
			specieID = ParticleManager_getSpecieID( iteratorOfSpecie = speciesIteratorA )
			nameOfSpecie = ParticleManager_getNameOfSpecie( specieID )
			numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
			numberOfTotalContractions = ParticleManager_getTotalNumberOfContractions( specieID )

			do speciesIteratorB = speciesIteratorA+1, ParticleManager_getNumberOfQuantumSpecies()
				
				otherSpecieID = ParticleManager_getSpecieID( iteratorOfSpecie = speciesIteratorB )
				
				!! Restringe suma de terminos repulsivos de la misma especie.
				if ( otherSpecieID /= specieID ) then
					
					nameOfOtherSpecie = ParticleManager_getNameOfSpecie( otherSpecieID )
					numberOfContractionsOfOtherSpecie = ParticleManager_getNumberOfContractions( otherSpecieID )
					numberOfTotalContractionsOfOtherSpecie = ParticleManager_getTotalNumberOfContractions( otherSpecieID )

					auxValue = 0.0_8

					do u = 1 , numberOfTotalContractions
						do v = 1 , numberOfTotalContractions
							do l = 1 , numberOfTotalContractionsOfOtherSpecie
								do k = 1 , numberOfTotalContractionsOfOtherSpecie
									
									auxValue = auxValue +&
										(  RHFWaveFunction_instance( specieID ).densityMatrix.values(v,u) &
										* RHFWaveFunction_instance( otherSpecieID).densityMatrix.values(l,k) &
										*  IntegralManager_getInterspecieRepulsionIntegral( u, v, l, k, &
												specieID, otherSpecieID,numberOfContractions,numberOfContractionsOfOtherSpecie, &
												numberOfTotalContractions, numberOfTotalContractionsOfOtherSpecie ) )

								end do
							end do
						end do
					end do

					auxValue = auxValue *  ParticleManager_getCharge( specieID=specieID ) &
										* ParticleManager_getCharge( specieID=otherSpecieID )

					output =output +auxValue
				
				end if
			
			end do
		end do

	end function RHFWaveFunction_getTotalCoupligEnergy
	
	!**
	! @brief Resetea los atributos de clase
	!**
	subroutine RHFWaveFunction_reset()
		implicit none
			
		
		integer :: speciesIterator
		integer :: specieID
		
		do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
			
			specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
			RHFWaveFunction_instance( specieID ).totalEnergyForSpecie = 0.0_8
			RHFWaveFunction_instance( specieID ).independentSpecieEnergy =0.0_8
			RHFWaveFunction_instance( specieID ).kineticEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).puntualInteractionEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).independentParticleEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).repulsionEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).couplingEnergy = 0.0_8
			RHFWaveFunction_instance( specieID ).addTwoParticlesMatrix = .false.
			RHFWaveFunction_instance( specieID ).addCouplingMatrix = .false.
			RHFWaveFunction_instance( specieID ).wasBuiltFockMatrix = .false.
			
			RHFWaveFunction_instance( specieID ).waveFunctionCoefficients.values = 0.0_8
			RHFWaveFunction_instance( specieID ).molecularOrbitalsEnergy.values = 0.0_8
			RHFWaveFunction_instance( specieID ).independentParticleMatrix.values = 0.0_8
			RHFWaveFunction_instance( specieID ).densityMatrix.values = 0.0_8
			RHFWaveFunction_instance( specieID ).transformationMatrix.values = 0.0_8
			RHFWaveFunction_instance( specieID ).twoParticlesMatrix.values = 0.0_8
			RHFWaveFunction_instance( specieID ).couplingMatrix.values = 0.0_8
			RHFWaveFunction_instance( specieID ).fockMatrix.values = 0.0_8
			
			if ( associated( RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr ) )  &
				RHFWaveFunction_instance(specieID).kineticMatrixValuesPtr => null()
			
			if ( associated( RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr )) & 
				RHFWaveFunction_instance(specieID).puntualInteractionMatrixValuesPtr => null()
			
			if ( associated( RHFWaveFunction_instance(specieID).overlapMatrixValuesPtr )) &
				RHFWaveFunction_instance(specieID).overlapMatrixValuesPtr => null()

			
		end do
	
	end subroutine RHFWaveFunction_reset
	
end module RHFWaveFunction_
