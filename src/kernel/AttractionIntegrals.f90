!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief   Modulo para calculo de integrales de atraccion repulsion con cargas puntuales.
!
! Este modulo define una seudoclase cuyos metodos devuelven la integral de atraccion 
! o repulsion entre una  particula cuantica descrita mediante una distribucion  
! gausiana sin normalizar  y una carga puntual localizada en \f$R_c\f$. El calculo de la
! integral  no considera la carga de la particula puntual. 
!
! \f[ (\bf{a} \mid A(0) \mid \bf{b}) = \int_{TE} {{\varphi(\bf{r};
!  {\bf{\zeta}}_a, \bf{n_a,R_A})} {\frac{1}{\mid \bf{r-R_C} \mid
!  }} {\varphi(\bf{r};{\zeta}_b,\bf{n_b,R_B})}},dr\f]
!
! Donde:
!
! <table>
! <tr> <td> \f$ \zeta \f$ : <td> <dfn> Exponente orbital. </dfn>
! <tr> <td> <b> r  </b> : <td> <dfn> Coordenas espaciales de la funcion. </dfn>
! <tr> <td> \f$ n_a ,n_b \f$ : <td> <dfn> Indice de momento angular. </dfn>
! <tr> <td> \f$ R_A , R_B \f$ : <td> <dfn> Origen de la funcion gaussiana cartesiana.</dfn>
! <tr> <td> \f$ R_C  \f$ : <td> <dfn> origen de la particula puntual </dfn>
! </table>
!
! Este tipo de integral corresponde a una integral de dos centros, calculada por 
! aproximacion numerica, utilizando la integral auxiliar (ver Math_):
!
!    \f[ F_m(U)= \int_{0}^{1} {t^{2m}e^{-Tt^2}}\,dt \f]
!
! La integral de repulsion-atraccion con cargas puntuales se calcula de 
! acuerdo m�todo recursivo propuesto por Obara-Sayka. La expresion general
! de la integral es: 
!
! \f[({\bf{a + 1_i}} \parallel A(0) \parallel {\bf{b}})^{(m)} = \f] 
! \f[ (P_i -A_i) ({\bf{a}} \parallel A(0) \parallel {\bf{b}} )^{(m)} 
!  - (P_i -C_i)({\bf{a}} \parallel A(0) \parallel {\bf{b}} )^{(m+1)} \f]
! \f[ + \frac{1}{2 \zeta} N_i(\bf{a}) \left\{ ({\bf{a-1_i}} \parallel A(0)
! \parallel {\bf{b}})^{(m)} - ({\bf{a-1_i}} \parallel A(0) \parallel 
! {\bf{b}})^{(m+1)}  \right\}\f]
! \f[ + \frac{1}{2 \zeta} N_i(\bf{b}) \left\{ ({\bf{a}} \parallel A(0) 
! \parallel {\bf{b-1_i}})^{(m)} - ({\bf{a}} \parallel A(0) \parallel 
! {\bf{b-1_i}})^{(m+1)}  \right\}\f]
!
! Donde (m) es un entero no negativo que hace referencia al orden de la 
! integral dentro de la recursi�n. Los parametros <b> P </b> y \f$ \zeta \f$ de la
! expresion provienen del producto de dos funciones gaussianas. 
!
! @author Fernando Posada (efposadac@unal.edu.co)
!
! <b> Fecha de creacion : </b> 20010-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapta al estandar de codificacion propuesto.
!   - <tt> 2007-05-15 </tt>: Fernando Posada. ( efposadac@unal.edu.co )
!        -# Reescribe el módulo.
!
! @see PrimitiveGaussian
!
!**
module AttractionIntegrals_
	use PrimitiveGaussian_
	use Math_
	use Exception_
		
	!! Atributos de una particula puntual
	type, public :: puntualParticle
		real(8) :: x
	    real(8) :: y
	    real(8) :: z
	    real(8) :: charge
	end type puntualParticle

	public ::  &
		AttractionIntegrals_compute, &
		PrimitiveGaussian_attractionIntegral
		
	private :: &
		AttractionIntegrals_oSRecursion
		
contains
	
	!<
	!@brief Evalua integrales overlap para cualquier momento angular
	!@author Edwin Posada, 2010
	!@return devuelve los valores de integrales para una capa dada (output)
	!>
	subroutine AttractionIntegrals_compute (ami, amj, nprim1, nprim2, npunt, A, B, exp1, exp2, coef1, coef2, norm1, norm2, particles, buffer)
	  implicit none

	  integer, intent(in) :: ami(0:3), amj(0:3)
	  integer, intent(in) :: nprim1, nprim2
	  integer, intent(in) :: npunt
	  real(8), intent(in) :: A(0:3), B(0:3)
	  real(8), intent(in) :: exp1(0:nprim1) ,exp2(0:nprim2)
	  real(8), intent(in) :: coef1(0:nprim1), coef2(0:nprim2)
	  real(8), intent(in) :: norm1(0:nprim1), norm2(0:nprim2)
	  type(puntualParticle), intent(in) :: particles(0:npunt)
	  real(8), intent(inout) :: buffer

	  real(8), allocatable :: AI0(:,:,:)
	  real(8) :: PA(0:3), PB(0:3), PC(0:3), P(0:3)
	  real(8) :: a1, c1, d1
	  real(8) :: a2, c2, d2
	  real(8) :: over_pf
	  real(8) :: AB2
	  real(8) :: gam, oog
	  integer :: am1, am2
	  integer :: izm, iym, ixm
	  integer :: jzm, jym, jxm
	  integer :: indmax
	  integer :: p1, p2
	  integer :: atom
	  integer :: max_am, mmax
	  integer :: iind
	  integer :: jind
	  integer :: i, j

	  buffer = 0.0_8

	  am1 = sum(ami)
	  am2 = sum(amj)

	  max_am = max(am1, am2) + 1

	  indmax = (max_am-1)*max_am*max_am+1

	  if(allocated(AI0))deallocate(AI0)
	  allocate(AI0(0:indmax, 0:indmax, 0:2*max_am+1))

	  AI0 = 0.0_8

      !   computa intermedios
	  AB2 = 0.0_8
	  AB2 = AB2 + (A(0) - B(0)) * (A(0) - B(0))
	  AB2 = AB2 + (A(1) - B(1)) * (A(1) - B(1))
	  AB2 = AB2 + (A(2) - B(2)) * (A(2) - B(2))

	  izm = 1
	  iym = am1+1
	  ixm = iym*iym

	  jzm = 1
	  jym = am2+1
	  jxm = jym*jym

	  do p1=0, nprim1-1
	    a1 = exp1(p1)
	    c1 = coef1(p1)
	    d1 = norm1(p1)
	    do p2=0, nprim2 -1
	      a2 = exp2(p2)
	      c2 = coef2(p2)
	      d2 = norm2(p2)
	      gam = a1 + a2
	      oog = 1.0/gam

	      P(0) = (a1*A(0) + a2*B(0))*oog
	      P(1) = (a1*A(1) + a2*B(1))*oog
	      P(2) = (a1*A(2) + a2*B(2))*oog
	      PA(0) = P(0) - A(0)
	      PA(1) = P(1) - A(1)
	      PA(2) = P(2) - A(2)
	      PB(0) = P(0) - B(0)
	      PB(1) = P(1) - B(1)
	      PB(2) = P(2) - B(2)

	      over_pf = exp(-a1*a2*AB2*oog) * sqrt(Math_PI*oog) * Math_PI * oog * c1 * c2 * d1 * d2

	      do atom=0, npunt - 1
		PC(0) = P(0) - particles(atom).x
		PC(1) = P(1) - particles(atom).y
		PC(2) = P(2) - particles(atom).z

		mmax = am1 + am2 + 1

		call AttractionIntegrals_OSrecurs(AI0,PA,PB,PC,gam,mmax,am1,am2)

		iind = ami(2)*izm + ami(1)*iym + ami(0)*ixm

		jind = amj(2)*jzm + amj(1)*jym + amj(0)*jxm

		buffer = buffer - AI0(iind,jind,0) * particles(atom).charge * over_pf

	      end do
	    end do
	  end do

	end subroutine AttractionIntegrals_compute


	subroutine AttractionIntegrals_OSrecurs(AI0, PA, PB, PC, gam, mmax, iang, jang)
	  implicit none
	  real(8), intent(inout), allocatable :: AI0(:,:,:)
	  real(8), intent(in) :: PA(0:3)
	  real(8), intent(in) :: PB(0:3)
	  real(8), intent(in) :: PC(0:3)
	  real(8), intent(in) :: gam
	  integer, intent(in) :: mmax
	  integer, intent(in) :: iang
	  integer, intent(in) :: jang

	  real(8), dimension(0: mmax) :: F
	  real(8) :: pp
	  real(8) :: tmp
	  real(8) :: u
	  integer :: a, b, m
	  integer :: izm, iym, ixm
	  integer :: jzm, jym, jxm
	  integer :: ix,iy,iz,jx,jy,jz
	  integer :: iind,jind

	  izm = 1
	  iym = iang + 1
	  ixm = iym * iym
	  jzm = 1
	  jym = jang + 1
	  jxm = jym * jym
	  pp = 1 / (2 * gam)
	  tmp = sqrt(gam)*(2.0_8/Math_SQRT_PI)
	  u = gam*(PC(0) * PC(0) + PC(1) * PC(1) + PC(2) * PC(2))

	  call Math_fgamma0(mmax,u,F)

	  do m = 0, mmax
	    AI0(0, 0, m) = tmp * F(m)
	  end do

      !     Upward recursion in j with i=0

	  do b = 1, jang
	    do jx = 0, b
	      do jy=0, b - jx
		jz = b - jx - jy
		jind = jx * jxm + jy * jym + jz * jzm
		if (jz > 0) then
		  do m=0, mmax - b	! Electrostatic potential integrals
		    AI0(0,jind,m) = PB(2)*AI0(0,jind-jzm,m) - PC(2)*AI0(0, jind-jzm, m+1)
		  end do

		  if (jz > 1) then
		    do m=0, mmax-b
		      AI0(0,jind,m) = AI0(0,jind,m) + pp*(jz-1)*(AI0(0,jind-2*jzm,m) - AI0(0,jind-2*jzm,m+1))
		    end do
		  end if

		else if (jy > 0) then
		  do m=0, mmax-b
		    AI0(0,jind,m) = PB(1)*AI0(0,jind-jym,m) -	PC(1)*AI0(0,jind-jym,m+1)
		  end do

		  if (jy > 1) then
		    do m=0, mmax-b
		      AI0(0,jind,m) = AI0(0,jind,m) + pp*(jy-1)*(AI0(0,jind-2*jym,m) - AI0(0,jind-2*jym,m+1))
		    end do
		  end if

		else if (jx > 0) then
		  do m=0, mmax-b
		    AI0(0,jind,m) = PB(0)*AI0(0,jind-jxm,m) -	PC(0)*AI0(0,jind-jxm,m+1)
		  end do

		  if (jx > 1) then
		    do m=0, mmax-b
		      AI0(0,jind,m) = AI0(0,jind,m) + pp*(jx-1)*(AI0(0,jind-2*jxm,m) - AI0(0,jind-2*jxm,m+1))
		    end do
		  end if

		else
		  print*, "  There's some error in the AI_OSrecurs algorithm"
		end if
	      end do
	    end do
	  end do

      !     The following fragment cannot be vectorized easily, I guess :-)
      !     Upward recursion in i with all possible j's

	  do b=0, jang
	    do jx=0, b
	      do jy=0, b-jx
		jz = b-jx-jy
		jind = jx*jxm + jy*jym + jz*jzm
		do a=1, iang
		  do ix=0, a
		    do iy=0, a-ix
		      iz = a-ix-iy
		      iind = ix*ixm + iy*iym + iz*izm
		      if (iz > 0) then
			do m=0, mmax-a-b
			  AI0(iind,jind,m) = PA(2)*AI0(iind-izm,jind,m) - PC(2)*AI0(iind-izm,jind,m+1)
			end do

			if (iz > 1) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*(iz-1)*(AI0(iind-2*izm,jind,m) - AI0(iind-2*izm,jind,m+1))
			  end do
			end if

			if (jz > 0) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*jz*(AI0(iind-izm,jind-jzm,m) - AI0(iind-izm,jind-jzm,m+1))
			  end do
			end if

		      else if (iy > 0) then
			do m=0, mmax-a-b
			  AI0(iind,jind,m) = PA(1)*AI0(iind-iym,jind,m) - PC(1)*AI0(iind-iym,jind,m+1)
			end do

			if (iy > 1) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*(iy-1)*(AI0(iind-2*iym,jind,m) - AI0(iind-2*iym,jind,m+1))
			  end do
			end if

			if (jy > 0) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*jy*(AI0(iind-iym,jind-jym,m) - AI0(iind-iym,jind-jym,m+1))
			  end do
			end if

		      else if (ix > 0) then
			do m=0, mmax-a-b
			  AI0(iind,jind,m) = PA(0)*AI0(iind-ixm,jind,m) - PC(0)*AI0(iind-ixm,jind,m+1)
			end do

			if (ix > 1) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*(ix-1)*(AI0(iind-2*ixm,jind,m) - AI0(iind-2*ixm,jind,m+1))
			  end do
			end if

			if (jx > 0) then
			  do m=0, mmax-a-b
			    AI0(iind,jind,m) = AI0(iind,jind,m) + pp*jx*(AI0(iind-ixm,jind-jxm,m) - AI0(iind-ixm,jind-jxm,m+1))
			  end do
			end if
		      else
			print*, "There's some error in the AI_OSrecurs algorithm"
		      end if
		    end do
		  end do
		end do
	      end do
	    end do
	  end do

	end subroutine AttractionIntegrals_OSrecurs


	subroutine PrimitiveGaussian_attractionIntegral(  primitiveA, primitiveB, originPuntualParticle, output)
	  	implicit none

 		type(PrimitiveGaussian) , intent(in) :: primitiveA
		type(PrimitiveGaussian) , intent(in) :: primitiveB
		real(8), intent(in) :: originPuntualParticle(3)
		real(8), allocatable, intent(inout) :: output(:)

		real(8), allocatable :: auxIntegrals(:,:,:)
		real(8) :: zeta, zetaInv, overPf
		real(8) :: P(3), PA(0:3), PB(0:3), PC(0:3)
		real(8) :: AB2
	 	integer :: auxMoment
		integer :: indexA, indexB
  	  	integer :: auxAX, auxAY, auxAZ
		integer :: auxBX, auxBY, auxBZ
	 	integer :: maxAngularMoment
	 	integer :: maxIndex
	  	integer :: ax, ay, az
	  	integer :: bx, by, bz
	  	integer :: ai, aj
	  	integer :: bi, bj
	  	integer :: i
	  	integer :: counter

	  	maxAngularMoment = max(sum(primitiveA%angularMomentIndex), sum(primitiveB%angularMomentIndex)) + 1
	  	maxIndex = (maxAngularMoment-1)*maxAngularMoment*maxAngularMoment + 1_8
	  	auxMoment = sum(primitiveA%angularMomentIndex) + sum(primitiveB%angularMomentIndex) + 1

		if(allocated(auxIntegrals))deallocate(auxIntegrals)
	  	allocate(auxIntegrals(0:maxIndex, 0:maxIndex, 0:2*maxAngularMoment+1))

	  	auxIntegrals = 0.0_8

      	!calcular intermediarios
  		AB2 = 0.0_8
	  	AB2 = AB2 + (primitiveA%origin(1) - primitiveB%origin(1)) * (primitiveA%origin(1) - primitiveB%origin(1))
	  	AB2 = AB2 + (primitiveA%origin(2) - primitiveB%origin(2)) * (primitiveA%origin(2) - primitiveB%origin(2))
	  	AB2 = AB2 + (primitiveA%origin(3) - primitiveB%origin(3)) * (primitiveA%origin(3) - primitiveB%origin(3))

	  	auxAZ = 1
	  	auxAY = sum(primitiveA%angularMomentIndex)+1
	  	auxAX = auxAY * auxAY

	  	auxBZ = 1
	  	auxBY = sum(primitiveB%angularMomentIndex)+1
	  	auxBX = auxBY * auxBY

 		zeta = primitiveA%orbitalExponent + primitiveB%orbitalExponent
	  	zetaInv = 1.0_8/zeta

	  	P(1) = ( primitiveA%orbitalExponent*primitiveA%origin(1) + primitiveB%orbitalExponent *primitiveB%origin(1))*zetaInv
	  	P(2) = ( primitiveA%orbitalExponent*primitiveA%origin(2) + primitiveB%orbitalExponent *primitiveB%origin(2))*zetaInv
	  	P(3) = ( primitiveA%orbitalExponent*primitiveA%origin(3) + primitiveB%orbitalExponent *primitiveB%origin(3))*zetaInv

		PA(0) = P(1) - primitiveA%origin(1)
		PA(1) = P(2) - primitiveA%origin(2)
		PA(2) = P(3) - primitiveA%origin(3)
		PB(0) = P(1) - primitiveB%origin(1)
		PB(1) = P(2) - primitiveB%origin(2)
		PB(2) = P(3) - primitiveB%origin(3)

		overPf = exp(-primitiveA%orbitalExponent * primitiveB%orbitalExponent * AB2*zetaInv ) * &
					sqrt(Math_PI*zetaInv ) * Math_PI * zetaInv

		PC(0) = P(1) - originPuntualParticle(1)
		PC(1) = P(2) - originPuntualParticle(2)
		PC(2) = P(3) - originPuntualParticle(3)

  	  	!! recursion
  		call AttractionIntegrals_oSRecursion(auxIntegrals, PA, PB, PC, zeta, auxMoment, sum(primitiveA%angularMomentIndex), &
	  												sum(primitiveB%angularMomentIndex))

  		output = 0.0_8
		counter = 0

		if(primitiveA%angularMoment == 0) then

			ax = primitiveA%angularMomentIndex(1)
			ay = primitiveA%angularMomentIndex(2)
			az = primitiveA%angularMomentIndex(3)

			bx = primitiveB%angularMomentIndex(1)
			by = primitiveB%angularMomentIndex(2)
			bz = primitiveB%angularMomentIndex(3)

			indexA = az*auxAZ + ay*auxAY + ax*auxAX
			indexB = bz*auxBZ + by*auxBY + bx*auxBX

			counter = counter + 1

			output(counter) = auxIntegrals(indexA,indexB,0) * overPf
		else

		    do ai = 0 , primitiveA%angularMoment
	        	ax = primitiveA%angularMoment - ai
	            do aj = 0 , ai
		            ay = ai - aj
	                az = aj
	        	    do bi = 0 , primitiveB%angularMoment
	        			bx = primitiveB%angularMoment - bi
	            		do bj = 0 , bi
		            		by = bi - bj
	                		bz = bj

							indexA = az*auxAZ + ay*auxAY + ax*auxAX
							indexB = bz*auxBZ + by*auxBY + bx*auxBX

							counter = counter + 1

							output(counter) = auxIntegrals(indexA,indexB,0) * overPf

					  	end do
				  	end do
			  	end do
		  	end do

	  	end if

	end subroutine PrimitiveGaussian_attractionIntegral


	subroutine AttractionIntegrals_oSRecursion(auxIntegrals, PA, PB, PC, zeta, sumAngularMoment, &
												angularMomentA, angularMomentB)
		implicit none
 		real(8), intent(inout), allocatable :: auxIntegrals(:,:,:)
	  	real(8), intent(in) :: PA(0:3), PB(0:3), PC(0:3)
	  	real(8), intent(in) :: zeta
	  	integer, intent(in) :: sumAngularMoment
	  	integer(8), intent(in) :: angularMomentA, angularMomentB

		real(8), dimension(0: sumAngularMoment) :: incompletGamma
		real(8) :: incompleteGammaArgument
	  	real(8) :: twoZetaInv
	  	real(8) :: auxVal
	  	integer :: indexA, indexB
  	  	integer :: auxAX, auxAY, auxAZ
		integer :: auxBX, auxBY, auxBZ
	  	integer :: ix,iy,iz,jx,jy,jz
	  	integer :: a, b, m

		type(Exception) :: ex

  	  	auxAZ = 1
	  	auxAY = angularMomentA + 1
	  	auxAX = auxAY*auxAY

	  	auxBZ = 1
	  	auxBY = angularMomentB + 1
	  	auxBX = auxBY*auxBY

	  	twoZetaInv = 1.0_8/(2.0_8*zeta)
	  	auxVal = sqrt(zeta)* (2.0_8/Math_SQRT_PI)

	  	incompleteGammaArgument = zeta * (PC(0) * PC(0) + PC(1) * PC(1) + PC(2) * PC(2))

	  	call Math_fgamma0(int(sumAngularMoment),incompleteGammaArgument,incompletGamma)

		!! Integrales Auxiliares
	  	do m = 0, sumAngularMoment
	    	auxIntegrals(0, 0, m) = auxVal * incompletGamma(m)
	  	end do

		!! Upward recursion in j with i=0

	  	do b = 1, angularMomentB
	    	do jx = 0, b
	      		do jy=0, b - jx
					jz = b - jx - jy
					indexB = jx * auxBX + jy * auxBY + jz * auxBZ
					if (jz > 0) then
		  				do m=0, sumAngularMoment - b	! Electrostatic potential integrals
		    				auxIntegrals(0,indexB,m) = PB(2)*auxIntegrals(0,indexB-auxBZ,m) - &
		    											PC(2)*auxIntegrals(0, indexB-auxBZ, m+1)
		  				end do

		  				if (jz > 1) then
		    				do m=0, sumAngularMoment-b
		      					auxIntegrals(0,indexB,m) = auxIntegrals(0,indexB,m) + twoZetaInv*(jz-1)*(auxIntegrals&
		      						(0,indexB-2*auxBZ,m) - auxIntegrals(0,indexB-2*auxBZ,m+1))
		    				end do
		  				end if

					else if (jy > 0) then
		  				do m=0, sumAngularMoment-b
		    				auxIntegrals(0,indexB,m) = PB(1)*auxIntegrals(0,indexB-auxBY,m) -	&
		    											PC(1)*auxIntegrals(0,indexB-auxBY,m+1)
		  				end do

		  				if (jy > 1) then
		    				do m=0, sumAngularMoment-b
		      					auxIntegrals(0,indexB,m) = auxIntegrals(0,indexB,m) + twoZetaInv*(jy-1)*(auxIntegrals&
		      						(0,indexB-2*auxBY,m) - auxIntegrals(0,indexB-2*auxBY,m+1))
		    				end do
		  				end if

					else if (jx > 0) then
		  				do m=0, sumAngularMoment-b
		    				auxIntegrals(0,indexB,m) = PB(0)*auxIntegrals(0,indexB-auxBX,m) - &
														PC(0)*auxIntegrals(0,indexB-auxBX,m+1)
		  				end do

		  				if (jx > 1) then
		    				do m=0, sumAngularMoment-b
		      					auxIntegrals(0,indexB,m) = auxIntegrals(0,indexB,m) + twoZetaInv*(jx-1)*(auxIntegrals&
									(0,indexB-2*auxBX,m) - auxIntegrals(0,indexB-2*auxBX,m+1))
		    				end do
		  				end if

					else
						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the  oSRecursion function" )
						call Exception_setDescription( ex, "There is some error in the Attraction Integrals OSrecursion algorithm" )
						call Exception_show( ex )
					end if
	      		end do
	    	end do
		end do
      	!! El siguiente algoritmo no puede ser facilmente vectorizado...
      	!! Upward recursion in i with all possible j's

	  	do b=0, angularMomentB
	    	do jx=0, b
	      		do jy=0, b-jx
					jz = b-jx-jy
					indexB = jx*auxBX + jy*auxBY + jz*auxBZ
					do a=1, angularMomentA
		  				do ix=0, a
		    				do iy=0, a-ix
		      					iz = a-ix-iy
		      					indexA = ix*auxAX + iy*auxAY + iz*auxAZ
		      					if (iz > 0) then
									do m=0, sumAngularMoment-a-b
			  							auxIntegrals(indexA,indexB,m) = PA(2)*auxIntegrals(indexA-auxAZ,indexB,m) - &
			  															PC(2)*auxIntegrals(indexA-auxAZ,indexB,m+1)
									end do

									if (iz > 1) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*(iz-1)*&
			    								(auxIntegrals(indexA-2*auxAZ,indexB,m) - auxIntegrals(indexA-2*auxAZ,indexB,m+1))
			  							end do
									end if

									if (jz > 0) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*jz*&
			    								(auxIntegrals(indexA-auxAZ,indexB-auxBZ,m) - auxIntegrals(indexA-auxAZ,indexB-auxBZ,m+1))
			  							end do
									end if

		      					else if (iy > 0) then
									do m=0, sumAngularMoment-a-b
			  							auxIntegrals(indexA,indexB,m) = PA(1)*auxIntegrals(indexA-auxAY,indexB,m) - &
			  															PC(1)*auxIntegrals(indexA-auxAY,indexB,m+1)
									end do

									if (iy > 1) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*(iy-1)*&
			    								(auxIntegrals(indexA-2*auxAY,indexB,m) - auxIntegrals(indexA-2*auxAY,indexB,m+1))
			  							end do
									end if

									if (jy > 0) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*jy*(auxIntegrals&
			    							(indexA-auxAY,indexB-auxBY,m) - auxIntegrals(indexA-auxAY,indexB-auxBY,m+1))
			  							end do
									end if

		      					else if (ix > 0) then
									do m=0, sumAngularMoment-a-b
			  							auxIntegrals(indexA,indexB,m) = PA(0)*auxIntegrals(indexA-auxAX,indexB,m) - &
			  															PC(0)*auxIntegrals(indexA-auxAX,indexB,m+1)
									end do

									if (ix > 1) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*(ix-1)*&
			    								(auxIntegrals(indexA-2*auxAX,indexB,m) - auxIntegrals(indexA-2*auxAX,indexB,m+1))
			  							end do
									end if

									if (jx > 0) then
			  							do m=0, sumAngularMoment-a-b
			    							auxIntegrals(indexA,indexB,m) = auxIntegrals(indexA,indexB,m) + twoZetaInv*jx*&
			    								(auxIntegrals(indexA-auxAX,indexB-auxBX,m) - auxIntegrals(indexA-auxAX,indexB-auxBX,m+1))
			  							end do
									end if

		      					else
									call Exception_constructor( ex , ERROR )
									call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the  oSRecursion function" )
									call Exception_setDescription( ex, "There is some error in the Attraction Integrals OSrecursion algorithm")
									call Exception_show( ex )
		      					end if
		    				end do
		  				end do
					end do
	      		end do
	    	end do
	  	end do

	end subroutine AttractionIntegrals_oSRecursion

end module AttractionIntegrals_
