!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia
!!    http://www.unal.edu.co
!!
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>
!!    Keywords:
!!
!!    thisPointer files is part of nonBOA
!!
!!    thisPointer program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    thisPointer program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details%
!!
!!    You should have received a copy of the GNU General Public License
!!    along with thisPointer program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************

!**
! @brief Modulo para administracion de particulas 
! 
!  Este modulo crear y manipular grupos de particulas cuanticas o puntuales
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-14
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulos y metodos basicos
!
! @see XMLParser_
!
!**
module ParticleManager_
	use Particle_
	use String_
	use ParticlesIDs_
	use ContractedGaussiansIDs_
	use ContractedGaussian_
	use AtomicElementManager_
	use ElementalParticleManager_
	use ConstantsOfCoupling_
	use XMLParser_
	use Map_
	use Units_
	use Vector_
	use Matrix_

	
	implicit none
	
	type , public :: ParticleManager

		character(30) :: name
		integer :: numberOfPuntualParticles
		integer :: numberOfQuantumParticles
		integer :: numberOfQuantumSpecies
        integer :: numberOfFragments
		integer :: totalNumberOfParticles
		integer :: sizeOfList
		integer :: currentSize					!< Atributo asignado por conveniencia
		integer :: iteratorOfSpecie				!< Atributo asignado por conveniencia
		logical :: isInstanced
		integer, allocatable :: centersOfOptimization(:,:)
		
		!!************************************************
		!! Informacion asociada a especies del sistema
		!!
		type(Map) :: speciesID	
		type(Map) :: mass	
		type(Map) :: charge
		type(Map) :: spin	
		type(Map) :: kappa	
		type(Map) :: eta	
		type(Map) :: lambda	
		type(Map) :: particlesFraction
		type(Map) :: basisSetName
		type(Map) :: numberOfContractions
		type(Map) :: numberOfParticles
		type(Map) :: ocupationNumber
		type(Map) :: multiplicity
		!!************************************************	
		
		type(Particle), allocatable :: particles(:)
		type(Particle), pointer :: particlesPtr(:)
		type(ParticlesIDs) :: idsOfPuntualParticles	
		type(ParticlesIDs) :: idsOfQuantumParticles	
		type(ContractedGaussiansIDs), allocatable :: idsOfContractionsForSpecie(:)
		
	end type ParticleManager

	!< enum ParticleManager__showFlags {
	integer, parameter :: LABELS_NUMERATED = 1

	!< }
	
	
	type(ParticleManager), target, public :: ParticleManager_instance
	

	public :: &
		ParticleManager_constructor, &
		ParticleManager_destructor, &
		ParticleManager_show, &
		ParticleManager_showParticlesInformation, &
		ParticleManager_addParticle, &
		ParticleManager_getAllParticlesOfSameSpecie,&
		ParticleManager_getParticlesOfSameSpecieAndBehavior, &
		ParticleManager_getParticlesWithSameBehavior,&
		ParticleManager_getNumberOfPuntualParticles, &
		ParticleManager_getNumberOfQuantumParticles, &
		ParticleManager_getNumberOfQuantumSpecies, &
		ParticleManager_getNumberOfParticles, &
		ParticleManager_getTotalNumberOfParticles, &
		ParticleManager_getNumberOfContractions, &
		ParticleManager_getTotalNumberOfContractions,&
		ParticleManager_getMaxAngularMoment,&
		ParticleManager_getTotalNumberOfPrimitives,&
		ParticleManager_getOriginOfPuntualParticle, &
		ParticleManager_getChargeOfPuntualParticle, &
		ParticleManager_getOrigin, &
		ParticleManager_getValuesOfFreeCoordinates, &
		ParticleManager_getPositionOfCenterOfOptimizacion, &
		ParticleManager_getNumberOfFreeCoordinates, &
		ParticleManager_getNumberOfCoordinates, &
		ParticleManager_getNumberOfCentersOfOptimization, &
		ParticleManager_getCartesianMatrixOfCentersOfOptimization, &
		ParticleManager_getDistanceMatrix, &
		ParticleManager_getCenterOfOptimization, &
		ParticleManager_getSpecieID, &
		ParticleManager_findSpecie, &
        ParticleManager_getBaseNameForSpecie, &
		ParticleManager_getNameOfSpecie, &
		ParticleManager_getName, &
		ParticleManager_getSymbol, &
		ParticleManager_getParticlePtr, &
		ParticleManager_getContractionPtr, &
		ParticleManager_getFactorOfInterchangeIntegrals, &
	 	ParticleManager_getOwnerOfPuntualParticle, &
		ParticleManager_getOwnerCenter, &
		ParticleManager_getNameOfPuntualParticle, &
		ParticleManager_getSymbolOfPuntualParticle, &
	 	ParticleManager_getOcupationNumber,&
		ParticleManager_getMultiplicity,&
		ParticleManager_getEta, &
		ParticleManager_getLambda,&
		ParticleManager_getKappa, &
		ParticleManager_getParticlesFraction,&
		ParticleManager_getMass, &
		ParticleManager_getMassInPosition, &
		ParticleManager_getTotalMass, &
		ParticleManager_getCenterOfMass, &
		ParticleManager_getCharge, &
		ParticleManager_getLabelsOfContractions, &
		ParticleManager_getLabelsOfCentersOfOptimization, &
		ParticleManager_getChargesOfCentersOfOptimization, &
		ParticleManager_isQuantum, &
		ParticleManager_isCenterOfOptimization, &
		ParticleManager_isComponentFixed, &
		ParticleManager_isFrozen, &
		ParticleManager_iterateSpecie, &
		ParticleManager_beginSpecie, &
		ParticleManager_endSpecie, &
		ParticleManager_rewindSpecies, &
		ParticleManager_setParticlesPositions, &
		ParticleManager_setOwner, &
		ParticleManager_puntualParticlesEnergy,&
		ParticleManager_changeOriginOfSystem
		
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine ParticleManager_constructor( numberOfParticles, name )
		implicit none
		integer :: numberOfParticles
		character(*), optional :: name
				
		ParticleManager_instance%name = "undefined"
		if ( present(name)) ParticleManager_instance%name = trim(name)
		
		ParticleManager_instance%numberOfQuantumParticles = 0
		ParticleManager_instance%iteratorOfSpecie = 0
		ParticleManager_instance%numberOfPuntualParticles = 0
		ParticleManager_instance%numberOfQuantumSpecies =0
        ParticleManager_instance%numberOfFragments = 1
		ParticleManager_instance%isInstanced = .true.
		ParticleManager_instance%particlesPtr => null()
		ParticleManager_instance%sizeOfList = numberOfParticles
		ParticleManager_instance%currentSize = 0
		allocate( ParticleManager_instance%particles(ParticleManager_instance%sizeOfList) )
		call Map_constructor( ParticleManager_instance%speciesID )
		call Map_constructor( ParticleManager_instance%mass )
		call Map_constructor( ParticleManager_instance%charge )
		call Map_constructor( ParticleManager_instance%spin )
		call Map_constructor( ParticleManager_instance%kappa )
		call Map_constructor( ParticleManager_instance%lambda )
		call Map_constructor( ParticleManager_instance%particlesFraction )
		call Map_constructor( ParticleManager_instance%basisSetName )
		call Map_constructor( ParticleManager_instance%eta )
		call Map_constructor( ParticleManager_instance%numberOfParticles )
		call Map_constructor( ParticleManager_instance%numberOfContractions )
		call Map_constructor( ParticleManager_instance%ocupationNumber )
		call Map_constructor( ParticleManager_instance%multiplicity )
		
		
	end subroutine ParticleManager_constructor

	
	
	!**
	! Define el destructor para clase
	!
	! @param thisPointer Funcion base
	!**
	subroutine ParticleManager_destructor()
		implicit none
		
		integer :: i
		integer :: j
		
		ParticleManager_instance%particlesPtr => null()
		
		if ( allocated ( ParticleManager_instance%particles ) ) then
			
			do i=1, size( ParticleManager_instance%particles )
				call Particle_destructor( ParticleManager_instance%particles(i) )
			end do
			
			deallocate(ParticleManager_instance%particles)
			
		end if



		if ( allocated ( ParticleManager_instance%idsOfContractionsForSpecie) )  &
			call ContractedGaussiansIDs_destructor( ParticleManager_instance%idsOfContractionsForSpecie )
			
		ParticleManager_instance%numberOfQuantumParticles = 0
		ParticleManager_instance%numberOfPuntualParticles = 0
		ParticleManager_instance%numberOfQuantumSpecies = 0
                ParticleManager_instance%numberOfFragments = 0
		ParticleManager_instance%isInstanced = .false.
		ParticleManager_instance%particlesPtr => null()
		ParticleManager_instance%sizeOfList = 0
		ParticleManager_instance%currentSize = 0
		call ParticlesIDs_destructor( ParticleManager_instance%idsOfPuntualParticles )
		call ParticlesIDs_destructor( ParticleManager_instance%idsOfQuantumParticles )
		call Map_destructor( ParticleManager_instance%speciesID )
		call Map_destructor( ParticleManager_instance%mass )
		call Map_destructor( ParticleManager_instance%charge )
		call Map_destructor( ParticleManager_instance%spin )
		call Map_destructor( ParticleManager_instance%kappa )
		call Map_destructor( ParticleManager_instance%lambda )
		call Map_destructor( ParticleManager_instance%particlesFraction )
		call Map_destructor( ParticleManager_instance%basisSetName )
		call Map_destructor( ParticleManager_instance%eta )
		call Map_destructor( ParticleManager_instance%numberOfParticles )
		call Map_destructor( ParticleManager_instance%numberOfContractions )
		call Map_destructor( ParticleManager_instance%ocupationNumber )
		call Map_destructor( ParticleManager_instance%multiplicity )

	end subroutine ParticleManager_destructor
				
	!**
	! @brief Muestra los atributos de todas las particulas en el Administrador de particulas
	!
	! @ Falta adicionar procediminetopara que muestre una sola particula
	!**
	subroutine ParticleManager_show( nameOfParticle )
		implicit none
		character(*), optional ::nameOfParticle

		integer :: i
		type(Exception) :: ex
				
		if ( associated(ParticleManager_instance%particlesPtr) ) then
			
			if ( present( nameOfParticle ) ) then
			
				!! Aqui adiconar codigo para una sola particula
			
			else
				
				do i=1, size(ParticleManager_instance%particlesPtr)
					
					call Particle_show( ParticleManager_instance%particlesPtr(i))
					
				end do
			
			end if
			
		else
		
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the show() function" )
			call Exception_setDescription( ex, "You should load something particle before use this function" )
			call Exception_show( ex )
		
		end if
		
		
	end subroutine ParticleManager_show

	!**
	! @brief Muestra los atributos de todas las particulas en el Administrador de particulas
	!
	! @ Falta adicionar procediminetopara que muestre una sola particula
	!**
	subroutine ParticleManager_showParticlesInformation()
		implicit none

		integer :: i
		integer :: j
		character(16) :: auxString
		real(8) :: origin(3)

		print *,""
		print *," INFORMATION OF PARTICLES :"
		print *,"========================="
		write (6,"(T10,A29,I8.0)") "Total number of particles   =", ParticleManager_instance%totalNumberOfParticles
		write (6,"(T10,A29,I8.0)") "Number of quantum particles =",ParticleManager_instance%numberOfQuantumParticles
		write (6,"(T10,A29,I8.0)") "Number of puntual charges   =", ParticleManager_instance%numberOfPuntualParticles
		write (6,"(T10,A29,I8.0)") "Number of quantum species   =", ParticleManager_instance%numberOfQuantumSpecies
		write (6,"(T10,A29,I8)") "Number of molecular frags%  =", ParticleManager_instance%numberOfFragments
				
		!!***********************************************************************
		!! Imprime iformaci�n sobre masa, carga y n�mero de part�culas encontradas
		!!
		print *,""
		print *,"                INFORMATION OF QUANTUM SPECIES "
		write (6,"(T7,A62)") "-------------------------------------------------------------------"
 		write (6,"(T8,A2,A5,A8,A5,A4,A5,A6,A5,A4,A5,A12)") "ID", " ","Symbol", " ","mass", " ","charge", " ","spin","","multiplicity"
		write (6,"(T7,A62)") "-------------------------------------------------------------------"
		
		do i = 1, ParticleManager_instance%numberOfQuantumSpecies
			write (6,'(T8,I3.0,A5,A5,A5,F7.1,A5,F5.1,A5,F5.2,A5,F5.2)') int(Map_getValue( ParticleManager_instance%speciesID, iterator=i )), " ",&
				Map_getKey( ParticleManager_instance%speciesID, iterator=i )," ",Map_getValue( ParticleManager_instance%mass, iterator=i ),&
				" ",Map_getValue( ParticleManager_instance%charge, iterator=i ), " ",Map_getValue( ParticleManager_instance%spin, iterator=i ), &
				" ",Map_getValue( ParticleManager_instance%multiplicity, iterator=i )
		end do

		print *,""
		print *,"                  CONSTANTS OF COUPLING "
		write (6,"(T7,A55)") "-------------------------------------------------------"
 		write (6,"(T10,A8,A5,A5,A5,A4,A5,A6,A5,A9)") "Symbol", " ","kappa", " ","eta", " ","lambda","","ocupation"
		write (6,"(T7,A55)") "-------------------------------------------------------"
		
		do i = 1, ParticleManager_instance%numberOfQuantumSpecies
			write (6,'(T10,A5,A5,F7.1,A5,F5.2,A5,F5.2,A5,F5.2,A5,F5.2)') &
				Map_getKey( ParticleManager_instance%speciesID, iterator=i )," ",Map_getValue( ParticleManager_instance%kappa, iterator=i ),&
				" ",Map_getValue( ParticleManager_instance%eta, iterator=i ), " ",Map_getValue( ParticleManager_instance%lambda, iterator=i ),&
				" ",Map_getValue( ParticleManager_instance%ocupationNumber, iterator=i )
		end do

		print *,""
		print *,"                  BASIS SET FOR SPECIES "
		write (6,"(T7,A55)") "-------------------------------------------------------"
		write (6,"(T10,A8,A5,A8,A5,A12,A5,A9)") "Symbol", " ","N. Basis", " ","N. Particles"," ","Basis Set" 
		write (6,"(T7,A55)") "-------------------------------------------------------"
		
		do i = 1, ParticleManager_instance%numberOfQuantumSpecies
			
			auxString = Map_getKey( ParticleManager_instance%basisSetName, iterator=i )
			auxString = auxString(1: scan( auxString, "/" ) - 1 )
			
			write (6,'(T10,A8,A5,I8,A5,I12,A5,A10)') &
				Map_getKey( ParticleManager_instance%speciesID, iterator=i )," ",&
				int(Map_getValue( ParticleManager_instance%numberOfContractions, iterator=i ))," ",&
				int(Map_getValue( ParticleManager_instance%numberOfParticles, iterator=i ))," ", trim(auxString)
				
		end do

		write (6,*) ""
		write (6,"(T10,A35)")"                     ATOMIC BASIS"
		write (6,"(T10,A60)") "------------------------------------------------------------"
			write (6,"(T10,A11,A9,A20,A20)") " PRIMITIVE ", "  SHELL  "," EXPONENT "," COEFFICIENT " 
			write (6,"(T10,A60)") "------------------------------------------------------------"
			
		do i =1, ParticleManager_instance%numberOfQuantumSpecies
			auxString=trim( Map_getKey( ParticleManager_instance%speciesID, iterator=i ) )
			
			write (6,*) ""
			write( 6, "(T5,A32,A5)") "BEGIN DESCRIPTION OF BASIS FOR: ", trim( Map_getKey( ParticleManager_instance%speciesID, iterator=i ) )
			write (6,"(T5,A30)") "================================"
			write (6,*) ""

			do j =1, size(ParticleManager_instance%particlesPtr)
				if (	trim(ParticleManager_instance%particlesPtr(j)%symbol) == trim(auxString)	.and. &
					ParticleManager_instance%particlesPtr(j)%isQuantum ) then
					call BasisSet_showInCompactForm( 	ParticleManager_instance%particlesPtr(j)%basis,&
					trim(ParticleManager_instance%particlesPtr(j)%nickname) )
				end if
				
			end do
			write (6,"(T5,A28)") "... END DESCRIPTION OF BASIS"
			write (6,*) ""
		end do


		print *,""
		print *," END INFORMATION OF PARTICLES"
		print *,""

	end subroutine ParticleManager_showParticlesInformation

	!**
	! @brief Adiciona una nueva particula al administrador de particulas
	!
	!**
	subroutine ParticleManager_addParticle( name, baseName, origin, fix, multiplicity, addParticles, fragmentNumber )
		implicit none
		character(*), intent(in) :: name
		character(*), intent(in), optional :: baseName
		character(*), intent(in), optional :: fix
		real(8), intent(in), optional :: origin(3)
		real(8), intent(in), optional :: multiplicity
		integer, intent(in), optional :: addParticles
        integer, intent(in), optional :: fragmentNumber
		
		type(AtomicElement) :: element
		type(ElementalParticle) :: eparticle
		type(ConstantsOfCoupling) :: couplingConstants
		character(50) :: auxName
		character(50) :: internalName
		character(50) :: auxNickname
		character(10) :: auxMassicNumber
		character(3) :: varsToFix
		character(5) :: massNumberString
		real(8) :: auxOrigin(3)
		type(Particle), pointer :: lastParticle
		integer :: i
		integer :: j
		integer :: auxNumOfParticles
		integer :: auxAddionOfParticles
       	integer :: auxFragmentNumber
		logical :: auxIsDummy
		real(8) :: auxVal
		real(8) :: auxNum
		real(8) :: auxMultiplicity
		real(8), external ::  molecularsystem__mp_molecularsystem_getcharge
		logical :: isElectronicParticle
		
		isElectronicParticle =.false.

		varsToFix=""
		if ( present(fix) ) varsToFix =trim(fix)
		
		auxOrigin=[0.0_8,0.0_8,0.0_8]
		if   ( present(origin) ) auxOrigin= origin
		
		auxMultiplicity=0.0_8
		if   ( present(multiplicity) ) auxMultiplicity=multiplicity

        auxFragmentNumber=1
		if   ( present(fragmentNumber) ) auxFragmentNumber=fragmentNumber
		
		auxAddionOfParticles=0
		if   ( present(addParticles) ) auxAddionOfParticles= addParticles
		
		!!****************************************************************************
		!! Obtiene informacion de un nucleo atomico 
		!!
		call AtomicElement_constructor( element )
		
		auxIsDummy =.false.
		auxName = ""
		auxMassicNumber = ""
		auxNickname=""
		auxNickname  = trim(name)
		internalName = trim(name)
		auxName = trim(internalName)
		
		!! Verifica si se trata de una particula dummy
		if (  scan( internalName, "*" ) /= 0 ) then

			internalName = internalName(1: scan( internalName, "*" ) - 1 )
			auxName = trim(internalName)
			auxNickname=trim(internalName)
			auxIsDummy =.true. 
			APMO_instance%ARE_THERE_DUMMY_ATOMS = .true.
			
		end if
		
		!! Verifica si se trata de una una base electronica
		if (  ( scan( internalName, "e" ) == 1 ) .and. ( scan( internalName, "-" ) > 1 ) )then
			
			if(  scan( internalName, "[" ) /= 0 ) then

				internalName = internalName(scan( internalName, "[" )+1:scan( internalName, "]" ) - 1  )
				auxName = trim(internalName)
				isElectronicParticle=.true.
			else
				auxName=trim(internalName)
			end if
		end if

		!! Verifica si se trata de un nucleo con especificacion de numero de masa
		if (  scan( internalName, "_" ) /= 0 ) then

			auxName = internalName(1: scan( internalName, "_" ) - 1 )
			!! Obtiene el numero de masa especificado
			auxMassicNumber = internalName( scan( internalName, "_" )+1 : len( trim(internalName) ) ) 
		
		end if

		!! Obtiene infomacion del elemento atomico especificado (de su simbolo)
		if ( trim(auxMassicNumber) /= "" ) then

			call AtomicElementManager_loadElement( element, trim(auxName), massicNumber=trim ( auxMassicNumber ) )

		else

			call AtomicElementManager_loadElement ( element, trim(auxName) )
			
		end if

		!!
		!!****************************************************************************

        if( auxFragmentNumber > ParticleManager_instance%numberOfFragments ) &
                    ParticleManager_instance%numberOfFragments=auxFragmentNumber

		ParticleManager_instance%currentSize=ParticleManager_instance%currentSize+1
		ParticleManager_instance%particlesPtr => ParticleManager_instance%particles(1:ParticleManager_instance%currentSize)
		
		auxNumOfParticles = ParticleManager_instance%currentSize
		lastParticle => ParticleManager_instance%particlesPtr( auxNumOfParticles )
		!!
		!!****************************************************************************

		!!****************************************************************************
		!! Carga informacion sobre un particula cuantica
		!!
 		if ( present(baseName) .and. ( trim(baseName) /= "dirac") ) then

			if ( element%isInstanced ) then

				!! Carga informacion asociada a electrones	
				if ( isElectronicParticle ) then
					
					call Particle_constructor( this=lastParticle, isQuantum= .true. , origin=auxOrigin, &
                                                basisSetName=trim(baseName), &
						elementName=trim(internalName), isDummy= auxIsDummy,&
                                                owner=auxNumOfParticles,nickname=trim(auxNickname),&
                                                fragmentNumber=auxFragmentNumber )

					call Particle_setComponentFixed( lastParticle, varsToFix )

					lastParticle%name = "e-"
					lastParticle%symbol = "e-"
					lastParticle%charge = PhysicalConstants_ELECTRON_CHARGE
					lastParticle%mass = PhysicalConstants_ELECTRON_MASS
					lastParticle%totalCharge = -element%atomicNumber
					lastParticle%internalSize = element%atomicNumber
					lastParticle%statistics="fermion"
					lastParticle%spin = PhysicalConstants_SPIN_ELECTRON
				
				!! Carga informacion asociada a nucleos
				else 

					call Particle_constructor( this=lastParticle, isQuantum= .true. , origin=auxOrigin,&
                                                basisSetName=trim(baseName), &
						elementName=trim(auxName), isDummy= auxIsDummy, owner=auxNumOfParticles, &
						massNumber= int(element%massicNumber),nickname=trim(auxNickname), &
                                                fragmentNumber=auxFragmentNumber )

					call Particle_setComponentFixed( lastParticle, varsToFix )
					lastParticle%name = element%name
					massNumberString = adjustl( String_convertIntegerToString( int(element%massicNumber ) ) )
				 	lastParticle%symbol = trim(auxName)//"_"//trim(massNumberString)
					lastParticle%charge = element%atomicNumber
					call AtomicElement_show( element )
					lastParticle%mass = element%atomicWeight*1822.88839_8
! 					lastParticle%mass = element%massicNumber * PhysicalConstants_NEUTRON_MASS &
! 						+ element%atomicNumber * (PhysicalConstants_PROTON_MASS - PhysicalConstants_NEUTRON_MASS)

					lastParticle%totalCharge = element%atomicNumber
					lastParticle%internalSize = 1
					
					!! Identifica la estadistica de la particula
					if ( Math_isEven( INT( element%massicNumber ) ) ) then
						
						lastParticle%statistics = "boson"
						if ( Math_isEven( INT( element%atomicNumber ) ) ) then
							lastParticle%spin = 0.0_8
						else
							lastParticle%spin = element%nuclearSpin
						end if
					
					else
					
						lastParticle%statistics = "fermion"
						lastParticle%spin = element%nuclearSpin
						
					end if
				
				end if

			!! Carga informacion asociada a particulas elementales
			else  

				call Particle_constructor( this=lastParticle, isQuantum= .true. , origin=auxOrigin, &
                                                basisSetName=trim(baseName), &
						elementName=trim(internalName), isDummy= auxIsDummy, &
                                                owner=auxNumOfParticles,nickname=trim(auxNickname), &
                                                fragmentNumber=auxFragmentNumber )

				!! Obtiene informacion de una particula elemental
				call ElementalParticle_constructor( eparticle )
				call ElementalParticleManager_loadParticle( eparticle, trim( auxName ) )
				
				lastParticle%name = eParticle%name
				lastParticle%symbol = eParticle%symbol
				lastParticle%charge = eParticle%charge
				lastParticle%mass = eParticle%mass
				lastParticle%totalCharge = eParticle%charge
				lastParticle%internalSize = 1
				lastParticle%spin = eParticle%spin
				
				!! Identifica la estadistica de la particula
				if ( abs( ceiling( abs( eParticle%spin ) ) - abs( eParticle%spin ) ) < APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
					lastParticle%statistics = "boson"
				else
					lastParticle%statistics = "fermion"
				end if
				
				call ElementalParticle_destructor()
			
			end if
				
			!!
			!!********************************************************************************
			
			!!********************************************************************************
			!! Actualiza informacion global sobre particulas y especies presentes
			!!

			!! Inicializa informacion para la especie que se ingresa por primera vez
			if ( Map_find( ParticleManager_instance%speciesID, trim( lastParticle%symbol )) == 0  ) then

				!!***************************************************************************
				!! Ajuste de constantes de acoplamiento para la nueva especie encontrada
				!!
				call ConstantsOfCoupling_constructor( couplingConstants )
				call XMLParser_constructor( trim( trim( APMO_instance%DATA_DIRECTORY ) // "/dataBases/constantsOfCoupling.xml"), &
					lastParticle%symbol, couplingConstants=couplingConstants )
				call XMLParser_destructor()
				
				!! Busca constantes de acoplamineto en la base de datos
				if ( couplingConstants%isInstanced ) then
					
					call Map_insert( ParticleManager_instance%kappa, lastParticle%symbol, couplingConstants%kappa )
					call Map_insert( ParticleManager_instance%eta, lastParticle%symbol, couplingConstants%eta )
					call Map_insert( ParticleManager_instance%lambda, lastParticle%symbol, couplingConstants%lambda )
					call Map_insert( ParticleManager_instance%particlesFraction, lastParticle%symbol, couplingConstants%particlesFraction )
				
				!!
				!! Si no encuentra informacion de las constantes de acoplamiento, asigna valores por omision
				!! de acuedo con la estadistica de la particula
				!!
				else
					
					!! Constantes de acoplamiento para fermiones
					if ( trim(lastParticle%statistics) == "fermion" ) then
						
						call Map_insert( ParticleManager_instance%kappa, lastParticle%symbol, -1.0_8 )
						call Map_insert( ParticleManager_instance%eta, lastParticle%symbol, 2.0_8 )
						call Map_insert( ParticleManager_instance%lambda, lastParticle%symbol, 2.0_8 )
						call Map_insert( ParticleManager_instance%particlesFraction, lastParticle%symbol, 0.5_8 )
					
					!! Constantes de acoplamiento para bosones
					else 
					
						call Map_insert( ParticleManager_instance%kappa, lastParticle%symbol, -1.0_8 )
						call Map_insert( ParticleManager_instance%eta, lastParticle%symbol, 1.0_8 )
						call Map_insert( ParticleManager_instance%lambda, lastParticle%symbol, 1.0_8 )
						call Map_insert( ParticleManager_instance%particlesFraction, lastParticle%symbol, 1.0_8 )
					
					end if
				
				end if
				
				!!***************************************************************************
				
				ParticleManager_instance%numberOfQuantumSpecies = ParticleManager_instance%numberOfQuantumSpecies + 1
				
				!! Notese que el ID se asigna de acuerdo al orden de entrada de la particula en el administrador
				call Map_insert( ParticleManager_instance%speciesID, lastParticle%symbol,&
					real( ParticleManager_instance%numberOfQuantumSpecies, 8 ) )
					
				!!
				!! Solo se almacena el nombre de la base asociado a la  primera particula que de ingrese al administrador
				!! sin embargo si se especifica una base "diferente" para la misma especie se tiene en cuenta dentro del calculo.
				!!
				call Map_insert( ParticleManager_instance%basisSetName, trim(baseName)//"/"//trim(lastParticle%symbol), &
				real( ParticleManager_instance%numberOfQuantumSpecies, 8 ) )
				call Map_insert( ParticleManager_instance%mass, lastParticle%symbol,lastParticle%mass )
				call Map_insert( ParticleManager_instance%charge, lastParticle%symbol,lastParticle%charge )
				call Map_insert( ParticleManager_instance%spin, lastParticle%symbol,lastParticle%spin )
				
				call Map_insert( ParticleManager_instance%numberOfContractions, lastParticle%symbol, real(lastParticle%basisSetSize,8) )
				
				!! Inserta una nueva estructura para almacenamiento de indices de contracciones de la nueva especie 
				call ContractedGaussiansIDs_addVault(ParticleManager_instance%idsOfContractionsForSpecie)
				
				!! Adiciona los indices (IDs) asociados a las contracciones de una nueva particula  
				do j=1, size( lastParticle%basis%contractions )
					
					call ContractedGaussiansIDs_add(ParticleManager_instance%idsOfContractionsForSpecie( &
							ParticleManager_instance%numberOfQuantumSpecies) , auxNumOfParticles, j )
				
				end do
				
    if ( trim(lastParticle%symbol)=="e-" ) then
       auxNum= lastParticle%internalSize + auxAddionOfParticles
       ParticleManager_instance%numberOfQuantumParticles = ParticleManager_instance%numberOfQuantumParticles  + auxAddionOfParticles
       if ( lastParticle%isDummy ) auxNum = 0
       
    else
       auxNum= lastParticle%internalSize+ auxAddionOfParticles
       if ( lastParticle%isDummy ) auxNum = 0
    end if
    
								
				call Map_insert( ParticleManager_instance%numberOfParticles, lastParticle%symbol,  auxNum )
				

				call Map_insert( ParticleManager_instance%ocupationNumber, lastParticle%symbol, auxNum * &
					ParticleManager_instance%particlesFraction%value(ParticleManager_instance%numberOfQuantumSpecies) )
					
				call Map_insert( ParticleManager_instance%multiplicity, lastParticle%symbol,  multiplicity )
				
				call ConstantsOfCoupling_destructor()
				
			!! Actualiza informacion de la especie si ya se encontraba en el administrador de particulas
			else
			
				!! Suma las contraccione asociadas a la ultima particula especificada
				i = Map_find( ParticleManager_instance%numberOfContractions,trim( lastParticle%symbol ) )
				ParticleManager_instance%numberOfContractions%value(i) = ParticleManager_instance%numberOfContractions%value(i) +  &
					lastParticle%basisSetSize
				
				!! Adiciona los ids de particula asociados a las nuevas funciones base
				do j=1, size( lastParticle%basis%contractions )
					call ContractedGaussiansIDs_add(ParticleManager_instance%idsOfContractionsForSpecie(i) , auxNumOfParticles, j )
				end do
				
				!! Evita la suma del tama�o de la particula si se trata de una particula virtual "dummy"
				if ( .not.lastParticle%isDummy ) then
					
					i = Map_find( ParticleManager_instance%numberOfParticles, trim( lastParticle%symbol ) )
					ParticleManager_instance%numberOfParticles%value(i) = ParticleManager_instance%numberOfParticles%value(i) &
					+  lastParticle%internalSize + auxAddionOfParticles
						
					ParticleManager_instance%ocupationNumber%value( i) = ParticleManager_instance%particlesFraction%value(i) *  &
					ParticleManager_instance%numberOfParticles%value(i)

				end if
				
			end if

			if ( .not.lastParticle%isDummy ) then
				ParticleManager_instance%numberOfQuantumParticles = ParticleManager_instance%numberOfQuantumParticles &
					+ lastParticle%internalSize
				
				!! Actualiza la lista de IDs asociados particulas cuanticas no virtuales 
				call ParticlesIDs_add( ParticleManager_instance%idsOfQuantumParticles, auxNumOfParticles)
			end if
			!!********************************************************************************
			
		!!
		!!****************************************************************************

		!!****************************************************************************
		!! Carga informacion sobre una particulas fijas
		!!
		else

			call Particle_constructor( lastParticle, isQuantum=.false. , name=trim(auxName), origin=auxOrigin, &
				owner=auxNumOfParticles,nickname=trim(auxNickname), &
                                fragmentNumber=auxFragmentNumber )
			call Particle_setComponentFixed(lastParticle, varsToFix )
			lastParticle%basisSetName="dirac"
			
			!! Carga informacion asociada a un nucleo atomico fijo
			if ( element%isInstanced ) then
				
				lastParticle%name = element%name
				lastParticle%symbol = trim(internalName)
				lastParticle%charge = element%atomicNumber
				lastParticle%mass = element%atomicWeight*1822.88839_8
!				lastParticle%mass = element%massicNumber * PhysicalConstants_NEUTRON_MASS &
!						+ element%atomicNumber * (PhysicalConstants_PROTON_MASS - PhysicalConstants_NEUTRON_MASS)
				lastParticle%totalCharge = element%atomicNumber
				lastParticle%internalSize = 1
				
				if ( Math_isEven( INT( element%massicNumber ) ) ) then
						
						lastParticle%statistics = "boson"
						if ( Math_isEven( INT( element%atomicNumber ) ) ) then
							lastParticle%spin = 0.0_8
						else
							lastParticle%spin = element%nuclearSpin
						end if
					
					else
					
						lastParticle%statistics = "fermion"
						lastParticle%spin = element%nuclearSpin
						
				end if

			!! Carga informacion asociada a una particula elemental fija
			else
					
				call ElementalParticle_constructor( eparticle )
				call ElementalParticleManager_loadParticle( eparticle, trim( auxName ) )
				
				lastParticle%name = eParticle%name
				lastParticle%symbol = eParticle%symbol
				lastParticle%charge = eParticle%charge
				lastParticle%mass = eParticle%mass
				lastParticle%totalCharge = eParticle%charge
				lastParticle%internalSize = 1
				lastParticle%spin = eParticle%spin
					
			end if
			
			ParticleManager_instance%numberOfPuntualParticles = ParticleManager_instance%numberOfPuntualParticles + 1
			call ParticlesIDs_add( ParticleManager_instance%idsOfPuntualParticles, auxNumOfParticles)
 		
		end if
		
		!!****************************************************************************
		
		if ( .not.lastParticle%isDummy ) then

			ParticleManager_instance%totalNumberOfParticles = ParticleManager_instance%numberOfQuantumParticles &
						+ ParticleManager_instance%numberOfPuntualParticles
		end if
		
		call ParticleManager_setOwner()
		call AtomicElement_destructor(element) 
		call ElementalParticle_destructor()
		lastParticle => null()		
		
	end subroutine ParticleManager_addParticle
	
 !**
 ! @brief Ajusta el owner de la particula especificada
 !
 !**
 subroutine ParticleManager_setOwner()
   implicit none
   integer :: i
   integer :: particleID
   
   particleID = size( ParticleManager_instance%particlesPtr )
   
   do i=1, particleID - 1
      
      if( 	abs( ParticleManager_instance%particlesPtr(i)%origin(1) - ParticleManager_instance%particlesPtr(particleID)%origin(1) ) &
           < APMO_instance%DOUBLE_ZERO_THRESHOLD .and. &
          abs( ParticleManager_instance%particlesPtr(i)%origin(2) - ParticleManager_instance%particlesPtr(particleID)%origin(2) ) &
          < APMO_instance%DOUBLE_ZERO_THRESHOLD .and. &
          abs( ParticleManager_instance%particlesPtr(i)%origin(3) - ParticleManager_instance%particlesPtr(particleID)%origin(3) ) &
          < APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
        
         !!
         !! Selecciona quien debe ser  la particula propietaria, cuando encuentra dos
         !! especies sobre el mismo origen. Se opta por la mas pesada como la propietaria
         !! y la liviana como hija
         !!
         if ( ParticleManager_instance%particlesPtr( particleID )%mass  > ParticleManager_instance%particlesPtr(i)%mass )	then
            
            
           call Particle_setOwner(ParticleManager_instance%particlesPtr(i), owner = particleID )
           ParticleManager_instance%particlesPtr(particleID)%isCenterOfOptimization =.true.
           ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization =.false.
           call Particle_setChild(ParticleManager_instance%particlesPtr(particleID), i)
           call Particle_removeChilds( ParticleManager_instance%particlesPtr(i) )
           
        else 
           
           call Particle_setOwner( ParticleManager_instance%particlesPtr(particleID), &
                owner= ParticleManager_instance%particlesPtr(i)%owner )
           call Particle_removeChilds( ParticleManager_instance%particlesPtr(particleID) )
           ParticleManager_instance%particlesPtr(particleID)%isCenterOfOptimization =.false.
           ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization =.true.
           
           return
           
        end if
     end if
     
     
  end do
  
end subroutine ParticleManager_setOwner

	!**
	! @brief Devuelve un arreglo de punteros a todas las particulas del mismo tipo sean cuanticas o no
	!
	! @todo falta implementacion completa
	!**
	subroutine ParticleManager_getAllParticlesOfSameSpecie( allParticlesPtr, symbolOfSpecie )
		implicit none
		type(ParticlesIDs), intent(inout) :: allParticlesPtr
		character(*), optional, intent(in) :: symbolOfSpecie
		
			
	end subroutine ParticleManager_getAllParticlesOfSameSpecie
	
	
	!**
	! @brief Devuelve un arreglo de punteros a todas las particulas de la misma especie
	! y con el mismo comportamiento. Por omisi�n devuelve las particulas
	! cu�nticas de la misma especie.
	!
	! @todo falta implementacion completa
	!**
	subroutine ParticleManager_getParticlesOfSameSpecieAndBehavior ( particlesPtr, symbolOfSpecie, isQuantum )
		implicit none
		type(ParticlesIDs), intent(inout) :: particlesPtr
		character(*), optional, intent(in) :: symbolOfSpecie
		logical, optional, intent(in) :: isQuantum

	end subroutine ParticleManager_getParticlesOfSameSpecieAndBehavior
	
	!**
	! @brief Devuelve un arreglo de punteros al todas las particulas del mismo 
	! bien sean cuanticas o cargas puntuales% Por omisi�n devuelve las particulas
	! cu�nticas del mismo tipo
	!
	! @todo falta implementacion completa
	!**
	subroutine ParticleManager_getParticlesWithSameBehavior( particlesPtr, isQuantum )
		implicit none
		type(ParticlesIDs), intent(inout) :: particlesPtr
		logical, optional, intent(in) :: isQuantum

	end subroutine ParticleManager_getParticlesWithSameBehavior


	!**
	! @brief Retorna el numero de particulas puntuales en el administrador de particulas
	!
	!**
	function ParticleManager_getNumberOfPuntualParticles() result( output )
		implicit none
		integer :: output
		
		output = ParticleManager_instance%numberOfPuntualParticles
			
	end function ParticleManager_getNumberOfPuntualParticles
	
	!**
	! @brief Retorna el numero de particulas cuanticas en el administrador de particulas
	!
	!**
	function ParticleManager_getNumberOfQuantumParticles() result( output )
		implicit none
		integer :: output
		
		output = ParticleManager_instance%numberOfQuantumParticles
		
	end function ParticleManager_getNumberOfQuantumParticles
	
	
	!**
	! @brief Retorna el numero de particulas cuanticas en el administrador de particulas
	!
	!**
	function ParticleManager_getNumberOfParticles( specieID ) result( output )
		implicit none
		integer :: specieID
		integer :: output
		
		output = int( Map_getValue( ParticleManager_instance%numberOfParticles, iterator= specieID ) )
		
	end function ParticleManager_getNumberOfParticles
	
	!**
	! @brief Retorna el numero total de particulas presentes en el sistema 
	!
	!**
	function ParticleManager_getTotalNumberOfParticles() result( output )
		implicit none
		integer :: output
		
		output = size(ParticleManager_instance%particlesPtr)
		
	end function ParticleManager_getTotalNumberOfParticles
	
	
	!**
	! @brief Retorna el numero de especies cuanticas en el administrador de particulas
	!
	!**
	function ParticleManager_getNumberOfQuantumSpecies() result( output )
		implicit none
		integer :: output
		
		output = ParticleManager_instance%numberOfQuantumSpecies
		
	end function ParticleManager_getNumberOfQuantumSpecies

	!**
	! @brief Retorna el numero de contracciones para una especie especificada
	!
	!**
	function ParticleManager_getNumberOfContractions( specieID ) result( output )
		implicit none
		integer :: specieID
		integer :: output
		
		output = ContractedGaussiansIDs_getSize( ParticleManager_instance%idsOfContractionsForSpecie(specieID) )
			
	end function ParticleManager_getNumberOfContractions
	
	!**
	! @brief Retorna el numero de contracciones por indice de momento angular para una especie especificada
	!
	!**
	function ParticleManager_getTotalNumberOfContractions( specieID ) result( output )
		implicit none
		integer :: specieID
		integer :: output
		integer :: i, j
		integer :: numberOfContractions
		integer :: particleID
		integer :: contractionID

		output = 0

		do i=1, ParticleManager_instance%numberOfQuantumSpecies

			if ( specieID ==  i ) then

				numberOfContractions = int( Map_getValue(ParticleManager_instance%numberOfContractions, iterator = i ) )

				do j=1, numberOfContractions

					particleID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%particleID
					contractionID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%contractionIDInParticle

  					output = output + ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%numCartesianOrbital

				end do

			end if

		end do

	end function ParticleManager_getTotalNumberOfContractions

	!**
	! @brief Retorna el valor del momento angular maximo del sistema
	!
	!**
	function ParticleManager_getMaxAngularMoment() result(output)
		implicit none
		integer :: output

		integer :: i, j
		integer :: auxAngularMoment
		integer :: numberOfContractions
		integer :: particleID
		integer :: contractionID

		auxAngularMoment = 0
		output = 0

		do i=1, ParticleManager_instance%numberOfQuantumSpecies

			numberOfContractions = int( Map_getValue(ParticleManager_instance%numberOfContractions, iterator = i ) )

			do j=1, numberOfContractions

				particleID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%particleID
				contractionID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%contractionIDInParticle

  				auxAngularMoment = sum(ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%angularMomentIndex)
  				output = max(output, auxAngularMoment)

			end do

		end do

	end function ParticleManager_getMaxAngularMoment

	!**
	! @brief Retorna el numero de primitives par el sistema
	!
	!**
	function ParticleManager_getTotalNumberOfPrimitives() result(output)
		implicit none
		integer :: output

		integer :: i, j
		integer :: numberOfContractions
		integer :: particleID
		integer :: contractionID

		output = 0

		do i=1, ParticleManager_instance%numberOfQuantumSpecies

			numberOfContractions = int( Map_getValue(ParticleManager_instance%numberOfContractions, iterator = i ) )

			do j=1, numberOfContractions

				particleID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%particleID
				contractionID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%contractionIDInParticle

  				output = output + ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%length * &
  								  ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%numCartesianOrbital

			end do

		end do

	end function ParticleManager_getTotalNumberOfPrimitives

	!**
	! @brief Retorna un ID asociado a la especie especificada
	!
	!**
	function ParticleManager_getSpecieID( iteratorOfSpecie, nameOfSpecie ) result( output )
		implicit none
		integer, intent(in), optional :: iteratorOfSpecie
		character(*), intent(in), optional :: nameOfSpecie
		integer :: output
		
		type(Exception) :: ex
		
		
		if ( ParticleManager_instance%isInstanced ) then
			
			if ( present(iteratorOfSpecie) ) then
				
				output = int( Map_getValue( ParticleManager_instance%speciesID,iterator=iteratorOfSpecie ) )
				
			
			else if ( present(nameOfSpecie) ) then
				
				output = int( Map_getValue( ParticleManager_instance%speciesID, trim(nameOfSpecie ) ) )
				
			
			end if
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the getNumberOfContraction function" )
			call Exception_setDescription( ex, "You should instance the ParticleManager before use this function" )
			call Exception_show( ex )
		end if

	end function ParticleManager_getSpecieID


	!**
	! @brief Retorna un ID asociado a la especie especificada
	!
	!**
	function ParticleManager_findSpecie( nameOfSpecie ) result( output )
		implicit none
		character(*), intent(in) :: nameOfSpecie
		integer :: output
		
		type(Exception) :: ex
		
		
		if ( ParticleManager_instance%isInstanced ) then
			
				
				output = int( Map_find( ParticleManager_instance%speciesID, trim(nameOfSpecie ) ) )
				
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the getNumberOfContraction function" )
			call Exception_setDescription( ex, "You should instance the ParticleManager before use this function" )
			call Exception_show( ex )
		end if

	end function ParticleManager_findSpecie



	!**
	! @brief Retorna un ID asociado a la especie especificada
	!
	!**
	function ParticleManager_getBaseNameForSpecie( nameOfSpecie ) result( output )
		implicit none
		character(*), intent(in) :: nameOfSpecie
		character(50) :: output
                integer :: speciesID			
		
                speciesID = int( Map_getValue( ParticleManager_instance%speciesID, trim(nameOfSpecie ) ) )
                output=trim(Map_getKey( ParticleManager_instance%basisSetName, iterator=speciesID ) )
                output = output(1: scan( output, "/" ) - 1 )
			

	end function ParticleManager_getBaseNameForSpecie
	
	!**
	! @brief Indica si la particula es un centro de optimizacion o no
	!
	!**
	function ParticleManager_isCenterOfOptimization( iterator ) result( output )
		implicit none
		integer, intent(in) :: iterator
		logical :: output
		
		output = ParticleManager_instance%particlesPtr(iterator)%isCenterOfOptimization

	end function ParticleManager_isCenterOfOptimization



	!**
	! @brief Indica si la componete cartesiana debe modificarse durante la optimzacion de geometria
	!
	!**
	function ParticleManager_isComponentFixed( iterator, component ) result( output )
		implicit none
		integer, intent(in) :: iterator
		integer, intent(in) :: component
		logical :: output
		
		output = ParticleManager_instance%particlesPtr(iterator)%fixComponent(component)
			
	end function ParticleManager_isComponentFixed
	
	!**
	! @brief Indica si la especie ha sido congelada o no
	!
	!**
	function ParticleManager_isFrozen( nameOfSpecie ) result(output)
		implicit none

		character(*), optional :: nameOfSpecie
		logical :: output
		integer :: i

		output = .false.


		do i = 1, size(APMO_instance%FROZEN_PARTICLE)
			if( trim(APMO_instance%FROZEN_PARTICLE(i)) == trim(nameOfSpecie) ) then
				output = .true.
				return
			end if
		end do

!		print*, trim(nameOfSpecie)
!		print*, trim(APMO_instance%FROZEN_PARTICLE)
!		print*, trim(output)

	end function ParticleManager_isFrozen

	!**
	! @brief Retorna el nombre de especie cuantica 
	!
	!**
	function ParticleManager_iterateSpecie() result( output )
		implicit none
		character(30) :: output
		
		ParticleManager_instance%iteratorOfSpecie =ParticleManager_instance%iteratorOfSpecie+ 1
		
		if ( ParticleManager_instance%iteratorOfSpecie > ParticleManager_instance%numberOfQuantumSpecies ) &
			ParticleManager_instance%iteratorOfSpecie = 1
		
		if ( ParticleManager_instance%isInstanced ) then
			
			output = trim( Map_getKey( ParticleManager_instance%speciesID,iterator=ParticleManager_instance%iteratorOfSpecie ) )
		else
		
			call abort(" Error in function: ParticleManager_iterateSpecie() ")
			
		end if

	end function ParticleManager_iterateSpecie
	
	!**
	! @brief Retorna el nombre de especie cuantica 
	!
	!**
	subroutine ParticleManager_rewindSpecies() 
		implicit none
		
		if ( ParticleManager_instance%isInstanced ) then
			
			ParticleManager_instance%iteratorOfSpecie = 0
			
		end if

	end subroutine ParticleManager_rewindSpecies
	
	!**
	! @brief Retorna un ID asociado a la especie especificada
	!
	!**
	function ParticleManager_getNameOfSpecie( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		character(30) :: output
		
		type(Exception) :: ex
		
		if ( ParticleManager_instance%isInstanced ) then
			
			output = Map_getKey( ParticleManager_instance%speciesID, real(specieID,8) )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the getNumberOfContraction function" )
			call Exception_setDescription( ex, "You should instance the ParticleManager before use this function" )
			call Exception_show( ex )
		end if

	end function ParticleManager_getNameOfSpecie
	
	!**
	!   @brief Retorna un iterador a laprimera especie en el sistema molecular
	!
	!**
	function ParticleManager_beginSpecie() result( output )
		implicit none
		integer :: output
		
		type(Exception) :: ex
		
		if ( ParticleManager_instance%isInstanced ) then
			
			output = Map_begin(ParticleManager_instance%speciesID )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the getBeginSpecie function" )
			call Exception_setDescription( ex, "You should instance the ParticleManager before use this function" )
			call Exception_show( ex )
		end if

	end function ParticleManager_beginSpecie
	
	!**
	!   @brief Retorna un iterador a la ultima especie en el sistema molecular
	!
	!**
	function ParticleManager_endSpecie() result( output )
		implicit none
		integer :: output
		
		type(Exception) :: ex
		
		if (ParticleManager_instance%isInstanced ) then
			
			output = Map_end( ParticleManager_instance%speciesID )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object ParticleManager in the getLastSpecie function" )
			call Exception_setDescription( ex, "You should instance the ParticleManager before use this function" )
			call Exception_show( ex )
		end if

	end function ParticleManager_endSpecie
	
	!**
	! Retorna un puntero la contraccion iesima del tipo de particula dado
	!
	! @todo Adicionar condicionales para verificar limites de especie o contraccion, antes de devolverla
	!**
	function ParticleManager_getContractionPtr( specieID, numberOfContraction ) result( output )
		implicit none
		integer, intent(in) :: specieID
		integer, intent(in) :: numberOfContraction
		type(ContractedGaussian), pointer :: output
		
		integer :: particleID
		integer :: contractionID
		
		particleID = ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(numberOfContraction)%particleID
		contractionID=ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(numberOfContraction)%contractionIDInParticle

		output => ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)

	end function ParticleManager_getContractionPtr
	
	!**
	! @brief Retorna el origen de la particula puntual especificada
	!
	!**
	function ParticleManager_getOriginOfPuntualParticle( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output(3)
		
		output = ParticleManager_instance%particlesPtr( ParticleManager_instance%idsOfPuntualParticles%particleID( specieID ) )%origin
		
	end function ParticleManager_getOriginOfPuntualParticle
	
	!**
	! @brief Retorna el origen de la particula especificada
	!
	!**
	function ParticleManager_getOrigin( iterator ) result( output )
		implicit none 
		integer, intent(in) :: iterator
		real(8) :: output(3)
		
		output = ParticleManager_instance%particlesPtr( iterator )%origin
		
	end function ParticleManager_getOrigin
	
	!**
	! @brief Retorna el nombre de la particula especificada
	!
	!**
	function ParticleManager_getName( iterator ) result( output )
		implicit none 
		integer, intent(in) :: iterator
		character(30) :: output
		
		output = trim( ParticleManager_instance%particlesPtr( iterator )%name )
		
		
	end function ParticleManager_getName
	
	!**
	! @brief Retorna el simbolo de la particula especificada
	!
	!**
	function ParticleManager_getSymbol( iterator ) result( output )
		implicit none 
		integer, intent(in) :: iterator
		character(30) :: output
		
		output = trim( ParticleManager_instance%particlesPtr( iterator )%symbol )
		
	end function ParticleManager_getSymbol
	
	!**
	! @brief Retorna un puntero a la particula especificada .
	!
	!**
	function ParticleManager_getParticlePtr( iindex ) result( output )
		implicit none 
		integer, intent(in) :: iindex
		type(Particle), pointer :: output
		
		output => ParticleManager_instance%particlesPtr( iindex )
		
		
	end function ParticleManager_getParticlePtr
	
	
	
	!**
	! @brief Retorna un vector con los valores de las coordenadas libres del sistema molecular
	!
	! @todo Este algoritmo dede ser optimizado
	!**
	function ParticleManager_getValuesOfFreeCoordinates() result( output )
		implicit none
		type(Vector) :: output
		

		real(8), allocatable :: auxVector(:)
		integer, allocatable :: auxCentersOfOptimization(:,:)
		integer :: numberOfParticles
		integer :: i
		integer :: j
		integer :: k
		
		numberOfParticles = size(ParticleManager_instance%particlesPtr)
		allocate(auxVector(numberOfParticles*3) )
		allocate(auxCentersOfOptimization(numberOfParticles*3,2) )
		
		k=0
		!! Determina el numero de coordenadas libres durante la optimizacion 
		do i=1, numberOfParticles
		
			if (  ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				do j=1, 3
					if ( .not.ParticleManager_instance%particlesPtr(i)%fixComponent(j) ) then
						k = k +1
						auxVector( k) = ParticleManager_instance%particlesPtr(i)%origin(j)
						auxCentersOfOptimization(k,1) = ParticleManager_instance%particlesPtr(i)%owner
						auxCentersOfOptimization(k,2) = j
					end if
				end do
			end if
			
		end do	
		
		call Vector_constructor(output, k)
		output%values = auxVector(1:k)
		allocate( ParticleManager_instance%centersOfOptimization(k,2) )
 		ParticleManager_instance%centersOfOptimization = auxCentersOfOptimization(1:k,:)

		deallocate(auxCentersOfOptimization)
		deallocate(auxVector)
	
	end function ParticleManager_getValuesOfFreeCoordinates 
						
		!**
	! @brief Retorna un vector con los valores de las coordenadas libres del sistema molecular
	!
	! @todo Este algoritmo dede ser optimizado
	!**
	function ParticleManager_getPositionOfCenterOfOptimizacion() result( output )
		implicit none
		type(Vector) :: output
		
		real(8), allocatable :: auxVector(:)
		integer, allocatable :: auxCentersOfOptimization(:,:)
		integer :: numberOfParticles
		integer :: i
		integer :: j
		integer :: k
		
		numberOfParticles = size(ParticleManager_instance%particlesPtr)
		allocate(auxVector(numberOfParticles*3) )
		allocate(auxCentersOfOptimization(numberOfParticles*3,2) )
		
		k=0
		!! Determina el numero de coordenadas libres durante la optimizacion 
		do i=1, numberOfParticles
		
			if (  ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				do j=1, 3
						k = k +1
						auxVector( k) = ParticleManager_instance%particlesPtr(i)%origin(j)
						auxCentersOfOptimization(k,1) = ParticleManager_instance%particlesPtr(i)%owner
						auxCentersOfOptimization(k,2) = j
				end do
			end if
			
		end do	
		
		call Vector_constructor(output, k)
		output%values = auxVector(1:k)
		if( .not.allocated( ParticleManager_instance%centersOfOptimization)) allocate( ParticleManager_instance%centersOfOptimization(k,2) )
		ParticleManager_instance%centersOfOptimization = auxCentersOfOptimization(1:k,:)

		deallocate(auxCentersOfOptimization)
		deallocate(auxVector)
	
	end function ParticleManager_getPositionOfCenterOfOptimizacion

						
						
	function ParticleManager_getNumberOfCoordinates() result( output )
		implicit none
		integer :: output
						
		integer :: i
		integer :: j
		
		output = 0
		
		do i=1, size(ParticleManager_instance%particlesPtr )
			
			if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				
				 output =output + 1
				
			end if
						
		end do
		
		output = output*3
		
	end function ParticleManager_getNumberOfCoordinates 


	function ParticleManager_getNumberOfFreeCoordinates() result( output )
		implicit none
		integer :: output
						
		integer :: i
		
		output = 0
		
		do i=1, size(ParticleManager_instance%particlesPtr )
			
			if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				
				if (.not.ParticleManager_instance%particlesPtr(i)%fixComponent(1) ) output =output + 1
				if (.not.ParticleManager_instance%particlesPtr(i)%fixComponent(2) ) output =output + 1
				if (.not.ParticleManager_instance%particlesPtr(i)%fixComponent(3) ) output =output + 1
				
			end if
						
		end do
		
	end function ParticleManager_getNumberOfFreeCoordinates

						
	function ParticleManager_getNumberOfCentersOfOptimization( fragmentNumber ) result( output)
          implicit none
          integer, optional, intent(in) :: fragmentNumber
          integer :: output

          integer :: i

          output = 0

	  if( present(fragmentNumber)) then
	       do i=1, size(ParticleManager_instance%particlesPtr)
		    if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization .and. &
			 ParticleManager_instance%particlesPtr(i)%fragmentNumber == fragmentNumber) output =output + 1
	       end do
	  else
	       do i=1, size(ParticleManager_instance%particlesPtr)
		    if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) output =output + 1
	       end do
	  end if
	
	end function ParticleManager_getNumberOfCentersOfOptimization
				

	function ParticleManager_getCartesianMatrixOfCentersOfOptimization( fragmentNumber ) result( output)
		implicit none
                integer, optional, intent(in) :: fragmentNumber
		type(Matrix) :: output
		
		integer :: ssize
		integer :: i
		integer :: j
		
                if( present(fragmentNumber) ) then
                    ssize = ParticleManager_getNumberOfCentersOfOptimization(fragmentNumber)
                else
                    ssize = ParticleManager_getNumberOfCentersOfOptimization()
                end if

		call Matrix_constructor( output, int(ssize,8), 3 )

               j =0

                if( present(fragmentNumber) ) then
                    do i = 1, ParticleManager_getTotalNumberOfParticles()

                         if ( ParticleManager_isCenterOfOptimization( i ) .and. &
                              (ParticleManager_instance%particlesPtr(i)%fragmentNumber== fragmentNumber)) then
                              j = j + 1
                              output%values(j,:) = ParticleManager_getOrigin( iterator = i )
                         end if
                    end do

               else

                    do i = 1, ParticleManager_getTotalNumberOfParticles()


                         if ( ParticleManager_isCenterOfOptimization( i ) ) then
                              j = j + 1

                              output%values(j,:) = ParticleManager_getOrigin( iterator = i )

                         end if

                    end do

               end if
	
	end function ParticleManager_getCartesianMatrixOfCentersOfOptimization



	function ParticleManager_getDistanceMatrix() result( output )
		implicit none
		type(Matrix) :: output

		type(Matrix) :: auxMatrix
		integer :: i
		integer :: j
		integer :: ssize
				
		auxMatrix = ParticleManager_getCartesianMatrixOfCentersOfOptimization()
		
		ssize = size(auxMatrix%values,dim=1)
		
		call Matrix_constructor(output, int(ssize,8), int(ssize,8) )
				
		do i=1,ssize
			do j=1, ssize
				output%values(i,j) = sqrt( sum( ( auxMatrix%values(i,:) - auxMatrix%values(j,:) )**2) )
			end do
		end do
		
		call Matrix_destructor( auxMatrix )
				
	end function ParticleManager_getDistanceMatrix


	function ParticleManager_getCenterOfOptimization( coordinate ) result( output )
		implicit none
		integer, intent(in) :: coordinate
		integer :: output(2)
		
		output(1) = ParticleManager_instance%centersOfOptimization(coordinate,1)
		output(2) = ParticleManager_instance%centersOfOptimization(coordinate,2)
	
	end  function ParticleManager_getCenterOfOptimization
	
	!**
	! @brief Retorna la carga de la particula puntual especificada
	!
	!**
	function ParticleManager_getChargeOfPuntualParticle( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		output = ParticleManager_instance%particlesPtr( ParticleManager_instance%idsOfPuntualParticles%particleID( specieID ) )%charge

	end function ParticleManager_getChargeOfPuntualParticle
	
	!**
	! @brief Retorna el indice del porpietario de la particula puntual especificada
	!
	!**
	function ParticleManager_getOwnerOfPuntualParticle( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		integer :: output
		
		output = ParticleManager_instance%particlesPtr( ParticleManager_instance%idsOfPuntualParticles%particleID( specieID ) )%owner
			
	end function ParticleManager_getOwnerOfPuntualParticle
	
	!**
	! @brief Retorna el numero de fragmento en el sistema molecular
	!
	!**
	function ParticleManager_getNumberOfFragments() result( output )
		implicit none
		integer :: output
		
		output = ParticleManager_instance%numberOfFragments
			
	end function ParticleManager_getNumberOfFragments




        !**
	! @brief Retorna el indice del porpietario de la particula puntual especificada
	!
	!**
	function ParticleManager_getOwnerCenter( specieID ) result( output )
		implicit none
		integer, intent(in), optional :: specieID
		integer :: output
		
		output = ParticleManager_instance%particlesPtr( specieID )%owner
			
	end function ParticleManager_getOwnerCenter
	
	!**
	! @brief Retorna el nombre de la particula puntual especificada
	!
	!**
	function ParticleManager_getNameOfPuntualParticle( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		character(30) :: output
		
		output = ParticleManager_instance%particlesPtr( ParticleManager_instance%idsOfPuntualParticles%particleID(specieID) )%name
		
	end function ParticleManager_getNameOfPuntualParticle

	function ParticleManager_getSymbolOfPuntualParticle( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		character(30) :: output
		
		output = ParticleManager_instance%particlesPtr( ParticleManager_instance%idsOfPuntualParticles%particleID(specieID) )%symbol
		
	end function ParticleManager_getSymbolOfPuntualParticle
	
	
	!**
	! @brief 	Retorna el factor multiplicativo para las integrals de intercambio en la construccion 
	!		de la matrix de particula independiente (G) 
	!**	
	function ParticleManager_getFactorOfInterchangeIntegrals( specieID ) result( output )
		implicit none
		integer :: specieID
		real(8) :: output
		
		output = Map_getValue( ParticleManager_instance%kappa, iterator=specieID) &
				/ Map_getValue( ParticleManager_instance%eta, iterator=specieID)
	
	end function ParticleManager_getFactorOfInterchangeIntegrals
	
	
	!**
	! @brief 	Retorna el numero de ocupacion para la especie solicitada
	!**	
	function ParticleManager_getOcupationNumber( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		integer :: output
		
		output = Map_getValue( ParticleManager_instance%ocupationNumber, iterator=specieID)
		
	end function ParticleManager_getOcupationNumber
	
	
	!**
	! @brief 	Retorna la multiplicidad para la especie solicitada
	!**	
	function ParticleManager_getMultiplicity( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		integer :: output
		
		output = Map_getValue( ParticleManager_instance%multiplicity, iterator=specieID )
	
	end function ParticleManager_getMultiplicity

	
	!**
	! @brief 	Retorna la constante de acoplamiento "eta" para la particula solicitada
	!**	
	function ParticleManager_getEta( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		output = real( Map_getValue( ParticleManager_instance%eta, iterator=specieID), 8 )
		
	end function ParticleManager_getEta
	
	!**
	! @brief 	Retorna la constante de acoplamiento "lambda" para la particula solicitada
	!**	
	function ParticleManager_getLambda( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		output = real( Map_getValue( ParticleManager_instance%lambda, iterator=specieID), 8 )
		
	end function ParticleManager_getLambda
	
	!**
	! @brief 	Retorna la constante de acoplamiento "kappa" para la particula solicitada
	!**	
	function ParticleManager_getKappa( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		output = real( Map_getValue( ParticleManager_instance%kappa, iterator=specieID), 8 )
	
	end function ParticleManager_getKappa

	function ParticleManager_getParticlesFraction( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		output = real( Map_getValue( ParticleManager_instance%particlesFraction, iterator=specieID), 8 )
	
	end function ParticleManager_getParticlesFraction

	
	!**
	! @brief 	Retorna la masa de la especie solicitada
	!**	
	function ParticleManager_getMass( specieID ) result( output )
		implicit none
		integer :: specieID
		real(8) :: output
		
		output = real( Map_getValue( ParticleManager_instance%mass, iterator=specieID), 8 )
		
	end function ParticleManager_getMass

	!**
	! @brief 	Retorna el valor de masa localizada en un punto dado del espacio.
	!**	
	function ParticleManager_getMassInPosition( position, unid ) result( output )
		implicit none
		real(8),intent(in) :: position(3)
		character(*), optional :: unid
		real(8) :: output
		
		integer :: i
		
		output = 0.0_8

		do i=1,size( ParticleManager_instance%particlesPtr )

			if( 	abs( position(1) - ParticleManager_instance%particlesPtr(i)%origin(1) ) < APMO_instance%DOUBLE_ZERO_THRESHOLD .and. &
				abs( position(2) - ParticleManager_instance%particlesPtr(i)%origin(2) ) < APMO_instance%DOUBLE_ZERO_THRESHOLD .and. &
				abs( position(3) - ParticleManager_instance%particlesPtr(i)%origin(3) ) < APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
				
				output = output +	ParticleManager_instance%particlesPtr(i)%mass*ParticleManager_instance%particlesPtr(i)%internalSize

			end if

		end do

		if ( present(unid) ) then

			select case( trim(unid) )
			
				case ("AU")
				
				case("SI")

					output = output * kg
		
				case("AMU")

					output = output * AMU

				case default

			end select

		end if

	
	end function ParticleManager_getMassInPosition

	!**
	! @brief 	Retorna la masa de la especie solicitada
	!**	
	function ParticleManager_getTotalMass( unid ) result( output )
		implicit none
		real(8) :: output
		character(*), optional :: unid
		
		integer :: i
		
		output = 0.0_8
		do i=1, size( ParticleManager_instance%particlesPtr)
			output = output + ParticleManager_instance%particlesPtr(i)%mass*ParticleManager_instance%particlesPtr(i)%internalSize
		end do

		if ( present(unid) ) then

			select case( trim(unid) )
			
				case ("AU")
				
				case("SI")

					output = output * kg
		
				case("AMU")

					output = output * AMU

				case default

			end select

		end if
	
	end function ParticleManager_getTotalMass


	!**
	! @brief 	Retorna el centro de masa para las particulas en el administrados
	!
	! @warning Se asume que la masa de toda parrticula esta localizada donde se centre 
	!		    su funcion base o la particula fija.
	!**	
	function ParticleManager_getCenterOfMass( ) result( output )
		implicit none

		real(8) :: output(3)

		integer :: i

		output = 0.0_8
	
	end function ParticleManager_getCenterOfMass
	
		
	!**
	! @brief 	Retorna la carga de la especie solicitada
	!**	
	function ParticleManager_getCharge( specieID, iterator ) result( output )
		implicit none
		integer, optional :: specieID
		integer, optional :: iterator
		real(8) :: output
		
		if (present(iterator) ) then
				output = ParticleManager_instance%particlesPtr(iterator)%charge
		else
			output = real( Map_getValue( ParticleManager_instance%charge, iterator=specieID), 8 )
		end if
		
	end function ParticleManager_getCharge

	!**
	! @brief 	Retorna las etiquetas de las contracciones gaussianas asociadas al 
	!		momento angular de la especie es especificada
	!**
	function ParticleManager_getLabelsOfContractions( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		character(18),allocatable :: output(:)
		
		
		integer :: numberOfContractions
		integer :: totalNumberOfContractions
		character(30) :: symbolOfSpecie
		character(9), allocatable :: shellCode(:)
		integer :: i
		integer :: j
		integer :: k
		integer :: m
		integer :: particleID
		integer :: contractionID
		
		m = 0
		do i=1, ParticleManager_instance%numberOfQuantumSpecies
				
			if ( specieID ==  i ) then
				
				numberOfContractions = int( Map_getValue(ParticleManager_instance%numberOfContractions, iterator = i ) )
				totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions(i)

				if (allocated(output)) deallocate(output)
				allocate( output(totalNumberOfContractions) )
				output=""
				
				do j=1, numberOfContractions
					
					particleID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%particleID
					contractionID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%contractionIDInParticle

					if(allocated(shellCode)) deallocate(shellCode)
					allocate(shellCode(ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%numCartesianOrbital))

					shellCode = ContractedGaussian_getShellCode(ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID))

					do k = 1, ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%numCartesianOrbital
						m = m + 1

	   					write (output(m),"(I5,A2,A5,A2,A4)") m,"  ", &
	  					trim(ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%name), "  ", &
	  					trim(shellCode(k))//" "

					end do
				
				end do

				return
				
			end if
		
		end do
		
	end function ParticleManager_getLabelsOfContractions

	!**
	! @brief 	Retorna las etiquetas asocoadas a los centros de optimizacion
	!**
	function ParticleManager_getLabelsOfCentersOfOptimization(flags, fragment) result( output )
		implicit none
		character(10),allocatable :: output(:)
		integer, optional :: flags
                integer, optional :: fragment
		
		integer :: numberOfCenters
		integer :: internalFlags
		integer :: i
		integer :: j
		
		internalFlags=0
		if( present(flags) ) internalFlags=flags
                if ( present(fragment) ) then
		   numberOfCenters = ParticleManager_getNumberOfCentersOfOptimization(fragment)
                else
                    numberOfCenters = ParticleManager_getNumberOfCentersOfOptimization()
                end if

		if ( allocated( output ) ) deallocate( output )
		allocate( output( numberOfCenters ) )

		j = 0

               if ( present(fragment) ) then
		    do i=1, size(ParticleManager_instance%particlesPtr )
			 
			 if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization .and. &
                              ParticleManager_instance%particlesPtr(i)%fragmentNumber==fragment ) then
				   
				   j = j + 1	
				   
				   select case(internalFlags)
				   
					     case( LABELS_NUMERATED )
						  output(j) = trim( ParticleManager_instance%particlesPtr(i)%symbol )//"("//trim(adjustl(String_convertIntegerToString(j)))//")"
					     case default
						  output(j) = trim( ParticleManager_instance%particlesPtr(i)%symbol )
				   end select
				   
			 end if
						  
		    end do

               else

		    do i=1, size(ParticleManager_instance%particlesPtr )
			 
			 if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				   
				   j = j + 1	
				   
				   select case(internalFlags)
				   
					     case( LABELS_NUMERATED )
						  output(j) = trim( ParticleManager_instance%particlesPtr(i)%symbol )//"("//trim(adjustl(String_convertIntegerToString(j)))//")"
					     case default
						  output(j) = trim( ParticleManager_instance%particlesPtr(i)%symbol )
				   end select
				   
			 end if
						  
		    end do



               end if

	end function ParticleManager_getLabelsOfCentersOfOptimization
	

	function ParticleManager_getChargesOfCentersOfOptimization( fragment ) result( output )
		implicit none
                integer, optional, intent(in) :: fragment
		real(8),allocatable :: output(:)
				
		integer :: numberOfCenters
		integer :: i
		integer :: j
		

                if ( present(fragment) ) then
                    numberOfCenters = ParticleManager_getNumberOfCentersOfOptimization(fragment)
                else
                    numberOfCenters = ParticleManager_getNumberOfCentersOfOptimization()
                end if

		if ( allocated( output ) ) deallocate( output )
		allocate( output( numberOfCenters ) )

               j=0
               if ( present(fragment) ) then
		
		    do i=1, size(ParticleManager_instance%particlesPtr )

			 if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization .and. &
                              ParticleManager_instance%particlesPtr(i)%fragmentNumber == fragment ) then

				   j = j + 1	

				   output(j) = ParticleManager_instance%particlesPtr(i)%totalCharge

			 end if

		    end do

               else

		    do i=1, size(ParticleManager_instance%particlesPtr )

			 if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then

				   j = j + 1	

				   output(j) = ParticleManager_instance%particlesPtr(i)%totalCharge

			 end if

		    end do

               end if

	end function ParticleManager_getChargesOfCentersOfOptimization




	!**
	! @brief 	Indica si es una particula fija 
	!**	
	function ParticleManager_isQuantum( specieID ) result( output )
		implicit none
		integer, intent(in) :: specieID
		logical :: output
		
		output = ParticleManager_instance%particlesPtr(specieID)%isQuantum
	
	end function ParticleManager_isQuantum
	
	!**
	! @brief calcula la energia total para una especie especificada
	!
	!**					
	function ParticleManager_puntualParticlesEnergy(  ) result( output )
		implicit none
		real(8) :: output

		integer :: i
		integer :: j
		real(8) :: deltaOrigin(3)
		real(8) :: tmp

		output =0.0_8
		
		do i=1, size( ParticleManager_instance%particlesPtr )
			
			if ( .not.ParticleManager_instance%particlesPtr(i)%isQuantum ) then

				do j = i + 1 , size( ParticleManager_instance%particlesPtr )

					if ( .not.ParticleManager_instance%particlesPtr(j)%isQuantum ) then

						deltaOrigin = 	ParticleManager_instance%particlesPtr(i)%origin &
									- ParticleManager_instance%particlesPtr(j)%origin
					
						output=output + ( ( ParticleManager_instance%particlesPtr(i)%charge &
								* ParticleManager_instance%particlesPtr(j)%charge )&
								/ sqrt( sum( deltaOrigin**2.0_8 ) ) )

					end if
				end do

			end if

		end do		

	end function ParticleManager_puntualParticlesEnergy
	
	!**
	! @brief calcula la energia total para una especie especificada
	!
	! @todo Este algoritmo debe ser optimizado
	!**					
	subroutine ParticleManager_setParticlesPositions( cartesianVector ) 
		implicit none
		type(Vector), intent(in),optional :: cartesianVector
		
		integer :: i
		integer :: j
		integer :: k
		integer :: m
		real(8) :: origin(3)
		
		k = 0

		do i=1, size(ParticleManager_instance%particlesPtr)

			if( k < size(cartesianVector%values) ) then
			
				if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
					
					origin=ParticleManager_getOrigin(i)
					do j=1,3
						k=k+1				
						if( .not.ParticleManager_instance%particlesPtr(i)%fixComponent(j) ) &
							origin(j)=cartesianVector%values( k )
					end do
						
					call Particle_setOrigin( ParticleManager_instance%particlesPtr(i), origin )
					
					if ( allocated(ParticleManager_instance%particlesPtr(i)%childs) ) then
						
						do j=1, size( ParticleManager_instance%particlesPtr(i)%childs )
							m=ParticleManager_instance%particlesPtr(i)%childs(j)
							call Particle_setOrigin( ParticleManager_instance%particlesPtr(m), origin )
						end do
						
					end if
				end if
						
			else
				return
			end if
		
		end do
		
	end subroutine ParticleManager_setParticlesPositions
	
	subroutine ParticleManager_changeOriginOfSystem( origin)
		implicit none
		real(8) :: origin(3)
		real(8) :: auxOrigin(3)
		
		integer :: i
		
		do i=1, size(ParticleManager_instance%particlesPtr)

			auxOrigin=ParticleManager_getOrigin(i)
			auxOrigin=auxOrigin-origin
			call Particle_setOrigin( ParticleManager_instance%particlesPtr(i), auxOrigin )
		
		end do
		
	end subroutine ParticleManager_changeOriginOfSystem

	subroutine ParticleManager_fixAllNucleous()
		implicit none
		integer :: i

		i = 0
		do i=1, size(ParticleManager_instance%particlesPtr )
			
			if ( ParticleManager_instance%particlesPtr(i)%isCenterOfOptimization ) then
				
				ParticleManager_instance%particlesPtr(i)%isQuantum = .false.
				
			end if

		end do

		ParticleManager_instance%numberOfPuntualParticles=0

		do i=1, size(ParticleManager_instance%particlesPtr )
			
			if ( .not.ParticleManager_instance%particlesPtr(i)%isQuantum ) then
				
				ParticleManager_instance%numberOfPuntualParticles = ParticleManager_instance%numberOfPuntualParticles +1
			end if

		end do

	end subroutine ParticleManager_fixAllNucleous
	
	!**
	! @brief metodo con propositos de depuracion
	!	permite evaluar el cambio en coeficientes de contraccion duracte tiempo de ejecucion
	!	se pueden incluir otras verificaciones
	!**
	subroutine ParticleManager_verifyCoefficients( message, threshold )
		implicit none
		character(*) :: message
		real(8) :: threshold
		
		
		integer ::i
		integer ::j
		integer :: particleID
		integer :: contractionID
		
		
		
			print *,""
			print *,"Begin Verification"
			print *,"---------------------------------------------------------------------------------"
			print *,trim(message)
			
			
			do i=1,ParticleManager_instance%numberOfQuantumSpecies
				
				do j=1,size( ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID )
					
					particleID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%particleID
					contractionID = ParticleManager_instance%idsOfContractionsForSpecie(i)%contractionID(j)%contractionIDInParticle
				
					if ( sum(ParticleManager_instance%particlesPtr(particleID)%basis%contractions(contractionID)%contractionCoefficients) &
						>threshold ) then
						call abort("Coefficients has been changed")
					end if
				
				end do

			end do
			print *,""
			print *,"---------------------------------------------------------------------------------"
			print *,""
			
	end subroutine ParticleManager_verifyCoefficients

end module ParticleManager_
