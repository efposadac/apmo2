!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief 	Permite definicr interfases ha miminizadores internos y/o externos
! 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!
!**
module GenericMinimizer_
	use GenericInterface_
	use Vector_
	use Matrix_
	use Exception_
	implicit none
	
	!< enum miminizer_status {
	integer, parameter :: GENERIC_MINIMIZER_MAXIMUM_CALLS_DEPLETED	=  -2
	integer, parameter :: GENERIC_MINIMIZER_BAD_DESCENT_DIRECTION	=  -1
	integer, parameter :: GENERIC_MINIMIZER_BAD_GRADIENT_DIRECTION	=  0
	integer, parameter :: GENERIC_MINIMIZER_CONTINUE	=  1
	integer, parameter :: GENERIC_MINIMIZER_SUCCESS	=  2
	!< }
	
	
	!< enum miminizer_method {
	integer, parameter :: GENERIC_MINIMIZER_CG	=  0
	integer, parameter :: GENERIC_MINIMIZER_BFGS	=  1
	!< }
	
	type, public :: GenericMinimizer
		character(30) :: vendor
		real(8) :: accuracyOfLinearSearch
		real(8) :: gradientTolerance
		real(8) :: functionValue
		real(8) :: initialStepSize
		integer :: numberOfIterations
		integer :: numberOfCallsToFunctionValue
		integer :: maximumCallsToFunctionValue
		integer :: status
		integer :: numberOfFreeCoordinates
		integer :: printProcess
		type(Vector) :: valuesOfIndependentVariables
		type(Vector) :: gradientVector
		character(30) :: method
		
	end type
	
	type(GenericMinimizer), public :: GenericMinimizer_instance
	
	public :: &
		GenericMinimizer_constructor, &
		GenericMinimizer_destructor,&
		GenericMinimizer_info, &
		GenericMinimizer_setAccuracyOfLinearSearch, &
		GenericMinimizer_setGradientTolerance, &
		GenericMinimizer_setMaximumNumberOfIterations, &
		GenericMinimizer_setMethod, &
		GenericMinimizer_setNumberOfFreeCoordinates, &
		GenericMinimizer_getCurrentPoint, &
		GenericMinimizer_getCurrentNumberOfIterations, &
		GenericMinimizer_run

contains	
	
	subroutine GenericMinimizer_constructor()
		implicit none
		
		GenericMinimizer_instance.accuracyOfLinearSearch = 1.0E-20
		GenericMinimizer_instance.gradientTolerance = APMO_instance.MINIMIZATION_TOLERANCE_GRADIENT
		GenericMinimizer_instance.numberOfIterations = 0
		GenericMinimizer_instance.numberOfCallsToFunctionValue = 0
		GenericMinimizer_instance.maximumCallsToFunctionValue = APMO_instance.MINIMIZATION_MAX_ITERATION
		GenericMinimizer_instance.status = GENERIC_MINIMIZER_CONTINUE
		GenericMinimizer_instance.printProcess =0
		GenericMinimizer_instance.method = "L-BFGS"
		GenericMinimizer_instance.vendor = "NEOS"
		GenericMinimizer_instance.initialStepSize =APMO_instance.MINIMIZATION_INITIAL_STEP_SIZE
		GenericMinimizer_instance.numberOfFreeCoordinates = 0
		
		GenericMinimizer_instance.initialStepSize =10  !! LINEA temporal
			
	end subroutine GenericMinimizer_constructor
	
	subroutine GenericMinimizer_destructor()
		implicit none
		
		call Vector_destructor( GenericMinimizer_instance.valuesOfIndependentVariables )
 		call Vector_destructor( GenericMinimizer_instance.gradientVector )

	end subroutine GenericMinimizer_destructor
	
	subroutine GenericMinimizer_info()
		implicit none
		
			write (6,"(T5,A30,A)") "LIBRARY VENDOR              : ", trim( GenericMinimizer_instance.vendor )
			write (6,"(T5,A30,A)") "METHOD OF OPTIMIZATION      : ", trim(GenericMinimizer_instance.method)
			write (6,"(T5,A30,ES10.4)") "ACCURACY OF LINEAR SEARCH   : ", GenericMinimizer_instance.accuracyOfLinearSearch
			write (6,"(T5,A30,ES10.4)") "GRADIENT TOLERANCE          : ", GenericMinimizer_instance.gradientTolerance
			write (6,"(T5,A30,I4)") "MAXIMUM NUMBER OF ITERATIONS: ", GenericMinimizer_instance.maximumCallsToFunctionValue 
		
	end subroutine GenericMinimizer_info
	
	
	subroutine GenericMinimizer_setAccuracyOfLinearSearch( accuracyOfLinearSearch )
		implicit none
		real(8), intent(in) :: accuracyOfLinearSearch
		
		GenericMinimizer_instance.accuracyOfLinearSearch = accuracyOfLinearSearch
		
	end subroutine GenericMinimizer_setAccuracyOfLinearSearch
	
	subroutine GenericMinimizer_setGradientTolerance( gradientTolerance )
		implicit none
		real(8), intent(in) :: gradientTolerance
		
		GenericMinimizer_instance.gradientTolerance = gradientTolerance
		
	end subroutine GenericMinimizer_setGradientTolerance
	
	subroutine GenericMinimizer_setMethod( method )
		implicit none
		character(*) :: method
		
		GenericMinimizer_instance.method = trim(method)
		
	end subroutine GenericMinimizer_setMethod
	
	subroutine GenericMinimizer_setMaximumNumberOfIterations( maximumNumberOfIterations )
		implicit none
		integer, intent(in) :: maximumNumberOfIterations
		
		GenericMinimizer_instance.maximumCallsToFunctionValue = maximumNumberOfIterations
		
	end subroutine GenericMinimizer_setMaximumNumberOfIterations
	
	subroutine GenericMinimizer_setInitialPoint( initialValuesOfIndependentVariables )
		implicit none
		type(Vector), intent(inout) :: initialValuesOfIndependentVariables
		integer :: numberOfIndependentVariables		
				
		numberOfIndependentVariables=size(initialValuesOfIndependentVariables.values)
		
		call Vector_constructor( GenericMinimizer_instance.valuesOfIndependentVariables, numberOfIndependentVariables )
		call Vector_constructor( GenericMinimizer_instance.gradientVector, numberOfIndependentVariables )
		GenericMinimizer_instance.valuesOfIndependentVariables = initialValuesOfIndependentVariables
		
	end subroutine GenericMinimizer_setInitialPoint
	
	subroutine GenericMinimizer_setNumberOfFreeCoordinates( numberOfFreeCoordinates )
		implicit none
		integer :: numberOfFreeCoordinates
		
		GenericMinimizer_instance.numberOfFreeCoordinates = numberOfFreeCoordinates

	end subroutine GenericMinimizer_setNumberOfFreeCoordinates
	
	function GenericMinimizer_getCurrentPoint() result ( output )
		implicit none
		type(Vector) :: output
		
		output = GenericMinimizer_instance.valuesOfIndependentVariables
		
	end function GenericMinimizer_getCurrentPoint

	function GenericMinimizer_getCurrentNumberOfIterations() result ( output )
		implicit none
		integer :: output
		
		output = GenericMinimizer_instance.numberOfIterations
		
	end function GenericMinimizer_getCurrentNumberOfIterations
	
	!**
	!
	!**
	subroutine GenericMinimizer_run()
		implicit none
		
		
		type(Matrix), external :: molecularsystem__mp_molecularsystem_getempricalforceconstants
		type(Vector) :: workVector
		type(Matrix) :: initialHess
		type(Exception) :: ex
		
		integer :: numberOfIndependentVariables, i, m, iprint, isave(44),mdim
		integer, allocatable ::   nbd(:), iwa(:)
		character*60 :: task, csave
      		logical ::   lsave(4)
		real(8), allocatable :: uVector(:), lVector(:)
		real(8) :: f, factr, pgtol,dsave(29), t1, t2,rmsGradient
		
		
		m=20
		numberOfIndependentVariables=size(GenericMinimizer_instance.valuesOfIndependentVariables.values)
		
		!! Calcula el taman~o del vector de trabajo y asigna memoria
		mdim=2*m*numberOfIndependentVariables+4*numberOfIndependentVariables+12*m*m+12*m
		call Vector_constructor( workVector, mdim )
		allocate( nbd(numberOfIndependentVariables) )
		allocate( iwa(numberOfIndependentVariables*3) )
		allocate( uVector(numberOfIndependentVariables) )
		allocate( lVector(numberOfIndependentVariables) )

		iprint = -1 !! Suprime impresiones del algoritmo

		!! Suprime criterio de parada internos
		factr=0.0d0
		pgtol=0.0d0

		!! Definicion de limites o restricciones sobre las variables
		!! lVector especifica rango inferior
		!! uVector   especifica rango superior
		nbd=0 !! Asune que se trata de un problema sin restricciones

		!! Se inicia el metodo de minimizacion
		task = 'START'

		!! Inicio de cilco de iteracion
	
 
 111  continue
 
		call setulb(numberOfIndependentVariables,m,GenericMinimizer_instance.valuesOfIndependentVariables.values,lVector,uVector, &
			nbd,GenericMinimizer_instance.functionValue,GenericMinimizer_instance.gradientVector.values,factr,pgtol, &
			workVector.values,iwa,task,iprint, csave, lsave, isave, dsave )
	
		if ( task(1:2) == 'FG' ) then
			call GenericMinimizer_calculateFunctionAndGradient()
				
			rmsGradient= sqrt( sum( (GenericMinimizer_instance.gradientVector.values )**2.0 ) /&
								GenericMinimizer_instance.numberOfFreeCoordinates  )

			if( rmsGradient < GenericMinimizer_instance.gradientTolerance ) then

				GenericMinimizer_instance.status = GENERIC_MINIMIZER_SUCCESS
				return
			end if
			
			goto 111
			
		endif
	
		if (task(1:5) .eq. 'NEW_X') then 
	
			!! Criterio de parada por maximo numero de iteraciones
			if (isave(34) > GenericMinimizer_instance.maximumCallsToFunctionValue) then
				task='STOP: TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT'
				GenericMinimizer_instance.status = GENERIC_MINIMIZER_MAXIMUM_CALLS_DEPLETED
			end if
	
			rmsGradient= sqrt( sum( (GenericMinimizer_instance.gradientVector.values )**2.0 ) /&
								GenericMinimizer_instance.numberOfFreeCoordinates  )

 			if( rmsGradient < GenericMinimizer_instance.gradientTolerance ) then
!			if (dsave(13) .le. GenericMinimizer_instance.gradientTolerance*(1.0d0 + abs(GenericMinimizer_instance.functionValue))) then
				
				task='STOP: THE PROJECTED GRADIENT IS SUFFICIENTLY SMALL'
				GenericMinimizer_instance.status = GENERIC_MINIMIZER_SUCCESS
				return
			end if
	
			!! Realiza  una nueva iteracion
			goto 111
	
		endif
		
		print *,"TASK=",task
	
		deallocate( nbd )
		deallocate( iwa )
		deallocate( uVector )
		deallocate( lVector )
	
	end subroutine GenericMinimizer_run

	subroutine GenericMinimizer_calculateFunctionAndGradient()
		implicit none
		
		real(8), external :: minimizer__mp_minimizer_getfunctionvalue
		external :: minimizer__mp_minimizer_showinformationofiteration
		type(Vector), external :: minimizer__mp_minimizer_getgradient
		real(8) :: rmsGradient
		
		!!
		!! Calcula el valor de la funcion y el gradiente inicial
		!!
		GenericMinimizer_instance.functionValue = minimizer__mp_minimizer_getfunctionvalue( &
			GenericMinimizer_instance.valuesOfIndependentVariables )
		GenericMinimizer_instance.gradientVector = minimizer__mp_minimizer_getgradient( &
			GenericMinimizer_instance.valuesOfIndependentVariables )
			
		GenericMinimizer_instance.numberOfCallsToFunctionValue = &
			GenericMinimizer_instance.numberOfCallsToFunctionValue + 1

		GenericMinimizer_instance.numberOfIterations = &
			GenericMinimizer_instance.numberOfIterations + 1
			
		call minimizer__mp_minimizer_showinformationofiteration( &
			GenericMinimizer_instance.gradientVector, &
			GenericMinimizer_instance.functionValue, &
			GenericMinimizer_instance.numberOfCallsToFunctionValue )
	


	end subroutine GenericMinimizer_calculateFunctionAndGradient
				
end module GenericMinimizer_