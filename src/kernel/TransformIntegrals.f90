!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module TransformIntegrals_
	use IFPORT
	use APMO_
	use Matrix_
	use IndexMap_
	
	
	implicit none

	!! @brief Clase encargada de realizar transformacion de integrales atomicas a  moleculares
	!!
	!! 	Esta clase reliza la transformacion de integrales de orbitales atomicos a orbitales moleculares,
	!!	creando una interface al algoritmo de   Yamamoto, Shigeyoshi; Nagashima, Umpei.
   	!!	Computer Physics Communications, 2005, 166, 58-65
	!! 
	!! @author Sergio Gonzalez
	!!
	!! <b> Fecha de creacion : </b> 2009-07-07
	!!   - <tt> 2009-07-07 </tt>: Sergio Gonzalez ( sagonzalez@unal.edu.co )
	!!        -# Creacion del archivo y las funciones basicas
	!<
	type, public :: TransformIntegrals
		character(30) :: name
		character(255) :: fileForCoefficients
		character(255) :: fileForIntegrals
		character(255) :: prefixOfFile
		integer :: numberOfContractions
		integer :: bias
		integer :: specieID
		integer :: otherSpecieID
		integer :: unidOfOutputForCoefficients
		integer :: unidOfOutputForIntegrals

	end type

	!! TypeOfIntegrals {
		integer, parameter :: ONE_SPECIE				= 0
		integer, parameter :: TWO_SPECIES			= 1
	!! }

	public :: &
		TransformIntegrals_constructor, &
		TransformIntegrals_destructor, &
		TransformIntegrals_atomicToMolecularOfOneSpecie, &
		TransformIntegrals_atomicToMolecularOfTwoSpecies

	private
contains


	!>
	!! @brief Contructor de la clase
	!<
	subroutine TransformIntegrals_constructor(this)
		implicit none
		type(TransformIntegrals) :: this

		this.unidOfOutputForCoefficients = APMO_instance.UNID_FOR_MOLECULAR_ORBITALS_FILE
		this.unidOfOutputForIntegrals = APMO_instance.UNID_FOR_MP2_INTEGRALS_FILE
		this.fileForCoefficients = trim(APMO_instance.INPUT_FILE)//"mo.values"
		this.fileForIntegrals = trim(APMO_instance.INPUT_FILE)//"aoint.values"
		this.prefixOfFile =  trim(APMO_instance.INPUT_FILE)
	
	end subroutine TransformIntegrals_constructor

	!>
	!! @brief Contructor de la clase
	!<
	subroutine TransformIntegrals_destructor(this)
		implicit none
		type(TransformIntegrals) :: this
	
	end subroutine TransformIntegrals_destructor

	!>
	!! @brief Transforma integrales de repulsion atomicas entre particulas de la misma especie
	!! 		a integrales moleculares.
	!<
	subroutine TransformIntegrals_atomicToMolecularOfOneSpecie( this, coefficientsOfAtomicOrbitals, &
		 molecularIntegrals, specieID, nameOfSpecie )
		implicit none
		type(TransformIntegrals) :: this
		type(Matrix) :: coefficientsOfAtomicOrbitals
		type(Matrix) :: molecularIntegrals
		integer :: specieID
		character(*) :: nameOfSpecie
		
		integer :: errorNum
		real(8) :: initialTime
		real(8) :: finalTime
		
		if ( .not.APMO_instance.OPTIMIZE ) then
			initialTime = DCLOCK()
		end if

		this.numberOfContractions=size(coefficientsOfAtomicOrbitals.values,dim=1)
		this.specieID = specieID

		call TransformIntegrals_writeCoefficients(this, coefficientsOfAtomicOrbitals)
		call TransformIntegrals_writeIntegralsOfRepulsionForOneSpecie(this, nameOfSpecie)

		!! Inicia proceso de transformacion
		call fourIndexTransformation( this.numberOfContractions,  trim(this.prefixOfFile ) )

		!! Lee  de disco las integrales tranformadas
		call TransformIntegrals_readIntegralsTransformed( this, molecularIntegrals, ONE_SPECIE )
		
		!! Remueve archivos empleados en proceso de transformacion
		errorNum = system("rm "// trim(this.prefixOfFile)//"*.dat "// trim(this.prefixOfFile) // "*.values "  )

		if ( .not.APMO_instance.OPTIMIZE ) then
			finalTime = DCLOCK()
			write (6,"(T15,A30,ES10.2,A4)") "cpu-time  for transformation:  ", finalTime-initialTime ," (s)"
			print *,""
		end if

	end subroutine TransformIntegrals_atomicToMolecularOfOneSpecie

	!>
	!! @brief Transforma integrales de repulsion atomicas entre particulas de diferente especie
	!! 		a integrales moleculares.
	!<
	subroutine TransformIntegrals_atomicToMolecularOfTwoSpecies( this, coefficientsOfAtomicOrbitals, &
		 otherCoefficientsOfAtomicOrbitals, molecularIntegrals, specieID, nameOfSpecie, otherSpecieID, nameOfOtherSpecie )
		implicit none
		type(TransformIntegrals) :: this
		type(Matrix) :: coefficientsOfAtomicOrbitals
		type(Matrix) :: otherCoefficientsOfAtomicOrbitals
		type(Matrix) :: molecularIntegrals
		integer :: specieID
		character(*) :: nameOfSpecie
		integer :: otherSpecieID
		character(*) :: nameOfOtherSpecie
		
		integer :: errorNum
		real(8) :: initialTime
		real(8) :: finalTime


		if ( .not.APMO_instance.OPTIMIZE ) then
			initialTime = DCLOCK()
		end if

		this.numberOfContractions = size(coefficientsOfAtomicOrbitals.values,dim=1)+size(otherCoefficientsOfAtomicOrbitals.values,dim=1)
		this.bias = size(coefficientsOfAtomicOrbitals.values,dim=1)
		this.specieID = specieID
		this.otherSpecieID = otherSpecieID

		call TransformIntegrals_writeCoefficients( this, coefficientsOfAtomicOrbitals, otherCoefficientsOfAtomicOrbitals )

 		call TransformIntegrals_writeIntegralsOfRepulsionForTwoSpecies(this, nameOfSpecie, nameOfOtherSpecie)

		!! Inicia proceso de transformacion
		call fourIndexTransformation( this.numberOfContractions,  trim(this.prefixOfFile ) )


		! Lee  de disco las integrales tranformadas
		call TransformIntegrals_readIntegralsTransformed( this, molecularIntegrals, TWO_SPECIES )

		!! Remueve archivos empleados en proceso de transformacion
		errorNum = system("rm "// trim(this.prefixOfFile)//"*.dat "// trim(this.prefixOfFile) // "*.values "  )

		if ( .not.APMO_instance.OPTIMIZE ) then
			finalTime = DCLOCK()
			write (6,"(T15,A30,ES10.2,A4)") "cpu-time  for transformation:  ", finalTime-initialTime ," (s)"
			print *,""
		end if

	end subroutine TransformIntegrals_atomicToMolecularOfTwoSpecies

	!>
	!! @brief Escribe los coefficientes de combinacion para los orbitales atomicos.
	!! 		El almacenamiento requiere guardar columnas completas una tras de otra
	!<
	subroutine TransformIntegrals_writeCoefficients( this, coefficients, otherCoefficients )
		implicit none
		type(TransformIntegrals) :: this
		type(Matrix) :: coefficients
		type(Matrix), optional :: otherCoefficients
		
		integer :: a
		integer :: b
		
		open( UNIT=this.unidOfOutputForCoefficients,FILE=trim(this.fileForCoefficients),STATUS='REPLACE', &
				ACCESS='SEQUENTIAL', FORM='FORMATTED' )

		if ( .not.present(otherCoefficients) ) then

			this.numberOfContractions=size(coefficients.values,dim=1)

			do a=1, this.numberOfContractions
				do b=1,this.numberOfContractions
				
					write(this.unidOfOutputForCoefficients,*) a,b,coefficients.values(b,a)
				
				end do
			end do

		else

			this.numberOfContractions=size(coefficients.values,dim=1)+size(otherCoefficients.values,dim=1)
			this.bias = size(coefficients.values,dim=1)

			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!! Escribe en disco los coeficientes de combinacion  para un par de especies,  haciendo
			!! un "this.bias" de uno de los conjuntos sobre el otro
			!!
			do a=1, this.numberOfContractions
				do b=1,this.numberOfContractions
				
					if ( ( a <= this.bias ) .and. ( b <= this.bias ) ) then
						write(this.unidOfOutputForCoefficients,*) a, b, coefficients.values( b, a )
					else &
					if ( ( a > this.bias ) .and. ( b > this.bias ) ) then
						write(this.unidOfOutputForCoefficients,*) a, b, otherCoefficients.values( b-this.bias, a-this.bias )
					else
						write(this.unidOfOutputForCoefficients,*) a,b,0.0_8
					end if
				
				end do
			end do

		end if

		close( this.unidOfOutputForCoefficients )

	end subroutine TransformIntegrals_writeCoefficients

	!>
	!! @brief RUTINA OBSOLETA- TENDERA A DESAPARECER. Escribe en disco integrales de repulsion de una especie
	!!		Se requiere eliminar por simetria las integrales repetidas de l tensor de repulsion, ademas
	!!		el alcenamiento requiere que los indices (i,j,k,l) varien de derecha a izquierda.
	!!		Las integrales con valor cero no requieren almacenamiento.
	!!
	!!	@todo Gestionar manejo de indicador de error en apertura,lectura y escritura en disco
	!<
	subroutine TransformIntegrals_writeIntegralsOfRepulsionForOneSpecie(this, nameOfSpecie)
		implicit none
		type(TransformIntegrals) :: this
		character(*) :: nameOfSpecie
		
		integer :: a
		integer :: b
		integer :: r
		integer :: s
		integer :: errorValue
		real(8) :: integralValue


		!! Abre el archivo binario que mantiene las integrales de repulsion intra-especie
		open( UNIT=34, FILE=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".ints", STATUS='old', &
						ACCESS='sequential', FORM='binary' )

		!! Habilita unidad para almacenamiento temporal de integrales de respulsion !! ESTO SOLO ES TEMPORAL Y DEBE
		!! CAMBIARSE EN LA LIBRERIA DE TRANSFORMACION PARA ACCEDER DE MANERA DIRECTA AL ARCHIVO BINARIO
		!! 
		open( UNIT=this.unidOfOutputForIntegrals,FILE=trim(this.fileForIntegrals),STATUS='REPLACE', &
		ACCESS='SEQUENTIAL', FORM='FORMATTED' )

		do
			read(UNIT=34,IOSTAT=errorValue) a, b, r, s, integralValue
			if (a<0) exit
		
			write(this.unidOfOutputForIntegrals,*) a,b,r,s,integralValue
		
		end do

		close( this.unidOfOutputForIntegrals )
		close( 34 )

	end subroutine TransformIntegrals_writeIntegralsOfRepulsionForOneSpecie

	!>
	!! @brief RUTINA OBSOLETA- TENDERA A DESAPARECER. Escribe en disco integrales de repulsion de dos especies
	!!		Se requiere eliminar por simetria las integrales repetidas de l tensor de repulsion, ademas
	!!		el alcenamiento requiere que los indices (i,j,k,l) varien de derecha a izquierda.
	!!		Las integrales con valor cero no requieren almacenamiento.
	!<
	subroutine TransformIntegrals_writeIntegralsOfRepulsionForTwoSpecies(this, nameOfSpecie, nameOfOtherSpecie)
		implicit none
		type(TransformIntegrals) :: this
		character(*) :: nameOfSpecie
		character(*) :: nameOfOtherSpecie
		
		integer :: a
		integer :: b
		integer :: r
		integer :: s
		integer :: errorValue
		real(8) :: integralValue

		!! Abre el archivo binario que mantiene las integrales de repulsion inter-especie
		open( UNIT=34, FILE=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//"."//trim(nameOfOtherSpecie)//".ints", STATUS='old', &
						ACCESS='sequential', FORM='binary' )

		!! Habilita unidad para almacenamiento temporal de integrales de respulsion !! ESTO SOLO ES TEMPORAL Y DEBE
		!! CAMBIARSE EN LA LIBRERIA DE TRANSFORMACION PARA ACCEDER DE MANERA DIRECTA AL ARCHIVO BINARIO
		!! 
		open(UNIT=this.unidOfOutputForIntegrals,FILE=trim(this.fileForIntegrals),STATUS='REPLACE', &
		ACCESS='SEQUENTIAL', FORM='FORMATTED')

		do
			read(UNIT=34,IOSTAT=errorValue) a, b, r, s, integralValue
			if (a<0) exit
		
			write(this.unidOfOutputForIntegrals,*) a, b, r+this.bias, s+this.bias, integralValue
		
		end do

		close( this.unidOfOutputForIntegrals )
		close( 34 )

	end subroutine TransformIntegrals_writeIntegralsOfRepulsionForTwoSpecies



	subroutine TransformIntegrals_readIntegralsTransformed(this, matrixContainer, typeOfIntegrals )
		implicit none
		type(TransformIntegrals) :: this
		type(Matrix) :: matrixContainer
		integer :: typeOfIntegrals

		integer(8) :: numberOfIntegrals
		integer(8) :: auxIndex
		real(8),dimension(791) :: integralValue
		real(8) :: auxValue
		integer :: iter
		integer :: errorValue
		integer :: bufferA
		integer :: bufferB
		integer :: bufferSize
		integer :: lowerIndices(2), upperIndeces(2), counter(2)
		integer,dimension(4,791) :: indexBuffer


		!! Accesa el archivo binario con las integrales en terminos de orbitales moleculares
		open(unit=this.unidOfOutputForIntegrals, file=trim(this.prefixOfFile)//"moint.dat", &
					status='old',access='sequential', form='unformatted' )

		select case( typeOfIntegrals)

			case(ONE_SPECIE)

				if ( allocated(matrixContainer.values ) ) deallocate(matrixContainer.values)
			
				numberOfIntegrals   =	 int( ( (  this.numberOfContractions * (  this.numberOfContractions + 1.0_8 ) / 4.0_8 ) * &
						( (  this.numberOfContractions * (  this.numberOfContractions + 1.0_8) / 2.0_8 ) + 1.0_8) ), 8 )
				
				call Matrix_constructor( matrixContainer, numberOfIntegrals, 1_8, 0.0_8 )
				matrixContainer.values = 0.0_8

				do
					read(UNIT=this.unidOfOutputForIntegrals,IOSTAT=errorValue) bufferA,bufferB,integralValue,indexBuffer
		
					bufferSize = iabs( bufferA)
					if ( bufferA /= 0 ) then
						do iter=1,bufferSize
						
							auxIndex = IndexMap_tensorR4ToVector(indexBuffer(1,iter),indexBuffer(2,iter), &
							indexBuffer(3,iter), indexBuffer(4,iter), this.numberOfContractions )
						
						matrixContainer.values( auxIndex, 1 ) = integralValue(iter)

						end do
					end if
			
					if ( bufferA <= 0 ) exit	
		
				end do
			
		
			case(TWO_SPECIES)

				
				if ( allocated(matrixContainer.values ) ) deallocate(matrixContainer.values)

				numberOfIntegrals = ( this.bias    *  ( ( this.bias + 1.0_8) / 2.0_8 ) ) * &
					( (this.numberOfContractions-this.bias) * ( ( (this.numberOfContractions-this.bias) + 1.0_8 ) / 2.0_8 ) )
				
				call Matrix_constructor( matrixContainer, numberOfIntegrals, 1_8, 0.0_8 )
				
				matrixContainer.values = 0.0_8

				do
					read(UNIT=this.unidOfOutputForIntegrals,IOSTAT=errorValue) bufferA,bufferB,integralValue,indexBuffer
							
					bufferSize = iabs( bufferA)
					if ( bufferA /= 0 ) then
						do iter=1,bufferSize
							counter=1
							if ( indexBuffer(1,iter ) > this.bias )  then
								indexBuffer(1,iter)=indexBuffer(1,iter)-this.bias
								upperIndeces( counter(1) ) = indexBuffer(1,iter)
								counter(1)=2
							else
								lowerIndices( counter(2) ) = indexBuffer(1,iter)
								counter(2)=2
							end if

							if ( indexBuffer(2,iter ) > this.bias ) then
								indexBuffer(2,iter)=indexBuffer(2,iter)-this.bias
								upperIndeces( counter(1) ) = indexBuffer(2,iter)
								counter(1)=2
							else
								lowerIndices( counter(2) ) = indexBuffer(2,iter)
								counter(2)=2
							end if

							if ( indexBuffer(3,iter ) > this.bias ) then
								indexBuffer(3,iter)=indexBuffer(3,iter)-this.bias
								upperIndeces( counter(1) ) = indexBuffer(3,iter)
								counter(1)=2
							else
								lowerIndices( counter(2) ) = indexBuffer(3,iter)
								counter(2)=2
							end if

							if ( indexBuffer(4,iter ) > this.bias ) then
								indexBuffer(4,iter)=indexBuffer(4,iter)-this.bias
								upperIndeces( counter(1) ) = indexBuffer(4,iter)
								counter(1)=2
							else
								lowerIndices( counter(2) ) = indexBuffer(4,iter)
								counter(2)=2
							end if



							auxIndex = IndexMap_tensorR4ToVector(lowerIndices(1),lowerIndices(2),upperIndeces(1),upperIndeces(2), &
										 this.bias, this.numberOfContractions - this.bias )

							matrixContainer.values(auxIndex,1)=integralValue(iter)

						end do
					end if
				
					if ( bufferA <= 0 ) exit	
			
			end do

		end select

		close( this.unidOfOutputForIntegrals)
		
	end subroutine TransformIntegrals_readIntegralsTransformed

end module TransformIntegrals_