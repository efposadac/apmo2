module LibintInterface2_
  use Exception_
  !use ExternalPotential_
  !use InterPotential_
  !use InterPotential_Manager_
  use LibintTypes_
  use Math_
  !use Input_Parameters_
  use, intrinsic :: iso_c_binding
  use ContractedGaussian_
  use ParticleManager_
  use LibintInterface_
  implicit none

  !>
  !! @brief Description
  !!
  !! @author Edwin Posada
  !!
  !! <b> Creation data : </b> 10-07-10
  !!
  !! <b> History change: </b>
  !!
  !!   - <tt> 10-07-10 </tt>:  Edwin Posada ( efposadac@unal.edu.co )
  !!        -# Modulo que sirve como interfaz para las librerias LIBINT
  !!   - <tt> 2011-02-13 </tt>: Fernando Posada ( efposadac@unal.edu.co )
  !!        -# Reescribe y adapta el módulo para su inclusion en Lowdin
  !<

  !<
  !! Macro definition to avoid so large code
  !>

#define contraction(n) ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(\
  specieID)%contractionID(n)%particleID)%basis%contractions(\
  ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(\
  n)%contractionIDInParticle)

#define primitive(n, m) ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(\
  specieID)%contractionID(n)%particleID)%basis%contractions(\
  ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(\
  n)%contractionIDInParticle)%primitives(m)

#define otherContraction(n,specie) ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(\
  specie)%contractionID(n)%particleID)%basis%contractions(\
  ParticleManager_instance%idsOfContractionsForSpecie(specie)%contractionID(\
  n)%contractionIDInParticle)

#define otherPrimitive(n, m, specie) ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(\
  specie)%contractionID(n)%particleID)%basis%contractions(\
  ParticleManager_instance%idsOfContractionsForSpecie(specie)%contractionID(\
  n)%contractionIDInParticle)%primitives(m)


  type, public :: LibintInterface2
     character(5) :: job
     logical :: isInstanced
     integer :: maxAngularMoment
     integer :: numberOfPrimitives
     integer :: libintStorage
     type(lib_int) :: libint
     type(lib_deriv) :: libderiv
     type(lib_r12) :: libr12
  end type LibintInterface2

  type, public :: erisStack
     integer :: a
     integer :: b
     integer :: c
     integer :: d
     real(8) :: integrals
  end type erisStack


  !<
  !!Apuntadores a las funciones de libint.a, libderiv.a y libr12.a
  !>
  !type(c_funptr), dimension(0:6,0:6,0:6,0:6), bind(c), private  :: build_eri
  !type(c_funptr), dimension(0:3,0:3,0:3,0:3), bind(c), private  :: build_deriv1_eri
  !type(c_funptr), dimension(0:3,0:3,0:3,0:3), bind(c), private  :: build_r12_grt
  !type(c_funptr), dimension(0:7,0:7,0:7,0:7), bind(c), private  :: libint2_build_r12kg12
  !type(c_funptr), dimension(0:7,0:7,0:7,0:7), bind(c), private  :: libint2_build_eri

  interface

     !<
     !!Interfaz a libderiv.a
     !>
     !		subroutine LibintInterface2_initLibDerivBase() bind(C, name="init_libderiv_base")
     !			implicit none
     !
     !		end subroutine LibintInterface2_initLibDerivBase
     !
     !		function LibintInterface2_initLibDeriv1(libDeriv, maxAngMoment, numberOfPrimitives, maxCartesianOrbital) &
     !			bind(C, name="init_libderiv1")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			integer(kind=c_int) :: LibintInterface2_initLibDeriv1
     !			type(lib_deriv) :: libDeriv
     !			integer(kind=c_int), value :: maxAngMoment
     !			integer(kind=c_int), value :: numberOfPrimitives
     !			integer(kind=c_int), value :: maxCartesianOrbital
     !
     !		end function LibintInterface2_initLibDeriv1
     !
     !		subroutine LibintInterface2_buildLibDeriv1(libDeriv, numberOfPrimitives) bind(C)
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(lib_deriv) :: libDeriv
     !			integer(kind=c_int),value :: numberOfPrimitives
     !
     !		end subroutine LibintInterface2_buildLibDeriv1
     !
     !		subroutine LibintInterface2_freeLibDeriv(libDeriv) bind(C, name="free_libderiv")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(lib_deriv) :: libDeriv
     !
     !		end subroutine LibintInterface2_freeLibDeriv

     !<
     !!Interfaz a libr12.a
     !>
     !		subroutine LibintInterface2_initLibR12Base() bind(C, name="init_libr12_base")
     !			implicit none
     !
     !		end subroutine LibintInterface2_initLibR12Base
     !
     !		function LibintInterface2_initLibR12(libR12, maxAngMoment, numberOfPrimitives, maxCartesianOrbital) bind(C, name="init_libr12")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			integer(kind=c_int) :: LibintInterface2_initLibR12
     !			type(lib_r12) :: libR12
     !			integer(kind=c_int), value :: maxAngMoment
     !			integer(kind=c_int), value :: numberOfPrimitives
     !			integer(kind=c_int), value :: maxCartesianOrbital
     !
     !		end function LibintInterface2_initLibR12
     !
     !		subroutine LibintInterface2_buildLibR12(libR12, numberOfPrimitives) bind(C)
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(lib_r12) :: libR12
     !			integer(kind=c_int),value :: numberOfPrimitives
     !
     !		end subroutine LibintInterface2_buildLibR12
     !
     !		subroutine LibintInterface2_freeLibR12(LibR12) bind(C, name="free_libr12")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(lib_r12) :: libR12
     !
     !		end subroutine LibintInterface2_freeLibR12

     !<
     !!Interfaz a libint2.a
     !>
     !		subroutine LibintInterface2_libint2StaticInit() bind(C, name="libint2_static_init")
     !			implicit none
     !
     !		end subroutine LibintInterface2_libint2StaticInit
     !
     !		subroutine LibintInterface2_libint2StaticCleanup() bind(C, name="libint2_static_cleanup")
     !			implicit none
     !
     !		end subroutine LibintInterface2_libint2StaticCleanup

     !		subroutine LibintInterface2_libint2InitR12kg12(libIntG12, maxAngMoment, buf) bind(C, name="libint2_init_eri")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(libint2) :: libIntG12
     !			integer(kind=c_int), value :: maxAngMoment
     !			real(kind=c_double) :: buf
     !
     !		end subroutine LibintInterface2_libint2InitR12kg12

     !		function LibintInterface2_libint2NeedMemoryR12kg12(maxAngMoment) bind(C, name="libint2_need_memory_eri")
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			integer(kind=c_int) :: LibintInterface2_libint2NeedMemoryR12kg12
     !			integer(kind=c_int), value :: maxAngMoment
     !
     !		end function LibintInterface2_libint2NeedMemoryR12kg12

     !		subroutine LibintInterface2_buildLibIntG12(libIntG12) bind(C)
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(libint2) :: libIntG12
     !
     !		end subroutine LibintInterface2_buildLibIntG12

     !		subroutine LibintInterface2_libint2CleanupR12kg12(LibIntG12) bind(C, name="libint2_cleanup_eri")
     !			use LibintInterface_Types_
     !			use, intrinsic :: iso_c_binding
     !			implicit none
     !
     !			type(libint2) :: libIntG12
     !
     !		end subroutine LibintInterface2_libint2CleanupR12kg12

  end interface

  type(LibintInterface2), public :: LibintInterface2_instance !! Singleton
  type(erisStack), private :: eris

INTEGER :: INTEGRAL_STACK_SIZE = 1

contains


  !>
  !! @brief Constructor por omision, se llama una sola vez durante todo el cálculo
  !!
  !! @param job, trabajo a realizar
  !<
  subroutine LibintInterface2_constructor( maxAngMoment, numberOfPrimitives, job )
    implicit none

    integer, intent(in) :: maxAngMoment
    integer, intent(in) :: numberOfPrimitives
    character(*) :: job

    integer(kind=c_int) :: libIntStorage_c
    integer(kind=c_int) :: maxAngMoment_c
    integer(kind=c_int) :: numberOfPrimitives_c

    if (.not. LibintInterface2_isInstanced()) then

       LibintInterface2_instance%job = trim(job)
       LibintInterface2_instance%maxAngularMoment = maxAngMoment
       LibintInterface2_instance%numberOfPrimitives = numberOfPrimitives


       select case(trim(LibintInterface2_instance%job))
       case("ERIS")
          call LibintInterface_initLibIntBase()
          maxAngMoment_c = LibintInterface2_instance%maxAngularMoment
          numberOfPrimitives_c = LibintInterface2_instance%numberOfPrimitives
          libIntStorage_c = LibintInterface_initLibInt(LibintInterface2_instance%libint, maxAngMoment_c, numberOfPrimitives_c)
          LibintInterface2_instance%libintStorage = libIntStorage_c


          !   case("DERIV")
          !   call LibintInterface2_initLibDerivBase()
          !   call LibintInterface2_initializeLibDeriv()

       ! case("G12")
       !    call LibintInterface_libint2StaticInit()
       !    maxAngMoment_c = LibintInterface2_instance%maxAngularMoment
       !    numberOfPrimitives_c = LibintInterface2_instance%numberOfPrimitives

       !    call LibintInterface_libint2InitR12kg12(LibintInterface2_instance%libintG12, maxAngMoment_c, 0.0_8)
       !    libIntStorage_c = LibintInterface_libint2NeedMemoryR12kg12(maxAngMoment_c)
       !    LibintInterface_instance%libintStorage = libIntStorage_c

       end select

       LibintInterface2_instance%isInstanced = .true.
    else
       call LibintInterface2_exception( ERROR, "in libint interface constructor function",&
            "you must destroy this object before to use it again")
    end if

  end subroutine LibintInterface2_constructor


  !>
  !! @brief Destructor por omision para ser llamdo una sola vez
  !!
  !! @param this
  !<
  subroutine LibintInterface2_destructor()
    implicit none

    if (LibintInterface2_isInstanced()) then

       select case(trim(LibintInterface2_instance%job))
       case("ERIS")
          !call LibintInterface2_freeLibInt(LibintInterface2_instance%libint) !!does not work

          !case("DERIV")
          !   call LibintInterface2_terminateLibDeriv(LibintInterface2_instance%libderiv)

       case("G12")
          !call LibintInterface2_libint2StaticCleanup()

       end select

       LibintInterface2_instance%isInstanced = .false.
    else
       call LibintInterface2_exception( ERROR, "in libint interface destructor function", "you must instantiate this object before to destroying it")
    end if

  end subroutine LibintInterface2_destructor

  !>
  !! @brief Muestra informacion del objeto (una sola vez)
  !!
  !! @param this 
  !<
  subroutine LibintInterface2_show()
    implicit none

    write(*, "(/A)")  "  ========================================================="
    write(*,  "(A)")  "         BEGIN TWO REPULSION INTEGRALS CALCULATION         "
    write(*,  "(A)")  "                     LIBINT library                        "
    write(*, "(A)")   "            Fermann, J. T.; Valeev, F. L. 2010             "
    write(*, "(A)" )  "  ---------------------------------------------------------"
    write(*, "(A)")   "                 IMPLEMENTATION V. 2.0             "
    write(*, "(A)")   "              Posada E. F. ; Reyes A. 2011                 "
    write(*, "(A)")   "  ========================================================="

    write(*, "(/A)" )  "   LIBINT IS RUNNING WITH NEXT PARAMETERS: "
    write(*, "(A, T50, A)")  "  work ", trim(LibintInterface2_instance%job)
    write(*, "(A, T50,A)")  "  Storage ", "DISK"
    write(*, "(A, T43,I5)" )  "  maxAngularMoment", LibintInterface2_instance%maxAngularMoment
    write(*, "(A, T43,I5)" )  "  numberOfPrimitives", LibintInterface2_instance%numberOfPrimitives
    write(*, "(A, T43,I5/)") "  Memory required (in words)", LibintInterface2_instance%libintStorage

  end subroutine LibintInterface2_show


  !<
  !! @brief calculate eris using libint library for all basis set (intra-specie)
  !>
  subroutine LibintInterface2_diskIntraSpecie(nameOfSpecie, specieID, job)
    implicit none

    character(*), intent(in) :: nameOfSpecie
    character(*), intent(in) :: job
    integer, intent(in) :: specieID

    integer :: numberOfContractions
    integer :: totalNumberOfContractions
    integer :: numberOfPrimitives
    integer :: maxAngularMoment
    integer :: sumAngularMoment
    integer :: arraySize !! number of cartesian orbitals for maxAngularMoment
    integer :: n,u,m !! auxiliary itetators
    integer :: aa, bb, rr, ss !! permuted iterators (LIBINT)
    integer :: a, b, r, s !! not permuted iterators (original)
    integer :: pa, pb, pr, ps !! labels index
    integer :: apa, apb, apr, aps !! labels index

    integer :: ii, jj, kk, ll !! cartesian iterators for primitives and contractions
    integer :: aux, order !!auxiliary index
    integer :: arraySsize(1)
    integer :: sizeTotal !!For Large systems
    integer :: auxIndex
    integer :: auxIndexB

    integer :: counter, auxCounter

    integer,target :: i, j, k, l !! contraction length iterators
    integer,pointer :: pi, pj, pk, pl !! pointer to contraction length iterators
    integer,pointer :: poi, poj, pok, pol !! pointer to contraction length iterators
    integer, allocatable :: labelsOfContractions(:) !! cartesian position of contractions in all basis set

    integer*1, allocatable :: bufferA(:) !! avoid rep integrals
    integer :: contador 

    real(8), dimension(:), pointer :: integralsPtr !! pointer to C array of integrals
    real(8), dimension(:), pointer :: temporalPtr

    real(8), allocatable :: auxIntegrals(:) !!array with permuted integrals aux!
    real(8), allocatable :: integralsValue (:) !!array with permuted integrals
    real(8), allocatable :: incompletGamma(:) !!array with incomplete gamma integrals

    real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3) !!geometric values that appear in gaussian product
    real(8) :: zeta, eta, rho !! exponents... that appear in gaussian product
    real(8) :: s1234, s12, s34, AB2, CD2, PQ2 !!geometric quantities that appear in gaussian product
    real(8) :: incompletGammaArgument
    real(8) :: k1234

    real(8) :: startTime, endTime

    type(prim_data), target :: primitiveQuartet !!Primquartet object needed by LIBINT
    type(c_ptr) :: resultPc !! array of integrals from C (LIBINT)

    procedure(LibintInterface_buildLibInt), pointer :: pBuild !!procedure to calculate eris on LIBINT


    call cpu_time(startTime)

    !! open file for integrals
    open( UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//".ints", STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='Unformatted')

    !! Get number of shells and cartesian contractions
    numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
    totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

    !! Libint constructor (solo una vez)
    maxAngularMoment = ParticleManager_getMaxAngularMoment()
    numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

    if( .not. LibintInterface2_isInstanced() ) then
       call LibintInterface2_constructor( maxAngularMoment, numberOfPrimitives, trim(job))
       call LibintInterface2_show()
    end if

    !! allocating space for integrals just one time (do not put it inside do loop!!!)
    arraySize = ((maxAngularMoment + 1)*(maxAngularMoment + 2))/2

    sizeTotal = (totalNumberOfContractions *(totalNumberOfContractions + 1 ))/2
    sizeTotal = (sizeTotal *(sizeTotal + 1))/2

    !! Get contractions labels for integrals index
    if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
    allocate(labelsOfContractions(numberOfContractions))

    !!Real labels for contractions
    aux = 1
    do i = 1, numberOfContractions
       labelsOfContractions(i) = aux
       aux = aux + contraction(i)%numCartesianOrbital
    end do

    contador = 0

    if(allocated(incompletGamma)) deallocate(incompletGamma)
    if(allocated(auxIntegrals)) deallocate(auxIntegrals)
    if(allocated(integralsValue)) deallocate(integralsValue)
    if(allocated(bufferA)) deallocate(bufferA)

    allocate(auxIntegrals(arraySize* arraySize* arraySize * arraySize), &
         integralsValue(arraySize* arraySize* arraySize* arraySize), &
         incompletGamma(0:MaxAngularMoment*4), &
         bufferA(sizeTotal))

    bufferA = 0
    counter = 0
    auxCounter = 0

    !!Start Calculating integrals for each shell
    do a = 1, numberOfContractions
       n = a
       do b = a, numberOfContractions
          u = b
          do r = n , numberOfContractions
             do s = u,  numberOfContractions
                !!Calcula el momento angular total
                sumAngularMoment =  contraction(a)%angularMoment + &
                     contraction(b)%angularMoment + &
                     contraction(r)%angularMoment + &
                     contraction(s)%angularMoment

                !! Calcula el tamano del arreglo de integrales para la capa (ab|rs)
                arraySize = contraction(a)%numCartesianOrbital * &
                     contraction(b)%numCartesianOrbital * &
                     contraction(r)%numCartesianOrbital * &
                     contraction(s)%numCartesianOrbital

                !! For (ab|rs)  ---> RESTRICTION a>b && r>s && r+s > a+b
                aux = 0
                order = 0

                !! permuted index
                aa = a
                bb = b
                rr = r
                ss = s

                !!pointer to permuted index under a not permuted loop
                pi => i
                pj => j
                pk => k
                pl => l

                !!pointer to not permuted index under a permuted loop
                poi => i
                poj => j
                pok => k
                pol => l

                if (contraction(a)%angularMoment < contraction(b)%angularMoment) then

                   aa = b
                   bb = a

                   pi => j
                   pj => i

                   poi => j
                   poj => i

                   order = order + 1

                end if

                if (contraction(r)%angularMoment < contraction(s)%angularMoment) then

                   rr = s
                   ss = r

                   pk => l
                   pl => k

                   pok => l
                   pol => k

                   order = order + 3

                end if

                if((contraction(a)%angularMoment + contraction(b)%angularMoment) > &
                     (contraction(r)%angularMoment + contraction(s)%angularMoment)) then

                   aux = aa
                   aa = rr
                   rr = aux

                   aux = bb
                   bb = ss
                   ss = aux

                   select case(order)
                   case(0)
                      pi => k
                      pj => l
                      pk => i
                      pl => j

                      poi => k
                      poj => l
                      pok => i
                      pol => j

                   case(1)
                      pi => k
                      pj => l
                      pk => j
                      pl => i

                      poi => l
                      poj => k
                      pok => i
                      pol => j

                   case(3)
                      pi => l
                      pj => k
                      pk => i
                      pl => j

                      poi => k
                      poj => l
                      pok => j
                      pol => i

                   case(4)
                      pi => l
                      pj => k
                      pk => j
                      pl => i

                      poi => l
                      poj => k
                      pok => j
                      pol => i

                   end select

                end if

                !!************************************
                !! Calculate iteratively primitives
                !!

                !!Distancias AB, CD
                AB = contraction(aa)%origin - contraction(bb)%origin
                CD = contraction(rr)%origin - contraction(ss)%origin

                AB2 = dot_product(AB, AB)
                CD2 = dot_product(CD, CD)

                !! Asigna valores a la estrucutra Libint
                LibintInterface2_instance%libint%AB = AB
                LibintInterface2_instance%libint%CD = CD

                !!start :)
                integralsValue(1:arraySize) = 0.0_8
                do l = 1, contraction(s)%length
                   do k = 1, contraction(r)%length
                      do j = 1, contraction(b)%length
                         do i = 1, contraction(a)%length

                            !!LIBINT PRIMQUARTET

                            !!Exponentes
                            zeta = primitive(aa,pi)%orbitalExponent + &
                                 primitive(bb,pj)%orbitalExponent

                            eta =  primitive(rr,pk)%orbitalExponent + &
                                 primitive(ss,pl)%orbitalExponent

                            rho  = (zeta * eta) / (zeta + eta) !Exponente reducido ABCD

                            !!prim_data.U
                            P  = (( primitive(aa,pi)%orbitalExponent * contraction(aa)%origin ) + &
                                 ( primitive(bb,pj)%orbitalExponent * contraction(bb)%origin )) / zeta

                            Q  = (( primitive(rr,pk)%orbitalExponent * contraction(rr)%origin ) + &
                                 ( primitive(ss,pl)%orbitalExponent * contraction(ss)%origin )) / eta

                            W  = ((zeta * P) + (eta * Q)) / (zeta + eta)

                            PQ = P - Q

                            primitiveQuartet%U(1:3,1)= (P - contraction(aa)%origin)
                            primitiveQuartet%U(1:3,3)= (Q - contraction(rr)%origin)
                            primitiveQuartet%U(1:3,5)= (W - P)
                            primitiveQuartet%U(1:3,6)= (W - Q)

                            !!Distancias ABCD(PQ2)
                            PQ2 = dot_product(PQ, PQ)

                            !!Evalua el argumento de la funcion gamma incompleta
                            incompletGammaArgument = rho*PQ2

                            !!Overlap Factor
                            s12 = ((Math_PI/zeta)**1.5_8) * exp(-(( primitive(aa,pi)%orbitalExponent * &
                                 primitive(bb,pj)%orbitalExponent) / zeta) * AB2)

                            s34 = ((Math_PI/ eta)**1.5_8) * exp(-(( primitive(rr,pk)%orbitalExponent * &
                                 primitive(ss,pl)%orbitalExponent) /  eta) * CD2)

                            s1234 = sqrt(rho/Math_PI) * s12 * s34

                            call Math_fgamma0(sumAngularMoment,incompletGammaArgument,incompletGamma(0:sumAngularMoment))

                            !!prim_data.F
                            primitiveQuartet%F(1:sumAngularMoment+1) = 2.0_8 * incompletGamma(0:sumAngularMoment) * s1234

                            !!Datos restantes para prim.U
                            primitiveQuartet%oo2z = (0.5_8 / zeta)
                            primitiveQuartet%oo2n = (0.5_8 / eta)
                            primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
                            primitiveQuartet%poz = (rho/zeta)
                            primitiveQuartet%pon = (rho/eta)
                            primitiveQuartet%oo2p = (0.5_8 / rho)

                            if(arraySize == 1) then

                               auxIntegrals(1) = primitiveQuartet%F(1)

                            else

                               arraySsize(1) = arraySize

                               LibintInterface2_instance%libint%PrimQuartet = c_loc(primitiveQuartet)

                               call c_f_procpointer(build_eri( contraction(ss)%angularMoment , &
                                    contraction(rr)%angularMoment , &
                                    contraction(bb)%angularMoment , &
                                    contraction(aa)%angularMoment), pBuild)

                               resultPc = pBuild(LibintInterface2_instance%libint,1)

                               call c_f_pointer(resultPc, temporalPtr, arraySsize)

                               integralsPtr => temporalPtr

                               auxIntegrals(1:arraySize) = integralsPtr(1:arraySize) !!it is to slow with pointer...! so.. copy

                            end if !!done by primitives

                            !!Normalize by primitives
                            m = 0
                            do ii = 1, contraction(aa)%numCartesianOrbital
                               do jj = 1, contraction(bb)%numCartesianOrbital
                                  do kk = 1, contraction(rr)%numCartesianOrbital
                                     do ll = 1, contraction(ss)%numCartesianOrbital
                                        m = m + 1
                                        auxIntegrals(m) = auxIntegrals(m) &
                                             * primitive(aa,pi)%normalizationConstant(ii) &
                                             * primitive(bb,pj)%normalizationConstant(jj) &
                                             * primitive(rr,pk)%normalizationConstant(kk) &
                                             * primitive(ss,pl)%normalizationConstant(ll)
                                     end do
                                  end do
                               end do
                            end do !! done by cartesian of contractions


                            auxIntegrals(1:arraySize) = auxIntegrals(1:arraySize) &
                                 * contraction(aa)%contractionCoefficients(pi) &
                                 * contraction(bb)%contractionCoefficients(pj) &
                                 * contraction(rr)%contractionCoefficients(pk) &
                                 * contraction(ss)%contractionCoefficients(pl)

                            integralsValue(1:arraySize) = integralsValue(1:arraySize) + auxIntegrals(1:arraySize)

                         end do
                      end do
                   end do
                end do !!done by contractios

                !!normalize by contraction
                m = 0
                do ii = 1,  contraction(aa)%numCartesianOrbital
                   do jj = 1,  contraction(bb)%numCartesianOrbital
                      do kk = 1,  contraction(rr)%numCartesianOrbital
                         do ll = 1, contraction(ss)%numCartesianOrbital
                            m = m + 1

                            integralsValue(m) = integralsValue(m) &
                                 * contraction(aa)%normalizationConstant(ii) &
                                 * contraction(bb)%normalizationConstant(jj) &
                                 * contraction(rr)%normalizationConstant(kk) &
                                 * contraction(ss)%normalizationConstant(ll)
                         end do
                      end do
                   end do
                end do !! done by cartesian of contractions

                !!write to disk
                m = 0
                do i = 1, contraction(aa)%numCartesianOrbital
                   do j = 1, contraction(bb)%numCartesianOrbital
                      do k = 1, contraction(rr)%numCartesianOrbital
                         do l = 1, contraction(ss)%numCartesianOrbital

                            m = m + 1


                            !! index not permuted
                            pa=labelsOfContractions(a)+poi-1
                            pb=labelsOfContractions(b)+poj-1
                            pr=labelsOfContractions(r)+pok-1
                            ps=labelsOfContractions(s)+pol-1

                            apa=pa
                            apb=pb
                            apr=pr
                            aps=ps

                            if( pa <= pb .and. pr <= ps .and. (pa*1000)+pb >= (pr*1000)+ps) then

                               aux = pa
                               pa = pr
                               pr = aux

                               aux = pb
                               pb = ps
                               ps = aux

                            end if

                            if( pa <= pb .and. pr <= ps ) then

                               !auxIndexB=auxIndex
                               !auxIndex = IndexMap_tensorR4ToVector(pa,pb,pr,ps, totalNumberOfContractions ) !!Esto es lento (cambiar)!!!!!!!!

                               if (  (apa /= pa .or. apb/=pb .or. apr/=pr .or. aps/=ps) .and. ( b==s ) ) then

                                  !((apa==apb .and. aps-apr<=1) .or. ( apr==aps .and. apb-apa<=1) .or. &
                                  !(apa==apr ) .or. (apa==aps) .or. (apb==aps .and. apa-apr<=2) .or. (b==s)) )then
                                  !if  ( auxIndex <= auxIndexB .and. b==s) then
                                  !print *, "    ========="
                                  !print *, "    a,b,r,s,aI", pa,pb,pr,ps,auxIndex
                                  !print *, "    a,b,r,s,a2", apa,apb,apr,aps,aux

                                  !contador=contador+1

                               else 

                                  !if(bufferA(auxIndex) == 0) then
                                  !bufferA(auxIndex) = 1

                                  if(abs(integralsValue(m)) > 1.0D-10) then

                                     eris%a = pa
                                     eris%b = pb
                                     eris%c = pr
                                     eris%d = ps
                                     eris%integrals = integralsValue(m)

                                     write(34) &
                                          eris%a, &
                                          eris%b, &
                                          eris%c, &
                                          eris%d, &
                                          eris%integrals
                                  end if

                                  !else if (bufferA(auxIndex) == 1 ) then

                                  !print *, "XXXXXXXXXXX"
                                  !print *, "pa,b,r,s,aI", pa,pb,pr,ps,auxIndex
                                  !print *, "apa,b,r,s,a", apa,apb,apr,aps,auxIndex
                                  !print *, "a,b,r,s    ",a,b,r,s

                                  !if ( b==s) then
                                  !   print *, "||||||||||||||||"
                                  !end if

                                  !contador = contador +1

                               end if
                            end if
                         end do
                      end do
                   end do
                end do
             end do
             u=r+1
          end do
       end do
    end do !! done by basis set

    eris%a = -1
    eris%b = -1
    eris%c = -1
    eris%d = -1
    eris%integrals = 0.0_8


    write(34) &
         eris%a, &
         eris%b, &
         eris%c, &
         eris%d, &
         eris%integrals

    close(34)

    !deallocate(bufferA)

    call cpu_time(endTime)

    write(6,"(A,I10,A,F12.4,A)") "*****Time for ", auxCounter, "   "//trim(nameOfSpecie)//" non-zero integrals ", (endTime - startTime), "(s)"

  end subroutine LibintInterface2_diskIntraSpecie


!   !<
!   !! calculate eris using libint library for all basis set (inter-specie)
!   !>
!   subroutine LibintInterface2_diskInterSpecie(nameOfSpecie, otherNameOfSpecie,  specieID, otherSpecieID, job, isInterSpecies, isCouplingA, isCouplingB)
!     implicit none

!     integer,target :: specieID
!     integer,target :: otherSpecieID
!     character(*) :: nameOfSpecie
!     character(*) :: otherNameOfSpecie
!     character(*), intent(in) :: job
!     logical, optional :: isInterSpecies
!     logical, optional :: isCouplingA
!     logical, optional :: isCouplingB

!     logical :: interSpecies
!     logical :: couplingA
!     logical :: couplingB

!     integer :: totalNumberOfContractions
!     integer :: otherTotalNumberOfContractions
!     integer :: numberOfContractions
!     integer :: otherNumberOfContractions
!     integer :: numberOfPrimitives
!     integer :: maxAngularMoment
!     integer :: sumAngularMoment
!     integer :: arraySize !! number of cartesian orbitals for maxAngularMoment
!     integer :: n,u,m !! auxiliary itetators
!     integer :: aa, bb, rr, ss !! permuted iterators (LIBINT)
!     integer :: a, b, r, s !! not permuted iterators (original)
!     integer*2 :: pa, pb, pr, ps !! labels index
!     integer :: ii, jj, kk, ll !! cartesian iterators for primitives and contractions
!     integer :: aux, order !!auxiliary index
!     integer :: arraySsize(1)
!     integer :: sizeTotal
!     integer :: auxIndex
!     integer :: counter, auxCounter

!     integer,target :: i, j, k, l !! contraction length iterators
!     integer,pointer :: pi, pj, pk, pl !! pointer to contraction length iterators
!     integer,pointer :: poi, poj, pok, pol !! pointer to contraction length iterators
!     integer, pointer :: pSpecieID, pOtherSpecieID !! pointer to species ID
!     integer, allocatable :: labelsOfContractions(:) !! cartesian position of contractions in all basis set
!     integer, allocatable :: otherLabelsOfContractions(:) !! cartesian position of contractions in all basis set
!     !integer, allocatable :: buffer(:) !! avoid rep integrals
!     !integer*1, allocatable :: buffer(:) !! avoid rep integrals

!     real(8), dimension(:), pointer :: integralsPtr !! pointer to C array of integrals
!     real(8), dimension(:), pointer :: temporalPtr

!     real(8), allocatable :: auxIntegrals(:) !!array with permuted integrals aux!
!     real(8), allocatable :: integralsValue (:) !!array with permuted integrals
!     real(8), allocatable :: incompletGamma(:) !!array with incomplete gamma integrals

!     real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3) !!geometric values that appear in gaussian product
!     real(8) :: zeta, eta, rho !! exponents... that appear in gaussian product
!     real(8) :: s1234, s12, s34, AB2, CD2, PQ2 !!geometric quantities that appear in gaussian product
!     real(8) :: incompletGammaArgument
!     real(8) :: k1234

!     real(8) :: startTime, endTime

!     type(prim_data), target :: primitiveQuartet !!Primquartet object needed by LIBINT
!     type(c_ptr) :: resultPc !! array of integrals from C (LIBINT)

!     procedure(LibintInterface_buildLibInt), pointer :: pBuild !!procedure to calculate eris on LIBINT

!     call cpu_time(startTime)

!     interSpecies = .true.
!     couplingA = .false.
!     couplingB = .false.

!     if(present(isInterSpecies)) interSpecies = isInterSpecies
!     if(present(isCouplingA)) couplingA = isCouplingA
!     if(present(isCouplingB)) couplingB = isCouplingB

!     !! open file for integrals
!     open(UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//"."//trim(otherNameOfSpecie)//".ints", &
!          STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='Unformatted')

!     !! Get number of shells and cartesian contractions
!     numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
!     totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

!     !! Get number of shells and cartesian contractions (other specie)
!     otherNumberOfContractions = ParticleManager_getNumberOfContractions( otherSpecieID )
!     otherTotalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( otherSpecieID )

!     !! Libint constructor (solo una vez)
!     maxAngularMoment = ParticleManager_getMaxAngularMoment()
!     numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

!     if( .not. LibintInterface2_isInstanced() ) then
!        call LibintInterface2_constructor( maxAngularMoment, numberOfPrimitives, trim(job))
!        call LibintInterface2_show()
!     end if

!     !! allocating space for integrals just one time (do not put it inside do loop!!!)
!     arraySize = ((maxAngularMoment + 1)*(maxAngularMoment + 2))/2

!     sizeTotal = ((totalNumberOfContractions *(totalNumberOfContractions + 1 ))/2) * ((otherTotalNumberOfContractions *(otherTotalNumberOfContractions + 1 ))/2)

!     !! Get contractions labels for integrals index
!     if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
!     allocate(labelsOfContractions(numberOfContractions))

!     !!Real labels for contractions
!     aux = 1
!     do i = 1, numberOfContractions
!        labelsOfContractions(i) = aux
!        aux = aux + contraction(i)%numCartesianOrbital
!     end do

!     !! Get contractions labels for integrals index (other specie)
!     if (allocated(otherLabelsOfContractions)) deallocate(otherLabelsOfContractions)
!     allocate(otherLabelsOfContractions(otherNumberOfContractions))

!     !!Real labels for contractions (other specie)
!     aux = 1
!     do i = 1, otherNumberOfContractions
!        otherLabelsOfContractions(i) = aux
!        aux = aux + otherContraction(i,otherSpecieID)%numCartesianOrbital
!     end do

!     !! Allocating some space
!     if(allocated(incompletGamma)) deallocate(incompletGamma)
!     if(allocated(auxIntegrals)) deallocate(auxIntegrals)
!     if(allocated(integralsValue)) deallocate(integralsValue)
!     !if(allocated(buffer)) deallocate(buffer)

!     allocate(auxIntegrals(arraySize* arraySize* arraySize * arraySize), &
!          integralsValue(arraySize* arraySize* arraySize* arraySize), &
!          incompletGamma(0:MaxAngularMoment*4))    
!     !buffer(sizeTotal))

!     !buffer = 0
!     counter = 0
!     auxCounter = 0

!     eris%a(1:INTEGRAL_STACK_SIZE)=1
!     eris%b(1:INTEGRAL_STACK_SIZE)=1
!     eris%c(1:INTEGRAL_STACK_SIZE)=1
!     eris%d(1:INTEGRAL_STACK_SIZE)=1
!     eris%integrals(1:INTEGRAL_STACK_SIZE)=1.0_8

!     !!Start Calculating integrals for each shell
!     do a = 1, numberOfContractions
!        do b = a, numberOfContractions
!           do r = 1 , otherNumberOfContractions
!              do s = r,  otherNumberOfContractions

!                 !!Calcula el momento angular total
!                 sumAngularMoment =  contraction(a)%angularMoment + &
!                      contraction(b)%angularMoment + &
!                      otherContraction(r,otherSpecieID)%angularMoment + &
!                      otherContraction(s,otherSpecieID)%angularMoment

!                 !! Calcula el tamano del arreglo de integrales para la capa (ab|rs)
!                 arraySize = contraction(a)%numCartesianOrbital * &
!                      contraction(b)%numCartesianOrbital * &
!                      otherContraction(r,otherSpecieID)%numCartesianOrbital * &
!                      otherContraction(s,otherSpecieID)%numCartesianOrbital

!                 !! For (ab|rs)  ---> RESTRICTION a>b && r>s && r+s > a+b
!                 aux = 0
!                 order = 0

!                 !! permuted index
!                 aa = a
!                 bb = b
!                 rr = r
!                 ss = s

!                 !!pointer to permuted index under a not permuted loop
!                 pi => i
!                 pj => j
!                 pk => k
!                 pl => l

!                 !!pointer to not permuted index under a permuted loop
!                 poi => i
!                 poj => j
!                 pok => k
!                 pol => l

!                 !!Pointer to specie ID
!                 pSpecieID => specieID
!                 pOtherSpecieID => otherSpecieID

!                 if (contraction(a)%angularMoment < contraction(b)%angularMoment) then

!                    aa = b
!                    bb = a

!                    pi => j
!                    pj => i

!                    poi => j
!                    poj => i

!                    order = order + 1
!                 end if

!                 if (otherContraction(r,otherSpecieID)%angularMoment < otherContraction(s,otherSpecieID)%angularMoment) then

!                    rr = s
!                    ss = r

!                    pk => l
!                    pl => k

!                    pok => l
!                    pol => k

!                    order = order + 3

!                 end if

!                 if((contraction(a)%angularMoment + contraction(b)%angularMoment) > &
!                      (otherContraction(r,otherSpecieID)%angularMoment + otherContraction(s,otherSpecieID)%angularMoment)) then

!                    aux = aa
!                    aa = rr
!                    rr = aux

!                    aux = bb
!                    bb = ss
!                    ss = aux

!                    pSpecieID => otherSpecieID
!                    pOtherSpecieID => specieID

!                    select case(order)
!                    case(0)
!                       pi => k
!                       pj => l
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => i
!                       pol => j

!                    case(1)
!                       pi => k
!                       pj => l
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => i
!                       pol => j

!                    case(3)
!                       pi => l
!                       pj => k
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => j
!                       pol => i

!                    case(4)
!                       pi => l
!                       pj => k
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => j
!                       pol => i

!                    end select

!                    order = order + 5

!                 end if

!                 !!************************************
!                 !! Calculate iteratively primitives
!                 !!

!                 !!Distancias AB, CD
!                 AB = otherContraction(aa, pSpecieID)%origin - otherContraction(bb, pSpecieID)%origin
!                 CD = otherContraction(rr, pOtherSpecieID)%origin - otherContraction(ss, pOtherSpecieID)%origin

!                 AB2 = dot_product(AB, AB)
!                 CD2 = dot_product(CD, CD)

!                 !! Asigna valores a la estrucutra Libint
!                 LibintInterface2_instance%libint%AB = AB
!                 LibintInterface2_instance%libint%CD = CD

!                 !!start :)
!                 integralsValue(1:arraySize) = 0.0_8

!                 !! not-permuted loop
!                 do l = 1, otherContraction(s,otherSpecieID)%length
!                    do k = 1, otherContraction(r,otherSpecieID)%length
!                       do j = 1, contraction(b)%length
!                          do i = 1, contraction(a)%length

!                             !!LIBINT PRIMQUARTET

!                             !!Exponentes
!                             zeta = otherPrimitive(aa,pi,pSpecieID)%orbitalExponent + &
!                                  otherPrimitive(bb,pj,pSpecieID)%orbitalExponent

!                             eta =  otherPrimitive(rr,pk,pOtherSpecieID)%orbitalExponent + &
!                                  otherPrimitive(ss,pl,pOtherSpecieID)%orbitalExponent

!                             rho  = (zeta * eta) / (zeta + eta) !Exponente reducido ABCD

!                             !!prim_data.U
!                             P  = (( otherPrimitive(aa,pi,pSpecieID)%orbitalExponent * otherContraction(aa,pSpecieID)%origin ) + &
!                                  ( otherPrimitive(bb,pj,pSpecieID)%orbitalExponent * otherContraction(bb,pSpecieID)%origin )) / zeta

!                             Q  = (( otherPrimitive(rr,pk,pOtherSpecieID)%orbitalExponent * otherContraction(rr,pOtherSpecieID)%origin ) + &
!                                  ( otherPrimitive(ss,pl,pOtherSpecieID)%orbitalExponent * otherContraction(ss,pOtherSpecieID)%origin )) / eta

!                             W  = ((zeta * P) + (eta * Q)) / (zeta + eta)

!                             PQ = P - Q

!                             primitiveQuartet%U(1:3,1)= (P - otherContraction(aa,pSpecieID)%origin)
!                             primitiveQuartet%U(1:3,3)= (Q - otherContraction(rr,pOtherSpecieID)%origin)
!                             primitiveQuartet%U(1:3,5)= (W - P)
!                             primitiveQuartet%U(1:3,6)= (W - Q)

!                             !!Distancias ABCD(PQ2)
!                             PQ2 = dot_product(PQ, PQ)

!                             !!Evalua el argumento de la funcion gamma incompleta
!                             incompletGammaArgument = rho*PQ2

!                             !!Overlap Factor
!                             s12 = ((Math_PI/zeta)**1.5_8) * exp(-(( otherPrimitive(aa,pi,pSpecieID)%orbitalExponent * &
!                                  otherPrimitive(bb,pj,pSpecieID)%orbitalExponent) / zeta) * AB2)

!                             s34 = ((Math_PI/ eta)**1.5_8) * exp(-(( otherPrimitive(rr,pk,pOtherSpecieID)%orbitalExponent * &
!                                  otherPrimitive(ss,pl,pOtherSpecieID)%orbitalExponent) /  eta) * CD2)

!                             s1234 = sqrt(rho/Math_PI) * s12 * s34

!                             call Math_fgamma0(sumAngularMoment,incompletGammaArgument,incompletGamma(0:sumAngularMoment))

!                             !!prim_data.F
!                             primitiveQuartet%F(1:sumAngularMoment+1) = 2.0_8 * incompletGamma(0:sumAngularMoment) * s1234

!                             !!Datos restantes para prim.U
!                             primitiveQuartet%oo2z = (0.5_8 / zeta)
!                             primitiveQuartet%oo2n = (0.5_8 / eta)
!                             primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
!                             primitiveQuartet%poz = (rho/zeta)
!                             primitiveQuartet%pon = (rho/eta)
!                             primitiveQuartet%oo2p = (0.5_8 / rho)

!                             if(arraySize == 1) then

!                                auxIntegrals(1) = primitiveQuartet%F(1)

!                             else

!                                arraySsize(1) = arraySize

!                                LibintInterface2_instance%libint%PrimQuartet = c_loc(primitiveQuartet)

!                                call c_f_procpointer(build_eri( otherContraction(ss,pOtherSpecieID)%angularMoment , &
!                                     otherContraction(rr,pOtherSpecieID)%angularMoment , &
!                                     otherContraction(bb,pSpecieID)%angularMoment , &
!                                     otherContraction(aa,pSpecieID)%angularMoment), pBuild)

!                                resultPc = pBuild(LibintInterface2_instance%libint,1)

!                                call c_f_pointer(resultPc, temporalPtr, arraySsize)

!                                integralsPtr => temporalPtr

!                                auxIntegrals(1:arraySize) = integralsPtr(1:arraySize) !!it is to slow with pointer...! so.. copy

!                             end if !!done by primitives

!                             !!Normalize by primitives
!                             m = 0
!                             do ii = 1, otherContraction(aa,pSpecieID)%numCartesianOrbital
!                                do jj = 1, otherContraction(bb,pSpecieID)%numCartesianOrbital
!                                   do kk = 1, otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                                      do ll = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital
!                                         m = m + 1
!                                         auxIntegrals(m) = auxIntegrals(m) &
!                                              * otherPrimitive(aa,pi,pSpecieID)%normalizationConstant(ii) &
!                                              * otherPrimitive(bb,pj,pSpecieID)%normalizationConstant(jj) &
!                                              * otherPrimitive(rr,pk,pOtherSpecieID)%normalizationConstant(kk) &
!                                              * otherPrimitive(ss,pl,pOtherSpecieID)%normalizationConstant(ll)
!                                      end do
!                                   end do
!                                end do
!                             end do !! done by cartesian of contractions


!                             auxIntegrals(1:arraySize) = auxIntegrals(1:arraySize) &
!                                  * otherContraction(aa,pSpecieID)%contractionCoefficients(pi) &
!                                  * otherContraction(bb,pSpecieID)%contractionCoefficients(pj) &
!                                  * otherContraction(rr,pOtherSpecieID)%contractionCoefficients(pk) &
!                                  * otherContraction(ss,pOtherSpecieID)%contractionCoefficients(pl)

!                             integralsValue(1:arraySize) = integralsValue(1:arraySize) + auxIntegrals(1:arraySize)

!                          end do
!                       end do
!                    end do
!                 end do !!done by contractios

!                 !!normalize by contraction
!                 m = 0
!                 do ii = 1,  otherContraction(aa,pSpecieID)%numCartesianOrbital
!                    do jj = 1,  otherContraction(bb,pSpecieID)%numCartesianOrbital
!                       do kk = 1,  otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                          do ll = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital
!                             m = m + 1

!                             integralsValue(m) = integralsValue(m) &
!                                  * otherContraction(aa,pSpecieID)%normalizationConstant(ii) &
!                                  * otherContraction(bb,pSpecieID)%normalizationConstant(jj) &
!                                  * otherContraction(rr,pOtherSpecieID)%normalizationConstant(kk) &
!                                  * otherContraction(ss,pOtherSpecieID)%normalizationConstant(ll)
!                          end do
!                       end do
!                    end do
!                 end do !! done by cartesian of contractions
!                 !!write to disk
!                 m = 0
!                 do i = 1, otherContraction(aa,pSpecieID)%numCartesianOrbital
!                    do j = 1, otherContraction(bb,pSpecieID)%numCartesianOrbital
!                       do k = 1, otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                          do l = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital

!                             m = m + 1

!                             !! index not permuted
!                             pa=labelsOfContractions(a)+poi-1
!                             pb=labelsOfContractions(b)+poj-1
!                             pr=otherLabelsOfContractions(r)+pok-1
!                             ps=otherLabelsOfContractions(s)+pol-1

!                             if( pa <= pb .and. pr <= ps ) then

!                                !if(interSpecies) then

!                                !   auxIndex = IndexMap_tensorR4ToVector22(pa,pb,pr,ps, totalNumberOfContractions, otherTotalNumberOfContractions )

!                                !else if(couplingA) then

!                                !auxIndex = IndexMap_tensorR4ToVector33(pa,pb,pr,ps, totalNumberOfContractions, otherTotalNumberOfContractions )

!                                !else if(couplingB) then

!                                !   auxIndex = IndexMap_tensorR4ToVector33(pr,ps,pa,pb, otherTotalNumberOfContractions, totalNumberOfContractions )

!                                !end if

!                                !if(buffer(auxIndex) == 0) then

!                                !buffer(auxIndex) = 1

!                                if(abs(integralsValue(m)) > 1.0D-10) then

!                                   counter = counter + 1
!                                   auxCounter = auxCounter + 1

!                                   eris%a(counter) = pa
!                                   eris%b(counter) = pb
!                                   eris%c(counter) = pr
!                                   eris%d(counter) = ps
!                                   eris%integrals(counter) = integralsValue(m)

!                                end if


!                                if( counter == INTEGRAL_STACK_SIZE ) then

!                                   write(34) eris%a(1:INTEGRAL_STACK_SIZE), eris%b(1:INTEGRAL_STACK_SIZE), &
!                                        eris%c(1:INTEGRAL_STACK_SIZE), eris%d(1:INTEGRAL_STACK_SIZE), &
!                                        eris%integrals(1:INTEGRAL_STACK_SIZE)
!                                   counter = 0

!                                end if

!                             end if
!                          end do
!                       end do
!                    end do
!                 end do
!              end do
!              u=r+1
!           end do
!        end do
!     end do !! done by basis set

!     eris%a(counter+1) = -1
!     eris%b(counter+1) = -1
!     eris%c(counter+1) = -1
!     eris%d(counter+1) = -1
!     eris%integrals(counter+1) = 0.0_8	

!     !print *, "pa,pb,pr,ps ", pa,pb,pr,ps

!     write(34) eris%a(1:INTEGRAL_STACK_SIZE), eris%b(1:INTEGRAL_STACK_SIZE), eris%c(1:INTEGRAL_STACK_SIZE), &
!          eris%d(1:INTEGRAL_STACK_SIZE), eris%integrals(1:INTEGRAL_STACK_SIZE)

!     close(34)

!     !deallocate(buffer)

!     call cpu_time(endTime)

!     write(6,"(A,I10,A,F12.2,A)") "*****Time for ", auxCounter, "   "//trim(nameOfSpecie)//"/"//trim(otherNameOfSpecie)//" non-zero integrals ", (endTime - startTime), "(s)"

!   end subroutine LibintInterface2_diskInterSpecie



!   !<
!   !! @brief calculate eris using libint library for all basis set (intra-specie) on the fly
!   !>   
!   subroutine LibintInterface2_directIntraSpecie(nameOfSpecie, specieID, job, densityMatrix, twoParticlesMatrix, factor)
!     implicit none

!     character(*), intent(in) :: nameOfSpecie
!     character(*), intent(in) :: job
!     integer, intent(in) :: specieID
!     type(matrix), intent(inout):: twoParticlesMatrix
!     type(matrix), intent(in) :: densityMatrix
!     real(8), intent(in) :: factor

!     integer :: numberOfContractions
!     integer :: totalNumberOfContractions
!     integer :: numberOfPrimitives
!     integer :: maxAngularMoment
!     integer :: sumAngularMoment
!     integer :: arraySize !! number of cartesian orbitals for maxAngularMoment
!     integer :: n,u,m !! auxiliary itetators
!     integer :: aa, bb, rr, ss !! permuted iterators (LIBINT)
!     integer :: a, b, r, s !! not permuted iterators (original)
!     integer*2 :: pa, pb, pr, ps !! labels index
!     integer*2 :: apa, apb, apr, aps !! labels index

!     integer :: ii, jj, kk, ll !! cartesian iterators for primitives and contractions
!     integer :: aux, order !!auxiliary index
!     integer :: arraySsize(1)
!     integer :: auxIndex

!     integer :: counter, auxCounter

!     integer,target :: i, j, k, l !! contraction length iterators
!     integer,pointer :: pi, pj, pk, pl !! pointer to contraction length iterators
!     integer,pointer :: poi, poj, pok, pol !! pointer to contraction length iterators
!     integer, allocatable :: labelsOfContractions(:) !! cartesian position of contractions in all basis set

!     real(8), dimension(:), pointer :: integralsPtr !! pointer to C array of integrals
!     real(8), dimension(:), pointer :: temporalPtr

!     real(8), allocatable :: auxIntegrals(:) !!array with permuted integrals aux!
!     real(8), allocatable :: integralsValue (:) !!array with permuted integrals
!     real(8), allocatable :: incompletGamma(:) !!array with incomplete gamma integrals

!     real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3) !!geometric values that appear in gaussian product
!     real(8) :: zeta, eta, rho !! exponents... that appear in gaussian product
!     real(8) :: s1234, s12, s34, AB2, CD2, PQ2 !!geometric quantities that appear in gaussian product
!     real(8) :: incompletGammaArgument
!     real(8) :: k1234

!     real(8) :: startTime, endTime

!     real(8) :: coulomb,exchange

!     type(prim_data), target :: primitiveQuartet !!Primquartet object needed by LIBINT
!     type(c_ptr) :: resultPc !! array of integrals from C (LIBINT)

!     procedure(LibintInterface_buildLibInt), pointer :: pBuild !!procedure to calculate eris on LIBINT

!     call cpu_time(startTime)
!     !! Get number of shells and cartesian contractions
!     numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
!     totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

!     !! Libint constructor (solo una vez)
!     maxAngularMoment = ParticleManager_getMaxAngularMoment()
!     numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

!     if( .not. LibintInterface2_isInstanced() ) then
!        call LibintInterface2_constructor( maxAngularMoment, numberOfPrimitives, trim(job))
!        call LibintInterface2_show()
!     end if

!     !! allocating space for integrals just one time (do not put it inside do loop!!!)
!     arraySize = ((maxAngularMoment + 1)*(maxAngularMoment + 2))/2

!     !! Get contractions labels for integrals index
!     if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
!     allocate(labelsOfContractions(numberOfContractions))

!     !!Real labels for contractions
!     aux = 1
!     do i = 1, numberOfContractions
!        labelsOfContractions(i) = aux
!        aux = aux + contraction(i)%numCartesianOrbital
!     end do

!     if(allocated(incompletGamma)) deallocate(incompletGamma)
!     if(allocated(auxIntegrals)) deallocate(auxIntegrals)
!     if(allocated(integralsValue)) deallocate(integralsValue)

!     allocate(auxIntegrals(arraySize* arraySize* arraySize * arraySize), &
!          integralsValue(arraySize* arraySize* arraySize* arraySize), &
!          incompletGamma(0:MaxAngularMoment*4))

!     counter = 0
!     auxCounter = 0

!     !print *, "number of contractions", numberOfContractions

!     !!Start Calculating integrals for each shell
!     do a = 1, numberOfContractions
!        n = a
!        do b = a, numberOfContractions
!           u = b
!           do r = n , numberOfContractions
!              do s = u,  numberOfContractions
!                 !!Calcula el momento angular total
!                 sumAngularMoment =  contraction(a)%angularMoment + &
!                      contraction(b)%angularMoment + &
!                      contraction(r)%angularMoment + &
!                      contraction(s)%angularMoment

!                 !! Calcula el tamano del arreglo de integrales para la capa (ab|rs)
!                 arraySize = contraction(a)%numCartesianOrbital * &
!                      contraction(b)%numCartesianOrbital * &
!                      contraction(r)%numCartesianOrbital * &
!                      contraction(s)%numCartesianOrbital

!                 !! For (ab|rs)  ---> RESTRICTION a>b && r>s && r+s > a+b
!                 aux = 0
!                 order = 0

!                 !! permuted index
!                 aa = a
!                 bb = b
!                 rr = r
!                 ss = s

!                 !!pointer to permuted index under a not permuted loop
!                 pi => i
!                 pj => j
!                 pk => k
!                 pl => l

!                 !!pointer to not permuted index under a permuted loop
!                 poi => i
!                 poj => j
!                 pok => k
!                 pol => l

!                 if (contraction(a)%angularMoment < contraction(b)%angularMoment) then

!                    aa = b
!                    bb = a

!                    pi => j
!                    pj => i

!                    poi => j
!                    poj => i

!                    order = order + 1
!                 end if

!                 if (contraction(r)%angularMoment < contraction(s)%angularMoment) then

!                    rr = s
!                    ss = r

!                    pk => l
!                    pl => k

!                    pok => l
!                    pol => k

!                    order = order + 3

!                 end if

!                 if((contraction(a)%angularMoment + contraction(b)%angularMoment) > &
!                      (contraction(r)%angularMoment + contraction(s)%angularMoment)) then

!                    aux = aa
!                    aa = rr
!                    rr = aux

!                    aux = bb
!                    bb = ss
!                    ss = aux

!                    select case(order)
!                    case(0)

!                       pi => k
!                       pj => l
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => i
!                       pol => j

!                    case(1)
!                       pi => k
!                       pj => l
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => i
!                       pol => j

!                    case(3)
!                       pi => l
!                       pj => k
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => j
!                       pol => i

!                    case(4)
!                       pi => l
!                       pj => k
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => j
!                       pol => i

!                    end select

!                 end if

!                 !!************************************
!                 !! Calculate iteratively primitives
!                 !!

!                 !!Distancias AB, CD
!                 AB = contraction(aa)%origin - contraction(bb)%origin
!                 CD = contraction(rr)%origin - contraction(ss)%origin

!                 AB2 = dot_product(AB, AB)
!                 CD2 = dot_product(CD, CD)

!                 !! Asigna valores a la estrucutra Libint
!                 LibintInterface2_instance%libint%AB = AB
!                 LibintInterface2_instance%libint%CD = CD

!                 !!start :)
!                 integralsValue(1:arraySize) = 0.0_8
!                 do l = 1, contraction(s)%length
!                    do k = 1, contraction(r)%length
!                       do j = 1, contraction(b)%length
!                          do i = 1, contraction(a)%length

!                             !!LIBINT PRIMQUARTET

!                             !!Exponentes
!                             zeta = primitive(aa,pi)%orbitalExponent + &
!                                  primitive(bb,pj)%orbitalExponent

!                             eta =  primitive(rr,pk)%orbitalExponent + &
!                                  primitive(ss,pl)%orbitalExponent

!                             rho  = (zeta * eta) / (zeta + eta) !Exponente reducido ABCD

!                             !!prim_data.U
!                             P  = (( primitive(aa,pi)%orbitalExponent * contraction(aa)%origin ) + &
!                                  ( primitive(bb,pj)%orbitalExponent * contraction(bb)%origin )) / zeta

!                             Q  = (( primitive(rr,pk)%orbitalExponent * contraction(rr)%origin ) + &
!                                  ( primitive(ss,pl)%orbitalExponent * contraction(ss)%origin )) / eta

!                             W  = ((zeta * P) + (eta * Q)) / (zeta + eta)

!                             PQ = P - Q

!                             primitiveQuartet%U(1:3,1)= (P - contraction(aa)%origin)
!                             primitiveQuartet%U(1:3,3)= (Q - contraction(rr)%origin)
!                             primitiveQuartet%U(1:3,5)= (W - P)
!                             primitiveQuartet%U(1:3,6)= (W - Q)

!                             !!Distancias ABCD(PQ2)
!                             PQ2 = dot_product(PQ, PQ)

!                             !!Evalua el argumento de la funcion gamma incompleta
!                             incompletGammaArgument = rho*PQ2

!                             !!Overlap Factor
!                             s12 = ((Math_PI/zeta)**1.5_8) * exp(-(( primitive(aa,pi)%orbitalExponent * &
!                                  primitive(bb,pj)%orbitalExponent) / zeta) * AB2)

!                             s34 = ((Math_PI/ eta)**1.5_8) * exp(-(( primitive(rr,pk)%orbitalExponent * &
!                                  primitive(ss,pl)%orbitalExponent) /  eta) * CD2)

!                             s1234 = sqrt(rho/Math_PI) * s12 * s34

!                             call Math_fgamma0(sumAngularMoment,incompletGammaArgument,incompletGamma(0:sumAngularMoment))

!                             !!prim_data.F
!                             primitiveQuartet%F(1:sumAngularMoment+1) = 2.0_8 * incompletGamma(0:sumAngularMoment) * s1234

!                             !!Datos restantes para prim.U
!                             primitiveQuartet%oo2z = (0.5_8 / zeta)
!                             primitiveQuartet%oo2n = (0.5_8 / eta)
!                             primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
!                             primitiveQuartet%poz = (rho/zeta)
!                             primitiveQuartet%pon = (rho/eta)
!                             primitiveQuartet%oo2p = (0.5_8 / rho)

!                             if(arraySize == 1) then

!                                auxIntegrals(1) = primitiveQuartet%F(1)

!                             else

!                                arraySsize(1) = arraySize

!                                LibintInterface2_instance%libint%PrimQuartet = c_loc(primitiveQuartet)

!                                call c_f_procpointer(build_eri( contraction(ss)%angularMoment , &
!                                     contraction(rr)%angularMoment , &
!                                     contraction(bb)%angularMoment , &
!                                     contraction(aa)%angularMoment), pBuild)

!                                resultPc = pBuild(LibintInterface2_instance%libint,1)

!                                call c_f_pointer(resultPc, temporalPtr, arraySsize)

!                                integralsPtr => temporalPtr

!                                auxIntegrals(1:arraySize) = integralsPtr(1:arraySize) !!it is to slow with pointer...! so.. copy

!                             end if !!done by primitives

!                             !!Normalize by primitives
!                             m = 0
!                             do ii = 1, contraction(aa)%numCartesianOrbital
!                                do jj = 1, contraction(bb)%numCartesianOrbital
!                                   do kk = 1, contraction(rr)%numCartesianOrbital
!                                      do ll = 1, contraction(ss)%numCartesianOrbital
!                                         m = m + 1
!                                         auxIntegrals(m) = auxIntegrals(m) &
!                                              * primitive(aa,pi)%normalizationConstant(ii) &
!                                              * primitive(bb,pj)%normalizationConstant(jj) &
!                                              * primitive(rr,pk)%normalizationConstant(kk) &
!                                              * primitive(ss,pl)%normalizationConstant(ll)
!                                      end do
!                                   end do
!                                end do
!                             end do !! done by cartesian of contractions


!                             auxIntegrals(1:arraySize) = auxIntegrals(1:arraySize) &
!                                  * contraction(aa)%contractionCoefficients(pi) &
!                                  * contraction(bb)%contractionCoefficients(pj) &
!                                  * contraction(rr)%contractionCoefficients(pk) &
!                                  * contraction(ss)%contractionCoefficients(pl)

!                             integralsValue(1:arraySize) = integralsValue(1:arraySize) + auxIntegrals(1:arraySize)

!                          end do
!                       end do
!                    end do
!                 end do !!done by contractios

!                 !!normalize by contraction
!                 m = 0
!                 do i = 1,  contraction(aa)%numCartesianOrbital
!                    do j = 1,  contraction(bb)%numCartesianOrbital
!                       do k = 1,  contraction(rr)%numCartesianOrbital
!                          do l = 1, contraction(ss)%numCartesianOrbital
!                             m = m + 1
!                             integralsValue(m) = integralsValue(m) &
!                                  * contraction(aa)%normalizationConstant(i) &
!                                  * contraction(bb)%normalizationConstant(j) &
!                                  * contraction(rr)%normalizationConstant(k) &
!                                  * contraction(ss)%normalizationConstant(l)
!                             !! index not permuted
!                             pa=labelsOfContractions(a)+poi-1
!                             pb=labelsOfContractions(b)+poj-1
!                             pr=labelsOfContractions(r)+pok-1
!                             ps=labelsOfContractions(s)+pol-1

!                             apa=pa
!                             apb=pb
!                             apr=pr
!                             aps=ps

!                             if( pa <= pb .and. pr <= ps .and. (pa*1000)+pb >= (pr*1000)+ps) then

!                                aux = pa
!                                pa = pr
!                                pr = aux

!                                aux = pb
!                                pb = ps
!                                ps = aux

!                             end if

!                             if( ( pa <= pb .and. pr <= ps ) .and. abs(integralsValue(m)) > 1.0D-10) then

!                                if ( (apa /= pa .or. apb/=pb .or. apr/=pr .or. aps/=ps) .and. &
!                                     ( b==s ) ) then
!                                   go to 50

!                                else
!                                   auxCounter = auxCounter + 1

!                                   coulomb = densityMatrix%values(pr,ps)*integralsValue(m)

!                                   !!*****************************************************************************
!                                   !! 	Adiciona aportes debidos al operador de coulomb
!                                   !!*****
!                                   if( pa == pr .and. pb == ps ) then

!                                      twoParticlesMatrix%values(pa,pb) = &
!                                           twoParticlesMatrix%values(pa,pb) + coulomb

!                                      if( pr /= ps ) then

!                                         twoParticlesMatrix%values(pa,pb) = &
!                                              twoParticlesMatrix%values(pa,pb) + coulomb

!                                      end if

!                                   else

!                                      twoParticlesMatrix%values(pa,pb) = &
!                                           twoParticlesMatrix%values(pa,pb) + coulomb

!                                      if( pr /= ps ) then

!                                         twoParticlesMatrix%values(pa,pb) = &
!                                              twoParticlesMatrix%values(pa,pb) + coulomb

!                                      end if

!                                      coulomb = densityMatrix%values(pa,pb)*integralsValue(m)

!                                      twoParticlesMatrix%values(pr,ps) = &
!                                           twoParticlesMatrix%values(pr,ps) + coulomb

!                                      if ( pa /= pb ) then

!                                         twoParticlesMatrix%values( pr, ps ) = &
!                                              twoParticlesMatrix%values( pr, ps ) + coulomb

!                                      end if

!                                   end if

!                                   !!
!                                   !!*****************************************************************************

!                                   !!*****************************************************************************
!                                   !! 	Adicionaa aportess debidoss al operadorr de intercambio
!                                   !!*****
!                                   if( pr /= ps ) then

!                                      exchange =densityMatrix%values(pb,ps)*integralsValue(m)* factor

!                                      twoParticlesMatrix%values( pa, pr ) = &
!                                           twoParticlesMatrix%values( pa, pr ) + exchange

!                                      if( pa == pr .and. pb /= ps ) then

!                                         twoParticlesMatrix%values( pa, pr ) = &
!                                              twoParticlesMatrix%values( pa, pr ) + exchange

!                                      end if

!                                   end if

!                                   if ( pa /= pb ) then

!                                      exchange = densityMatrix%values(pa,pr)*integralsValue(m) * factor

!                                      if( pb > ps ) then

!                                         twoParticlesMatrix%values( ps, pb ) = &
!                                              twoParticlesMatrix%values( ps, pb) + exchange

!                                      else

!                                         twoParticlesMatrix%values( pb, ps ) = &
!                                              twoParticlesMatrix%values( pb, ps ) + exchange

!                                         if( pb==ps .and. pa /= pr ) then

!                                            twoParticlesMatrix%values( pb, ps ) = &
!                                                 twoParticlesMatrix%values( pb, ps ) + exchange

!                                         end if

!                                      end if

!                                      if ( pr /= ps ) then

!                                         exchange=densityMatrix%values(pa,ps)*integralsValue(m) * factor

!                                         if( pb <= pr ) then

!                                            twoParticlesMatrix%values( pb, pr ) = &
!                                                 twoParticlesMatrix%values( pb, pr ) + exchange

!                                            if( pb == pr ) then

!                                               twoParticlesMatrix%values( pb, pr ) = &
!                                                    twoParticlesMatrix%values( pb, pr ) + exchange

!                                            end if

!                                         else

!                                            twoParticlesMatrix%values( pr, pb ) = &
!                                                 twoParticlesMatrix%values( pr, pb) + exchange

!                                            if( pa == pr .and. ps == pb ) goto 30

!                                         end if

!                                      end if

!                                   end if


!                                   exchange = densityMatrix%values(pb,pr)*integralsValue(m) * factor

!                                   twoParticlesMatrix%values( pa, ps ) = &
!                                        twoParticlesMatrix%values( pa, ps ) + exchange

! 30                                continue

!                                   !!
!                                   !!*****************************************************************************

!                                end if
!                             end if

! 50                          continue 
!                          end do
!                       end do
!                    end do
!                 end do
!              end do
!              u=r+1
!           end do
!        end do
!     end do !! done by basis set

!     call cpu_time(endTime)
!     write(6,"(A,I10,A,F12.4,A)") "*****Time for ", auxCounter, "   "//trim(nameOfSpecie)//" non-zero integrals ", (endTime - startTime), "(s)"

!   end subroutine LibintInterface2_directIntraSpecie


!   !<
!   !! @brief calculate eris using libint library for all basis set (intra-specie)
!   !>
!   subroutine LibintInterface2_F12diskIntraSpecie(nameOfSpecie, specieID, job)
!     implicit none

!     character(*), intent(in) :: nameOfSpecie
!     character(*), intent(in) :: job
!     integer, intent(in) :: specieID

!     integer :: numberOfContractions
!     integer :: totalNumberOfContractions
!     integer :: numberOfPrimitives
!     integer :: maxAngularMoment
!     integer :: sumAngularMoment
!     integer :: arraySize !! number of cartesian orbitals for maxAngularMoment
!     integer :: n,u,m !! auxiliary itetators
!     integer :: aa, bb, rr, ss !! permuted iterators (LIBINT)
!     integer :: a, b, r, s !! not permuted iterators (original)
!     integer :: pa, pb, pr, ps !! labels index
!     integer :: apa, apb, apr, aps !! labels index

!     integer :: ii, jj, kk, ll !! cartesian iterators for primitives and contractions
!     integer :: aux, order !!auxiliary index
!     integer :: arraySsize(1)
!     integer :: sizeTotal !!For Large systems
!     integer :: auxIndex
!     integer :: auxIndexB

!     integer :: counter, auxCounter

!     integer,target :: i, j, k, l !! contraction length iterators
!     integer,pointer :: pi, pj, pk, pl !! pointer to contraction length iterators
!     integer,pointer :: poi, poj, pok, pol !! pointer to contraction length iterators
!     integer, allocatable :: labelsOfContractions(:) !! cartesian position of contractions in all basis set

!     integer*1, allocatable :: bufferA(:) !! avoid rep integrals
!     integer :: contador 

!     integer :: potID, potSize, potLength

!     real(8), dimension(:), pointer :: integralsPtr !! pointer to C array of integrals
!     real(8), dimension(:), pointer :: temporalPtr

!     real(8), allocatable :: auxIntegrals(:) !!array with permuted integrals aux!
!     real(8), allocatable :: auxIntegralsValue (:) !!array with permuted integrals auxiliary
!     real(8), allocatable :: integralsValue (:) !!array with permuted integrals
!     real(8), allocatable :: incompletGamma(:) !!array with incomplete gamma integrals

!     real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3) !!geometric values that appear in gaussian product
!     real(8) :: zeta, eta, rho !! exponents... that appear in gaussian product
!     real(8) :: s1234, s12, s34, AB2, CD2, PQ2 !!geometric quantities that appear in gaussian product
!     real(8) :: incompletGammaArgument
!     real(8) :: k1234

!     real(8) :: startTime, endTime

!     type(libint2), target :: primitiveQuartet !!Primquartet object needed by LIBINT
!     type(c_ptr) :: resultPc !! array of integrals from C (LIBINT)

!     procedure(LibintInterface_buildLibInt), pointer :: pBuild !!procedure to calculate eris on LIBINT


!     call cpu_time(startTime)

!     !! open file for integrals
!     open( UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//".ints", STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='Unformatted')

!     !! Get number of shells and cartesian contractions
!     numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
!     totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

!     !! Libint constructor (solo una vez)
!     maxAngularMoment = ParticleManager_getMaxAngularMoment()
!     numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

!     if( .not. LibintInterface2_isInstanced() ) then
!        call LibintInterface2_constructor( maxAngularMoment, numberOfPrimitives, trim(job))
!        call LibintInterface2_show()
!     end if

!     !! allocating space for integrals just one time (do not put it inside do loop!!!)
!     arraySize = ((maxAngularMoment + 1)*(maxAngularMoment + 2))/2

!     sizeTotal = (totalNumberOfContractions *(totalNumberOfContractions + 1 ))/2
!     sizeTotal = (sizeTotal *(sizeTotal + 1))/2

!     !! Get contractions labels for integrals index
!     if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
!     allocate(labelsOfContractions(numberOfContractions))

!     !!Real labels for contractions
!     aux = 1
!     do i = 1, numberOfContractions
!        labelsOfContractions(i) = aux
!        aux = aux + contraction(i)%numCartesianOrbital
!     end do

!     contador = 0

!     if(allocated(incompletGamma)) deallocate(incompletGamma)
!     if(allocated(auxIntegrals)) deallocate(auxIntegrals)
!     if(allocated(integralsValue)) deallocate(integralsValue)
!     if(allocated(auxIntegralsValue)) deallocate(integralsValue)
!     if(allocated(bufferA)) deallocate(bufferA)

!     allocate(auxIntegrals(arraySize* arraySize* arraySize * arraySize), &
!          integralsValue(arraySize* arraySize* arraySize* arraySize), &
!          auxIntegralsValue(arraySize* arraySize* arraySize* arraySize), &
!          incompletGamma(0:MaxAngularMoment*4), &
!          bufferA(sizeTotal))

!     bufferA = 0
!     counter = 0
!     auxCounter = 0

!     do i=1,size(InterPotential_Manager_instance%interPots)
!        if( trim(InterPotential_Manager_instance%interPots(i)%specie)==trim(nameOfSpecie) .and. &
!             trim(InterPotential_Manager_instance%interPots(i)%otherSpecie)==trim(nameOfSpecie)) then
!           potID=i
!           exit
!        end if
!     end do

!     !!Start Calculating integrals for each shell
!     do a = 1, numberOfContractions
!        n = a
!        do b = a, numberOfContractions
!           u = b
!           do r = n , numberOfContractions
!              do s = u,  numberOfContractions
!                 !!Calcula el momento angular total
!                 sumAngularMoment =  contraction(a)%angularMoment + &
!                      contraction(b)%angularMoment + &
!                      contraction(r)%angularMoment + &
!                      contraction(s)%angularMoment

!                 !! Calcula el tamano del arreglo de integrales para la capa (ab|rs)
!                 arraySize = contraction(a)%numCartesianOrbital * &
!                      contraction(b)%numCartesianOrbital * &
!                      contraction(r)%numCartesianOrbital * &
!                      contraction(s)%numCartesianOrbital

!                 !! For (ab|rs)  ---> RESTRICTION a>b && r>s && r+s > a+b
!                 aux = 0
!                 order = 0

!                 !! permuted index
!                 aa = a
!                 bb = b
!                 rr = r
!                 ss = s

!                 !!pointer to permuted index under a not permuted loop
!                 pi => i
!                 pj => j
!                 pk => k
!                 pl => l

!                 !!pointer to not permuted index under a permuted loop
!                 poi => i
!                 poj => j
!                 pok => k
!                 pol => l

!                 if (contraction(a)%angularMoment < contraction(b)%angularMoment) then

!                    aa = b
!                    bb = a

!                    pi => j
!                    pj => i

!                    poi => j
!                    poj => i

!                    order = order + 1

!                 end if

!                 if (contraction(r)%angularMoment < contraction(s)%angularMoment) then

!                    rr = s
!                    ss = r

!                    pk => l
!                    pl => k

!                    pok => l
!                    pol => k

!                    order = order + 3

!                 end if

!                 if((contraction(a)%angularMoment + contraction(b)%angularMoment) > &
!                      (contraction(r)%angularMoment + contraction(s)%angularMoment)) then

!                    aux = aa
!                    aa = rr
!                    rr = aux

!                    aux = bb
!                    bb = ss
!                    ss = aux

!                    select case(order)
!                    case(0)
!                       pi => k
!                       pj => l
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => i
!                       pol => j

!                    case(1)
!                       pi => k
!                       pj => l
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => i
!                       pol => j

!                    case(3)
!                       pi => l
!                       pj => k
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => j
!                       pol => i

!                    case(4)
!                       pi => l
!                       pj => k
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => j
!                       pol => i

!                    end select

!                 end if

!                 !!************************************
!                 !! Calculate iteratively primitives
!                 !!

!                 !!Distancias AB, CD
!                 AB = contraction(aa)%origin - contraction(bb)%origin
!                 CD = contraction(rr)%origin - contraction(ss)%origin

!                 AB2 = dot_product(AB, AB)
!                 CD2 = dot_product(CD, CD)

!                 !! Asigna valores a la estrucutra Libint
!                 LibintInterface2_instance%libint%AB = AB
!                 LibintInterface2_instance%libint%CD = CD

!                 !!start :)                                
!                 integralsValue(1:arraySize) = 0.0_8

!                 do potSize=1, size(InterPotential_Manager_instance%interPots(potID)%gaussianComponents)

!                    auxIntegralsValue(1:arraySize) = 0.0_8

!                    do potLength = 1, InterPotential_Manager_instance%interPots(potID)%gaussianComponents(potSize)%length

!                       do l = 1, contraction(s)%length
!                          do k = 1, contraction(r)%length
!                             do j = 1, contraction(b)%length
!                                do i = 1, contraction(a)%length

!                                   !!LIBINT2 PRIMQUARTET
!                                   call LibintInterface_buildlibint2(&
!                                        primitive(aa,pi), &
!                                        primitive(bb,pj), &
!                                        primitive(rr,pk), &
!                                        primitive(ss,pl), &
!                                        InterPotential_Manager_instance%interPots(potID)%gaussianComponents(potSize)%primitives(potLength))

! 				  primitiveQuartet = LibintInterface_instance%libintG12

!                                   if(arraySize == 1) then

!                                      auxIntegrals(1) = primitiveQuartet%LIBINT_T_SS_K0G12_SS_0

!                                   else

!                                      arraySsize(1) = arraySize

!                                      allocate(temporalPtr(arraySize))
!                                      temporalPtr = 0.0_8

!                                      call LibintInterface_Wrapper_buildG12(contraction(ss)%angularMoment , &
!                                           contraction(rr)%angularMoment , &
!                                           contraction(bb)%angularMoment , &
!                                           contraction(aa)%angularMoment , &
!                                           maxAngularMoment, &
!                                           arraySize, primitiveQuartet, temporalPtr)

!                                      integralsPtr => temporalPtr

!                                      auxIntegrals(1:arraySize) = integralsPtr(1:arraySize) !!it is to slow with pointer...! so.. copy

!                                   end if !!done by primitives

!                                   !!Normalize by primitives
!                                   m = 0
!                                   do ii = 1, contraction(aa)%numCartesianOrbital
!                                      do jj = 1, contraction(bb)%numCartesianOrbital
!                                         do kk = 1, contraction(rr)%numCartesianOrbital
!                                            do ll = 1, contraction(ss)%numCartesianOrbital
!                                               m = m + 1
!                                               auxIntegrals(m) = auxIntegrals(m) &
!                                                    * primitive(aa,pi)%normalizationConstant(ii) &
!                                                    * primitive(bb,pj)%normalizationConstant(jj) &
!                                                    * primitive(rr,pk)%normalizationConstant(kk) &
!                                                    * primitive(ss,pl)%normalizationConstant(ll) 
!                                            end do !ii
!                                         end do!jj
!                                      end do!kk
!                                   end do !!ll 


!                                   auxIntegrals(1:arraySize) = auxIntegrals(1:arraySize) &
!                                        * contraction(aa)%contractionCoefficients(pi) &
!                                        * contraction(bb)%contractionCoefficients(pj) &
!                                        * contraction(rr)%contractionCoefficients(pk) &
!                                        * contraction(ss)%contractionCoefficients(pl) &
!                                        * InterPotential_Manager_instance%interPots(potID)%gaussianComponents(potSize)%contractionCoefficients(potLength)

!                                   auxIntegralsValue(1:arraySize) = auxIntegralsValue(1:arraySize) + auxIntegrals(1:arraySize)

!                                end do !l
!                             end do !k
!                          end do !j
!                       end do !i   !! done by contractios

!                    end do !! G12 lenght

!                    !!normalize by contraction
!                    m = 0
!                    do ii = 1,  contraction(aa)%numCartesianOrbital
!                       do jj = 1,  contraction(bb)%numCartesianOrbital
!                          do kk = 1,  contraction(rr)%numCartesianOrbital
!                             do ll = 1, contraction(ss)%numCartesianOrbital
!                                m = m + 1

!                                auxIntegralsValue(m) = auxIntegralsValue(m) &
!                                     * contraction(aa)%normalizationConstant(ii) &
!                                     * contraction(bb)%normalizationConstant(jj) &
!                                     * contraction(rr)%normalizationConstant(kk) &
!                                     * contraction(ss)%normalizationConstant(ll)
!                             end do
!                          end do
!                       end do
!                    end do !! done by cartesian of contractions

!                    integralsValue(1:arraySize) = integralsValue(1:arraySize) + auxIntegralsValue(1:arraySize)

! 		end do!! done for potential

!   !!write to disk
!                 m = 0
!                 do i = 1, contraction(aa)%numCartesianOrbital
!                    do j = 1, contraction(bb)%numCartesianOrbital
!                       do k = 1, contraction(rr)%numCartesianOrbital
!                          do l = 1, contraction(ss)%numCartesianOrbital

!                             m = m + 1


!                             !! index not permuted
!                             pa=labelsOfContractions(a)+poi-1
!                             pb=labelsOfContractions(b)+poj-1
!                             pr=labelsOfContractions(r)+pok-1
!                             ps=labelsOfContractions(s)+pol-1

!                             apa=pa
!                             apb=pb
!                             apr=pr
!                             aps=ps

!                             if( pa <= pb .and. pr <= ps .and. (pa*1000)+pb >= (pr*1000)+ps) then

!                                aux = pa
!                                pa = pr
!                                pr = aux

!                                aux = pb
!                                pb = ps
!                                ps = aux

!                             end if

!                             if( pa <= pb .and. pr <= ps ) then

!                                !auxIndexB=auxIndex
!                                !auxIndex = IndexMap_tensorR4ToVector(pa,pb,pr,ps, totalNumberOfContractions ) !!Esto es lento (cambiar)!!!!!!!!

!                                if (  (apa /= pa .or. apb/=pb .or. apr/=pr .or. aps/=ps) .and. ( b==s ) ) then

!                                   !((apa==apb .and. aps-apr<=1) .or. ( apr==aps .and. apb-apa<=1) .or. &
!                                   !(apa==apr ) .or. (apa==aps) .or. (apb==aps .and. apa-apr<=2) .or. (b==s)) )then
!                                   !if  ( auxIndex <= auxIndexB .and. b==s) then
!                                   !print *, "    ========="
!                                   !print *, "    a,b,r,s,aI", pa,pb,pr,ps,auxIndex
!                                   !print *, "    a,b,r,s,a2", apa,apb,apr,aps,aux

!                                   !contador=contador+1

!                                else 

!                                   !if(bufferA(auxIndex) == 0) then
!                                   !bufferA(auxIndex) = 1

!                                   if(abs(integralsValue(m)) > 1.0D-10) then

!                                      counter = counter + 1
!                                      auxCounter = auxCounter + 1

!                                      eris%a(counter) = pa
!                                      eris%b(counter) = pb
!                                      eris%c(counter) = pr
!                                      eris%d(counter) = ps
!                                      eris%integrals(counter) = integralsValue(m)

!                                   end if

!                                   if( counter == INTEGRAL_STACK_SIZE ) then

!                                      write(34) &
!                                           eris%a(1:INTEGRAL_STACK_SIZE), &
!                                           eris%b(1:INTEGRAL_STACK_SIZE), &
!                                           eris%c(1:INTEGRAL_STACK_SIZE), &
!                                           eris%d(1:INTEGRAL_STACK_SIZE), &
!                                           eris%integrals(1:INTEGRAL_STACK_SIZE)
!                                      counter = 0

!                                   end if

!                                   !else if (bufferA(auxIndex) == 1 ) then

!                                   !print *, "XXXXXXXXXXX"
!                                   !print *, "pa,b,r,s,aI", pa,pb,pr,ps,auxIndex
!                                   !print *, "apa,b,r,s,a", apa,apb,apr,aps,auxIndex
!                                   !print *, "a,b,r,s    ",a,b,r,s

!                                   !if ( b==s) then
!                                   !   print *, "||||||||||||||||"
!                                   !end if

!                                   !contador = contador +1

!                                end if
!                             end if
!                          end do
!                       end do
!                    end do
!                 end do
!              end do
!              u=r+1
!           end do
!        end do
!     end do !! done by basis set

!     eris%a(counter+1) = -1
!     eris%b(counter+1) = -1
!     eris%c(counter+1) = -1
!     eris%d(counter+1) = -1
!     eris%integrals(counter+1) = 0.0_8


!     write(34) &
!          eris%a(1:INTEGRAL_STACK_SIZE), &
!          eris%b(1:INTEGRAL_STACK_SIZE), &
!          eris%c(1:INTEGRAL_STACK_SIZE), &
!          eris%d(1:INTEGRAL_STACK_SIZE), &
!          eris%integrals(1:INTEGRAL_STACK_SIZE)

!     close(34)

!     !deallocate(bufferA)

!     call cpu_time(endTime)

!     write(6,"(A,I10,A,F12.4,A)") "*****Time for ", auxCounter, "   "//trim(nameOfSpecie)//" non-zero integrals ", (endTime - startTime), "(s)"

!   end subroutine LibintInterface2_F12diskIntraSpecie


!   !<
!   !! calculate eris using libint library for all basis set (inter-specie)
!   !>
!   subroutine LibintInterface2_F12diskInterSpecie(nameOfSpecie, otherNameOfSpecie,  specieID, otherSpecieID, job, isInterSpecies, isCouplingA, isCouplingB)
!     implicit none

!     integer,target :: specieID
!     integer,target :: otherSpecieID
!     character(*) :: nameOfSpecie
!     character(*) :: otherNameOfSpecie
!     character(*), intent(in) :: job
!     logical, optional :: isInterSpecies
!     logical, optional :: isCouplingA
!     logical, optional :: isCouplingB

!     logical :: interSpecies
!     logical :: couplingA
!     logical :: couplingB

!     integer :: totalNumberOfContractions
!     integer :: otherTotalNumberOfContractions
!     integer :: numberOfContractions
!     integer :: otherNumberOfContractions
!     integer :: numberOfPrimitives
!     integer :: maxAngularMoment
!     integer :: sumAngularMoment
!     integer :: arraySize !! number of cartesian orbitals for maxAngularMoment
!     integer :: n,u,m !! auxiliary itetators
!     integer :: aa, bb, rr, ss !! permuted iterators (LIBINT)
!     integer :: a, b, r, s !! not permuted iterators (original)
!     integer*2 :: pa, pb, pr, ps !! labels index
!     integer :: ii, jj, kk, ll !! cartesian iterators for primitives and contractions
!     integer :: aux, order !!auxiliary index
!     integer :: arraySsize(1)
!     integer :: sizeTotal
!     integer :: auxIndex
!     integer :: counter, auxCounter

!     integer :: potID, potLength, potSize

!     integer,target :: i, j, k, l !! contraction length iterators
!     integer,pointer :: pi, pj, pk, pl !! pointer to contraction length iterators
!     integer,pointer :: poi, poj, pok, pol !! pointer to contraction length iterators
!     integer, pointer :: pSpecieID, pOtherSpecieID !! pointer to species ID
!     integer, allocatable :: labelsOfContractions(:) !! cartesian position of contractions in all basis set
!     integer, allocatable :: otherLabelsOfContractions(:) !! cartesian position of contractions in all basis set
!     !integer, allocatable :: buffer(:) !! avoid rep integrals
!     !integer*1, allocatable :: buffer(:) !! avoid rep integrals

!     real(8), dimension(:), pointer :: integralsPtr !! pointer to C array of integrals
!     real(8), dimension(:), pointer :: temporalPtr

!     real(8), allocatable :: auxIntegrals(:) !!array with permuted integrals aux!
!     real(8), allocatable :: auxIntegralsValue (:) !!array with permuted integrals
!     real(8), allocatable :: integralsValue (:) !!array with permuted integrals
!     real(8), allocatable :: incompletGamma(:) !!array with incomplete gamma integrals

!     real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3) !!geometric values that appear in gaussian product
!     real(8) :: zeta, eta, rho !! exponents... that appear in gaussian product
!     real(8) :: s1234, s12, s34, AB2, CD2, PQ2 !!geometric quantities that appear in gaussian product
!     real(8) :: incompletGammaArgument
!     real(8) :: k1234

!     real(8) :: startTime, endTime

!     type(libint2), target :: primitiveQuartet !!Primquartet object needed by LIBINT
!     type(c_ptr) :: resultPc !! array of integrals from C (LIBINT)

!     procedure(LibintInterface_buildLibInt), pointer :: pBuild !!procedure to calculate eris on LIBINT

!     call cpu_time(startTime)

!     interSpecies = .true.
!     couplingA = .false.
!     couplingB = .false.

!     if(present(isInterSpecies)) interSpecies = isInterSpecies
!     if(present(isCouplingA)) couplingA = isCouplingA
!     if(present(isCouplingB)) couplingB = isCouplingB

!     !! open file for integrals
!     open(UNIT=34,FILE=trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//"."//trim(otherNameOfSpecie)//".ints", &
!          STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='Unformatted')

!     !! Get number of shells and cartesian contractions
!     numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
!     totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( specieID )

!     !! Get number of shells and cartesian contractions (other specie)
!     otherNumberOfContractions = ParticleManager_getNumberOfContractions( otherSpecieID )
!     otherTotalNumberOfContractions = ParticleManager_getTotalNumberOfContractions( otherSpecieID )

!     !! Libint constructor (solo una vez)
!     maxAngularMoment = ParticleManager_getMaxAngularMoment()
!     numberOfPrimitives = ParticleManager_getTotalNumberOfPrimitives()

!     if( .not. LibintInterface2_isInstanced() ) then
!        call LibintInterface2_constructor( maxAngularMoment, numberOfPrimitives, trim(job))
!        call LibintInterface2_show()
!     end if

!     !! allocating space for integrals just one time (do not put it inside do loop!!!)
!     arraySize = ((maxAngularMoment + 1)*(maxAngularMoment + 2))/2

!     sizeTotal = ((totalNumberOfContractions *(totalNumberOfContractions + 1 ))/2) * ((otherTotalNumberOfContractions *(otherTotalNumberOfContractions + 1 ))/2)

!     !! Get contractions labels for integrals index
!     if (allocated(labelsOfContractions)) deallocate(labelsOfContractions)
!     allocate(labelsOfContractions(numberOfContractions))

!     !!Real labels for contractions
!     aux = 1
!     do i = 1, numberOfContractions
!        labelsOfContractions(i) = aux
!        aux = aux + contraction(i)%numCartesianOrbital
!     end do

!     !! Get contractions labels for integrals index (other specie)
!     if (allocated(otherLabelsOfContractions)) deallocate(otherLabelsOfContractions)
!     allocate(otherLabelsOfContractions(otherNumberOfContractions))

!     !!Real labels for contractions (other specie)
!     aux = 1
!     do i = 1, otherNumberOfContractions
!        otherLabelsOfContractions(i) = aux
!        aux = aux + otherContraction(i,otherSpecieID)%numCartesianOrbital
!     end do

!     !! Allocating some space
!     if(allocated(incompletGamma)) deallocate(incompletGamma)
!     if(allocated(auxIntegrals)) deallocate(auxIntegrals)
!     if(allocated(integralsValue)) deallocate(integralsValue)
!     if(allocated(auxIntegralsValue)) deallocate(integralsValue)
!     !if(allocated(buffer)) deallocate(buffer)

!     allocate(auxIntegrals(arraySize* arraySize* arraySize * arraySize), &
!          integralsValue(arraySize* arraySize* arraySize* arraySize), &
!          auxIntegralsValue(arraySize* arraySize* arraySize* arraySize), &
!          incompletGamma(0:MaxAngularMoment*4))    
!     !buffer(sizeTotal))

!     !buffer = 0
!     counter = 0
!     auxCounter = 0

!     eris%a(1:INTEGRAL_STACK_SIZE)=1
!     eris%b(1:INTEGRAL_STACK_SIZE)=1
!     eris%c(1:INTEGRAL_STACK_SIZE)=1
!     eris%d(1:INTEGRAL_STACK_SIZE)=1
!     eris%integrals(1:INTEGRAL_STACK_SIZE)=1.0_8

!     !!Start Calculating integrals for each shell
!     do a = 1, numberOfContractions
!        do b = a, numberOfContractions
!           do r = 1 , otherNumberOfContractions
!              do s = r,  otherNumberOfContractions

!                 !!Calcula el momento angular total
!                 sumAngularMoment =  contraction(a)%angularMoment + &
!                      contraction(b)%angularMoment + &
!                      otherContraction(r,otherSpecieID)%angularMoment + &
!                      otherContraction(s,otherSpecieID)%angularMoment

!                 !! Calcula el tamano del arreglo de integrales para la capa (ab|rs)
!                 arraySize = contraction(a)%numCartesianOrbital * &
!                      contraction(b)%numCartesianOrbital * &
!                      otherContraction(r,otherSpecieID)%numCartesianOrbital * &
!                      otherContraction(s,otherSpecieID)%numCartesianOrbital

!                 !! For (ab|rs)  ---> RESTRICTION a>b && r>s && r+s > a+b
!                 aux = 0
!                 order = 0

!                 !! permuted index
!                 aa = a
!                 bb = b
!                 rr = r
!                 ss = s

!                 !!pointer to permuted index under a not permuted loop
!                 pi => i
!                 pj => j
!                 pk => k
!                 pl => l

!                 !!pointer to not permuted index under a permuted loop
!                 poi => i
!                 poj => j
!                 pok => k
!                 pol => l

!                 !!Pointer to specie ID
!                 pSpecieID => specieID
!                 pOtherSpecieID => otherSpecieID

!                 if (contraction(a)%angularMoment < contraction(b)%angularMoment) then

!                    aa = b
!                    bb = a

!                    pi => j
!                    pj => i

!                    poi => j
!                    poj => i

!                    order = order + 1
!                 end if

!                 if (otherContraction(r,otherSpecieID)%angularMoment < otherContraction(s,otherSpecieID)%angularMoment) then

!                    rr = s
!                    ss = r

!                    pk => l
!                    pl => k

!                    pok => l
!                    pol => k

!                    order = order + 3

!                 end if

!                 if((contraction(a)%angularMoment + contraction(b)%angularMoment) > &
!                      (otherContraction(r,otherSpecieID)%angularMoment + otherContraction(s,otherSpecieID)%angularMoment)) then

!                    aux = aa
!                    aa = rr
!                    rr = aux

!                    aux = bb
!                    bb = ss
!                    ss = aux

!                    pSpecieID => otherSpecieID
!                    pOtherSpecieID => specieID

!                    select case(order)
!                    case(0)
!                       pi => k
!                       pj => l
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => i
!                       pol => j

!                    case(1)
!                       pi => k
!                       pj => l
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => i
!                       pol => j

!                    case(3)
!                       pi => l
!                       pj => k
!                       pk => i
!                       pl => j

!                       poi => k
!                       poj => l
!                       pok => j
!                       pol => i

!                    case(4)
!                       pi => l
!                       pj => k
!                       pk => j
!                       pl => i

!                       poi => l
!                       poj => k
!                       pok => j
!                       pol => i

!                    end select

!                    order = order + 5

!                 end if

!                 !!************************************
!                 !! Calculate iteratively primitives
!                 !!

!                 !!Distancias AB, CD
!                 AB = otherContraction(aa, pSpecieID)%origin - otherContraction(bb, pSpecieID)%origin
!                 CD = otherContraction(rr, pOtherSpecieID)%origin - otherContraction(ss, pOtherSpecieID)%origin

!                 AB2 = dot_product(AB, AB)
!                 CD2 = dot_product(CD, CD)

!                 !! Asigna valores a la estrucutra Libint
!                 LibintInterface2_instance%libint%AB = AB
!                 LibintInterface2_instance%libint%CD = CD

!                 !!start :)
!                 integralsValue(1:arraySize) = 0.0_8

!                 !! not-permuted loop
!                 do potSize=1, size(InterPotential_Manager_instance%interPots(potID)%gaussianComponents)

!                    auxIntegralsValue(1:arraySize) = 0.0_8

!                    do potLength = 1, InterPotential_Manager_instance%interPots(potID)%gaussianComponents(potSize)%length

!                       do l = 1, otherContraction(s,otherSpecieID)%length
!                          do k = 1, otherContraction(r,otherSpecieID)%length
!                             do j = 1, contraction(b)%length
!                                do i = 1, contraction(a)%length

!                                   !!LIBINT2 PRIMQUARTET
!                                   call LibintInterface_buildlibint2(&
!                                        otherPrimitive(aa,pi,pSpecieID), &
!                                        otherPrimitive(bb,pj,pSpecieID), &
!                                        otherPrimitive(rr,pk,pOtherSpecieID), &
!                                        otherPrimitive(ss,pl,pOtherSpecieID), &
!                                        InterPotential_Manager_instance%interPots(potID)%gaussianComponents(potSize)%primitives(potLength))

! 				  primitiveQuartet = LibintInterface_instance%libintG12

!                                   if(arraySize == 1) then

!                                      auxIntegrals(1) = primitiveQuartet%LIBINT_T_SS_K0G12_SS_0

!                                   else

!                                      arraySsize(1) = arraySize

!                                      allocate(temporalPtr(arraySize))
!                                      temporalPtr = 0.0_8

!                                      call LibintInterface_Wrapper_buildG12(&
!                                           otherContraction(ss,pOtherSpecieID)%angularMoment , &
!                                           otherContraction(rr,pOtherSpecieID)%angularMoment , &
!                                           otherContraction(bb,pSpecieID)%angularMoment , &
!                                           otherContraction(aa,pSpecieID)%angularMoment , &
!                                           maxAngularMoment, &
!                                           arraySize, primitiveQuartet, temporalPtr)

!                                      integralsPtr => temporalPtr

!                                      auxIntegrals(1:arraySize) = integralsPtr(1:arraySize) !!it is to slow with pointer...! so.. copy

!                                   end if !!done by primitives

!                                   !!Normalize by primitives
!                                   m = 0
!                                   do ii = 1, otherContraction(aa,pSpecieID)%numCartesianOrbital
!                                      do jj = 1, otherContraction(bb,pSpecieID)%numCartesianOrbital
!                                         do kk = 1, otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                                            do ll = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital
!                                               m = m + 1
!                                               auxIntegrals(m) = auxIntegrals(m) &
!                                                    * otherPrimitive(aa,pi,pSpecieID)%normalizationConstant(ii) &
!                                                    * otherPrimitive(bb,pj,pSpecieID)%normalizationConstant(jj) &
!                                                    * otherPrimitive(rr,pk,pOtherSpecieID)%normalizationConstant(kk) &
!                                                    * otherPrimitive(ss,pl,pOtherSpecieID)%normalizationConstant(ll)
!                                            end do
!                                         end do
!                                      end do
!                                   end do !! done by cartesian of contractions


!                                   auxIntegrals(1:arraySize) = auxIntegrals(1:arraySize) &
!                                        * otherContraction(aa,pSpecieID)%contractionCoefficients(pi) &
!                                        * otherContraction(bb,pSpecieID)%contractionCoefficients(pj) &
!                                        * otherContraction(rr,pOtherSpecieID)%contractionCoefficients(pk) &
!                                        * otherContraction(ss,pOtherSpecieID)%contractionCoefficients(pl)

!                                   auxIntegralsValue(1:arraySize) = auxIntegralsValue(1:arraySize) + auxIntegrals(1:arraySize)

!                                end do
!                             end do
!                          end do
!                       end do !!done by contractions

!                    end do !! done by contraction plus potential length


!                    !!normalize by contraction
!                    m = 0
!                    do ii = 1,  otherContraction(aa,pSpecieID)%numCartesianOrbital
!                       do jj = 1,  otherContraction(bb,pSpecieID)%numCartesianOrbital
!                          do kk = 1,  otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                             do ll = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital
!                                m = m + 1

!                                auxIntegralsValue(m) = auxIntegralsValue(m) &
!                                     * otherContraction(aa,pSpecieID)%normalizationConstant(ii) &
!                                     * otherContraction(bb,pSpecieID)%normalizationConstant(jj) &
!                                     * otherContraction(rr,pOtherSpecieID)%normalizationConstant(kk) &
!                                     * otherContraction(ss,pOtherSpecieID)%normalizationConstant(ll)
!                             end do
!                          end do
!                       end do
!                    end do !! done by cartesian of contractions

!                    integralsValue(1:arraySize) = integralsValue(1:arraySize) + auxIntegralsValue(1:arraySize)		  

! 		end do!! done for potential

!   !!write to disk
!                 m = 0
!                 do i = 1, otherContraction(aa,pSpecieID)%numCartesianOrbital
!                    do j = 1, otherContraction(bb,pSpecieID)%numCartesianOrbital
!                       do k = 1, otherContraction(rr,pOtherSpecieID)%numCartesianOrbital
!                          do l = 1, otherContraction(ss,pOtherSpecieID)%numCartesianOrbital

!                             m = m + 1

!                             !! index not permuted
!                             pa=labelsOfContractions(a)+poi-1
!                             pb=labelsOfContractions(b)+poj-1
!                             pr=otherLabelsOfContractions(r)+pok-1
!                             ps=otherLabelsOfContractions(s)+pol-1

!                             if( pa <= pb .and. pr <= ps ) then

!                                !if(interSpecies) then

!                                !   auxIndex = IndexMap_tensorR4ToVector22(pa,pb,pr,ps, totalNumberOfContractions, otherTotalNumberOfContractions )

!                                !else if(couplingA) then

!                                !auxIndex = IndexMap_tensorR4ToVector33(pa,pb,pr,ps, totalNumberOfContractions, otherTotalNumberOfContractions )

!                                !else if(couplingB) then

!                                !   auxIndex = IndexMap_tensorR4ToVector33(pr,ps,pa,pb, otherTotalNumberOfContractions, totalNumberOfContractions )

!                                !end if

!                                !if(buffer(auxIndex) == 0) then

!                                !buffer(auxIndex) = 1

!                                if(abs(integralsValue(m)) > 1.0D-10) then

!                                   counter = counter + 1
!                                   auxCounter = auxCounter + 1

!                                   eris%a(counter) = pa
!                                   eris%b(counter) = pb
!                                   eris%c(counter) = pr
!                                   eris%d(counter) = ps
!                                   eris%integrals(counter) = integralsValue(m)

!                                end if


!                                if( counter == INTEGRAL_STACK_SIZE ) then

!                                   write(34) eris%a(1:INTEGRAL_STACK_SIZE), eris%b(1:INTEGRAL_STACK_SIZE), &
!                                        eris%c(1:INTEGRAL_STACK_SIZE), eris%d(1:INTEGRAL_STACK_SIZE), &
!                                        eris%integrals(1:INTEGRAL_STACK_SIZE)
!                                   counter = 0

!                                end if

!                             end if
!                          end do
!                       end do
!                    end do
!                 end do
!              end do
!              u=r+1
!           end do
!        end do
!     end do !! done by basis set

!     eris%a(counter+1) = -1
!     eris%b(counter+1) = -1
!     eris%c(counter+1) = -1
!     eris%d(counter+1) = -1
!     eris%integrals(counter+1) = 0.0_8	

!     !print *, "pa,pb,pr,ps ", pa,pb,pr,ps

!     write(34) eris%a(1:INTEGRAL_STACK_SIZE), eris%b(1:INTEGRAL_STACK_SIZE), eris%c(1:INTEGRAL_STACK_SIZE), &
!          eris%d(1:INTEGRAL_STACK_SIZE), eris%integrals(1:INTEGRAL_STACK_SIZE)

!     close(34)

!     !deallocate(buffer)

!     call cpu_time(endTime)

!     write(6,"(A,I10,A,F12.2,A)") "*****Time for ", auxCounter, "   "//trim(nameOfSpecie)//"/"//trim(otherNameOfSpecie)//" non-zero integrals ", (endTime - startTime), "(s)"

!   end subroutine LibintInterface2_F12diskInterSpecie

  !!>
  !! @brief Indica si el objeto ha sido instanciado o no
  !!
  !<
  function LibintInterface2_isInstanced( ) result( output )
    implicit  none

    logical :: output

    output = LibintInterface2_instance%isInstanced

  end function LibintInterface2_isInstanced


  !>
  !! @brief  Maneja excepciones de la clase
  !<
  subroutine LibintInterface2_exception( typeMessage, description, debugDescription)
    implicit none

    integer :: typeMessage
    character(*) :: description
    character(*) :: debugDescription

    type(Exception) :: ex

    call Exception_constructor( ex , typeMessage )
    call Exception_setDebugDescription( ex, debugDescription )
    call Exception_setDescription( ex, description )
    call Exception_show( ex )
    call Exception_destructor( ex )

  end subroutine LibintInterface2_exception

end module LibintInterface2_
