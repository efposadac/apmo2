!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Manico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para calculo de integrales de overlap 
! 
! Este modulo contiene los algoritmos necesarios para la evaluacian de integrales
! de overlap entre pares de funciones gaussianas primitivas (PrimitiveGaussian_),
! sin normalizar.
!
! \f[ (\bf{a} \mid \bf{b}) = \int_{TE} {{\varphi(\bf{r};{\zeta}_a,
! \bf{a,A})}{\varphi(\bf{r};{\zeta}_b,\bf{b,B})}}\,dr \f]
!
! Donde:
!
! <table>
! <tr> <td> \f$ \zeta \f$ : <td> <dfn> exponente orbital. </dfn> 
! <tr> <td> <b> r </b> : <td> <dfn> coordenas espaciales de la funcian. </dfn>
! <tr> <td> <b> n </b> : <td> <dfn> indice de momento angular. </dfn>
! <tr> <td> <b> R </b> : <td> <dfn> origen de la funcion gaussiana cartesiana. </dfn>
! </table>
!
! La integral de traslapamiento entre funciones gaussinas primitivas se calcula 
! con el matodo recursivo propuesto por Obara-Sayka, el cual transforma las 
! gaussiana de entrada en una sola mediante la identidad del producto gausiano.
! La expresian general para el calculos de las integrales es: 
!
! \f[ ({\bf{a + 1_i}} \parallel {\bf{b}}) = (P_i -A_i) ({\bf{a}} \parallel 
! {\bf{b}} ) + \frac{1}{2 \zeta} N_i(\bf{a}) ({\bf{a - 1_i}} \parallel 
! {\bf{b}} ) + \frac{1}{2 \zeta} N_i(\bf{b}) + ({\bf{a}} \parallel {\bf{b-1_i}})\f]
!
! Los parametros <b> P </b> y \f$ \zeta \f$ de la expresian provienen del producto de dos 
! funciones gaussianas.
!
! @author Edwin Posada
!
! <b> Fecha de creacion : </b> 2010-03-11
!
! <b> Historial de modificaciones: </b>
!
!
! @see gaussianProduct, PrimitiveGaussian_ 
!
!**
module OverlapIntegrals_
	use PrimitiveGaussian_
	use Exception_
	
	implicit none 
	
	public :: &
		PrimitiveGaussian_overlapIntegral, &
		OverlapIntegrals_compute
		
	private :: &
		OverlapIntegral_oSRecursion
		
contains


	!<
	!@brief Evalua integrales overlap para cualquier momento angular
	!@author Edwin Posada, 2010
	!@return devuelve los valores de integrales de overlap para una capa o individual
	!>

	subroutine OverlapIntegrals_compute(ami, amj, nprim1, nprim2, A, B, exp1, exp2, coef1, coef2, norm1, norm2, buffer)
	  implicit none

	  integer, intent(in) :: ami(0:3), amj(0:3)
	  integer, intent(in) :: nprim1, nprim2
	  real(8), intent(in) :: A(0:3), B(0:3)
	  real(8), intent(in) :: exp1(0:nprim1), exp2(0:nprim2)
	  real(8), intent(in) :: coef1(0:nprim1), coef2(0:nprim2)
	  real(8), intent(in) :: norm1(0:nprim1), norm2(0:nprim2)
	  real(8), intent(out):: buffer

	  real(8), allocatable ::  x(:,:), y(:,:), z(:,:)
	  real(8) :: AB2
	  real(8) :: a1, c1, d1
	  real(8) :: a2, c2, d2
	  real(8) :: gam, oog
	  real(8) :: PA(0:3), PB(0:3), P(0:3)
	  real(8) :: over_pf
	  real(8) :: x0, y0, z0

	  integer :: am1, am2
	  integer :: mmax
	  integer :: p1, p2
	  integer :: ao12
	  integer :: ii, jj, kk, ll
	  integer :: l1, m1, n1
	  integer :: l2, m2, n2

	  am1 = sum(ami)
	  am2 = sum(amj)

	  buffer = 0.0_8

	  mmax = max(am1, am2) + 1

	  allocate(x(0:mmax+2, 0:mmax+2), y(0:mmax+2, 0:mmax+2), z(0:mmax+2, 0:mmax+2))

	  x = 0.0_8
	  x = 0.0_8
	  x = 0.0_8

      !   calcular intermediarios

	  AB2 = 0.0_8
	  AB2 = AB2 + (A(0) - B(0)) * (A(0) - B(0))
	  AB2 = AB2 + (A(1) - B(1)) * (A(1) - B(1))
	  AB2 = AB2 + (A(2) - B(2)) * (A(2) - B(2))

	  do p1=0, nprim1 - 1
	      a1 = exp1(p1)
	      c1 = coef1(p1)
	      d1 = norm1(p1)
	      do p2=0, nprim2 - 1
		  a2 = exp2(p2)
		  c2 = coef2(p2)
		  d2 = norm2(p2)
		  gam = a1 + a2
		  oog = 1.0/gam

		  P(0) = (a1*A(0) + a2*B(0))*oog
		  P(1) = (a1*A(1) + a2*B(1))*oog
		  P(2) = (a1*A(2) + a2*B(2))*oog
		  PA(0) = P(0) - A(0)
		  PA(1) = P(1) - A(1)
		  PA(2) = P(2) - A(2)
		  PB(0) = P(0) - B(0)
		  PB(1) = P(1) - B(1)
		  PB(2) = P(2) - B(2)

		  over_pf = exp(-a1*a2*AB2*oog) * sqrt(Math_PI*oog) * Math_PI * oog * c1 * c2 * d1 * d2

      !           recursion
		  call OverlapIntegrals_OSrecurs(x, y, z, PA, PB, gam, am1+2, am2+2)

		  x0 = x(ami(0),amj(0))
		  y0 = y(ami(1),amj(1))
		  z0 = z(ami(2),amj(2))

		  ao12 = ao12 + 1
		  buffer = buffer + over_pf*x0*y0*z0

	      end do
	  end do

	  deallocate(x)
	  deallocate(y)
	  deallocate(z)

	end subroutine OverlapIntegrals_compute

	subroutine OverlapIntegrals_OSrecurs(x, y, z, PA, PB, gam, am1, am2)
	  implicit none

	  real(8), intent(inout), allocatable :: x(:,:), y(:,:), z(:,:)
	  real(8), intent(in) :: PA(0:3), PB(0:3)
	  real(8), intent(in) :: gam
	  integer, intent(in) :: am1, am2

	  real(8) :: pp
	  integer :: i, j, k

	  pp = 1/(2*gam)

	  x(0,0) = 1.0_8
	  y(0,0) = 1.0_8
	  z(0,0) = 1.0_8

      !     Upward recursion in j for i=0

	  x(0,1) = PB(0)
	  y(0,1) = PB(1)
	  z(0,1) = PB(2)

	  do j=1, am2 -1
	    x(0,j+1) = PB(0)*x(0,j)
	    y(0,j+1) = PB(1)*y(0,j)
	    z(0,j+1) = PB(2)*z(0,j)
	    x(0,j+1) = x(0,j+1) + j*pp*x(0,j-1)
	    y(0,j+1) = y(0,j+1) + j*pp*y(0,j-1)
	    z(0,j+1) = z(0,j+1) + j*pp*z(0,j-1)
	  end do

      !     Upward recursion in i for all j's

	  x(1,0) = PA(0)
	  y(1,0) = PA(1)
	  z(1,0) = PA(2)

	  do j=1, am2
	    x(1,j) = PA(0)*x(0,j)
	    y(1,j) = PA(1)*y(0,j)
	    z(1,j) = PA(2)*z(0,j)
	    x(1,j) = x(1,j) + j*pp*x(0,j-1)
	    y(1,j) = y(1,j) + j*pp*y(0,j-1)
	    z(1,j) = z(1,j) + j*pp*z(0,j-1)
	  end do

	  do i=1, am1 - 1
	    x(i+1,0) = PA(0)*x(i,0)
	    y(i+1,0) = PA(1)*y(i,0)
	    z(i+1,0) = PA(2)*z(i,0)
	    x(i+1,0) = x(i+1,0) + i*pp*x(i-1,0)
	    y(i+1,0) = y(i+1,0) + i*pp*y(i-1,0)
	    z(i+1,0) = z(i+1,0) + i*pp*z(i-1,0)
	    do j=1, am2
	      x(i+1,j) = PA(0)*x(i,j)
	      y(i+1,j) = PA(1)*y(i,j)
	      z(i+1,j) = PA(2)*z(i,j)
	      x(i+1,j) = x(i+1,j) + i*pp*x(i-1,j)
	      y(i+1,j) = y(i+1,j) + i*pp*y(i-1,j)
	      z(i+1,j) = z(i+1,j) + i*pp*z(i-1,j)
	      x(i+1,j) = x(i+1,j) + j*pp*x(i,j-1)
	      y(i+1,j) = y(i+1,j) + j*pp*y(i,j-1)
	      z(i+1,j) = z(i+1,j) + j*pp*z(i,j-1)
	    end do
	  end do

	end subroutine OverlapIntegrals_OSrecurs

	function PrimitiveGaussian_overlapIntegral(  primitiveA, primitiveB ) result (output)
	  	implicit none

 		type(PrimitiveGaussian) , intent(in) :: primitiveA
		type(PrimitiveGaussian) , intent(in) :: primitiveB
		real(8) :: output(primitiveA%numCartesianOrbital * primitiveB%numCartesianOrbital)

		real(8), allocatable :: orbitalIntegralsX(:,:)
		real(8), allocatable :: orbitalIntegralsY(:,:)
		real(8), allocatable :: orbitalIntegralsZ(:,:)
		real(8) :: zeta, zetaInv, overPf
		real(8) :: P(3), PA(0:3), PB(0:3)
		real(8) :: x0, y0, z0
		real(8) :: AB2
		integer(8) :: angularMomentIndexA(3, primitiveA%numcartesianOrbital)
		integer(8) :: angularMomentIndexB(3, primitiveB%numcartesianOrbital)
	 	integer :: maxAngularMomentIndex
	 	integer :: ax, ay, az
	 	integer :: bx, by, bz
	  	integer :: a, b
	  	integer :: counter

		angularMomentIndexA = PrimitiveGaussian_getAllAngularMomentIndex(primitiveA)
		angularMomentIndexB = PrimitiveGaussian_getAllAngularMomentIndex(primitiveB)

	  	maxAngularMomentIndex = max(sum(primitiveA%angularMomentIndex), sum(primitiveB%angularMomentIndex))

		if(allocated(orbitalIntegralsX))deallocate(orbitalIntegralsX)
	  	allocate(orbitalIntegralsX(0:maxAngularMomentIndex+3, 0:maxAngularMomentIndex+3))
	  	if(allocated(orbitalIntegralsY))deallocate(orbitalIntegralsY)
	  	allocate(orbitalIntegralsY(0:maxAngularMomentIndex+3, 0:maxAngularMomentIndex+3))
	  	if(allocated(orbitalIntegralsZ))deallocate(orbitalIntegralsZ)
	  	allocate(orbitalIntegralsZ(0:maxAngularMomentIndex+3, 0:maxAngularMomentIndex+3))

	  	orbitalIntegralsX = 0.0_8
	  	orbitalIntegralsY = 0.0_8
	  	orbitalIntegralsZ = 0.0_8

      	!calcular intermediarios
  		AB2 = 0.0_8
	  	AB2 = AB2 + (primitiveA%origin(1) - primitiveB%origin(1)) * (primitiveA%origin(1) - primitiveB%origin(1))
	  	AB2 = AB2 + (primitiveA%origin(2) - primitiveB%origin(2)) * (primitiveA%origin(2) - primitiveB%origin(2))
	  	AB2 = AB2 + (primitiveA%origin(3) - primitiveB%origin(3)) * (primitiveA%origin(3) - primitiveB%origin(3))

 		zeta = primitiveA%orbitalExponent + primitiveB%orbitalExponent
	  	zetaInv = 1.0_8/zeta

	  	P(1) = ( primitiveA%orbitalExponent*primitiveA%origin(1) + primitiveB%orbitalExponent *primitiveB%origin(1))*zetaInv
	  	P(2) = ( primitiveA%orbitalExponent*primitiveA%origin(2) + primitiveB%orbitalExponent *primitiveB%origin(2))*zetaInv
	  	P(3) = ( primitiveA%orbitalExponent*primitiveA%origin(3) + primitiveB%orbitalExponent *primitiveB%origin(3))*zetaInv

		PA(0) = P(1) - primitiveA%origin(1)
		PA(1) = P(2) - primitiveA%origin(2)
		PA(2) = P(3) - primitiveA%origin(3)
		PB(0) = P(1) - primitiveB%origin(1)
		PB(1) = P(2) - primitiveB%origin(2)
		PB(2) = P(3) - primitiveB%origin(3)

		overPf = exp(-primitiveA%orbitalExponent * primitiveB%orbitalExponent * AB2*zetaInv ) * &
					sqrt(Math_PI*zetaInv ) * Math_PI * zetaInv

  	  	!! recursion
	  	call OverlapIntegral_oSRecursion(orbitalIntegralsX, orbitalIntegralsY, orbitalIntegralsZ, PA, PB, zeta,&
	  									sum(primitiveB%angularMomentIndex)+2, sum(primitiveA%angularMomentIndex)+2)

  		output = 0.0_8

		if(primitiveA%isVectorized .and. primitiveB%isVectorized ) then

			ax = primitiveA%angularMomentIndex(1)
			ay = primitiveA%angularMomentIndex(2)
			az = primitiveA%angularMomentIndex(3)

			bx = primitiveB%angularMomentIndex(1)
			by = primitiveB%angularMomentIndex(2)
			bz = primitiveB%angularMomentIndex(3)

			x0 = orbitalIntegralsX(ax, bx)
		  	y0 = orbitalIntegralsY(ay, by)
		  	z0 = orbitalIntegralsZ(az, bz)

		  	output(1) = overPf*x0*y0*z0

		else if(primitiveA%isVectorized .and. .not.primitiveB%isVectorized ) then

			counter = 1
			do b = 1, primitiveB%numCartesianOrbital

				ax = primitiveA%angularMomentIndex(1)
				ay = primitiveA%angularMomentIndex(2)
				az = primitiveA%angularMomentIndex(3)

				bx = angularMomentIndexB(1, b)
				by = angularMomentIndexB(2, b)
				bz = angularMomentIndexB(3, b)

				x0 = orbitalIntegralsX(ax, bx)
			  	y0 = orbitalIntegralsY(ay, by)
			  	z0 = orbitalIntegralsZ(az, bz)

			  	output(counter) = overPf*x0*y0*z0
			  	counter = counter + 1

			end do

		else if(.not.primitiveA%isVectorized .and. primitiveB%isVectorized ) then

			counter = 1
			do a = 1, primitiveA%numCartesianOrbital

				bx = primitiveB%angularMomentIndex(1)
				by = primitiveB%angularMomentIndex(2)
				bz = primitiveB%angularMomentIndex(3)

				ax = angularMomentIndexA(1, a)
				ay = angularMomentIndexA(2, a)
				az = angularMomentIndexA(3, a)

				x0 = orbitalIntegralsX(ax, bx)
			  	y0 = orbitalIntegralsY(ay, by)
			  	z0 = orbitalIntegralsZ(az, bz)

			  	output(counter) = overPf*x0*y0*z0
			  	counter = counter + 1

			end do

		else if(.not.primitiveA%isVectorized .and. .not.primitiveB%isVectorized ) then

			counter = 1
			do a = 1, primitiveA%numCartesianOrbital
				do b = 1, primitiveB%numCartesianOrbital

					ax = angularMomentIndexA(1, a)
					ay = angularMomentIndexA(2, a)
					az = angularMomentIndexA(3, a)

					bx = angularMomentIndexB(1, b)
					by = angularMomentIndexB(2, b)
					bz = angularMomentIndexB(3, b)

					x0 = orbitalIntegralsX(ax, bx)
				  	y0 = orbitalIntegralsY(ay, by)
				  	z0 = orbitalIntegralsZ(az, bz)

				  	output(counter) = overPf*x0*y0*z0
				  	counter = counter + 1

				end do
			end do
		end if

	end function PrimitiveGaussian_overlapIntegral

	subroutine OverlapIntegral_oSRecursion(orbitalIntegralsX, orbitalIntegralsY, orbitalIntegralsZ,&
												 PA, PB, zeta, angularMomentIndexA, angularMomentIndexB)
		implicit none

	  	real(8), intent(inout), allocatable :: orbitalIntegralsX(:,:), orbitalIntegralsY(:,:), orbitalIntegralsZ(:,:)
	  	real(8), intent(in) :: PA(0:3), PB(0:3)
	  	real(8), intent(in) :: zeta
	  	integer(8), intent(in) :: angularMomentIndexA, angularMomentIndexB

	  	real(8) :: twoZetaInv
	  	integer :: i, j, k

	  	twoZetaInv = 1_8/(2_8*zeta)

	  	orbitalIntegralsX(0,0) = 1.0_8
	  	orbitalIntegralsY(0,0) = 1.0_8
	  	orbitalIntegralsZ(0,0) = 1.0_8

      	!! Upward recursion in j for i=0

	  	orbitalIntegralsX(0,1) = PB(0)
	  	orbitalIntegralsY(0,1) = PB(1)
	  	orbitalIntegralsZ(0,1) = PB(2)

	  	do j=1, angularMomentIndexB -1
	    	orbitalIntegralsX(0,j+1) = PB(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(0,j+1) = PB(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(0,j+1) = PB(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(0,j+1) = orbitalIntegralsX(0,j+1) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(0,j+1) = orbitalIntegralsY(0,j+1) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(0,j+1) = orbitalIntegralsZ(0,j+1) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

      	!! Upward recursion in i for all j's

	  	orbitalIntegralsX(1,0) = PA(0)
	  	orbitalIntegralsY(1,0) = PA(1)
	  	orbitalIntegralsZ(1,0) = PA(2)

	  	do j=1, angularMomentIndexB
	    	orbitalIntegralsX(1,j) = PA(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(1,j) = PA(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(1,j) = PA(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(1,j) = orbitalIntegralsX(1,j) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(1,j) = orbitalIntegralsY(1,j) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(1,j) = orbitalIntegralsZ(1,j) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

	  	do i=1, angularMomentIndexA - 1
	    	orbitalIntegralsX(i+1,0) = PA(0)*orbitalIntegralsX(i,0)
	    	orbitalIntegralsY(i+1,0) = PA(1)*orbitalIntegralsY(i,0)
	    	orbitalIntegralsZ(i+1,0) = PA(2)*orbitalIntegralsZ(i,0)
	    	orbitalIntegralsX(i+1,0) = orbitalIntegralsX(i+1,0) + i*twoZetaInv*orbitalIntegralsX(i-1,0)
	    	orbitalIntegralsY(i+1,0) = orbitalIntegralsY(i+1,0) + i*twoZetaInv*orbitalIntegralsY(i-1,0)
	    	orbitalIntegralsZ(i+1,0) = orbitalIntegralsZ(i+1,0) + i*twoZetaInv*orbitalIntegralsZ(i-1,0)
	    	do j=1, angularMomentIndexB
	      		orbitalIntegralsX(i+1,j) = PA(0)*orbitalIntegralsX(i,j)
	      		orbitalIntegralsY(i+1,j) = PA(1)*orbitalIntegralsY(i,j)
	      		orbitalIntegralsZ(i+1,j) = PA(2)*orbitalIntegralsZ(i,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + i*twoZetaInv*orbitalIntegralsX(i-1,j)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + i*twoZetaInv*orbitalIntegralsY(i-1,j)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + i*twoZetaInv*orbitalIntegralsZ(i-1,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + j*twoZetaInv*orbitalIntegralsX(i,j-1)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + j*twoZetaInv*orbitalIntegralsY(i,j-1)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + j*twoZetaInv*orbitalIntegralsZ(i,j-1)
	    	end do
	  	end do

	end subroutine OverlapIntegral_oSRecursion

end module OverlapIntegrals_

