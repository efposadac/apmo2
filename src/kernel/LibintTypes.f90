!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module LibintTypes_
	use, intrinsic :: iso_c_binding
	use R12Integrals_
	implicit none

	!>
	!! @brief Description
	!!
	!! @author Edwin Posada
	!!
	!! <b> Creation data : </b> 10-07-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 10-07-10 </tt>:  Edwin Posada ( efposadac@unal.edu.co )
	!!        -# Modulo que contiene los diferentes tipos a usarse en la interface Lowdin - Libint
	!!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
	!!        -# description
	!!
	!<

	!<
	!! @WARNING Para tener total claridad sobre el tamaño de los arreglos mire los archivos libint.h, libderiv.h y libr12.h
	!! Configurado para MAX_AM 6 para libint 5 libderiv y 5 libr12
	!<

	!<
	!! Estructuras libint.h
	!>
	type, bind(c) :: lib_int
		type(c_ptr)     :: int_stack
		type(c_ptr)     :: PrimQuartet
		real(c_double)  :: AB(3)
		real(c_double)  :: CD(3)
		type(c_ptr)     :: vrr_classes(13,13)
		type(c_ptr)     :: vrr_stack
	end type lib_int

	type, bind(c) :: prim_data
		real(c_double) :: F(25)
		real(c_double) :: U(3,6)
		real(c_double) :: twozeta_a
		real(c_double) :: twozeta_b
		real(c_double) :: twozeta_c
		real(c_double) :: twozeta_d
		real(c_double) :: oo2z
		real(c_double) :: oo2n
		real(c_double) :: oo2zn
		real(c_double) :: poz
		real(c_double) :: pon
		real(c_double) :: oo2p
		real(c_double) :: ss_r12_ss
	end type prim_data

	!<
	!!Estructuras libderiv.h
	!>
	type, bind(c) :: lib_deriv
		type(c_ptr)     :: int_stack
		type(c_ptr)     :: PrimQuartet
		type(c_ptr)     :: zero_stack
		type(c_ptr)     :: ABCD(12+144)
		real(c_double)  :: AB(3)
		real(c_double)  :: CD(3)
		type(c_ptr)     :: deriv_classes(12,7,7)
		type(c_ptr)     :: deriv2_classes(144,7,7)
		type(c_ptr)     :: dvrr_classes(7,7)
		type(c_ptr)     :: dvrr_stack
	end type lib_deriv

	!<
	!!Estructuras libr12.h
	!>
	type, bind(c) :: lib_r12
		type(c_ptr)     :: int_stack
		type(c_ptr)     :: PrimQuartet
		type(contr_data):: ShellQuartet
		type(c_ptr)     :: te_ptr(4)
		type(c_ptr)     :: t1vrr_classes(11,11)
		type(c_ptr)     :: t2vrr_classes(11,11)
		type(c_ptr)     :: rvrr_classes(11,11)
		type(c_ptr)     :: gvrr_classes(12,12)
		type(c_ptr)		:: r12vrr_stack
	end type lib_r12


end module LibintTypes_
