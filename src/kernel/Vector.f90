!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Clase encargada de manipular todo lo relacionado con matrices
!
! Esta clase manipula todo lo relacionado con matrices de tipo numerico, ademas
! de servir como una interface transparente para el uso de LAPACK en cuanto a metodos
! que involuran algebra lineal
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-19
!   - <tt> 2007-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!   - <tt> 2007-08-26 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Acoplamiento con el primer nivel de BLAS (daxpy,ddot,dnrm2,idamax)
!   - <tt> 2008-10-20 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
!        -# Modifico el formato del la funcion show
!   - <tt> 2009-04-27 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
!        -# Corrigio las funciones que retornan el maximo o  minimo de un vector.
!   - <tt> 2009-05-27 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
!        -# Creo operadores y funciones para operaciones basicas entre vectores
!**

module Vector_
	use APMO_
	use Exception_
	use BlasInterface_
	
	implicit none
	
	interface assignment(=)
		module procedure Vector_copyConstructor
	end interface
	
	interface operator ( / )
		module procedure Vector_scalarDiv
	end interface


	!< enum Vector_printFormatFlags {
	integer, parameter :: HORIZONTAL = 1
	integer, parameter :: VERTICAL = 2
	integer, parameter :: WITH_KEYS = 4
	!< }
	
	type, public :: Vector
		real(8) , allocatable :: values(:)
	end type Vector
	
	public :: &
		Vector_constructor, &
		Vector_copyConstructor, &
		Vector_destructor, &
		Vector_show, &
		Vector_getPtr, &
		Vector_swapElements, &
		Vector_getSize, &
		Vector_getElement, &
		Vector_setElement, &
		Vector_setIdentity, &
		Vector_setNull, &
		Vector_getMax, &
		Vector_getMin, &
		Vector_isNull, &
		Vector_plus, &
		Vector_scalarDiv, &
		Vector_dot, &
		Vector_cross, &
		Vector_norm, &
		Vector_removeElement
		
contains
	
	!**
	! Constructor por omisi�n
	!**
	subroutine Vector_constructor( this, ssize, value, values )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(in) :: ssize
		real(8), optional, intent(in) :: value
		real(8), optional, intent(in) :: values(:)
		
		real(8) :: valueTmp
		
		valueTmp = 0.0_8
		
		if ( allocated( this.values ) ) deallocate( this.values ) 
		allocate( this.values( ssize ) )
		
		if( present(value) ) then
		
			valueTmp = value
			this.values = valueTmp
			
		end if
		
		if( present(values) ) then
		
			this.values = values
			
		end if
		
	end subroutine Vector_constructor
	
	!**
	! @brief Constructor de copia
	! Reserva la memoria necesaria para otherMatrix y le asigna los valores de this
	!**
	subroutine Vector_copyConstructor( this, otherVector )
		implicit none
		type(Vector), intent(inout) :: this
		type(Vector), intent(in) :: otherVector
		
		if ( allocated( this.values ) ) deallocate( this.values ) 
		allocate( this.values( size(otherVector.values, DIM=1) ) )
		
		this.values = otherVector.values
		
	end subroutine Vector_copyConstructor
		
	!**
	! Destructor
	!**
	subroutine Vector_destructor( this )
		implicit none
		type(Vector), intent(inout) :: this
		
		if( allocated(this.values) ) deallocate( this.values )
		
	end subroutine Vector_destructor
	
	!**
	! Imprime a salida estandar la matriz realizando cambio de linea
	! con un maximo de "APMO_instance.FORMAT_NUMBER_OF_COLUMNS" columnas
	!**
	subroutine Vector_show( this, flags, keys )
		implicit none
		type(Vector), intent(in) :: this
		integer, intent(in), optional :: flags
		character(*), intent(in), optional :: keys(:)
		
		integer :: columns
		integer :: ssize
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: u
		integer :: tmpFlags
		type(Exception) :: ex
		
		tmpFlags = HORIZONTAL
		if( present(flags) ) then
			if( flags == WITH_KEYS )  then
				tmpFlags = HORIZONTAL + WITH_KEYS
			else
				tmpFlags = flags
			end if
		end if
		
		ssize = size( this.values , DIM=1 )
		
		if( tmpFlags == HORIZONTAL .or. tmpFlags == HORIZONTAL + WITH_KEYS ) then
		
			columns = ssize
			
			do k=1, ceiling( (ssize * 1.0)/(APMO_instance.FORMAT_NUMBER_OF_COLUMNS * 1.0 ) )
				
				l = APMO_instance.FORMAT_NUMBER_OF_COLUMNS * ( k - 1 ) + 1
				u = APMO_instance.FORMAT_NUMBER_OF_COLUMNS * ( k )
				
				if( u > ssize ) then
					columns = l + APMO_instance.FORMAT_NUMBER_OF_COLUMNS*( 1 - k ) +  ssize - 1
					u = columns
				end if
				
				if( ( tmpFlags - HORIZONTAL ) == WITH_KEYS ) then
				
					if( present( keys ) ) then
						write (6,"(<columns>A18)") ( trim(keys(i)), i = l, u )
					else
						write (6,"(<columns>I15)") ( i, i = l, u )
					end if
					
				end if
				
				print *,""
				write (6,"(<columns>F15.6)") ( this.values(i), i = l, u )
				print *,""
				
			end do
			
		else if( tmpFlags == VERTICAL .or. tmpFlags == VERTICAL + WITH_KEYS ) then
			
			if( ( tmpFlags - VERTICAL ) == WITH_KEYS ) then
				
				if( present( keys ) ) then
					do i=1, ssize
					
						write (6,"(A18,F15.6)") trim(keys(i)), this.values(i)
					end do
				else
					do i=1, ssize
						write (6,"(I5,F15.6)") i, this.values(i)
					end do
				end if
				
			else
			
				do i=1, ssize
					write (6,"(F15.6)") this.values(i)
				end do
				
			end if
		
		else
		
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object Vector in the show() function" )
			call Exception_setDescription( ex, "Bad flags selected" )
			call Exception_show( ex )
			
		end if
		
	end subroutine Vector_show
		
	!**
	! Devuelve un apuntador a la matrix solicitada
	!
	! @param this matrix de m x n
	! @return Apuntador a la matriz solicitada.
	! @todo No ha sido probada
	!**
	function Vector_getPtr( this ) result( output )
		implicit none
		type(Vector) , target , intent(in) :: this
		real(8) , pointer :: output(:)
		
		output => null()
		output => this.values
	
	end function Vector_getPtr
	
	!**
	! Intercambia los elementos i y j el vector
	!**
	subroutine Vector_swapElements( this, i, j )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j
		
		real(8) :: value1
		real(8) :: value2
		
		value1 = this.values( i )
		value2 = this.values( j )
		
		this.values( i ) = value2
		this.values( j ) = value1

	end subroutine Vector_swapElements
	
	!**
	! Retorna el tama�o del vector
	!**
	function Vector_getSize( this ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		integer :: output
		
		output = size( this.values , DIM=1 )
		
	end function Vector_getSize
	
	!**
	! Retorna el elemento i-esimo del vector
	!**
	function Vector_getElement( this, i ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(in) :: i
		real(8) :: output
		
		output = this.values( i )
		
	end function Vector_getElement
	
	!**
	! Selecciona el valor del elemento i-esimo del vector
	!**
	subroutine Vector_setElement( this, i, value )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(in) :: i
		real(8), intent(in) :: value
		
		this.values( i ) = value
		
	end subroutine Vector_setElement
	
	!**
	! Selecciona todos los elementos del vector a 1.0
	!**
	subroutine Vector_setIdentity( this )
		implicit none
		type(Vector), intent(inout) :: this
		
		this.values = 1.0_8
		
	end subroutine Vector_setIdentity
	
	!**
	! Selecciona a cero todos los elementos del vector
	!**
	subroutine Vector_setNull( this )
		implicit none
		type(Vector), intent(inout) :: this
		
		this.values = 0.0_8
		
	end subroutine Vector_setNull
	
	!**
	! Retorna el maximo valor absoluto y la posicion de este
	!**
	function Vector_getMax( this, pos ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(inout), optional :: pos
		real(8) :: output
		
		integer :: posTmp
		
#ifdef BLAS_VECTOR_SUPPORT
		if( present(pos) ) then
			pos = idamax( size(this.values), this.values, 1 )
		else
			posTmp = idamax( size(this.values), this.values, 1 )
		end if
		
		output = this.values(pos)
#else
		if( present(pos) ) then
			pos = maxloc( this.values, DIM=1 )
		else
			posTmp = maxloc( this.values, DIM=1 )
		end if
		
		output = this.values(pos)
#endif
		
	end function Vector_getMax

	!**
	! Retorna el valor minimo valor absoluto y la posicion de este
	!**
	function Vector_getMin( this, pos ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		integer, intent(inout), optional :: pos
		real(8) :: output
		
		integer :: posTmp
		
		if( present(pos) ) then
			pos = minloc( this.values, DIM=1 )
		else
			posTmp = minloc( this.values, DIM=1 )
		end if
		
		output = this.values(pos)
		
	end function Vector_getMin

	!**
	! Retorna true si todos lo elementos del vector sun iguales a
	! cero, false de otra manera
	!
	! @todo Falta implementar, depende de la declaracion de un umbral de comparacion de enteros en la clase APMO
	!**
	function Vector_isNull( this ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		logical :: output
		
		output = .false.
		
	end function Vector_isNull
	
	!**
	! Suma dos vectores
	!
	! @warning No verifica que los dos vectores sean del mismo tama�o
	!**
	function Vector_plus( this, otherVector ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		type(Vector), intent(in) :: otherVector
		type(Vector) :: output
		
		call Vector_copyConstructor( output, otherVector )
		
#ifdef BLAS_VECTOR_SUPPORT
		call daxpy( size(this.values), 1.0_8, this.values, 1, output.values, 1 )
#else
		output.values = this.values + otherVector.values
#endif
		
	end function Vector_plus

	function Vector_scalarDiv( this, scalar ) result( output )
		implicit none
		type(vector), intent(in) :: this
		real(8), intent(in) :: scalar
		type(Vector) :: output

		output=this
		output.values=output.values/scalar

	end function Vector_scalarDiv
	
	!**
	! Calcula el producto punto de dos vectores
	!
	! @warning No verifica que los dos vectores sean del mismo tama�o
	!**
	function Vector_dot( this, otherVector ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		type(Vector), intent(in) :: otherVector
		real(8) :: output
		
#ifdef BLAS_VECTOR_SUPPORT
		output = ddot( size(this.values), this.values, 1, otherVector.values, 1 )
#else
		output = dot_product( this.values, otherVector.values )
#endif
		
	end function Vector_dot
	
	!**
	! Calcula el producto vectorial tridimedional
	!
	! @warning No verifica que los dos vectores sean del mismo tama�o, 
	! 		   y asume que se trata de vectores tridimesionales
	!**
	function Vector_cross( this, otherVector ) result ( output )
		implicit none
		type(Vector), intent(in) :: this
		type(Vector), intent(in) :: otherVector
		type(Vector) :: output

		allocate( output.values(3) )

		output.values = [this.values(2)*otherVector.values(3) -  this.values(3)*otherVector.values(2), &
				 this.values(3)*otherVector.values(1) -  this.values(1)*otherVector.values(3), &
				 this.values(1)*otherVector.values(2) -  this.values(2)*otherVector.values(1)  ]
		
	end function Vector_cross

	!**
	! Calcula el producto vectorial tridimedional entre vectores nativos de fortran
	!
	!**
	function Vector_Fortran_Cross( fotranVector, otherFotranVector ) result ( output )
		implicit none
		real(8), intent(in) :: fotranVector(3)
		real(8), intent(in) :: otherFotranVector(3)
		real(8) :: output(3)

		

		output = [fotranVector(2)*otherFotranVector(3) -  fotranVector(3)*otherFotranVector(2), &
				 fotranVector(3)*otherFotranVector(1) -  fotranVector(1)*otherFotranVector(3), &
				 fotranVector(1)*otherFotranVector(2) -  fotranVector(2)*otherFotranVector(1)  ]
		
	end function Vector_Fortran_Cross


	!**
	! Calcula la norma euclideana de un vector
	!**
	function Vector_norm( this ) result ( output )
		implicit none
		type(Vector), intent(inout) :: this
		real(8) :: output
		
#ifdef BLAS_VECTOR_SUPPORT
		output = dnrm2( size(this.values), this.values, 1 )
#else
		output = sqrt( dot_product( this.values, this.values ) )
#endif
		
	end function Vector_norm

		!> 
	!! @brief Remueve la fila especificada de una matriz
	!<
	subroutine Vector_removeElement( this, numberOfElement )
		implicit none
		type(Vector) :: this
		integer, intent(in) :: numberOfElement

		real(8), allocatable :: auxArray(:)
		integer :: numberOfElements

		numberOfElements = size( this.values )

		if (numberOfElement <= numberOfElements ) then
			
			allocate( auxArray(numberOfElements-1) )
			auxArray(1:numberOfElement-1) = this.values(1:numberOfElement-1)
			auxArray(numberOfElement:numberOfElements-1) = this.values(numberOfElement+1:numberOfElements)
			deallocate( this.values )
			allocate( this.values(numberOfElements-1) )
			this.values = auxArray
			deallocate( auxArray )

		end if

	end subroutine Vector_removeElement


	
end module Vector_
