!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008 by                                                        !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Clase estatica que contiene la interface para libreriar externas
!
! @author Sergio Gonzalez
!
! <b> Fecha de creacion : </b> 2009-02-24
!   - <tt> 2008-08-19 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!**
module GenericInterface_
	
	implicit none
	
	interface GenericInterface
	
		!**
		! Realiza proceso de minimizacion restringida o no de una funcion arbitraria
		! tomado de: 	Zhu, C.; Lu, P. and Nocedal, J. Department of Electrical Engineering
		!		and Computer Science. Northwestern University. 1996
		!		L-BFGS-B FORTRAN SUBROUTINES FOR LARGE-SCALE BOUND CONSTRAINED
		!		OPTIMIZATION
		!**
		subroutine  setulb(n, m, x, l, u, nbd, f, g, factr, pgtol, wa, iwa, task, iprint,  csave, lsave, isave, dsave)
			character*60    ::  task, csave
 			logical ::   lsave(4)
			integer :: n, m, iprint, nbd(*), iwa(*), isave(44)
			real(8) :: f, factr, pgtol, x(*), l(*), u(*), g(*), wa(*), dsave(29)
		end subroutine setulb

		!**
		! Realiza transformacion de cuatro indices de integrales en OA a integrales
		! en orbitales moleculares.
		! tomado de: 	Yamamoto, Shigeyoshi; Nagashima, U. 
		!               Four-index integral tranformation exploiting symmetry.
		!		Computer Physics Communications, 2005, 166, 58-65
		!**
		subroutine  fourIndexTransformation(numberOfContractions, nameFile )
			integer :: numberOfContractions
			character(*) :: nameFile
		end subroutine fourIndexTransformation
	
	end interface GenericInterface
	
end module GenericInterface_
