!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module EnergyDecomposition_
 use Exception_
        use Matrix_
        implicit none

        !>
        !! @brief Description
        !!
        !! @author lalita
        !!
        !! <b> Creation data : </b> 02-09-11
        !!
        !! <b> History change: </b>
        !!
        !!   - <tt> 02-09-11 </tt>:  lalita ( email@server )
        !!        -# description.
        !!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
        !!        -# description
        !!
        !<
	type, public :: EnergyDecomposition
		character(20) :: name
		logical :: isInstanced
		end type

	public :: &
		EnergyDecomposition_constructor, &
		EnergyDecomposition_destructor, &
		EnergyDecomposition_show, &
		EnergyDecomposition_electrostaticEnergy

contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine EnergyDecomposition_constructor(this)
		implicit none
		type(EnergyDecomposition) :: this

		this%isInstanced = .true.

	end subroutine EnergyDecomposition_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine EnergyDecomposition_destructor(this)
		implicit none
		type(EnergyDecomposition) :: this

		this%isInstanced = .false.

	end subroutine EnergyDecomposition_destructor

	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	subroutine EnergyDecomposition_show(this)
		implicit none
		type(EnergyDecomposition) :: this
		
	end subroutine EnergyDecomposition_show

	!!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function EnergyDecomposition_isInstanced( this ) result( output )
		implicit  none
		type(EnergyDecomposition), intent(in) :: this
		logical :: output

		output = this%isInstanced

	end function EnergyDecomposition_isInstanced

	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine EnergyDecomposition_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription

		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )

	end subroutine EnergyDecomposition_exception


	!>
	!! @brief  Genera la matriz ESX
	!! a partir de las matriz de overlap o de fock.
	!<
	function EnergyDecomposition_electrostaticEnergy ( auxMatrix ) result(output)
		implicit none
		type(Matrix), intent(in) :: auxMatrix
		type(Matrix) :: output
		integer(8) :: numRowMatrix
		integer(8) :: numColMatrix
		integer :: i

		numRowMatrix = size(auxMatrix%values, DIM=1)
		numColMatrix = size(auxMatrix%values, DIM=2)
		
		call Matrix_constructor(output,numRowMatrix,numColMatrix)
		
		output%values=0.0_8

		!copia los bloques de diagonal de la matriz
		
		do i=1, numRowmatrix
			output%values(i,i)=auxMatrix%values(i,i)
		end do

		! asi tendriamos ESX falta quitar la derivada de overlap para tener ES 
                                                                                                                                  
	end function EnergyDecomposition_electrostaticEnergy

	!>
	!! @brief  Genera la matriz PL
	!! a partir de las matrices de overlap o de fock.
	!! no incluye la diagonal, hay que sumarla
	!<
	function EnergyDecomposition_polarizationEnergy (auxMatrix, indexes) result(output)
		implicit none
		type(Matrix), intent(in) :: auxMatrix
		type(Matrix) :: output
		integer(8) :: indexes(:)
		integer :: numMonomer
		integer :: i, j, k
		integer(8) :: numRowMatrix
		integer(8) :: numColMatrix

		numRowMatrix = size(auxMatrix%values, DIM=1)
		numColMatrix = size(auxMatrix%values, DIM=2)

		call Matrix_constructor(output,numRowMatrix,numColMatrix)

		numMonomer= size(indexes)/4

		output%values=0.0_8

		!copia los bloques de la matriz compuestos por orbitales ocupados del monomero y
		! orbitales ocupados de los otros monomeros

		do j=1, numMonomer*2, 2  !recorre los indices de los orbitales ocupados
			do i=indexes(j), indexes(j+1) !recorre los orbitales ocupados de un monomero
				do  k=indexes(j+numMonomer*2),indexes(j+1+numMonomer*2)	!recorre los orbitales virtuales del mismo monomero
					output%values(i,k) = auxMatrix%values(i,k)
					output%values(k,i) = output%values(i,k)
				end do
			end do
		end do

	end function EnergyDecomposition_polarizationEnergy

	!>
	!! @brief  Genera la matriz EX
	!! a partir de las matrices de overlap o de fock.
	!! no incluye la diagonal, hay que sumarla
	!<

		function EnergyDecomposition_exchangeEnergy (auxMatrix, indexes) result(output)
		implicit none
		type(Matrix), intent(in) :: auxMatrix
		type(Matrix) :: output
		integer(8) :: indexes(:)
		integer :: numMonomer
		integer :: i, j, k
		integer(8) :: numRowMatrix
		integer(8) :: numColMatrix

		numRowMatrix = size(auxMatrix%values, DIM=1)
		numColMatrix = size(auxMatrix%values, DIM=2)

		call Matrix_constructor(output,numRowMatrix,numColMatrix)

		numMonomer= size(indexes)/4

		output%values=0.0_8

		!copia los bloques de la matriz compuestos por orbitales ocupados del monomero y
		! orbitales ocupados de los otros monomeros

		do j=1, numMonomer*2, 2   !recorre los indices de orbitales ocupados
			do i=indexes(j), indexes(j+1)	!recorre el orbital ocupado
				do  k=indexes(j+2),indexes(numMonomer*2) !recorre los otros orbitales ocupados
					output%values(i,k) = auxMatrix%values(i,k)
					output%values(k,i) = output%values(i,k)
				end do
			end do
		end do



	end function EnergyDecomposition_exchangeEnergy

	!>
	!! @brief  Genera la matriz CT
	!! a partir de las matrices de overlap o de fock.
	!! no incluye la diagonal, hay que sumarla
	!<

		function EnergyDecomposition_chargeTransfer (auxMatrix, indexes, indexMonomer) result(output)
		implicit none
		type(Matrix), intent(in) :: auxMatrix
		type(Matrix) :: output
		integer(8) :: indexes(:)
		integer(8) :: indexMonomer(4)
		integer :: numMonomer
		integer :: i, j, k,l
		integer(8) :: numRowMatrix
		integer(8) :: numColMatrix

		numRowMatrix = size(auxMatrix%values, DIM=1)
		numColMatrix = size(auxMatrix%values, DIM=2)

		call Matrix_constructor(output,numRowMatrix,numColMatrix)

		numMonomer= size(indexes)/4

		output%values=0.0_8

		!copia los bloques de la matriz compuestos por orbitales ocupados del monomero y
		! orbitales desocupados de los otros monomeros

		do i=numMonomer*2, size(indexes), 2  			!recorre los indices de los orbitales virtuales
			if (indexes(i)/=indexMonomer(3))then			!filtra los orbitales virtuales del monomero
				do j=indexMonomer(1),indexMonomer(2)	!recorre los orbitales ocupados del monomero
					do k=indexes(i), indexes(i+1)		!recorre los orbitales virtuales
						output%values(k,j) = auxMatrix%values(k,j)
						output%values(j,k) =output%values(k,j)
					end do
				end do
			end if
		end do

		! copia los bloques de la matriz compuestos por orbitales virtuales de los otros monomeros
		! y con los orbitales virtuales de los otros monomeros
		do j=numMonomer*2, size(indexes),2	!recorre los indices de los orbitales virtuales
			do i=numMonomer*2, size(indexes),2	!recorre los indices de los orbitales virtuales
				if ((j/=i) .and. (indexes(j)/=indexMonomer(3)) .and. (indexes(i)/=indexMonomer(3)))then ! selecciona los blosque compuestos por los orbitales virtuales diferentes del monomero
					do k=indexes(j),indexes(j+1)
						do l=indexes(i),indexes(i+1)
							output%values(j,i)= auxMatrix%values(j,i)
						end do
					end do
				end if
			end do
		end do


	end function EnergyDecomposition_chargeTransfer

!	function EnergyDecomposition_CTPLX (auxMatrix, indexes, indexMonomer) result(output)
!		implicit none
!		type(Matrix), intent(in) :: auxMatrix
!		type(Matrix) :: output
!		integer(8) :: indexes(:)
!		integer(8) :: indexMonomer(4)
!		integer :: numMonomer
!		integer :: i, j, k,l
!		integer(8) :: numRowMatrix
!		integer(8) :: numColMatrix
!
!		numRowMatrix = size(auxMatrix%values, DIM=1)
!		numColMatrix = size(auxMatrix%values, DIM=2)
!
!		call Matrix_constructor(output,numRowMatrix,numColMatrix)
!		output%values=0.0_8
!
!		!copia los bloques de la matriz compuestos por orbitales ocupados del monomero y
!		! orbitales ocupados de los otros monomeros
!
!		do j=1, numMonomer*2, 2  !recorre los indices de los orbitales ocupados
!			do i=indexes(j), indexes(j+1) !recorre los orbitales ocupados de un monomero
!				do  k=indexes(j+numMonomer*2),indexes(j+1+numMonomer*2)	!recorre los orbitales virtuales del mismo monomero
!					output%values(i,k) = auxMatrix%values(i,k)
!					output%values(k,i) = output%values(i,k)
!				end do
!			end do
!		end do
!
!
!	end function EnergyDecomposition_CTPLX
end module EnergyDecomposition_
