!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions, analytic derivatives.                          !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para calculo de derivadas de integrales de attraccion coulombica 
! 
! Este modulo contiene los algoritmos necesarios para la evaluacion de derivadas
! de integrales de energia cinetica entre pares de funciones gaussianas primitivas 
! (PrimitiveGaussian_), sin normalizar.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-12-03
!
! @see gaussianProduct, PrimitiveGaussian_, FAttractionIntegrals 
!
!**
module AttractionDerivatives_
	use PrimitiveGaussian_
	use Exception_

	private ::
		real(8) :: originPuntualCharge(3)
		real(8) :: commonFactor
		real(8) :: reducedOrbExponent
		real(8) :: originFactor
		real(8) :: incompleteGammaArgument
		real(8) :: incompletGamma_0
		real(8) :: incompletGamma_1
		real(8) :: incompletGamma_2
		real(8) :: incompletGamma_3
		real(8) :: incompletGamma_4 
		real(8) :: incompletGamma_5
		integer :: component
		integer :: center
		integer :: indxCenterA
		integer :: indxCenterB
		integer :: RcIndx
		
		type(PrimitiveGaussian) :: primitives(2)
		type(PrimitiveGaussian) :: primitiveAxB
		

	public :: &
		PrimitiveGaussian_attractionDerivative
		
	private :: &
		AttractionDerivatives_ss, &
		AttractionDerivatives_ps, &
		AttractionDerivatives_pp, &
		AttractionDerivatives_ds, &
		AttractionDerivatives_dp, &
		AttractionDerivatives_dd
		
contains

	!**
	! Retorna el valor de la derivada de integrales de atraccion coulombica 
	! Esta funci�n se encarga de utilizar el procedimineto adecuado
	! de acuerdo al momento angular de las funciones de entrada
	!
	! @param primitiveA Gausiana primitiva 
	! @param primitiveB Gausiana primitiva 
	!
	! @return Valor de la integral de atraccion coulombica
	!**
	function PrimitiveGaussian_attractionDerivative( primitiveA , primitiveB, originPuntualParticle ,&
		center_, component_, puntualParticleOwner) result( output )
		
		implicit none
		
		type(PrimitiveGaussian), intent(in) :: primitiveA
		type(PrimitiveGaussian), intent(in) :: primitiveB
		integer, intent(in) :: center_
		integer, intent(in) :: component_
		integer, intent(in), optional :: puntualParticleOwner
		real(8) :: originPuntualParticle(3)
		real(8) :: output
		
		integer :: caseIntegral
		type(Exception) :: ex
		
		!!*******************************************************************
		!! Seleccion el m�todo adecuado para el calculo de la integral
		!! de acuedo con el momento angular de las funciones de entrada.
		!!
		
		originPuntualCharge = originPuntualParticle
				
		!! Ordena la gausianas de entrada de mayor a menor momento
		call  PrimitiveGaussian_sort( primitives,primitiveA, primitiveB )
		
		center = center_
		component = component_
		indxCenterA=primitives(1).owner
		indxCenterB=primitives(2).owner
		
		RcIndx = 0
		if (present(puntualParticleOwner)) RcIndx=puntualParticleOwner
		
		if ( 	abs( primitives(1).origin(1)-originPuntualCharge(1) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD .and. & 
			abs( primitives(1).origin(2)-originPuntualCharge(2) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD .and. & 
			abs( primitives(1).origin(3)-originPuntualCharge(3) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD  ) then
			RcIndx= indxCenterA
		end if
		
		if ( 	abs( primitives(2).origin(1)-originPuntualCharge(1) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD .and. & 
			abs( primitives(2).origin(2)-originPuntualCharge(2) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD .and. & 
			abs( primitives(2).origin(3)-originPuntualCharge(3) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD  ) then
			RcIndx= indxCenterB
		end if
		
		!! Obtiene el producto de la dos gausianas de entrada
		primitiveAxB = PrimitiveGaussian_product(primitiveA, primitiveB)
		reducedOrbExponent = PrimitiveGaussian_reducedOrbitalExponent( primitives(1) , primitives(2) )

		!!*******************************************************************
		!! Determina la abscisa donde ser� evaluada la funci�n gamma incompleta 
		!!
		incompleteGammaArgument = primitiveAxB.orbitalExponent  * ( &
		( primitiveAxB.origin(1) - originPuntualCharge(1) )**2.0_8 + &
		( primitiveAxB.origin(2) - originPuntualCharge(2) )**2.0_8 + &
		( primitiveAxB.origin(3) - originPuntualCharge(3) )**2.0_8 )
		!!********************************************************************
			
		commonFactor = ( Math_PI/primitiveAxB.orbitalExponent ) * &
			PrimitiveGaussian_productConstant( primitives(1) , primitives(2) )	
			
		originFactor= ( (primitiveA.origin(1) & 
				- primitiveB.origin(1) ) ** 2.0_8 &
				+ ( primitiveA.origin(2) &
				- primitiveB.origin(2) ) ** 2.0_8 &
				+ (primitiveA.origin(3) &
				- primitiveB.origin(3) ) ** 2.0_8 ) * reducedOrbExponent
		
		!! Determina la integral que se debe usar
		caseIntegral = 4*PrimitiveGaussian_getAngularMoment(primitives(1)) + &
			PrimitiveGaussian_getAngularMoment(primitives(2))
		 
		select case (caseIntegral)
		
		case(0) 
			output = AttractionDerivatives_ss()
		
		case(4) 
			output = AttractionDerivatives_ps()
				
		case(5) 
			output = AttractionDerivatives_pp()
		
		case(8)
			output = AttractionDerivatives_ds()
			
		case(9) 
			output = AttractionDerivatives_dp()
			
		case(10) 
			output = AttractionDerivatives_dd()
		
		case default
			output = 0.0_8
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the attraction derivative function" )
			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
			call Exception_show( ex )

		end select
		!!*******************************************************************
		
	end function PrimitiveGaussian_attractionDerivative


	!**
	! Retorna el valor de la derivada integral <i> d(s|A|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_ss() result( output )
		implicit none
		real(8) ::output
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		
		!! Calcula la integral de atraccion coulombica
		
		output = -4.0_8 * ( reducedOrbExponent * incompletGamma_0 &
			* (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))&
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) + incompletGamma_1 &
			* (originPuntualCharge(component) - primitiveAxB.origin(component)) &
			* (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) &
			- Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent &
			- Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) &
			* commonFactor
	
	end function AttractionDerivatives_ss
	!**
	! Retorna el valor de la derivada integral <i> d(p|A|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_ps() result( output )
		implicit none
		real(8) ::output
		
		integer :: alpha
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma(incompleteGammaArgument,2)
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		
		!! Calcula la integral de atraccion coulombica
		output= (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
    Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
   2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
    (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
      primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
   4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
    (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
   4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
    (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) * commonFactor /& 
 (primitiveAxB.orbitalExponent)
				
	end function AttractionDerivatives_ps
	
	!**
	! Retorna el valor de la derivada integral <i> d(p|A|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_pp() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma(incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma(incompleteGammaArgument,3)
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		beta  = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de atraccion coulombica
		output= (-2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
    Math_kroneckerDelta(beta, component)*& 
    (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(1).orbitalExponent - & 
   2.0_8*Math_kroneckerDelta(beta, component)*& 
    (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
    (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
     Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + (-primitives(2).origin(beta) + primitiveAxB.origin(beta))*& 
    (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
     2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
   (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*& 
    (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
     2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
   2.0_8*Math_kroneckerDelta(alpha, beta)*& 
    (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
       (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
       Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
         (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
        (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
         (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))))*commonFactor/primitiveAxB.orbitalExponent
				
	end function AttractionDerivatives_pp
	
	!**
	! Retorna el valor de la derivada integral <i> d(d|A|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_ds() result( output )
		implicit none
		real(8) ::output
		
		integer :: alpha
		integer :: beta
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma(incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma(incompleteGammaArgument,3)
				
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		
		!! Calcula la integral de atraccion coulombica
		output=(2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
    Math_kroneckerDelta(beta, component)*& 
    (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
   2.0_8*Math_kroneckerDelta(beta, component)*& 
    (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
    (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
     Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
     2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
   (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
       Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
      primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
   2.0_8*Math_kroneckerDelta(alpha, beta)*& 
    (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
       (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
       Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
         (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
        (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
         (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))))*commonFactor/primitiveAxB.orbitalExponent
				
	end function AttractionDerivatives_ds


	!**
	! Retorna el valor de la derivada integral <i> d(d|A|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_dp() result( output )
		implicit none
		real(8) ::output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma(incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma(incompleteGammaArgument,3)
		incompletGamma_4 = Math_incompletGamma(incompleteGammaArgument,4)
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de atraccion coulombica
		output= ((Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
    Math_kroneckerDelta(kappa, component)*& 
    (incompletGamma_0*(Math_kroneckerDelta(alpha, beta) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
     2.0_8*primitiveAxB.orbitalExponent*incompletGamma_2*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - primitiveAxB.origin(beta)) - & 
     incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
         originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
           primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
          primitiveAxB.origin(beta))))*primitives(1).orbitalExponent + Math_kroneckerDelta(kappa, component)*& 
    (incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
     2.0_8*primitiveAxB.orbitalExponent*incompletGamma_3*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - primitiveAxB.origin(beta)) - & 
     incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
         originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
           primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
          primitiveAxB.origin(beta))))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
     Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
      primitives(2).orbitalExponent) + Math_kroneckerDelta(beta, kappa)*& 
    (-incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
     incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
     incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
     incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
     2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
      (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
   Math_kroneckerDelta(alpha, kappa)*& 
    (-incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
     incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
     incompletGamma_1*Math_kroneckerDelta(beta, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
     incompletGamma_2*Math_kroneckerDelta(beta, component)*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
      (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
     2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
     2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
      (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
   primitiveAxB.orbitalExponent*(-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
    (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
     2.0_8*Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, beta)*& 
      (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
         (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
          (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) - & 
   primitiveAxB.orbitalExponent*(-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*& 
    (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
     2.0_8*Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, beta)*& 
      (-(reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
         (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
          (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent)))))*commonFactor/(primitiveAxB.orbitalExponent**2) 
				
	end function AttractionDerivatives_dp

	!**
	! Retorna el valor de la derivada integral <i> d(d|A|d)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de atraccion coulombica
	!
	!**
	function AttractionDerivatives_dd() result( output )
		implicit none
		real(8) ::output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma(incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma(incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma(incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma(incompleteGammaArgument,3)
		incompletGamma_4 = Math_incompletGamma(incompleteGammaArgument,4)
		incompletGamma_5 = Math_incompletGamma(incompleteGammaArgument,5)
		
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 2 )
		
		!! Calcula la integral de atraccion coulombica
		output=(2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
    Math_kroneckerDelta(lambda, component)*& 
    ((incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(primitives(1).origin(alpha) + & 
           originPuntualCharge(alpha) - 2.0_8*primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
           kappa)*(primitives(1).origin(beta) + originPuntualCharge(beta) - 2.0_8*primitiveAxB.origin(beta))) + & 
       incompletGamma_0*(Math_kroneckerDelta(beta, kappa)*(-primitives(1).origin(alpha) + & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
          (-primitives(1).origin(beta) + primitiveAxB.origin(beta))) + & 
       incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(-originPuntualCharge(alpha) + & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
          (-originPuntualCharge(beta) + primitiveAxB.origin(beta)))) - & 
     (incompletGamma_0*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_2*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + & 
     (incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_3*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(originPuntualCharge(kappa) - primitiveAxB.origin(kappa)))*primitives(1).orbitalExponent - & 
   2.0_8*Math_kroneckerDelta(lambda, component)*& 
    ((incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitives(1).origin(alpha) + & 
           originPuntualCharge(alpha) - 2.0_8*primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
           kappa)*(primitives(1).origin(beta) + originPuntualCharge(beta) - 2.0_8*primitiveAxB.origin(beta))) + & 
       incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-primitives(1).origin(alpha) + & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
          (-primitives(1).origin(beta) + primitiveAxB.origin(beta))) + & 
       incompletGamma_3*(Math_kroneckerDelta(beta, kappa)*(-originPuntualCharge(alpha) + & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
          (-originPuntualCharge(beta) + primitiveAxB.origin(beta)))) - & 
     (incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_3*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + & 
     (incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_4*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_3*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(originPuntualCharge(kappa) - primitiveAxB.origin(kappa)))*& 
    (-(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)) + Math_kroneckerDelta(indxCenterA, center)*& 
      primitives(1).orbitalExponent + Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
   Math_kroneckerDelta(kappa, lambda)*& 
    (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
     2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
     2.0_8*Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     2.0_8*Math_kroneckerDelta(beta, component)*& 
      (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*(-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, beta)*& 
      (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
         (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
          (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, beta)*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) - incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(originPuntualCharge(component) - primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
           reducedOrbExponent*primitives(2).origin(component) + (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
         Math_kroneckerDelta(indxCenterB, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) + & 
   Math_kroneckerDelta(beta, lambda)*& 
    (-2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(1).orbitalExponent + & 
     2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(1).orbitalExponent - & 
     2.0_8*Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     2.0_8*Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     (-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     (-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*(-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, kappa)*& 
      (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
         (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
          (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(alpha, kappa)*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) - incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(originPuntualCharge(component) - primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
           reducedOrbExponent*primitives(2).origin(component) + (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
         Math_kroneckerDelta(indxCenterB, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) + & 
   Math_kroneckerDelta(alpha, lambda)*& 
    (-2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_0*(primitives(1).origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_1*(-originPuntualCharge(beta) + primitiveAxB.origin(beta)))*primitives(1).orbitalExponent + & 
     2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(primitives(1).origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_2*(-originPuntualCharge(beta) + primitiveAxB.origin(beta)))*primitives(1).orbitalExponent - & 
     2.0_8*Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(primitives(1).origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_2*(-originPuntualCharge(beta) + primitiveAxB.origin(beta)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     2.0_8*Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_2*(primitives(1).origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_3*(-originPuntualCharge(beta) + primitiveAxB.origin(beta)))*& 
      (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
        primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
     (-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       2.0_8*incompletGamma_1*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       2.0_8*incompletGamma_2*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) - & 
     (-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*(-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(beta, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     (-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*(-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
         Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(beta, component)*& 
        primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(beta, kappa)*& 
      (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
         (primitives(1).origin(component) - primitives(2).origin(component))) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + Math_kroneckerDelta(indxCenterB, center)*& 
          (-(reducedOrbExponent*primitives(1).origin(component)) + reducedOrbExponent*primitives(2).origin(component) + & 
           (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))) + & 
     2.0_8*Math_kroneckerDelta(beta, kappa)*& 
      (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        (primitives(1).origin(component) - primitives(2).origin(component)) - incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(originPuntualCharge(component) - primitiveAxB.origin(component)) + & 
         Math_kroneckerDelta(indxCenterA, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
           reducedOrbExponent*primitives(2).origin(component) + (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
         Math_kroneckerDelta(indxCenterB, center)*(reducedOrbExponent*primitives(1).origin(component) - reducedOrbExponent*primitives(2).origin(component) + & 
           (-originPuntualCharge(component) + primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) + & 
   2.0_8*(-primitives(2).origin(lambda) + primitiveAxB.origin(lambda))*& 
    ((Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_0*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_2*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*primitives(1).orbitalExponent + Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_3*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
       Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
        primitives(2).orbitalExponent) + Math_kroneckerDelta(beta, kappa)*& 
      (-incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
       incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     Math_kroneckerDelta(alpha, kappa)*& 
      (-incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       incompletGamma_1*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
       incompletGamma_2*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     primitiveAxB.orbitalExponent*(-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_0*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
       2.0_8*Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_1*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_1*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_1*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) - (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_2*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) + 2.0_8*Math_kroneckerDelta(alpha, beta)*& 
        (-(reducedOrbExponent*incompletGamma_0*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
             center))*(primitives(1).origin(component) - primitives(2).origin(component))) + & 
         incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         incompletGamma_1*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + & 
             primitiveAxB.origin(component)) + Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
           Math_kroneckerDelta(indxCenterB, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) - & 
     primitiveAxB.orbitalExponent*(-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*& 
      (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
       2.0_8*Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_2*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) - (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_3*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) + 2.0_8*Math_kroneckerDelta(alpha, beta)*& 
        (-(reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
             center))*(primitives(1).origin(component) - primitives(2).origin(component))) + & 
         incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + & 
             primitiveAxB.origin(component)) + Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
           Math_kroneckerDelta(indxCenterB, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))))) - & 
   2.0_8*(-originPuntualCharge(lambda) + primitiveAxB.origin(lambda))*& 
    ((Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
      Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_1*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_3*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*primitives(1).orbitalExponent + Math_kroneckerDelta(kappa, component)*& 
      (incompletGamma_2*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta))) + & 
       2.0_8*primitiveAxB.orbitalExponent*incompletGamma_4*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(originPuntualCharge(beta) - & 
         primitiveAxB.origin(beta)) - incompletGamma_3*(Math_kroneckerDelta(alpha, beta) + & 
         2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta)*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha)) - & 
           originPuntualCharge(beta)*primitiveAxB.origin(alpha) + primitives(1).origin(alpha)*(originPuntualCharge(beta) - & 
             primitiveAxB.origin(beta)) - originPuntualCharge(alpha)*primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.origin(alpha)*& 
            primitiveAxB.origin(beta))))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
       Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
        primitives(2).orbitalExponent) + Math_kroneckerDelta(beta, kappa)*& 
      (-incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent + & 
       incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
       incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     Math_kroneckerDelta(alpha, kappa)*& 
      (-incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent + & 
       incompletGamma_2*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) - & 
       incompletGamma_3*Math_kroneckerDelta(beta, component)*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) + & 
       2.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(beta) - primitiveAxB.origin(beta))*& 
        (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
          (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent))) + & 
     primitiveAxB.orbitalExponent*(-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
      (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_1*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
       2.0_8*Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_2*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_2*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_2*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_3*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) - (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_3*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) + 2.0_8*Math_kroneckerDelta(alpha, beta)*& 
        (-(reducedOrbExponent*incompletGamma_1*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
             center))*(primitives(1).origin(component) - primitives(2).origin(component))) + & 
         incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         incompletGamma_2*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + & 
             primitiveAxB.origin(component)) + Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
           Math_kroneckerDelta(indxCenterB, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent)))) - & 
     primitiveAxB.orbitalExponent*(-originPuntualCharge(kappa) + primitiveAxB.origin(kappa))*& 
      (2.0_8*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
        Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_2*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*primitives(2).orbitalExponent - & 
       2.0_8*Math_kroneckerDelta(beta, component)*& 
        (incompletGamma_3*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_4*(-originPuntualCharge(alpha) + primitiveAxB.origin(alpha)))*& 
        (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
          primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
       (-primitives(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_3*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_3*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_3*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_4*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) - (-originPuntualCharge(beta) + primitiveAxB.origin(beta))*& 
        (-2.0_8*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - & 
           Math_kroneckerDelta(indxCenterB, center))*Math_kroneckerDelta(alpha, component)*& 
          primitives(2).orbitalExponent + 2.0_8*incompletGamma_4*Math_kroneckerDelta(alpha, component)*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         4.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
          (reducedOrbExponent*incompletGamma_3*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
              center))*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
           incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, & 
               center) - Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - & 
             Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent)) - & 
         4.0_8*primitiveAxB.orbitalExponent*(originPuntualCharge(alpha) - primitiveAxB.origin(alpha))*(reducedOrbExponent*incompletGamma_4*& 
            (Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
            (primitives(1).origin(component) - primitives(2).origin(component)) + incompletGamma_5*(originPuntualCharge(component) - & 
             primitiveAxB.origin(component))*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - & 
             Math_kroneckerDelta(indxCenterA, center)*primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*& 
              primitives(2).orbitalExponent))) + 2.0_8*Math_kroneckerDelta(alpha, beta)*& 
        (-(reducedOrbExponent*incompletGamma_2*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(2, & 
             center))*(primitives(1).origin(component) - primitives(2).origin(component))) + & 
         incompletGamma_4*(originPuntualCharge(component) - primitiveAxB.origin(component))*& 
          (primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center) - Math_kroneckerDelta(indxCenterA, center)*& 
            primitives(1).orbitalExponent - Math_kroneckerDelta(indxCenterB, center)*primitives(2).orbitalExponent) + & 
         incompletGamma_3*(primitiveAxB.orbitalExponent*Math_kroneckerDelta(RcIndx, center)*(-originPuntualCharge(component) + & 
             primitiveAxB.origin(component)) + Math_kroneckerDelta(indxCenterA, center)*(reducedOrbExponent*primitives(1).origin(component) - & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(1).orbitalExponent) + & 
           Math_kroneckerDelta(indxCenterB, center)*(-(reducedOrbExponent*primitives(1).origin(component)) + & 
             reducedOrbExponent*primitives(2).origin(component) + (originPuntualCharge(component) - primitiveAxB.origin(component))*primitives(2).orbitalExponent))))))*commonFactor/& 
 (2.0_8*primitiveAxB.orbitalExponent**2)
	end function AttractionDerivatives_dd
	
end module AttractionDerivatives_
