!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    thisPtr files is part of nonBOA                                                 !
!!                                                                                 !
!!    thisPtr program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    thisPtr program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with thisPtr program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de bases gausianas.
! 
!  Este modulo define una seudoclase para la construccion de funciones base expresadas 
!  como combinacion lineal de funciones gausianas primitiva.
!
!     \f[ \chi = \sum_{i = 1}^{K}{C_i {\phi}_i(r,\zeta,n)} \f]
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-02-07
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see FContractedGaussian, FPrimitiveGaussian
!
! @todo Implementar metodos para lectura y escritura de archivos de bases datos.
!**
module BasisSet_
	use ContractedGaussian_
	use Exception_
	use String_
	
	implicit none
	
	type, public :: BasisSet
		
		character(30)	:: name
		integer		:: length
		integer		:: ttype
		type(ContractedGaussian), allocatable :: contractions(:)
		
		logical :: isInstanced		!< Dispuesta por razones de conveniencia
		integer :: contractionLength	!< Dispuesta por razones de conveniencia
		integer :: numberOfPrimitives 

	end type BasisSet
	
	type(BasisSet), pointer, private :: thisPtr
	integer, private :: iterContraction_i
	integer, private :: iterContraction_j
	real(8), allocatable, private :: exponents(:)
	real(8), allocatable, private :: coefficients(:)
	character(5), private :: shellCode
	character(30), private :: bblockName
	character(30), private :: currentBlockName
	character(30), private :: currentBlockSymbol
	
	public :: &
		BasisSet_constructor, &
		BasisSet_destructor, &
		BasisSet_copyConstructor,&
		BasisSet_show, &
		BasisSet_showInCompactForm, &
		BasisSet_showInSimpleForm, &
		BasisSet_getNumberOfContractions, &
		BasisSet_getNumberOfPrimitives, &
		BasisSet_set, &
		BasisSet_setOrigin, &
		BasisSet_XMLParser_startElement, &
		BasisSet_XMLParser_endElement, &
		BasisSet_isInstanced
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine BasisSet_constructor( this )
		implicit none
		type(BasisSet), intent(inout) :: this
		
		this%name = ""
		this%length = 0
		this%ttype = 0
		this%isInstanced = .false.
		this%contractionLength =0
		this%numberOfPrimitives =0

	end subroutine BasisSet_constructor
	
	!**
	! @brief Define el constructor de copia para la clase
	!
	!**
	subroutine BasisSet_copyConstructor( this, otherThis )
		implicit none
		type(BasisSet), intent(inout) :: this
		type(BasisSet), intent(in) :: otherThis
		
		integer :: i
		
		if ( allocated(otherthis%contractions ) ) then
		
			this%name = trim(otherthis%name)
			this%length = otherthis%length
			this%ttype = otherthis%ttype
			this%isInstanced = otherthis%isInstanced
			this%numberOfPrimitives= otherthis%numberOfPrimitives
			
			allocate( this%contractions( size( otherthis%contractions ) ) )
			
			do i=1, this%length

				call ContractedGaussian_copyConstructor( this%contractions(i), otherthis%contractions(i))
				
			end do
			
		end if
		
	end subroutine BasisSet_copyConstructor
	
	
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine BasisSet_destructor( this )
		implicit none
		type(BasisSet), intent(inout) :: this
		
		integer :: i
		
		if( associated(thisPtr) ) nullify(thisPtr)
		
		do i = 1, size(this%contractions)
			
			call ContractedGaussian_destructor( this%contractions(i) )
		
		end do
		
		if ( allocated( this%contractions ) ) deallocate( this%contractions )
		
		this%name = ""
		this%length = 0
		this%ttype = 0
		this%isInstanced = .false.
		this%numberOfPrimitives = 0
		
	end subroutine BasisSet_destructor
	
		
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine BasisSet_show( this )
		implicit none
		type(BasisSet) , intent(in) :: this
		integer ::  itr_i, itr_j
		
			print *,""
			print *,"======="
			print *,"Basis Set: ", this%name, " - Type: ", this%ttype
			print *,""
	
		do itr_i =1, this%length
			
			write (6,300) this%name,itr_i
300			FORMAT ('Basis info by: ',A10,' - Contraction : ',I3.3)
			call ContractedGaussian_show( this%contractions(itr_i) )
			
			do itr_j=1,this%contractions(itr_i)%length
				
				print *,""
				write (6,200) this%name,itr_i,itr_j
200				FORMAT ('Basis info by: ',A10,' - Contraction: ',I3.3,' - Gaussiane: ',I3.2)
				call ContractedGaussian_show(this%contractions(itr_i),itr_j)
				print *,""
			
			end do
			print *,""
		end do
	
	end subroutine BasisSet_show
	
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine BasisSet_showInCompactForm( this, nameOfOwner )
		implicit none
		type(BasisSet) , intent(in) :: this
		character(*) ::nameOfOwner
		integer ::  i
		
		print *,""
		write(6,"(T5,A10,A11,A15)") trim(nameOfOwner)," BASIS SET: ", trim(this%name)

		do i =1, this%length
			call ContractedGaussian_showInCompactForm(this%contractions(i) )
		end do
	
	end subroutine BasisSet_showInCompactForm

	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine BasisSet_showInSimpleForm( this, nameOfOwner, unidOfOutput )
		implicit none
		type(BasisSet) , intent(in) :: this
		character(*) ::nameOfOwner
		integer :: unidOfOutput

		integer ::  i
		
		do i =1, this%length
			call ContractedGaussian_showInSimpleForm( this%contractions(i),unidOfOutput )
		end do
			
	end subroutine BasisSet_showInSimpleForm


	!**
	! Retorna el numero de gausiana primitivas dentro de las contracciones presentes
	!
	!**
	function BasisSet_getNumberOfContractions( this ) result( output )
		implicit none
		type(BasisSet) :: this
		integer :: output
		
				
		output = size( this%contractions )
		
		
	end function BasisSet_getNumberOfContractions


	!**
	! Retorna el numero de gausiana primitivas dentro de las contracciones presentes
	!
	!**
	function BasisSet_getNumberOfPrimitives( this ) result( output )
		implicit none
		type(BasisSet) :: this
		integer :: output
		
		integer :: i
		
		output=0
		do i = 1, size( this%contractions )
			 
			 output = output + ContractedGaussian_getLength( this%contractions(i) )
		end do 
		
		
	end function BasisSet_getNumberOfPrimitives



	
	!**
	! Ajusta algunos atributos de las contracciones asociadas a la funcion base
	!
	!**
	subroutine BasisSet_setOrigin( this, origin )
		implicit none
		type(BasisSet) :: this
		real(8) :: origin(3)
		
		integer :: i
		
		do i = 1, size( this%contractions )
			 
			 call ContractedGaussian_set( this%contractions(i), origin = origin )
		
		end do 
		
		
	end subroutine BasisSet_setOrigin
	
	!**
	! Ajusta el origen de las contracciones asociadas a la funcion base
	!
	!**
	subroutine BasisSet_set( this, origin, owner ) 
		implicit none
		type(BasisSet) :: this
		real(8), optional :: origin(3)
		integer, optional :: owner
		
		integer :: i
		
		do i = 1, this%length
			 
			 call ContractedGaussian_set( this%contractions(i), origin = origin, owner = owner )
		
		end do 
		
		
	end subroutine BasisSet_set
	
	
	subroutine BasisSet_XMLParser_startElement (this, tagName, dataName, dataValue, blockName)
		implicit none
		type(BasisSet), target, intent(inout) :: this
		character(*) :: tagName
		character(*) :: dataName
		character(*) :: dataValue
		character(*) :: blockName
		
		character(5) :: auxVal
		type(Exception) :: ex
		
		thisPtr=>null()
		thisPtr => this
		
		bblockName = trim(blockName)
		
		select case ( trim(tagName) ) 

			case ("Basis")
				
				select case ( trim(dataName) )
					
					case("name")
									
						thisPtr%name = trim(dataValue)
						
					case("type")
									
						thisPtr%ttype = INUM(dataValue)
						
				end select

			case ("Atom")
				
				select case ( trim(dataName) )
					
					case("numberContractions")
								
						if ( ( bblockName == currentBlockName ) .or. ( bblockName == currentBlockSymbol ) ) then
							
							if( allocated( thisPtr%contractions) ) deallocate( thisPtr%contractions )
							thisPtr%length = JNUM( dataValue )
							allocate( thisPtr%contractions(  thisPtr%length ) )
							iterContraction_j=1
							
						end if
					
					case("name")
						currentBlockName = trim(dataValue)
							
					case("symbol")
						currentBlockSymbol = trim(dataValue)	
						
				end select
				
			case ("ContractedGaussian")
			
				select case ( trim(dataName) )
					
					case("size")
									
						if( allocated( exponents ) ) then
							deallocate( exponents ) 
							deallocate( coefficients )
						end if 
						this%contractionLength = JNUM( dataValue)
						allocate( exponents( this%contractionLength ) )
						allocate( coefficients( this%contractionLength ) )
						iterContraction_i=1
					
					case("shellCode")
						
						shellCode = trim( dataValue )
					
				end select
			
			case ("PrimitiveGaussian")
			
				if( iterContraction_i <= this%contractionLength ) then
					select case ( trim(dataName) )
						
						case("exponent")
							exponents( iterContraction_i ) = DNUM(dataValue)
						
						case("coefficient")
							coefficients( iterContraction_i ) = DNUM(dataValue)
						
					end select

				else
					auxVal= String_convertIntegerToString(iterContraction_i+1)
					call Exception_constructor( ex , ERROR )
					call Exception_setDebugDescription( ex, "Class object Basis in the XMLParser_startElement function" )
					call Exception_setDescription( ex, "   The number of primitives for the contraction "//trim(auxVal)//"  of the "&
					//trim(bblockName)//" is not correct, check the XML file for the basis "//trim(thisPtr%name) )
					call Exception_show( ex )
					
				end if

		end select
		
		
	end subroutine BasisSet_XMLParser_startElement
	
	subroutine BasisSet_XMLParser_endElement( tagName)
		implicit none
		character(*) :: tagName
		
		type(Exception) :: ex
		character(30) :: auxSymbol
		integer :: auxVal
		
		if ( ( trim(bblockName) == trim(currentBlockName) ) .or. ( trim(bblockName) == trim(currentBlockSymbol) ) ) then

			 thisPtr%isInstanced = .true.
			 
			 auxSymbol =trim(currentBlockSymbol)
		
			select case ( trim(tagName) )
											
				case ("PrimitiveGaussian")
					iterContraction_i=iterContraction_i+1
					
				case ("ContractedGaussian")

					if ( iterContraction_j <= thisPtr%length ) then

						select case ( trim(shellCode) )
						
							case ("s")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=0_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("p")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=1_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("d")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=2_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("f")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=3_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("g")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=4_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("h")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=5_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("i")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=6_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("j")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=7_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("k")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=8_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1
							case ("l")
								call ContractedGaussian_constructor( thisPtr%contractions(iterContraction_j), exponents, coefficients, &
									angularMoment=9_8, name=auxSymbol )
								iterContraction_j=iterContraction_j+1

						end select
					else

						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object Basis in the XMLParser_endElement function" )
						call Exception_setDescription( ex, "     The number of contractions for the "//trim(bblockName)//&
								" is not correct, check the XML file      for the basis " //trim(thisPtr%name) )
						call Exception_show( ex )
						
					end if
				
				case ("Atom")
					
					
			end select
		
		end if

		if ( trim(tagName) == "Basis" .and. .not.thisPtr%isInstanced) then
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object Basis in the XMLParser_endElement function" )
			call Exception_setDescription( ex, "The basis set "//trim(thisPtr%name)//" for the " //  trim(bblockName) // " element,  was not found" )
			call Exception_show( ex )
		 
		end if
		
	end subroutine BasisSet_XMLParser_endElement
	
	!**
	! @brief Indica si el objeto ha sido instanciado o no
	!
	!**
	function BasisSet_isInstanced( this ) result( output )
		implicit  none
		type(BasisSet), intent(in) :: this
		logical :: output
		
		output = this%isInstanced
	
	end function BasisSet_isInstanced
	
end module BasisSet_
