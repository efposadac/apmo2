!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalezm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

module Matrix_Integer_
	use IFPORT
	use APMO_
	use Matrix_
	use Exception_
	use Vector_
	use LapackInterface_
	
	implicit none
	
	!>
	!! @brief Clase encargada de manipular matrices de enteros
	!!
	!! Esta clase manipula todo lo relacionado con matrices de tipo numerico
	!!
	!! @author Sergio Gonzalez
	!!
	!! <b> Fecha de creacion : </b> 2009-04-24
	!!   - <tt> 2009-04-24 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
	!!        -# Creacion del archivo y las funciones basicas
	!<	
	type, public :: Matrix_Integer
		integer , allocatable :: values(:,:)
	end type Matrix_Integer

	interface assignment(=)
		module procedure Matrix_Integer_copyConstructor
	end interface


	public :: &
		Matrix_Integer_constructor, &
		Matrix_Integer_copyConstructor, &
		Matrix_Integer_diagonalConstructor, &
		Matrix_Integer_randomElementsConstructor, &
		Matrix_Integer_destructor, &
		Matrix_Integer_show, &
		Matrix_Integer_getPtr, &
		Matrix_Integer_swapRows, &
		Matrix_Integer_swapColumns, &
		Matrix_Integer_getNumberOfRows, &
		Matrix_Integer_getNumberOfColumns, &
		Matrix_Integer_getElement, &
		Matrix_Integer_setElement, &
		Matrix_Integer_getColumn, &
		Matrix_Integer_getRow, &
		Matrix_Integer_setIdentity, &
		Matrix_Integer_setNull, &
		Matrix_Integer_getMax, &
		Matrix_Integer_getMin, &
		Matrix_Integer_getDeterminant, &
		Matrix_Integer_isNull, &
		Matrix_Integer_getTransPose, &
		Matrix_Integer_trace, &
		Matrix_Integer_plus, &
		Matrix_Integer_removeRow, &
		Matrix_Integer_removeColumn
		
	private :: &
		Matrix_Integer_exception
		
contains
	
	!>
	!! @brief Constructor
	!! Constructor por omisi�n
	!<
	subroutine Matrix_Integer_constructor( this, rows, cols, value )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: rows
		integer, intent(in) :: cols
		integer, optional, intent(in) :: value
		
		integer :: valueTmp
		
		valueTmp = 0
		if( present(value) ) valueTmp = value
		
		if (allocated(this.values)) deallocate(this.values)
		allocate( this.values( rows, cols ) )
		
		this.values = valueTmp
		
	end subroutine Matrix_Integer_constructor
	
	!>
	!! @brief Constructor de copia
	!! Reserva la memoria necesaria para this y le asigna los valores de otherMatrix_Integer
	!<
	subroutine Matrix_Integer_copyConstructor( this, otherMatrix_Integer )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		type(Matrix_Integer), intent(in) :: otherMatrix_Integer
		
		
		if ( allocated(  otherMatrix_Integer.values ) ) then

			if ( allocated(  this.values ) ) then
			
				if ( ( size(this.values, DIM=1) /= size(otherMatrix_Integer.values, DIM=1) ) .or. &
					(size(this.values, DIM=2) /= size(otherMatrix_Integer.values, DIM=2) )  ) then
			
					deallocate( this.values )
					allocate( this.values( size(otherMatrix_Integer.values, DIM=1), size(otherMatrix_Integer.values, DIM=2) ) )
					
				end if
					
			else
			
				allocate( this.values( size(otherMatrix_Integer.values, DIM=1), size(otherMatrix_Integer.values, DIM=2) ) )
			
			end if
			
			this.values = otherMatrix_Integer.values

		else

			call Matrix_Integer_exception(WARNING, "The original matrix wasn't allocated ", "Class object Matrix_Integer in the copyConstructor() function")

		end if
		


	end subroutine Matrix_Integer_copyConstructor
	
	!>
	!! @brief Constructor
	!!    Reserva la memoria necesaria para this y le asigna los valores del vector
	!!    diagonalVector a sus elementos de la diagonal y al resto cero
	!<
	subroutine Matrix_Integer_diagonalConstructor( this, diagonalVector )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		type(Vector), intent(in) :: diagonalVector
		
		integer :: i
		
		if( allocated( this.values ) ) deallocate( this.values )
		
		if( allocated( diagonalVector.values ) ) then
			
			allocate( this.values( size(diagonalVector.values), size(diagonalVector.values) ) )
			
			call Matrix_Integer_setIdentity( this )
			
			do i=1, size( this.values, DIM=1 )
				this.values(i, i) = diagonalVector.values(i)
			end do
			
		else

			call Matrix_Integer_exception( WARNING, "The original matrix wasn't allocated ", "Class object Matrix_Integer in the copyConstructor() function" )
			
		end if
		
	end subroutine Matrix_Integer_diagonalConstructor
	
	!>
	!! @brief Constructor
	!! Reserva la memoria necesaria para this y le asigna los valores aleatorios
	!! a sus elementos
	!!
	!! @param symmetric Si su valor es .true. genera una matriz simetrica, de lo
	!!                 contrario genera una matriz no simetrica
	!<
	subroutine Matrix_Integer_randomElementsConstructor( this, rows, cols, symmetric )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: rows
		integer, intent(in) :: cols
		logical, intent(in), optional :: symmetric
		
		integer :: value
		integer :: j
		integer :: i                  ! Counts random numbers
		integer(4) :: timeArray(3)    ! Holds the hour, minute, and second
		logical :: symmetricTmp
		
		if( allocated( this.values ) ) deallocate( this.values )
		
		symmetricTmp = .false.
		if( present(symmetric) ) symmetricTmp = symmetric
		
		call itime(timeArray)     ! Get the current time
		i = rand ( timeArray(1)+timeArray(2)+timeArray(3) )
		
		allocate( this.values( rows, cols ) )
		
		call Matrix_Integer_setIdentity( this )
		
		if( symmetricTmp ) then
		
			do i=1, rows
				do j=i, cols
					this.values(i, j) = irand(0)
					this.values(j, i) = this.values(i, j)
				end do
			end do
			
		else
			
			do i=1, rows
				do j=1, cols
					this.values(i, j) = irand(0)
				end do
			end do
			
		end if
		
	end subroutine Matrix_Integer_randomElementsConstructor
	
		
	!>
	!! @brief  Destructor
	!<
	subroutine Matrix_Integer_destructor( this )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		
		if( allocated(  this.values ) ) deallocate( this.values )
		
	end subroutine Matrix_Integer_destructor
	
	!>
	!! @brief  Imprime a salida estandar la matriz realizando cambio de linea
	!!              con un maximo de "APMO_instance.FORMAT_NUMBER_OF_COLUMNS" columnas
	!<
	subroutine Matrix_Integer_show( this, rowKeys, columnKeys, flags )
		implicit none
		type(Matrix_Integer), intent(in) :: this
		character(*), intent(in), optional :: rowKeys(:)
		character(*), intent(in), optional :: columnKeys(:)
		integer, intent(in), optional :: flags
		
		integer :: auxColNum
		integer :: columns
		integer :: rows
		integer :: i
		integer :: j
		integer :: k
		integer :: lowerLimit
		integer :: upperLimit
		integer :: tmpFlags
		
		tmpFlags = WITHOUT_KEYS
		if( present(flags) ) then
			tmpFlags = flags
		end if
		
		rows = size( this.values, DIM=1 )
		columns = size( this.values, DIM=2 )
				
		if( present( rowKeys ) ) then
			if( size( rowKeys ) < rows ) then
			
				call Matrix_Integer_exception(WARNING, "The size of row keys is low than number of matrix rows", &
					 "Class object Matrix_Integer in the show() function" )
				
			end if
		end if
		
		if( present( columnKeys ) ) then
			if( size( columnKeys ) < columns ) then
			
				call Matrix_Integer_exception(WARNING, "The size of column keys is low than number of matrix columns", &
					"Class object Matrix_Integer in the show() function" )
				
			end if
		end if
		
		do k=1, ceiling( (columns * 1.0)/(APMO_instance.FORMAT_NUMBER_OF_COLUMNS * 1.0 ) )
		
			lowerLimit = APMO_instance.FORMAT_NUMBER_OF_COLUMNS * ( k - 1 ) + 1
			upperLimit = APMO_instance.FORMAT_NUMBER_OF_COLUMNS * ( k )
			auxColNum = APMO_instance.FORMAT_NUMBER_OF_COLUMNS
			
			if ( upperLimit > columns ) then
				auxColNum =  APMO_instance.FORMAT_NUMBER_OF_COLUMNS -  upperLimit + columns
				upperLimit = columns
			end if
			
			if( present( columnKeys ) ) then

				if( tmpFlags == WITH_COLUMN_KEYS .or. tmpFlags == WITH_BOTH_KEYS ) then
					write (6,"(21X,<auxColNum>A15)") ( columnKeys(i), i = lowerLimit, upperLimit )
				end if
				
			else
			
				if( tmpFlags /= WITHOUT_KEYS ) then
					if( tmpFlags == WITH_COLUMN_KEYS .or. tmpFlags == WITH_BOTH_KEYS ) then
						write (6,"(5X,<auxColNum>I15)") ( i,i=lowerLimit,upperLimit )
					end if
				end if
				
			end if
				
			print *,""
			
			if( present( rowKeys ) ) then
			
				if( tmpFlags == WITH_ROW_KEYS .or. tmpFlags == WITH_BOTH_KEYS ) then
					write (6,"(A18,<auxColNum>I15)") ( rowKeys(i), ( this.values(i,j), j=lowerLimit,upperLimit ), i = 1, rows )
				else
					write (6,"(5X,<auxColNum>I15)") ( ( this.values(i,j), j=lowerLimit,upperLimit ), i = 1, rows )
				end if
				
			else
				if( tmpFlags /= WITHOUT_KEYS ) then
				
					if( ( tmpFlags == WITH_ROW_KEYS .or. tmpFlags == WITH_BOTH_KEYS ) .and. tmpFlags /= WITHOUT_KEYS ) then
						write (6,"(I5,<auxColNum>I15)") ( i, ( this.values(i,j), j=lowerLimit,upperLimit ), i = 1, rows )
					else
						write (6,"(5X,<auxColNum>I15)") ( ( this.values(i,j), j=lowerLimit,upperLimit ), i = 1, rows )
					end if
					
				else
				
					write (6,"(5X,<auxColNum>I15)") ( ( this.values(i,j), j=lowerLimit,upperLimit ), i = 1, rows )

				end if
			end if
			
			print *,""
			
		end do
		
	end subroutine Matrix_Integer_show
		
	!>
	!! @brief Devuelve un apuntador a la matrix solicitada
	!!
	!! @param this matrix de m x n
	!! @return Apuntador a la matriz solicitada.
	!! @todo No ha sido probada
	!<
	function Matrix_Integer_getPtr( this ) result( output )
		implicit none
		type(Matrix_Integer) , target , intent(in) :: this
		integer , pointer :: output(:,:)
		
		output => null()
		output => this.values
	
	end function Matrix_Integer_getPtr
	
	!>
	!! @brief  Intercambia las filas i y j
	!!
	!! @todo Falta implementar
	!<
	subroutine Matrix_Integer_swapRows( this, i, j )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j

	end subroutine Matrix_Integer_swapRows
	
	!>
	!! @brief  Intercambia las columnsa i y j
	!!
	!! @todo Falta implementar
	!<
	subroutine Matrix_Integer_swapColumns( this, i, j )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j
		
	end subroutine Matrix_Integer_swapColumns
	
	!>
	!! @brief  Retorna el n�mero de filas de la matriz
	!<
	function Matrix_Integer_getNumberOfRows( this ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer :: output
		
		output = size( this.values , DIM=1 )
		
	end function Matrix_Integer_getNumberOfRows
	
	!>
	!! @brief  Retorna el n�mero de columnas de la matriz
	!<
	function Matrix_Integer_getNumberOfColumns( this ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer :: output
		
		output = size( this.values , DIM=2 )
		
	end function Matrix_Integer_getNumberOfColumns
	
	!>
	!! @brief Retorna el elemento de la columna i-esima y la fila j-esima
	!<
	function Matrix_Integer_getElement( this, i, j ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j
		integer :: output
		
		output = this.values( i, j )
		
	end function Matrix_Integer_getElement
	
	!>
	!! @brief Selecciona el valor del elemento de la columna i-esima y la fila j-esima
	!<
	subroutine Matrix_Integer_setElement( this, i, j, value )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j
		integer, intent(in) :: value
		
		this.values( i, j ) = value
		
	end subroutine Matrix_Integer_setElement
	
	!>
	!! @brief Retorna un vector con los elementos de la columna i-esima
	!!
	! @todo Falta implementar
	!<
	function Matrix_Integer_getColumn( this, i ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		type(Vector) , pointer :: output(:)
		
		output => null()
!! 		output => this.values(:,n)

		!! A=M(:,n)//columnas
		
	end function Matrix_Integer_getColumn
	
	!>
	!! @brief  Retorna un vector con los elementos de la fila i-esima
	!!
	!! @todo Falta implementar
	!<
	function Matrix_Integer_getRow( this, i ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in) :: i
		type(Vector) , pointer :: output(:)
		
		output => null()
!! 		output => this.values(x,:)

		!! A=M(x,:)//filas
		
	end function Matrix_Integer_getRow
	
	!>
	!! @brief  Convierte la matriz en una matriz identidad
	!<
	subroutine Matrix_Integer_setIdentity( this )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		
		integer :: i
		integer :: rows
		
		this.values = 0.0_8
		rows = size( this.values , DIM=1 )
		
		do i=1, rows
			
			this.values(i,i) = 1
			
		end do
		
	end subroutine Matrix_Integer_setIdentity
	
	!>
	!! @brief Selecciona todos los valores de la matriz a cero
	!<
	subroutine Matrix_Integer_setNull( this )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		
		this.values = 0
		
	end subroutine Matrix_Integer_setNull
	
	!>
	!! Retorna el maximo valor encontrado dentro de la matriz
	!!
	!! @todo Falta implementar
	!<
	function Matrix_Integer_getMax( this, colPos, rowPos ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(inout), optional :: colPos
		integer, intent(inout), optional :: rowPos
		integer :: output
		
		colPos = 0
		rowPos = 0
		output = 0
		
	end function Matrix_Integer_getMax

	!>
	!! @brief  Retorna el minimo valor encontrado dentro de la matriz
	!!
	!! @todo Falta implementar
	!<
	function Matrix_Integer_getMin( this, colPos, rowPos ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(inout), optional :: colPos
		integer, intent(inout), optional :: rowPos
		integer :: output
		
		colPos = 0
		rowPos = 0
		output = 0
		
	end function Matrix_Integer_getMin

	!>
	!! @brief Retorna el determinante de la matriz
	!!
	!! @param flags Indica las propiedades adicionales de la matriz que
	!!              permite optimizar el calculo
	!! @todo Falta implementar
	!<
	function Matrix_Integer_getDeterminant( this, method, flags ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer, intent(in), optional :: method
		integer, intent(in), optional :: flags
		integer :: output
		
		output = 0
		
	end function Matrix_Integer_getDeterminant
	!>
	!! @brief  Retorna true si la matriz tiene todos sus elementos iguales
	!!               a cero, de lo contrario false
	!!
	!! @todo Falta implementar
	!<
	function Matrix_Integer_isNull( this ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		logical :: output
		
		output = .false.
		
	end function Matrix_Integer_isNull

	!>
	!! @brief Retorna la transpuesta de la matriz
	!!
	!! @todo Falta implementar
	!<
	function Matrix_Integer_getTransPose( this ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		integer :: output
		
		output = 0.0_8
		
	end function Matrix_Integer_getTransPose
	
	!>
	!! @brief  Retorna la traza de la matriz
	!<
	function Matrix_Integer_trace( this ) result ( output )
		implicit none
		type(Matrix_Integer) , intent(in) :: this
		integer :: output
		
		integer :: i
		
		output = 0
		
		do i = 1, size( this.values, DIM=1 )
		
			output = output + this.values( i, i )
			
		end do

	end function Matrix_Integer_trace
	
	!>
	!! @brief Suma dos matrices
	!!
	!! @todo Falta meter el soporte blas
	!<
	function Matrix_Integer_plus( this, otherMatrix_Integer ) result ( output )
		implicit none
		type(Matrix_Integer), intent(inout) :: this
		type(Matrix_Integer), intent(in) :: otherMatrix_Integer
		type(Matrix_Integer) :: output
		
#ifdef BLAS_MATRIX_SUPPORT
!! 		type(Vector) :: x
!! 		integer :: alpha
!! 		integer :: beta
!! 		integer :: matrixOrder
!! 		
!! 		call Vector_constructor( x, size(this.values, DIM=1), value=1.0_8 )
!! 		alpha = 1.0_8
!! 		beta = 0.0_8
!! 		matrixOrder = size( this.values, DIM=1 )
!! 		
!! 		call Matrix_Integer_constructor( output, size(this.values, DIM=1), size(otherMatrix_Integer.values, DIM=2) )
!! 		
!! 		call dsymv( &
!! 			UPPER_TRIANGLE_IS_STORED, &
!! 			matrixOrder, &
!! 			alpha, &
!! 			output.values, &
!! 			matrixOrder, &
!! 			x, &
!! 			1, &
!! 			beta, &
!! 			x, &
!! 			1 )
#else
		call Matrix_Integer_constructor( output, size(this.values, DIM=1), size(otherMatrix_Integer.values, DIM=2) )
		output.values = this.values + otherMatrix_Integer.values
#endif
		
	end function Matrix_Integer_plus


	!> 
	!! @brief Remueve la fila especificada de una matriz
	!<
	subroutine Matrix_Integer_removeRow( this, numberOfRow )
		implicit none
		type(Matrix_Integer) :: this
		integer, intent(in) :: numberOfRow

		integer, allocatable :: auxArray(:,:)
		integer :: rows
		integer :: columns

		rows = size( this.values, dim=1 )

		if (numberOfRow <= rows ) then
			
			columns = size( this.values, dim=2 )
		
			allocate( auxArray(rows-1,columns) )
			auxArray(1:numberOfRow-1,:) = this.values(1:numberOfRow-1,:)
			auxArray(numberOfRow:rows-1,:) = this.values(numberOfRow+1:rows,:)
			deallocate( this.values )
			allocate( this.values(rows-1,columns) )
			this.values = auxArray
			deallocate( auxArray )

		end if

	end subroutine Matrix_Integer_removeRow

	!> 
	!! @brief Remueve la columna especificada de una matriz
	!<
	subroutine Matrix_Integer_removeColumn( this, numberOfColumn )
		implicit none
		type(Matrix_Integer) :: this
		integer, intent(in) :: numberOfColumn

		integer, allocatable :: auxArray(:,:)
		integer :: rows
		integer :: columns

		columns = size( this.values, dim=2 )

		if (numberOfColumn <= columns ) then
		
			rows = size( this.values, dim=1 )	
			
		
			allocate( auxArray(rows,columns-1) )
			auxArray(:,1:numberOfColumn-1) = this.values(:,1:numberOfColumn-1)
			auxArray(:,numberOfColumn:columns-1) = this.values(:,numberOfColumn+1:columns)
			deallocate( this.values )
			allocate( this.values(rows,columns-1) )
			this.values = auxArray
			deallocate( auxArray )

		end if

	end subroutine Matrix_Integer_removeColumn

	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine Matrix_Integer_exception( typeMessage, description, debugDescription)
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
	end subroutine Matrix_Integer_exception
	
end module Matrix_Integer_
