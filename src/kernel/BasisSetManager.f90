!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de bases gausianas.
! 
!  Este modulo define una seudoclase para la construccion de funciones base expresadas 
!  como combinacion lineal de funciones gausianas primitiva.
!
!     \f[ \chi = \sum_{i = 1}^{K}{C_i {\phi}_i(r,\zeta,n)} \f]
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-02-07
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see FContractedGaussian, FPrimitiveGaussian
!
! @todo Implementar metodos para lectura y escritura de archivos de bases datos.
!**
module BasisSetManager_
	use BasisSet_
	use XMLParser_
	use APMO_
	
	implicit none
	
	type, public :: BasisSetManager
		character :: name
	end type BasisSetManager
	
	
	public :: 
	 	type(BasisSetManager), target :: BasisSetManager_instance
	
	logical, private :: instanced = .false.
	
	public  &
		BasisSetManager_constructor, &
		BasisSetManager_destructor, &
		BasisSetManager_loadBasisSet, &
		BasisSetManager_setOrigin, &
		BasisSetManager_set
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine BasisSetManager_constructor()
		implicit none
		
	end subroutine BasisSetManager_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine BasisSetManager_destructor()
		implicit none
	
	end subroutine BasisSetManager_destructor
	
	!**
	! Carga los atributos de una funcion base del respectivo archivo en formato XML
	!
	!**
	subroutine BasisSetManager_loadBasisSet ( basis, basisSetName, particleName )
		implicit none
		type(BasisSet), intent(inout) :: basis
		character(*) , intent(in) :: basisSetName
		character(*) , intent(in) :: particleName

		call BasisSet_constructor(basis)
		
		call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // &
			trim(APMO_instance.BASIS_SET_DATABASE) //trim(basisSetName) // ".xml"), &
			trim(particleName), basis=basis )
		
	end subroutine BasisSetManager_loadBasisSet
	
	!**
	! Ajusta el origen de la funcion base especificada
	!
	!**
	subroutine BasisSetManager_setOrigin( basis, origin )
		implicit none
		type(BasisSet) :: basis(:)
		real(8) :: origin(3)
		
		integer :: i
		
		do i = 1, size(basis)
			
			call BasisSet_setOrigin( basis(i), origin)
		
		end do
		
		
	end subroutine BasisSetManager_setOrigin
	
	!**
	! Ajusta el origen de la funcion base especificada
	!
	!**
	subroutine BasisSetManager_set( basis, origin, owner )
		implicit none
		type(BasisSet) :: basis(:)
		real(8), optional :: origin(3)
		integer, optional :: owner
		
		integer :: i 
		
		do i = 1, size(basis)
			
			call BasisSet_set( basis(i), origin = origin, owner = owner )
		
		end do
		
	end subroutine BasisSetManager_set
	
	
end module BasisSetManager_