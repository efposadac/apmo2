!!**********************************************************************************
!!    Copyright (C) 2008 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Permite almacenar IDs asociados a especies de particulas
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-26
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-08-26 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion del modulo
!
! @todo Definir estructuras como verdaderos vectores de punteros , ver VectorContratedGaussiansPtr
!**
module ParticlesIDs_
	use Particle_
		
	implicit none
	
	
	type, public :: ParticlesIDs
		integer, allocatable :: particleID(:)
	end type ParticlesIDs
	
	
	public :: &
		ParticlesIDs_constructor, &
		ParticlesIDs_destructor, &
		ParticlesIDs_add, &
		ParticlesIDs_remove, &
		ParticlesIDs_getSize

	contains
	
	!**
	! Define el metodo para el constructor de la clase.
	!
	!**
	subroutine ParticlesIDs_constructor( this )
		implicit none
		type(ParticlesIDs), intent(inout) :: this
		
		
	end subroutine ParticlesIDs_constructor
	
	!**
	! Define el metodo para el destructor de la clase.
	!
	!**
	subroutine ParticlesIDs_destructor(this)
		implicit none
		type(ParticlesIDs), intent(inout) :: this
		
		if ( allocated(this.particleID) ) 	deallocate( this.particleID )
			
	end subroutine ParticlesIDs_destructor
	
	!**
	! @ brief adiciona un ID al arreglo
	!
	!**
	subroutine ParticlesIDs_add(this, particleID )
		implicit none
		type(ParticlesIDs), intent(inout) :: this
		integer, intent(in) :: particleID
		
		type(ParticlesIDs) :: auxThis
		integer :: ssize
		integer :: i
		
		
		if ( allocated( this.particleID ) ) then
			
			ssize = size( this.particleID )
			allocate ( auxThis.particleID( ssize ) )
			
			do i = 1, ssize
				auxThis.particleID(i) = this.particleID(i)
			end do
			
			deallocate( this.particleID )
			allocate( this.particleID(ssize + 1 ) )
			
			!! Este ciclo puede ser evitado, revice la documentacion de fortran
			!! asociada con copia de arreglos de estructuras
			do i = 1, ssize
				this.particleID(i) = auxThis.particleID(i)
			end do
			
			deallocate( auxThis.particleID )
			
			this.particleID(ssize + 1) = particleID
		
		else
			
			allocate( this.particleID(1) )
			this.particleID(1) = particleID
			
		end if
			
	end subroutine ParticlesIDs_add
	
	!**
	! @brief Remueve un elemento del arreglo
	!
	! @todo Falta implemntacion completa
	!**
	subroutine ParticlesIDs_remove(this, particleID )
		implicit none
		type(ParticlesIDs), intent(inout) :: this
		integer, intent(in) :: particleID
		
			
	end subroutine ParticlesIDs_remove
	
	!**
	! @brief Retorna el numero de IDs almacenados
	!
	!**
	function ParticlesIDs_getSize(this ) result(output)
		implicit none
		type(ParticlesIDs), intent(in) :: this
		integer :: output
		
		output=size(this.particleID)
			
	end function ParticlesIDs_getSize

	

end module ParticlesIDs_
