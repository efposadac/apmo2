#!/bin/bash

F90_SOURCES=`ls *.f90`
C_SOURCES=`ls *.c`

rm -rf .deps
mkdir .deps

for FILE in $F90_SOURCES
do
	echo -n "Building dependencies for $FILE ... "
	FILENAME=${FILE%.*}
	./buildDeps.awk $FILE > .deps/$FILENAME.Po
	echo "OK"
done

for FILE in $C_SOURCES
do
	echo -n "Building dependencies for $FILE ... "
	FILENAME=${FILE%.*}
	echo "$FILENAME.o:" > .deps/$FILENAME.Po
	echo "OK"
done

EXIST_LINE=`grep "DEP_FILES_GQT " Makefile`

if [ -z  "$EXIST_LINE" ]
then
	echo -n "Updating Makefile ... "
	echo "DEP_FILES_GQT = \$(shell ls .deps/ | awk '{ printf(\".deps/%s \", \$\$0) } END{ printf(\"\n\") }')" >> Makefile
	echo "-include \$(DEP_FILES_GQT)" >> Makefile
	echo "OK"
fi
