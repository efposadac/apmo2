!!**********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, moment integrals, recursive integrals,        !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!** 
! @brief Modulo para calculo de integrales de momento angular
!
! Este modulo contiene los algoritmos necesarios para la evaluaci�n de integrales de 
! momento angular entre pares de funciones gaussianas primitivas (PrimitiveGaussian), 
! sin normalizar. 
!
! Las integrales son de la forma:
!
! \f[ (\bf{a} \mid \mathbf{L_{\mu}} \mid \bf{b}) = \int_{TE} {\varphi(\bf{r};{\zeta}_a,
! \mathbf{a,A})} {{\mathbf{L_{\mu}}} {\varphi(\bf{r};{\zeta}_b,\mathbf{b,B})}},dr \f]
!
! @author Sergio A. Gonz�lez M�nico
!
! <b> Fecha de creaci�n : </b> 2007-06-15
!   - <tt> 2007-03-09 </tt>: N�stor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Se adapt� al estandar de codificaci�n propuesto
!   - <tt> 2007-06-09 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
!        -# Se adicion� m�todo de integrales para momentos de primer orden
!
! @todo implentar integrales de moments mayores e iguales a 2.
!
! @warning  Las integrales de momento angulat no han sido probadas ni comparadas. 
!**
module AngularMomentIntegrals_
	use PrimitiveGaussian_
	use Exception_
	
	implicit none
	
	private 
		integer :: component
		real(8) :: originRc(3)
		real(8) :: commonFactor
		
		type(PrimitiveGaussian) :: primitives(2)
		type(PrimitiveGaussian) :: primitiveAxB
		
	public :: &
		PrimitiveGaussian_angularMomentIntegral
	
	private :: &
		AngularMomentIntegrals_ss
	
		
contains
	
	!**
	! Retorna el valor de la integral de momento de momento angular entre pares de funciones 
	! gausianas  sin normalizar, pasadas como par�metro.
	!
	! @param primitiveA Gausiana primitiva 
	! @param primitiveB Gausiana primitiva 
	! @param origin_Rc Origen alrededor del cual se calcula el momento.
	! @param component_ componente x, y o z del momento angular.
	!
	! @return Valor de la integral de momento angular
	!**
	function PrimitiveGaussian_angularMomentIntegral( primitiveA , primitiveB , origin_Rc , component_)  &
		result( output )
		implicit none
		type(PrimitiveGaussian) , intent( in ) :: primitiveA
		type(PrimitiveGaussian) , intent( in ) :: primitiveB
		real(8) , optional, intent( in ) :: origin_Rc( 3 )
		integer, optional, intent( in ) :: component_
		real( 8 ) :: output
		
		integer :: caseIntegral
		type(Exception) :: ex

		originRc=0.0_8
		if ( present(origin_Rc) ) originRc = origin_Rc
		
		component = 1
		if ( present(component_) ) component=component_

		
		!! Ordena la gausianas de entrada de mayor a menor momento
		call  PrimitiveGaussian_sort( primitives, primitiveA , primitiveB )

		!! Obtiene el producto de la dos gausianas de entrada
		primitiveAxB = PrimitiveGaussian_product(primitiveA,primitiveB)
		
		commonFactor= ( Math_PI/primitiveAxB.orbitalExponent )**1.5_8 * &
			PrimitiveGaussian_productConstant( primitives(1) , primitives(2) )
		
		!! Determina la integral que se debe usar
		caseIntegral = 	4 * PrimitiveGaussian_getAngularMoment( primitives(1) ) &
				+ PrimitiveGaussian_getAngularMoment( primitives(2) )


		select case ( caseIntegral )

		case(0) 
			
			output = AngularMomentIntegrals_ss( primitives(1) , primitives(2) )
		
!! 		case(4) 
!! 			
!! 			output = AngularMomentIntegrals_ps( primitives(1) , primitives(2) )
!! 		
!! 		case(5) 
!! 			
!! 			output = AngularMomentIntegrals_pp( primitives(1) , primitives(2) )
!! 			 
!! 		case(8)
!! 			output = AngularMomentIntegrals_ds( primitives(1) , primitives(2) )
!! 			end if
!! 		
!! 		case(9)
!! 			output = AngularMomentIntegrals_dp( primitives(1) , primitives(2) )
!! 		
!! 		case(10)
!! 			output = AngularMomentIntegrals_dd( primitives(1) , primitives(2) )
!! 			
		case default
			print *,""
			output = 0.0_8
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the angular moment function" )
			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
			call Exception_show( ex )
			
			 
		end select
		
		!!**************************************************************
		
	end function PrimitiveGaussian_angularMomentIntegral
		
	!**
	! Retorna el valor de la integral (s|L_u|s) para un par de funciones gaussianas 
	! sin normalizar, pasadas como par�metro.
	!
	! @param primitiveA Gausiana primitiva 
	! @param primitiveB Gausiana primitiva 
	!
	! @return Valor de la integral de momento angular.
	!**
	function AngularMomentIntegrals_ss( primitiveA , PrimitiveB ) result( output )
		implicit none
		type(PrimitiveGaussian) , intent(in) :: primitiveA , primitiveB 
		real(8) :: output
		
		real(8) :: xi
		real(8) :: orthogVector(3)

		if  ( (PrimitiveGaussian_getAngularMoment( primitiveA , primitiveB ) == 0)  ) then 

			xi= ( primitiveA.orbitalExponent *primitiveB.orbitalExponent ) / &
			( primitiveA.orbitalExponent  + primitiveB.orbitalExponent )

			!! Determina vector othogonal  (A-C) X (B - C)
			orthogVector=Math_crossProduct( ( primitiveA.origin - originRc ) , &
			( primitiveB.origin - originRc )  )

			!! Calcula integral de momento
			output = 2.0_8 * xi * ( orthogVector(component)  ) * &
				commonFactor
		else
			output=0.0_8
		end if
	end function AngularMomentIntegrals_ss
	
end module AngularMomentIntegrals_