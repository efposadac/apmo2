!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para definicion de datos de particulas elemetales
! 
!  Este modulo define una seudoclase para manejo de datos  correspondientes
!  a particulas elementales.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see XMLParser_
!
!**
module ConstantsOfCoupling_

	implicit none
	
	type , public :: ConstantsOfCoupling
		character(30) :: name
		character(30) :: symbol
		real(8) :: kappa
		real(8) :: eta
		real(8) :: lambda
		real(8) :: particlesFraction
		
		logical :: isInstanced	!< Dispuesta por razones de conveniencia
		
	end type ConstantsOfCoupling

	private ::
		type(ConstantsOfCoupling), pointer :: thisPtr
		character(20) :: bblockName
		character(20) :: currentBlockName
		character(20) :: currentBlockSymbol
	
	public :: &
		ConstantsOfCoupling_constructor, &
		ConstantsOfCoupling_destructor, &
		ConstantsOfCoupling_show, &
		ConstantsOfCoupling_XMLParser_startElement, &
		ConstantsOfCoupling_XMLParser_endElement
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine ConstantsOfCoupling_constructor(this)
		implicit none
		type(ConstantsOfCoupling) :: this
		
		this.name			=	""
		this.symbol		=	""
		this.kappa			=	0.0_8
		this.lambda		=	0.0_8
		this.eta			=	0.0_8
		this.particlesFraction	=	0.0_8
		this.isInstanced		=	.false.
		
	end subroutine ConstantsOfCoupling_constructor
	
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine ConstantsOfCoupling_destructor()
		implicit none
		
		if( associated(thisPtr) ) then
			
			thisPtr.isInstanced		=	.false.
			nullify(thisPtr)
		end if
		
	end subroutine ConstantsOfCoupling_destructor
	
		
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine ConstantsOfCoupling_show( this )
		implicit none
		type(ConstantsOfCoupling) , intent(in) :: this
		
		
		print *,""
		print *,"======================="
		print *,"  Constants Of Coupling"
		print *,"======================="
		print *,""
		write (6,"(T10,A20,A12)") "Name    = ",this.name
		write (6,"(T10,A20,A12)") "Symbol  = ",this.symbol
		write (6,"(T10,A20,F12.5)") "kappa    = ",this.kappa
		write (6,"(T10,A20,F12.5)") "eta  = ",this.eta
		write (6,"(T10,A20,F12.5)") "lambda    = ",this.lambda
		write (6,"(T10,A20,F12.5)") "particlesFraction = ",this.particlesFraction
		print *,""
		
	end subroutine ConstantsOfCoupling_show

	
	subroutine ConstantsOfCoupling_XMLParser_startElement (this, tagName, dataName, dataValue, blockName)
		implicit none
		type(ConstantsOfCoupling), target, intent(inout) :: this
		character(*) :: tagName
		character(*) :: dataName
		character(*) :: dataValue
		character(*) :: blockName
		
		logical :: aux = .false.
		
		if( associated( thisPtr) ) nullify(thisPtr)
		thisPtr => this
		
		select case ( trim(tagName) ) 

			case ("specie")
				
				select case ( trim(dataName) )
					
					case("name")
									
						currentBlockName = trim(dataValue)
						
					case("symbol")
									
						currentBlockSymbol = trim(dataValue)
						
				end select
		end select
		
		if ( ( trim( blockName ) == currentBlockName ) .or. ( trim( blockName ) == currentBlockSymbol ) ) then
		
			
			select case ( trim(tagName) )
			
				case ("specie")
				
					
					select case ( trim(dataName) )
						
						
						case("name")
							
							thisPtr.name = trim(dataValue)
						
						case("symbol")
								
							thisPtr.symbol = trim(dataValue)
							thisPtr.name = currentBlockName
							
					end select
	
				case ("constants")
				
					select case ( trim(dataName) )
						
						case("kappa")
										
							thisPtr.kappa = DNUM(dataValue)
							
						case("lambda")
										
							thisPtr.lambda= DNUM(dataValue)
							
						case("eta")
										
							thisPtr.eta = DNUM( dataValue )
							
						case("particlesFraction")
										
							thisPtr.particlesFraction = DNUM( dataValue )
							
					end select
	
			end select
		
		end if
		
	end subroutine ConstantsOfCoupling_XMLParser_startElement
	
	subroutine ConstantsOfCoupling_XMLParser_endElement( tagName)
		implicit none
		character(*) :: tagName
		
		select case ( trim(tagName) ) 

			case ("specie")
				
				currentBlockName = ""
				currentBlockSymbol = ""
				 
				 if ( thisPtr.name /= "")  then
				 	thisPtr.isInstanced = .true.
			  	end if
				
		end select
		
	end subroutine ConstantsOfCoupling_XMLParser_endElement
	
end module ConstantsOfCoupling_