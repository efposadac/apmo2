!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Clase estatica que contiene la interface para utilizar blas
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-26
!   - <tt> 2008-08-26 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas del nivel 1
!           tomadas de (http://www.netlib.org/blas/)
!**
module BlasInterface_
	
	implicit none
	
	interface BlasInterface
	
		!**
		! Scales a vector by a constant
		! x <- a*x
		!**
		subroutine dscal( N, SA, SX, INCX )
			real(8) :: SA
			integer :: INCX, N
			real(8) :: SX(*)
		end subroutine dscal
		
		!**
		! Constant times a vector plus a vector
		! y <- ax + b
		!**
		subroutine daxpy( N, DA, DX, INCX, DY, INCY )
			real(8) :: DA
			integer :: INCX, INCY, N
			real(8) :: DX(*), DY(*)
		end subroutine daxpy
		
		!**
		! Forms the dot product of two vectors
		! dot <- x^T*y
		!**
		function ddot( N, DX, INCX, DY, INCY ) result ( output )
			integer :: INCX, INCY, N
			real(8) :: DX(*), DY(*)
			real(8) :: output
		end function ddot
		
		!**
		! Returns the euclidean norm of a vector
		! norm <- sqrt(x^T*x)
		!**
		function dnrm2( N, X, INCX ) result ( output )
			integer :: INCX,N
			real(8) :: X(*)
			real(8) :: output
		end function dnrm2
		
		!**
		! Finds the index of element having max. absolute value
		! @todo El compilardor dice que esta funcion ya ha sido definida
		!**
!! 		function idamax( N, DX, INCX ) result ( output )
!! 			integer :: INCX,N
!! 			real(8) :: DX(*)
!! 			integer :: output
!! 		end function idamax
		
		!**
		! Forms the dot product of two complex vectors
		! dot <- x^T*y
		!**
		function zdotc( N, ZX, INCX, ZY, INCY ) result ( output )
			integer :: INCX,INCY,N
			complex(8) :: ZX(*),ZY(*)
			complex(8) :: output
		end function zdotc
		
		!**
		! Returns the euclidean norm of a complex vector
		! norm <- sqrt(x^T*x)
		!**
		function dznrm2( N, X, INCX ) result ( output )
			integer :: INCX,N
			complex(8) :: X(*)
			real(8) :: output
		end function dznrm2
		
	end interface BlasInterface
	
end module BlasInterface_
