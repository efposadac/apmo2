!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para deficion de parser XML en Fortran
! 
!  Este modulo define una seudoclase  que implementa un parser para
!  obtener informacion de atributos de archivos escritos en formato XML
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-01
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
!**
module XMLParser_
	use , intrinsic ::ISO_C_BINDING
	use BasisSet_
	use AtomicElement_
	use ElementalParticle_
	use ConstantsOfCoupling_
	use ExternalPotential_
	use Exception_
		
	implicit none
	
	
 	type(BasisSet), pointer, private :: basisSetPointer
 	type(AtomicElement), pointer, private :: atomicElementPointer
	type(ElementalParticle), pointer, private :: elementalParticlePointer
	type(ConstantsOfCoupling), pointer, private :: constantsOfCouplingPointer
	type(ExternalPotential), pointer, private :: externalPotentialPointer
	character(50), private :: className
	character(50), private :: bblockName
		
		type(Exception) :: ex
		
	public :: &
		XMLParser_constructor, &
		XMLParser_destructor, &
		XMLParser_startElement, &
		XMLParser_endElement
	
	interface
		subroutine XMLPARSERBASE_PARSEFILE( fileName)
			!DEC$ ATTRIBUTES C :: XMLParserBase_parseFile
			!DEC$ ATTRIBUTES REFERENCE :: fileName

			implicit none
			character(*) :: fileName
		end subroutine XMLPARSERBASE_PARSEFILE
		
		
	end interface
	
	
		
contains
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine XMLParser_constructor( fileName, blockName, basis, element, pparticle,&
	        couplingConstants, eexternalPotential )
		implicit none
		character(*) :: fileName
		character(*) :: blockName
		type(BasisSet), optional, target :: basis
		type(AtomicElement), optional, target :: element
		type(ElementalParticle), optional, target :: pparticle
		type(ConstantsOfCoupling), optional, target :: couplingConstants
		type(ExternalPotential), optional, target :: eexternalPotential
		
		if ( present( basis) )  then
			className = "basisSet"
			basisSetPointer => basis
			
		end if
		
		
		if ( present( element ) )  then
			className = "element"
			atomicElementPointer => element
			
		end if
		
		if ( present( pparticle ) )  then
			className = "particle"
			elementalParticlePointer => pparticle
			
		end if
		
		if ( present( couplingConstants ) )  then
			className = "constantsOfCoupling"
			constantsOfCouplingPointer => couplingConstants
			
		end if
		
		if ( present( eexternalPotential ) )  then
			className = "externalPotential"
			externalPotentialPointer => eexternalPotential
			
		end if

		
		bblockName = trim( blockName )
		
		call XMLParserBase_parseFile( trim(fileName)//"#" )
		
		
	end subroutine XMLParser_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine XMLParser_destructor()
		implicit none
		
		if (associated( basisSetPointer ) ) nullify( basisSetPointer )
		if (associated( atomicElementPointer ) ) nullify( atomicElementPointer )
		if (associated( elementalParticlePointer ) ) nullify( elementalParticlePointer )
		if (associated( constantsOfCouplingPointer ) ) nullify( constantsOfCouplingPointer )
                if (associated( externalPotentialPointer ) ) nullify( externalPotentialPointer )	
	end subroutine XMLParser_destructor
	
	!**
	! Ejecuta una accion al encontrar un string asociado a un atributo
	! especificado en un archivo en formato XML
	!
	!**
	subroutine XMLParser_startElement ( tagName, dataName, dataValue)
		implicit none
		character(30) :: tagName
		character(30) :: dataName
		character(30) :: dataValue
		
		select case ( trim(className) )
			
			case("basisSet")
				
				call BasisSet_XMLParser_startElement(basisSetPointer, trim(tagName), trim(dataName), trim(dataValue),&
				      bblockName )
	
				
			case("element")
				
				call AtomicElement_XMLParser_startElement(atomicElementPointer, trim(tagName), trim(dataName),&
				      trim(dataValue), bblockName )

				
			case("particle")
				
				call ElementalParticle_XMLParser_startElement(elementalParticlePointer, trim(tagName), trim(dataName),&
				      trim(dataValue), bblockName )
				
			case("constantsOfCoupling")
				
				call ConstantsOfCoupling_XMLParser_startElement(constantsOfCouplingPointer, trim(tagName),&
				      trim(dataName), trim(dataValue), bblockName )
			
			case("externalPotential")
				
				call ExternalPotential_XMLParser_startElement(externalPotentialPointer, trim(tagName), trim(dataName), &
					trim(dataValue), bblockName )
			
			
			
			case default
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object XMLParser_in the XMLParser_startElement function" )
				call Exception_setDescription( ex, "Database type: " // trim(className) //  ", not found" )
				call Exception_show( ex )
				
		end select
		
	end subroutine XMLParser_startElement
	
	
	!**
	! Ejecuta una accion al encotrar el final de una elemento asociado a un atributo
	! especificado en un archivo en formato XML
	!
	!**
	subroutine XMLParser_endElement ( tagName )
		implicit none
		character(30) :: tagName
		
		select case ( trim(className) )
			
			case("basisSet")
				
				call BasisSet_XMLParser_endElement( trim(tagName) )
			
			case("element")
				
				call AtomicElement_XMLParser_endElement( trim(tagName) )
				
			case("particle")
				
				call ElementalParticle_XMLParser_endElement( trim(tagName) )
				
			case("constantsOfCoupling")

				call ConstantsOfCoupling_XMLParser_endElement( trim(tagName) )
				
			case("externalPotential")

				call ExternalPotential_XMLParser_endElement( trim(tagName) )
			
			case default
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object XMLParser_in the XMLParser_endElement function" )
				call Exception_setDescription( ex, "Database type: " // trim(className) //  ", not found" )
				call Exception_show( ex )
					
		end select
		
		
	end subroutine XMLParser_endElement
	
	
	
	
end module XMLParser_
