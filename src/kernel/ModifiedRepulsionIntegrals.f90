!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module ModifiedRepulsionIntegrals_
	use PrimitiveGaussian_
	use Exception_
	use Math_

	implicit none

	!>
	!! @brief Description
	!!
	!! @author Fernando Posada
	!!
	!! <b> Creation data : </b> 10-21-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 10-21-10 </tt>:  Fernando Posada ( efposadac@unal.edu.co )
	!!        -# creacion del modulo
	!!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
	!!        -# description
	!!
	!<
	type, public :: ModifiedRepulsionIntegrals
		character(20) :: name
		logical :: isInstanced
	end type

	public :: &
		ModifiedRepulsionIntegrals_constructor, &
		ModifiedRepulsionIntegrals_destructor, &
		ModifiedRepulsionIntegrals_compute, &
		ModifiedRepulsionIntegrals_show
		
private		
contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine ModifiedRepulsionIntegrals_constructor(this)
		implicit none
		type(ModifiedRepulsionIntegrals) :: this

		this.isInstanced = .true.

	end subroutine ModifiedRepulsionIntegrals_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine ModifiedRepulsionIntegrals_destructor(this)
		implicit none
		type(ModifiedRepulsionIntegrals) :: this

		this.isInstanced = .false.

	end subroutine ModifiedRepulsionIntegrals_destructor

	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	subroutine ModifiedRepulsionIntegrals_show(this)
		implicit none
		type(ModifiedRepulsionIntegrals) :: this
	end subroutine ModifiedRepulsionIntegrals_show

	!<
	!! Calcula integrales de repulsion ERIS con un termino adicional (GTG) de la forma (ab|exp(-gammar12)|cd)
	!! Unicamente implementada la (ss|O0|ss)
	!! @Warning Falta implementar integrales para mayor momento angular
	!! @Literature Valery Weber, Claude Daul, Computer Physics communications, 158(2004)1-11
	!>

	function ModifiedRepulsionIntegrals_compute(primitiveGaussianA, primitiveGaussianB, &
												primitiveGaussianC, primitiveGaussianD, &
												primitiveGaussianR12) result(output)
		implicit none

		type(primitiveGaussian) :: primitiveGaussianA
		type(primitiveGaussian) :: primitiveGaussianB
		type(primitiveGaussian) :: primitiveGaussianC
		type(primitiveGaussian) :: primitiveGaussianD
		type(primitiveGaussian) :: primitiveGaussianR12
		real(8) :: output

		type(primitiveGaussian) :: primitiveGaussianAxB
		type(primitiveGaussian) :: primitiveGaussianCxD
		type(primitiveGaussian) :: primitiveGaussianAxBxCxD
		real(8) :: alpha
		real(8) :: AB(3), CD(3), PQ(3)
		real(8) :: AB2, CD2, PQ2

		type(Exception) :: ex


		if (sum(primitiveGaussianA%angularMomentIndex) == 0 .and. sum(primitiveGaussianB%angularMomentIndex) == 0 .and. &
			sum(primitiveGaussianC%angularMomentIndex) == 0 .and. sum(primitiveGaussianD%angularMomentIndex) == 0 .and. &
			sum(primitiveGaussianR12%angularMomentIndex) == 0) then

			!! (00|O_{0}|00) funcion auxiliar

			!!Producto entre gaussianas A y B
			primitiveGaussianAxB = primitiveGaussian_product(primitiveGaussianA, primitiveGaussianB)

			!!Producto entre gaussianas C y D
			primitiveGaussianCxD =  primitiveGaussian_product(primitiveGaussianC, primitiveGaussianD)

			!!Exponente reducido alpha
			alpha = (primitiveGaussianAxB%orbitalExponent * primitiveGaussianAxB%orbitalExponent) /&
					(primitiveGaussianAxB%orbitalExponent + primitiveGaussianAxB%orbitalExponent)

			!!Cantidades geometricas
			AB = primitiveGaussianA%origin - primitiveGaussianB%origin
			CD = primitiveGaussianC%origin - primitiveGaussianD%origin
			PQ = primitiveGaussianAxB%origin - primitiveGaussianCxD%origin

			AB2 = dot_product(AB, AB)
        	CD2 = dot_product(CD, CD)
			PQ2 = dot_product(PQ, PQ)

			!!Computamos la Integral
			output = ((Math_PI**2)/ (primitiveGaussianAxB%orbitalExponent * primitiveGaussianAxB%orbitalExponent))**1.5_8 &
					*(alpha/(alpha + primitiveGaussianR12%orbitalExponent))**1.5_8 &
					* exp(-((primitiveGaussianA%orbitalExponent * primitiveGaussianB%orbitalExponent) &
					/ primitiveGaussianAxB%orbitalExponent) * AB2) &
					* exp(-((primitiveGaussianC%orbitalExponent * primitiveGaussianD%orbitalExponent) &
					/ primitiveGaussianCxD%orbitalExponent) * CD2) &
					* exp(-((alpha * primitiveGaussianR12%orbitalExponent) &
					/ (alpha + primitiveGaussianR12%orbitalExponent)) * PQ2)
		else

		call ModifiedRepulsionIntegrals_exception( ERROR, "in ModifiedRepulsionIntegrals_compute function",&
												   "Only S angular moment its implemented")
		end if

	end function ModifiedRepulsionIntegrals_compute

	!!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function ModifiedRepulsionIntegrals_isInstanced( this ) result( output )
		implicit  none
		type(ModifiedRepulsionIntegrals), intent(in) :: this
		logical :: output
		
		output = this.isInstanced
	
	end function ModifiedRepulsionIntegrals_isInstanced

	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine ModifiedRepulsionIntegrals_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription
	
		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )
	
	end subroutine ModifiedRepulsionIntegrals_exception

end module ModifiedRepulsionIntegrals_
