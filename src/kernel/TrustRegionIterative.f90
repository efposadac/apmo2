!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module TrustRegionIterative_
	use APMO_
	use Matrix_
	use Vector_
	use Exception_
	use Math_
	implicit none
	
	!>
	!! @brief Clase encargada de minizar funciones multidimesionales mediante
	!!		metodo de radio de confianza.
	!!
	!! @author Sergio A. Gonzalez-Monico
	!!
	!! <b> Fecha de creacion : </b> 2009-06-11
	!!   - <tt> 2009-06-11 </tt>: Sergio A. Gonzalez-Monico ( sagonzalezm@unal.edu.co )
	!!        -# Creacion del archivo y las funciones basicas
	!<	
	type, public :: TrustRegionIterative
		character(20) :: name
		integer :: outputUnid
		integer :: numberOfVariables
		integer :: realNumberOfIteration
		integer :: numberOfIteration
		integer :: maximumNumberOfIterations
		type(Vector)  :: variables
		type(Vector)  :: gradient
		type(Vector)  :: oldVariables
		type(Vector)  :: oldGradient
		type(Vector)  :: step
		type(Vector)  :: gradientProjectedOnExtDegrees
		type(Vector)  :: gradientProjectedOnHessiane
		type(Matrix) :: hessiane
		type(Matrix) :: hessianeProjected
		real(8) :: functionValue
		real(8) :: oldFunctionValue
		real(8) :: trustRadius
		real(8) :: lagrangeMultiplier
		real(8) :: squareStepSize
		real(8) :: optimizationTolerance
		real(8) :: predictedChangeOfFunction
		logical :: hasConverged
		logical :: isHardCase
		logical :: isSuitableStep
		logical :: hasRestarted
	end type
	
	!< enum Vector_printFormatFlags {
	real(8), parameter :: MAXIMUM_TRUST_RADIO = 0.3_8
	real(8), private, parameter :: MINIMUM_TRUST_RADIO = 0.001_8
	real(8), private, parameter :: ZERO_BOUND = 1.0D-12
	!< }


	public :: &
		TrustRegionIterative_constructor, &
		TrustRegionIterative_destructor, &
		TrustRegionIterative_getHessiane,&
		TrustRegionIterative_getGradient, &
		TrustRegionIterative_getNumberOfIterations,&
		TrustRegionIterative_getRealNumberOfIterations,&
		TrustRegionIterative_setInitialHessian, &
		TrustRegionIterative_setMaximumIterations, &
		TrustRegionIterative_iterate, &
		TrustRegionIterative_isMinimum, &
		TrustRegionIterative_restart
		

private
contains

	subroutine TrustRegionIterative_constructor( this, initialPoint )
		implicit none
		type(TrustRegionIterative) :: this
		real(8) :: initialPoint(:)
		
				
		this.numberOfVariables = size( initialPoint)	
		call Vector_constructor( this.variables, this.numberOfVariables, 0.0_8 )
		call Vector_constructor( this.oldVariables, this.numberOfVariables, 0.0_8 )
		this.variables.values = initialPoint
		call Vector_constructor( this.gradient, this.numberOfVariables, 0.0_8 )
		call Vector_constructor( this.oldGradient, this.numberOfVariables, 0.0_8 )
		call Vector_constructor( this.step, this.numberOfVariables, 0.0_8 )
		call Vector_constructor( this.gradientProjectedOnExtDegrees, this.numberOfVariables, 0.0_8 )
		call Vector_constructor( this.gradientProjectedOnHessiane, this.numberOfVariables, 0.0_8 )
		call Matrix_constructor( this.hessiane, int(this.numberOfVariables,8), int(this.numberOfVariables,8), 0.0_8 )
		call Matrix_constructor( this.hessianeProjected, int(this.numberOfVariables,8), int(this.numberOfVariables,8), 0.0_8 )
		this.trustRadius = 0.1_8
		this.optimizationTolerance = APMO_instance.MINIMIZATION_TOLERANCE_GRADIENT
		this.realNumberOfIteration = 0
		this.numberOfIteration = 0
		this.maximumNumberOfIterations = APMO_instance.MINIMIZATION_MAX_ITERATION
		this.hasConverged = .false.
		this.isSuitableStep = .false.
		this.hasRestarted = .false.
		this.outputUnid = APMO_instance.UNID_FOR_OUTPUT_FILE
		
	end subroutine TrustRegionIterative_constructor

	subroutine TrustRegionIterative_destructor( this)
		implicit none
		type(TrustRegionIterative) :: this
		
		call Vector_destructor( this.variables )
		call Vector_destructor( this.oldVariables )
		call Vector_destructor( this.gradient )
		call Vector_destructor( this.oldGradient )
		call Vector_destructor( this.step )
		call Vector_destructor( this.gradientProjectedOnExtDegrees )
		call Vector_destructor( this.gradientProjectedOnHessiane )
		call Matrix_destructor( this.hessiane)
		call Matrix_destructor( this.hessianeProjected)
		this.trustRadius = 0.1_8
		this.realNumberOfIteration = 0
		this. numberOfIteration = 0
		this.numberOfVariables = 0
		this.hasConverged = .false.
		this.isSuitableStep = .false.
		this.hasRestarted = .false.
		
	end subroutine TrustRegionIterative_destructor
	
	function TrustRegionIterative_getHessiane( this ) result(output)
		implicit none
		type(TrustRegionIterative) :: this
		type(Matrix) :: output
		
		output = this.hessiane
		
	end function TrustRegionIterative_getHessiane
	
	function TrustRegionIterative_getGradient( this ) result(output)
		implicit none
		type(TrustRegionIterative) :: this
		real(8) :: output
		
		output = sqrt(dot_product(this.gradient.values,this.gradient.values)/this.numberOfVariables)
		
	end function TrustRegionIterative_getGradient



	!>
	!! @brief Retorna el numero de iteraciones realizadas hasta el momento
	!<
	function TrustRegionIterative_getNumberOfIterations( this ) result(output)
		implicit none
		type(TrustRegionIterative) :: this
		integer :: output
		
		output = this.numberOfIteration
		
	end function TrustRegionIterative_getNumberOfIterations
	
	!>
	!! @brief Retorna el numero de iteraciones realizadas hasta el momento
	!<
	function TrustRegionIterative_getRealNumberOfIterations( this ) result(output)
		implicit none
		type(TrustRegionIterative) :: this
		integer :: output
		
		output = this.realNumberOfIteration
		
	end function TrustRegionIterative_getRealNumberOfIterations
	
	function TrustRegionIterative_isMinimum( this ) result(output)
		implicit none
		type(TrustRegionIterative) :: this
		logical :: output
		
		call TrustRegionIterative_checkOptimizationCriteria( this )
		output = this.hasConverged
		
	end function TrustRegionIterative_isMinimum
	
	
	
	subroutine TrustRegionIterative_setInitialHessian( this, initialHessianMatrix)
		implicit none
		type(TrustRegionIterative) :: this
		real(8) :: initialHessianMatrix(:,:)
		
		this.hessiane.values = initialHessianMatrix
		
	end subroutine TrustRegionIterative_setInitialHessian
	
	subroutine TrustRegionIterative_setMaximumIterations( this, numberOfIterations)
		implicit none
		type(TrustRegionIterative) :: this
		integer :: numberOfIterations
		
		this.maximumNumberOfIterations = numberOfIterations
		
	end subroutine TrustRegionIterative_setMaximumIterations

	
	subroutine TrustRegionIterative_iterate( this, functionValue, gradientOfFunction, &
		projectGradientFunction, projectHessianeFunction, showFunction )
		implicit none
		type(TrustRegionIterative) :: this
		
		interface
			
			function functionValue( pointOfEvaluation, iterator ) result( output )
				real(8) :: pointOfEvaluation(:)
				integer, optional :: iterator
				real(8) :: output
			end function functionValue
			
			subroutine gradientOfFunction( pointOfEvaluation, gradient )
				real(8) :: pointOfEvaluation(:)
				real(8) :: gradient(:)
			end subroutine gradientOfFunction
			
			subroutine projectGradientFunction( gradient, gradientProjectedOnExtDegrees )
				real(8) :: gradient(:)
				real(8) :: gradientProjectedOnExtDegrees(:)
			end subroutine projectGradientFunction
			
			subroutine projectHessianeFunction( hessiane,hessianeProjected )
				real(8) :: hessiane(:,:)
				real(8) :: hessianeProjected(:,:)
			end subroutine projectHessianeFunction
			
			subroutine showFunction( pointOfEvaluation, gradient, functionValue )
				real(8) :: pointOfEvaluation(:)
				real(8) :: gradient(:)
  				real(8) :: functionValue
			end subroutine showFunction

		end interface
		
		if ( this.realNumberOfIteration == 0  ) then
			
			!! Acota el radio de confianza inicial
			this.trustRadius = min(MAXIMUM_TRUST_RADIO,this.trustRadius)
			!! Calcula el valor de la funcion y su gradiente en el punto inicial
			this.functionValue= functionValue( this.variables.values )
			call gradientOfFunction(this.variables.values, this.gradient.values )
			this.hasRestarted=.false.
			!! Muestra el estimado actual de la minimizacion
			write (this.outputUnid,"(T10,A21,I5)") "NUMBER OF ITERATION: ", 0
			write(this.outputUnid,"(T10,A20)") "===================="
			write(this.outputUnid,"(A1)") " "
			call showFunction( this.variables.values, this.gradient.values, this.functionValue )
			write(this.outputUnid,"(T10,A19,F12.7)") "RMS GRADIENT     = ",TrustRegionIterative_getGradient(this)
			write(this.outputUnid,"(A1)") " "

		end if

		if ( this.realNumberOfIteration == 0 .or. this.hasRestarted ) this.trustRadius = min(MAXIMUM_TRUST_RADIO,this.trustRadius)

		
		this.realNumberOfIteration = this.realNumberOfIteration + 1
		this.numberOfIteration = this.numberOfIteration +1
		
		write (this.outputUnid,"(T10,A21,I5)") "NUMBER OF ITERATION: ", this.realNumberOfIteration
		write(this.outputUnid,"(T10,A20)") "===================="
		write(this.outputUnid,"(A1)") " "

		
		if ( this.realNumberOfIteration <= this.maximumNumberOfIterations ) then
					
				
			!! Almacena valores actuales de la funcion, su gradiente y el punto sobre la superficie
			this.oldFunctionValue   = this.functionValue
			this.oldGradient.values = this.gradient.values
			this.oldVariables.values = this.variables.values
				
			this.isSuitableStep=.false.	
			!! Realiza calculo de paso de optimizacion hasta que alcance un valor adecuado
			do while ( .not.this.isSuitableStep )
			
				!! Proyecta los grados de libetad externos del gradiente
				call projectGradientFunction( this.gradient.values, this.gradientProjectedOnExtDegrees.values )
					
				!! Proyecta los grados de libetad externos de la hesiana
				call projectHessianeFunction( this.hessiane.values, this.hessianeProjected.values )

						
				
				!! Calcula la nueva direccion de busqueda
				call TrustRegionIterative_calculateStep(this)
				
				!! Ejecuta el paso
				this.variables.values=this.variables.values + this.step.values
				
				!! Calcula el valor de la funcion y el gradiente
				this.functionValue = functionValue( this.variables.values )
				call gradientOfFunction( this.variables.values, this.gradient.values )
				
				!! Calcula una prediccion del cambio de energia
				call TrustRegionIterative_predictedChangeOfFunction( this )

				!! Actualiza el valor del radio de confianza
				call TrustRegionIterative_updateTrustRadio( this )

				!! Actualiza la hesiana
				call TrustRegionIterative_updateHessiane( this )

				!! Mantiene el valor de gradiente y variables si el paso no es aducuado
				if ( .not.this.isSuitableStep ) then
					this.gradient = this.oldGradient
					this.variables = this.oldVariables
				end if
			
			end do
			
			!! Muestra el estimado actual de la minimizacion
			call showFunction( this.variables.values, this.gradient.values, this.functionValue )
				
		else

			call TrustRegionIterative_exception( ERROR, "The maximum number of iterations was exceded", &
				"Class object TrustRegionIterative in iterate() function")
		
		end if

	end subroutine TrustRegionIterative_iterate
	
	subroutine TrustRegionIterative_checkOptimizationCriteria(this)
		implicit none
		type(TrustRegionIterative) :: this
		
		real(8) :: rmsOfGradient
		real(8) :: rmsOfDisplacement
		real(8) :: maximumGradient
		real(8) :: maximumDisplacement
		real(8) :: thresholdForRmsGradient
		real(8) :: thresholdForRmsDisplacement
		real(8) :: thresholdForSingleGradient
		real(8) :: thresholdForSingleDisplacement
		real(8) :: thresholdForFunctionChange
		real(8) :: changeOfFunction
		logical :: isRmsOfGradientConverged
		logical :: isRmsOfDisplacementConverged
		logical :: isMaximumGradientConverged
		logical :: isMaximumDisplacementConverged
		logical :: isFunctionChangeConverged
		character(4) :: stateOfConvergenceOfRmsGradient
		character(4) :: stateOfConvergenceOfRmsDisplacement
		character(4) :: stateOfConvergenceOfSingleGradient
		character(4) :: stateOfConvergenceOfSingleDisplacement
		character(4) :: stateOfConvergenceOfFunctionChange
		
		!!******************************************************
		!! define los umbrales de convergencia para gradiente y desplazamiento
		thresholdForSingleGradient = 5.0*this.optimizationTolerance
		thresholdForRmsGradient = 1.0*this.optimizationTolerance
		thresholdForSingleDisplacement = 6.0*this.optimizationTolerance
		thresholdForRmsDisplacement = 4.0*this.optimizationTolerance
		thresholdForFunctionChange = 1.0D-5
		!!
		!!******************************************************
		
		
		!!*******************************************************
		!! Determina convergencia en gradiente RMS
		!!****
		isRmsOfGradientConverged=.false.
		stateOfConvergenceOfRmsGradient = "--"
		rmsOfGradient = sqrt(dot_product(this.gradient.values,this.gradient.values)/this.numberOfVariables)
		if (rmsOfGradient <= thresholdForRmsGradient) then
			stateOfConvergenceOfRmsGradient = "OK  "
			isRmsOfGradientConverged=.true.
		end if
		!!
		!!*******************************************************
		
		!!*******************************************************
		!! Determina convergencia en desplazamiento RMS
		!!****
		isRmsOfDisplacementConverged =.false.
		stateOfConvergenceOfRmsDisplacement = "--  "
		rmsOfDisplacement = sqrt(dot_product(this.step.values,this.step.values)/this.numberOfVariables)
		if (rmsOfDisplacement <= thresholdForRmsDisplacement) then
			stateOfConvergenceOfRmsDisplacement = "OK  "
			isRmsOfDisplacementConverged =.true.
		end if
		!!
		!!*******************************************************

		!!*******************************************************
		!! Determina convergencia para la componente mas grande del gradiente
		!!****
		isMaximumGradientConverged=.false.
		stateOfConvergenceOfSingleGradient = "--  "
		maximumGradient = max(maxval(this.gradient.values),abs(minval(this.gradient.values)))
		if ( maximumGradient <= thresholdForSingleGradient ) then
		stateOfConvergenceOfSingleGradient = "OK  "
			isMaximumGradientConverged=.true.
		end if
		!!
		!!*******************************************************
		
		!!*******************************************************
		!! Determina convergencia para la componente mas grande del desplazamiento
		!!****
		isMaximumDisplacementConverged =.false.
		stateOfConvergenceOfSingleDisplacement="--  "
		maximumDisplacement = max(maxval(this.step.values),abs(minval(this.step.values)))

		if (maximumDisplacement <= thresholdForSingleDisplacement) then
			stateOfConvergenceOfSingleDisplacement="OK  "
			isMaximumDisplacementConverged =.true.
		end if
		!!
		!!*******************************************************

		
		!!*******************************************************
		!!  Determina el cambio de la funcion entre las dos ultimas iteraciones
		!!****
		changeOfFunction = abs(this.functionValue - this.oldFunctionValue)
		changeOfFunction = min(abs(this.oldFunctionValue),changeOfFunction)
		isFunctionChangeConverged = .false.
		stateOfConvergenceOfFunctionChange="--  "
		if ( changeOfFunction  < thresholdForFunctionChange ) then
			stateOfConvergenceOfFunctionChange="OK  "
			isFunctionChangeConverged =.true.
		end if
		!!
		!!*******************************************************
		
		write(this.outputUnid,"(T10,A19,F12.7,A13,F12.7,A1,A4)") "RMS GRADIENT     = ",rmsOfGradient, &
						" THRESHOLD = ",thresholdForRmsGradient," ",trim(stateOfConvergenceOfRmsGradient)
		write(this.outputUnid,"(T10,A19,F12.7,A13,F12.7,A1,A4)") "MAX GRADIENT     = ",maximumGradient, &
						" THRESHOLD = ",thresholdForSingleGradient," ",trim(stateOfConvergenceOfSingleGradient)
		write(this.outputUnid,"(T10,A19,F12.7,A13,F12.7,A1,A4)") "RMS DISPLACEMENT = ",rmsOfDisplacement,&
						" THRESHOLD = ",thresholdForRmsDisplacement," ",trim(stateOfConvergenceOfRmsDisplacement)
		write(this.outputUnid,"(T10,A19,F12.7,A13,F12.7,A1,A4)") "MAX DISPLACEMENT = ",maximumDisplacement,&
						" THRESHOLD = ",thresholdForSingleDisplacement," ",trim(stateOfConvergenceOfSingleDisplacement)
		write(this.outputUnid,"(T10,A19,F12.7,A13,F12.7,A1,A4)") "FUNCTION  CHANGE = ",changeOfFunction,&
						" THRESHOLD = ",thresholdForFunctionChange," ",trim(stateOfConvergenceOfFunctionChange)
		write(this.outputUnid,*) ""
		
		if(maximumGradient> 1.0D+1) then

			call TrustRegionIterative_exception( ERROR, "The maximum gradient is very high", &
				"Class object TrustRegionIterative in checkOptimizationCriteria() function")
		
		end if	

		!!*********************************************************
		!! Analiza los criterios de convergencia
		!!****
		if ( isMaximumGradientConverged .and. isRmsOfGradientConverged ) then

			
			if ( isRmsOfDisplacementConverged .and. isMaximumDisplacementConverged ) then
				this.hasConverged = .true.
				write (this.outputUnid,"(T10,A57)") "THE THRESHOLD FOR GRADIENT AND DISPLACEMENT WERE ACHIEVED"
			
			else if ( isFunctionChangeConverged ) then
				this.hasConverged = .true.
				write (this.outputUnid,"(T10,A63)") "THE THRESHOLD FOR GRADIENT AND CHANGE OF FUNCTION WERE ACHIEVED"
			
			else if (this.realNumberOfIteration == 1) then
				this.hasConverged = .true.
				write (this.outputUnid,"(T15,A32)") "THE CURRET POSITION IS A MINIMUM"
			end if

		else
			this.hasConverged = .false.
			write (this.outputUnid,"(T2,A25)") "---BEGINNING NEW SEARCH---"
		end if
		!!
		!!*********************************************************

		
		if ( this.hasConverged ) then
			write(this.outputUnid,*) ""
			write(this.outputUnid,"(T15,A35)") "//////////////////////////////////////////////////////////////////////////////////////////////////////////////"
			write(this.outputUnid,"(T15,A23,I5,A7)") "MINIMUM LOCATED AFTER ",this.realNumberOfIteration," STEPS"
			write(this.outputUnid,"(T15,A35)") "//////////////////////////////////////////////////////////////////////////////////////////////////////////////"
			write(this.outputUnid,*) ""
		end if
		
	end subroutine TrustRegionIterative_checkOptimizationCriteria
	
	
	subroutine TrustRegionIterative_calculateStep( this)
		implicit none
		type(TrustRegionIterative) :: this

		real(8) :: tau
		integer :: i
		integer :: j
		integer :: numberOfTransAndRot
		integer :: lowest
		real(8), allocatable :: auxVector(:)
		real(8) :: auxVal

		allocate( auxVector(this.numberOfVariables) )
		call Matrix_symmetrize( this.hessianeProjected, 'L' )
		!! Calcula vectores y valores propios, acotando los ultimos entre el rango (1E-8,1E3)
		call Matrix_eigenProperties(this.hessianeProjected)
		
		!! Determina si la hesina es definida positiva
		this.hessianeProjected.isPositiveDefinited=Matrix_isPositiveDefinited( this.hessianeProjected )
		
		!! Ordena los vectores propios de manera que los grados translacionales y rotaciones
		!! queden al final
		j=this.numberOfVariables
		i=1
		numberOfTransAndRot=0
		do while ( i < j )

			if ( abs(this.hessianeProjected.eigenValues(i) ) <1.0D-8 .and. &
				abs(this.hessianeProjected.eigenValues(j) ) >1.0D-8	) then
				numberOfTransAndRot=numberOfTransAndRot+1
				if(numberOfTransAndRot>6) then
					call TrustRegionIterative_exception(WARNING,"There are more of six null eigenValues  ", &
					"Class object TrustRegionIterative in calculateStep() function")
				end if	
				auxVal = this.hessianeProjected.eigenValues(j)
				auxVector=this.hessianeProjected.eigenVectors(:,j)
				this.hessianeProjected.eigenVectors(:,j)=this.hessianeProjected.eigenVectors(:,i)
				this.hessianeProjected.eigenValues(j)=this.hessianeProjected.eigenValues(i)
				this.hessianeProjected.eigenVectors(:,i)=auxVector
				this.hessianeProjected.eigenValues(i)=auxVal
				j=j-1
			else
				i=j+1
			end if
			i=i+1

		end do
		
		deallocate(auxVector)

		call Matrix_boundEigenValues( this.hessianeProjected, lowerBond =1.0D-8, upperBond = 1.0D3 )
		
		!! Proyecta el gradiente (proyectado sobre grados externos) sobre los vectores propios de la hesiana
		this.gradientProjectedOnHessiane.values=matmul( transpose(this.hessianeProjected.eigenVectors), &
														 this.gradientProjectedOnExtDegrees.values )

		do i=1,this.numberOfVariables
			if( abs(this.hessianeProjected.eigenValues(i)-1.0D-8)< APMO_instance.DOUBLE_ZERO_THRESHOLD )&
				this.gradientProjectedOnHessiane.values(i)=0.0_8
		end do	

		!! Determina la posicion del valor propio no nulo y mayor que la cota inferior establecida
		do i=1,this.numberOfVariables

			if ( this.hessianeProjected.eigenValues(i) ==Math_NaN ) then
				
				call TrustRegionIterative_exception(ERROR,"The hessiane is wrong", &
					"Class object TrustRegionIterative in calculateStep() function")

			else if ( abs (this.hessianeProjected.eigenValues(i) ) > 1.0D-8 ) then
				lowest = i
				exit
			end if

		end do

		if ( abs(lowest)>this.numberOfVariables ) then
			call TrustRegionIterative_exception(ERROR,"The hessiane is wrong", &
					"Class object TrustRegionIterative in calculateStep() function")
		end if
		!!**********************************************************
		!! CASO III: Realiza paso de More-Sorensen
		!!***
		if ( abs( this.gradientProjectedOnHessiane.values(lowest) ) < 1.0D-8 ) then
			
			call TrustRegionIterative_MoreSorensenStep(this, lowest )
			if( this.isHardCase ) return
			
		end if
		!!
		!!**********************************************************
		
		!!**********************************************************
		!! CASO I : Realiza paso Newton-Rapson
		!!***
		call TrustRegionIterative_NewtonRaphsonStep( this )
		this.squareStepSize = dot_product(this.step.values, this.step.values)
		!!
		!!**********************************************************
		
		!!**********************************************************
		!! CASO II: Realiza  un paso Newton-Rapson restringido
		!!***
		if ( this.squareStepSize > this.trustRadius**2 ) then
			
			write (this.outputUnid, "(T10,A17,F10.5,A28,F10.5)") "THE CURRET STEP: ",sqrt(this.squareStepSize), " EXCEEDED THE TRUST RADIUS: ",this.trustRadius
			call TrustRegionIterative_lagrangeMultiplierOfLinearFunction(this)
			call TrustRegionIterative_restrictedNewtonRaphsonStep(this)
			this.squareStepSize = dot_product(this.step.values, this.step.values)
		
		else &
		if ( .not.this.hessianeProjected.isPositiveDefinited ) then
			
			write (this.outputUnid, *) "The hessiane is not positive definite"
			call TrustRegionIterative_lagrangeMultiplierOfLinearFunction(this)
			call TrustRegionIterative_restrictedNewtonRaphsonStep(this)
			this.squareStepSize = dot_product(this.step.values, this.step.values)
		
		end if
		!!
		!!**********************************************************
		
		!! Transforma el paso proyectado al espacio original
		this.step.values = matmul( this.hessianeProjected.eigenVectors,this.step.values)

	end subroutine TrustRegionIterative_calculateStep
				
	subroutine TrustRegionIterative_MoreSorensenStep(this, eigenValueIndex )
		implicit none
		type(TrustRegionIterative) :: this
		integer :: eigenValueIndex

		integer :: i
		
		this.lagrangeMultiplier = -this.hessianeProjected.eigenValues( eigenValueIndex )
		
		!!********************************
		!! Calcula el paso para cada variable. Este paso esta proyectado 
		!! sobre los vectores propios de la hesiana
		!!***
		this.step.values = 0.0_8
		do i=1, size( this.gradientProjectedOnHessiane.values )
			if ( i /= eigenValueIndex ) then
				this.step.values(i) = - this.gradientProjectedOnHessiane.values(i) &
											/( this.hessianeProjected.eigenValues(i) &
											+ this.lagrangeMultiplier + ZERO_BOUND)
			end if
		end do

		!!*******************************************************
		!! Verifica que la norma del paso calculado no supere la cota 
		!! maxima para el radio de confianza
		!!***
		this.isHardCase=.false.
		this.squareStepSize =  dot_product( this.step.values, this.step.values )

		if ( this.squareStepSize < this.trustRadius**2) then
			this.step.values( eigenValueIndex ) = sqrt( this.trustRadius**2 - this.squareStepSize )
			write (this.outputUnid, *) "Hard case encountered "
			
			!! Transforma el paso proyectado al espacio original
			this.step.values = matmul( this.hessianeProjected.eigenVectors,this.step.values)
			this.isHardCase=.true.
		end if
				
	end subroutine TrustRegionIterative_MoreSorensenStep
				
	
	!>
	!! @brief calcula el paso de Newton-Raphson, empleando la hessiana y el gradiente proyectados
	!>
	subroutine TrustRegionIterative_NewtonRaphsonStep( this )
		implicit none
		type(TrustRegionIterative) :: this
				
		integer :: i
		

		do i=1, size( this.hessianeProjected.eigenValues )
		
			if ( ( abs( this.hessianeProjected.eigenValues(i) ) < 1.0D-5 ) .and. &
				( this.numberOfVariables > 1 ) ) then
				
				this.step.values(i) = 0.0_8
			else
			
				this.step.values(i) = - this.gradientProjectedOnHessiane.values(i) &
								/ ( this.hessianeProjected.eigenValues(i) + ZERO_BOUND )
			end if
			
		end do
				
	end subroutine TrustRegionIterative_NewtonRaphsonStep
	
	!>
	!! @brief calcula el paso de Newton-Raphson modificado, empleando la hessiana y el gradiente proyectados
	!>
	subroutine TrustRegionIterative_restrictedNewtonRaphsonStep( this )
		implicit none
		type(TrustRegionIterative) :: this
				
		integer :: i
		
		do i=1, size( this.hessianeProjected.eigenValues )
		
			if ( ( abs( this.hessianeProjected.eigenValues(i) ) < 1.0D-5 ) .and. &
				( this.numberOfVariables > 1 ) ) then
				
				this.step.values(i) = 0.0_8
			else
			
				this.step.values(i) = - this.gradientProjectedOnHessiane.values(i) &
								/ ( this.hessianeProjected.eigenValues(i) + this.lagrangeMultiplier + ZERO_BOUND )
			end if
			
		end do
				
	end subroutine TrustRegionIterative_restrictedNewtonRaphsonStep
	
	
	subroutine TrustRegionIterative_lagrangeMultiplierOfLinearFunction( this )
		implicit none
		type(TrustRegionIterative) :: this
		
		real(8) :: linearFunction
		real(8) :: derivativeOfLinearFunction
		real(8) :: initialLagrangeMultiplier
		real(8) :: lowerBond
		real(8) :: upperBond
		real(8) :: tolerance
		integer, parameter :: numberOfIterations=100
		integer :: i
		integer :: j
		
		initialLagrangeMultiplier = 0.05_8
		
		!! Acota el rango de busqueda del multiplicador de Lagrange
		lowerBond = 0.0_8
		upperBond = 1.0D6
		
		if ( this.hessianeProjected.eigenValues(1) <= 0.0_8 ) then
			
			initialLagrangeMultiplier = 0.05_8 - this.hessianeProjected.eigenValues(1)
			lowerBond = -this.hessianeProjected.eigenValues(1)
			
		end if
		
		tolerance = 1.0_8
		i=0
		do while( (tolerance > 1.0D-5 ) .and. ( i< numberOfIterations ) )
		
			!!*****************************************
			!! Calcula el valor de la funcion lineal del 
			!!	multplicador de Lagrange
			!!***
			linearFunction=0.0_8
			do j=1, this.numberOfVariables
				linearFunction=linearFunction + ( this.gradientProjectedOnHessiane.values(j) &
							/( this.hessianeProjected.eigenValues(j) + initialLagrangeMultiplier))**2
			end do
			!!
			!!*****************************************
			
			!!*****************************************
			!! Calcula el valor de la derivada de funcion lineal del 
			!!	multplicador de Lagrange
			!!***
			derivativeOfLinearFunction=0.0_8
			do j=1, this.numberOfVariables
				derivativeOfLinearFunction=derivativeOfLinearFunction +  this.gradientProjectedOnHessiane.values(j)**2 &
							/ ( this.hessianeProjected.eigenValues(j) + initialLagrangeMultiplier) **3
			end do
			!!
			!!*****************************************
			
			!!*****************************************
			!! Calcula el valor del multplicador de Lagrange
			!! mediante el metodo de Newton
			!!***
			this.lagrangeMultiplier = initialLagrangeMultiplier + ( sqrt( linearFunction) / this.trustRadius - 1.0_8 ) &
								*( linearFunction/derivativeOfLinearFunction )
			
			!! Establece las nuevas cotas para calcular el multplicador de Lagrange
			if ( ( this.lagrangeMultiplier <= upperBond ).and. ( this.lagrangeMultiplier > lowerBond )) then
			
				if ( this.lagrangeMultiplier < initialLagrangeMultiplier ) then
					upperBond = initialLagrangeMultiplier
				else
					lowerBond = initialLagrangeMultiplier
				end if
				
				initialLagrangeMultiplier = this.lagrangeMultiplier
				
			else
			
				if ( this.lagrangeMultiplier > initialLagrangeMultiplier ) then
					upperBond = initialLagrangeMultiplier
				else
					lowerBond = initialLagrangeMultiplier
				end if
			
				if ( abs( upperBond - 1.0D6) < APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
					initialLagrangeMultiplier = this.lagrangeMultiplier + 0.05_8
				else
					initialLagrangeMultiplier = 0.5_8*( upperBond + lowerBond)
				end if

			
			end if
			!!
			!!***********************************************
		
			tolerance= abs(sqrt( linearFunction ) - this.trustRadius )
			i=i+1
		
		end do
		
		if ( i < numberOfIterations ) then
		
			!!***********************************************************
			!! Verifica que el valor del multiplicador de Lagrange tenga un valor valido
			!!***
			if ( ( this.hessianeProjected.eigenValues(1) < 0.0_8 ) &
				.and. ( this.lagrangeMultiplier < -this.hessianeProjected.eigenValues(1) ) ) then
				
				write (this.outputUnid, *) "The Lagrange multiplier is more lowew that  the smallest hessian eigenvalue "
				call TrustRegionIterative_exception(ERROR, "The calculation of Lagrange Multiplier has failed'", &
				"Class object TrustRegionIterative in lagrangeMultiplierInLinearSection() function")
			
			else &
			if ( (this.hessianeProjected.eigenValues(1) > 0.0_8) .and. ( this.lagrangeMultiplier < 0.0_8 ) ) then
				
				write (this.outputUnid, *) "The Lagrange multiplier is negative"
				call TrustRegionIterative_exception(ERROR, "The calculation of Lagrange Multiplier has failed'", &
				"Class object TrustRegionIterative in lagrangeMultiplierInLinearSection() function")
			
			end if

			write (this.outputUnid, "(T10,A40,I5,A11)") "THE LAGRANGE MULTIPLIER CONVERGED AFTER ", i," ITERATIONS"
	
		else
			
			write (this.outputUnid, *) "The Newton's method for root-finding did not converge "
			write (this.outputUnid, *) "Lagrange multiplier in linear section: ", this.lagrangeMultiplier
			write (this.outputUnid, *) "Current trust radius: ", this.trustRadius
			call TrustRegionIterative_exception(ERROR, "The calculation of Lagrange Multiplier has failed'", &
				"Class object TrustRegionIterative in lagrangeMultiplierInLinearSection() function")

		end if
		
	end subroutine TrustRegionIterative_lagrangeMultiplierOfLinearFunction
	
	!>
	!! @brief Calcula una prediccion al cambio de la funcion
	!<
	subroutine TrustRegionIterative_predictedChangeOfFunction( this )
		implicit none
		type(TrustRegionIterative) :: this
	
		real(8), allocatable :: auxVector(:)

		
		allocate( auxVector(this.numberOfVariables) )
		auxVector = matmul( this.hessianeProjected.values,this.step.values )
		
		!! Expande la funcion como una serie de Taylor de segundo orden
		this.predictedChangeOfFunction = - dot_product( this.oldGradient.values, this.step.values ) &
				- 0.5_8 * dot_product(this.step.values, auxVector )
		write(this.outputUnid, "(T10,A28,F12.8)") "PREDICTED FUNCTION CHANGE = ",this.predictedChangeOfFunction
		
		deallocate( auxVector )
		
	end subroutine TrustRegionIterative_predictedChangeOfFunction
	
	!>
	!! @brief Actualiza en valor de radio de confianza
	!<
	subroutine TrustRegionIterative_updateTrustRadio( this )
		implicit none
		type(TrustRegionIterative) :: this
		
		real(8), parameter :: upperBondForRatio = 0.75_8
		real(8), parameter :: lowerBondForRatio = 0.25_8
		real(8), parameter :: radiusFactor = 2.0_8
		real(8) :: changeOfFunction
		real(8) :: ratioOfChange
		
		this.isSuitableStep = .true.
		changeOfFunction = this.oldFunctionValue - this.functionValue
		
		!! No altera el tamano del radio para cambio pequenos de energia
		if ( ( changeOfFunction < 0.0_8) .and. (abs(changeOfFunction)< 1.0D-4) ) return
		
		ratioOfChange = changeOfFunction / max( abs( this.predictedChangeOfFunction), ZERO_BOUND  )
		
		!!**********************************************
		!! Modifica el valor del radio de confianza
		!!****
		if ( ratioOfChange < 0.0_8 ) this.isSuitableStep = .false.
		
		if ( ratioOfChange > upperBondForRatio ) then
			if ( sqrt(this.squareStepSize) > ( 0.8_8 * this.trustRadius ) ) this.trustRadius = radiusFactor * this.trustRadius
		else &
		if ( ( ratioOfChange >= lowerBondForRatio) .and. ( ratioOfChange <= upperBondForRatio ) ) then
			this.trustRadius = this.trustRadius 
		else
			this.trustRadius = this.trustRadius / radiusFactor
		end if
		!!
		!!**********************************************
		
		!! Establece limite inferior para cambio de radio de confianza
		if ( (this.trustRadius < MINIMUM_TRUST_RADIO ) .and. ( .not.this.isSuitableStep ) ) then
			this.isSuitableStep = .true.
			write(this.outputUnid, *) "Can not reduce the trust radius anymore, forcing to accept it"
		end if
			
		!! Acota el radio de confianza
		this.trustRadius = max( this.trustRadius, MINIMUM_TRUST_RADIO )
		this.trustRadius = min( this.trustRadius, MAXIMUM_TRUST_RADIO )
		write(this.outputUnid, "(T10,A25,F12.8)") "ACTUAL FUNCTION CHANGE = ",changeOfFunction
		write(this.outputUnid, "(T10,A18,F12.8)") "RATIO OF CHANGE = ", ratioOfChange
		write(this.outputUnid, "(T10,A15,F12.8)") "TRUST RADIUS = ",this.trustRadius
		
	end subroutine TrustRegionIterative_updateTrustRadio
	
	!>
	!! @brief Actualiza la matriz hesiana empleando la
	!! formula de Broyden-Fletcher-Goldfab-Shano (BFGS)
	!<
	subroutine TrustRegionIterative_updateHessiane( this )
		implicit none
		type( TrustRegionIterative ) :: this
		
		type(Vector) :: changeOfGradient
		type(Vector) :: auxVector
		real(8) :: factor(4)
		integer :: i
		integer :: j
		
		call Vector_constructor( changeOfGradient, this.numberOfVariables )
		call Vector_constructor( auxVector, this.numberOfVariables )
		
		changeOfGradient.values= this.gradient.values - this.oldGradient.values
		
		!! Elimina ruido numerico en el paso y el el gradiente
		do i=1, this.numberOfVariables
			this.step.values(i) = Math_roundNumber( this.step.values(i),1.0D7 )
			changeOfGradient.values(i) = Math_roundNumber( changeOfGradient.values(i), 1.0D7 )
		end do
		
		
		factor(1)=dot_product(changeOfGradient.values,this.step.values)
		factor(3)=dot_product(changeOfGradient.values,changeOfGradient.values)
		factor(4)=dot_product(this.step.values,this.step.values)
		
		if ( factor(1) < sqrt(3.0D-8*factor(3)*factor(4) ) ) then
			write(this.outputUnid, "(T10,A33)") "THE HESSIANE HAS NOT BEEN UPDATED"
			return
		else

 			factor(1)= 1.0_8/factor(1)

			do i=1,this.numberOfVariables
				auxVector.values(i) = dot_product(this.hessiane.values(i,:), this.step.values )
			end do

!			auxVector.values = matmul( this.hessiane.values, this.step.values )
			factor(2) = 1.0_8 / dot_product( this.step.values, auxVector.values )
	
			!!***************************************************
			!! Realiza la actualizacion de la hesiana
			!!***
			do i=1, this.numberOfVariables
				do j=i, this.numberOfVariables
				
					this.hessiane.values(i,j) = this.hessiane.values(i,j) + factor(1) * changeOfGradient.values(i) &
						* changeOfGradient.values(j) - factor(2) * auxVector.values(i) * auxVector.values(j)
					this.hessiane.values(j,i)=this.hessiane.values(i,j)
				end do
			end do
			write(this.outputUnid, "(T10,A29)") "THE HESSIANE HAS BEEN UPDATED"
			!!
			!!***************************************************
		end if
		
		call Vector_destructor( changeOfGradient )
		call Vector_destructor( auxVector )

	end subroutine TrustRegionIterative_updateHessiane
	
	!>
	!! @brief Reinicia el iterador tomando como punto de partida el estimado actual del minimo
	!!		y la hessiana actual
	!>	
	subroutine TrustRegionIterative_restart( this )
		implicit none
		type(TrustRegionIterative) :: this
		
		

		type(Vector) :: initialpoint
		type(Vector) :: auxGradient
		integer :: currentIterations
		
		
		initialPoint= this.variables
		auxGradient = this.gradient
		currentIterations=this.realNumberOfIteration
		
		write (this.outputUnid,*) ""
		write (this.outputUnid,"(T10,A52)") "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		write (this.outputUnid,"(T10,A52)") "!!!!  RESTARTING THE ALGORITHM  OF MINIMIZATION !!!!"
		write (this.outputUnid,"(T10,A52)") "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		write (this.outputUnid,*) ""

		call TrustRegionIterative_destructor( this )
		call TrustRegionIterative_constructor(this, initialPoint.values)
!!		call Matrix_setIdentity( this.hessiane )
		this.hasRestarted=.true.
		this.realNumberOfIteration=currentIterations
		this.gradient=auxGradient
		call Vector_destructor( initialPoint )
		call Vector_destructor( auxGradient )

		
	end subroutine TrustRegionIterative_restart
	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine TrustRegionIterative_exception( typeMessage, description, debugDescription)
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
	end subroutine TrustRegionIterative_exception


end module TrustRegionIterative_
