!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief 	Permite definicr interfases ha miminizadores internos y/o externos
! 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulo y m�todos
!
!**
module GSLInterface_
	use , intrinsic :: ISO_C_BINDING
	use StructureInterface_
	
	implicit none
	
	interface GSLInterface
	
		!**
		! Realiza la minimizacion de una funcion especificada
		!**
		subroutine gsl_minimizer_run( valuesOfIndependentVariables , state )
			!DEC$ ATTRIBUTES C :: gsl_minimizer_run
			!DEC$ ATTRIBUTES REFERENCE :: valuesOfIndependentVariables
			!DEC$ ATTRIBUTES REFERENCE :: state
			use StructureInterface_
			implicit none
			type(arrayStructure) :: valuesOfIndependentVariables
			integer :: state
		end subroutine gsl_minimizer_run
		
		
	end interface GSLInterface
		
		
	
end module GSLInterface_