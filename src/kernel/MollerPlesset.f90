!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Implementacion de teoria de perturbaciones Moler-Plesset
! 
!  Este modulo incorpora los algoritmos necesarios para realizar correcciones
! de energia y dipolo la funciones de onda OMNE/HF empleando el formalismo
! de la teoria de perturbaciones Moller-Plesset. 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-05-25
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-05-25 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y procedimientos basicos para correccion de segundo orden
!
!**
module MollerPlesset_
	use IFPORT
	use MolecularSystem_
	use ParticleManager_
	use IntegralManager_
	use GenericInterface_
	use Exception_
	use Vector_
	use TransformIntegrals_
	
	
	implicit none
	
	!< enum MollerPlesset_correctionFlags {
	integer, parameter :: FIRST_ORDER = 1
	integer, parameter :: SECOND_ORDER = 2
	integer, parameter :: THIRD_ORDER = 3
	!< }
	
	type, private :: MollerPlesset
		
		character(50) :: name
		integer :: orderOfCorrection
		integer :: numberOfSpecies
		integer :: frozenCoreBoundary
		real(8) :: totalEnergy
		real(8) :: totalCorrection
		real(8) :: secondOrderCorrection
		real(8) :: thirdOrderCorrection
		
		!! Vectores para almacenamiento de correcciones a la energia.para cada especie 
		type(Vector) :: energyCorrectionOfSecondOrder
		type(Vector) :: energyOfCouplingCorrectionOfSecondOrder
		
		logical :: isInstanced
		
	end type MollerPlesset
	
	private ::
		
		type(MollerPlesset), target :: MollerPlesset_instance
		
	private :: &
		MollerPlesset_secondOrderCorrection, &
		MollerPlesset_thirdOrderCorrection
	
	public :: &
		MollerPlesset_constructor, &
		MollerPlesset_destructor, &
		MollerPlesset_show, &
		MollerPlesset_run, &
		MollerPlesset_getTotalEnergy, &
		MollerPlesset_getEnergyCorrection, &
		MollerPlesset_getSpecieCorrection
		
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine MollerPlesset_constructor( orderOfCorrection )
		implicit none
		integer, intent(in) :: orderOfCorrection
		
		integer :: i
		type(Exception) :: ex
			
		if( .not.MollerPlesset_instance.isInstanced ) then
			
			MollerPlesset_instance.orderOfCorrection = orderOfCorrection
			MollerPlesset_instance.numberOfSpecies = ParticleManager_getNumberOfQuantumSpecies()

			if ( MollerPlesset_instance.orderOfCorrection >= 2 ) then
				call Vector_constructor( MollerPlesset_instance.energyCorrectionOfSecondOrder, MollerPlesset_instance.numberOfSpecies)
				
				i = MollerPlesset_instance.numberOfSpecies * ( MollerPlesset_instance.numberOfSpecies-1 ) / 2
				
				call Vector_constructor( MollerPlesset_instance.energyOfCouplingCorrectionOfSecondOrder, i)
			end if
			
			if ( MollerPlesset_instance.orderOfCorrection >= 3 ) then
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object MollerPlesset in the constructor() function" )
				call Exception_setDescription( ex, "This order correction hasn't been implemented" )
				call Exception_show( ex )
				
			end if
		
			MollerPlesset_instance.isInstanced =.true.
			
		end if
			
	end subroutine MollerPlesset_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine MollerPlesset_destructor()
		implicit none
		
               MollerPlesset_instance.isInstanced =.false.

	end subroutine MollerPlesset_destructor
	
	!**
	! @brief Muestra informacion asociada con la correccion MPn
	!**
	subroutine MollerPlesset_show()
		implicit none
		
		integer :: i
		integer :: j
		integer :: k
		
               if ( MollerPlesset_instance.isInstanced )  then

                print *,""
	        print *," POST HARTREE-FOCK CALCULATION"
	        print *," MANY-BODY PERTURBATION THEORY:"
	        print *,"=============================="
	        print *,""
	        write (6,"(T10,A25)") "MOLLER-PLESSET FORMALISM "
	        write (6,"(T10,A23, I5)") "ORDER OF CORRECTION = ",MollerPlesset_instance.orderOfCorrection


		    print *,""
		    write (6,'(T10,A15,ES20.12)') "E(0) + E(1) = ", MolecularSystem_getTotalEnergy()
		    write (6,'(T10,A15,ES20.12)') "E(2) = ", MollerPlesset_instance.secondOrderCorrection
		    write (6,'(T25,A20)') "________________________" 
		    write (6,'(T10,A15,ES25.17)') "E(MP2)= ", MollerPlesset_instance.totalEnergy
		    print *,""
		    write ( 6,'(T10,A35)') "-----------------------------------------------"
		    write ( 6,'(T10,A15,A20)') " E(n){ Specie } ","   E(n) / Hartree "
		    write ( 6,'(T10,A35)') "-----------------------------------------------"
		    print *,""
		    
		    do i=1, MollerPlesset_instance.numberOfSpecies
			 write (*,'(T10,A5,A4,A8,ES16.8)') "E(2){", trim(  ParticleManager_getNameOfSpecie( i ) ),"   } = ", &
					     MollerPlesset_instance.energyCorrectionOfSecondOrder.values(i)
		    end do
		    
		    print *,""
		    k=0
		    do i=1, MollerPlesset_instance.numberOfSpecies
			 do j=i+1,MollerPlesset_instance.numberOfSpecies
				   k=k+1
				   write (*,'(T10,A5,A8,A4,ES16.8)') "E(2){", &
					     trim(  ParticleManager_getNameOfSpecie( i ) ) // "/" // trim(  ParticleManager_getNameOfSpecie( j ) ), &
					     "} = ", MollerPlesset_instance.energyOfCouplingCorrectionOfSecondOrder.values(k)
			 end do
		    end do

               end if
		
	end subroutine MollerPlesset_show
	
	!**
	! @brief realiza la correccion de energia MPn orden dado
	!**
	subroutine MollerPlesset_run()
		implicit none
		
		type(Exception) :: ex
		integer :: i
	
		if ( MollerPlesset_instance.isInstanced ) then
			
			do i=2, MollerPlesset_instance.orderOfCorrection
			
				select case( i )
			
					case(  SECOND_ORDER )
						
						call MollerPlesset_secondOrderCorrection()
						MollerPlesset_instance.totalCorrection= MollerPlesset_instance.secondOrderCorrection
					case(  THIRD_ORDER )
						
						call MollerPlesset_thirdOrderCorrection()
						
						MollerPlesset_instance.totalCorrection= MollerPlesset_instance.totalCorrection +&
														MollerPlesset_instance.thirdOrderCorrection
						
					case default
					
						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object MollerPlesset in run() function" )
						call Exception_setDescription( ex, "This order correction hasn't been implemented" )
						call Exception_show( ex )
					
				end select
			
			end do
			
			MollerPlesset_instance.totalEnergy = MolecularSystem_getTotalEnergy() + MollerPlesset_instance.totalCorrection
			
		else
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MollerPlesset in run() function" )
			call Exception_setDescription( ex, "You should to instance MollerPlesset module before use this function" )
			call Exception_show( ex )
		
		end if
	
	
	end subroutine MollerPlesset_run
	
	!**
	! @ Retorna el la correccion a la energia 
	!**
	function MollerPlesset_getEnergyCorrection() result( output)
		implicit none
		real(8) :: output
		
		output = MollerPlesset_instance.totalCorrection
		
	end function MollerPlesset_getEnergyCorrection
	
	!**
	! @ Retorna el la correccion a la energia 
	!**
	function MollerPlesset_getSpecieCorrection( specieName) result( output)
		implicit none
		character(*) :: specieName
		real(8) :: output
		
		integer :: i

		do i=1, MollerPlesset_instance.numberOfSpecies

			if ( trim(specieName) == trim(  ParticleManager_getNameOfSpecie( i ) ) ) then
			
				output = MollerPlesset_instance.energyCorrectionOfSecondOrder.values(i)
				return

			end if 

		end do

	end function MollerPlesset_getSpecieCorrection



	!**
	! @ Retorna la energia final com correccion Moller-Plesset de orrden dado 
	!**
	function MollerPlesset_getTotalEnergy() result(output)
		implicit none
		real(8) :: output
		
		output = MollerPlesset_instance.totalEnergy
	
	end function MollerPlesset_getTotalEnergy
	
	
	!**
	! @brief Realiza la correccion de segundo orden a la enegia 
	!**
	subroutine MollerPlesset_secondOrderCorrection()
		implicit none
		
		integer :: a
		integer :: b
		integer :: r
		integer :: s
		integer :: p
		integer :: t
		integer :: i
		integer :: j
		integer :: m
		integer :: n
		integer :: u
		integer :: specieID
		integer :: otherSpecieID
		integer :: electronsID
		integer :: ocupationNumber
		integer :: ocupationNumberOfOtherSpecie
		integer :: numberOfContractions
		integer :: numberOfContractionsOfOtherSpecie
		integer(4) :: errorNum
		integer(8) :: auxIndex
		character(10) :: nameOfSpecie
		character(10) :: nameOfOtherSpecie
		type(Vector) :: eigenValues
		type(Vector) :: eigenValuesOfOtherSpecie
		type(Matrix) :: auxMatrix
		type(TransformIntegrals) :: repulsionTransformer
		real(8) :: lambda
		real(8) :: lambdaOfOtherSpecie
		real(8) :: independentEnergyCorrection
		real(8) :: couplingEnergyCorrection
		real(8) :: auxVal
		real(8) :: auxVal_A
		real(8) :: auxVal_B

		
		electronsID = ParticleManager_getSpecieID(  nameOfSpecie="e-" )
		call TransformIntegrals_constructor( repulsionTransformer )

		
		!!*******************************************************************************************
		!! Calculo de correcciones de segundo orden para particulas de la misma especie
		!!
		if ( .not.APMO_instance.OPTIMIZE ) then
                        print *,""
                        print *,"BEGIN FOUR-INDEX INTEGRALS TRANFORMATION:"
                        print *,"========================================"
			print *,""
			print *,"--------------------------------------------------"
			print *,"    Algorithm Four-index integral tranformation"
			print *,"      Yamamoto, Shigeyoshi; Nagashima, Umpei. "
			print *,"  Computer Physics Communications, 2005, 166, 58-65"
			print *,"--------------------------------------------------"
			print *,""
			
		end if

		do i=1, MollerPlesset_instance.numberOfSpecies
		
			MollerPlesset_instance.frozenCoreBoundary = 1
			if ( i == electronsID )  MollerPlesset_instance.frozenCoreBoundary = &
				APMO_instance.MP_FROZEN_CORE_BOUNDARY
			
			nameOfSpecie= trim(  ParticleManager_getNameOfSpecie( i ) )
			independentEnergyCorrection = 0.0_8
			
			if( trim(nameOfSpecie)=="e-" .or. .not.APMO_instance.MP_ONLY_ELECTRONIC_CORRECTION ) then
				
				specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecie )
				eigenValues = MolecularSystem_getEigenValues(i)
				ocupationNumber = ParticleManager_getOcupationNumber( i )
				numberOfContractions = ParticleManager_getNumberOfContractions( i )
				lambda = ParticleManager_getLambda( i )
				
				
				if ( .not.APMO_instance.OPTIMIZE ) then
					write (6,"(T10,A)")"INTEGRALS TRANSFORMATION FOR: "//trim(nameOfSpecie)
					print *,""
				end if
	
				call TransformIntegrals_atomicToMolecularOfOneSpecie( repulsionTransformer,&
						MolecularSystem_getEigenvectors(specieID), auxMatrix, specieID, trim(nameOfSpecie) )
	
				!!**************************************************************************
				!!	Calcula la correccion de segundo orden a la energia
				!!****
				do a=MollerPlesset_instance.frozenCoreBoundary, ocupationNumber
					do b=MollerPlesset_instance.frozenCoreBoundary,ocupationNumber
						do r=ocupationNumber+1, numberOfContractions
							do s=r, numberOfContractions
						
								auxIndex = IndexMap_tensorR4ToVector(a,r,b,s, numberOfContractions )
								auxVal_A= auxMatrix.values(auxIndex, 1)
	
								if (  dabs( auxVal_A)  > 1.0E-10_8 ) then
							
									if ( s>r ) then
										
										if (a==b) then
											
											if( abs( lambda  -  1.0_8 ) > APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
												
												independentEnergyCorrection = independentEnergyCorrection + 2.0_8 *  auxVal_A**2.0  &
												* ( lambda  -  1.0_8 ) / ( eigenValues.values(a) + eigenValues.values(b) &
												- eigenValues.values(r) - eigenValues.values(s) )
	
											end if
		
										else
	
											auxIndex = IndexMap_tensorR4ToVector(r, b, s, a, numberOfContractions )
											auxVal_B= auxMatrix.values(auxIndex, 1)
	
											independentEnergyCorrection = independentEnergyCorrection + 2.0_8 *  auxVal_A  &
											* ( lambda * auxVal_A  - auxVal_B ) / ( eigenValues.values(a) + eigenValues.values(b) &
											- eigenValues.values(r) - eigenValues.values(s) )
	
										end if
	
									else if ( a==b .and. r==s ) then
	
										if ( abs( lambda  -  1.0_8 ) > APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
											
											independentEnergyCorrection = independentEnergyCorrection +  auxVal_A**2.0_8  &
												* ( lambda - 1.0_8 ) / ( 2.0_8*( eigenValues.values(a)-eigenValues.values(r)))
										
										end if
	
									else
										
										if ( abs( lambda  -  1.0_8 ) > APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
											independentEnergyCorrection = independentEnergyCorrection +  auxVal_A**2.0  &
												* ( lambda  - 1.0_8 ) / ( eigenValues.values(a) + eigenValues.values(b) &
												- eigenValues.values(r) - eigenValues.values(s) )
										end if
	
									end if
								end if
	
							end do
						end do
					end do
				end do
			end if

			MollerPlesset_instance.energyCorrectionOfSecondOrder.values(i) = independentEnergyCorrection &
				* ( ( ParticleManager_getCharge( specieID ) )**4.0_8 )

			call Matrix_destructor(auxMatrix)
			!!
			!!**************************************************************************

		end do
		
		!! Suma las correcciones de energia para especies independientes 
		MollerPlesset_instance.secondOrderCorrection = sum( MollerPlesset_instance.energyCorrectionOfSecondOrder.values )
		!!
		!!*******************************************************************************************
		

		!!*******************************************************************************************
		!! Calculo de correcion se segundo orden para interaccion entre particulas de especie diferente
		!!
		if ( MollerPlesset_instance.numberOfSpecies > 1 ) then
		
			couplingEnergyCorrection = 0.0_8
			m=0

			do i = 1 , MollerPlesset_instance.numberOfSpecies

				nameOfSpecie= trim(  ParticleManager_getNameOfSpecie( i ) )
				specieID =ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecie )
				eigenValues = MolecularSystem_getEigenValues(i)
				ocupationNumber = ParticleManager_getOcupationNumber( i )
				numberOfContractions = ParticleManager_getNumberOfContractions( i )
				lambda = ParticleManager_getEta( i )
				
				do j = i + 1 , MollerPlesset_instance.numberOfSpecies
					m = m + 1

					nameOfOtherSpecie= trim(  ParticleManager_getNameOfSpecie( j ) )
					otherSpecieID =ParticleManager_getSpecieID( nameOfSpecie=nameOfOtherSpecie )
					eigenValuesOfOtherSpecie = MolecularSystem_getEigenValues(j)
					ocupationNumberOfOtherSpecie = ParticleManager_getOcupationNumber( j )
					numberOfContractionsOfOtherSpecie = ParticleManager_getNumberOfContractions( j )
					lambdaOfOtherSpecie = ParticleManager_getLambda( i )
					couplingEnergyCorrection = 0.0_8
					
					if ( .not.APMO_instance.OPTIMIZE ) then
						write (6,"(T10,A)") "INTER-SPECIES INTEGRALS TRANSFORMATION FOR: "//trim(nameOfSpecie)//"/"//trim(nameOfOtherSpecie)
						print *,""
					end if
					
					call TransformIntegrals_atomicToMolecularOfTwoSpecies( repulsionTransformer, &
					 		MolecularSystem_getEigenVectors(i), MolecularSystem_getEigenVectors(j), &
		 					auxMatrix, specieID, nameOfSpecie, otherSpecieID, nameOfOtherSpecie )
					
					auxMatrix.values = auxMatrix.values * ParticleManager_getCharge( specieID ) &
									 * ParticleManager_getCharge( otherSpecieID )


					do a=1, ocupationNumber
						do p=1,ocupationNumberOfOtherSpecie
							do r=ocupationNumber+1, numberOfContractions
								do t=ocupationNumberOfOtherSpecie+1, numberOfContractionsOfOtherSpecie

									auxIndex = IndexMap_tensorR4ToVector(a,r,p,t, numberOfContractions, &
										 numberOfContractionsOfOtherSpecie )

									couplingEnergyCorrection = couplingEnergyCorrection +  &
										 ( ( auxMatrix.values(auxIndex,1) )**2.0_8 ) & 
										/ (eigenValues.values(a) + eigenValuesOfOtherSpecie.values(p) &
										 -eigenValues.values(r)-eigenValuesOfOtherSpecie.values(t) ) 

								end do
							end do
						end do
					end do

					MollerPlesset_instance.energyOfCouplingCorrectionOfSecondOrder.values(m)= &
						( lambda * lambdaOfOtherSpecie * couplingEnergyCorrection ) /4.0_8
						
				end do
			end do

			call Matrix_destructor(auxMatrix)
			!! Adiciona la correccion del termino de acoplamiento
			MollerPlesset_instance.secondOrderCorrection = MollerPlesset_instance.secondOrderCorrection + &
										sum( MollerPlesset_instance.energyOfCouplingCorrectionOfSecondOrder.values )
		
		end if
		!!
		!!*******************************************************************************************

		if ( .not.APMO_instance.OPTIMIZE ) then
                    print *,"END FOUR-INDEX INTEGRALS TRANFORMATION"
                    print *,""
                end if

	
	end subroutine MollerPlesset_secondOrderCorrection

	!**
	! @brief Realiza la correccion de tercer orden a la enegia 
	!**
	subroutine MollerPlesset_thirdOrderCorrection()
		implicit none
		
		type(Exception) :: ex
		
		call Exception_constructor( ex , ERROR )
		call Exception_setDebugDescription( ex, "Class object MollerPlesset in thirdOrderCorrection() function" )
		call Exception_setDescription( ex, "This order correction hasn't been implemented" )
		call Exception_show( ex )
	
	end subroutine MollerPlesset_thirdOrderCorrection

	
end module MollerPlesset_