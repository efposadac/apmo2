!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module Solver_
     use APMO_
     use RHFSolver_
     use MollerPlesset_
     use CalculateProperties_
     use IntegralManager_
     use MolecularSystem_
     use Exception_
     implicit none

     type, public :: Solver
          character(20) :: name
          character(20) :: methodName
          logical :: withProperties
          real(8) :: energy
     end type


     type(Solver), public :: apmo_solver

     public :: &
          Solver_constructor, &
          Solver_destructor, &
          Solver_show, &
          Solver_run, &
          Solver_reset


     private
contains


     subroutine Solver_constructor( this, methodName )
          implicit none
          type(Solver) :: this
          character(*) :: methodName

          this.methodName = trim(methodName)
          call IntegralManager_constructor()
          this.withProperties=.true.
          select case ( trim(this.methodName) )

               case('RHF')
                    call Solver_rhfConstructor( this )
               case('RHF-MP2')
                    call Solver_rhfMP2Constructor( this )
               case('ROHF')
                    call Solver_rohfConstructor( this )
               case('ROHF-MP2')
                    call Solver_rohfMP2Constructor( this )
               case('UHF')
                    call Solver_uhfConstructor( this )
               case('UHF-MP2')
                    call Solver_uhfMP2Constructor( this )
               case default
                    call Solver_exception(ERROR, "You should select a method before use the main program", &
                         "Source Solver class in constructor() function" )
          end select

     end subroutine Solver_constructor


     subroutine Solver_destructor( this )
          implicit none
          type(Solver) :: this

          select case ( trim(this.methodName) )

               case('RHF')
                    call Solver_rhfDestructor( this )
               case('RHF-MP2')
                    call Solver_rhfMP2Destructor( this )
               case('ROHF')

               case('ROHF-MP2')

               case('UHF')

               case('UHF-MP2')

               case default

          end select

               call IntegralManager_destructor()

     end subroutine Solver_destructor


     subroutine Solver_run( this )
          implicit none
          type(Solver) :: this

          select case ( trim(this.methodName) )

               case('RHF')
                    call Solver_rhfRun( this )
               case('RHF-MP2')
                    call Solver_rhfMp2Run( this )
               case('ROHF')

               case('ROHF-MP2')

               case('UHF')

               case('UHF-MP2')

               case default

          end select

     end subroutine Solver_run


     subroutine Solver_reset( this )
          implicit none
          type(Solver) :: this

          select case ( trim(this.methodName) )

               case('RHF')
                    call RHFSolver_reset()
               case('RHF-MP2')
                    call RHFSolver_reset()
               case('ROHF')

               case('ROHF-MP2')

               case('UHF')

               case('UHF-MP2')

               case default

          end select

     end subroutine Solver_reset

     subroutine Solver_show( this )
          implicit none
          type(Solver) :: this

          real(8) :: auxValue

          call MolecularSystem_showEigenVectors()
          call MolecularSystem_showEnergyComponents()
          call MolecularSystem_showPopulationAnalyses()
          call MolecularSystem_showCharges()
          call CalculateProperties_showContributionsToElectrostaticMoment( CalculateProperties_instance )
          call MollerPlesset_show()

          if ( APMO_instance.ONLY_ELECTRONIC_EFFECT )  then

               call MolecularSystem_calculateElectronicEffect( auxValue )

               if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) then

                    auxValue = auxValue + MollerPlesset_getSpecieCorrection("e-")
                    print *,""
                    write (6,"(T10,A31,ES20.12)") "TOTAL MP2 ELECTRONIC ENERGY = ", auxValue
               end if

          end if

     end subroutine Solver_show


     subroutine Solver_rhfConstructor( this )
          implicit none
          type(Solver) :: this

         call RHFSolver_constructor()
         call CalculateProperties_constructor(CalculateProperties_instance )

     end subroutine Solver_rhfConstructor


     subroutine Solver_rhfMP2Constructor( this )
          implicit none
          type(Solver) :: this


          call Solver_rhfConstructor( this )
          if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) then
               call MollerPlesset_constructor(APMO_instance.MOLLER_PLESSET_CORRECTION)
          end if

     end subroutine Solver_rhfMP2Constructor

     subroutine Solver_rohfConstructor( this )
          implicit none
          type(Solver) :: this

     end subroutine Solver_rohfConstructor


     subroutine Solver_rohfMP2Constructor( this )
          implicit none
          type(Solver) :: this

     end subroutine Solver_rohfMP2Constructor


     subroutine Solver_uhfConstructor( this )
          implicit none
          type(Solver) :: this

     end subroutine Solver_uhfConstructor


     subroutine Solver_uhfMP2Constructor( this )
          implicit none
          type(Solver) :: this

     end subroutine Solver_uhfMP2Constructor

     subroutine Solver_rhfDestructor( this )
          implicit none
          type(Solver) :: this

          call RHFSolver_destructor()
          call CalculateProperties_destructor(CalculateProperties_instance )

     end subroutine Solver_rhfDestructor


     subroutine Solver_rhfMP2Destructor( this )
          implicit none
          type(Solver) :: this

          call Solver_rhfDestructor( this )
          call MollerPlesset_destructor()

     end subroutine Solver_rhfMP2Destructor


     subroutine Solver_rhfRun( this )
          implicit none
          type(Solver) :: this

          call RHFSolver_run()

          if ( this.withProperties ) &
               call CalculateProperties_dipole( CalculateProperties_instance )

               this.energy = MolecularSystem_getTotalEnergy()


     end subroutine Solver_rhfRun


     subroutine Solver_rhfMP2Run( this )
          implicit none
          type(Solver) :: this

          call Solver_rhfRun( this )
          call MollerPlesset_run()
	  this.energy = MollerPlesset_getTotalEnergy()

     end subroutine Solver_rhfMP2Run


     subroutine Solver_exception(typeMessage, description, debugDescription)
          implicit none
          integer :: typeMessage
          character(*) :: description
          character(*) :: debugDescription

          type(Exception) :: ex

          call Exception_constructor( ex , typeMessage )
          call Exception_setDebugDescription( ex, debugDescription )
          call Exception_setDescription( ex, description )
          call Exception_show( ex )
          call Exception_destructor( ex )

     end subroutine Solver_exception



end module Solver_
