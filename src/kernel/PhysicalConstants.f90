!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief  Modulo para definicion y ajuste de constantes globales
!
! Este modulo contiene la definicion de constantes globales utilizadas dentro de
! modulos y sus procedimiento asociados, al igual que algunos metodos que ajustan
! sus valores de forma adecuada en tiempo de ejecucion.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creaci� : </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapta al estandar de codificacion propuesto.
!**
module PhysicalConstants_
	!! Define algunas constantes nmericas 

	!****************************************************************************
	!! definicion de algunas constantes atomicas y nucleares en unidades at�icas.
	!!
	real(8) , parameter :: PhysicalConstants_ELECTRON_CHARGE = -1.0_8
	real(8) , parameter :: PhysicalConstants_ELECTRON_MASS = 1.0_8
	real(8) , parameter :: PhysicalConstants_PROTON_CHARGE = 1.0_8
	real(8) , parameter :: PhysicalConstants_PROTON_MASS = 1836.15267247_8
	real(8) , parameter :: PhysicalConstants_NEUTRON_CHARGE = 0.0_8
	real(8) , parameter :: PhysicalConstants_NEUTRON_MASS = 1838.6836605_8
	real(8) , parameter :: PhysicalConstants_SPIN_UP_ELECTRON = 0.5_8
	real(8) , parameter :: PhysicalConstants_SPIN_DOWN_ELECTRON = -0.5_8
	real(8) , parameter :: PhysicalConstants_SPIN_ELECTRON = 0.5_8
	real(8) , parameter :: PhysicalConstants_SPIN_FERMION = 0.5_8 !< solo se especifica este valor pero puede ser n*0.5 n=1,2,3,..., en U.A.
	real(8) , parameter :: PhysicalConstants_SPIN_BOSON = 1.0_8 !< solo se especifica este valor pero puede ser n=0,1,2,3,..., en U.A.
	!!***************************************************************************
	
end module PhysicalConstants_
