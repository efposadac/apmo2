!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module LibintInterface_
	use Exception_
	use LibintTypes_
	use PrimitiveGaussian_
	use IndexMap_
	use Math_
	use, intrinsic :: iso_c_binding
	implicit none

	!>
	!! @brief Description
	!!
	!! @author Edwin Posada
	!!
	!! <b> Creation data : </b> 10-07-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 10-07-10 </tt>:  Edwin Posada ( efposadac@unal.edu.co )
	!!        -# Modulo que sirve como interfaz para las librerias LIBINT
	!!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
	!!        -# description
	!<

	type, public :: LibintInterface
		character(5) :: job
		logical :: isInstanced
		integer :: maxAngularMoment
		integer :: numberOfPrimitives
		integer :: libintStorage
		type(lib_int) :: libint
		type(lib_deriv) :: libderiv
		type(lib_r12) :: libr12
	end type

	public :: &
		LibintInterface_constructor, &
		LibintInterface_destructor, &
		LibintInterface_isInstanced, &
		LibintInterface_show, &
		LibintInterface_initializeShell,&
		LibintInterface_compute

	private :: &
		LibintInterface_canonicalOrder,&
		LibintInterface_computeEris, &
		LibintInterface_computeDeriv, &
		LibintInterface_computeR12, &
		LibintInterface_orbitalIndex, &
		LibintInterface_buildPrimQuartet, &
		LibintInterface_buildShellQuartet, &
		LibintInterface_normalize, &
		LibintInterface_initializeLibInt, &
		LibintInterface_terminateLibInt, &
		LibintInterface_getEris, &
		LibintInterface_initializeLibDeriv, &
		LibintInterface_terminateLibDeriv, &
		LibintInterface_getDerivs, &
		LibintInterface_initializeLibR12, &
		LibintInterface_terminateLibR12, &
		LibintInterface_getR12, &
		LibintInterface_exception


	!<
	!!Apuntadores a las funciones de libint.a, libderiv.a y libr12.a
	!>
	type(c_funptr), dimension(0:6,0:6,0:6,0:6), bind(c) :: build_eri
	type(c_funptr), dimension(0:3,0:3,0:3,0:3), bind(c) :: build_deriv1_eri
	type(c_funptr), dimension(0:5,0:5,0:5,0:5), bind(c) :: build_r12_grt

	interface

		!<
		!!Interfaz a libint.a
		!>
		subroutine LibintInterface_initLibIntBase() bind(C, name="init_libint_base")
			implicit none

		end subroutine LibintInterface_initLibIntBase

		function LibintInterface_initLibInt(libInt, maxAngMoment, numberOfPrimitives) bind(C, name="init_libint")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			integer(kind=c_int) :: LibintInterface_initLibInt
			type(lib_int) :: libInt
			integer(kind=c_int), value :: maxAngMoment
			integer(kind=c_int), value :: numberOfPrimitives

		end function LibintInterface_initLibInt

		function LibintInterface_buildLibInt(libInt, numberOfPrimitives) bind(C)
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(c_ptr) :: LibintInterface_buildLibint
			type(lib_int) :: libInt
			integer(kind=c_int), value :: numberOfPrimitives

		end function LibintInterface_buildLibInt


		subroutine LibintInterface_freeLibInt(libInt) bind(C, name="free_libint")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(lib_int) :: libInt

		end subroutine LibintInterface_freeLibInt

		!<
		!!Interfaz a libderiv.a
		!>
		subroutine LibintInterface_initLibDerivBase() bind(C, name="init_libderiv_base")
			implicit none

		end subroutine LibintInterface_initLibDerivBase

		function LibintInterface_initLibDeriv1(libDeriv, maxAngMoment, numberOfPrimitives, maxCartesianOrbital) bind(C, name="init_libderiv1")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			integer(kind=c_int) :: LibintInterface_initLibDeriv1
			type(lib_deriv) :: libDeriv
			integer(kind=c_int), value :: maxAngMoment
			integer(kind=c_int), value :: numberOfPrimitives
			integer(kind=c_int), value :: maxCartesianOrbital

		end function LibintInterface_initLibDeriv1

		subroutine LibintInterface_buildLibDeriv1(libDeriv, numberOfPrimitives) bind(C)
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(lib_deriv) :: libDeriv
			integer(kind=c_int),value :: numberOfPrimitives

		end subroutine LibintInterface_buildLibDeriv1

		subroutine LibintInterface_freeLibDeriv(libDeriv) bind(C, name="free_libderiv")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(lib_deriv) :: libDeriv

		end subroutine LibintInterface_freeLibDeriv

		!<
		!!Interfaz a libr12.a
		!>
		subroutine LibintInterface_initLibR12Base() bind(C, name="init_libr12_base")
			implicit none

		end subroutine LibintInterface_initLibR12Base

		function LibintInterface_initLibR12(libR12, maxAngMoment, numberOfPrimitives, maxCartesianOrbital) bind(C, name="init_libr12")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			integer(kind=c_int) :: LibintInterface_initLibR12
			type(lib_r12) :: libR12
			integer(kind=c_int), value :: maxAngMoment
			integer(kind=c_int), value :: numberOfPrimitives
			integer(kind=c_int), value :: maxCartesianOrbital

		end function LibintInterface_initLibR12

		subroutine LibintInterface_buildLibR12(libR12, numberOfPrimitives) bind(C)
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(lib_r12) :: libR12
			integer(kind=c_int),value :: numberOfPrimitives

		end subroutine LibintInterface_buildLibR12

		subroutine LibintInterface_freeLibR12(LibR12) bind(C, name="free_libr12")
			use LibintTypes_
			use, intrinsic :: iso_c_binding
			implicit none

			type(lib_r12) :: libR12

		end subroutine LibintInterface_freeLibR12

	end interface
		
	!<
	!! Factores en comun
	!>
	type(prim_data), private, target :: primitiveQuartet
	type(contr_data), private :: shellQuartet

	integer, private :: arraySize
	integer, private :: angularMomentA
	integer, private :: angularMomentB
	integer, private :: angularMomentC
	integer, private :: angularMomentD
	integer, private :: sumAngularMoment
	integer, private :: maxAngularMoment
	integer, private :: numberOfCartesianA
	integer, private :: numberOfCartesianB
	integer, private :: numberOfCartesianC
	integer, private :: numberOfCartesianD

	logical, private :: interSpecies
	logical, private :: couplingA
	logical, private :: couplingB

	type(LibintInterface), public :: LibintInterface_instance
	integer, public, allocatable :: cartesianOrbitalIndex(:)
	integer, public :: order

contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param job, trabajo a realizar
	!<
	subroutine LibintInterface_constructor(this, maxAngMoment, numberOfPrimitives, job)
		implicit none

		type(LibintInterface), intent(inout) :: this
		integer, intent(in) :: maxAngMoment
		integer, intent(in) :: numberOfPrimitives
		character(*) :: job

		if (.not. LibintInterface_isInstanced()) then

			this%job = trim(job)
			this%maxAngularMoment = maxAngMoment
			this%numberOfPrimitives = numberOfPrimitives


			select case(trim(this%job))
				case("ERIS")
					call LibintInterface_initLibIntBase()
					call LibintInterface_initializeLibInt()
				case("R12")
					call LibintInterface_initLibR12Base()
					call LibintInterface_initializeLibR12()
				case("DERIV")
					call LibintInterface_initLibDerivBase()
					call LibintInterface_initializeLibDeriv()

			end select

			this%isInstanced = .true.
		else
			call LibintInterface_exception( ERROR, "in libint interface constructor function",&
												"you must destroy this object before to use it again")
		end if

	end subroutine LibintInterface_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine LibintInterface_destructor()
		implicit none

		if (LibintInterface_isInstanced()) then

			select case(trim(LibintInterface_instance%job))
				case("ERIS")
!					call LibintInterface_terminateLibInt() !!No funciona
				case("R12")
					call LibintInterface_terminateLibR12()
				case("DERIV")
					call LibintInterface_terminateLibDeriv(LibintInterface_instance%libderiv)

			end select

			LibintInterface_instance%isInstanced = .false.
		else
			call LibintInterface_exception( ERROR, "in libint interface destructor function",&
												"you must instantiate this object before to destroying it")
		end if

	end subroutine LibintInterface_destructor

	!>
	!! @brief Muestra informacion del objeto (mejorar!)
	!!
	!! @param this 
	!<
	subroutine LibintInterface_show(this)
		implicit none

		type(LibintInterface) :: this
		write(*, "(/A)")  "  ------------------------------------------"
		write(*, "(A)" )  "   LIBINT IS RUNNING WITH NEXT PARAMETERS: "
		write(*, "(A)" )  "  ------------------------------------------"
		write(*, "(A, A19)")  "  work", trim(LibintInterface_instance%job)
		write(*, "(A, I5)" )  "  maxAngularMoment", this%maxAngularMoment
		write(*, "(A, I4)" )  "  numberOfPrimitives", this%numberOfPrimitives
		write(*, "(A, I10/)") "  words of memory", this%libintStorage

		!! Solo para propositos de pruebas
!		write(*, "(A)")  "FOR PRIMITIVES:"
!		write(*, "(A)")  "------------------------------------------"
!		write(*, "(A, I5)" )  "maxAngularMoment", maxAngularMoment
!		write(*, "(A, I5)" )  "sumAngularMoment", sumAngularMoment
!		write(*, "(A, I15)")  "arraySize", arraySize
!		write(*, "(A, I16)")  "order", order
!		write(*, "(A/)") ""

	end subroutine LibintInterface_show

	!<
	!! Inicializa los datos globales para el calculo de una capa dada (AB|CD)
	!! Solo se llama una vez en terminos de contracciones debido a que son datos identicos
	!! para todas las posibles combinaciones de primitivas de una capa
	!>
	subroutine LibintInterface_initializeShell(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD , &
											   indexA, indexB, indexC, indexD, totalNumberOfContractions, otherTotalNumberOfContractions,&
											   AOIndex, isInterSpecies, isCouplingA, isCouplingB)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		integer, intent(in) :: indexA, indexB, indexC, indexD
		integer, intent(in) :: totalNumberOfContractions
		integer, optional, intent(in) :: otherTotalNumberOfContractions
		integer, allocatable :: AOIndex(:)

		logical, optional, intent(in) :: isInterSpecies
		logical, optional, intent(in) :: isCouplingA
		logical, optional, intent(in) :: isCouplingB

		integer :: otherTTotalNumberOfContractions

		!!Se trata de in integral entre especies?
		interSpecies = .false.
		if(present(isInterSpecies))	interSpecies = isInterSpecies

		!!Se refiere al orden de los indices para el caso del calculo de energia de acoplamiento
		couplingA = .false.
		if(present(isCouplingA)) couplingA = isCouplingA

		couplingB = .false.
		if(present(isCouplingB)) couplingB = isCouplingB


		!! Copia momentos angulares
		angularMomentA = primitiveGaussianA%angularMoment
		angularMomentB = primitiveGaussianB%angularMoment
		angularMomentC = primitiveGaussianC%angularMoment
		angularMomentD = primitiveGaussianD%angularMoment

!		write(*, "(A, I5, I5, I5, I5)"),"momentum", angularMomentA, angularMomentB, angularMomentC, angularMomentD

		!!Calcula el momento angular maximo y total
		sumAngularMoment = angularMomentA + angularMomentB + angularMomentC + angularMomentD
		maxAngularMoment = max(angularMomentA, angularMomentB, angularMomentC, angularMomentD)

		!! Selecciona el caso de permutacion a realizar
		order = LibintInterface_canonicalOrder( )

		!!Calcula el tamano del arreglo para las integrales
		numberOfCartesianA = primitiveGaussianA%numCartesianOrbital
		numberOfCartesianB = primitiveGaussianB%numCartesianOrbital
		numberOfCartesianC = primitiveGaussianC%numCartesianOrbital
		numberOfCartesianD = primitiveGaussianD%numCartesianOrbital

		!! Calcula el tamano de los arreglos
		arraySize = numberOfCartesianA * numberOfCartesianB * numberOfCartesianC * numberOfCartesianD

		otherTTotalNumberOfContractions = 0
		if(present(otherTotalNumberOfContractions)) otherTTotalNumberOfContractions = otherTotalNumberOfContractions

		!! Calcula los indices para el buffer
		if(allocated(AOIndex)) deallocate(AOIndex)
		allocate(AOIndex(arraySize))

		!! Calcula los indices de las integrales interpretandolos como un vector
		select case (order)
			case(0)
				!!(SS|SS)
				call LibintInterface_orbitalIndex(  numberOfCartesianA, numberOfCartesianB, numberOfCartesianC, numberOfCartesianD, &
													indexA, indexB, indexC, indexD, totalNumberOfContractions, otherTTotalNumberOfContractions  )
			case(1)
				!!(AB|CD)
				call LibintInterface_orbitalIndex(  numberOfCartesianA, numberOfCartesianB, numberOfCartesianC, numberOfCartesianD, &
													indexA, indexB, indexC, indexD, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(2)
				!!(BA|CD)
				call LibintInterface_orbitalIndex(  numberOfCartesianB, numberOfCartesianA, numberOfCartesianC, numberOfCartesianD, &
													indexB, indexA, indexC, indexD, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(3)
				!!(AB|DC)
				call LibintInterface_orbitalIndex(  numberOfCartesianA, numberOfCartesianB, numberOfCartesianD, numberOfCartesianC, &
													indexA, indexB, indexD, indexC, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(4)
				!!(BA|DC)
				call LibintInterface_orbitalIndex(  numberOfCartesianB, numberOfCartesianA, numberOfCartesianD, numberOfCartesianC, &
													indexB, indexA, indexD, indexC, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(5)
				!!(CD|AB)
				call LibintInterface_orbitalIndex(  numberOfCartesianC, numberOfCartesianD, numberOfCartesianA, numberOfCartesianB, &
													indexC, indexD, indexA, indexB, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(6)
				!!(DC|AB)
				call LibintInterface_orbitalIndex(  numberOfCartesianD, numberOfCartesianC, numberOfCartesianA, numberOfCartesianB, &
													indexD, indexC, indexA, indexB, totalNumberOfContractions, otherTTotalNumberOfContractions  )

			case(7)
				!!(CD|BA)
				call LibintInterface_orbitalIndex(  numberOfCartesianC, numberOfCartesianD, numberOfCartesianB, numberOfCartesianA, &
													indexC, indexD, indexB, indexA,  totalNumberOfContractions, otherTTotalNumberOfContractions )

			case(8)
				!!(DC|BA)
				call LibintInterface_orbitalIndex(  numberOfCartesianD, numberOfCartesianC, numberOfCartesianB, numberOfCartesianA, &
													indexD, indexC, indexB, indexA,  totalNumberOfContractions, otherTTotalNumberOfContractions )

		end select

		AOIndex = cartesianOrbitalIndex

	end subroutine LibintInterface_initializeShell
	!<
	!! Computa Eris, Geminales o Derivadas de repulsion segun sea el caso
	!>
	subroutine LibintInterface_compute (primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD, value)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		real(8), allocatable :: value(:)

		!!Asigna memoria e inicializa el arreglo de integrales
		if(allocated(value)) deallocate(value)
		allocate(value(arraySize))

		value = 0.0_8

		!!A trabajar!!
		select case (trim(LibintInterface_instance%job))
			case( "ERIS" )

				value = LibintInterface_computeEris(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD )

			case( "DERIV" )

				value = LibintInterface_computeDeriv(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD )

			case( "R12" )

				value = LibintInterface_computeR12(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD )

		end select

	end subroutine LibintInterface_compute

	!<
	!! Define el caso de permutacion que se debe hacer a un cuarteto de primitivas especifico para cumplir con el
	!! orden necesario para calcular integrales de libint (canonical order)
	!>
	function LibintInterface_canonicalOrder()	result(output)
		implicit none

		integer :: output

		!! el orden por defecto para libint es: a>b || c>d || c+d > a+b
		if (sumAngularMoment == 0) then
			output = 0
		else if (angularMomentA >=angularMomentB.and.angularMomentC >=angularMomentD.and.(angularMomentC+angularMomentD) >=(angularMomentA+angularMomentB)) then
			output = 1
		else if (angularMomentA < angularMomentB.and.angularMomentC >=angularMomentD.and.(angularMomentC+angularMomentD) >=(angularMomentA+angularMomentB)) then
			output = 2
		else if (angularMomentA >=angularMomentB.and.angularMomentC < angularMomentD.and.(angularMomentC+angularMomentD) >=(angularMomentA+angularMomentB)) then
			output = 3
		else if (angularMomentA < angularMomentB.and.angularMomentC < angularMomentD.and.(angularMomentC+angularMomentD) >=(angularMomentA+angularMomentB)) then
			output = 4
		else if (angularMomentA >=angularMomentB.and.angularMomentC >=angularMomentD.and.(angularMomentC+angularMomentD) < (angularMomentA+angularMomentB)) then
			output = 5
		else if (angularMomentA >=angularMomentB.and.angularMomentC < angularMomentD.and.(angularMomentC+angularMomentD) < (angularMomentA+angularMomentB)) then
			output = 6
		else if (angularMomentA < angularMomentB.and.angularMomentC >=angularMomentD.and.(angularMomentC+angularMomentD) < (angularMomentA+angularMomentB)) then
			output = 7
		else if (angularMomentA < angularMomentB.and.angularMomentC < angularMomentD.and.(angularMomentC+angularMomentD) < (angularMomentA+angularMomentB)) then
			output = 8
		end if

	end function LibintInterface_canonicalOrder

	!<
	!! Computa integrales de repulsion
	!>
	function LibintInterface_computeEris(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD ) result(output)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		real(8) :: output(arraySize)

		real(8), pointer :: integralsPtr(:)

		!!Realiza el trabajo de acuerdo con el orden calculado
		select case (order)
			case(0)
				!!La integral (00|00) es igual a prim%f(1)
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)
				output(1) = primitiveQuartet%F(1)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)
			case(1)
				!! (ab|cd)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianA%origin - primitiveGaussianB%origin
				LibintInterface_instance%libint%CD = primitiveGaussianC%origin - primitiveGaussianD%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentD, angularMomentC, angularMomentB, angularMomentA, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)

			case(2)
				!! (ba|cd)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianC, primitiveGaussianD)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianB%origin - primitiveGaussianA%origin
				LibintInterface_instance%libint%CD = primitiveGaussianC%origin - primitiveGaussianD%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentD, angularMomentC, angularMomentA, angularMomentB, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianB, primitiveGaussianA, primitiveGaussianC, primitiveGaussianD)

			case(3)
				!! (ab|dc)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianD, primitiveGaussianC)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianA%origin - primitiveGaussianB%origin
				LibintInterface_instance%libint%CD = primitiveGaussianD%origin - primitiveGaussianC%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentC, angularMomentD, angularMomentB, angularMomentA, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianA, primitiveGaussianB, primitiveGaussianD, primitiveGaussianC)

			case(4)
				!! (ba|dc)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianD, primitiveGaussianC)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianB%origin - primitiveGaussianA%origin
				LibintInterface_instance%libint%CD = primitiveGaussianD%origin - primitiveGaussianC%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentC, angularMomentD, angularMomentA, angularMomentB, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianB, primitiveGaussianA, primitiveGaussianD, primitiveGaussianC)

			case(5)
				!! (cd|ab)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianA, primitiveGaussianB)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianC%origin - primitiveGaussianD%origin
				LibintInterface_instance%libint%CD = primitiveGaussianA%origin - primitiveGaussianB%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentB, angularMomentA, angularMomentD, angularMomentC, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianC, primitiveGaussianD, primitiveGaussianA, primitiveGaussianB)

			case(6)
				!! (dc|ab)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianA, primitiveGaussianB)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianD%origin - primitiveGaussianC%origin
				LibintInterface_instance%libint%CD = primitiveGaussianA%origin - primitiveGaussianB%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentB, angularMomentA, angularMomentC, angularMomentD, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianD, primitiveGaussianC, primitiveGaussianA, primitiveGaussianB)

			case(7)
				!! (cd|ba)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianB, primitiveGaussianA)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianC%origin - primitiveGaussianD%origin
				LibintInterface_instance%libint%CD = primitiveGaussianB%origin - primitiveGaussianA%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentA, angularMomentB, angularMomentD, angularMomentC, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianC, primitiveGaussianD, primitiveGaussianB, primitiveGaussianA)

			case(8)
				!! (dc|ba)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianB, primitiveGaussianA)

				!! Asigna valores a la estrucutra Libint
				LibintInterface_instance%libint%AB = primitiveGaussianD%origin - primitiveGaussianC%origin
				LibintInterface_instance%libint%CD = primitiveGaussianB%origin - primitiveGaussianA%origin

				!! Calcula Integrales
				call LibintInterface_getEris(angularMomentA, angularMomentB, angularMomentC, angularMomentD, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

				!!Normaliza Integrales
				call LibintInterface_normalize( output, primitiveGaussianD, primitiveGaussianC, primitiveGaussianB, primitiveGaussianA)

		end select

	end function LibintInterface_computeEris

	!<
	!! Computa derivadas de repulsion de primer orden
	!! Falta implementacion completa
	!>
	function LibintInterface_computeDeriv(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD ) result(output)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		real(8) :: output(arraySize)

	end function LibintInterface_computeDeriv

	!<
	!! Computa integrales R12
	!>
	function LibintInterface_computeR12(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD ) result(output)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		real(8) :: output(arraySize)

		real(8), pointer :: integralsPtr(:)

		!!Realiza el trabajo de acuerdo con el orden calculado
		select case (order)
			case(0)
				!!(00|00)
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)

				output(1) = primitiveQuartet%F(1)

			case(1)
				!! (ab|cd)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentD, angularMomentC, angularMomentB, angularMomentA, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(2)
				!! (ba|cd)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianC, primitiveGaussianD)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianC, primitiveGaussianD)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentD, angularMomentC, angularMomentA, angularMomentB, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(3)
				!! (ab|dc)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianD, primitiveGaussianC)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianD, primitiveGaussianC)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentC, angularMomentD, angularMomentB, angularMomentA, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(4)
				!! (ba|dc)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianD, primitiveGaussianC)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianB, primitiveGaussianA, primitiveGaussianD, primitiveGaussianC)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentC, angularMomentD, angularMomentA, angularMomentB, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(5)
				!! (cd|ab)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianA, primitiveGaussianB)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianA, primitiveGaussianB)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentB, angularMomentA, angularMomentD, angularMomentC, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(6)
				!! (dc|ab)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianA, primitiveGaussianB)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianA, primitiveGaussianB)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentB, angularMomentA, angularMomentC, angularMomentD, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(7)
				!! (cd|ba)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianB, primitiveGaussianA)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianC, primitiveGaussianD, primitiveGaussianB, primitiveGaussianA)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentA, angularMomentB, angularMomentD, angularMomentC, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

			case(8)
				!! (dc|ba)
				!! Construye la estructura primQuartet
				call LibintInterface_buildPrimQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianB, primitiveGaussianA)

				!! Construye la estructura shellQuartet
				call LibintInterface_buildShellQuartet(primitiveGaussianD, primitiveGaussianC, primitiveGaussianB, primitiveGaussianA)

				!! Calcula Integrales
				call LibintInterface_getR12(angularMomentA, angularMomentB, angularMomentC, angularMomentD, integralsPtr )
				output(1:arraySize) = integralsPtr(1:arraySize)

		end select

	end function LibintInterface_computeR12

	!<
	!! Construye los indices para el buffer de integrales
	!>
	subroutine LibintInterface_orbitalIndex( cartesianA, cartesianB, cartesianC, cartesianD, &
											 indexA, indexB, indexC, indexD, &
											 totalNumberOfContractions, otherNumberOfContractions )
		implicit none

		integer, intent(in) :: cartesianA, cartesianB, cartesianC, cartesianD
		integer, intent(in) :: indexA, indexB, indexC, indexD
		integer, intent(in) :: totalNumberOfContractions
		integer, intent(in) :: otherNumberOfContractions

		integer :: i, j, k, l
		integer	:: p1, p2, p3, p4
		integer	:: a, b, c, d
		integer(8) :: auxIndex

		if (allocated(cartesianOrbitalIndex)) deallocate(cartesianOrbitalIndex)
		allocate(cartesianOrbitalIndex(arraySize))

		do a = 1,CartesianA
			p1 = (a-1) * CartesianB
			do b = 1, CartesianB
				p2 = (p1 + b-1) * CartesianC
				do c = 1, CartesianC
					p3 = (p2 + c-1) * CartesianD
					do d = 1, CartesianD
						p4 = p3 + d

						!! Se calculan los índices del tensor...
						i = a +(indexA - 1)
						j = b +(indexB - 1)
						k = c +(indexC - 1)
						l = d +(indexD - 1)

						!! Se calculan los indices del vector...
						if(couplingB) then
							auxIndex = IndexMap_tensorR4ToVector3(k,l,i,j, otherNumberOfContractions, totalNumberOfContractions, order )
							cartesianOrbitalIndex(p4) = auxIndex
							cycle
						end if

						if(couplingA) then
							auxIndex = IndexMap_tensorR4ToVector3(i,j,k,l, totalNumberOfContractions, otherNumberOfContractions, order )
							cartesianOrbitalIndex(p4) = auxIndex
							cycle
						end if

						if(interSpecies) then
							auxIndex = IndexMap_tensorR4ToVector2(i,j,k,l, totalNumberOfContractions, otherNumberOfContractions, order )
							cartesianOrbitalIndex(p4) = auxIndex
							cycle
						end if

						auxIndex = IndexMap_tensorR4ToVector(i,j,k,l, totalNumberOfContractions )
						cartesianOrbitalIndex(p4) = auxIndex

					end do
				end do
			end do
		end do

	end subroutine LibintInterface_orbitalIndex

	!<
	!! Construye la estructura primquartet
	!>
	subroutine LibintInterface_buildPrimQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD

		real(8), allocatable :: incompletGamma(:)
		real(8) :: P(3), Q(3), W(3), AB(3), CD(3), PQ(3)
		real(8) :: zeta, eta, rho
		real(8) :: s1234, s12, s34, AB2, CD2, PQ2
		real(8) :: incompletGammaArgument
		real(8) :: k1234
		integer :: i

		!!Exponentes
		zeta = primitiveGaussianA%orbitalExponent + primitiveGaussianB%orbitalExponent
		eta = primitiveGaussianC%orbitalExponent + primitiveGaussianD%orbitalExponent
		rho  = (zeta * eta) / (zeta + eta) !Exponente reducido ABCD

		!!prim_data.U
		P  = ((primitiveGaussianA%orbitalExponent * primitiveGaussianA%origin) + (primitiveGaussianB%orbitalExponent * primitiveGaussianB%origin)) / zeta
		Q  = ((primitiveGaussianC%orbitalExponent * primitiveGaussianC%origin) + (primitiveGaussianD%orbitalExponent * primitiveGaussianD%origin)) / eta
		W  = ((zeta * P) + (eta * Q)) / (zeta + eta)
		PQ = P - Q
		primitiveQuartet%U(1:3,1)= (P - primitiveGaussianA%origin)
		primitiveQuartet%U(1:3,3)= (Q - primitiveGaussianC%origin)
		primitiveQuartet%U(1:3,5)= (W - P)
		primitiveQuartet%U(1:3,6)= (W - Q)

		!!Distancias AB, CD, ABCD(PQ2)
		AB = primitiveGaussianA%origin - primitiveGaussianB%origin
		CD = primitiveGaussianC%origin - primitiveGaussianD%origin

		AB2 = dot_product(AB, AB)
		CD2 = dot_product(CD, CD)
		PQ2 = dot_product(PQ, PQ)

		!!Evalua el argumento de la funcion gamma incompleta
		incompletGammaArgument = rho*PQ2

		!!Overlap Factor
		s12 = ((Math_PI/zeta)**1.5_8) * exp(-((primitiveGaussianA%orbitalExponent * primitiveGaussianB%orbitalExponent) / zeta) * AB2)
		s34 = ((Math_PI/ eta)**1.5_8) * exp(-((primitiveGaussianC%orbitalExponent * primitiveGaussianD%orbitalExponent) /  eta) * CD2)
		s1234 = sqrt(rho/Math_PI) * s12 * s34

		primitiveQuartet%F = 0.0_8

		select case (trim (LibintInterface_instance%job))

			case ( "ERIS" )

				if (allocated(incompletGamma)) deallocate(incompletGamma)
				allocate(incompletGamma(0:sumAngularMoment))

				incompletGamma = 0.0_8

				call Math_fgamma0(sumAngularMoment,incompletGammaArgument,incompletGamma)

				!!prim_data.F
				primitiveQuartet%F(1:sumAngularMoment+1) = 2.0_8 * incompletGamma(0:sumAngularMoment) * s1234

				!!Datos restantes para prim.U
				primitiveQuartet%oo2z = (0.5_8 / zeta)
				primitiveQuartet%oo2n = (0.5_8 / eta)
				primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
				primitiveQuartet%poz = (rho/zeta)
				primitiveQuartet%pon = (rho/eta)
				primitiveQuartet%oo2p = (0.5_8 / rho)

			case( "DERIV" )

				if (allocated(incompletGamma)) deallocate(incompletGamma)
				allocate(incompletGamma(0:sumAngularMoment+1))

				incompletGamma = 0.0_8

				call Math_fgamma0(sumAngularMoment+1,incompletGammaArgument,incompletGamma)

				!!prim_data.F
				primitiveQuartet%F = 2.0_8 * incompletGamma * s1234

				!!Datos restantes para prim.U
				primitiveQuartet%U(1:3,2) = P - primitiveGaussianB%origin
				primitiveQuartet%U(1:3,4) = Q - primitiveGaussianD%origin
				primitiveQuartet%twozeta_a = 2.0_8 * primitiveGaussianA%orbitalExponent
				primitiveQuartet%twozeta_b = 2.0_8 * primitiveGaussianB%orbitalExponent
				primitiveQuartet%twozeta_c = 2.0_8 * primitiveGaussianC%orbitalExponent
				primitiveQuartet%twozeta_d = 2.0_8 * primitiveGaussianD%orbitalExponent
				primitiveQuartet%oo2z = (0.5_8 / zeta)
				primitiveQuartet%oo2n = (0.5_8 / eta)
				primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
				primitiveQuartet%poz = (rho/zeta)
				primitiveQuartet%pon = (rho/eta)
				primitiveQuartet%oo2p = (0.5_8 / rho)

			case( "R12" )

				if (allocated(incompletGamma)) deallocate(incompletGamma)
				allocate(incompletGamma(0:sumAngularMoment+1))

				incompletGamma = 0.0_8

				call Math_fgamma0(sumAngularMoment+1,incompletGammaArgument,incompletGamma)

				!!prim_data.F
				primitiveQuartet%F = 2.0_8 * incompletGamma * s1234

				!!Datos restantes para prim.U
				primitiveQuartet%U(1:3,2) = Q - primitiveGaussianA%origin
				primitiveQuartet%U(1:3,4) = P - primitiveGaussianC%origin
				primitiveQuartet%twozeta_a = 2.0_8 * primitiveGaussianA%orbitalExponent
				primitiveQuartet%twozeta_b = 2.0_8 * primitiveGaussianB%orbitalExponent
				primitiveQuartet%twozeta_c = 2.0_8 * primitiveGaussianC%orbitalExponent
				primitiveQuartet%twozeta_d = 2.0_8 * primitiveGaussianD%orbitalExponent
				primitiveQuartet%oo2z = (0.5_8 / zeta)
				primitiveQuartet%oo2n = (0.5_8 / eta)
				primitiveQuartet%oo2zn = (0.5_8 / (zeta + eta))
				primitiveQuartet%poz = (rho/zeta)
				primitiveQuartet%pon = (rho/eta)
				primitiveQuartet%oo2p = (0.5_8 / rho)
			   	primitiveQuartet%ss_r12_ss = (1.0_8 / rho) * incompletGamma(1) + PQ2 * (incompletGamma(1)-incompletGamma(2))

		   	end select

	end subroutine LibintInterface_buildPrimQuartet

	!<
	!! Construye la estructura shellQuartet
	!! @warning son datos de la capa... debe pasarse al initialize y programar segun order!!!!
	!>
	subroutine LibintInterface_buildShellQuartet(primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD

		shellQuartet%AB = primitiveGaussianA%origin - primitiveGaussianB%origin
		shellQuartet%CD = primitiveGaussianC%origin - primitiveGaussianD%origin
		shellQuartet%AC = primitiveGaussianA%origin - primitiveGaussianC%origin
		shellQuartet%ABdotAC = dot_product(shellQuartet%AB, shellQuartet%AC)
		shellQuartet%CDdotCA = dot_product(shellQuartet%CD, -shellQuartet%AC)

	end subroutine LibintInterface_buildShellQuartet

	!<
	!! Normaliza las integrales de un cuarteto de primitivas
	!>
	subroutine LibintInterface_normalize(integrals, primitiveGaussianA, primitiveGaussianB, primitiveGaussianC, primitiveGaussianD)
		implicit none

		type(PrimitiveGaussian), intent(in)	:: primitiveGaussianA
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianB
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianC
		type(PrimitiveGaussian), intent(in) :: primitiveGaussianD
		real(8), intent(inout) :: integrals(arraySize)

		integer :: i, j, k, l
		integer :: m

		m = 0

		do i = 1, primitiveGaussianA%numCartesianOrbital
			do j = 1, primitiveGaussianB%numCartesianOrbital
				do k = 1,primitiveGaussianC%numCartesianOrbital
					do l = 1, primitiveGaussianD%numCartesianOrbital
						m = m + 1
						integrals(m) = integrals(m) * primitiveGaussianA%normalizationConstant(i) &
											  		* primitiveGaussianB%normalizationConstant(j) &
											  		* primitiveGaussianC%normalizationConstant(k) &
											  		* primitiveGaussianD%normalizationConstant(l)
					end do
				end do
			end do
		end do

	end subroutine

	!!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function LibintInterface_isInstanced( ) result( output )
		implicit  none

		logical :: output
		
		output = LibintInterface_instance%isInstanced

	end function LibintInterface_isInstanced

	!<
	!! Inicializa Libint
	!>
	subroutine LibintInterface_initializeLibInt()

		integer(kind=c_int)	:: libIntStorage
		integer(kind=c_int)	:: maxAngMoment
		integer(kind=c_int)	:: numberOfPrimitives

		maxAngMoment = LibintInterface_instance%maxAngularMoment
		numberOfPrimitives= LibintInterface_instance%numberOfPrimitives

		libIntStorage = LibintInterface_initLibInt(LibintInterface_instance%libint, maxAngMoment, numberOfPrimitives)

		LibintInterface_instance%libintStorage = libIntStorage

	end subroutine LibintInterface_initializeLibInt

	!<
	!! Termina Libint
	!>
	subroutine LibintInterface_terminateLibInt()

		call LibintInterface_freeLibInt(LibintInterface_instance%libint)

	end subroutine LibintInterface_terminateLibInt

	!<
	!! Calcula Integrales de repulsion, solo funciona para una combinacion de primitivas a la vez
	!>
	subroutine LibintInterface_getEris(angMomentD, angMomentC, angMomentB, angMomentA, integralsPtr )
		implicit none

		integer, intent(in) :: angMomentD, angMomentC, angMomentB, angMomentA
		real(8), dimension(:), pointer :: integralsPtr

		procedure(LibintInterface_buildLibInt), pointer :: pBuild
		type(c_ptr) :: resultPc
		real(8), dimension(:), pointer :: temporalPtr
		integer :: arraySsize(1)

		arraySsize(1) = arraySize

		LibintInterface_instance%libint%PrimQuartet = c_loc(primitiveQuartet)

		call c_f_procpointer(build_eri(angMomentD,angMomentC,angMomentB,angMomentA),pBuild)

		resultPc = pBuild(LibintInterface_instance%libint,1)

		call c_f_pointer(resultPc, temporalPtr, arraySsize)

		integralsPtr => temporalPtr

	end subroutine LibintInterface_getEris

	!<
	!! Inicializa Libderiv
	!! Arreglar!!!!!
	!>
	subroutine LibintInterface_initializeLibDeriv()

		integer(kind=c_int) :: lib_deriv_storage
		integer(kind=c_int) :: maxAngularMoment
		integer(kind=c_int) :: max_classes
		integer(kind=c_int) :: max_prim_local

!		max_am_local= max_am
!		max_prim = 1
!		max_classes = (((max_am + 1)*(max_am + 2))/2)**4
!
!		lib_deriv_storage = init_deriv1(deriv, max_am_local, max_prim, max_classes)

	end subroutine LibintInterface_initializeLibDeriv

	!<
	!! Termina Libderiv
	!! Arreglar!!!!
	!>
	subroutine LibintInterface_terminateLibDeriv(deriv)
		type(lib_deriv) :: deriv

		call LibintInterface_freeLibDeriv(deriv)

	end subroutine LibintInterface_terminateLibDeriv

	!<
	!! Calcula Derivadas de repulsion, solo funciona para una combinacion de primitivas a la vez
	!! Calcula solo primeras derivadas por ahora
	!! Falta implementar adecuadamente
	!!
	!! @param(x) Numero de orbitales cartesianos
	!>
	subroutine LibintInterface_getDerivs(n_d, n_c, n_b, n_a, numberOfCartesianA, numberOfCartesianB, numberOfCartesianC, numberOfCartesianD, deriv, prim, work_forces, a_mysize)
		integer, intent(in)                        :: n_d, n_c, n_b, n_a
		integer(8), intent(in)                     :: numberOfCartesianA, numberOfCartesianB, numberOfCartesianC, numberOfCartesianD
		type(lib_deriv)                            :: deriv
		type(prim_data), target                    :: prim
		real(8), dimension(numberOfCartesianA*numberOfCartesianB*numberOfCartesianC*numberOfCartesianD,12) :: work_forces
		integer                                    :: a_mysize(1)

!		procedure(build_deriv1), pointer           :: pbuild_deriv1
!		type(c_ptr)                                :: pc_result
!		real(c_double), dimension(:), pointer      :: tmp_data
!		integer                                    :: i, k
!
!		deriv%PrimQuartet = c_loc(prim)
!		call c_f_procpointer(build_deriv1_eri(n_d,n_c,n_b,n_a),pbuild_deriv1)
!		call pbuild_deriv1(deriv,1)
!
!		do k=1,12
!			if(k==4 .or. k==5 .or. k==6) cycle
!			pc_result = deriv%ABCD(k)
!			call c_f_pointer(pc_result, tmp_data , a_mysize)
!			do i=1,a_mysize(1)
!				work_forces(i,k) = tmp_data(i)
!			end do
!		end do

	end subroutine LibintInterface_getDerivs

	!<
	!! Inicializa Libr12
	!>
	subroutine LibintInterface_initializeLibR12()
		implicit none

		integer(kind=c_int)	:: libR12Storage
		integer(kind=c_int)	:: maxAngMoment
		integer(kind=c_int)	:: numberOfPrimitives
		integer(kind=c_int) :: maxCartesianOrbitals

		maxAngMoment = LibintInterface_instance%maxAngularMoment
		numberOfPrimitives= LibintInterface_instance%numberOfPrimitives

		maxCartesianOrbitals = (((maxAngMoment + 1)*(maxAngMoment + 2))/2)**4

		libR12Storage = LibintInterface_initLibR12(LibintInterface_instance%libr12, maxAngMoment, numberOfPrimitives, maxCartesianOrbitals)

		LibintInterface_instance%libintStorage = libR12Storage

	end subroutine LibintInterface_initializeLibR12

	!<
	!! Termina Libr12
	!>
	subroutine LibintInterface_terminateLibR12()
		implicit none

		call LibintInterface_freeLibR12(LibintInterface_instance%libr12)

	end subroutine LibintInterface_terminateLibR12

	!<
	!! Calcula Integrales 1 = [Eris], 2 = [r12], 3 = [r12, t1], 4 = [r12, t2].
	!! Solo funciona para una combinacion de primitivas a la vez
	!>
	subroutine LibintInterface_getR12(angMomentD, angMomentC, angMomentB, angMomentA, integralsPtr )
		implicit none

		integer, intent(in) :: angMomentD, angMomentC, angMomentB, angMomentA
		real(8), dimension(:), pointer :: integralsPtr

		procedure(LibintInterface_buildLibR12), pointer :: pBuildR12
		type(c_ptr) :: resultPc
		real(8), dimension(:), pointer :: temporalPtr
		integer :: arraySsize(1)

		arraySsize(1) = arraySize

		LibintInterface_instance%libr12%ShellQuartet = shellQuartet
		LibintInterface_instance%libr12%PrimQuartet = c_loc(primitiveQuartet)

		call c_f_procpointer(build_r12_grt(angMomentD,angMomentC,angMomentB,angMomentA),pBuildR12)

		call pBuildR12(LibintInterface_instance%libr12,1)

		!! 1 = [Eris], 2 = [r12], 3 = [r12, t1], 4 = [r12, t2]
		resultPc = LibintInterface_instance%libr12%te_ptr(1)

		call c_f_pointer(resultPc, temporalPtr, arraySsize)

		integralsPtr => temporalPtr

	end subroutine LibintInterface_getR12
	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine LibintInterface_exception( typeMessage, description, debugDescription)
		implicit none

		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription

		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )

	end subroutine LibintInterface_exception

end module LibintInterface_
