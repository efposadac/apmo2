!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************
!**
! @brief  Modulo para manejo de cadenas de caracateres
!
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creaciacion : </b> 2008-18-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-18-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y procedimientos b�sicos
!**
module String_
	use Matrix_
	use Vector_
	
	implicit none
	
	type, public :: String
		character(30) :: name
		
	end type
	
	public :: &
		String_constructor, &
		String_destructor, &
		String_convertRealToString, &
		String_convertVectorOfRealsToString, &
		String_convertIntegerToString, &
		String_getLowercase, &
		String_getUppercase, &
         String_findSubstring


	private	
contains
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine String_constructor()
		implicit none
		
	end subroutine String_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine String_destructor()
		implicit none
	
	end subroutine String_destructor
	
	!**
	! @brief Convierte un vector de reales de doble precision en una cadena de caracteres
	!**
	function String_convertVectorOfRealsToString( vectorOfReals ) result( output )
		implicit none
		type(Vector), intent(in) :: vectorOfReals
		character(20), allocatable :: output(:)
		
		integer:: orderOfMatrix
		integer:: i
		
		orderOfMatrix=size(vectorOfReals%values )
					
		if ( allocated( output ) ) deallocate( output )
		allocate( output( orderOfMatrix ) )
				
		do i=1, orderOfMatrix
			
			write (output(i)," (F12.4)") vectorOfReals%values(i)
			
		end do
		
	end function String_convertVectorOfRealsToString
	
	
	!**
	! @brief Conviete un real de doble precision en una cadena de caracteres
	!**
	function String_convertRealToString( realValue ) result(output)
		implicit none
		real(8), intent(in) :: realValue
		character(12):: output
	
		write (output," (F12.4)") realValue
	
	end function String_convertRealToString
	
	!**
	! @brief Conviete un entero en una cadena de caracteres
	!**
	function String_convertIntegerToString( integerValue ) result(output)
		implicit none
		integer, intent(in) :: integerValue
		character(5):: output
		
		write (output,'(I5)') integerValue
               output=adjustl(output)

	end function String_convertIntegerToString

	!>
	!! @brief Devuelve una cadena de entrada en minuscula
	!>
	function String_getLowercase( inputString, from, to ) result( output )
		implicit none
		character(*):: inputString
		integer, optional :: from
		integer, optional :: to
		character(255):: output

		integer:: ffrom
		integer:: tto
		integer:: i

		ffrom = 1
		tto =  len_trim(inputString)

		if(present(from)) ffrom = from
		if(present(to)) tto = to

		output=trim(inputString)

		do i=ffrom, tto
			output(i:i) = char( IOR( ichar(inputString (i:i) ),32) ) 
    	end do

	end function String_getLowercase
	
	!>
	!! @brief Devuelve una cadena de entrada en mayuscula
	!>
	function String_getUppercase( inputString, from, to ) result( output )
		implicit none
		character(*):: inputString
		integer, optional :: from
		integer, optional :: to
		character(255):: output

		integer:: ffrom
		integer:: tto
		integer:: i
		integer::j
		
		ffrom = 1
		tto =  len_trim(inputString)

		if(present(from)) ffrom = from
		if(present(to)) tto = to

		output=trim(inputString) 
		do i=ffrom, tto
                         j=ichar(inputString (i:i))
                         if ( j>96 ) output(i:i) = char( XOR( j,32) )

    		end do

	end function String_getUppercase


    !>
	!! @brief encuentra la posicion de primera coincidencia de una cadena especificada
    !!        e indica el posible numero de coincidencias en la cadena.
	!>
	function String_findSubstring( inputString, substring, probabilityOfCoincidence ) result(output)
		implicit none
		character(*):: inputString
		character(*):: substring
                integer, optional, intent(inout) :: probabilityOfCoincidence
                integer:: output

		integer:: i
		integer::j
		integer::k
                integer, allocatable :: mark(:)
                logical:: areCoincidence
                integer:: probOfCoincidence

               output=0
               k=len(inputString)

               !! Separa memoria para el maximo numero de posible coincidencias entre cadenas
               allocate( mark( k ) )


               !!**************************************************************
               !! Busca los indices de las posibles posiciones de coincidencia
               !!*****
               i=0
               do j=1,len(inputString)

                         if( inputString(j:j) == substring(1:1) ) then 
                              i=i+1
                              mark(i) = j
                         end if
               end do
               !!
               !!**************************************************************
               probOfCoincidence=i
               if (present(probabilityOfCoincidence)) probabilityOfCoincidence=probOfCoincidence

               !!**************************************************************
               !!  Rechaza los posible indice que no podrian servir
               !!********
               !!!!???????????????????????????
               !!
               !!**************************************************************


               !!**************************************************************
               !! inicia la busqueda de la primera coincidencias
               !!*******
               do  i=1, probOfCoincidence

                    areCoincidence=.true.
                    do j=2,len(substring)
                         k=mark(i)+j-1
                         if( inputString(k:k)/=substring(j:j) ) then
                              areCoincidence=.false.
                              exit
                         end if
                    end do

                    if(areCoincidence) exit

               end do
               !!
               !!**************************************************************

               output=mark(i)

	end function String_findSubstring
	
end module String_
