!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia
!!    http://www.unal.edu.co
!!
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>
!!    Keywords:
!!
!!    thisPointer files is part of nonBOA
!!
!!    thisPointer program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    thisPointer program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with thisPointer program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************

!**
! @brief Modulo para diagonalizacion de matrices de especies cuanticas
! 
!  Este modulo define una seudoclase para realizacion de ciclos SCF de
! especies cu�nticas.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-30
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulo y m�todos
!
!**
module SCFIntraspecies_
	use Matrix_
	use Vector_
	use Exception_
	use SCFConvergence_
	use List_
	use ParticleManager_
	use MolecularSystem_
	use WaveFunction_
	use InputOutput_
	implicit none

	
	!< enum Matrix_type {
	integer, parameter, public :: SCF_INTRASPECIES_CONVERGENCE_FAILED		=  0
	integer, parameter, public :: SCF_INTRASPECIES_CONVERGENCE_CONTINUE	=  1
	integer, parameter, public :: SCF_INTRASPECIES_CONVERGENCE_SUCCESS	=  2
	!< }
	
	type, public :: SCFIntraspecies
		
		character(30) :: name
		
		!!**************************************************************
		!! Matrices requeridas y alteradas en la realizacion del ciclo SCF
		!!
		type(Matrix), pointer :: fockMatrixPtr
		type(Matrix), pointer :: densityMatrixPtr
		type(Matrix), pointer :: transformationMatrixPtr
		type(Matrix), pointer :: waveFunctionCoefficientsPtr
		type(Vector), pointer :: molecularOrbitalsEnergyPtr
		!!**************************************************************
		
		logical, pointer :: wasBuiltFockMatrixPtr
		
		!!**************************************************************
		!!  Variables y objetos asociados al m�todo SCF
		!!
		integer :: numberOfIterations
		type(List) :: energySCF
		type(List) :: standartDesviationOfDensityMatrixElements
		type(List) :: diisError
		type(SCFConvergence) :: convergenceMethod
		!!**************************************************************
		
	end type SCFIntraspecies
	
	private ::
		type(matrix) :: auxMatrix
	
	
	type(SCFIntraspecies), public, allocatable, target :: SCFIntraspecies_instance(:)
	
	
	public :: &
		SCFIntraspecies_constructor, &
		SCFIntraspecies_destructor, &
		SCFIntraspecies_showIteratiosStatus, &
		SCFIntraspecies_setNumberOfIterations,&
		SCFIntraspecies_getNumberOfIterations,&
		SCFIntraspecies_getLastEnergy, &
		SCFIntraspecies_getStandardDeviationOfDensistyMatrix,&
		SCFIntraspecies_getDiisError, &
		SCFIntraspecies_iterate, &
		SCFIntraspecies_restart, &
		SCFIntraspecies_reset, &
		SCFIntraspecies_actualizeDensityMatrix, &
		SCFIntraspecies_testEnergyChange, &
		SCFIntraspecies_testDensityMatrixChange

		
		
	
contains
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine SCFIntraspecies_constructor( )
		implicit none
			
		integer :: speciesIterator
		integer :: specieID
		integer(8) :: numberOfContractions
		character(30) :: specieName
		
		call SCFIntraspecies_destructor()
		
		call WaveFunction_constructor()
		allocate( SCFIntraspecies_instance( ParticleManager_getNumberOfQuantumSpecies() ) )
		
		do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
			
			
			numberOfContractions =  ParticleManager_getNumberOfContractions( speciesIterator )
			specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
			
			SCFIntraspecies_instance( specieID ).name = trim( ParticleManager_getNameOfSpecie( specieID) )
			SCFIntraspecies_instance( specieID ).numberOfIterations = 0
			SCFIntraspecies_instance( specieID ).fockMatrixPtr => WaveFunction_getFockMatrixPtr( specieID )
			SCFIntraspecies_instance( specieID ).densityMatrixPtr => WaveFunction_getDensityMatrixPtr( specieID )
			SCFIntraspecies_instance( specieID ).transformationMatrixPtr =>WaveFunction_getTransformationMatrixPtr( specieID )
			SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr => WaveFunction_getWaveFunctionCoefficientsPtr( specieID )
			SCFIntraspecies_instance( specieID ).molecularOrbitalsEnergyPtr => WaveFunction_getMolecularOrbitalsEnergyPtr( specieID )
			SCFIntraspecies_instance( specieID ).wasBuiltFockMatrixPtr => WaveFunction_getBuiltStatePtr( specieID )
			
			call List_constructor( SCFIntraspecies_instance( specieID ).energySCF,"energy", APMO_instance.LISTS_SIZE )
			call List_constructor( SCFIntraspecies_instance( specieID ).diisError,"diisError", APMO_instance.LISTS_SIZE )
			call List_constructor( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements, &
				"densitySD",APMO_instance.LISTS_SIZE )

			!! Instancia un objeto para manejo de aceleracion y convergencia del metodo SCF
			Call SCFConvergence_constructor(SCFIntraspecies_instance(specieID).convergenceMethod, &
				SCFIntraspecies_instance( specieID ).name, APMO_instance.CONVERGENCE_METHOD)

		end do
			
	end subroutine SCFIntraspecies_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine SCFIntraspecies_destructor( )
		implicit none
				
		integer :: speciesIterator
		integer :: specieID
				
		if ( allocated( SCFIntraspecies_instance) ) then
		
		
			do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
				
				specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
				
				call SCFConvergence_destructor( SCFIntraspecies_instance( specieID ).convergenceMethod )
				call List_destructor( SCFIntraspecies_instance( specieID ).energySCF )
				call List_destructor( SCFIntraspecies_instance( specieID ).diisError )
				call List_destructor( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
			
				SCFIntraspecies_instance( specieID ).fockMatrixPtr => null()
				SCFIntraspecies_instance( specieID ).densityMatrixPtr => null()
				SCFIntraspecies_instance( specieID ).transformationMatrixPtr => null()
				SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr => null()
				SCFIntraspecies_instance( specieID ).molecularOrbitalsEnergyPtr => null()
				SCFIntraspecies_instance( specieID ).wasBuiltFockMatrixPtr => null()
	
			end do
			
			deallocate( SCFIntraspecies_instance)
			call WaveFunction_destructor()
	
		end if

	end subroutine SCFIntraspecies_destructor
	
	!**
	! @brief 	Retorna el numero de iteraciones realizadas para una especie especificada
	!
	!**
	subroutine SCFIntraspecies_showIteratiosStatus( status, nameOfSpecie ) 
		implicit none
		character(*), intent(in) :: nameOfSpecie
		integer :: status

		select case(status)
		
			case(SCF_INTRASPECIES_CONVERGENCE_FAILED)
				
				print *,""
				print *,"SCF STATUS: CONVERGENCE FAILED BY: ",trim(nameOfSpecie)
				print *,""
			
			case(SCF_INTRASPECIES_CONVERGENCE_CONTINUE)
				
				print *,""
				print *,"SCF STATUS: CONVERGENCE CONTINUE BY: ",trim(nameOfSpecie)
				print *,""
			
			case(SCF_INTRASPECIES_CONVERGENCE_SUCCESS)
				
				print *,""
				print *,"SCF STATUS: CONVERGENCE SUCCESS BY: ",trim(nameOfSpecie)
				print *,""
	
		end select
	
		
	end subroutine SCFIntraspecies_showIteratiosStatus

	!**
	! @brief 	Retorna el numero de iteraciones realizadas para una especie especificada
	!
	!**
	subroutine SCFIntraspecies_setNumberOfIterations( numberOfIterations,  nameOfSpecie )
		implicit none
		integer, intent(in) :: numberOfIterations
		character(*), intent(in), optional :: nameOfSpecie
	
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		SCFIntraspecies_instance( specieID ).numberOfIterations = numberOfIterations
		
	
	end subroutine SCFIntraspecies_setNumberOfIterations
	

	!**
	! @brief 	Retorna el numero de iteraciones realizadas para una especie especificada
	!
	!**
	function SCFIntraspecies_getNumberOfIterations( nameOfSpecie ) result( output )
		implicit none
		character(*), intent(in), optional :: nameOfSpecie
		integer :: output
	
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		output = SCFIntraspecies_instance( specieID ).numberOfIterations
		
	
	end function SCFIntraspecies_getNumberOfIterations
	
	!**
	! @brief Retorna la energia calculada en la ultima iteracion
	!**
	function SCFIntraspecies_getLastEnergy( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		real(8) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		!! Obtiene el valor de energia de la ultima iteracion
		call List_end( SCFIntraspecies_instance( specieID ).energySCF )
		output= List_current( SCFIntraspecies_instance( specieID ).energySCF )
		
	end function SCFIntraspecies_getLastEnergy
	
	!**
	! @brief Retorna de la densviacion estandar de la matrix de densidad en la ultima iteracion
	!**
	function SCFIntraspecies_getStandardDeviationOfDensistyMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		real(8) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		
		call List_end( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
		output= List_current( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
		
	end function SCFIntraspecies_getStandardDeviationOfDensistyMatrix
	
	!**
	! @brief Retorna de la densviacion estandar de la matrix de densidad en la ultima iteracion
	!**
	function SCFIntraspecies_getDiisError( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		real(8) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		
		call List_end( SCFIntraspecies_instance( specieID ).diisError )
		output= List_current( SCFIntraspecies_instance( specieID ).diisError )
		
	end function SCFIntraspecies_getDiisError


	!>
	!! @brief Realiza iteraci�n SCF
	!!
	!! @param nameOfSpecie nombre de la especie seleccionada.
	!!
	!! @warning Se debe garantizar la actualizacion de la matriz de densidad  y calculos de esta derivados
	!!	cuando se emplee este metodo con el parametro actualizeDensityMatrix=.false.
	!<
	subroutine SCFIntraspecies_iterate( nameOfSpecie,  actualizeDensityMatrix ) 
		implicit none
		character(*), optional :: nameOfSpecie
		logical,optional :: actualizeDensityMatrix
		
		integer :: specieID
		integer :: orderOfMatrix 
		type(Matrix) :: fockMatrixTransformed
		type(Matrix) :: beforeDensityMatrix, auxMatrix
		type(Vector) :: beforeMolecularOrbitalsEnergy, auxVector
		character(30) :: nameOfSpecieSelected
		logical :: internalActualizeDensityMatrix

		integer(8) :: numberOfMatrixElements
		integer :: numberOfContractions
		integer :: st(1), pt(3), dt(6), ft(10), gt(15), ht(21)
		integer :: angularMoment
		integer :: i, j, k, u, v
		integer, allocatable :: trans(:)
		
		type(Exception) :: ex
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		internalActualizeDensityMatrix = .true.
		if ( present(  actualizeDensityMatrix ) ) internalActualizeDensityMatrix =  actualizeDensityMatrix
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )
		
		if ( .not.SCFIntraspecies_instance( specieID ).wasBuiltFockMatrixPtr ) call WaveFunction_builtFockMatrix( nameOfSpecieSelected )
	
		if ( .not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS ) then
     
			call SCFIntraspecies_convergenceMethod( specieID )

			call Matrix_copyConstructor( fockMatrixTransformed, SCFIntraspecies_instance( specieID ).fockMatrixPtr )

			fockMatrixTransformed.values = &
				matmul( matmul( transpose( SCFIntraspecies_instance( specieID ).transformationMatrixPtr.values ) , &
					fockMatrixTransformed.values), SCFIntraspecies_instance( specieID ).transformationMatrixPtr.values )


			!! Calcula valores y vectores propios de matriz de Fock transformada.
			call Matrix_eigen( fockMatrixTransformed, SCFIntraspecies_instance( specieID ).molecularOrbitalsEnergyPtr, &
						SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr, SYMMETRIC )

			!! Calcula los  vectores propios para matriz de Fock
			SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr.values = 		&
					matmul( SCFIntraspecies_instance( specieID ).transformationMatrixPtr.values , 	&
					SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr.values )
				
			if (  internalActualizeDensityMatrix == .true. ) then
				!! Determina la desviacion estandar de los elementos de la matriz de densidad
				call Matrix_copyConstructor( beforeDensityMatrix, SCFIntraspecies_instance( specieID ).densityMatrixPtr )
				call WaveFunction_builtDensityMatrix( nameOfSpecieSelected )
				call List_push_back( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements, &
						Matrix_standardDeviation( beforeDensityMatrix, SCFIntraspecies_instance( specieID ).densityMatrixPtr ) )
				
				!! Calcula energia total para la especie especificada
				call WaveFunction_obtainEnergy( nameOfSpecieSelected )
				call List_push_back( SCFIntraspecies_instance( specieID ).energySCF, WaveFunction_getEnergy( specieID ) )
				call List_push_back( SCFIntraspecies_instance( specieID ).diisError, &
						SCFConvergence_getDiisError( SCFIntraspecies_instance( specieID ).convergenceMethod) )
				call Matrix_destructor( beforeDensityMatrix )
				
			end if
				
			!! Actualiza el contador para el numero de iteraciones SCF de la especie actual
			SCFIntraspecies_instance( specieID ).numberOfIterations =  SCFIntraspecies_instance( specieID ).numberOfIterations + 1
			
			call Matrix_destructor(fockMatrixTransformed)
			
		else
			
			orderOfMatrix = ParticleManager_getTotalNumberOfContractions( specieID )
			numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
			
			beforeDensityMatrix = InputOutput_getMatrix(orderOfMatrix, orderOfMatrix, &
				file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".vec", &
				binary = .true.)
			beforeMolecularOrbitalsEnergy = InputOutput_getVector(orderOfMatrix, &
				file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".val", binary=.true.)
			
			!!! transformacion de indices para 
			if ( ParticleManager_isFrozen(trim(nameOfSpecie))) then

				!!Tamano del arreglo de nuevas etiquetas
				if(allocated(trans)) deallocate(trans)
				allocate(trans(orderOfMatrix))

				!! Reglas de transformacion de indices
				st(1)    = 1
				pt(1:3)  = [1, 2, 3]
				dt(1:6)  = [1, 4, 5, 2, 6, 3]
				ft(1:10) = [1, 4, 5, 6, 10, 8, 2, 7, 9, 3]
				gt(1:15) = [1, 4, 5, 10, 13, 11, 6, 14, 15, 8, 2, 7, 12, 9, 3]
                                ht(1:21) = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
!				write(*,*) ""
!				write(*,"(A)",advance="no") "READING EXTERNAL COEFFICIENTS 2..."

				trans = 0
				u = 1
				v = 0

				do i = 1, numberOfContractions
					angularMoment = ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
									specieID)%contractionID(i)%particleID)%basis%contractions( &
									ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
									i)%contractionIDInParticle)%angularMoment
								
					select case(angularMoment)
						case(0)
							trans(u) = st(1) + v
							u = u + 1
							
						case(1)
							do j = 1, 3
								trans(u) = pt(j) + v
								u = u + 1
							end do
							
						case(2)
							do j = 1, 6
								trans(u) = dt(j) + v
								u = u + 1
							end do
							
						case(3)
							do j = 1, 10
								trans(u) = ft(j) + v
								u = u + 1
							end do
							
						case(4)
							do j = 1, 15
								trans(u) = gt(j) + v
								u = u + 1
							end do
						case(5)
                                                        do j = 1, 21
                                                                trans(u) = ht(j) + v
                                                                u = u + 1
                                                        end do
						case default
							call Exception_constructor( ex , WARNING )
							call Exception_setDebugDescription( ex, "Class object SCFIntra in the iterate function" )
							call Exception_setDescription( ex, "this angular moment is not implemented (l>4)" )
							call Exception_show( ex )
							
							return
						
					end select
					v = u - 1
					
				end do
				
				!!Matriz de Coeficientes
				call Matrix_constructor( auxMatrix, int(orderOfMatrix, 8), int(orderOfMatrix, 8) )
				
				do i=1,orderOfMatrix
					do j=1,orderOfMatrix
						auxMatrix.values( i, j ) = beforeDensityMatrix.values( trans(i), trans(j) )
					end do
				end do
				
				beforeDensityMatrix.values = auxMatrix.values
				
				call Matrix_destructor(auxMatrix)
				
				!!Vector de valores propios
				call Vector_constructor(auxVector, orderOfMatrix)
				
				do i = 1, orderOfMatrix
					auxVector.values(i) = beforeMolecularOrbitalsEnergy.values(trans(i))
				end do
				
				beforeMolecularOrbitalsEnergy.values = auxVector.values
				
				call Vector_destructor(auxVector)
				
!				write(*,*) "OK"
				
			end if
			
			SCFIntraspecies_instance( specieID ).waveFunctionCoefficientsPtr.values = beforeDensityMatrix.values
			
			SCFIntraspecies_instance( specieID ).molecularOrbitalsEnergyPtr.values = beforeMolecularOrbitalsEnergy.values
			
			if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
				if(ParticleManager_isFrozen(trim(nameOfSpecie))) then

				else
					call WaveFunction_builtDensityMatrix( nameOfSpecieSelected )
				end if
			else
				call WaveFunction_builtDensityMatrix( nameOfSpecieSelected )
			end if

			call WaveFunction_obtainEnergy( nameOfSpecieSelected )
			call List_push_back( SCFIntraspecies_instance( specieID ).energySCF, WaveFunction_getEnergy( specieID ) )
			
		end if
		
		SCFIntraspecies_instance( specieID ).wasBuiltFockMatrixPtr = .false.
		
	end subroutine SCFIntraspecies_iterate
	
	
	!**
	! @brief Reinicia el proceso de iteracion SCF para la especie especificada
	!
	!**
	subroutine SCFIntraspecies_restart( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		SCFIntraspecies_instance( specieID ).numberOfIterations = 0
		
		call List_clear( SCFIntraspecies_instance( specieID ).energySCF )
		call List_clear( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
		call List_clear( SCFIntraspecies_instance( specieID ).diisError )
		call SCFConvergence_reset()
	
	end subroutine SCFIntraspecies_restart
	
	!**
	! @brief Reinicializa el modulo
	!
	!**
	subroutine SCFIntraspecies_reset() 
		implicit none
		
		integer :: specieID
		integer :: speciesIterator
		
		do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
			
			
			specieID = ParticleManager_getSpecieID( iteratorOfSpecie=speciesIterator )
			SCFIntraspecies_instance( specieID ).numberOfIterations = 0
			SCFIntraspecies_instance( specieID ).wasBuiltFockMatrixPtr =.false.
			call List_clear( SCFIntraspecies_instance( specieID ).energySCF )
			call List_clear( SCFIntraspecies_instance( specieID ).diisError )
			call List_clear( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )

			call SCFConvergence_destructor( SCFIntraspecies_instance( specieID ).convergenceMethod )
			Call SCFConvergence_constructor(SCFIntraspecies_instance(specieID).convergenceMethod, &
				SCFIntraspecies_instance( specieID ).name, APMO_instance.CONVERGENCE_METHOD)
			
		end do

		call SCFConvergence_reset()
	
	end subroutine SCFIntraspecies_reset


	subroutine SCFIntraspecies_actualizeDensityMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		
		integer :: specieID
		type(Matrix) :: beforeDensityMatrix
		character(30) :: nameOfSpecieSelected
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )

		specieID = ParticleManager_getSpecieID( nameOfSpecie=trim(nameOfSpecieSelected ) )

		!! Determina la desviacion estandar de los elementos de la matriz de densidad 
		call Matrix_copyConstructor( beforeDensityMatrix, SCFIntraspecies_instance( specieID ).densityMatrixPtr )

		if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
			if(ParticleManager_isFrozen(trim(nameOfSpecie))) then

			else
				call WaveFunction_builtDensityMatrix( nameOfSpecieSelected )
			end if
		else
			call WaveFunction_builtDensityMatrix( nameOfSpecieSelected )
		end if
                
		call List_push_back( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements, &
			Matrix_standardDeviation( beforeDensityMatrix, SCFIntraspecies_instance( specieID ).densityMatrixPtr ) )
		
		!! Calcula energia total para la especie especificada
		call WaveFunction_obtainEnergy( nameOfSpecieSelected )
		call List_push_back( SCFIntraspecies_instance( specieID ).energySCF, WaveFunction_getEnergy( specieID ) )
		call List_push_back( SCFIntraspecies_instance( specieID ).diisError, &
				SCFConvergence_getDiisError( SCFIntraspecies_instance( specieID ).convergenceMethod) )


		call Matrix_destructor( beforeDensityMatrix )

	end subroutine SCFIntraspecies_actualizeDensityMatrix
	
	!**
	! @brief Prueba si la energia del la ultima iteracion a sufrido un cambio por debajo de cierta tolerancia
	!**
	function SCFIntraspecies_testEnergyChange( nameOfSpecie, tolerace ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		real(8), intent(in) :: tolerace
		integer :: output
		
		type(Exception) :: ex
		integer :: specieID
		character(30) :: nameOfSpecieSelected
		real(8) :: deltaEnergy
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		if ( SCFIntraspecies_getNumberOfIterations( nameOfSpecieSelected ) &
			 > APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) then
			 
			 output =	SCF_INTRASPECIES_CONVERGENCE_FAILED
			 
			 call Exception_constructor( ex , ERROR )
			 call Exception_setDebugDescription( ex, "Class object SCFIntraspecies in the testEnergyChange function" )
			 call Exception_setDescription( ex, "SCF_INTRASPECIES_CONVERGENCE_FAILED BY: "//trim(nameOfSpecieSelected)//" ITERATIONS EXCEEDED")
			 call Exception_show( ex )
			 
		else
		
			if ( SCFIntraspecies_getNumberOfIterations(nameOfSpecieSelected) > 1 ) then
				specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
						
				!! Obtiene el valor de energia de la ultima iteracion
				call List_end( SCFIntraspecies_instance( specieID ).energySCF )
				deltaEnergy= List_current( SCFIntraspecies_instance( specieID ).energySCF )
				
				!! Obtiene el valor  de energia anterior a la ultima iteracion
				call List_iterate( SCFIntraspecies_instance( specieID ).energySCF, -1)
				
				!! Obtiene el cambio de energia en las ultimas dos iteraciones
				deltaEnergy = deltaEnergy - List_current( SCFIntraspecies_instance( specieID ).energySCF )
				
				if( abs( deltaEnergy - APMO_instance.DOUBLE_ZERO_THRESHOLD) < tolerace  ) then
					
					output =	SCF_INTRASPECIES_CONVERGENCE_SUCCESS
			
				else
				
					output =	SCF_INTRASPECIES_CONVERGENCE_CONTINUE
					
				
				end if
			
			else
			
				output =	SCF_INTRASPECIES_CONVERGENCE_CONTINUE
			
			end if
		
		end if
			
	end function SCFIntraspecies_testEnergyChange
	
	!**
	! @brief Prueba si la matriz de densidad ha sufrido un cambio respecto a una tolerancia dada
	!
	!**
	function SCFIntraspecies_testDensityMatrixChange( nameOfSpecie, tolerace ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		real(8), intent(in) :: tolerace
		integer :: output
		
		type(Exception) :: ex
		integer :: specieID
		character(30) :: nameOfSpecieSelected
		real(8) :: deltaDensityMatrix
		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		if ( SCFIntraspecies_getNumberOfIterations( nameOfSpecieSelected ) &
			 > APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) then
			 
			 output =	SCF_INTRASPECIES_CONVERGENCE_FAILED
			 
			 call Exception_constructor( ex , ERROR )
			 call Exception_setDebugDescription( ex, "Class object SCFIntraspecies in the testDensityMatrixChange function" )
			 call Exception_setDescription( ex, "SCF_INTRASPECIES_CONVERGENCE_FAILED BY: "//trim(nameOfSpecieSelected)//" ITERATIONS EXCEEDED" )
			 call Exception_show( ex )
			 
		else
		
			if ( SCFIntraspecies_getNumberOfIterations(nameOfSpecieSelected) > 1 ) then
				specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
						
				!! Obtiene la desviacion de la ultima iteracion
				call List_end( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
				deltaDensityMatrix= List_current( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
				
				if( abs( deltaDensityMatrix - APMO_instance.DOUBLE_ZERO_THRESHOLD) > tolerace ) then
				
					output =	SCF_INTRASPECIES_CONVERGENCE_CONTINUE
		
				else
			
					output =	SCF_INTRASPECIES_CONVERGENCE_SUCCESS
			
				end if
			
			else
			
				output =	SCF_INTRASPECIES_CONVERGENCE_CONTINUE
			
			end if
		
		end if
			
	end function SCFIntraspecies_testDensityMatrixChange
	
	!**
	! @brief Aplica el metodo de convegencia SCF especificado para la matriz de Fock
	!**
	subroutine SCFIntraspecies_convergenceMethod( specieID)
		implicit none
		integer, intent(in) :: specieID
		
		!!**************************************************************************************************
		!! Emplea metodo de convergecia SCF
		!!

		if (  SCFIntraspecies_instance( specieID ).numberOfIterations  > 1  )then
		
			if ( SCFConvergence_isInstanced( SCFIntraspecies_instance( specieID ).convergenceMethod ) ) then
				
				if ( APMO_instance.CONVERGENCE_METHOD == SCF_CONVERGENCE_DAMPING ) then


					call SCFConvergence_setMethod( SCFIntraspecies_instance( specieID ).convergenceMethod, &
						SCFIntraspecies_instance( specieID ).fockMatrixPtr, SCFIntraspecies_instance( specieID ).densityMatrixPtr, &
						WaveFunction_getOverlapMatrixPtr( specieID ), methodType = SCF_CONVERGENCE_DAMPING )

				else &
				if ( APMO_instance.CONVERGENCE_METHOD == SCF_CONVERGENCE_DIIS ) then

					call SCFConvergence_setMethod( SCFIntraspecies_instance( specieID ).convergenceMethod, &
						SCFIntraspecies_instance( specieID ).fockMatrixPtr, SCFIntraspecies_instance( specieID ).densityMatrixPtr, &
						WaveFunction_getOverlapMatrixPtr( specieID ), methodType=SCF_CONVERGENCE_DIIS )
				
				else &
				if ( APMO_instance.CONVERGENCE_METHOD == SCF_CONVERGENCE_MIXED ) then

					call SCFConvergence_setMethod( SCFIntraspecies_instance( specieID ).convergenceMethod, &
						SCFIntraspecies_instance( specieID ).fockMatrixPtr, SCFIntraspecies_instance( specieID ).densityMatrixPtr, &
						WaveFunction_getOverlapMatrixPtr( specieID ), methodType=SCF_CONVERGENCE_MIXED )
				
				end if


				call SCFConvergence_run( SCFIntraspecies_instance( specieID ).convergenceMethod )
				
				
			end if
		
		else
			
				call SCFConvergence_setInitialDensityMatrix( SCFIntraspecies_instance( specieID ).convergenceMethod, &
							SCFIntraspecies_instance( specieID ).densityMatrixPtr )
			
				call SCFConvergence_setInitialFockMatrix( SCFIntraspecies_instance( specieID ).convergenceMethod, &
						SCFIntraspecies_instance( specieID ).fockMatrixPtr )
					
		end if
		!!
		!!**************************************************************************************************
	
	end subroutine SCFIntraspecies_convergenceMethod
		
end module SCFIntraspecies_
