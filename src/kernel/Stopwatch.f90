!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module Stopwatch_
     use IFPORT

     implicit none

     type, public :: Stopwatch
               character(20) :: name
               real(8) :: startTime
               real(8) :: enlapsetTime
               character(255) :: initialDate 
               character(255) :: currentDate 
     end type


     type(Stopwatch), public :: apmo_stopwatch

     public :: &
          Stopwatch_constructor, &
          Stopwatch_destructor, &
          Stopwatch_start, &
          Stopwatch_stop, &
          Stopwatch_getCurretData

     private
contains

     subroutine Stopwatch_constructor( this )
          implicit none
          type(Stopwatch) :: this

          this.name=""
          this.startTime = 0.0_8
          call FDATE(this.initialDate)
          this.currentDate = trim(this.initialDate)
          this.enlapsetTime = 0.0_8

     end subroutine Stopwatch_constructor

     subroutine Stopwatch_destructor( this )
          implicit none
          type(Stopwatch) :: this

          this.name=""
          this.startTime = 0.0_8
          this.initialDate = ""
          this.currentDate = ""
          this.enlapsetTime = 0.0_8

     end subroutine Stopwatch_destructor

     subroutine Stopwatch_start( this )
          implicit none
          type(Stopwatch) :: this

          this.startTime =DCLOCK()

     end subroutine Stopwatch_start


     subroutine Stopwatch_stop( this )
          implicit none
          type(Stopwatch) :: this

          this.enlapsetTime =  DCLOCK() - this.startTime

     end subroutine Stopwatch_stop


     function Stopwatch_getCurretData( this ) result( output )
          implicit none
          type(Stopwatch) :: this
          character(100) :: output

          call FDATE(this.currentDate)
          output=trim(this.currentDate)

     end function Stopwatch_getCurretData



end module Stopwatch_