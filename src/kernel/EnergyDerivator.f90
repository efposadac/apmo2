!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief 	Modulo para calculo de derivadas de orden n para la  energia
!		del sistema molecular
! 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y meodos
!
!**
module EnergyDerivator_
	use IFPORT
	use omp_lib
	use APMO_
	use Exception_
	use IntegralManager_
	use ParticleManager_
	use RHFSolver_
	use Vector_
	use Matrix_
	use MolecularSystem_
	use MecanicProperties_
	use Math_
	
	implicit none
	
	type, public :: EnergyDerivator
		
		character(30) :: name
		logical :: isAnalytic
		logical :: isIntanced
		real(8) :: deltaX
		real(8) :: output
		
		integer :: order
		type(Vector) pointsOfEvaluation
		integer, allocatable :: components(:)
		logical :: derivativeWithMP
		
	end type
	
	private ::
		type(EnergyDerivator) :: EnergyDerivator_instance
	
	private :: &
	 	EnergyDerivator_calculateAnalyticUncoupledFirstDerivative, &
		EnergyDerivator_calculateAnalyticCouplingFirstDerivative, &
		EnergyDerivator_calculateFistDerivativeOfPuntualEnergy
	
	public :: &
		EnergyDerivator_constructor, &
		EnergyDerivator_destructor, &
		EnergyDerivator_setName, &
		EnergyDerivator_setAnalytic, &
		EnergyDerivator_setNumeric, &
		EnergyDerivator_setNumericStepSize, &
		EnergyDerivator_getDerivative, &
		EnergyDerivator_getAnalyticDerivative, &
		EnergyDerivator_getNumericGradient, &
		EnergyDerivator_getNumericDerivative, &
		EnergyDerivator_getNumericDerivative2, &
		EnergyDerivator_getTypeGradient, &
		EnergyDerivator_isInstanced

contains
		!**
		! @brief Define el constructor
		!**
		subroutine EnergyDerivator_constructor()
			implicit none

! 			print*, " EnergyDerivator_constructor" 

			EnergyDerivator_instance.name = ""
			EnergyDerivator_instance.isIntanced = .true.
			EnergyDerivator_instance.isAnalytic = APMO_instance.ANALYTIC_GRADIENT
			EnergyDerivator_instance.deltaX = APMO_instance.NUMERICAL_DERIVATIVE_DELTA
			EnergyDerivator_instance.derivativeWithMP = APMO_instance.OPTIMIZE_WITH_MP


		end subroutine EnergyDerivator_constructor
		
		!**
		! @brief Define el destructor
		!**
		subroutine EnergyDerivator_destructor()
			implicit none

			integer :: i
			integer :: status

! 			print*, " EnergyDerivator_destructor"

			EnergyDerivator_instance.isIntanced = .false.
		
 
		end subroutine EnergyDerivator_destructor
		
		!**
		! @brief Ajusta el nombre del minimizador empleado
		!**
		subroutine EnergyDerivator_setName()
			implicit none
		
! 			print*, " EnergyDerivator_setName" 

		end subroutine EnergyDerivator_setName
		
		!**
		! @brief Ajusta calculo del gradiente de forma numerica
		!**
		subroutine EnergyDerivator_setNumeric()
			implicit none

! 			print*, " EnergyDerivator_setNumeric" 

			EnergyDerivator_instance.isAnalytic = .false.
		

		end subroutine EnergyDerivator_setNumeric
		
		!**
		! @brief Ajusta calculo del gradiente de forma analitic
		!**
		subroutine EnergyDerivator_setAnalytic()
			implicit none

! 			print*, " EnergyDerivator_setAnalytic" 

			EnergyDerivator_instance.isAnalytic = .true.
		

		end subroutine EnergyDerivator_setAnalytic
		
		!**
		! @brief Ajusta calculo del gradiente de forma analitic
		!**
		subroutine EnergyDerivator_setNumericStepSize(stepSize)
			implicit none
			real(8) :: stepSize

! 			print*, " EnergyDerivator_setNumericStepSize" 

			EnergyDerivator_instance.deltaX = stepSize
		

		end subroutine EnergyDerivator_setNumericStepSize

		!**
		! @brief Indica si el gradiente es numerico o analitico
		!**
		function EnergyDerivator_getTypeGradient() result(output)
			implicit none
			character(30) :: output

! 			print*, " EnergyDerivator_getTypeGradient" 

			if ( EnergyDerivator_instance.isAnalytic == .true. ) then
				output = "ANALYTIC" 
			else
				output = "NUMERICAL"
			end if
		

		end function EnergyDerivator_getTypeGradient
		
		
		!**
		! @brief 	Retorna el valor de la derivada de orden n en un punto dado
		!		de las variables independientes 
		!**
		function EnergyDerivator_getDerivative( evaluationPoint, components, functionValue, order, iterator ) result(output)
			implicit none
			type(Vector), target, intent(in) :: evaluationPoint
			integer, intent(in) :: components(:)
			integer, intent(in), optional :: order
			integer, intent(in), optional :: iterator !!Lowdin
			real(8) :: output

			interface
				function functionValue( pointOfEvaluation, iterador ) result( output )
					real(8) :: pointOfEvaluation(:)
					integer, optional :: iterador
					real(8) :: output
				end function functionValue
			end interface
			
! 			print*, "EnergyDerivator_getDerivative"
			
			EnergyDerivator_instance.order = 1
			if ( present(order) ) EnergyDerivator_instance.order = order
			EnergyDerivator_instance.pointsOfEvaluation = evaluationPoint
			allocate ( EnergyDerivator_instance.components( size(components) ) )
			EnergyDerivator_instance.components = components
			
			output=0.0_8

			if ( EnergyDerivator_instance.isAnalytic ) then
			
				output = EnergyDerivator_getAnalyticDerivative()
				
			else
				if( EnergyDerivator_instance.derivativeWithMP .and. APMO_instance.MOLLER_PLESSET_CORRECTION==2) then
					output = EnergyDerivator_gradientWithMP()
				else
					output = EnergyDerivator_getNumericDerivative( functionValue, iterator )
				end if

			end if
			
			if ( abs(output) < 1.0E-8 ) output=0.0D+0

			!! Deja el sistema en su posicion original
			call ParticleManager_setParticlesPositions( evaluationPoint )
			deallocate(EnergyDerivator_instance.components)
		

		end function EnergyDerivator_getDerivative

		function EnergyDerivator_getNumericGradient( evaluationPoint, functionValue ) result(output)
			implicit none
			type(Vector), target, intent(in) :: evaluationPoint
			real(8), allocatable :: output(:)

			interface
				function functionValue( pointOfEvaluation, iterador ) result( output )
					real(8) :: pointOfEvaluation(:)
					integer, optional :: iterador
					real(8) :: output
				end function functionValue
			end interface

			real(8) :: auxVal
			real(8) :: auxValue(2)
			type(Matrix) :: projector
			type(Matrix) :: gradientProjected
			integer :: infoProcess
			integer :: ssize
			integer :: numberOfSymmetricModes
			integer :: nucleiAndComponent
			integer :: i
			
! 			print*, " EnergyDerivator_getNumericGradient" 
			
			EnergyDerivator_instance.pointsOfEvaluation = evaluationPoint

			allocate( output( size(evaluationPoint.values) ) )

			projector=MecanicProperties_getHandyProjector( MolecularSystem_instance.mecProperties,infoProcess )

			ssize=size(projector.values, dim=1)
			do i=1,ssize
				auxVal=sqrt( dot_product(projector.values(i,:),projector.values(i,:)) )
				if( auxVal > 1.0D-4 )  projector.values(i,:)=projector.values(i,:)/auxVal
			end do

			!! Verifica la independencia lineal de los vetores 
			projector.values=transpose(projector.values)
			call Matrix_selectLinearlyIndependentVectors(projector, numberOfSymmetricModes)
			projector.values=transpose(projector.values)

			output=0.0
			do i=1, numberOfSymmetricModes

				if( APMO_instance.OPTIMIZE_WITH_MP ) then

					!! Ajusta punto x+h
					EnergyDerivator_instance.pointsOfEvaluation.values = EnergyDerivator_instance.deltaX*projector.values(i,:) &
						+ evaluationPoint.values

					call ParticleManager_setParticlesPositions( EnergyDerivator_instance.pointsOfEvaluation )
					call EnergyDerivator_writeInput(trim(APMO_instance.INPUT_FILE)//"0.apmo")

					!! Ajusta punto x-h
					EnergyDerivator_instance.pointsOfEvaluation.values = -EnergyDerivator_instance.deltaX*projector.values(i,:) &
						+ evaluationPoint.values

					call ParticleManager_setParticlesPositions( EnergyDerivator_instance.pointsOfEvaluation )
					call EnergyDerivator_writeInput(trim(APMO_instance.INPUT_FILE)//"1.apmo")
			
					call EnergyDerivator_calculateFile(trim(APMO_instance.INPUT_FILE),auxValue)

				else
					EnergyDerivator_instance.pointsOfEvaluation.values = EnergyDerivator_instance.deltaX*projector.values(i,:) &
						+ evaluationPoint.values

					auxValue(1) = functionValue( EnergyDerivator_instance.pointsOfEvaluation.values )

					EnergyDerivator_instance.pointsOfEvaluation.values = -EnergyDerivator_instance.deltaX*projector.values(i,:) &
						+ evaluationPoint.values

					auxValue(2) = functionValue( EnergyDerivator_instance.pointsOfEvaluation.values )
					

				end if

				print "(A1$)",":"
				output(i) = ( auxValue(1) - auxValue(2) ) / ( 2.0_8 * EnergyDerivator_instance.deltaX )

			end do

			output=matmul( transpose(projector.values), output)

			do i=1,size(output)
				if( abs(output(i)) < 1.0D-8 ) output(i)=0.0
			end do

			call ParticleManager_setParticlesPositions( evaluationPoint )
			call Matrix_destructor(projector)


		end function EnergyDerivator_getNumericGradient

		!**
		! @brief 	Retorna el valor de la derivada de orden n en un punto dado
		!		de las variables independientes 
		!**
		function EnergyDerivator_getAnalyticDerivative() result(output)
			implicit none
			real(8) :: output
			
			type(Exception) :: ex
! 			print*, " EnergyDerivator_getAnalyticDerivative" 
			if ( EnergyDerivator_instance.order <= 1 ) then
			
				output = EnergyDerivator_calculateAnalyticUncoupledFirstDerivative() 
			
				if ( ParticleManager_getNumberOfQuantumSpecies() > 1) &
					output = output + EnergyDerivator_calculateAnalyticCouplingFirstDerivative()
				
				output = output + EnergyDerivator_calculateFistDerivativeOfPuntualEnergy()
				
			else
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object EnergyDerivator in the getAnalyticDerivative function" )
				call Exception_setDescription( ex, "This order isn't implemented" )
				call Exception_show( ex )
				
			end if
		
		

		end function EnergyDerivator_getAnalyticDerivative
		
		!**
		! @brief 	Retorna el valor de la derivada de orden n en un punto dado
		!		de las variables independientes 
		!**
		function EnergyDerivator_getNumericDerivative( functionValue, iterator ) result( output )
			implicit none
			real(8) :: output
			integer, optional :: iterator

			interface
				function functionValue( pointOfEvaluation, iterator ) result( output )
					real(8) :: pointOfEvaluation(:)
					integer, optional :: iterator
					real(8) :: output
				end function functionValue
			end interface

			type(Exception) :: ex
			type(Vector) :: auxEvaluationPoint
			real(8), allocatable :: auxValue(:)
			real(8) :: energyDeltaLess

			integer :: nucleiAndComponent(2)

! 			print*, "EnergyDerivator_getNumericDerivative"
			  
			output=0.0_8
			
			if ( EnergyDerivator_instance.order <= 2 ) then
				select case( EnergyDerivator_instance.order )

					case(1)

						nucleiAndComponent= ParticleManager_getCenterOfOptimization(EnergyDerivator_instance.components(1) )
						if ( ParticleManager_isComponentFixed( nucleiAndComponent(1),nucleiAndComponent(2) ) ) return
						
! 						if( abs(EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) ) < &
! 							1.0D-10 ) return

						allocate( auxValue(2) )

						!! Ajusta punto x+h
						EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
													EnergyDerivator_instance.pointsOfEvaluation.values( &
													EnergyDerivator_instance.components(1) ) &
													+ EnergyDerivator_instance.deltaX
						auxValue(1) = functionValue( EnergyDerivator_instance.pointsOfEvaluation.values, iterator )

						!! Ajusta punto x-h
						EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
													EnergyDerivator_instance.pointsOfEvaluation.values( &
													EnergyDerivator_instance.components(1) ) &
													- 2.0_8 * EnergyDerivator_instance.deltaX
						auxValue(2) = functionValue( EnergyDerivator_instance.pointsOfEvaluation.values, iterator+100 )

						!! calcula primera derivada de la energia
						if(trim( APMO_instance.EXTERNAL_SOFTWARE_NAME ) /= "lowdin") then
							output = ( auxValue(1) - auxValue(2) ) / ( 2.0_8 * EnergyDerivator_instance.deltaX )
						end if
						
						deallocate( auxValue )
					
					case(2)

					
						if ( EnergyDerivator_instance.components(1) == EnergyDerivator_instance.components(2) ) then
						
							allocate( auxValue(5) )
							
							!! Ajusta en punto x-2h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											-2.0_8 * EnergyDerivator_instance.deltaX
					
							auxValue(1) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x-h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											+ EnergyDerivator_instance.deltaX
					
							auxValue(2) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )

							!! Ajusta en punto inicial
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											+ EnergyDerivator_instance.deltaX
					
							auxValue(3) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x+h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											+ EnergyDerivator_instance.deltaX
					
							auxValue(4) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x+2*h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											+ EnergyDerivator_instance.deltaX
					
							auxValue(5) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
						
							!! Calcula segunda derivada d^2/dx^2
							output = ( 16.0_8 * ( auxValue(4) - auxValue(2) ) - 30.0_8 * auxValue(3) - auxValue(1) - auxValue(5) ) &
									/ ( 12.0_8 * ( EnergyDerivator_instance.deltaX**2.0_8 ) )
									
						else
						
							allocate( auxValue(4) )
							
							!! Ajusta en punto x-h, y-h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											- EnergyDerivator_instance.deltaX
										
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(2) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(2) ) &
											- EnergyDerivator_instance.deltaX
																				
							auxValue(1) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x-h, y+h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(2) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(2) ) &
											+ 2.0_8 * EnergyDerivator_instance.deltaX
																				
							auxValue(2) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x+h, y+h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(1) ) &
											+ 2.0_8 * EnergyDerivator_instance.deltaX
																				
							auxValue(3) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )
							
							!! Ajusta en punto x+h, y-h
							EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(2) ) = &
											EnergyDerivator_instance.pointsOfEvaluation.values( &
											EnergyDerivator_instance.components(2) ) &
											- 2.0_8 * EnergyDerivator_instance.deltaX
							
							auxValue(4) = functionValue( &
										EnergyDerivator_instance.pointsOfEvaluation.values )

							!! Calcula segunda derivada d^2/dxy
							output =( auxValue(3) - auxValue(4) - auxValue(2) + auxValue(1) ) &
									/ ( 4.0_8 * (EnergyDerivator_instance.deltaX**2.0_8) )
						
						end if
			
				end select
			
			else
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object EnergyDerivator in the getNumericDerivative function" )
				call Exception_setDescription( ex, "This order isn't implemented" )
				call Exception_show( ex )
				
			end if
		

		end function EnergyDerivator_getNumericDerivative
		
		function EnergyDerivator_getNumericDerivative2(auxValue1,auxValue2) result(output)
			implicit none
			real(8) :: auxValue1
			real(8) :: auxValue2
			real(8) :: output
			
			output = ( auxValue1 - auxValue2 ) / ( 2.0_8 * EnergyDerivator_instance.deltaX )
			
		end function EnergyDerivator_getNumericDerivative2
		
		function EnergyDerivator_gradientWithMP() result(output)
			implicit none
			real(8) :: output

			integer :: status
			real(8) :: auxValue(2)
			integer :: nucleiAndComponent(2)
! print*, " EnergyDerivator_gradientWithMP" 
			output = 0.0_8

			nucleiAndComponent= ParticleManager_getCenterOfOptimization(EnergyDerivator_instance.components(1) )
			if ( ParticleManager_isComponentFixed( nucleiAndComponent(1),nucleiAndComponent(2) ) ) return
						
			!! Ajusta punto x+h
			EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
										EnergyDerivator_instance.pointsOfEvaluation.values( &
										EnergyDerivator_instance.components(1) ) &
										+ EnergyDerivator_instance.deltaX

			call ParticleManager_setParticlesPositions( EnergyDerivator_instance.pointsOfEvaluation )
			
			call EnergyDerivator_writeInput(trim(APMO_instance.INPUT_FILE)//"0.apmo")

			!! Ajusta punto x-h
			EnergyDerivator_instance.pointsOfEvaluation.values( EnergyDerivator_instance.components(1) ) = &
										EnergyDerivator_instance.pointsOfEvaluation.values( &
										EnergyDerivator_instance.components(1) ) &
										- 2.0_8 * EnergyDerivator_instance.deltaX

			call ParticleManager_setParticlesPositions( EnergyDerivator_instance.pointsOfEvaluation )

			call EnergyDerivator_writeInput(trim(APMO_instance.INPUT_FILE)//"1.apmo")
			
			call EnergyDerivator_calculateFile(trim(APMO_instance.INPUT_FILE),auxValue)

			!! calcula primera derivada de la energia
			output = ( auxValue(1) - auxValue(2) ) / ( 2.0_8 * EnergyDerivator_instance.deltaX )


		end function EnergyDerivator_gradientWithMP

		subroutine EnergyDerivator_writeInput(nameOfFile)
			implicit none
			character(*) :: nameOfFile
			integer :: i
! 			print*, " EnergyDerivator_writeInput"
			open( UNIT=34,FILE=trim(nameOfFile),STATUS='REPLACE', &
				ACCESS='SEQUENTIAL', FORM='FORMATTED' )
			
			write (34,*) "SYSTEM_DESCRIPTION='' "
			write (34,*) ""
			write (34,*) "GEOMETRY"
				do i=1,size(  ParticleManager_instance.particles )
					write(34,"(A10,A10,<3>F15.10)") 	trim(ParticleManager_instance.particles(i).nickname), &
										trim(ParticleManager_instance.particles(i).basisSetName), &
										ParticleManager_instance.particles(i).origin*0.52917724924_8
				end do
			write (34,*) "END GEOMETRY"
			write (34,*) ""
			write (34,*) "TASKS"
				write (34,*) "     method=RHF"
				write (34,*) "     mollerPlessetCorrection=2"
			write (34,*) "END TASKS"
			write (34,*) ""
			write (34,*) "CONTROL"
				write (34,*) "     scfnonelectronicenergytolerance =",APMO_instance.SCF_NONELECTRONIC_ENERGY_TOLERANCE
				write (34,*) "     scfelectronicenergytolerance =",APMO_instance.SCF_ELECTRONIC_ENERGY_TOLERANCE
				write (34,*) "     nonelectronicdensitymatrixtolerance =",APMO_instance.NONELECTRONIC_DENSITY_MATRIX_TOLERANCE
				write (34,*) "     electronicdensitymatrixtolerance =",APMO_instance.ELECTRONIC_DENSITY_MATRIX_TOLERANCE
				write (34,*) "     totalenergytolerance =",APMO_instance.TOTAL_ENERGY_TOLERANCE
				write (34,*) "     strongenergytolerance =",APMO_instance.STRONG_ENERGY_TOLERANCE
				write (34,*) "     densityfactorthreshold =",APMO_instance.DENSITY_FACTOR_THRESHOLD
				write (34,*) "     scfnonelectronicmaxiterations =",APMO_instance.SCF_NONELECTRONIC_MAX_ITERATIONS
				write (34,*) "     scfelectronicmaxiterations =",APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS
				write (34,*) "     scfmaxiterations =",APMO_instance.SCF_MAX_ITERATIONS
				write (34,*) "     scfinterspeciesmaximumiterations =",APMO_instance.SCF_INTERSPECIES_MAXIMUM_ITERATIONS
				write (34,*) "     diisswitchthreshold =",APMO_instance.DIIS_SWITCH_THRESHOLD
				write (34,*) "     convergencemethod =",APMO_instance.CONVERGENCE_METHOD
			write (34,*) "END CONTROL"
			close(34)

 
		end subroutine EnergyDerivator_writeInput

		subroutine EnergyDerivator_calculateFile(fileName, values)
			implicit none
			character(*) :: fileName
			real(8) :: values(2)
		
			integer :: status
			integer :: i
			integer :: auxValue
			character :: auxChar
			character(255) :: line
			
! print*, " EnergyDerivator_calculateFile" 
			do i=1, ParticleManager_getNumberOfQuantumSpecies()
				if ( ParticleManager_instance.particles(i).isQuantum ) then
					status=system("cp "//trim(fileName)//trim(ParticleManager_getNameOfSpecie( i ))//".vec " &
							//trim(fileName)//"0."//trim(ParticleManager_getNameOfSpecie( i ))//".vec ")
					status=system("cp "//trim(fileName)//trim(ParticleManager_getNameOfSpecie( i ))//".vec " &
							//trim(fileName)//"1."//trim(ParticleManager_getNameOfSpecie( i ))//".vec ")
				end if
			end do

			call omp_set_num_threads(2)
			!$OMP PARALLEL private(status)

				if ( omp_get_thread_num()==0) then
					status=system("apmo "//trim(fileName)//"0.apmo")
				else if ( omp_get_thread_num()==1) then
					status=system("apmo "//trim(fileName)//"1.apmo")
				end if

			!$OMP END PARALLEL
	
			!!     lee el valor de la energia

			open(UNIT=70,FILE=trim(fileName)//"0.out",STATUS='unknown',ACCESS='sequential')
			read(70,'(A)') line
			line=trim(adjustl(trim(line)))
			
			do while((line(1:13)) /= "Enlapsed Time")
				read(70,'(A)',end=10) line
				line=trim(adjustl(trim(line)))

				if ( (line(1:7)) == "E(MP2)=" )  &
					values(1) = dnum(line(scan(trim(line),"=" )+1:len_trim(line)))

			end do
			close(70)
			status=system("rm "//trim(fileName)//"0.*")		
			open(UNIT=70,FILE=trim(fileName)//"1.out",STATUS='unknown',ACCESS='sequential')
			read(70,'(A)') line
			line=trim(adjustl(trim(line)))
			
			do while((line(1:13)) /= "Enlapsed Time")
				read(70,'(A)',end=10) line
				line=trim(adjustl(trim(line)))

				if ( (line(1:7)) == "E(MP2)=" )  &
					values(2) = dnum(line(scan(trim(line),"=" )+1:len_trim(line)))

			end do
			close(70)
			status=system("rm "//trim(fileName)//"1.*")

			return

10			call EnergyDerivator_exception(ERROR, "The single point calculation has failed", &
				"Class object EnergyDerivator in calculateFile() function")
			

		end subroutine EnergyDerivator_calculateFile

		!**
		! @brief 	Indica si el derivadoor a sido o instanciado
		! 
		!**
		function EnergyDerivator_isInstanced() result(output)
			implicit none
			logical :: output
! 			print*, " EnergyDerivator_isInstanced"			
			output=EnergyDerivator_instance.isIntanced
			
			
 
		end function EnergyDerivator_isInstanced
		
		!**
		! @brief  Calcula la primera derivada analitica para la matriz de Fock sin incluir el termino de
		!               acoplamiento interspecie
		!**
		function EnergyDerivator_calculateAnalyticUncoupledFirstDerivative() result(output)
			implicit none
			real(8) :: output
		
			character(30) :: nameOfSpecie
			integer :: nucleiAndComponent(2)
			integer :: specieIterator
			integer :: ocupationNumber
			integer(8) :: orderOfMatrix
			real(8), allocatable :: energyDerivative(:)
			real(8), allocatable :: repulsionDerivativeComponent(:)
			real(8), allocatable :: auxMatrix(:,:)
			real(8), allocatable :: otherAuxMatrix(:,:)
			type(Matrix), pointer :: matrixOfEigenvectors
			type(Vector), pointer :: vectorOfEigenvalues
			real(8), allocatable :: derivativeOfHcoreMatrix(:,:)
			real(8), allocatable :: derivativeOfOverlapMatrix(:,:)
			real(8), allocatable :: derivativeOfRepulsionVector(:)
			integer(8) :: numberOfIntegrals
			integer :: i
			integer :: j
			integer :: u
			integer :: v
			integer :: r
			integer :: s
! 	print*, " EnergyDerivator_calculateAnalyticUncoupledFirstDerivative" 	
			nucleiAndComponent= ParticleManager_getCenterOfOptimization(EnergyDerivator_instance.components(1) )
			
			if ( ParticleManager_isComponentFixed( nucleiAndComponent(1),nucleiAndComponent(2) ) ) then
				output=0.0_8
				return
			end if
			
			allocate( energyDerivative( ParticleManager_getNumberOfQuantumSpecies() ) )
			allocate( repulsionDerivativeComponent( ParticleManager_getNumberOfQuantumSpecies() ) )
			
						
			do specieIterator=1, ParticleManager_getNumberOfQuantumSpecies()
				
				matrixOfEigenvectors => null()
				matrixOfEigenvectors => WaveFunction_getWaveFunctionCoefficientsPtr( specieIterator )
				vectorOfEigenvalues => null()
				vectorOfEigenvalues => WaveFunction_getMolecularOrbitalsEnergyPtr( specieIterator )
				
				nameOfSpecie =  ParticleManager_getNameOfSpecie( specieIterator )
				ocupationNumber = ParticleManager_getOcupationNumber( specieIterator )
				orderOfMatrix = ParticleManager_getNumberOfContractions( specieIterator )
				
				!!*******************************************************************
				!! Matrices temporales para almacenamiento de derivadas 
				!! de integrales de orbitales atomicos
				!!
				allocate( auxMatrix(orderOfMatrix, orderOfMatrix) )
				allocate( otherAuxMatrix(orderOfMatrix, orderOfMatrix) )
				
				allocate( derivativeOfHcoreMatrix(orderOfMatrix, orderOfMatrix) )
				derivativeOfHcoreMatrix = Math_NaN
				
				allocate( derivativeOfOverlapMatrix(orderOfMatrix, orderOfMatrix) )
				derivativeOfOverlapMatrix = Math_NaN
				
				!! Calcula el numero de integrales que se almacenaran dentro de la matriz dada 
				numberOfIntegrals = ( orderOfMatrix    *  ( ( orderOfMatrix + 1.0_8) / 2.0_8 ) ) ** 2.0_8 
				allocate( derivativeOfRepulsionVector(numberOfIntegrals) )
				derivativeOfRepulsionVector = Math_NaN
				
				!!
				!!*******************************************************************
		
				!! Algoritmo provisional, debe emplearse esquema factorizado eficiente 
				!! de A New Dimension to Quantum Chemistry - Analytic Derivatives Methods pp.58
				!! eliminado para propositos de depuracion
				
				
				energyDerivative(specieIterator) = 0.0_8
				
				do i=1 , ocupationNumber
!!CONVERTIR EN UN UNICO CICLO
					do u = 1, orderOfMatrix
						do v = 1, orderOfMatrix
						
							energyDerivative(specieIterator) = energyDerivative(specieIterator) &
								+ matrixOfEigenvectors.values(u,i) &
								* matrixOfEigenvectors.values(v,i) &
								* ( dHcore(u,v) &
								- dOverlap(u,v) & 
								* vectorOfEigenvalues.values(i) )
						
						end do
					end do
				end do
				
				energyDerivative( specieIterator ) = ParticleManager_getLambda( specieIterator ) * energyDerivative( specieIterator )
				
				repulsionDerivativeComponent(specieIterator) = 0.0_8
		
! 				do i=1, ocupationNumber
! 					do j=1, ocupationNumber
! 						do u=1, orderOfMatrix
! 							do v=1, orderOfMatrix
! 								do r=1, orderOfMatrix
! 									do s=1, orderOfMatrix
! 										
! 										repulsionDerivativeComponent(specieIterator) = repulsionDerivativeComponent(specieIterator) &
! 											+ ( ParticleManager_getEta( nameOfSpecie)  * matrixOfEigenvectors.values(v,i) &
! 											* matrixOfEigenvectors.values(r,j) &
! 											+ ParticleManager_getKappa( nameOfSpecie)  * matrixOfEigenvectors.values(r,i) &
! 											* matrixOfEigenvectors.values(v,j) )&
! 											* matrixOfEigenvectors.values(s,j) &
! 											* matrixOfEigenvectors.values(u,i) &
! 											* dRepulsion( u,v,r,s )
! 							
! 									end do	
! 								end do
! 							end do
! 						end do
! 					end do
! 				end do
				
				!!
				!! Se debe garantiza que kappa se negativo  y eta positivo para emplea las siguientes expresiones
				!!	en caso contrario se debe descomentar el bloque anterior
				!!
				auxMatrix=( ( abs( ParticleManager_getEta( specieIterator ) ) )**0.5_8  )  * matrixOfEigenvectors.values
				otherAuxMatrix=( ( abs( ParticleManager_getKappa( specieIterator ) ) )**0.5_8 )  * matrixOfEigenvectors.values  
		
				do i=1, ocupationNumber
					do j=1, ocupationNumber
!!CONVERTIR EN UN UNICO CICLO
						do u=1, orderOfMatrix
							do v=1, orderOfMatrix
								do r=1, orderOfMatrix
									do s=1, orderOfMatrix
										
										repulsionDerivativeComponent(specieIterator) = repulsionDerivativeComponent(specieIterator) &
											+ ( auxMatrix(v,i) * auxMatrix(r,j) - otherAuxMatrix(r,i) * otherAuxMatrix(v,j) ) &
											* matrixOfEigenvectors.values(s,j) &
											* matrixOfEigenvectors.values(u,i) &
											* dRepulsion( u,v,r,s )
							
									end do	
								end do
							end do
						end do
					end do
				end do				
				
				repulsionDerivativeComponent( specieIterator ) = 	0.5_8 * ParticleManager_getLambda( specieIterator ) &
															* repulsionDerivativeComponent( specieIterator) &
															* ParticleManager_getCharge( specieID=specieIterator )**2.0
				
				energyDerivative( specieIterator ) = energyDerivative( specieIterator ) + repulsionDerivativeComponent( specieIterator )
				
				deallocate( auxMatrix )
				deallocate( otherAuxMatrix )
				deallocate( derivativeOfHcoreMatrix )
				deallocate( derivativeOfOverlapMatrix )
				deallocate( derivativeOfRepulsionVector )
		
			end do
		
			output = sum( energyDerivative )
			deallocate( energyDerivative )
			deallocate( repulsionDerivativeComponent )
			matrixOfEigenvectors => null()
			vectorOfEigenvalues => null()
			
			contains
			
				function dHcore(index_i, index_j) result( output )
					implicit none
					real(8) :: output
					integer :: index_i
					integer :: index_j
										
					
					if ( isNaN( derivativeOfHcoreMatrix( index_i, index_j ) ) ) then
						
						output=IntegralManager_getElement( KINETIC_DERIVATIVES, i=index_i, j=index_j, nameOfSpecie=nameOfSpecie, &
							nuclei = nucleiAndComponent(1), component = nucleiAndComponent(2)  )
						output = output + &
							IntegralManager_getElement( ATTRACTION_DERIVATIVES, i=index_i, j=index_j, nameOfSpecie=nameOfSpecie,&
							nuclei = nucleiAndComponent(1), component = nucleiAndComponent(2)  )
						
						derivativeOfHcoreMatrix(index_i, index_j) = output
						derivativeOfHcoreMatrix(index_j, index_i) = output
						
					else
					
						output = derivativeOfHcoreMatrix(index_i, index_j)
					
					end if
					
! print*, " dHcore" 
				end function dHcore
				
				function dOverlap(index_i, index_j) result( output )
					implicit none
					real(8) :: output
					integer :: index_i
					integer :: index_j
					
					if ( isNaN( derivativeOfOverlapMatrix( index_i, index_j) ) ) then
						
						output=IntegralManager_getElement( OVERLAP_DERIVATIVES, i=index_i, j=index_j, nameOfSpecie=nameOfSpecie, &
							nuclei = nucleiAndComponent(1), component = nucleiAndComponent(2)  )
					
						derivativeOfOverlapMatrix(index_i, index_j) = output
						derivativeOfOverlapMatrix(index_j, index_i) = output
						
					else
					
						output = derivativeOfOverlapMatrix(index_i, index_j)
					
					end if
				
! print*, " dOverlap" 
				end function dOverlap
				
				function dRepulsion(index_u, index_v, index_r, index_s) result( output )
					implicit none
					integer :: index_u
					integer :: index_v
					integer :: index_r
					integer :: index_s
					real(8) :: output
					
					integer(8) :: indexOfIntegral
					
					indexOfIntegral = IndexMap_tensorR4ToVector(index_u,index_v,index_r,index_s, int(orderOfMatrix), int(orderOfMatrix) )
					
					if ( isNaN( derivativeOfRepulsionVector( indexOfIntegral ) ) ) then
						
						output = ContractedGaussian_repulsionDerivative( &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_u).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_u).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_v).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_v).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_r).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_r).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_s).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_s).contractionIDInParticle), &
								nucleiAndComponent(1), nucleiAndComponent(2) )

						derivativeOfRepulsionVector( indexOfIntegral) = output
						
					else
					
						output = derivativeOfRepulsionVector( indexOfIntegral )
					
					end if
				
! print*, " dRepulsion" 
				end function dRepulsion
			
		

		end function EnergyDerivator_calculateAnalyticUncoupledFirstDerivative
		
		!**
		! @brief  Calcula la primera derivada analitica para la matriz de Fock sin incluir el termino de
		!               acoplamiento interspecie
		!**
		function EnergyDerivator_calculateAnalyticCouplingFirstDerivative() result(output)
			implicit none
			real(8) :: output
		
			character(30) :: nameOfSpecie
			character(30) :: nameOfOtherSpecie
			integer :: nucleiAndComponent(2)
			integer :: specieIterator
			integer :: otherSpecieIterator
			integer :: ocupationNumber
			integer :: otherOcupationNumber
			integer :: orderOfMatrix
			integer :: orderOfOtherMatrix
			real(8) :: couplingEnergyDerivative
			real(8) :: auxCouplingEnergyDerivative
			type(Matrix), allocatable :: derivativeOfRepulsionVector(:)
			real(8) :: lambda, otherLambda
			type(Matrix), pointer :: matrixOfEigenvectors
			type(Matrix), pointer :: otherMatrixOfEigenvectors
			integer(8) :: numberOfIntegrals
			integer :: iteratorOfMatrix
			integer :: a
			integer :: b
			integer :: u
			integer :: v
			integer :: r
			integer :: s
! 			print*, " EnergyDerivator_calculateAnalyticCouplingFirstDerivative" 
			nucleiAndComponent = ParticleManager_getCenterOfOptimization( EnergyDerivator_instance.components(1) )
			
			if ( ParticleManager_isComponentFixed( nucleiAndComponent(1),nucleiAndComponent(2) ) ) then
				output=0.0_8
				return
			end if
			
			iteratorOfMatrix = 	ParticleManager_getNumberOfQuantumSpecies() &
								* ( ParticleManager_getNumberOfQuantumSpecies() - 1 ) / 2 
			
			allocate( derivativeOfRepulsionVector(iteratorOfMatrix) )
			
			couplingEnergyDerivative = 0.0_8
			iteratorOfMatrix = 0
	
			do specieIterator = 1 , ParticleManager_getNumberOfQuantumSpecies()
			
				!! Obtiene vectores propios para una de las especies
				matrixOfEigenvectors => null()
				matrixOfEigenvectors => WaveFunction_getWaveFunctionCoefficientsPtr( specieIterator )
				orderOfMatrix = ParticleManager_getNumberOfContractions( specieIterator )
				ocupationNumber = ParticleManager_getOcupationNumber( specieIterator )
				lambda =ParticleManager_getLambda( specieIterator )
				nameOfSpecie =  ParticleManager_getNameOfSpecie( specieIterator )
			
				do otherSpecieIterator = specieIterator + 1, ParticleManager_getNumberOfQuantumSpecies()
					
					iteratorOfMatrix = iteratorOfMatrix + 1
							
					!! Obtiene vectores propios para otra especie
					otherMatrixOfEigenvectors => null()
					otherMatrixOfEigenvectors => WaveFunction_getWaveFunctionCoefficientsPtr( otherSpecieIterator )
					orderOfOtherMatrix = ParticleManager_getNumberOfContractions( otherSpecieIterator )
					otherOcupationNumber = ParticleManager_getOcupationNumber( otherSpecieIterator )
					otherLambda =ParticleManager_getLambda( otherSpecieIterator )
					nameOfOtherSpecie =  ParticleManager_getNameOfSpecie( otherSpecieIterator )
					auxCouplingEnergyDerivative=0.0_8
					
					do a=1, ocupationNumber
						do b=1,otherOcupationNumber
	
				!!CONVERTIR EN UN UNICO CICLO
						!!**************************************************************
						!! Suma derivadas de  la energia iterativamente sobre pares especies.
						!!
						do u = 1 , orderOfMatrix
							do v = 1 , orderOfMatrix
								do r = 1 , orderOfOtherMatrix
									do s = 1 , orderOfOtherMatrix
		
										auxCouplingEnergyDerivative = auxCouplingEnergyDerivative &
											+ ( matrixOfEigenvectors.values(u,a) * matrixOfEigenvectors.values(v,a) &
											* otherMatrixOfEigenvectors.values(r,b) * otherMatrixOfEigenvectors.values(s,b) &
											* dRepulsion( u,v,r,s )  )
										
									end do
								end do
							end do
						end do
						!!**************************************************************
	
						end do
					end do
					
					couplingEnergyDerivative = couplingEnergyDerivative + auxCouplingEnergyDerivative &
						* ParticleManager_getCharge( specieID=specieIterator ) &
						* ParticleManager_getCharge( specieID=otherSpecieIterator )  * lambda * otherLambda
					
				end do
			
			end do
			!!***************************************************************************
			
			output = couplingEnergyDerivative
			
			otherMatrixOfEigenvectors => null()
			matrixOfEigenvectors => null()
			
			do a=1, size(derivativeOfRepulsionVector)
				call Matrix_destructor( derivativeOfRepulsionVector(a) )
			end do
			deallocate( derivativeOfRepulsionVector )
			
			contains
			
				function dRepulsion(index_u, index_v, index_r, index_s) result( output )
					implicit none
					integer :: index_u
					integer :: index_v
					integer :: index_r
					integer :: index_s
					real(8) :: output
					integer(8) :: numberOfIntegrals
					
					integer(8) :: indexOfIntegral
					
					if ( .not.allocated( derivativeOfRepulsionVector(iteratorOfMatrix).values ) ) then
						
						numberOfIntegrals = 	( orderOfMatrix    *   ( orderOfMatrix + 1.0_8) / 2.0_8 ) * &
											( orderOfOtherMatrix * ( orderOfOtherMatrix + 1.0_8 ) / 2.0_8 )
						call Matrix_constructor( derivativeOfRepulsionVector(iteratorOfMatrix), numberOfIntegrals, 1_8, Math_NaN )
					
					end if
					
					indexOfIntegral = 	IndexMap_tensorR4ToVector(index_u,index_v,index_r,index_s, orderOfMatrix, orderOfOtherMatrix )
					
					if ( isNaN( derivativeOfRepulsionVector( iteratorOfMatrix ).values(indexOfIntegral, 1 ) ) ) then
						
						output = ContractedGaussian_repulsionDerivative( &
ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_u).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_u).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_v).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(specieIterator).contractionID(index_v).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(otherSpecieIterator).contractionID(index_r).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(otherSpecieIterator).contractionID(index_r).contractionIDInParticle), &
				ParticleManager_instance.particlesPtr(ParticleManager_instance.idsOfContractionsForSpecie(otherSpecieIterator).contractionID(index_s).particleID).basis.contractions(ParticleManager_instance.idsOfContractionsForSpecie(otherSpecieIterator).contractionID(index_s).contractionIDInParticle), &
							nucleiAndComponent(1), nucleiAndComponent(2) )
						
						derivativeOfRepulsionVector( iteratorOfMatrix ).values(indexOfIntegral, 1 ) = output
						
					else
					
						output = derivativeOfRepulsionVector( iteratorOfMatrix ).values(indexOfIntegral, 1 )
					
					end if
				
! print*, " dRepulsion" 
				end function dRepulsion
			

		end function EnergyDerivator_calculateAnalyticCouplingFirstDerivative
		
		
		!**
		! @brief Retorna la componente de la derivada anlitica asociada a las particulas puntuales del sistema
		!
		!**
		function EnergyDerivator_calculateFistDerivativeOfPuntualEnergy() result( output )
			implicit none
			real(8) :: output
			
			integer :: i
			integer :: j
			integer :: center
			integer :: otherCenter
			integer :: auxKronecker
			integer :: nucleiAndComponent(2)
			real(8) :: originComponent(3)
			
! 			print*, " EnergyDerivator_calculateFistDerivativeOfPuntualEnergy"
			nucleiAndComponent= ParticleManager_getCenterOfOptimization(EnergyDerivator_instance.components(1) )
			
			if ( ParticleManager_isComponentFixed( nucleiAndComponent(1),nucleiAndComponent(2) ) ) then
				output=0.0_8
				return
			end if
	
			output = 0.0_8 
			
			do i = 1 , ParticleManager_getNumberOfPuntualParticles()
				do j = i + 1 , ParticleManager_getNumberOfPuntualParticles()
					
					center= ParticleManager_getOwnerOfPuntualParticle( i )
					otherCenter= ParticleManager_getOwnerOfPuntualParticle( j )
					
					auxKronecker = 	Math_kroneckerDelta( nucleiAndComponent(1), center ) &
									- Math_kroneckerDelta( nucleiAndComponent(1), otherCenter )
					
					if (abs( auxKronecker ) > 0 ) then
				
						originComponent=	ParticleManager_getOriginOfPuntualParticle(i) &
										-ParticleManager_getOriginOfPuntualParticle( j) 
				
						output = output &
							+  ( (-1.0_8 * ParticleManager_getChargeOfPuntualParticle( i )  &
							*  ParticleManager_getChargeOfPuntualParticle( j )&
							/ ( sum( ( ParticleManager_getOriginOfPuntualParticle(i) &
							- ParticleManager_getOriginOfPuntualParticle(j) )**2.0_8 )  )**(1.5_8)  ) &
							* originComponent( nucleiAndComponent(2) ) * auxKronecker )
					end if
					
				end do
			end do
			
 
		end function EnergyDerivator_calculateFistDerivativeOfPuntualEnergy
	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine EnergyDerivator_exception( typeMessage, description, debugDescription)
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex
! print*, " EnergyDerivator_exception" 
			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	

	end subroutine EnergyDerivator_exception
		
	
end module EnergyDerivator_
