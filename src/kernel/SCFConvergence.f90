!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

module SCFConvergence_
	use Exception_
	use Matrix_
	use Vector_
	use StackOfMatrices_
	implicit none
	
	!>
	!! @brief Modulo para implementacion de m�todos de acelaracion convergencia SCF
	!!
	!! Este modulo contiene las implementaciones requeridas para realizar 
	!! procedimientos de aseguramineto o aceleracion de convergencia de metodo SCF.
	!! Normalmente estos metodos alteran la matriz de Fock y la matriz de densidad
	!! para forzar la convergencia de un procedimineto SCF no convergente.
	!!
	!! @author Sergio A. Gonzalez
	!!
	!! <b> Fecha de creacion : </b> 2008-09-20
	!!   - <tt> 2007-08-20 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
	!!        -# Creacion del modulo, y metodo "optimal damping"
	!! @todo implementar otros metdos como DIIS o Level shifting
	!<
	type, public :: SCFConvergence
		
		character(30) :: name
		
		type(Matrix) :: initialDensityMatrix
		type(Matrix) :: initialFockMatrix
				
		type(Matrix), pointer :: newFockMatrixPtr
		type(Matrix), pointer :: newDensityMatrixPtr
		type(Matrix), pointer :: overlapMatrixPtr
				
		!! Matrices requeidas en DIIS
		type(StackOfMatrices) :: componentsOfError
		type(StackOfMatrices) :: fockMatrices
		type(Matrix) :: diisEquationsSystem
		type(Matrix) :: orthonormalizationMatrix
				
		real(8) :: diisError
		integer :: methodType
		integer :: diisDimensionality
		integer :: currentsize
		logical :: isInstanced
		logical :: isLastComponent
		logical :: hadLowDiisError
	
	end type SCFConvergence

	!< enum Matrix_type {
	integer, parameter, public :: SCF_CONVERGENCE_DEFAULT			=  -1
	integer, parameter, public :: SCF_CONVERGENCE_NONE			=  0
	integer, parameter, public :: SCF_CONVERGENCE_DAMPING		=  1
	integer, parameter, public :: SCF_CONVERGENCE_DIIS				=  2
	integer, parameter, public :: SCF_CONVERGENCE_LEVEL_SHIFTING	=  3
	integer, parameter, public :: SCF_CONVERGENCE_MIXED			=  4
	!< }

	public ::&
		SCFConvergence_constructor, &
		SCFConvergence_destructor, &
		SCFConvergence_setName, &
		SCFConvergence_setInitialDensityMatrix, &
		SCFConvergence_setInitialFockMatrix, &
		SCFConvergence_getName, &
		SCFConvergence_isInstanced, &
		SCFConvergence_getInitialDensityMatrix, &
		SCFConvergence_getInitialFockMatrix, &
		SCFConvergence_getDiisError, &
		SCFConvergence_setMethodType, &
		SCFConvergence_setMethod, &
		SCFConvergence_run, &
		SCFConvergence_reset
		
		
	private	
contains	
		
		!>
		!! @brief Define el constructor para la clase
		!<
		subroutine SCFConvergence_constructor( this, name ,methodType )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			character(*),optional :: name
			integer, optional :: methodType
			
			this.name = "undefined"
			if ( present(name) ) this.name = trim(name)
			this.methodType = SCF_CONVERGENCE_DEFAULT
			if ( present(methodType) ) this.methodType = methodType
			this.isInstanced = .true.
			this.diisDimensionality = APMO_instance.DIIS_DIMENSIONALITY
			this.diisError = 0.0_8
			this.overlapMatrixPtr => null()
			this.isLastComponent = .false.
			this.hadLowDiisError = .false.

		end subroutine SCFConvergence_constructor
		
		!>
		!! @brief Define el destructor para la clase
		!<
		subroutine SCFConvergence_destructor( this )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			
			this.newFockMatrixPtr => null()
			this.newDensityMatrixPtr => null()
			if ( allocated(this.initialFockMatrix.values) ) call Matrix_destructor( this.initialFockMatrix)
			if ( allocated(this.initialDensityMatrix.values) ) call Matrix_destructor( this.initialDensityMatrix)
			if ( StackOfMatrices_isInstanced( this.fockMatrices ) ) then
				call StackOfMatrices_destructor( this.fockMatrices)
				call StackOfMatrices_destructor( this.componentsOfError)
				call Matrix_destructor( this.diisEquationsSystem )
				call Matrix_destructor( this.orthonormalizationMatrix )
			end if
			
			this.methodType =SCF_CONVERGENCE_DEFAULT
			this.isInstanced = .false.
			this.isLastComponent = .false.
			this.hadLowDiisError = .false.
			this.overlapMatrixPtr => null()
		
		end subroutine SCFConvergence_destructor
		
		!>
		!! @brief Permite asiganar un nombre al metodo
		!<
		subroutine SCFConvergence_setName( this, name )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			character(*), intent(in) :: name
			 
			this.name = trim(name)
		
		end subroutine SCFConvergence_setName
		
		!>
		!! @brief Ajusta la dimension del subespacio empleado por metodo DIIS
		!<
		subroutine SCFConvergence_setDiisDimensionality( this, diisDimensionality )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			integer, intent(in) :: diisDimensionality
			 
			this.diisDimensionality = diisDimensionality
		
		end subroutine SCFConvergence_setDiisDimensionality
		
		!>
		!! @brief Ajusta  la matriz de densidad inicial
		!<
		subroutine SCFConvergence_setInitialDensityMatrix( this, otherMatrix )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			type(Matrix), intent(in) :: otherMatrix
			 
			if (  this.isInstanced ) then
			
				call Matrix_copyConstructor( this.initialDensityMatrix, otherMatrix )
				
			else

				call SCFConvergence_exception( ERROR, "The object has not beeb instanced",&
				 "Class object SCFConvergence in setInitialDensityMatrix() function" )
			
			end if
		
		end subroutine SCFConvergence_setInitialDensityMatrix
		
		
		!>
		!! @brief Ajusta la matrix del Fock inicial
		!<
		subroutine SCFConvergence_setInitialFockMatrix( this, otherMatrix )
			implicit none
			type(SCFConvergence), intent(inout) :: this
			type(Matrix), intent(in) :: otherMatrix
			 
			if ( allocated( this.initialDensityMatrix.values ) ) then
			
					
				call Matrix_copyConstructor( this.initialFockMatrix, otherMatrix )
				
			else

				call SCFConvergence_exception( ERROR, "The object has not beeb instanced",&
				 "Class object SCFConvergence in setInitialFockMatrix() function"  )
				
			end if
		
		end subroutine SCFConvergence_setInitialFockMatrix


		!>
		!! @brief Define el metodo de convergencia SCF que debe emplear
		!<
		subroutine SCFConvergence_setMethodType( this,  methodType )
			implicit none
			type(SCFConvergence), intent(inout), target :: this
			integer,optional :: methodType
			

			if ( present(methodType) ) this.methodType = methodType
			
		
		end subroutine SCFConvergence_setMethodType
		
		!>
		!! @brief Ajusta los parametros requeridos por el metodo convergencia
		!<
		subroutine SCFConvergence_setMethod( this, newFockMatrix, newDensityMatrix, overlapMatrix, &
				 methodType )
			implicit none
			type(SCFConvergence), intent(inout), target :: this
			type(Matrix),target :: newFockMatrix
			type(Matrix),target :: newDensityMatrix
			type(Matrix),target, optional :: overlapMatrix
			integer, optional :: methodType

			type(Matrix) :: eigenVectors
			type(Vector) :: eigenValues
			integer :: orderOfMatrix
			integer :: i
			integer :: j

			this.newFockMatrixPtr => newFockMatrix
			this.newDensityMatrixPtr => newDensityMatrix

			if ( present(methodType) ) this.methodType = methodType

			if( present(overlapMatrix) .and. ( this.methodType == 2 .or. this.methodType == 4 .or. &
			APMO_instance.DIIS_ERROR_IN_DAMPING ) ) then
				this.overlapMatrixPtr => overlapMatrix
			
				if( .not.allocated(this.orthonormalizationMatrix.values) ) then

					!!*****************************************************
					!! Separa espacio de memoria para sistema de ecuaciones DIIS
					!!*****
					call Matrix_constructor( this.diisEquationsSystem, int(this.diisDimensionality+1,8), int(this.diisDimensionality+1,8) )
					this.diisEquationsSystem.values= 0.0_8
					this.diisEquationsSystem.values(1,:) = -1.0_8
					this.diisEquationsSystem.values(:,1) = -1.0_8
					this.diisEquationsSystem.values(1,1)= 0.0_8

					orderOfMatrix = size(this.newFockMatrixPtr.values, 1)
					this.orthonormalizationMatrix= this.overlapMatrixPtr
					
					call Vector_constructor( eigenValues, orderOfMatrix )
					call Matrix_constructor( eigenVectors, int(orderOfMatrix,8), int(orderOfMatrix,8) )
					
					call Matrix_eigen( this.orthonormalizationMatrix, eigenValues, eigenVectors, SYMMETRIC  )
					
					do i = 1 , orderOfMatrix
						do j = 1 , orderOfMatrix
							
							if ( eigenValues.values(j) > APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
								this.orthonormalizationMatrix.values(i,j) = &
										eigenVectors.values(i,j)/sqrt( eigenValues.values(j) )
							else
		
								call SCFConvergence_exception( ERROR, "The matrix of orthonormalization is singular",&
									"Class object SCFConvergence diis() function" )
		
							end if
						end do
					end do

					!!
					!! Esta matriz oermite obtener un vector de error balanceado e'=A~*e*A al usar una base ortonormal
					!!	donde A es la matriz de ortogonalizacion.
					!!
					this.orthonormalizationMatrix.values =	matmul( eigenVectors.values,&
														matmul( this.orthonormalizationMatrix.values, &
														transpose(eigenVectors.values) ) )

					call Matrix_destructor(eigenVectors)
					call Vector_destructor(eigenValues)

				end if

			end if

		end subroutine SCFConvergence_setMethod

		!>
		!! @brief Indica si el objeto ha sidi instanciado
		!>
		function SCFConvergence_isInstanced( this ) result( output )
			implicit none
			type(SCFConvergence), intent(in) :: this
			logical :: output
			 
			output =this.isInstanced
			
		end function SCFConvergence_isInstanced

		!>
		!! @brief devuelve el nombre del metodo
		!<
		function SCFConvergence_getName( this ) result( output )
			implicit none
			type(SCFConvergence), intent(in) :: this
			character(30) :: output
			 
			output =trim(this.name)
			
		
		end function SCFConvergence_getName

		!>
		!! @brief devuelve el nombre del metodo
		!<
		function SCFConvergence_getDiisError( this ) result( output )
			implicit none
			type(SCFConvergence), intent(in) :: this
			real(8) :: output
			 
			output = this.diisError
			
		
		end function SCFConvergence_getDiisError
		
		!>
		!! @brief devuelve la matrix de densidad inicial
		!<
		function SCFConvergence_getInitialDensityMatrix( this ) result( output )
			implicit none
			type(SCFConvergence), intent(in) :: this
			type(Matrix) :: output
			 
			if ( allocated( this.initialDensityMatrix.values ) ) then
			
				call Matrix_copyConstructor( output, this.initialDensityMatrix )
				
			else
				
				call SCFConvergence_exception( ERROR, "The object has not beeb instanced",&
				 "Class object SCFConvergence in getInitialDensityMatrix() function" )

			end if
		
		end function SCFConvergence_getInitialDensityMatrix
		
		!>
		!! @brief devuelve la matrix de Fock inicial
		!<
		function SCFConvergence_getInitialFockMatrix( this ) result( output )
			implicit none
			type(SCFConvergence), intent(in) :: this
			type(Matrix) :: output
			 
			if ( allocated( this.initialFockMatrix.values ) ) then
			
				call Matrix_copyConstructor( output, this.initialFockMatrix )
				
			else

				call SCFConvergence_exception( ERROR, "The object has not beeb instanced",&
				 "Class object SCFConvergence in getInitialFockMatrix() function" )
				
			end if
		
		end function SCFConvergence_getInitialFockMatrix
		
		
		!>
		!! @brief Calcula un mejor estimado de la matriz de Fock, mediante el 
		!!		metodo de convergencia seleccionado
		!<
		subroutine SCFConvergence_run(this)
			implicit none
			type(SCFConvergence) :: this

			if ( this.isInstanced ) then
			
				select case(this.methodType)
				
					case(SCF_CONVERGENCE_NONE)

						call SCFConvergence_none( this)
						
					case(SCF_CONVERGENCE_DAMPING)

						call SCFConvergence_damping( this )
					
					case(SCF_CONVERGENCE_DIIS)

						if ( StackOfMatrices_isInstanced( this.fockMatrices ) == .false. ) then
						
							call StackOfMatrices_constructor( this.fockMatrices, &
								ssize = this.diisDimensionality )
							call StackOfMatrices_constructor( this.componentsOfError, &
								ssize = this.diisDimensionality )
						end if
						
						call SCFConvergence_diis( this )
												
					case(SCF_CONVERGENCE_LEVEL_SHIFTING)

						call SCFConvergence_levelShifting( this )

					case( SCF_CONVERGENCE_MIXED )

						if ( StackOfMatrices_isInstanced( this.fockMatrices ) == .false. ) then
						
							call StackOfMatrices_constructor( this.fockMatrices, &
								ssize = this.diisDimensionality )
							call StackOfMatrices_constructor( this.componentsOfError, &
								ssize = this.diisDimensionality )
						end if
						
						call SCFConvergence_diis( this )
						if ( abs(this.diisError) > APMO_instance.DIIS_SWITCH_THRESHOLD .and. .not.this.hadLowDiisError ) then
							call SCFConvergence_damping( this )
						else 
							APMO_instance.DIIS_SWITCH_THRESHOLD=0.5
						end if

					case default

						call SCFConvergence_damping( this )
					
				end select
			
			else

				call SCFConvergence_exception( ERROR, "The object has not beeb instanced",&
				 "Class object SCFConvergence in run() function" )
			
			end if
		
		end subroutine SCFConvergence_run
		
		
		!>
		!! @brief No realiza ninguna modificacion a la matriz de Fock o a la
		!! 		matriz de densidad asociada
		!>
		subroutine SCFConvergence_none(  this )
			implicit none
			type(SCFConvergence) :: this
					
		end subroutine SCFConvergence_none
		
		
		!>
		!! @brief Calcula una nueva matriz de Fock con el metodo de amortiguamiento
		!<
		subroutine SCFConvergence_damping( this )
			implicit none
			type(SCFConvergence) :: this

			real(8) :: densityEffect
			real(8) :: fockAndDensityEffect
			real(8) :: dampingFactor
			type(Matrix) :: auxMatrix

			if( APMO_instance.DIIS_ERROR_IN_DAMPING ) then

				call SCFConvergence_obtainDiisError(this)
			end if
			!!********************************************************************************************
			!! Evalua el efecto del cambio de la matriz densidad en la energia
			!!
			call Matrix_copyConstructor( auxMatrix, this.initialFockMatrix)
			auxMatrix.values = matmul( auxMatrix.values, (this.newDensityMatrixPtr.values &
							- this.initialDensityMatrix.values) )

			densityEffect = -0.5_8 * Matrix_trace( auxMatrix )
			
			!!
			!!********************************************************************************************
			
			!!********************************************************************************************
			!! Evalua el efecto de los cambios de la matriz densidad y  la matiz de fock en energia
			!!
			auxMatrix.values = matmul( (this.newFockMatrixPtr.values &
								- this.initialFockMatrix.values), &
								( this.newDensityMatrixPtr.values &
								- this.initialDensityMatrix.values) )
								
			fockAndDensityEffect = Matrix_trace( auxMatrix )
			
			!!
			!!********************************************************************************************
			
 			if ( fockAndDensityEffect <=  densityEffect ) then

					this.initialFockMatrix.values = this.newFockMatrixPtr.values 
					this.initialDensityMatrix.values = this.newDensityMatrixPtr.values
			
			else
					dampingFactor = densityEffect / fockAndDensityEffect 
					this.initialFockMatrix.values = this.initialFockMatrix.values &
						+ dampingFactor * ( this.newFockMatrixPtr.values &
						- this.initialFockMatrix.values )
					
					!! Modifica la matrix de Fock con una matriz amortiguada
					this.newFockMatrixPtr.values = this.initialFockMatrix.values
					
					this.initialDensityMatrix.values = this.initialDensityMatrix.values &
						+ dampingFactor * ( this.newDensityMatrixPtr.values &
						- this.initialDensityMatrix.values )
					
			end if
			
			call Matrix_destructor( auxMatrix )

		end subroutine SCFConvergence_damping
		
		!>
		!! @brief Calcula una nueva matriz de densidad con la componentes de un
		!!		subespacio definido con la matrices de Fock anteriores
		!! 		y a la matriz de densidad asociada
		!<
		subroutine SCFConvergence_diis( this)
			implicit none
			type(SCFConvergence) :: this
						 
			type(Matrix) :: currentError
			real(8), pointer :: currentErrorPtr(:,:)
			real(8), pointer :: beforeErrorPtr(:,:)
			real(8), pointer :: fockMatrix(:,:)
			real(8), allocatable :: rightSideCoefficients(:)
			real(8), allocatable :: coefficientsOfCombination(:)
			type(Matrix) :: diisEquationsSystem !! Debe almacenarse una para cada especie
			type(Matrix) :: rightMatrix
			type(Matrix) :: leftMatrix
			type(Matrix) :: singularValues
			type(Matrix) :: auxMatrix
			integer :: orderOfMatrix
			integer :: i
			integer :: j

			orderOfMatrix = size(this.newFockMatrixPtr.values, 1)
			call Matrix_constructor( currentError, int(orderOfMatrix,8), int(orderOfMatrix,8 ) )

			!! Calcula FDS
			currentError.values = matmul( matmul( this.newFockMatrixPtr.values,&
				this.newDensityMatrixPtr.values), this.overlapMatrixPtr.values )
			
			!! Calcula la matriz de error FDS-SDF
			currentError.values = currentError.values - transpose(currentError.values)
			
			!! Ortonormaliza la componente de error
			currentError.values = matmul(transpose(this.orthonormalizationMatrix.values), &
									matmul( currentError.values, this.orthonormalizationMatrix.values) )

			!! Determina el error para la nueva componente
			this.diisError = abs( maxval(currentError.values) )

			if( this.diisError < APMO_instance.DIIS_SWITCH_THRESHOLD )  then	

				!! Almacena matriz de error y matriz de Fock
				call StackOfMatrices_push( this.componentsOfError, currentError.values )
				call StackOfMatrices_push( this.fockMatrices,  this.newFockMatrixPtr.values)
			
				this.currentSize = StackOfMatrices_size( this.fockMatrices )
			
				if ( this.currentsize >= 2 ) then
					
					!! Vector con valores de la constantes  del sistema lineal
					allocate( rightSideCoefficients( this.currentSize + 1 ) )
					rightSideCoefficients=0.0_8
					rightSideCoefficients(1)= -1.0_8
								
					allocate( coefficientsOfCombination( this.currentSize + 1 ) )
					call Matrix_constructor(auxMatrix, int(this.currentsize+1,8), int(this.currentsize+1,8) )

					!! Elimina la primera fila de la matriz de error y mueve las siguiente en un nivel
					if ( this.isLastComponent ) then

						this.diisEquationsSystem.values(2:this.currentsize,2:this.currentSize) = & 
							this.diisEquationsSystem.values(3:this.currentSize+1,3:this.currentSize+1)
						
					end if

					if ( this.currentSize <= this.diisDimensionality ) then
						
						!!
						!! Calcula la ultima fila de la matriz de error
						!! 
						currentErrorPtr => StackOfMatrices_getValuesPtr( this.componentsOfError, this.currentSize )
						do i=this.currentSize,1,-1
							beforeErrorPtr  => StackOfMatrices_getValuesPtr( this.componentsOfError, i )
							this.diisEquationsSystem.values(i+1,this.currentSize+1) = sum(currentErrorPtr*beforeErrorPtr)
							this.diisEquationsSystem.values(this.currentSize+1,i+1)  =  &
								this.diisEquationsSystem.values(i+1,this.currentSize+1)
							beforeErrorPtr => null()
						end do
						currentErrorPtr => null()
						if( this.currentSize==this.diisDimensionality ) this.isLastComponent=.true.
					end if

					auxMatrix.values = this.diisEquationsSystem.values(1:this.currentSize+1,1:this.currentSize+1)

					!! Diagonaliza la matiz por SVD de la forma A=UWV^T
					call Matrix_svd( auxMatrix, leftMatrix, rightMatrix, singularValues)
					
					!! Invierte la matriz de valores singulares
					do i=1,size(singularValues.values,dim=1)
						if( abs(singularValues.values(i,i)) > 1.0D-4 ) then
							singularValues.values(i,i) = 1/singularValues.values(i,i)
						else
							singularValues.values(i,i) = singularValues.values(i,i)
						end if
					end do
					
					coefficientsOfCombination=matmul( &
								matmul(matmul(transpose(rightMatrix.values), &
								singularValues.values),transpose(leftMatrix.values)),rightSideCoefficients )

					call Matrix_destructor( auxMatrix )
					call Matrix_destructor( singularValues )
					call Matrix_destructor( rightMatrix )
					call Matrix_destructor( leftMatrix )
				
					!! Construye una nueva matriz de Fock con una combinacion lineal de elementos del sub-espacio de F
					this.newFockMatrixPtr.values = 0.0_8
					
					do i=1, this.currentSize
						fockMatrix => StackOfMatrices_getValuesPtr( this.fockMatrices, i )
						this.newFockMatrixPtr.values =  &
							this.newFockMatrixPtr.values + coefficientsOfCombination(i+1) * fockMatrix
						fockMatrix => null()
					end do
					
					deallocate( rightSideCoefficients )
					deallocate( coefficientsOfCombination )
				
				else &
				if ( this.currentsize == 1 ) then
					
					currentErrorPtr => StackOfMatrices_getValuesPtr( this.componentsOfError, 1 )
					beforeErrorPtr  => StackOfMatrices_getValuesPtr( this.componentsOfError, 1 )
					this.diisEquationsSystem.values(2,2) = sum(currentErrorPtr*beforeErrorPtr)
					currentErrorPtr => null()
					beforeErrorPtr => null()
	
				end if
				this.hadLowDiisError = .true.
			end if

			call Matrix_destructor(currentError)
					
		end subroutine SCFConvergence_diis
		
		!>
		!! @brief Retorna el error diis de una matriz de fock
		!<
		subroutine SCFConvergence_obtainDiisError(this)
			implicit none
			type(SCFConvergence) :: this
						 
			type(Matrix) :: currentError
			integer :: orderOfMatrix

			orderOfMatrix = size(this.newFockMatrixPtr.values, 1)
			call Matrix_constructor( currentError, int(orderOfMatrix,8), int(orderOfMatrix,8 ) )

			!! Calcula FDS
			currentError.values = matmul( matmul( this.newFockMatrixPtr.values,&
				this.newDensityMatrixPtr.values), this.overlapMatrixPtr.values )
			
			!! Calcula la matriz de error FDS-SDF
			currentError.values = currentError.values - transpose(currentError.values)
			
			!! Ortonormaliza la componente de error
			currentError.values = matmul(transpose(this.orthonormalizationMatrix.values), &
									matmul( currentError.values, this.orthonormalizationMatrix.values) )

			this.diisError = abs( maxval(currentError.values) )

			call Matrix_destructor( currentError)

		end subroutine SCFConvergence_obtainDiisError

		!>
		!! @brief Modifica la Matriz de Fock empleando el metodo level shifting
		!!
		!! @todo Falta la implementacion completa 
		!<
		subroutine SCFConvergence_levelShifting(this)
			implicit none
			type(SCFConvergence) :: this
		
		end subroutine SCFConvergence_levelShifting


		subroutine SCFConvergence_reset()
			implicit none

			APMO_instance.DIIS_SWITCH_THRESHOLD = APMO_instance.DIIS_SWITCH_THRESHOLD_BKP

		end subroutine SCFConvergence_reset

		!>
		!! @brief  Maneja excepciones de la clase
		!<
		subroutine SCFConvergence_exception( typeMessage, description, debugDescription)
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
		end subroutine SCFConvergence_exception

	
end module SCFConvergence_	