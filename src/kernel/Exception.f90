!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief  Modulo para manejo de errores
!
! @author Sergio A. Gonz�ez M�ico
!
! <b> Fecha de creaci� : </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificaci�.
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonz�ez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapt�al estandar de codificaci� propuesto.
!**
module Exception_

	type, public :: Exception
		character(100) :: debugDescription
		character(255) :: description
		integer :: typeMessage
	end type Exception
	
 	integer, public, parameter :: INFORMATION = 1
	integer, public, parameter :: WARNING = 2
	integer, public, parameter :: ERROR = 3
	
	public :: &
		Exception_constructor, &
		Exception_destructor, &
		Exception_show, &
		Exception_setDebugDescription, &
		Exception_setDescription
	
contains

	subroutine Exception_constructor( this, typeMessage,  debugDescription, description )
		implicit none
		type(Exception) :: this
		integer :: typeMessage
		character(*), optional :: debugDescription
		character(*), optional :: description
		
		select case ( typeMessage )
		
			case( INFORMATION )
				this%typeMessage=INFORMATION
			
			case( WARNING )
				this%typeMessage=WARNING
			
			case( ERROR )
				this%typeMessage=ERROR
		
		end select
		
		if ( present(debugDescription) ) then
			
			this%debugDescription = trim(debugDescription)
		else
		
			this%debugDescription = "Undefined"
			
		end if
		
		if ( present(description) ) then
			
			this%description = trim(description)
			
		else
				
			this%description = "Undefined"
				
		end if
		
	end subroutine Exception_constructor
	
	subroutine Exception_destructor( this )
		implicit none
		type(Exception) :: this
		
		this%description = ""
		this%debugDescription = ""
		
	end subroutine Exception_destructor

	subroutine Exception_show( this )
		implicit none
		type(Exception) :: this
		
		select case  ( this%typeMessage )
		
			case( INFORMATION )
				print *,""
				print *,"+++ INFORMATION +++"
				print *,"   Debug description: ", trim(this%debugDescription)
				print *,"   Description: ", trim(this%description)
				print *,"+++"
				print *,""
				
			case( WARNING )
				print *,""
				print *,"!!! WARNING !!!"
				print *,"   Debug description: ", trim(this%debugDescription)
				print *,"   Description: ", trim(this%description)
				print *,"!!!"
				print *,""
				
			case( ERROR )
				print *,""
				print *,"### ERROR ###"
				print *,"   Debug description: ", trim(this%debugDescription)
				print *,"   Description: ", trim(this%description)
				print *,"###"
				print *,""
				call abort("")
		end select
			
		
	end subroutine Exception_show

	subroutine Exception_setDebugDescription( this, debugDescription )
		implicit none
		type(Exception) :: this
		character(*) :: debugDescription
		
		this%debugDescription = trim(debugDescription)
		
	end subroutine Exception_setDebugDescription

	subroutine Exception_setDescription( this , description )
		implicit none
		type(Exception) :: this
		character(*) :: description
		
		this%description = trim(description)
		
	end subroutine Exception_setDescription
	
	
end module Exception_
