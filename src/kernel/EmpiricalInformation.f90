!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module EmpiricalInformation_
	use APMO_
	use Matrix_
	use Vector_
	use AtomicElement_
	use ParticleManager_
	use MolecularSystem_
	use Exception_
	
	implicit none
	
	
	!>
	!! @brief Clase encargada del proveer parametros e informacion empirica del sistema
	!!		
	!!
	!! @author Sergio A. Gonzalez-Monico
	!!
	!! <b> Fecha de creacion : </b> 2009-06-258
	!!   - <tt> 2009-06-11 </tt>: Sergio A. Gonzalez-Monico ( sagonzalezm@unal.edu.co )
	!!        -# Creacion del archivo y las funciones basicas
	!<	
	type, public :: EmpiricalInformation
		character(20) :: name
	end type
	
	public :: &
		EmpiricalInformation_constructor, &
		EmpiricalInformation_destructor, &
		EmpiricalInformation_getCartesianForceConstants

private
contains

	!>
	!! @brief Constructor para la clase
	!<
	subroutine EmpiricalInformation_constructor( this )
		implicit none
		type(EmpiricalInformation) :: this
		
	end subroutine EmpiricalInformation_constructor

	!>
	!! Destructor para la clase
	!<
	subroutine EmpiricalInformation_destructor( this)
		implicit none
		type(EmpiricalInformation) :: this
		
	end subroutine EmpiricalInformation_destructor
	
	!>
	!! @brief 	Retorna una aproximacion a la matriz hessiana, basada en las 
	!!		formulas empiricas de Fischer y Almlof para calculo de constantes
	!!		de fuerza. J Phys Chem. 96, 24 1992, 9768-9774
	!!		de las variables independientes 
	!<
	function EmpiricalInformation_getCartesianForceConstants( this , system )  result( output )
		implicit none
		type(EmpiricalInformation) :: this
		type(MolecularSystem) :: system
		type(Matrix) :: output

		type(Vector) ::  cartesianCoordinates
		type(AtomicElement) :: element
		type(Exception) :: ex
		character(10) :: auxSymbol
		real(8) :: covalentRadius
		real(8) :: strengthConstant
		real(8) :: particlesDistance
		real(8) :: componentsOfStengthConstant(3,3)
		real(8) :: origin(3)
		integer :: numberOfCoordinates
		integer :: iteratorOfParticles_1
		integer :: iteratorOfParticles_2
		integer :: beginOfParticle
		integer :: beginOfOtherParticle
		integer :: iteratorOfCenter
		integer :: iteratorOfOtherCenter
		integer :: i
		integer :: j
		
		i=ParticleManager_getNumberOfCoordinates()
		
		call Matrix_constructor(output, int(i,8), int(i,8) )
		output.values = 0.0_8
		iteratorOfCenter = 0
		iteratorOfOtherCenter = 0
		
		call AtomicElement_constructor( element )
		
		!! Calculo de elementos fuera de la diagonal
		do iteratorOfParticles_1=1, ParticleManager_getTotalNumberOfParticles()
		
			if (   ParticleManager_isCenterOfOptimization( iteratorOfParticles_1 ) ) then
				iteratorOfCenter = iteratorOfCenter + 1
				iteratorOfOtherCenter =0

				do iteratorOfParticles_2=1, iteratorOfParticles_1-1
		
					if (   ParticleManager_isCenterOfOptimization( iteratorOfParticles_2 )  )  then
				
						iteratorOfOtherCenter = iteratorOfOtherCenter + 1

						auxSymbol=trim( ParticleManager_getSymbol( iteratorOfParticles_1 ) )
					
						if (  scan( auxSymbol, "_" ) /= 0 ) then
							auxSymbol = trim(auxSymbol(1: scan( auxSymbol, "_" ) - 1 ) )
						end if
					
						call AtomicElementManager_loadElement( element, auxSymbol )
						covalentRadius= element.covalentRadius
!						print *,"radio covalente para ",trim(auxSymbol),element.covalentRadius
						auxSymbol=trim(ParticleManager_getSymbol( iteratorOfParticles_2 ))
						
						if (  scan( auxSymbol, "_" ) /= 0 ) then
							auxSymbol = trim(auxSymbol(1: scan( auxSymbol, "_" ) - 1 ) )
						end if

						call AtomicElementManager_loadElement( element, auxSymbol )
						covalentRadius = covalentRadius + element.covalentRadius
!						print *,"radio covalente para ",trim(auxSymbol),element.covalentRadius

						origin = ParticleManager_getOrigin( iterator = iteratorOfParticles_1 ) 
						
						origin = origin - ParticleManager_getOrigin( iterator = iteratorOfParticles_2 )
						
						origin = origin * AMSTRONG

						particlesDistance = sum( origin*origin )
						strengthConstant = 0.3601_8 * exp( -1.944_8*( dsqrt(particlesDistance) -  covalentRadius ) )

						componentsOfStengthConstant(1,1) = (origin(1)*origin(1)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(1,2) = (origin(1)*origin(2)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(1,3) = (origin(1)*origin(3)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(2,2) = (origin(2)*origin(2)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(2,3) = (origin(2)*origin(3)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(3,3) = (origin(3)*origin(3)* strengthConstant)/particlesDistance
						componentsOfStengthConstant(2,1) =componentsOfStengthConstant(1,2)
						componentsOfStengthConstant(3,1) =componentsOfStengthConstant(1,3)
						componentsOfStengthConstant(3,2) =componentsOfStengthConstant(2,3)
						
						beginOfParticle = (iteratorOfCenter - 1) * 3
						beginOfOtherParticle = (iteratorOfOtherCenter - 1) * 3
						do i=1,3
							do j=1,3
								
								output.values( beginOfParticle+i, beginOfParticle +j) = &
									output.values( beginOfParticle+i, beginOfParticle +j) &
									+ componentsOfStengthConstant(i,j)

								output.values( beginOfOtherParticle+i, beginOfOtherParticle +j) = &
									output.values( beginOfOtherParticle+i, beginOfOtherParticle +j) &
									+ componentsOfStengthConstant(i,j)
								
								output.values( beginOfParticle+i, beginOfOtherParticle +j) = &
									output.values( beginOfParticle+i, beginOfOtherParticle +j) &
									- componentsOfStengthConstant(i,j)
								
								output.values( beginOfOtherParticle+i, beginOfParticle +j) = &
									output.values( beginOfOtherParticle+i, beginOfParticle +j) &
									- componentsOfStengthConstant(i,j)
							end do
						end do
					
					end if
		
				end do
			end if
		end do
	
		do i=1, size(output.values, dim=1)
			
			if(output.values(i,i) < 0.00001_8 ) output.values(i,i) = 0.00001_8
		
		end do
		

		call AtomicElement_destructor(element)
		
	end function EmpiricalInformation_getCartesianForceConstants
	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine EmpiricalInformation_exception( typeMessage, description, debugDescription )
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
	end subroutine EmpiricalInformation_exception


end module EmpiricalInformation_
