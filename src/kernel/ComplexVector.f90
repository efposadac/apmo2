!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Clase encargada de manipular todo lo relacionado con matrices
!
! Esta clase manipula todo lo relacionado con matrices de tipo numerico, ademas
! de servir como una interface transparente para el uso de LAPACK en cuanto a metodos
! que involuran algebra lineal
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-19
!   - <tt> 2007-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!   - <tt> 2007-08-26 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Acoplamiento con el primer nivel de BLAS (daxpy,ddot,dnrm2,idamax)
!**
module ComplexVector_
	use APMO_
	use Exception_
	use BlasInterface_
	
	implicit none
	
	!< enum ComplexVector_printFormatFlags {
	integer, parameter :: HORIZONTAL = 1
	integer, parameter :: VERTICAL = 2
	integer, parameter :: WITH_KEYS = 4
	!< }
	
	type, public :: ComplexVector
		complex(8) , allocatable :: values(:)
	end type ComplexVector
	
	public :: &
		ComplexVector_constructor, &
		ComplexVector_copyConstructor, &
		ComplexVector_destructor, &
		ComplexVector_show, &
		ComplexVector_getPtr, &
		ComplexVector_swapElements, &
		ComplexVector_getSize, &
		ComplexVector_getElement, &
		ComplexVector_setElement, &
		ComplexVector_setIdentity, &
		ComplexVector_setNull, &
		ComplexVector_isNull, &
		ComplexVector_plus, &
		ComplexVector_dot, &
		ComplexVector_norm
		
	private :: &
		ComplexVector_getSign
		
contains
	
	!**
	! Constructor por omisi�n
	!**
	subroutine ComplexVector_constructor( this, ssize, value, values )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer, intent(in) :: ssize
		complex(8), optional, intent(in) :: value
		complex(8), optional, intent(in) :: values(:)
		
		complex(8) :: valueTmp
		integer :: i
		
		valueTmp = cmplx(0.0_8, 0.0_8)
		
		allocate( this.values( 2*ssize ) )
		
		if( present(value) ) then
		
			valueTmp = value
			this.values = valueTmp
			
		elseif( present(values) ) then
			
			do i=1, size(values, DIM=1)
				this.values(2*i) = values(i)
			end do
			
		else
		
			this.values = valueTmp
			
		end if
		
	end subroutine ComplexVector_constructor
	
	!**
	! @brief Constructor de copia
	! Reserva la memoria necesaria para otherMatrix y le asigna los valores de this
	!**
	subroutine ComplexVector_copyConstructor( this, otherVector )
		implicit none
		type(ComplexVector), intent(inout) :: this
		type(ComplexVector), intent(in) :: otherVector
		
		allocate( this.values( size(otherVector.values, DIM=1) ) )
		
		this.values = otherVector.values
		
	end subroutine ComplexVector_copyConstructor
		
	!**
	! Destructor
	!**
	subroutine ComplexVector_destructor( this )
		implicit none
		type(ComplexVector), intent(inout) :: this
		
		deallocate( this.values )
		
	end subroutine ComplexVector_destructor
	
	!**
	! @internal
	! Creada para poner el signo de la parte compleja en el m�todo
	! ComplexVector_show(), para el caso de impresi�n horizontal
	!**
	function ComplexVector_getSign( value ) result ( output )
		real(8), intent(in) :: value
		character(2) :: output
		
		if( value > 0 ) then
			output = "+ "
		else
			output = "- "
		end if
	end function
	
	!**
	! Imprime a salida estandar la matriz realizando cambio de linea
	! con un maximo de "APMO_instance.FORMAT_NUMBER_OF_COLUMNS" columnas
	!**
	subroutine ComplexVector_show( this, flags, keys )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer, intent(in), optional :: flags
		character(5), intent(in), optional :: keys(:)
		
		integer :: columns
		integer :: ssize
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: u
		integer :: tmpFlags
		type(Exception) :: ex
		
		tmpFlags = HORIZONTAL
		if( present(flags) ) then
			if( flags == WITH_KEYS )  then
				tmpFlags = HORIZONTAL + WITH_KEYS
			else
				tmpFlags = flags
			end if
		end if
		
		ssize = size( this.values , DIM=1 )
		
		if( tmpFlags == HORIZONTAL .or. tmpFlags == HORIZONTAL + WITH_KEYS ) then
		
			columns = ssize
			
			do k=1, ceiling( ((ssize/2) * 1.0)/((APMO_instance.FORMAT_NUMBER_OF_COLUMNS/2) * 1.0 ) )
				
				l = ( APMO_instance.FORMAT_NUMBER_OF_COLUMNS/2 ) * ( k - 1 ) + 1
				u = ( APMO_instance.FORMAT_NUMBER_OF_COLUMNS/2 ) * ( k )
				
				if( u > ssize/2 ) then
					columns = l + APMO_instance.FORMAT_NUMBER_OF_COLUMNS*( 1 - k ) +  ssize - 1
					u = columns - 2
				end if
				
				if( ( tmpFlags - HORIZONTAL ) == WITH_KEYS ) then
				
					if( present( keys ) ) then
						write (6,"(<columns>A31)") ( keys(i), i = l, u )
					else
						write (6,"(<columns>I31)") ( i, i = l, u )
					end if
					
				end if
				
				print *,""
				
				write (6,"(<columns>(F15.6,A5,F10.6,A))") ( &
					( real(this.values(2*i)), ComplexVector_getSign(aimag(this.values(2*i))) , &
					abs(aimag(this.values(2*i))), "i" ), i = l, u )
				print *,""
				
			end do
			
		else if( tmpFlags == VERTICAL .or. tmpFlags == VERTICAL + WITH_KEYS ) then
			
			if( ( tmpFlags - VERTICAL ) == WITH_KEYS ) then
				
				if( present( keys ) ) then
					do i=1, ssize/2
						if( aimag(this.values(2*i)) < 0 ) then
							write (6,"(A5,F15.6,A5,F10.6,A)") &
							keys(i), real(this.values(2*i)), "- " , abs(aimag(this.values(2*i))), "i"
						else
							write (6,"(A5,F15.6,A5,F10.6,A)") &
							keys(i), real(this.values(2*i)), "+ " , abs(aimag(this.values(2*i))), "i"
						end if
					end do
				else
					do i=1, ssize/2
						if( aimag(this.values(2*i)) < 0 ) then
							write (6,"(I5,F15.6,A5,F10.6,A)") &
							i, real(this.values(2*i)), "- " , abs(aimag(this.values(2*i))), "i"
						else
							write (6,"(I5,F15.6,A5,F10.6,A)") &
							i, real(this.values(2*i)), "+ " , abs(aimag(this.values(2*i))), "i"
						end if
					end do
				end if
				
			else
				
				do i=1, ssize/2
					if( aimag(this.values(2*i)) < 0 ) then
						write (6,"(F15.6,A5,F10.6,A)") &
						real(this.values(2*i)), "- ", abs(aimag(this.values(2*i))), "i"
					else
						write (6,"(F15.6,A5,F10.6,A)") &
						real(this.values(2*i)), "+ ", aimag(this.values(2*i)), "i"
					end if
				end do
				
			end if
			
		else
		
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object ComplexVector in the show() function" )
			call Exception_setDescription( ex, "Bad flags selected" )
			call Exception_show( ex )
			
		end if
		
	end subroutine ComplexVector_show
		
	!**
	! Devuelve un apuntador a la matrix solicitada
	!
	! @param this matrix de m x n
	! @return Apuntador a la matriz solicitada.
	! @todo No ha sido probada
	!**
	function ComplexVector_getPtr( this ) result( output )
		implicit none
		type(ComplexVector) , target , intent(in) :: this
		complex(8) , pointer :: output(:)
		
		output => null()
		output => this.values
	
	end function ComplexVector_getPtr
	
	!**
	! Intercambia los elementos i y j el vector
	!**
	subroutine ComplexVector_swapElements( this, i, j )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer, intent(in) :: i
		integer, intent(in) :: j
		
		real(8) :: value1
		real(8) :: value2
		
		value1 = this.values( i )
		value2 = this.values( j )
		
		this.values( i ) = value2
		this.values( j ) = value1

	end subroutine ComplexVector_swapElements
	
	!**
	! Retorna el tama�o del vector
	! @todo No ha sido probado
	!**
	function ComplexVector_getSize( this ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer :: output
		
		output = size( this.values , DIM=1 )/2
		
	end function ComplexVector_getSize
	
	!**
	! Retorna el elemento i-esimo del vector
	! @todo No ha sido probado
	!**
	function ComplexVector_getElement( this, i ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer, intent(in) :: i
		real(8) :: output
		
		output = this.values( i )
		
	end function ComplexVector_getElement
	
	!**
	! Selecciona el valor del elemento i-esimo del vector
	!**
	subroutine ComplexVector_setElement( this, i, value )
		implicit none
		type(ComplexVector), intent(inout) :: this
		integer, intent(in) :: i
		complex(8), intent(in) :: value
		
		this.values( 2*i ) = value
		
	end subroutine ComplexVector_setElement
	
	!**
	! Selecciona todos los elementos del vector a 1.0
	! @todo No ha sido probado
	!**
	subroutine ComplexVector_setIdentity( this )
		implicit none
		type(ComplexVector), intent(inout) :: this
		
		this.values = cmplx(1.0_8, 0.0_8)
		
	end subroutine ComplexVector_setIdentity
	
	!**
	! Selecciona a cero todos los elementos del vector
	! @todo No ha sido probado
	!**
	subroutine ComplexVector_setNull( this )
		implicit none
		type(ComplexVector), intent(inout) :: this
		
		this.values = cmplx(0.0_8, 0.0_8)
		
	end subroutine ComplexVector_setNull
	
	!**
	! Retorna true si todos lo elementos del vector sun iguales a
	! cero, false de otra manera
	!
	! @todo Falta implementar, depende de la declaracion de un umbral de comparacion de enteros en la clase APMO
	!**
	function ComplexVector_isNull( this ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		logical :: output
		
		output = .false.
		
	end function ComplexVector_isNull
	
	!**
	! Suma dos vectores
	!
	! @warning No verifica que los dos vectores sean del mismo tama�o
	!**
	function ComplexVector_plus( this, otherVector ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		type(ComplexVector), intent(in) :: otherVector
		type(ComplexVector) :: output
		
		call ComplexVector_copyConstructor( output, otherVector )
		
!! #ifdef BLAS_VECTOR_SUPPORT
!! 		call daxpy( size(this.values), 1.0_8, this.values, 1, output.values, 1 )
!! #else
		output.values = this.values + otherVector.values
!! #endif
		
	end function ComplexVector_plus
	
	!**
	! Calcula el producto punto de dos vectores
	!
	! @warning No verifica que los dos vectores sean del mismo tama�o
	! @todo No funciona
	!**
	function ComplexVector_dot( this, otherVector ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		type(ComplexVector), intent(in) :: otherVector
		complex(8) :: output
		
!! #ifdef BLAS_VECTOR_SUPPORT
!!		output = zdotc( size(this.values)/2, this.values, 1, otherVector.values, 1 )
!! #else
!! 		output = dot_product( this.values, otherVector.values )
!! #endif
		
	end function ComplexVector_dot
	
	!**
	! Calcula la norma euclideana de un vector
	! @todo Funciona solo con soporte blas
	!**
	function ComplexVector_norm( this ) result ( output )
		implicit none
		type(ComplexVector), intent(inout) :: this
		real(8) :: output
		
!! #ifdef BLAS_VECTOR_SUPPORT
!!		output = dznrm2( size(this.values)/2, this.values, 1 )
!! #else
!! 		output = sqrt( dot_product( this.values, this.values ) )
!! #endif
		
	end function ComplexVector_norm
	
end module ComplexVector_
