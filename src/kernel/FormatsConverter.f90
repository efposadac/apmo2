!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module FormatsConverter_
	use  MolecularSystem_
	use BasisSet_
	use APMO_
	use ParticleManager_
	use Matrix_

	implicit none
	type, public :: FormatsConverter
		character(50) :: name
	end type
	
	type( FormatsConverter ), public :: FormatsConverter_instance
	
	public :: &
		FormatsConverter_constructor, &
		FormatsConverter_destructor, &
		FormatsConverter_writeMoldenFile, &
		FormatsConverter_writeAIMfile



contains
	subroutine FormatsConverter_constructor()
		implicit none
	end subroutine FormatsConverter_constructor

	subroutine FormatsConverter_destructor()
		implicit none
	end subroutine FormatsConverter_destructor

	subroutine FormatsConverter_writeMoldenFile( MolecularSystemInstance )
		implicit none
		type(MolecularSystem) :: MolecularSystemInstance
		
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: m
		integer :: specieID
		real :: occupation
		integer :: occupationTotal
		logical :: wasPress
		character(10) :: auxString
		character(10) :: symbol
		real(8) :: origin(3)
		real(8), allocatable :: charges(:)
		type(Matrix) :: localizationOfCenters
		type(Matrix) :: auxMatrix
		character(10),allocatable :: labels(:)


		if ( APMO_instance%ARE_THERE_DUMMY_ATOMS ) then
			auxString=ParticleManager_getNameOfSpecie( 1 )
			open(10,file=trim(APMO_instance%INPUT_FILE)//trim(auxString)//".mol",status='replace',action='write')
			write (10,"(A)") "This functionallity  is not supported with dummy atoms"
			close(10)

		else

			localizationOfCenters=ParticleManager_getCartesianMatrixOfCentersOfOptimization()
			auxMatrix=localizationOfCenters
			allocate( labels( size(auxMatrix%values,dim=1) ) )
			allocate( charges( size(auxMatrix%values,dim=1) ) )
			labels=ParticleManager_getLabelsOfCentersOfOptimization()
			charges=ParticleManager_getChargesOfCentersOfOptimization()
	
			do l=1,ParticleManager_getNumberOfQuantumSpecies()
				
				auxString=ParticleManager_getNameOfSpecie( l )
				open(10,file=trim(APMO_instance%INPUT_FILE)//trim(auxString)//".mol",status='replace',action='write')
					write(10,"(A)") "[Atoms] Angs"
					auxMatrix%values=0.0
					j=0
					do i=1,size(ParticleManager_instance%particlesPtr)
	
						if (	trim(ParticleManager_instance%particlesPtr(i)%symbol) == trim(auxString) ) then
							j=j+1
							origin = ParticleManager_getOrigin( iterator = i ) 
							auxMatrix%values(j,:)=origin
							symbol=trim( ParticleManager_instance%particlesPtr(i)%nickname )
							if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
							if(scan(symbol,"[") /=0) symbol=symbol(scan(symbol,"[")+1:scan(symbol,"]")-1)
	
							if ( APMO_instance%UNITS=="ANGSTROMS") origin = origin * AMSTRONG
							
							write (10,"(A,I,I,<3>F15.8)") trim(symbol), j,&
								int(abs(ParticleManager_instance%particlesPtr(i)%totalCharge)), origin(1), origin(2), origin(3)
	
						end if
	
					end do
					
					m=j
					do k=1,size(localizationOfCenters%values,dim=1)
					
						wasPress=.false.
						do i=1,j
							if( 	abs( auxMatrix%values(i,1) - localizationOfCenters%values(k,1)) < 1.0D-9 .and. &
								abs( auxMatrix%values(i,2) - localizationOfCenters%values(k,2)) < 1.0D-9 .and. &
								abs( auxMatrix%values(i,3) - localizationOfCenters%values(k,3)) < 1.0D-9  ) then
								wasPress=.true.
							end if
						end do
	
						if( .not.wasPress) then
							m=m+1
							origin=localizationOfCenters%values(k,:)
							if ( APMO_instance%UNITS=="ANGSTROMS") origin = origin * AMSTRONG
							symbol=labels(k)
							if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
							write (10,"(A,I,I,<3>F15.8)") trim(symbol), m,int(abs(charges(k))), origin(1), origin(2), origin(3)
						end if
	
					end do
	
	
					write(10,"(A)") "[GTO]"
					j=0
					do i=1,size(ParticleManager_instance%particlesPtr)
	
						if (	trim(ParticleManager_instance%particlesPtr(i)%symbol) == trim(auxString) ) then
							j=j+1
							
							write(10,"(I3,I2)") j,0
							call BasisSet_showInSimpleForm( ParticleManager_instance%particlesPtr(i)%basis,&
								trim(ParticleManager_instance%particlesPtr(i)%nickname),10 )
							write(10,*) ""
						end if
	
					end do
	
					write(10,"(A)") "[MO]"
	
	
					specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(auxString)) )
					occupationTotal=ParticleManager_getOcupationNumber( specieID )
					occupation=1.0/ParticleManager_getParticlesFraction( specieID )
					do j=1,size(MolecularSystem_instance%energyOfmolecularOrbitalPtr(specieID)%values)
						write (10,"(A5,ES15.5)") "Ene= ",MolecularSystem_instance%energyOfmolecularOrbitalPtr(specieID)%values(j)
						write (10,"(A11)") "Spin= Alpha"
						
						if ( j <= occupationTotal) then
							write (10,"(A,F6.4)") "Occup= ",occupation
						else
							write (10,"(A)") "Occup=0.0000"
						end if
						do k=1,size(MolecularSystem_instance%coefficientsOfCombinationPtr(specieID)%values,dim=1)
							write(10,"(I4,F15.6)") k,MolecularSystem_instance%coefficientsOfCombinationPtr(specieID)%values(k,j)
						end do
	
					end do
			
	
	
				close(10)
			end do
			
			call Matrix_destructor( localizationOfCenters )
			call Matrix_destructor( auxMatrix )
			deallocate(labels)

		end if

	end subroutine FormatsConverter_writeMoldenFile
	
	
	subroutine FormatsConverter_writeAIMfile( MolecularSystemInstance )
		type(MolecularSystem) :: MolecularSystemInstance
		integer :: unid
		type(Matrix) :: localizationOfCenters
		
		real(8), allocatable :: charges(:)
		real(8), allocatable :: exponents(:)
		real(8), allocatable :: contractionCoefficients(:)
		real(8), allocatable :: auxVector(:)
		real(8) :: origin(3)
		character(10),allocatable :: labels(:)
		character(10) :: symbol
		character(9), allocatable :: angularMoment(:)
		integer, allocatable :: centreAssignments(:)
		integer, allocatable :: typeAssignments(:)
		integer, allocatable :: contractionsSize(:)
		integer :: numberOfContractions
		integer :: numberOfPrimitives
		integer :: numberOfElectronicCenters
		integer :: specieID
		integer :: occupationTotal
		integer :: i,j,k,l,m,n
		
		unid=98

		open(UNIT=unid,FILE=trim(APMO_instance%INPUT_FILE)//"wfn",STATUS="replace",action='write')
		write(unid,"(A)")  trim(MolecularSysteminstance%name)
		
		localizationOfCenters=ParticleManager_getCartesianMatrixOfCentersOfOptimization()
		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie="e-" ) )
		occupationTotal=ParticleManager_getOcupationNumber( specieID )


		numberOfContractions=0
		numberOfPrimitives= 0
		numberOfElectronicCenters=0
		do i=1,size(ParticleManager_instance%particlesPtr )
			if (	trim(ParticleManager_instance%particlesPtr(i)%symbol) == "e-" ) then
				
				numberOfContractions = numberOfContractions + &
					BasisSet_getNumberOfContractions(ParticleManager_instance%particlesPtr(i)%basis)
				numberOfPrimitives = numberOfPrimitives + &
					BasisSet_getNumberOfPrimitives(ParticleManager_instance%particlesPtr(i)%basis)
				numberOfElectronicCenters=numberOfElectronicCenters+1
			end if
		end do
		
		
		write(unid,100) "GAUSSIAN", occupationTotal ,"MOL ORBITALS", &
			numberOfPrimitives,"PRIMITIVES", &
			size(localizationOfCenters%values,dim=1),"NUCLEI"

		allocate( centreAssignments(numberOfPrimitives) )
		allocate( typeAssignments(numberOfPrimitives) )
		allocate( exponents(numberOfPrimitives) )
		allocate( contractionCoefficients(numberOfPrimitives) )
		allocate( contractionsSize(numberOfContractions) )
		allocate( auxVector(numberOfPrimitives) )
		allocate( labels( size(localizationOfCenters%values,dim=1) ) )
		allocate( charges( size(localizationOfCenters%values,dim=1) ) )
			
		labels=ParticleManager_getLabelsOfCentersOfOptimization()	
		charges=ParticleManager_getChargesOfCentersOfOptimization()

		do i=1,size(localizationOfCenters%values,dim=1)
	
			symbol=labels(i)
			if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
				
			write(unid,101) trim(symbol)//"    ",i,"(CENTRE",i,")",localizationOfCenters%values(i,1:3),"CHARGE =",abs(charges(i))
			
		end do

		l=0
		m=0
		do i=1,size(ParticleManager_instance%particlesPtr )
			if (	trim(ParticleManager_instance%particlesPtr(i)%symbol) == "e-" ) then
				
				do j=1,size( ParticleManager_instance%particlesPtr(i)%basis%contractions )

					m=m+1
					angularMoment= ContractedGaussian_getShellCode( &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j))

					do n=1, size(angularmoment)
						contractionsSize(m)=size( ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%primitives )

						do k=1,size( ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%primitives )
							l=l+1
							centreAssignments(l)= &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%primitives(k)%owner &
								- numberOfElectronicCenters

							select case (trim(angularMoment(n)))
								case("S")
									typeAssignments(l)= 1

								case("Px")
									typeAssignments(l)= 2

								case("Py")
									typeAssignments(l)= 3

								case("Pz")
									typeAssignments(l)= 4

								case("Dxx")
									typeAssignments(l)= 5

								case("Dyy")
									typeAssignments(l)= 6

								case("Dzz")
									typeAssignments(l)= 7

								case("Dxy")
									typeAssignments(l)= 8

								case("Dxz")
									typeAssignments(l)= 9

								case("Dyz")
									typeAssignments(l)= 10
							end select

							exponents(l)= &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%primitives(k)%orbitalExponent
							contractionCoefficients(l)= &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%primitives(k)%normalizationConstant(n) * &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%contractionCoefficients(k) * &
								ParticleManager_instance%particlesPtr(i)%basis%contractions(j)%normalizationConstant(n)
						end do
					end do
				end do
			end if
		end do

		!!******************************************************************************
		!! Escribe los centros asociados a las funciones base electronicas
		!!
		do i=1,numberOfPrimitives/20
			write(unid,102) "CENTRE ASSIGNMENTS  ",(centreAssignments(j),j=(20*i-19),i*20)
		end do
		
		if ( mod(numberOfPrimitives,20)>0) then
			write(unid,102) &
			"CENTRE ASSIGNMENTS  ",(centreAssignments(j),j=(20*i-19),numberOfPrimitives)
		end if
		!!
		!!******************************************************************************

		!!******************************************************************************
		!! Escribe los indices asociados a los indices de momento angular
		!!
		do i=1,numberOfPrimitives/20
			write(unid,102) "TYPE ASSIGNMENTS    ",(typeAssignments(j),j=(20*i-19),i*20)
		end do
		
		if ( mod(numberOfPrimitives,20)>0) then
			write(unid,102) &
			"TYPE ASSIGNMENTS    ",(typeAssignments(j),j=(20*i-19),numberOfPrimitives)
		end if
		!!
		!!******************************************************************************

		!!******************************************************************************
		!! Escribe los exponentes de la funciones gausianas
		!!
		do i=1,numberOfPrimitives/5
			write(unid,103) "EXPONENTS ",(exponents(j),j=(5*i-4),i*5)
		end do

		if ( mod(numberOfPrimitives,5)>0) then
			write(unid,103) &
			"EXPONENTS ",(typeAssignments(j),j=(5*i-4),numberOfPrimitives)
		end if
		!!
		!!******************************************************************************

		!!******************************************************************************
		!! Escribe los orbitales moleculares ocupados
		!!
		do i=1,occupationTotal
			write(unid,104) "MO",i,"OCC NO =   2.00000000 ORB. ENERGY = ",&
				MolecularSysteminstance%energyOfmolecularOrbitalPtr(specieID)%values(i)

			k=1
			l=0
			do j=1,numberOfContractions
				l=l+contractionsSize(j)
				auxVector(k:l)=contractionCoefficients(k:l)*MolecularSysteminstance%coefficientsOfCombinationPtr(specieID)%values(j,i)
				k=k+contractionsSize(j)
			end do
			
			do j=1,numberOfPrimitives/5
				write(unid,105) (auxVector(k),k=(5*j-4),j*5)
			end do
			
			if ( mod(numberOfPrimitives,5)>0) then
				write(unid,105) (auxVector(k),k=(5*j-4),numberOfPrimitives)
			end if
			
		end do
		!!
		!!******************************************************************************
		write(unid,"(A8)") "END DATA"
		write(unid,106) "RHF      ENERGY =      ",MolecularSysteminstance%totalEnergy,"VIRIAL(-V/T)  = ", &
			-( MolecularSysteminstance%potentialEnergy / MolecularSysteminstance%kineticEnergy)

	close(unid)

          deallocate( centreAssignments )
	  deallocate( typeAssignments )
	  deallocate( exponents )
	  deallocate( contractionCoefficients )
	  deallocate( contractionsSize )
	  deallocate( auxVector )
	  deallocate( labels )
	  deallocate( charges )


	100   FORMAT (A8,11X,I3,1X,A12,4X,I3,1X,A10,6X,I3,1X,A6)
	101   FORMAT (2X,A3,I3,4X,A7,I3,A1,1X,3F12.8,2X,A8,F5.1)
	102   FORMAT (A18,2X,20I3)
	103   FORMAT (A10,5D14.7)
	104   FORMAT (A2,I3,21X,A36,F12.8)
	105   FORMAT (5D16.8)
	106   FORMAT (A18,F19.10,3X,A16,F12.8)
	
	end subroutine FormatsConverter_writeAIMfile


end module FormatsConverter_
