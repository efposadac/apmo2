!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module ExternalSoftware_
     use Supramolecular_
     use Exception_
     use String_
     use InputParser_
     implicit none


     !< enum format CartesianCoordinate {
          integer, parameter, public :: FRAGMENT_WITH_FRAGMENT_BASIS = B'000'
          integer, parameter, public :: FRAGMENT_WITH_COMPLEX_BASIS   = B'001'
     !< }

     type, private :: GamessInterface
	  character(10)  :: theory
	  character(255) :: nuclearBasisSet
	  character(255) :: externalNuclearBasisSet
	  character(255) :: electronicBasisSet
	  integer :: charge
	  integer :: multiplicity
	  integer :: mp2Correction
	  integer :: numberOfQuantumSpecies
	  integer :: isotopeMass
          real(8) :: totalEnergy
     end type

     type, private :: ApmoInterface
	  character(10) :: theory
	  character(255) :: nuclearBasisSet
	  character(255) :: electronicBasisSet
	  integer :: mp2Correction
          real(8) :: totalEnergy
     end type
    
     type, private :: LowdinInterface
	  character(255) :: electronicBasisSet
	  character(10) :: classic
	  character(10) :: exchange
	  character(10) :: correlation
	  character(10) :: guessType
	  integer :: printLevel
	  integer :: nbo
	  integer :: charge
	  integer :: multiplicity
	  integer :: polarOrder
	  real(8) :: Damping1
          real(8) :: Damping2
          real(8) :: totalEnergy
     end type


     type, public :: ExternalSoftware
          character(20) :: name
          type(Supramolecular) :: supramolecule
          type(GamessInterface) :: externalGamess
          type(ApmoInterface) :: externalApmo
	  type(LowdinInterface) :: externalLowdin
          character(100) :: nameOfInputFile
          character(100) :: nameOfOutputFile
          character(255) :: stringToFind
          character(255) :: commandString
          logical :: isInstanced
     end type

     type(ExternalSoftware), public :: external_instance


     public :: &
          ExternalSoftware_constructor, &
          ExternalSoftware_destructor, &
          ExternalSoftware_getEnergy, &
          ExternalSoftware_makeBSSEwithCP, &
	  ExternalSoftware_findString


     private
contains

     subroutine ExternalSoftware_constructor(this )
          implicit none
          type(ExternalSoftware) :: this

          integer :: i

! 	  print*, " ExternalSoftware_constructor"

          call Supramolecular_constructor( this.supramolecule )

          select case ( String_getLowercase( trim( APMO_instance.EXTERNAL_SOFTWARE_NAME )) )


               case("gamess")
                    this.name="gamess"
                    call ExternalSoftware_initializeGamess( this )

               case("apmo")
                    this.name="apmo"
                    call ExternalSoftware_initializeApmo( this )

	       case("lowdin")
		    call ExternalSoftware_initializeLowdin( this )
		    this.name="lowdin"
          end select

          this.isInstanced = .true.


 
     end subroutine ExternalSoftware_constructor

     subroutine ExternalSoftware_destructor(this)
          implicit none
          type(ExternalSoftware) :: this

! 	  print*, " ExternalSoftware_destructor" 

          call Supramolecular_destructor( this.supramolecule )
          this.isInstanced = .false.


     end subroutine ExternalSoftware_destructor



     function ExternalSoftware_getEnergy( this, withCounterPoiseCorrection, iterador ) result( output )
          implicit none
          type(ExternalSoftware) :: this
          logical, intent(in), optional :: withCounterPoiseCorrection
	  integer, optional :: iterador
          real(8) :: output

          real(8) :: auxVal
          integer :: status
	  integer :: iterator
          integer :: i
	  
! 	  print*, " ExternalSoftware_getEnergy" 
	  
	  iterator = 100
	  if(present(iterador)) iterator = iterador
	  
          if ( trim(APMO_instance.EXTERNAL_COMMAND) /= "" ) then

               output=0.0_8
               call Supramolecular_setCoordinates(this.supramolecule)

               if ( withCounterPoiseCorrection ) then
                    write (6,"(A)") ""
                    write (6,"(A)") "----------------------------------"
                    write (6,"(A)") "      COUNTERPOISE CORRECTION     "
                    write (6,"(A)") "----------------------------------"
                    write (6,"(A)") ""
                    write (6,"(T5A)") "* All geometries are of the complex"
                    write (6,"(A)") ""
               end if


               call ExternalSoftware_writeInput( this, 0, FRAGMENT_WITH_FRAGMENT_BASIS, iterator )
	       
	       if(trim( APMO_instance.EXTERNAL_SOFTWARE_NAME ) /= "lowdin" .or. iterator == 100) then
			
			status= system( trim(this.commandString) )
			auxVal=ExternalSoftware_findString( this, trim(this.nameOfOutputFile) )
	                write (6,"(T10,A,ES20.10)") "Energy for supramolecule                  :",auxVal
			
			status = system("rm -rf "//trim(this.nameOfInputFile))
			
			output=auxVal
			this.supramolecule.complexEnergy = output

			if ( withCounterPoiseCorrection ) then

			      do i=1, this.supramolecule.numberOfFragments

				  call ExternalSoftware_writeInput( this, i, FRAGMENT_WITH_FRAGMENT_BASIS )
				  status= system( trim(this.commandString) )
				  auxVal=ExternalSoftware_findString( this, trim(this.nameOfOutputFile) )
				  write (6,"(T10,A,I2,A,ES20.10)") "Energy for fragment ",i," with own basis     :",auxVal
				  output=output + auxVal

				  call ExternalSoftware_writeInput( this, i, FRAGMENT_WITH_COMPLEX_BASIS )
				  status= system( trim(this.commandString) )
				  auxVal=ExternalSoftware_findString( this, trim(this.nameOfOutputFile) )
				  write (6,"(T10,A,I2,A,ES20.10)") "Energy for fragment ",i," with complex basis :",auxVal
				  output=output - auxVal

			      end do

			end if

			print "(A1$)","."
		end if

          else

               call ExternalSoftware_exception(ERROR, &
                    "The command for external software has not been defined",&
                    "Class object ExternalSoftware in getEnergy() function" )

          end if


     end function ExternalSoftware_getEnergy

     subroutine ExternalSoftware_writeInput(this, fragmentNumber, flags, iterator)
          implicit none
          type(ExternalSoftware) :: this
          integer, optional :: fragmentNumber
          integer, optional :: flags
	  integer, optional :: iterator

! 	  print*, " ExternalSoftware_writeInput" 
	  
          select case ( trim(this.name ) )

               case("gamess")

                    call ExternalSoftware_writeGamesInput( this, fragmentNumber, flags )
               case("apmo")

                    call ExternalSoftware_writeApmoInput( this, fragmentNumber, flags )	
	       case("lowdin")

		    call ExternalSoftware_writeLowdinInput( this, fragmentNumber, flags, iterator )
          end select

          if ( trim(this.externalGamess.nuclearBasisSet)=="READIN" .and. trim(this.name)=="gamess" ) then
             this.commandString=trim(APMO_instance.EXTERNAL_COMMAND)//" "//trim(this.nameOfInputFile)//" NONE "// &
                  trim(this.externalGamess.externalNuclearBasisSet)
          else
             this.commandString= trim(APMO_instance.EXTERNAL_COMMAND)//" "//trim(this.nameOfInputFile)
          end if
          



     end subroutine ExternalSoftware_writeInput



     subroutine ExternalSoftware_writeGamesInput( this, fragmentNumber, flags )
          implicit none
          type(ExternalSoftware) :: this
          integer :: fragmentNumber
          integer :: flags

          integer :: i
          integer :: j

          if ( this.externalGamess.numberOfQuantumSpecies > 2) &
               call ExternalSoftware_exception( ERROR, &
                    "Mixtures of quantum nucleous are not accepted in gamess-neo", &
                    "Class object ExternalSoftware in writeGamessInput() function" )

          this.nameOfInputFile = trim(APMO_instance.INPUT_FILE)//&
                       trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//".ext.inp"

          this.nameOfOutputFile=trim(APMO_instance.INPUT_FILE)//&
                       trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//".ext.log"

          open( UNIT=34,FILE=trim(this.nameOfInputFile),STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='FORMATTED' )

               write (34,"(A,I1.1,A,I2.1,A/,A/,A/,A)") &
                    " $CONTRL SCFTYP="//trim(this.externalGamess.theory)// &
                    " RUNTYP=energy MULT=",this.externalGamess.multiplicity,&
                    " ICHARG=",this.externalGamess.charge," $END", &
                    " $CONTRL UNITS=BOHR $END", &
                    " $BASIS "//trim(this.externalGamess.electronicBasisSet)//" $END", &
                    " $SYSTEM  MWORDS=200 TIMLIM=20000000 $END"
               if ( this.externalGamess.mp2Correction == 2 ) write (34,"(A/,A)") &
                    " $CONTRL MPLEVL=2 $END", &
                    " $MP2 MP2PRP=.T. NACORE=0 CUTOFF=1.0E-12 $END"
               if ( this.externalGamess.numberOfQuantumSpecies > 1 ) write (34,"(A/,A/,A,A/,A)") &
                    " $SCF SOSCF=.F. ETHRSH=1.5 DIIS=.T. $END", &
                    " $INTGRL SCHWRZ=.F. $END", &
                    " $NEO  NEOSCF=RONHF"," BASNUC="//trim(this.externalGamess.nuclearBasisSet)//&
                    " NUNIQN="//trim(String_convertIntegerToString( &
                         Supramolecular_getNumberOfQuantumNucleous(this.supramolecule, fragmentNumber )  ))//&
                    " IUNIQN(1)="//trim(Supramolecular_getQuantumNucleousLocalization(this.supramolecule, fragmentNumber )), &
                    "           IUNIQT(1)="//trim(Supramolecular_getNucleonString(this.supramolecule, fragmentNumber) )// &
                    " NUMULT="//trim(String_convertIntegerToString(Supramolecular_getMultiplicity(this.supramolecule, fragmentNumber)) )//&
                    " $END"
               if ( this.externalGamess.mp2Correction == 2 .and. this.externalGamess.numberOfQuantumSpecies > 1) write (34,"(A)") &
                     " $NEO NEMPLV=2  $END"
               write (34,"(A/,A/,A)") &
                    " $DATA", &
                    " ----- SP calculation from APMO --------", &
                    " C1"

               call Supramolecular_writeFragment(this.supramolecule, fragmentNumber, &
                    CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE, 34)

               if ( IAND(flags,FRAGMENT_WITH_COMPLEX_BASIS) == FRAGMENT_WITH_COMPLEX_BASIS &
                    .and. fragmentNumber /= 0  ) then

                    do i=1,this.supramolecule.numberOfFragments

                         if( i /= fragmentNumber) then
                              call Supramolecular_scaleChargeOfFragment( this.supramolecule, i, scaleFactor=-1.0_8 )
                              call Supramolecular_writeFragment( this.supramolecule, i, &
                                   CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE, 34)
                              call Supramolecular_scaleChargeOfFragment( this.supramolecule, i, scaleFactor=-1.0_8 )
                         end if

                    end do

               end if

               write (34,"(A/,A)") &
                    " $END", &
                    ""

          close(34)

 
     end subroutine ExternalSoftware_writeGamesInput


     subroutine ExternalSoftware_writeApmoInput( this, fragmentNumber, flags )
          implicit none
          type(ExternalSoftware) :: this
          integer :: fragmentNumber
          integer :: flags

          integer :: i
          integer :: j

          this.nameOfInputFile = trim(APMO_instance.INPUT_FILE)//&
                        trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//".ext.apmo"

          this.nameOfOutputFile=trim(APMO_instance.INPUT_FILE)//&
                        trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//".ext.out"

          open( UNIT=34,FILE=trim(this.nameOfInputFile),STATUS='REPLACE', ACCESS='SEQUENTIAL', FORM='FORMATTED' )
	
               write (34,"(A/,A/,A)") &
                    "SYSTEM_DESCRIPTION='' ", &
                    "",&
                    "GEOMETRY"

                    if ( IAND(flags,FRAGMENT_WITH_COMPLEX_BASIS) == FRAGMENT_WITH_COMPLEX_BASIS &
                         .and. fragmentNumber /= 0  ) then
                         do i=1,this.supramolecule.numberOfFragments
                              if( i /= fragmentNumber) then
                                   call Supramolecular_removeStringToSymbolsOfFragment(this.supramolecule, i, "_*" )
                                   call Supramolecular_addPrefixToSymbolsOfFragment(this.supramolecule, i, "e-(" )
                                   call Supramolecular_addSuffixToSymbolsOfFragment(this.supramolecule, i, &
                                        ")* "//trim(this.externalApmo.electronicBasisSet ) )
                                   call Supramolecular_writeFragment(this.supramolecule, i, &
                                        CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_IN_ANGS, 34)
                                   call Supramolecular_restoreSymbolsOfFragment(this.supramolecule, i)
                              end if
                         end do
                    end if

                    call Supramolecular_removeStringToSymbolsOfFragment(this.supramolecule, fragmentNumber, "_*" )
                    call Supramolecular_addPrefixToSymbolsOfFragment(this.supramolecule, fragmentNumber, "e-(" )
                    call Supramolecular_addSuffixToSymbolsOfFragment(this.supramolecule, fragmentNumber, &
                         ")  "//trim(this.externalApmo.electronicBasisSet ) )
                    call Supramolecular_writeFragment(this.supramolecule, fragmentNumber, &
                         CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_IN_ANGS, 34)
                    call Supramolecular_restoreSymbolsOfFragment(this.supramolecule, fragmentNumber)
                    call Supramolecular_writeFragment(this.supramolecule, fragmentNumber, &
                         CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_BASIS+CARTESIAN_COORDINATE_IN_ANGS, 34)

               write (34,"(A/,A/,A/,A)") &
                    "END GEOMETRY", &
                    "",&
                    "TASKS",&
                    "      method=RHF"

               if ( this.externalApmo.mp2Correction == 2 ) write (34,"(A)") &
                    "      mollerPlessetCorrection=2"
                write (34,"(A/,A/,A/,A,E15.5/,A,ES15.5/,A,ES15.5/,A,ES15.5/,A,ES15.5/,A,ES15.5/,A,ES15.5/,A,ES15.5)" ) &
                     "END TASKS",&
                     "",&
                     "CONTROL",&
                     "      integralThreshold =",APMO_instance.INTEGRAL_THRESHOLD,&
                     "      scfNonElectronicEnergyTolerance =",APMO_instance.SCF_NONELECTRONIC_ENERGY_TOLERANCE,&
                     "      scfElectronicEnergyTolerance =",APMO_instance.SCF_ELECTRONIC_ENERGY_TOLERANCE,&
                     "      nonElectronicDensityMatrixTolerance =",APMO_instance.NONELECTRONIC_DENSITY_MATRIX_TOLERANCE,&
                     "      electronicDensityMatrixTolerance =",APMO_instance.ELECTRONIC_DENSITY_MATRIX_TOLERANCE,&
                     "      totalEnergyTolerance =",APMO_instance.TOTAL_ENERGY_TOLERANCE,&
                     "      diisSwitchThreshold =",APMO_instance.DIIS_SWITCH_THRESHOLD,&
                     "      densityFactorThreshold =",APMO_instance.DENSITY_FACTOR_THRESHOLD
               write (34,"(A,I10/,A,I10/,A,I10/,A,I10/,A,I2/,A/,A/,A)" ) &
                     "      scfNonelectronicMaxiterations =",APMO_instance.SCF_NONELECTRONIC_MAX_ITERATIONS,&
                     "      scfElectronicMaxiterations =",APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS,&
                     "      scfMaxiterations =",APMO_instance.SCF_MAX_ITERATIONS,&
                     "      scfInterspeciesMaximumIterations =",APMO_instance.SCF_INTERSPECIES_MAXIMUM_ITERATIONS,&
                     "      convergenceMethod =",APMO_instance.CONVERGENCE_METHOD,&
                     "      readCoefficients=F",&
                     "      transformToCenterOfMass=F",&
                     "END CONTROL"

          close(34)

 
     end subroutine ExternalSoftware_writeApmoInput

     subroutine ExternalSoftware_writeLowdinInput( this, fragmentNumber, flags, iterator )
          type(ExternalSoftware) :: this
          integer :: fragmentNumber
          integer :: flags
	  integer :: iterator

	  integer :: numberOfQuantumSystems
          integer :: i
          integer :: j

! 	  print*, " ExternalSoftware_writeLowdinInput"
	  
          this.nameOfInputFile = trim(APMO_instance.INPUT_FILE)//&
			trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//&
			trim(adjustl(trim(String_convertIntegerToString(iterator))))//".ext.inp"

          this.nameOfOutputFile=trim(APMO_instance.INPUT_FILE)//&
			trim(adjustl(trim(String_convertIntegerToString(fragmentNumber))))//&
		        trim(adjustl(trim(String_convertIntegerToString(iterator))))//".ext.out"
	  
          open( UNIT=34,FILE=trim(this.nameOfInputFile),STATUS='UNKNOWN', ACCESS='SEQUENTIAL', FORM='FORMATTED' )
	  
	  
 	  write(34, "(A/, I1/, A/)") &
 		"Print {", &
                this.externalLowdin.printLevel,&
                "}"
 
 	  write(34, "(A/, A/, 1F/, I1/, I1, I3/, I1, I3/, A/)")&
		"ept{",&
                "dkt",&
                100.0,&
                1,&
                1, 5,&
                1, 5,&
                "}"
 
 	  write(34, "(A)")&
 		"&Geometry"
                     call Supramolecular_removeStringToSymbolsOfFragment(this.supramolecule,fragmentNumber, "_*" )
                     call Supramolecular_writeFragment(this.supramolecule, fragmentNumber, &
                          CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_IN_ANGS, 34)
 
 	  write(34, "(A/)")&
 		"/"
 
          numberOfQuantumSystems = externalLowdin(1).numberOfExternalLowdin
          
          write(34, "(A)")&
               "&NBO"
          
          if(trim(externalLowdin(1).basisSetName) /= "dirac") then
             do i = 1, numberOfQuantumSystems
                write(34, "(A3,1F,I3,I3,A10)")&
                     trim(externalLowdin(i).atom),&
                     externalLowdin(i).isotopicMass,&
                     externalLowdin(i).initialAtom,&
                     externalLowdin(i).finalAtom,&
                     trim(externalLowdin(i).basisSetName)
             end do
          end if

          write(34, "(A/)")&
               "/"
          
          write(34, "(A/, F/, A/)")&
               "&Damping",&
               this.externalLowdin.Damping1,&
               "/" 
          write(34, "(A/, A, F/, A, I3/, A/)")&
               "&Control",&
               "damping = ", this.externalLowdin.Damping2,&
               "nprint = ", this.externalLowdin.printLevel,&
               "/"
          write(34, "(A/, A, I3/, A, I3/, A/)")&
               "&System_info",&
               "charge = ", this.externalLowdin.charge, &
               "multiplicity = ", this.externalLowdin.multiplicity, &
               "/"
          write(34, "(A/, A, A/, A, A/, A/)")&
               "&Basis", &
               "egbasis = ", trim(this.externalLowdin.electronicBasisSet), &
               "ngbasis = ", trim(externalLowdin(1).basisSetName), &
               "/"
          write(34, "(A/, A, A/, A, A/, A, A/, A/)")&
               "&Interactions", &
               "classic = ", trim(this.externalLowdin.classic), &
               "exchange = ", trim(this.externalLowdin.exchange), &
               "correlation = ", trim(this.externalLowdin.correlation), &
               "/"
          write(34, "(A/, A, A/, A/)")&
               "&Guess", &
               "guesstype = ",  trim(this.externalLowdin.guesstype), &
               "/"
          write(34, "(A/, A, I3/, A/)")&
               "&Polar", &
               "order = ", this.externalLowdin.polarOrder, &
               "/"
          i = system("cp  "//trim(this.nameOfInputFile// "  /home/edwin/"))
          
 
     end subroutine ExternalSoftware_writeLowdinInput

     function ExternalSoftware_findString(this, fileName ) result( output )
          implicit none
          type(ExternalSoftware) :: this
          character(*) :: fileName
          real(8) :: output

          integer :: ssize
          character(100) :: stringToFind
          character(255) :: line
	  
! 	  print*, " ExternalSoftware_findString" 

          stringToFind= trim(adjustl(this.stringToFind))
          open( UNIT=34,FILE=trim(adjustl(fileName)),STATUS='unknown',ACCESS='sequential' )

          if (.not.eof(34) ) then

               read(34,'(A)') line
               line=trim( adjustl( trim(line) ) )

          else

               call ExternalSoftware_exception( ERROR,"The output file is empty",&
                    "Class object ExternalSoftware in findString() function")

          end if

          ssize=len_trim( stringToFind )
          do while( line(1:ssize) /= stringToFind(1:ssize) .and. .not.eof(34) )
               read(34,'(A)') line
               line = trim( adjustl(trim(line)) )
          end do

          if ( line(1:ssize) == stringToFind(1:ssize) )  &
               output = dnum(line(scan(trim(line),"=" )+1:len_trim(line)))

          close(34)


     end function ExternalSoftware_findString



     subroutine ExternalSoftware_initializeGamess( this )
          type(ExternalSoftware) :: this

          integer :: existFile
          character(255) :: line
          integer :: i

	  do i=0, this.supramolecule.numberOfFragments 
	       call Supramolecular_removeStringToSymbolsOfFragment(this.supramolecule, i, "_*" )
	  end do

          this.externalGamess.theory = trim( APMO_instance.METHOD(1:3) )
          this.stringToFind="TOTAL ENERGY ="
          this.externalGamess.charge = 0
          this.externalGamess.multiplicity=1
          this.externalGamess.mp2Correction = APMO_instance.MOLLER_PLESSET_CORRECTION
          if( this.externalGamess.mp2Correction == 2 ) this.stringToFind="E(MP2)="
          this.externalGamess.numberOfQuantumSpecies = ParticleManager_instance.numberOfQuantumSpecies
          this.externalGamess.isotopeMass = Supramolecular_getNumberOfQuantumNucleones(this.supramolecule, 0 )
          this.externalGamess.totalEnergy = 0.0_8

          inquire(FILE = trim(APMO_instance.INPUT_FILE)//"gms.bs", EXIST = existFile )



          if ( existFile ) then
               this.externalGamess.externalNuclearBasisSet="NONE"
	       open(UNIT=34,FILE=trim(APMO_instance.INPUT_FILE)//"gms.bs",STATUS='unknown',ACCESS='sequential')
	       if ( .not.eof(34) ) then
		    read(34,'(A)') line
		    this.externalGamess.electronicBasisSet=trim(adjustl(trim(line)))
               else

                    call ExternalSoftware_exception(ERROR,&
                         "Electronic basis set was not found", &
                         "Class object ExternalSoftware in initializeGamess() function" )

	       end if


	       if ( .not.eof(34) ) then
		    read(34,'(A)') line
		    this.externalGamess.nuclearBasisSet=trim(adjustl(trim(line)))
		    if ( trim( this.externalGamess.nuclearBasisSet ) =="READIN" ) then

		        if ( .not.eof(34) ) then
                              read(34,'(A)') line
		             this.externalGamess.externalNuclearBasisSet=trim(adjustl(trim(line)))
			end if
		    end if 
	       end if

          close(34)
          else
               call ExternalSoftware_exception(ERROR, &
                    "You should to define a "//trim(APMO_instance.INPUT_FILE)//"gms.bs file", &
                    "Class object GamessInterface in constructor() function")
          end if


     end subroutine ExternalSoftware_initializeGamess


     subroutine ExternalSoftware_initializeApmo( this )
          type(ExternalSoftware) :: this

! 	  print*, " ExternalSoftware_initializeApmo"
	  
	  this.externalApmo.theory = trim( APMO_instance.METHOD(1:3) )
	  this.externalApmo.nuclearBasisSet= " "
	  this.externalApmo.electronicBasisSet=  ParticleManager_getBaseNameForSpecie( 'e-' )
          this.stringToFind="TOTAL ENERGY ="
	  this.externalApmo.mp2Correction=APMO_instance.MOLLER_PLESSET_CORRECTION
          if( this.externalApmo.mp2Correction == 2 ) this.stringToFind="E(MP2)="
          this.externalApmo.totalEnergy = 0.0_8

 
     end subroutine ExternalSoftware_initializeApmo

     subroutine ExternalSoftware_initializeLowdin( this )
          type(ExternalSoftware) :: this

! 	  print*, " ExternalSoftware_initializeLowdin" 
	  
	  this.externallowdin.electronicBasisSet=  ParticleManager_getBaseNameForSpecie( 'e-' )
	  this.externallowdin.classic= "coulomb"
	  this.externallowdin.exchange= "fock"
	  this.externallowdin.correlation= "none"
	  this.externallowdin.guessType= "core"
	  this.externallowdin.printLevel= 0
	  this.externallowdin.nbo= ParticleManager_instance.numberOfQuantumSpecies
	  this.externallowdin.charge= 1
	  this.externallowdin.multiplicity= 2
	  this.externallowdin.polarOrder= 0
          this.externallowdin.Damping1=0.1_8
	  this.externallowdin.Damping2= 0.3_8
          this.externallowdin.totalEnergy = 0.0_8
          this.stringToFind="TOTAL ENERGY= "


     end subroutine ExternalSoftware_initializeLowdin

     subroutine ExternalSoftware_makeBSSEwithCP( this)
          implicit none
          type( ExternalSoftware ) :: this

          real(8) :: output

          output = ExternalSoftware_getEnergy( this, withCounterPoiseCorrection=.true. )

          write(6,"(T10,A)") ""
          write(6,"(T10,A,ES20.10)") "BSSE  CORRECTION: ", output-this.supramolecule.complexEnergy
	  write(6,"(T10,A)") ""

 
     end subroutine ExternalSoftware_makeBSSEwithCP

     !>
     !! @brief  Maneja excepciones de la clase
     !<
     subroutine ExternalSoftware_exception(typeMessage, description, debugDescription)
          implicit none
          integer :: typeMessage
          character(*) :: description
          character(*) :: debugDescription

          type(Exception) :: ex

! 	  print*, " ExternalSoftware_exception" 
	  
          call Exception_constructor( ex , typeMessage )
          call Exception_setDebugDescription( ex, debugDescription )
          call Exception_setDescription( ex, description )
          call Exception_show( ex )
          call Exception_destructor( ex )


     end subroutine ExternalSoftware_exception

end module ExternalSoftware_
