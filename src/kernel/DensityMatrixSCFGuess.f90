!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************

!**
! Clase estatica que contiene los procedimientos para construir
! matrices de densidad iniciales relevantes en procesos tipo SCF
!**
module DensityMatrixSCFGuess_
	use ParticleManager_
	use Matrix_
	use Vector_
	use APMO_
	use RHFWaveFunction_
	use InputOutput_
	use String_
		
	implicit none
	
	public  &
		DensityMatrixSCFGuess_getGuess
		

	private

contains

	!**
	! Constructor por omision
	!**
	subroutine DensityMatrixSCFGuess_constructor()
		implicit none
		
	end subroutine DensityMatrixSCFGuess_constructor
	
	!**
	! Destructor
	!**
	subroutine DensityMatrixSCFGuess_destructor()
		implicit none
		
	end subroutine DensityMatrixSCFGuess_destructor
	

	function DensityMatrixSCFGuess_getGuess( densityType, nameOfSpecie ) result( output )
		implicit none
		character(*), intent(in) :: densityType
		character(*), intent(in), optional :: nameOfSpecie
		type(Matrix) :: output

		integer :: specieID
		integer(8) :: orderOfMatrix
		integer :: existFile
		
		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(nameOfSpecie) ) )
		orderOfMatrix = ParticleManager_getTotalnumberOfContractions( specieID )
			
		call Matrix_constructor(output, orderOfMatrix, orderOfMatrix, 0.0_8  ) 

		existFile=0
		if ( APMO_instance%READ_COEFFICIENTS ) then
			inquire(FILE = trim(APMO_instance%INPUT_FILE)//trim(nameOfSpecie)//".vec", EXIST = existFile )

		end if
			
		if ( existFile ) then

			call DensityMatrixSCFGuess_read( output, trim(nameOfSpecie) )
			

		else

			select case( trim( String_getUppercase( densityType ) ) )
			
				case( "ONES" )
					
					call DensityMatrixSCFGuess_ones( output, trim(nameOfSpecie) )
					
				case( "HCORE" )
					
					call DensityMatrixSCFGuess_hcore( output, trim(nameOfSpecie) )
					
				case( "HUCKEL" )
				
					call DensityMatrixSCFGuess_huckel( output, trim(nameOfSpecie) )
				
				case default
					
					call DensityMatrixSCFGuess_hcore( output, trim(nameOfSpecie) )
					
			end select
		end if

	end function DensityMatrixSCFGuess_getGuess

	!**
	! Realiza un calculo Huckel extendido utilizando la base
	! MINI de Huzinaga, proyectando esta sobre la base que se
	! este utilizando
	!
	! @todo Falta implementacion completa 
	!**
	subroutine DensityMatrixSCFGuess_huckel( densityMatrix, nameOfSpecie ) 
		implicit none
		type(Matrix), intent(inout) :: densityMatrix
		character(*), intent(in) :: nameOfSpecie
		
		
		integer :: specieID
		integer :: ocupationNumber
		integer(8) :: orderOfMatrix
		integer :: i
		
		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(nameOfSpecie) ) )
		ocupationNumber = ParticleManager_getOcupationNumber( specieID )
		orderOfMatrix = ParticleManager_getTotalnumberOfContractions( specieID )
		
		if ( .not.allocated(densityMatrix%values) ) then
			call Matrix_constructor(  densityMatrix , orderOfMatrix, orderOfMatrix, 0.0_8 )
		else
		 	densityMatrix%values=0.0_8
		end if
		 
		do i=1, ocupationNumber
		
			densityMatrix%values(i,i) =1.0_8
			
		end do
		
	end subroutine DensityMatrixSCFGuess_huckel
	
	!**
	! Diagonaliza el hamiltoniano monoelectronico para obtener los
	! orbitales iniciales de partida
	!
	! @todo Falta implementacion completa
	!**
	subroutine DensityMatrixSCFGuess_hcore( hcoreDensityMatrix, nameOfSpecie ) 
		implicit none
		type(Matrix), intent(inout) :: hcoreDensityMatrix
		character(*), intent(in) :: nameOfSpecie
		type(Matrix) :: auxGuess
		type(Matrix) :: auxTransformation
		type(Matrix) :: eigenVectors
		type(Vector) :: eigenValues
		
		
		integer :: specieID
		integer :: ocupationNumber
		integer(8) :: orderOfMatrix
		integer :: i
		integer :: j
		integer :: k
		
		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(nameOfSpecie) ) )
		ocupationNumber = ParticleManager_getOcupationNumber( specieID )
		orderOfMatrix = ParticleManager_getTotalnumberOfContractions( specieID )
		
		if ( .not.allocated(hcoreDensityMatrix%values) ) then
			call Matrix_constructor(  hcoreDensityMatrix , orderOfMatrix, orderOfMatrix, 0.0_8 )
		end if
		
		call Matrix_constructor(auxGuess, orderOfMatrix, orderOfMatrix )
		call Matrix_constructor(auxTransformation, orderOfMatrix, orderOfMatrix )
		call Matrix_constructor(eigenVectors, orderOfMatrix, orderOfMatrix )
		call Vector_constructor(eigenValues, int( orderOfMatrix ) )
		
		auxGuess=RHFWaveFunction_getIndependentParticleMatrix( trim(nameOfSpecie ) ) 
		auxTransformation = RHFWaveFunction_getTransformationMatrix( trim(nameOfSpecie ) )
		
		auxGuess%values = matmul( matmul( transpose(auxTransformation%values ) , auxGuess%values ) , auxTransformation%values )
	
		call Matrix_eigen( auxGuess, eigenValues, eigenVectors, SYMMETRIC )
	
		auxGuess%values = matmul( auxTransformation%values , eigenVectors%values)
		
		hcoreDensityMatrix%values = 0.0_8
		
		do i = 1 , orderOfMatrix 
			do j = 1 , orderOfMatrix
				do k = 1 , ocupationNumber
					hcoreDensityMatrix%values(i,j) = hcoreDensityMatrix%values( i,j ) + ( auxGuess%values(i,k) * auxGuess%values(j,k) )
				end do
			end do
		end do
	
		hcoreDensityMatrix%values =  ParticleManager_getEta( specieID ) * hcoreDensityMatrix%values
		
		call Matrix_destructor( auxGuess )
		call Matrix_destructor( auxTransformation )
		call Matrix_destructor( eigenVectors )
		call Vector_destructor( eigenValues )
				
	end subroutine DensityMatrixSCFGuess_hcore
	
		
	!**
	! Retorna una matriz con unos en la diagonal, hasta el n�mero
	! de ocupaci�n
	!**
	subroutine DensityMatrixSCFGuess_ones( densityMatrix, nameOfSpecie ) 
		implicit none
		type(Matrix), intent(inout) :: densityMatrix
		character(*), intent(in) :: nameOfSpecie
		
		
		integer :: specieID
		integer :: ocupationNumber
		integer(8) :: orderOfMatrix
		integer :: i
		
		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(nameOfSpecie) ) )
		ocupationNumber = ParticleManager_getOcupationNumber( specieID )
		orderOfMatrix = ParticleManager_getTotalnumberOfContractions( specieID )
		
		if ( .not.allocated(densityMatrix%values) ) then
			call Matrix_constructor(  densityMatrix , orderOfMatrix, orderOfMatrix, 0.0_8 )
		else
		 	densityMatrix%values=0.0_8
		end if
		 
		do i=1, ocupationNumber
		
			densityMatrix%values(i,i) =1.0_8
			
		end do
		
	end subroutine DensityMatrixSCFGuess_ones
	
	subroutine DensityMatrixSCFGuess_read( densityMatrix, nameOfSpecie ) 
		implicit none
		type(Matrix), intent(inout) :: densityMatrix
		character(*), intent(in) :: nameOfSpecie
		
		type(Matrix) :: vectors, auxVectors
		integer :: specieID
		integer :: orderOfMatrix
		integer(8) :: numberOfMatrixElements
		integer :: numberOfContractions
		integer :: st(1), pt(3), dt(6), ft(10), gt(15), ht(21)
		integer :: angularMoment
		integer :: ocupationNumber
		integer :: i, j, k, u, v
		integer, allocatable :: trans(:)
		
		type(Exception) :: ex

		specieID= int( ParticleManager_getSpecieID(  nameOfSpecie= trim(nameOfSpecie) ) )
		orderOfMatrix = ParticleManager_getTotalnumberOfContractions( specieID )
		numberOfContractions = ParticleManager_getNumberOfContractions( specieID )
		numberOfMatrixElements = int(orderOfMatrix, 8) ** 2_8

		ocupationNumber = ParticleManager_getOcupationNumber( specieID )

       	if (ParticleManager_isFrozen(trim(nameOfSpecie))) then
			vectors = InputOutput_getMatrix(orderOfMatrix, orderOfMatrix, &
            file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".dens", &
                        binary = .true.)
        else
                   vectors = InputOutput_getMatrix(orderOfMatrix, orderOfMatrix, &
			file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecie)//".vec", &
			binary = .true.)
                end if


		!!! transformacion de indices para 
		if (ParticleManager_isFrozen(trim(nameOfSpecie))) then

			!!Tamano del arreglo de nuevas etiquetas
			if(allocated(trans)) deallocate(trans)
			allocate(trans(orderOfMatrix))

			!! Reglas de transformacion de indices
			st(1)    = 1
			pt(1:3)  = [1, 2, 3]
			dt(1:6)  = [1, 4, 5, 2, 6, 3]
			ft(1:10) = [1, 4, 5, 6, 10, 8, 2, 7, 9, 3]
			gt(1:15) = [1, 4, 5, 10, 13, 11, 6, 14, 15, 8, 2, 7, 12, 9, 3]
                        ht(1:21) = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
			write(*,*) ""
			write(*,"(A)",advance="no") "READING EXTERNAL COEFFICIENTS..."

			trans = 0
			u = 1
			v = 0

			do i = 1, numberOfContractions
				angularMoment = ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
								 specieID)%contractionID(i)%particleID)%basis%contractions( &
								 ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
								 i)%contractionIDInParticle)%angularMoment

				select case(angularMoment)
					case(0)
						trans(u) = st(1) + v
						u = u + 1

					case(1)
						do j = 1, 3
							trans(u) = pt(j) + v
							u = u + 1
						end do

					case(2)
						do j = 1, 6
							trans(u) = dt(j) + v
							u = u + 1
						end do

					case(3)
						do j = 1, 10
							trans(u) = ft(j) + v
							u = u + 1
						end do

					case(4)
						do j = 1, 15
							trans(u) = gt(j) + v
							u = u + 1
						end do
                                        case(5)
                                                do j = 1, 21
                                                        trans(u) = ht(j) + v
                                                        u = u + 1
                                                end do
					case default
						call Exception_constructor( ex , WARNING )
						call Exception_setDebugDescription( ex, "Class object set Dens Matrix in the read function" )
						call Exception_setDescription( ex, "this angular moment is not implemented (l>4)" )
						call Exception_show( ex )

						return

				end select
				v = u - 1

			end do

			call Matrix_constructor( auxVectors, int(orderOfMatrix, 8), int(orderOfMatrix, 8) )

			do i=1,orderOfMatrix
				do j=1,orderOfMatrix
					auxVectors.values( i, j ) = vectors.values( trans(i), trans(j) )
				end do
			end do


			vectors.values = auxVectors.values

			call Matrix_destructor(auxVectors)

			!call InputOutput_writeMatrix( vectors, file="prueba.vec", binary =.true. )

            write(*,*) "OK"

		end if

        call Matrix_constructor( densityMatrix, int(orderOfMatrix,8), int(orderOfMatrix,8), 0.0_8 )

        if (ParticleManager_isFrozen(trim(nameOfSpecie))) then
           densityMatrix.values =  vectors.values
        else
           do i = 1 , orderOfMatrix
              do j = 1 , orderOfMatrix
                 do k = 1 , ocupationNumber
                    densityMatrix.values(i,j) = &
                         densityMatrix.values( i,j ) &
                         + ( vectors.values(i,k) * vectors.values(j,k) )
                 end do
              end do
           end do
           densityMatrix.values =  ParticleManager_getEta( specieID ) * densityMatrix.values
        end if
 
		call Matrix_destructor(vectors)

	end subroutine DensityMatrixSCFGuess_read
	
end module DensityMatrixSCFGuess_
