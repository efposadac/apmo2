!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module ExternalPotential_
	use Exception_
	use ContractedGaussian_
	use String_
	use Matrix_
	use Units_
	implicit none

	!>
	!! @brief Maneja potenciales externos
	!!
	!! @author Sergio A. Gonzalez
	!!
	!! <b> Creation data : </b> 06-08-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 06-08-10 </tt>:  Sergio A. Gonzalez ( sergmonic@gmail.com )
	!!        -# Creacioon del modulo y metodos basicos
	!!
	!<
	type, public :: ExternalPotential
		character(20) :: name
		character(50) :: interactionName
		character(50) :: interactiontype
		character(50) :: ttype
		character(50) :: coordinate
		character(50) :: units
		integer :: numOfComponents
		integer :: iter
		logical :: isInstanced
		type(ContractedGaussian) :: gaussianComponents
	end type

	
		type(ExternalPotential), pointer, private :: thisPtr
		real(8), allocatable, private :: exponents(:)
		real(8), allocatable, private :: coefficients(:)
		real(8), allocatable, private :: origin(:,:)
		character(30), private :: bblockName
		character(30), private :: currentBlockName




	public :: &
		ExternalPotential_constructor, &
		ExternalPotential_destructor, &
		ExternalPotential_show, &
		ExternalPotential_getInteractionMtx, &
		ExternalPotential_getPotential,&
		ExternalPotential_XMLParser_startElement, &
		ExternalPotential_XMLParser_endElement
		
private		
contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine ExternalPotential_constructor(this, interactionName, interactionType)
		implicit none
		
		type(ExternalPotential) :: this
		character(*) :: interactionName
		character(*) :: interactionType
		
		this%name=""
		this%interactionName = trim(interactionName)
		this%interactionType = trim(interactionType)
		this%ttype=""
		this%coordinate=""
		this%units="bohr"
		this%numOfComponents=0
		this%iter=1
        this%isInstanced=.false.
		
		call ExternalPotential_show(this)

	end subroutine ExternalPotential_constructor

	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine ExternalPotential_destructor(this)
		implicit none
		type(ExternalPotential) :: this
		
		this%isInstanced=.false.
		
	end subroutine ExternalPotential_destructor

	!>
	!! @brief 
	!!
	!! @param this
	!<
	subroutine ExternalPotential_show(this)
		implicit none
		type(ExternalPotential) :: this

		print*, "Interaction Type for ", trim(this%interactionName), ": ", trim(this%interactionType)

	end subroutine ExternalPotential_show


	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	function ExternalPotential_getPotential( this, coords )  result(output)
		implicit none
		type(ExternalPotential) :: this
		real(8) :: coords(3)
		real(8) :: output

		integer :: i

		output=0.0

		do i=1, this%gaussianComponents%length
			output = output+( this%gaussianComponents%contractionCoefficients(i)* &
			exp(-this%gaussianComponents%primitives(i)%orbitalExponent*( dot_product(coords,coords) ) ) )
		end do

	end function ExternalPotential_getPotential


!	function ExternalPotential_getInteractionMtx(this, contractions) result(output)
	subroutine ExternalPotential_getInteractionMtx( this, contractions )
		implicit none
		type(ExternalPotential) :: this
		type(ContractedGaussian) :: contractions(:)
		type(Matrix) :: output

		integer :: i, j, k, l, m, a, b
		integer :: numContractions
		real(8), allocatable :: auxVal(:)
		type(ContractedGaussian) :: auxContract

		do i = 1, size(contractions)
			numContractions = numContractions + contractions(i)%numCartesianOrbital
		end do

		call Matrix_constructor(output,int(numContractions,8),int(numContractions,8))

		a = 1
		b = 1

		do i=1, size(contractions)

			call ContractedGaussian_product(contractions(i), &
					this%gaussianComponents, auxContract)
			
			do j=1, size(contractions)

				call ContractedGaussian_overlapIntegral( auxContract, contractions(j), auxVal)

				m = 0
				do k = a, auxContract%numCartesianOrbital - 1
					do l = b, contractions(j)%numCartesianOrbital - 1
						m = m + 1
						output%values(k,l)= auxVal(m)
					end do
				end do
				b = b + contractions(j)%numCartesianOrbital
			end do
			a = a + auxContract%numCartesianOrbital
			call ContractedGaussian_destructor(auxContract)
		end do

        call Matrix_show(output)

        stop "ExternalPotential_getInteractionMtx"

!	end function ExternalPotential_getInteractionMtx
	end subroutine ExternalPotential_getInteractionMtx

	!>
	!! @brief Maneja eventos al encontrar el token de inicio
	!!
	!! @param this 
	!<
	subroutine ExternalPotential_XMLParser_startElement (this, tagName, dataName, dataValue, blockName)
		implicit none
		type(ExternalPotential), target, intent(inout) :: this
		character(*) :: tagName
		character(*) :: dataName
		character(*) :: dataValue
		character(*) :: blockName
		
		character(5) :: auxVal
		type(Exception) :: ex
		
		thisPtr=>null()
		thisPtr => this
		
		bblockName = trim(blockName)
		
		select case ( trim(tagName) ) 

			case ("Potential")
				
				select case ( trim(dataName) )
					
					case("name")
									
						thisPtr%name = trim(dataValue)
						
					case("type")
									
						thisPtr%ttype = trim(dataValue)
						
					case("coordinate")
									
						thisPtr%coordinate = trim(dataValue)
					
					case("units")
									
						thisPtr%units = trim(dataValue)
						
				end select

			case ("Interaction")
				
				select case ( trim(dataName) )
					
					case("name")
								
						currentBlockName = trim(dataValue)
						if ( bblockName == currentBlockName ) then
							thisPtr%interactionName = trim( dataValue )
          						thisPtr%iter=1
						end if
					
					case("size")
						
						if ( bblockName == currentBlockName ) then
							
						       if( allocated( exponents ) ) then
								 deallocate( exponents ) 
								 deallocate( coefficients )
								 deallocate( origin )
						       end if 

                                                       thisPtr%numOfComponents = JNUM( dataValue )
                                                       allocate( exponents( thisPtr%numOfComponents ) )
                                                       allocate( coefficients( thisPtr%numOfComponents ) )
						       allocate( origin( thisPtr%numOfComponents,3 ) )

						end if
							
				end select
				
			case ("GaussianComponent")
			
				if ( bblockName == currentBlockName ) then

				   if( thisPtr%iter <= thisPtr%numOfComponents ) then
					     select case ( trim(dataName) )
						  
						  case("exponent")
							    exponents( this%iter ) = DNUM(dataValue)
						  
						  case("factor")
							    coefficients( this%iter ) = DNUM(dataValue)

						  case("x")
							origin( this%iter,1 ) = DNUM(dataValue)
						  case("y")
							origin( this%iter,2 ) = DNUM(dataValue)
						  case("z")
							origin( this%iter,3 ) = DNUM(dataValue)
					     end select

				   else

					     auxVal= String_convertIntegerToString(this%iter+1)
					     call ExternalPotential_exception( ERROR, &
					          "   The number of components for the potential "//trim(auxVal)//"  of the "&
					           //trim(bblockName)//" is not correct, check the XML file for the potential "//trim(thisPtr%name), &
					          "Class object Basis in the XMLParser_startElement function" )

				   end if
                              end if

		end select
		
		
	end subroutine ExternalPotential_XMLParser_startElement

	!>
	!! @brief Maneja eventos al encontrar el token final
	!!
	!! @param this 
	!<
	subroutine ExternalPotential_XMLParser_endElement( tagName)
		implicit none
		character(*) :: tagName
		
		type(Exception) :: ex
		integer :: auxVal
		integer :: i
		
				
		if ( trim(bblockName) == trim(currentBlockName)  ) then

			thisPtr%isInstanced = .true.

			select case ( trim(tagName) )
											
				case ("GaussianComponent")
					thisPtr%iter=thisPtr%iter+1
					
				case ("Interaction")
				
					if(trim(thisPtr%units)=="angstrom") then
						
						exponents=exponents*(AMSTRONG**2)
						origin=origin/AMSTRONG
					end if

					call ContractedGaussian_constructor( thisPtr%gaussianComponents, &
		                            exponents, coefficients, noNormalize=.true. )
					do i=1,size(origin,dim=1)
						thisPtr%gaussianComponents%primitives(i)%origin = origin(i,:)
					end do	

			end select

		end if

		if ( trim(tagName) == "Potential" .and. .not.thisPtr%isInstanced) then

                         call ExternalPotential_exception( ERROR, &
		             "The interation "//trim(bblockName)//" for the "//trim(thisPtr%name)// " potential,  was not found",&
                              "Class object Basis in the XMLParser_endElement function" )		 

		end if

	end subroutine ExternalPotential_XMLParser_endElement



	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine ExternalPotential_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription
	
		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )
	
	end subroutine ExternalPotential_exception

end module ExternalPotential_
