!!**********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, moment integrals, recursive integrals,        !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para calculo de integrales de momento
!
! Este modulo contiene los algoritmos necesarios para la evaluacion de integrales de 
! momento entre pares de funciones gaussianas primitivas (PrimitiveGaussian_), 
! sin normalizar. 
!
! Las integrales son de la forma:
!
! \f[ (\bf{a} \mid {\mathbf{R}(\mu)} \mid \mathbf{b}) = \int_{TE} {{\varphi(\bf{r};{\zeta}_a,
! \bf{a,A})} {{\mathbf{R}(\mu)}} {\varphi(\bf{r};{\zeta}_b,\mathbf{b,B})}},dr \f]
!
! Donde:
!
! <table>
! <tr> <td> \f$ \zeta \f$ : <td> <dfn> exponente orbital. </dfn> 
! <tr> <td> <b> r </b> : <td> <dfn> coordenas espaciales de la funcion. </dfn>
! <tr> <td> <b> n </b> : <td> <dfn> indice de momento angular. </dfn>
! <tr> <td> <b> R </b> : <td> <dfn> origen de la funcion gaussiana cartesiana. </dfn>
! </table>
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-06-09
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-06-09 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapta al estandar de codificacion propuesto.
!
! @see PrimitiveGaussian_product, PrimitiveGaussian_ 
!
! @todo  Implementar integrales para funciones de momento angular 3.
!**
module MomentIntegrals_
	use PrimitiveGaussian_
	use OverlapIntegrals_
	use Exception_
	
	implicit none
	
	private ::
		real(8) :: originRc(3)
		real(8) :: commonFactor
		
		type(PrimitiveGaussian) :: primitives(2)
		type(PrimitiveGaussian) :: primitiveAxB
			
	public ::  &
		PrimitiveGaussian_momentIntegral
		
	private :: &
		MomentIntegrals_oSRecursion

contains
		
	!<
	!@brief Evalua integrales overlap para cualquier momento angular
	!@author Edwin Posada, 2010
	!@return devuelve los valores de integrales de overlap para una capa o individual
	!>
	function PrimitiveGaussian_momentIntegral(  primitiveA, primitiveB, originRC, component ) result (output)
	  	implicit none

 		type(PrimitiveGaussian) , intent(in) :: primitiveA
		type(PrimitiveGaussian) , intent(in) :: primitiveB
		real(8), intent(in) :: originRC(3)
		integer, intent(in) :: component
		real(8) :: output(primitiveA%numCartesianOrbital * primitiveB%numCartesianOrbital)


		real(8), allocatable :: orbitalIntegralsX(:,:)
		real(8), allocatable :: orbitalIntegralsY(:,:)
		real(8), allocatable :: orbitalIntegralsZ(:,:)
		real(8) :: zeta, zetaInv, overPf
		real(8) :: P(3), PA(0:3), PB(0:3)
		real(8) :: x00, y00, z00
		real(8) :: x01, y01, z01
		real(8) :: x10, y10, z10
		real(8) :: x11, y11, z11
		real(8) :: AB2
		integer(8) :: angularMomentIndexA(3, primitiveA%numcartesianOrbital)
		integer(8) :: angularMomentIndexB(3, primitiveB%numcartesianOrbital)
	 	integer :: maxAngularMomentIndex
	 	integer :: ax, ay, az
	 	integer :: bx, by, bz
	  	integer :: a, b
	  	integer :: counter

		angularMomentIndexA = PrimitiveGaussian_getAllAngularMomentIndex(primitiveA)
		angularMomentIndexB = PrimitiveGaussian_getAllAngularMomentIndex(primitiveB)

	  	maxAngularMomentIndex = max(sum(primitiveA%angularMomentIndex), sum(primitiveB%angularMomentIndex))

		if(allocated(orbitalIntegralsX))deallocate(orbitalIntegralsX)
	  	allocate(orbitalIntegralsX(0:maxAngularMomentIndex+2, 0:maxAngularMomentIndex+2))
	  	if(allocated(orbitalIntegralsY))deallocate(orbitalIntegralsY)
	  	allocate(orbitalIntegralsY(0:maxAngularMomentIndex+2, 0:maxAngularMomentIndex+2))
	  	if(allocated(orbitalIntegralsZ))deallocate(orbitalIntegralsZ)
	  	allocate(orbitalIntegralsZ(0:maxAngularMomentIndex+2, 0:maxAngularMomentIndex+2))

	  	orbitalIntegralsX = 0.0_8
	  	orbitalIntegralsY = 0.0_8
	  	orbitalIntegralsZ = 0.0_8

      	!calcular intermediarios
  		AB2 = 0.0_8
	  	AB2 = AB2 + (primitiveA%origin(1) - primitiveB%origin(1)) * (primitiveA%origin(1) - primitiveB%origin(1))
	  	AB2 = AB2 + (primitiveA%origin(2) - primitiveB%origin(2)) * (primitiveA%origin(2) - primitiveB%origin(2))
	  	AB2 = AB2 + (primitiveA%origin(3) - primitiveB%origin(3)) * (primitiveA%origin(3) - primitiveB%origin(3))

 		zeta = primitiveA%orbitalExponent + primitiveB%orbitalExponent
	  	zetaInv = 1.0_8/zeta

	  	P(1) = ( primitiveA%orbitalExponent*primitiveA%origin(1) + primitiveB%orbitalExponent *primitiveB%origin(1))*zetaInv
	  	P(2) = ( primitiveA%orbitalExponent*primitiveA%origin(2) + primitiveB%orbitalExponent *primitiveB%origin(2))*zetaInv
	  	P(3) = ( primitiveA%orbitalExponent*primitiveA%origin(3) + primitiveB%orbitalExponent *primitiveB%origin(3))*zetaInv

		PA(0) = P(1) - primitiveA%origin(1)
		PA(1) = P(2) - primitiveA%origin(2)
		PA(2) = P(3) - primitiveA%origin(3)
		PB(0) = P(1) - primitiveB%origin(1)
		PB(1) = P(2) - primitiveB%origin(2)
		PB(2) = P(3) - primitiveB%origin(3)

		overPf = exp(-primitiveA%orbitalExponent * primitiveB%orbitalExponent * AB2*zetaInv ) * &
					sqrt(Math_PI*zetaInv ) * Math_PI * zetaInv

  	  	!! recursion
	  	call MomentIntegrals_oSRecursion(orbitalIntegralsX, orbitalIntegralsY, orbitalIntegralsZ, PA, PB, zeta,&
	  									sum(primitiveB%angularMomentIndex)+2, sum(primitiveA%angularMomentIndex)+2)

  		output = 0.0_8

		if(primitiveA%isVectorized .and. primitiveB%isVectorized ) then

			ax = primitiveA%angularMomentIndex(1)
			ay = primitiveA%angularMomentIndex(2)
			az = primitiveA%angularMomentIndex(3)

			bx = primitiveB%angularMomentIndex(1)
			by = primitiveB%angularMomentIndex(2)
			bz = primitiveB%angularMomentIndex(3)

			x00 = orbitalIntegralsX(ax, bx)
		  	y00 = orbitalIntegralsY(ay, by)
		  	z00 = orbitalIntegralsZ(az, bz)

		  	x01 = orbitalIntegralsX(ax, bx+1)
		  	y01 = orbitalIntegralsY(ay, by+1)
		  	z01 = orbitalIntegralsZ(az, bz+1)

		  	x10 = orbitalIntegralsX(ax+1, bx)
		  	y10 = orbitalIntegralsY(ay+1, by)
		  	z10 = orbitalIntegralsZ(az+1, bz)

		  	x11 = orbitalIntegralsX(ax+1, bx+1)
		  	y11 = orbitalIntegralsY(ay+1, by+1)
		  	z11 = orbitalIntegralsZ(az+1, bz+1)

			select case (component)
				case(1) !X
		  			output(1) = output(1) - (overPf*(x01+x00*(primitiveB%origin(1)-originRC(1)))*y00*z00)
		  		case(2) !Y
		  			output(1) = output(1) - (overPf*x00*(y01+y00*(primitiveB%origin(2)-originRC(2)))*z00)
		  		case(3) !Z
		  			output(1) = output(1) - (overPf*x00*y00*(z01+z00*(primitiveB%origin(3)-originRC(3))))
		  	end select

		else if(primitiveA%isVectorized .and. .not.primitiveB%isVectorized ) then

			counter = 1
			do b = 1, primitiveB%numCartesianOrbital

				ax = primitiveA%angularMomentIndex(1)
				ay = primitiveA%angularMomentIndex(2)
				az = primitiveA%angularMomentIndex(3)

				bx = angularMomentIndexB(1, b)
				by = angularMomentIndexB(2, b)
				bz = angularMomentIndexB(3, b)

				x00 = orbitalIntegralsX(ax, bx)
			  	y00 = orbitalIntegralsY(ay, by)
			  	z00 = orbitalIntegralsZ(az, bz)

			  	x01 = orbitalIntegralsX(ax, bx+1)
			  	y01 = orbitalIntegralsY(ay, by+1)
			  	z01 = orbitalIntegralsZ(az, bz+1)

			  	x10 = orbitalIntegralsX(ax+1, bx)
			  	y10 = orbitalIntegralsY(ay+1, by)
			  	z10 = orbitalIntegralsZ(az+1, bz)

			  	x11 = orbitalIntegralsX(ax+1, bx+1)
			  	y11 = orbitalIntegralsY(ay+1, by+1)
			  	z11 = orbitalIntegralsZ(az+1, bz+1)

				select case (component)
					case(1) !X
			  			output(counter) = output(counter) - (overPf*(x01+x00*(primitiveB%origin(1)-originRC(1)))*y00*z00)
			  			counter = counter + 1
			  		case(2) !Y
			  			output(counter) = output(counter) - (overPf*x00*(y01+y00*(primitiveB%origin(2)-originRC(2)))*z00)
			  			counter = counter + 1
			  		case(3) !Z
			  			output(counter) = output(counter) - (overPf*x00*y00*(z01+z00*(primitiveB%origin(3)-originRC(3))))
			  			counter = counter + 1
			  	end select

			end do

		else if(.not.primitiveA%isVectorized .and. primitiveB%isVectorized ) then

			counter = 1
			do a = 1, primitiveA%numCartesianOrbital

				bx = primitiveB%angularMomentIndex(1)
				by = primitiveB%angularMomentIndex(2)
				bz = primitiveB%angularMomentIndex(3)

				ax = angularMomentIndexA(1, a)
				ay = angularMomentIndexA(2, a)
				az = angularMomentIndexA(3, a)

				x00 = orbitalIntegralsX(ax, bx)
			  	y00 = orbitalIntegralsY(ay, by)
			  	z00 = orbitalIntegralsZ(az, bz)

			  	x01 = orbitalIntegralsX(ax, bx+1)
			  	y01 = orbitalIntegralsY(ay, by+1)
			  	z01 = orbitalIntegralsZ(az, bz+1)

			  	x10 = orbitalIntegralsX(ax+1, bx)
			  	y10 = orbitalIntegralsY(ay+1, by)
			  	z10 = orbitalIntegralsZ(az+1, bz)

			  	x11 = orbitalIntegralsX(ax+1, bx+1)
			  	y11 = orbitalIntegralsY(ay+1, by+1)
			  	z11 = orbitalIntegralsZ(az+1, bz+1)

				select case (component)
					case(1) !X
			  			output(counter) = output(counter) - (overPf*(x01+x00*(primitiveB%origin(1)-originRC(1)))*y00*z00)
			  			counter = counter + 1
			  		case(2) !Y
			  			output(counter) = output(counter) - (overPf*x00*(y01+y00*(primitiveB%origin(2)-originRC(2)))*z00)
			  			counter = counter + 1
			  		case(3) !Z
			  			output(counter) = output(counter) - (overPf*x00*y00*(z01+z00*(primitiveB%origin(3)-originRC(3))))
			  			counter = counter + 1
			  	end select

			end do

		else if(.not.primitiveA%isVectorized .and. .not.primitiveB%isVectorized ) then

			counter = 1
			do a = 1, primitiveA%numCartesianOrbital
				do b = 1, primitiveB%numCartesianOrbital

					ax = angularMomentIndexA(1, a)
					ay = angularMomentIndexA(2, a)
					az = angularMomentIndexA(3, a)

					bx = angularMomentIndexB(1, b)
					by = angularMomentIndexB(2, b)
					bz = angularMomentIndexB(3, b)

					x00 = orbitalIntegralsX(ax, bx)
				  	y00 = orbitalIntegralsY(ay, by)
				  	z00 = orbitalIntegralsZ(az, bz)

				  	x01 = orbitalIntegralsX(ax, bx+1)
				  	y01 = orbitalIntegralsY(ay, by+1)
				  	z01 = orbitalIntegralsZ(az, bz+1)

				  	x10 = orbitalIntegralsX(ax+1, bx)
				  	y10 = orbitalIntegralsY(ay+1, by)
				  	z10 = orbitalIntegralsZ(az+1, bz)

				  	x11 = orbitalIntegralsX(ax+1, bx+1)
				  	y11 = orbitalIntegralsY(ay+1, by+1)
				  	z11 = orbitalIntegralsZ(az+1, bz+1)

					select case (component)
						case(1) !X
				  			output(counter) = output(counter) + overPf*(x01+x00*(primitiveB%origin(1)-originRC(1)))*y00*z00
				  			counter = counter + 1
				  		case(2) !Y
				  			output(counter) = output(counter) + overPf*x00*(y01+y00*(primitiveB%origin(2)-originRC(2)))*z00
				  			counter = counter + 1
				  		case(3) !Z
				  			output(counter) = output(counter) + overPf*x00*y00*(z01+z00*(primitiveB%origin(3)-originRC(3)))
				  			counter = counter + 1
				  	end select

				end do
			end do

		end if

	end function PrimitiveGaussian_momentIntegral

	subroutine MomentIntegrals_oSRecursion(orbitalIntegralsX, orbitalIntegralsY, orbitalIntegralsZ,&
												 PA, PB, zeta, angularMomentIndexA, angularMomentIndexB)
		implicit none

	  	real(8), intent(inout), allocatable :: orbitalIntegralsX(:,:), orbitalIntegralsY(:,:), orbitalIntegralsZ(:,:)
	  	real(8), intent(in) :: PA(0:3), PB(0:3)
	  	real(8), intent(in) :: zeta
	  	integer(8), intent(in) :: angularMomentIndexA, angularMomentIndexB

	  	real(8) :: twoZetaInv
	  	integer :: i, j, k

	  	twoZetaInv = 1_8/(2_8*zeta)

	  	orbitalIntegralsX(0,0) = 1.0_8
	  	orbitalIntegralsY(0,0) = 1.0_8
	  	orbitalIntegralsZ(0,0) = 1.0_8

      	!! Upward recursion in j for i=0

	  	orbitalIntegralsX(0,1) = PB(0)
	  	orbitalIntegralsY(0,1) = PB(1)
	  	orbitalIntegralsZ(0,1) = PB(2)

	  	do j=1, angularMomentIndexB -1
	    	orbitalIntegralsX(0,j+1) = PB(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(0,j+1) = PB(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(0,j+1) = PB(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(0,j+1) = orbitalIntegralsX(0,j+1) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(0,j+1) = orbitalIntegralsY(0,j+1) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(0,j+1) = orbitalIntegralsZ(0,j+1) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

      	!! Upward recursion in i for all j's

	  	orbitalIntegralsX(1,0) = PA(0)
	  	orbitalIntegralsY(1,0) = PA(1)
	  	orbitalIntegralsZ(1,0) = PA(2)

	  	do j=1, angularMomentIndexB
	    	orbitalIntegralsX(1,j) = PA(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(1,j) = PA(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(1,j) = PA(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(1,j) = orbitalIntegralsX(1,j) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(1,j) = orbitalIntegralsY(1,j) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(1,j) = orbitalIntegralsZ(1,j) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

	  	do i=1, angularMomentIndexA - 1
	    	orbitalIntegralsX(i+1,0) = PA(0)*orbitalIntegralsX(i,0)
	    	orbitalIntegralsY(i+1,0) = PA(1)*orbitalIntegralsY(i,0)
	    	orbitalIntegralsZ(i+1,0) = PA(2)*orbitalIntegralsZ(i,0)
	    	orbitalIntegralsX(i+1,0) = orbitalIntegralsX(i+1,0) + i*twoZetaInv*orbitalIntegralsX(i-1,0)
	    	orbitalIntegralsY(i+1,0) = orbitalIntegralsY(i+1,0) + i*twoZetaInv*orbitalIntegralsY(i-1,0)
	    	orbitalIntegralsZ(i+1,0) = orbitalIntegralsZ(i+1,0) + i*twoZetaInv*orbitalIntegralsZ(i-1,0)
	    	do j=1, angularMomentIndexB
	      		orbitalIntegralsX(i+1,j) = PA(0)*orbitalIntegralsX(i,j)
	      		orbitalIntegralsY(i+1,j) = PA(1)*orbitalIntegralsY(i,j)
	      		orbitalIntegralsZ(i+1,j) = PA(2)*orbitalIntegralsZ(i,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + i*twoZetaInv*orbitalIntegralsX(i-1,j)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + i*twoZetaInv*orbitalIntegralsY(i-1,j)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + i*twoZetaInv*orbitalIntegralsZ(i-1,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + j*twoZetaInv*orbitalIntegralsX(i,j-1)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + j*twoZetaInv*orbitalIntegralsY(i,j-1)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + j*twoZetaInv*orbitalIntegralsZ(i,j-1)
	    	end do
	  	end do

	end subroutine MomentIntegrals_oSRecursion

end module MomentIntegrals_
