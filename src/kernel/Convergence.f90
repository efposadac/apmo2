!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module Convergence_
	use Matrix_
	use StackOfMatrices_
	

	implicit none

	type, public :: Convergence
		
		character(30) :: name

		type(Matrix), pointer :: subspaceElementPtr
		type(StackOfMatrices) :: componentsOfError
		type(StackOfMatrices) :: subspaceElements
		type(Matrix) :: diisEquationsSystem
		type(Matrix) :: initialSubspaceElement

		real(8)  :: diisError
		integer :: diisDimensionality
		integer :: currentsize
		logical :: isInstanced
		logical :: isLastComponent

		integer :: methodType

	end type

	!< enum Matrix_type {
	integer, parameter, public :: CONVERGENCE_DDEFAULT		=  -1
	integer, parameter, public :: CONVERGENCE_NNONE			=  0
	integer, parameter, public :: CONVERGENCE_DDAMPING		=  1
	integer, parameter, public :: CONVERGENCE_DDIIS			=  2
	!< }



	public :: &
		Convergence_constructor, &
		Convergence_destructor

	private
contains

	subroutine Convergence_constructor(this, name, dimensionality, methodType )
		implicit none
		type(Convergence) :: this
		character(*), optional :: name
		integer, optional :: dimensionality
		integer, optional :: methodType

		this.name = "undefined"
		if ( present(name) ) this.name = trim(name)

		this.diisDimensionality = 5
		if ( present( dimensionality) ) this.diisDimensionality = dimensionality

		this.methodType=2
		if ( present(methodType) ) this.methodType=methodType
		
		this.isInstanced = .true.
		this.diisError = 0.0_8
		this.isLastComponent = .false.

	end subroutine Convergence_constructor

	subroutine Convergence_destructor(this)
		implicit none
		type(Convergence) :: this

		if ( StackOfMatrices_isInstanced( this.subspaceElements ) ) then

			call StackOfMatrices_destructor( this.subspaceElements )
			call StackOfMatrices_destructor( this.componentsOfError)
			call Matrix_destructor( this.diisEquationsSystem )
			
		end if
		
		this.isInstanced = .false.
		this.isLastComponent = .false.
		this.methodType = 2
		this.subspaceElementPtr => null()

	end subroutine Convergence_destructor

	!>
	!! @brief Ajusta los parametros requeridos por el metodo convergencia
	!<
	subroutine Convergence_setMethod( this, subspaceElement, methodType )
		implicit none
		type(Convergence), intent(inout), target :: this
		type(Matrix), target :: subspaceElement
		integer, optional :: methodType

		if ( present(methodType) ) this.methodType = methodType


		if ( this.methodType == 2 ) then
			
			this.subspaceElementPtr => subspaceElement

			!!*****************************************************
			!! Separa espacio de memoria para sistema de ecuaciones DIIS
			!!*****
			call Matrix_constructor( this.diisEquationsSystem, int(this.diisDimensionality+1,8), int(this.diisDimensionality+1,8) )
			this.diisEquationsSystem.values= 0.0_8
			this.diisEquationsSystem.values(1,:) = -1.0_8
			this.diisEquationsSystem.values(:,1) = -1.0_8
			this.diisEquationsSystem.values(1,1)= 0.0_8

		end if

	end subroutine Convergence_setMethod

	!>
	!! @brief Indica si el objeto ha sidi instanciado
	!>
	function Convergence_isInstanced( this ) result( output )
		implicit none
		type(Convergence), intent(in) :: this
		logical :: output
			
		output =this.isInstanced
		
	end function Convergence_isInstanced

	!>
	!! @brief devuelve el nombre del metodo
	!<
	function Convergence_getName( this ) result( output )
		implicit none
		type(Convergence), intent(in) :: this
		character(30) :: output
			
		output =trim(this.name)
		
	
	end function Convergence_getName

	!>
	!! @brief devuelve el error diis
	!<
	function Convergence_getDiisError( this ) result( output )
		implicit none
		type(Convergence), intent(in) :: this
		real(8) :: output
			
		output = this.diisError
		
	end function Convergence_getDiisError
	
	!>
	!! @brief Calcula un mejor estimado de un elmento del subespacio iterativo, mediante el 
	!!		metodo de convergencia seleccionado
	!<
	subroutine Convergence_run(this)
		implicit none
		type(Convergence) :: this

		if ( this.isInstanced ) then
		
			select case(this.methodType)
			
				case(CONVERGENCE_NNONE)

					call Convergence_none( this)
					
				case(CONVERGENCE_DDAMPING)

					call Convergence_damping( this )
				
				case(CONVERGENCE_DDIIS)

					if ( StackOfMatrices_isInstanced( this.subspaceElements ) == .false. ) then
					
						call StackOfMatrices_constructor( this.subspaceElements, &
							ssize = this.diisDimensionality )
						call StackOfMatrices_constructor( this.componentsOfError, &
							ssize = this.diisDimensionality )

						this.initialSubspaceElement = this.subspaceElementPtr

					else

						call Convergence_diis( this )

					end if

				case default

					call Convergence_damping( this )
				
			end select
		
		else

			call Convergence_exception( ERROR, "The object has not beeb instanced",&
				"Class object Convergence in run() function" )
		
		end if
	
	end subroutine Convergence_run
	
	!>
	!! @brief No realiza ninguna modificacion a la matriz de Fock o a la
	!! 		matriz de densidad asociada
	!>
	subroutine Convergence_none(  this )
		implicit none
		type(Convergence) :: this

	end subroutine Convergence_none
	
	!>
	!! @brief Calcula una nueva matriz de Fock con el metodo de amortiguamiento
	!<
	subroutine Convergence_damping( this )
		implicit none
		type(Convergence) :: this

	end subroutine Convergence_damping
	
	!>
	!! @brief Calcula una nueva matriz de densidad con la componentes de un
	!!		subespacio definido con la matrices de Fock anteriores
	!! 		y a la matriz de densidad asociada
	!<
	subroutine Convergence_diis( this)
		implicit none
		type(Convergence) :: this
						
		type(Matrix) :: currentError
		real(8), pointer :: currentErrorPtr(:,:)
		real(8), pointer :: beforeErrorPtr(:,:)
		real(8), pointer :: subspaceElement(:,:)
		real(8), allocatable :: rightSideCoefficients(:)
		real(8), allocatable :: coefficientsOfCombination(:)
		type(Matrix) :: diisEquationsSystem !! Debe almacenarse una para cada especie
		type(Matrix) :: rightMatrix
		type(Matrix) :: leftMatrix
		type(Matrix) :: singularValues
		type(Matrix) :: auxMatrix
		integer :: orderOfMatrix
		integer :: i
		integer :: j

		orderOfMatrix = size(this.subspaceElementPtr.values, 1)
		call Matrix_constructor( currentError, int(orderOfMatrix,8), int(orderOfMatrix,8 ) )

		if ( StackOfMatrices_empty( this.subspaceElements ) ) then

			currentError.values = this.subspaceElementPtr.values - this.initialSubspaceElement.values
			call Matrix_destructor( this.initialSubspaceElement )

		else
			this.currentSize = StackOfMatrices_size( this.subspaceElements )
		
			subspaceElement => StackOfMatrices_getValuesPtr( this.subspaceElements, this.currentsize )
		
			!! Calcula el elemnto de error  asociado al nuevo elemento del subespacio
			currentError.values = this.subspaceElementPtr.values - subspaceElement

		end if

		!! Determina el error para la nueva componente
		this.diisError = abs( maxval(currentError.values) )

		if( this.diisError < 0.5_8 )  then	

			!! Almacena matriz de error y matriz de Fock
			call StackOfMatrices_push( this.componentsOfError, currentError.values )
			call StackOfMatrices_push( this.subspaceElements,  this.subspaceElementPtr.values)
		
			this.currentSize = StackOfMatrices_size( this.subspaceElements )
		
			if ( this.currentsize >= 2 ) then
				
				!! Vector con valores de la constantes  del sistema lineal
				allocate( rightSideCoefficients( this.currentSize + 1 ) )
				rightSideCoefficients = 0.0_8
				rightSideCoefficients(1) = -1.0_8
							
				allocate( coefficientsOfCombination( this.currentSize + 1 ) )
				call Matrix_constructor(auxMatrix, int(this.currentsize+1,8), int(this.currentsize+1,8) )

				!! Elimina la primera fila de la matriz de error y mueve las siguiente en un nivel
				if ( this.isLastComponent ) then

					this.diisEquationsSystem.values(2:this.currentsize,2:this.currentSize) = & 
						this.diisEquationsSystem.values(3:this.currentSize+1,3:this.currentSize+1)
					
				end if

				if ( this.currentSize <= this.diisDimensionality ) then
					
					!!
					!! Calcula la ultima fila de la matriz de error
					!! 
					currentErrorPtr => StackOfMatrices_getValuesPtr( this.componentsOfError, this.currentSize )
					do i=this.currentSize,1,-1
						beforeErrorPtr  => StackOfMatrices_getValuesPtr( this.componentsOfError, i )
						this.diisEquationsSystem.values(i+1,this.currentSize+1) = sum(currentErrorPtr*beforeErrorPtr)
						this.diisEquationsSystem.values(this.currentSize+1,i+1)  =  &
							this.diisEquationsSystem.values(i+1,this.currentSize+1)
						beforeErrorPtr => null()
					end do
					currentErrorPtr => null()
					if( this.currentSize==this.diisDimensionality ) this.isLastComponent=.true.
				end if

				auxMatrix.values = this.diisEquationsSystem.values(1:this.currentSize+1,1:this.currentSize+1)

				!! Diagonaliza la matiz por SVD de la forma A=UWV^T
				call Matrix_svd( auxMatrix, leftMatrix, rightMatrix, singularValues)
				
				!! Invierte la matriz de valores singulares
				do i=1,size(singularValues.values,dim=1)
					if( abs(singularValues.values(i,i)) > 1.0D-4 ) then
						singularValues.values(i,i) = 1/singularValues.values(i,i)
					else
						singularValues.values(i,i) = singularValues.values(i,i)
					end if
				end do
				
				coefficientsOfCombination=matmul( &
							matmul(matmul(transpose(rightMatrix.values), &
							singularValues.values),transpose(leftMatrix.values)),rightSideCoefficients )

				call Matrix_destructor( auxMatrix )
				call Matrix_destructor( singularValues )
				call Matrix_destructor( rightMatrix )
				call Matrix_destructor( leftMatrix )
			
				!! Construye el mejor estimado de la solucion como una combinacion lineal de elementos del sub-espacio 
				this.subspaceElementPtr.values = 0.0_8
				
				do i=1, this.currentSize
					subspaceElement => StackOfMatrices_getValuesPtr( this.subspaceElements, i )
					this.subspaceElementPtr.values =  &
						this.subspaceElementPtr.values + coefficientsOfCombination(i+1) * subspaceElement
					subspaceElement => null()
				end do
				
				deallocate( rightSideCoefficients )
				deallocate( coefficientsOfCombination )
			
			else &
			if ( this.currentsize == 1 ) then
				
				currentErrorPtr => StackOfMatrices_getValuesPtr( this.componentsOfError, 1 )
				beforeErrorPtr  => StackOfMatrices_getValuesPtr( this.componentsOfError, 1 )
				this.diisEquationsSystem.values(2,2) = sum(currentErrorPtr*beforeErrorPtr)
				currentErrorPtr => null()
				beforeErrorPtr => null()

			end if

		end if

		call Matrix_destructor(currentError)
				
	end subroutine Convergence_diis
	
	!>
	!! @brief Retorna el error diis de una matriz de fock
	!<
	subroutine Convergence_obtainDiisError(this)
		implicit none
		type(Convergence) :: this

		real(8), pointer :: subspaceElement(:,:)
		type(Matrix) :: currentError
		integer :: orderOfMatrix

		orderOfMatrix = size(this.subspaceElementPtr.values, 1)
		call Matrix_constructor( currentError, int(orderOfMatrix,8), int(orderOfMatrix,8 ) )

		if ( StackOfMatrices_empty( this.subspaceElements ) ) then

			currentError.values = this.subspaceElementPtr.values - this.initialSubspaceElement.values
			call Matrix_destructor( this.initialSubspaceElement )

		else
			this.currentSize = StackOfMatrices_size( this.subspaceElements )
		
			subspaceElement => StackOfMatrices_getValuesPtr( this.subspaceElements, this.currentsize )
		
			!! Calcula el elemnto de error  asociado al nuevo elemento del subespacio
			currentError.values = this.subspaceElementPtr.values - subspaceElement

		end if

		!! Determina el error para la nueva componente
		this.diisError = abs( maxval(currentError.values) )

		call Matrix_destructor( currentError)

	end subroutine Convergence_obtainDiisError


	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine Convergence_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription

		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )

	end subroutine Convergence_exception

end module Convergence_