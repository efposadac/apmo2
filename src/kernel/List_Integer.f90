!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module List_Integer_
	use Exception_
	
	implicit none

	!>
	!! @brief  Modulo para implementacion de listas de enteros
	!!		Esta es un implementacion temporal, ver List para detalles
	!!
	!! @author Sergio A. Gonzalez
	!!
	!! <b> Fecha de creacion : </b> 2008-09-19
	!!   - <tt> 2007-08-19 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
	!!        -# Creacion del archivo y las funciones basicas
	!!
	!! @todo implementar empleando punteros como una lista enlazada de estos
	!! @todo Extender el m\'etodo para aceptar lista de vetores y matrices
	!<	

	type, public :: List_Integer
	
		character(30) :: name
		integer, allocatable :: data(:)
		integer, pointer :: current
		integer :: iterator
		integer :: maxSize
		logical :: isSizeUndefined
		
	end type List_Integer
	
	
	public :: &
		List_Integer_constructor, &
		List_Integer_assign, &
		List_Integer_back, &
		List_Integer_begin, &
		List_Integer_clear, &
		List_Integer_empty, &
		List_Integer_end, &
		List_Integer_erase, &
		List_Integer_front, &
		List_Integer_insert, &
		List_Integer_max_size, &
		List_Integer_merge, &
		List_Integer_pop_back, &
		List_Integer_pop_front, &
		List_Integer_remove, &
		List_Integer_push_back, &
		List_Integer_push_front, &
		List_Integer_resize	, &
		List_Integer_reverse, &
		List_Integer_size, &
		List_Integer_sort, &
		List_Integer_swap , &
		List_Integer_unique, &
		List_Integer_iterate, &
		List_Integer_current

		private :: &
			List_Integer_exception

contains
	
	!>
	!! @brief Constructor
	!!	Crea una lista y la inicializa
	!<		 
	subroutine List_Integer_constructor( this, name, ssize ) 
		implicit none
		type(List_Integer), target, intent(inout) :: this
		character(*), optional :: name
		integer, optional, intent(in) :: ssize
	
		this.maxSize =10
		this.name = "undefined"
		this.isSizeUndefined = .false.
		
		if ( present(name) ) this.name = trim(name)

		if (present(ssize)) then
		
			this.maxSize =ssize
			if( ssize < 0 ) then
				this.isSizeUndefined = .true.
				this.maxSize =1
			end if
			
		end if
		
		allocate( this.data(1) )
		this.data = 0
		this.iterator = 0
		this.current => null()
		
	end  subroutine List_Integer_constructor
	
	!>
	!! @brief Destructor
	!!            Elimina una lista previamente creada y libera la memoria asociada
	!!
	!! @ Verificar que la memoria priviamente separada sea liberada
	!<
	subroutine List_Integer_destructor( this ) 
		implicit none
		type(List_Integer), intent(inout) :: this
		
		integer :: i
		integer :: currentSize
	
		if ( allocated(this.data) ) then
	
			this.current => null()
			this.maxSize = 0
			this.iterator = 0
			deallocate(this.data)
	
		end if
		
	end  subroutine List_Integer_destructor

	!>
	!! @brief Asigna un grupo elementos a la lista
	!!
	!! @todo Falta implementar
	!<
	subroutine List_Integer_assign(this, first, last, value)
		implicit none
		type(List_Integer), intent(inout) :: this
		integer :: first
		integer :: last
		integer :: value
	
	end subroutine List_Integer_assign
	
	!>
	!! @brief Retorna un puntero al ultimo elemento de la lista
	!!
	!<
	function List_Integer_back( this ) result(output)
		implicit none
		type(List_Integer), target, intent(inout) :: this
		integer, pointer :: output
		
		if ( allocated( this.data) ) then
			
			output => null()
			this.iterator = size( this.data )
			output => this.data(this.iterator)
			
		
		else

			call List_Integer_exception( ERROR, "The list "// trim(this.name) //" is empty", "Class object List_Integer in the back() function")
		
		end if
	
	end function List_Integer_back
	
	!>
	!! @brief Coloca el puntero del iterador al inicio de la lista 
	!
	!<
	subroutine List_Integer_begin( this)
		type(List_Integer), target, intent(inout) :: this
		
		if ( allocated(this.data) ) then
			
			this.current => null()
			this.current => this.data(1)
			this.iterator = 1
			
		else
			
			call List_Integer_exception(ERROR, "The list "// trim(this.name) //" is empty", "Class object List_Integer in the begin() function" )
		
		end if
		
	end subroutine List_Integer_begin
	
	!>
	!! @brief Remueve todo los elementos del la lista 
	!!
	!<
	subroutine List_Integer_clear( this)
		type(List_Integer), intent(inout) :: this
		
		if ( allocated(this.data) ) then
	
			this.current=> null()
			deallocate( this.data )
			allocate( this.data(1) )
			this.data = 0.0_8
			this.iterator = 0
			
			
		end if
		
	end subroutine List_Integer_clear
	
	!>
	!! @brief Indica si la lista tiene o no elementos
	!<
	function List_Integer_empty( this ) result(output)
		implicit none
		type(List_Integer), intent(in) :: this
		logical :: output
	
		output=.false.
	
		if( .not.allocated(this.data) .or. this.iterator == 0 ) output =.true.
		
	end function List_Integer_empty

	!>
	!! @brief Coloca el puntero del iterador al final de la lista 
	!!
	!<
	subroutine List_Integer_end( this)
		type(List_Integer), target, intent(inout) :: this
		
		if ( allocated( this.data ) ) then
			
			this.current=>null()
			this.iterator = size(this.data)
			this.current => this.data( this.iterator )
			
		else
			
			call List_Integer_exception( ERROR, "The list "// trim(this.name) //" is empty", "Class object List_Integer in the end() function" )
		
		end if
		
	end subroutine List_Integer_end

	!>
	!! @brief Remueve todo los elementos del la lista 
	!!
	!! @todo Falta por implementar
	!<
	subroutine List_Integer_erase( this)
		type(List_Integer), intent(inout) :: this
		
		
	end subroutine List_Integer_erase


	!>
	!! @brief Retorna una puntero al primer elemento de la lista
	!!
	!<
	function List_Integer_front( this ) result(output)
		implicit none
		type(List_Integer), target, intent(inout) :: this
		integer, pointer :: output
		
		if ( allocated( this.data ) ) then
			
			output => null()	
			output => this.data(1)
		
		else
		
			call List_Integer_exception( ERROR, "The list "// trim(this.name) //" is empty", "Class object List_Integer in the back() function" )
		
		end if
	
	end function List_Integer_front

	!>
	!! @brief Inserta un elemento en la posicion especificada 
	!!
	!! @todo Falta implementacion completa
	!<
	subroutine List_Integer_insert( this, position)
		type(List_Integer), intent(inout) :: this
		integer :: position
		
	end subroutine List_Integer_insert

	!>
	!! @brief Retorna el maximo numero de elementos que se puden almacenar
	!! 		en la lista.
	!<
	function List_Integer_max_size( this) result(output)
		implicit none
		type(List_Integer),  intent(in) :: this
		integer :: output
	
		output = this.maxSize
		
	end function List_Integer_max_size	

	!>
	!! @brief Fusiona dos lista en una borrando un de ellas 
	!!
	!! @todo Falta implementacion completa
	!<
	subroutine List_Integer_merge( this, otherThisToMerge )
		type(List_Integer), intent(inout) :: this
		type(List_Integer), intent(inout) :: otherThisToMerge
		
		
	end subroutine List_Integer_merge


	!>
	!! @brief Elimina el �ltimo elemento de la lista, el primer elemento no es eliminado
	!<
	subroutine List_Integer_pop_back( this )
		implicit none
		type(List_Integer), target, intent(inout) :: this
		
		integer, allocatable :: auxData(:)
		integer :: ssize
	
		if ( allocated(this.data)  ) then
		
			if ( size(this.data) > 1 ) then
			
				this.current  => null()
				ssize = size(this.data) - 1
				allocate( auxData(ssize) )
				auxData = this.data(1:ssize)
				deallocate( this.data )
				allocate( this.data(ssize) )
				this.data = auxData
				this.iterator = ssize
				this.current => this.data(ssize)
				deallocate( auxData )
				
			end if
				
		else

			call List_Integer_exception( WARNING, "The list is empty", "Class object List_Integer in the pop_back() function" )

		end if

	end subroutine List_Integer_pop_back

	!>
	!! @brief Elimina el primer elemento de la lista
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_pop_front( this )
		implicit none
		type(List_Integer), intent(inout) :: this
		
	end subroutine List_Integer_pop_front

	!>
	!! @brief Adiciona un elemento al final a la lista
	!!
	!! @ Verificar que la memoria de cada nodo sea liberada cuando �ste se elimine
	!<
	subroutine List_Integer_push_back( this, data )
		implicit none
		type(List_Integer), target, intent(inout) :: this
		integer, intent(in) :: data
		
		integer, allocatable :: auxData(:)
		integer :: ssize
		
		if ( allocated(this.data)  ) then
		
			ssize = size( this.data ) 
			
			if ( this.isSizeUndefined ) this.maxSize = this.maxSize + 1
			
			if ( this.iterator /= 0 .and. ssize < this.maxSize ) then
				
				this.current => null()
				allocate( auxData( ssize ) )
				auxData = this.data
				deallocate(this.data)
				allocate( this.data( ssize + 1 ) )
				this.data(1:ssize) = auxData
				this.data(ssize+1) = data
				this.iterator = ssize+1
				this.current => this.data(ssize+1)
				deallocate(auxData)
				
			else if ( ssize== this.maxSize) then
			
				this.current => null()
				allocate( auxData( ssize ) )
				auxData = this.data
				this.data(1:ssize-1) = auxData(2:ssize)
				this.data(ssize) = data
				this.current => this.data(ssize)
				deallocate(auxData)
				
			else
			
				this.data(1) = data
				this.iterator = 1
				this.current => this.data(1)

			end if
		
		else
			
			call List_Integer_exception(ERROR, "The list "// trim(this.name) //" is empty", "Class object List_Integer in the begin() function" )
		
		end if
	
	end subroutine List_Integer_push_back

	!>
	!! @brief Adiciona un elemento al inicio de la lista
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_push_front( this )
		implicit none
		type(List_Integer), intent(inout) :: this
		
	end subroutine List_Integer_push_front
	
	!>
	!! @brief Remueve el nodo especificado de la lista
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_remove( this, position )
		implicit none
		type(List_Integer), intent(inout) :: this
		integer :: position
		
	end subroutine List_Integer_remove

	!>
	!! @brief Redefine el tama�o maximo de la lista
	!!
	!<
	subroutine List_Integer_resize( this, resize )
		implicit none
		type(List_Integer), intent(inout) :: this
		integer :: resize
		
		this.maxSize = resize
		
	end subroutine List_Integer_resize

	!>
	!! @brief Invierte la lista
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_reverse( this)
		implicit none
		type(List_Integer), intent(inout) :: this
		
	end subroutine List_Integer_reverse

	!>
	!! @brief Retorna el tama�o actual o ocupacion de la pila
	!<
	function List_Integer_size( this) result(output)
		implicit none
		type(List_Integer),  intent(in) :: this
		integer :: output
	
		output=0
	
		if( allocated(this.data) ) then
			
			output =size( this.data )
	
		end if
		
	end function List_Integer_size

	!>
	!! @brief Ordena los elementos de la lista en orden ascendente
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_sort( this)
		implicit none
		type(List_Integer), intent(inout) :: this
		
		
	end subroutine List_Integer_sort

	!>
	!! @brief Intercambia los elementos entre dos listas
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_swap( this, otherThis)
		implicit none
		type(List_Integer), intent(inout) :: this
		type(List_Integer), intent(inout) :: otherThis
		
		
	end subroutine List_Integer_swap

	!>
	!! @brief remueve los elemetos repetidos de la lista siempre que
	!!		sean consecutivos 
	!!
	!! @todo Falta toda la implemtacion
	!<
	subroutine List_Integer_unique( this)
		implicit none
		type(List_Integer), intent(inout) :: this
	
		
	end subroutine List_Integer_unique

	!>
	!! @brief Mueve el iterador de la lista el numero de nodo que se especifique 
	!!
	!<
	subroutine List_Integer_iterate( this, iterator)
		implicit none
		type(List_Integer), target, intent(inout) :: this
		integer, optional :: iterator
		
		integer :: auxIterator
		integer :: i
		
		auxIterator=1
		if ( present(iterator) ) auxIterator = iterator
		
		if ( allocated(this.data) ) then
			
			if( ( ( this.iterator + auxIterator ) >= 1 ) .and.  ( ( this.iterator + auxIterator) <= this.maxSize  )  ) then
			
				do i=1, abs(auxIterator)
			
					if ( auxIterator > 0) then
			
						this.iterator = this.iterator + 1
						this.current => this.data(this.iterator)
					
					else
						
						this.iterator = this.iterator - 1
						this.current => this.data(this.iterator)
					
					end if
				
				end do
					
			else

				call List_Integer_exception( ERROR, "The current iterator is greater than  the size of the List_Integer", &
						 "Class object List_Integer in the iterate(i) function" )
					
			end if
	
		end if
		
	end subroutine List_Integer_iterate


	!>
	!! @brief Retorna un puntero (referencia) al valor del nodo donde se encuentra el iterador 
	!!
	!! @todo Hacer que el procedimineto salga mediante una excepcion.
	!<
	function List_Integer_current( this ) result( output )
		implicit none
		type(List_Integer), target, intent(inout) :: this
		integer, pointer :: output
		
	
		if ( associated(this.current) ) then
		
			output => this.data( this.iterator )
		
		else
		
			!! Soluci�n temporal, no se llama la excepcion, dado que el programa no acepta salidas 
			!! alfanumericas antes de abortar el programa
			call abort( "ERROR, The list "//trim(this.name)//" is empty, current iterator is pointing to null()" )
					
		end if
		
	end function List_Integer_current


	!>
	!! @brief  Maneja exceociones de la clase
	!<
	subroutine List_Integer_exception(typeMessage, description, debugDescription)
			implicit none
			integer :: typeMessage
			character(*) :: description
			character(*) :: debugDescription
	
			type(Exception) :: ex

			call Exception_constructor( ex , typeMessage )
			call Exception_setDebugDescription( ex, debugDescription )
			call Exception_setDescription( ex, description )
			call Exception_show( ex )
			call Exception_destructor( ex )
	
	end subroutine List_Integer_exception

end module List_Integer_