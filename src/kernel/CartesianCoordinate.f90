!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module CartesianCoordinate_
     use Matrix_
     use String_
     implicit none


     type, public :: CartesianCoordinate
          character(100) :: name
          type(Matrix) :: coordinate
          character(50), allocatable :: atomSymbol(:)
          character(50), allocatable :: atomSymbolBkp(:)
          character(50), allocatable :: basisSet(:)
          real(8), allocatable :: charge(:)
          real(8), allocatable :: mass(:)
          character(255) :: quantumNucleiLocalization
          integer :: numberOfQuantumNucleous
          integer :: numberOfNucleones
     end type


     public :: &
          CartesianCoordinate_constructor, &
          CartesianCoordinate_destructor, &
          CartesianCoordinate_write, &
          CartesianCoordinate_scaleCharge, &
          CartesianCoordinate_scaleMass, &
          CartesianCoordinate_addPrefixToSymbols, &
          CartesianCoordinate_addSuffixToSymbols, &
          CartesianCoordinate_removeStringToSymbols, &
          CartesianCoordinate_restoreSymbols,&
          CartesianCoordinate_saveSymbols, &
          CartesianCoordinate_changeSymbolOfAtom

     !< enum format CartesianCoordinate {
     integer, parameter, public :: CARTESIAN_COORDINATE_MATRIX_ONLY  = B'00000'
     integer, parameter, public :: CARTESIAN_COORDINATE_WITH_SYMBOL  = B'00001'
     integer, parameter, public :: CARTESIAN_COORDINATE_WITH_CHARGE  = B'00010'
     integer, parameter, public :: CARTESIAN_COORDINATE_WITH_MASS    = B'00100'
     integer, parameter, public :: CARTESIAN_COORDINATE_WITH_BASIS   = B'01000'
     integer, parameter, public :: CARTESIAN_COORDINATE_IN_ANGS      = B'10000'
     !< }

     private
contains

     subroutine CartesianCoordinate_constructor( this, coordinate, symbols, charge, mass, name )
          implicit none
          type(CartesianCoordinate) :: this
          type(Matrix) :: coordinate
          character(*) :: symbols(:)
          real(8),optional :: charge(:)
          real(8),optional :: mass(:)
          character(*), optional :: name

          integer :: ssize
          integer :: i,j
          integer, allocatable :: auxVec(:)

          ssize = size(symbols)

          allocate( this%atomSymbol(ssize) )
          allocate( this%atomSymbolBkp(ssize) )
          allocate( this%basisSet(ssize) )
          this%basisSet=""

          if(present(charge) ) then
                allocate( this%charge(ssize) )
                this%charge = charge
          end if

          if(present(mass) ) then
               allocate( this%mass(ssize) )
               this%mass = mass
          end if

          if( present(name) ) this%name=trim( adjustl(name) )

          this%atomSymbol=symbols
          this%atomSymbolBkp=symbols
          this%coordinate=coordinate

          this%numberOfQuantumNucleous=0
          this%quantumNucleiLocalization=""
          do i=1, ssize
               j=String_findSubstring(  trim( this%atomSymbol(i) ), "_" )
               if (j>0) then
!                    this%numberOfNucleones=inum( trim(this%atomSymbol(i)(j+1:j+3)) )
                    this%numberOfQuantumNucleous=this%numberOfQuantumNucleous+1
                    this%quantumNucleiLocalization=trim(this%quantumNucleiLocalization)//","//trim(String_convertIntegerToString(i))
               end if
          end do



     end subroutine CartesianCoordinate_constructor


     subroutine CartesianCoordinate_destructor( this )
          implicit none
          type(CartesianCoordinate) :: this

          call Matrix_destructor(this%coordinate)
          if ( allocated( this%atomSymbol ) ) deallocate( this%atomSymbol )
          if ( allocated( this%atomSymbolBkp ) ) deallocate( this%atomSymbolBkp )
          if ( allocated( this%charge ) ) deallocate( this%charge )
          if ( allocated( this%mass ) ) deallocate( this%mass )
          if ( allocated( this%basisSet ) ) deallocate( this%basisSet)

     end subroutine CartesianCoordinate_destructor


     subroutine CartesianCoordinate_write( this, unid, flags )
          implicit none
          type(CartesianCoordinate) :: this
          integer :: unid
          integer :: flags

          integer :: i

          do i = 1, size( this%coordinate%values, dim=1)

               if( IAND(flags,CARTESIAN_COORDINATE_WITH_SYMBOL) == CARTESIAN_COORDINATE_WITH_SYMBOL  ) &
                    write(unid,"(A$)") trim( this%atomSymbol(i) )//"  "

               if( IAND(flags,CARTESIAN_COORDINATE_WITH_CHARGE) == CARTESIAN_COORDINATE_WITH_CHARGE .and. &
                    allocated(this%charge) ) &
                    write(unid,"(F5.2,A2$)") this%charge(i), "  "

               if( IAND(flags,CARTESIAN_COORDINATE_WITH_MASS) == CARTESIAN_COORDINATE_WITH_MASS .and. &
                    allocated(this%mass) ) &
                    write(unid,"(F10.2,A2$)") this%mass(i), "  "

               if( IAND(flags,CARTESIAN_COORDINATE_WITH_BASIS) == CARTESIAN_COORDINATE_WITH_BASIS .and. &
                    allocated(this%basisSet) ) &
                    write(unid,"(A$)") trim(this%basisSet(i))//"  "

               if( IAND(flags,CARTESIAN_COORDINATE_IN_ANGS) == CARTESIAN_COORDINATE_IN_ANGS ) then
                    write(unid,"(<3>F15.10)") this%coordinate%values(i,:)*0.52917724924_8
               else
                    write(unid,"(<3>F15.10)") this%coordinate%values(i,:)
               end if

          end do 

     end subroutine CartesianCoordinate_write

     subroutine CartesianCoordinate_scaleCharge(this, scaleFactor )
          implicit none
          type(CartesianCoordinate) :: this
          real(8) :: scaleFactor

          this%charge= this%charge*scaleFactor

     end subroutine CartesianCoordinate_scaleCharge


     subroutine CartesianCoordinate_scaleMass(this, scaleFactor )
          implicit none
          type(CartesianCoordinate) :: this
          real(8) :: scaleFactor

          this%mass= this%mass*scaleFactor

     end subroutine CartesianCoordinate_scaleMass

     subroutine CartesianCoordinate_addPrefixToSymbols(this, prefix )
          implicit none
          type(CartesianCoordinate) :: this
          character(*) :: prefix

          integer :: i

          do i=1,size(this%atomSymbol)

               this%atomSymbol(i)=trim(adjustl(prefix))//trim(adjustl(this%atomSymbol(i)))

          end do



     end subroutine CartesianCoordinate_addPrefixToSymbols


     subroutine CartesianCoordinate_addSuffixToSymbols(this, suffix )
          implicit none
          type(CartesianCoordinate) :: this
          character(*) :: suffix

          integer :: i

          do i=1,size(this%atomSymbol)

               this%atomSymbol(i)=trim(adjustl(this%atomSymbol(i)))//trim(adjustl(suffix))

          end do


     end subroutine CartesianCoordinate_addSuffixToSymbols



     subroutine CartesianCoordinate_removeStringToSymbols(this, sstring )
          implicit none
          type(CartesianCoordinate) :: this
          character(*) :: sstring

          integer :: coincidence
          integer :: i,j

          j=scan(trim(adjustl(sstring)),"*")

          if ( j>0 ) then

	       do i=1,size(this%atomSymbol)
		    coincidence = String_findSubstring(trim(this%atomSymbol(i)), trim(adjustl(sstring(1:j-1))) )
                     if (coincidence > 0 ) this%atomSymbol(i)=this%atomSymbol(i)(1:coincidence-1)
	       end do

          else

               do i=1,size(this%atomSymbol)
		    coincidence = String_findSubstring(trim(this%atomSymbol(i)), trim(adjustl(sstring)) )

                    if (coincidence > 0 ) &
	                 this%atomSymbol(i)=this%atomSymbol(i)(1:coincidence-1)// &
                         this%atomSymbol(i)(coincidence+len_trim(sstring):len_trim(this%atomSymbol(i)))
	       end do

          end if

     end subroutine CartesianCoordinate_removeStringToSymbols


     subroutine CartesianCoordinate_restoreSymbols(this )
          implicit none
          type(CartesianCoordinate) :: this

          this%atomSymbol = this%atomSymbolBkp

     end subroutine CartesianCoordinate_restoreSymbols


     subroutine CartesianCoordinate_saveSymbols(this )
          implicit none
          type(CartesianCoordinate) :: this

          this%atomSymbolBkp = this%atomSymbol

     end subroutine CartesianCoordinate_saveSymbols


     subroutine CartesianCoordinate_changeSymbolOfAtom(this, symbol, atomNumber )
          implicit none
          type(CartesianCoordinate) :: this
          character(*) :: symbol
          integer :: atomNumber

          this%atomSymbol(atomNumber) = trim(symbol)

     end subroutine CartesianCoordinate_changeSymbolOfAtom


end module CartesianCoordinate_
