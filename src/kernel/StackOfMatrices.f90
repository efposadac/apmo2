!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para implementacion de pilas 
!
! Modulo para manejo de pilas de datos, esta implementacion esta basada en 
! una estructura de datos tipo lista enlazada. 
!
! @author Sergio A. Gonzalez
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
! @todo verificar la liberacion de memoria al eliminar nodos de la pila
!**
module StackOfMatrices_
	use Exception_
	use Matrix_
	implicit none
	
	
	type, public :: StackOfMatrices
		character(30) :: name
		type(Matrix), allocatable :: node(:)
		real(8), pointer :: current(:,:)
		integer :: iterator
		integer :: maxSize
		logical :: isIstanced
	end type StackOfMatrices
	
	
	public :: &
		StackOfMatrices_constructor, &
		StackOfMatrices_destructor, &
		StackOfMatrices_isInstanced, &
		StackOfMatrices_size, &
		StackOfMatrices_empty, &
		StackOfMatrices_push, &
		StackOfMatrices_pop, &
		StackOfMatrices_popFront
		

contains

	!**
	! @brief Define el constructor para la clase
	!**
	subroutine StackOfMatrices_constructor( this, name, ssize ) 
		implicit none
		type(StackOfMatrices), intent(inout) :: this
		character(*),optional :: name
		integer, optional, intent(in) :: ssize
	
		this.maxSize =10
		this.name = "undefined"
		
		if ( present(name) ) this.name = trim(name)
		if (present(ssize)) this.maxSize =ssize
		this.isIstanced =.true.
		
		
	end  subroutine StackOfMatrices_constructor

	!**
	! @brief Define el destructor para la clase
	!
	! @ Verificar que la memoria priviamente separada sea liberada
	!**
	subroutine StackOfMatrices_destructor( this ) 
		implicit none
		type(StackOfMatrices), intent(inout) :: this
		
		integer i
	
		if (allocated(this.node)) then
	
			this.current => null()
			
			do i=1,size(this.node)
	
				call Matrix_destructor( this.node(i) )
	
			end do
	
			deallocate( this.node )
			this.name=""
			this.iterator = 0
			this.maxSize =0
			this.isIstanced =.false.
		end if
		
	end  subroutine StackOfMatrices_destructor

	!**
	! @brief Indica si la pila esta instanciadad
	!**
	function StackOfMatrices_isInstanced( this) result(output)
		implicit none
		type(StackOfMatrices), intent(in) :: this
		logical :: output
				
		 output = this.isIstanced
		
	end function StackOfMatrices_isInstanced

	!**
	! @brief Indica si la pila tiene o no nodos
	!**
	function StackOfMatrices_empty( this) result(output)
		implicit none
		type(StackOfMatrices), intent(in) :: this
		logical :: output
	
		output=.false.
	
		if( .not.allocated(this.node) .or. this.iterator == 0 ) output =.true.
		
	end function StackOfMatrices_empty

	!**
	! @brief Retorna el tama�o actual de la pila
	!**
	function StackOfMatrices_size( this) result(output)
		implicit none
		type(StackOfMatrices),  intent(in) :: this
		integer :: output
	
		output=0
	
		if( allocated(this.node) ) output =size(this.node)
	
	end function StackOfMatrices_size


	!**
	! @brief Adiciona un nodeo a la pila
	!
	!**
	subroutine StackOfMatrices_push( this, node )
		implicit none
		type(StackOfMatrices), intent(inout), target :: this
		real(8),intent(in) :: node(:,:)
		
		type(Exception) :: ex
		type(Matrix), allocatable ::  auxStack(:)
		integer :: currentSize
		integer :: columns
		integer :: rows
		integer :: i
	
		rows =size( node, 1)
		columns =size( node, 2)
		
	
		if ( allocated(this.node) ) then
		
			currentSize = size(this.node)
			if(  currentSize < this.maxSize )	then
		
				allocate( auxStack( currentSize ) )
				this.current => null()
				
				do i=1, currentSize
					auxStack(i)=  this.node(i)
					call Matrix_destructor( this.node(i) )
				end do
				
				deallocate( this.node )
				allocate( this.node(currentSize+1) )
				this.iterator = currentSize+1
				
				do i=1, currentSize 
					this.node(i) = auxStack(i)
					call Matrix_destructor( auxStack(i) )
				end do
				
				deallocate( auxStack )
				call Matrix_constructor( this.node(currentSize + 1), int(rows,8), int(columns,8) ) 
				this.node(currentSize + 1).values=node
				this.current => this.node(currentSize + 1).values
				
			else
			
				allocate( auxStack( this.maxSize ) )
				this.current => null()
				
				do i=1, this.maxSize
					auxStack(i)=  this.node(i)
					call Matrix_destructor( this.node(i) )
				end do
				
				do i=2, this.maxSize
					this.node(i-1) = auxStack(i)
					call Matrix_destructor( auxStack(i) )
				end do
				call Matrix_destructor( auxStack(1) )
				
				deallocate( auxStack )
				call Matrix_constructor( this.node(this.maxSize), int(rows,8), int(columns,8) ) 
				this.node(this.maxSize).values=node
				this.current => this.node(this.maxSize ).values
			
			end if
		
 		else
		
			allocate( this.node(1) )
			call Matrix_constructor( this.node(1), int(rows,8), int(columns,8) ) 
			this.node(1).values=node
			this.current => this.node(1).values
			this.iterator = 1
		
		end if
	
	end subroutine StackOfMatrices_push

	!**
	! @brief Elimina el �ltimo nodeo de la pila
	!**
	subroutine StackOfMatrices_pop( this )
		implicit none
		type(StackOfMatrices), intent(inout) :: this
		
		type(Exception) :: ex
	
	end subroutine StackOfMatrices_pop

	!**
	! @brief Elimina el primer elemento de la pila
	!**
	subroutine StackOfMatrices_popFront( this )
		implicit none
		type(StackOfMatrices), intent(inout), target :: this
		
		type(Matrix), allocatable ::  auxStack(:)
		integer :: currentSize
		integer :: i
		
		currentSize = size(this.node)
		
		if(  currentSize >= 2 )	then
		
			allocate( auxStack( currentSize-1 ) )
			this.current => null()
			
			do i=2, currentSize
				auxStack(i-1)=  this.node(i)
				call Matrix_destructor( this.node(i) )
			end do

			call Matrix_destructor( this.node(1) )
			
			deallocate( this.node )
			allocate( this.node(currentSize-1) )
			this.iterator = currentSize-1
			currentSize=this.iterator

			do i=1, currentSize 
				this.node(i) = auxStack(i)
				call Matrix_destructor( auxStack(i) )
			end do
			
			deallocate( auxStack )
			
			this.current => this.node(currentSize).values
		
		end if	

	end subroutine StackOfMatrices_popFront



	!**
	! @brief Elimina el �ltimo nodeo de la pila
	!**
	function StackOfMatrices_getValuesPtr( this, iterator ) result(output)
		implicit none
		type(StackOfMatrices), intent(inout), target :: this
		integer, intent(in) :: iterator
		real(8), pointer :: output(:,:)
		
		output => null()
		output => this.node(iterator).values
	
	end function StackOfMatrices_getValuesPtr


end module StackOfMatrices_