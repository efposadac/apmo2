!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    thisPointer files is part of nonBOA                                                 !
!!                                                                                 !
!!    thisPointer program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    thisPointer program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with thisPointer program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Defincion de clase para mapas
! 
! Define una seudoclase que alamcena elementos promados por una combinaci�n clave-valor
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-09-03
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulo y m�todos
!
! @todo Implementar mapa mediante una arbol binario, emplenado punteros
!**
module Map_
	use APMO_
	use Exception_
	implicit none
	
	type, public :: Map
		
		character(30) :: name
		character(30), allocatable :: key(:)
		real(8) , allocatable :: value(:)
		integer :: first
		integer :: last
		
	end type Map
	
	type(Map), target, public :: currentMap
	
	public :: &
		Map_constructor , &
 		Map_destructor , &
		Map_show, & 
		Map_insert, &
		Map_erase, &
		Map_find, &
		Map_swap, &
		Map_getValue, &
		Map_getKey, &
		Map_size, &
		Map_begin, &
		Map_end, &
		Map_clear, &
		Map_isEmpty
	
contains

	!**
	! Define el constructor para la clase
	!
	!**
	subroutine Map_constructor( this, ssize, name )
		implicit none
		type(Map), intent(inout) :: this
		integer, optional :: ssize
		character(*), optional :: name
		
		integer :: auxSize
		
		auxSize = 0
		if ( present(ssize) ) auxSize = ssize
		if ( present( name) )  this.name = trim(name)
		
		if ( allocated( this.key ) ) deallocate( this.key )
		allocate( this.key( auxSize ) )
		
		if ( allocated( this.value ) ) deallocate( this.value )
		allocate( this.value( auxSize ) )
		
		
		this.first = 1
		this.last = auxSize
			
	end subroutine Map_constructor
	
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine Map_destructor( this)
		implicit none
		type(Map), intent(inout) :: this
		
		if ( allocated( this.key ) ) deallocate( this.key )
		if ( allocated( this.value ) ) deallocate( this.value )
		this.name = "undefined"
		this.first = 0
		this.last = 0

	end subroutine Map_destructor
	
	!**
	! Muestra el contenido del mapa especificado
	!
	!**
	subroutine Map_show( this)
		implicit none
		type(Map), intent(in) :: this
		
		integer :: i
		
		if ( allocated( this.key ) ) then
			print *,""
			print *,"===="
			print *,"Map: "
			print *,"===="
			print *,""
			write (6,"(T10,A15,F10.2)") ( ( this.key(i), this.value(i) ), i=1, this.last )
			print *,""
		
		end if

	end subroutine Map_show
	
	
	!**
	! Adiciona un elemento al mapa especificado
	!
	!**
	subroutine Map_insert( this, key, value )
		implicit none
		type(Map), intent(inout), target :: this
		character(*), intent(in) :: key
		real(8), intent(in) :: value
		
		type(Map) :: auxMap
		integer :: i
		logical :: auxLogical
		type(Exception) :: ex
		
		if ( Map_find( this, trim(key) ) == 0 ) then
		
			call Map_constructor( auxMap,  this.last )
			
			auxMap.key = this.key
			auxMap.value = this.value
			
			call Map_constructor( this,  this.last + 1 )
			
			this.key(1 : auxMap.last ) = auxMap.key
			this.value(1 : auxMap.last) = auxMap.value
		
			this.key( this.last ) = trim( key )
			this.value( this.last ) = value
		
			call Map_destructor( auxMap )

		else
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object Map in the insert() function" )
			call Exception_setDescription( ex, "The key: "//trim(key)//" already was defined " )
			call Exception_show( ex )
		
		end if
		
	end subroutine Map_insert
	
	!**
	! Borra un elemento del mapa
	!
	!**
	subroutine Map_erase( this, key, iterator )
		implicit none
		type(Map), intent(inout), target :: this
		character(*), intent(in), optional :: key
		integer, intent(in), optional :: iterator
		
		type(Map) :: auxMap
		integer :: i,j
		
		 
			
		if ( Map_find( this, trim(key) ) /= 0 ) then
			
			call Map_constructor( auxMap, this.last - 1)
			
			j=0
			do  i = 1, this.last
				if ( trim( this.key(i) ) /= trim( key ) ) then
					j=j+1
					auxMap.key(j) = this.key(i)
					auxMap.value(j) = this.value(i)
				end if
			end do
			
			call Map_constructor( this, this.last - 1)

			this.key = auxMap.key
			this.value = auxMap.value
			
			call Map_destructor( auxMap )
		
		end if
		
	end subroutine Map_erase
	
	!**
	! Invierte la psocion de dos elementos
	!
	!**
	subroutine Map_swap( this, keyA, keyB )
		implicit none
		type(Map), intent(inout), target :: this
		character(*), intent(in) :: keyA
		character(*), intent(in) :: keyB
		
		integer :: i
		integer :: j
		integer :: auxValue
		character(30) :: auxKey
		
		i = Map_find( this, trim(keyA) )
		J = Map_find( this, trim(keyB) )
			
		if ( ( i /= 0 ) .and. ( j /=0 )) then
			
			auxKey = trim( this.key(i))
			this.key(i) = trim( this.key(j) )
			this.key(j) = trim( auxKey )
			
			auxValue= this.value(i)
			this.value(i) = this.value(j)
			this.value(j) = auxValue
			
		end if
		
	end subroutine Map_swap
	
	!>
	!! @brief Busca un elemento del mapa y retorna su iterador
	!!
	!<
	function Map_find( this, key ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		character(*), intent(in), optional :: key
		integer :: output
		
		integer :: i
	
		do  i = 1, this.last
			if ( trim( this.key(i) ) == trim( key ) ) exit
		end do
		
		
		if ( i > this.last ) then
			output = 0
		else
			output = i
		end if
		
		
		
	end function Map_find
	
	!**
	! Busca un elemento del mapa y retorna su valor
	!
	!**
	function Map_getValue( this, key, iterator ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		character(*), intent(in), optional :: key
		integer, intent(in), optional :: iterator
		real(8) :: output

		integer :: i
		
		if ( present(key) ) then
			
			do  i = 1, this.last
				if ( trim( this.key(i) ) == trim( key ) ) exit
			end do
		
		else if ( present(iterator) ) then
			
			i= iterator
		
		end if
		
		if ( i >  this.last ) then
			output = this.value( this.last ) 
		else
			output = this.value( i )
		end if
		
		
	end function Map_getValue
	
	!**
	! @brief Retorna la clave aociada a un valor dado
	!
	!**
	function Map_getKey( this, value, iterator ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		real(8), intent(in), optional :: value
		integer, intent(in), optional :: iterator
		character(30) :: output
		
		integer :: i
		
		if ( present(value) ) then
			
			do  i = 1, this.last
				if (  abs(this.value(i) - value)  < APMO_instance.DOUBLE_ZERO_THRESHOLD ) exit
			end do
		
		else if ( present(iterator) ) then
			
			i= iterator
		
		end if
		
		if ( i >  this.last ) then
			output =  this.key( this.last)  
		else
			output = this.key( i )
		end if
		
		
	end function Map_getKey
	
	!**
	! Devuelve el tama�o del mapa
	!
	!**
	function Map_size( this ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		integer :: output
		
		output = this.last
		
	end function Map_size

	!**
	! Devuelve un iterador al primer elemento del mapa
	!
	!**
	function Map_begin( this ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		integer :: output
		
		output = this.first
		
	end function Map_begin
		
	!**
	! Devuelve un iterador al �ltimo elemento del mapa
	!
	!**
	function Map_end( this ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		integer :: output
		
		output = this.last
		
	end function Map_end
	
	!**
	! Limpiar el mapa 
	!
	!**
	subroutine Map_clear( this ) 
		implicit none
		type(Map), intent(inout), target :: this
		integer :: output
		
		call Map_destructor( this )
		
	end subroutine Map_clear
	
	!**
	! Indica si el arreglo esta vacio o no
	!
	!**
	function Map_isEmpty( this ) result( output )
		implicit none
		type(Map), intent(in), target :: this
		logical :: output
		
		output = .false.
		if ( ( this.last - this.first ) <= 0 )	output = .true.
		
	end function Map_isEmpty
	
	
end module Map_
