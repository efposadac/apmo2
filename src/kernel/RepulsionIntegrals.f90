!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief   Modulo para calculo de integrales de atraccion/repulsion cuanticas. 
!
! Este modulo define una seudoclase cuyos metodos devuelven la integral de atraccion 
! o repulsion entre part�culas cuanticas descrita mediante una distribuci�n 
! gausiana sin normalizar. El Calculo de la integral  no considera las cargas de las
! part�culas 
!
! \f[(\bf{ab},\bf{cd}) = \int {\int {{\varphi(\bf{r_1};{\bf{\zeta}}_a,\bf{n_a,R_A})} 
! {\varphi(\bf{r_1};{\bf{\zeta}}_b,\bf{n_b,R_B})} {\frac{1}{\mid \bf{r_1-r_2} \mid }}
! {\varphi(\bf{r_2};{\zeta}_c,\bf{n_c,R_C})} {\varphi(\bf{r_2};{\bf{\zeta}}_d,
! \bf{n_d,R_D})}  }\,dr_2}\,dr_1  \f]
!
! Donde:
!
! <table>
! <tr> <td> \f$ \zeta \f$ : <td>  <dfn> exponente orbital. </dfn>
! <tr> <td> \f$ r_1,r_2 \f$ : <td> <dfn> coordenas espaciales de la funci�n. </dfn>
! <tr> <td> \f$ n_a,n_b,n_c,n_d \f$ :<td> <dfn>indice de momento angular.</dfn>
! <tr> <td> \f$ R_A,R_B,R_C,R_D \f$ : <td> <dfn>origen de la funciones gaussianas cartesianas.</dfn>
! </table>
!
! Este tipo de integral corresponde a una integral de cuatro centros, calculada por 
! aproximaci�non n�merica, utilizando la integral auxiliar (ver FMath):
!
! \f[ F_m(U)= \int_{0}^{1} {t^{2m}e^{-Tt^2}}\,dt \f]
!
! La integral de repulsion-atraccion entre particulas cuanticas se calcula de 
! acuerdo metodo recursivo propuesto por Obara-Sayka. La expresion general 
! de la integral es: 
!
! \f[({\bf{a + 1_i b}}, {\bf{cd}})^{(m)} = \f] 
! \f[ (P_i -A_i) ({\bf{ab}},{\bf{cd}} )^{(m)} - (W_i - P_i)
!  ({\bf{ab}},{\bf{cd}} )^{(m+1)} \f]
! \f[ + \frac{1}{2 \zeta} N_i(\bf{a}) \left\{ ({\bf{a-1_i , b}},{\bf{cd}})^{(m)} -
!  {\frac{\rho}{\zeta}}({\bf{a-1_i ,b}},{\bf{cd}})^{(m+1)}  \right\}\f]
! \f[ + \frac{1}{2 \zeta} N_i(\bf{b}) \left\{ ({\bf{a,b-1_i}},{\bf{cd}})^{(m)} - 
! {\frac{\rho}{\zeta}}({\bf{a,b-1_i}},{\bf{cd}})^{(m+1)}  \right\}\f]
! \f[ + {\frac{1}{2(\zeta + \eta)}}{\bf{N_i(c)}}{(\bf{ab},\bf{dc-1_i})^{m+1}} + 
! {\frac{1}{2(\zeta + \eta)}}{\bf{N_i(d)}}{(\bf{ab},\bf{cd-1_i})^{m+1}}  \f] 
!
! Donde (m) es un entero no negativo que hace referencia al orden de la integral 
! dentro de la recursion. Los parametros <b> P, W,</b> \f$ \xi , \eta , \rho \f$
! y \f$ \zeta \f$ de la expresion provienen la  reducci�n de cuatro funciones 
! exponenciales a una �nica funci�n.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2006-06-15
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapta al estandar de codificacion propuesto.
!
! @see PrimitiveGaussian_, FMath , PrimitiveGaussian_product() , incompleteGamma()
!
! @todo Implementar integrales para funciones de momento angular 3.
!**
module RepulsionIntegrals_
	use PrimitiveGaussian_
	use Exception_
	
	implicit none
	
	
	private ::
		type(PrimitiveGaussian) :: primitiveAxB
		type(PrimitiveGaussian) :: primitiveCxD
		type(PrimitiveGaussian) :: primitiveAxBxCxD
		type(PrimitiveGaussian) :: g(4)
		real(8) :: reducedExponent_AB
		real(8) :: reducedExponent_CD
		real(8) :: reducedExponent_ABCD
 		real(8) :: incompleteGammaArgument

		
		real(8) :: incompletGamma_0
		real(8) :: incompletGamma_1
		real(8) :: incompletGamma_2
		real(8) :: incompletGamma_3
		real(8) :: incompletGamma_4
		real(8) :: incompletGamma_5
		real(8) :: incompletGamma_6
		real(8) :: incompletGamma_7
		real(8) :: incompletGamma_8
	
		real(8) :: overlapFactor
	
	public ::  &
		PrimitiveGaussian_repulsionIntegral
		
	private :: &
		RepulsionIntegrals_ssss, &
		RepulsionIntegrals_psss, &
		RepulsionIntegrals_psps, &
		RepulsionIntegrals_ppss, &
		RepulsionIntegrals_ppps, &
		RepulsionIntegrals_pppp, &
		RepulsionIntegrals_dsss, &
		RepulsionIntegrals_dsps, &
		RepulsionIntegrals_dspp, &
		RepulsionIntegrals_dsds, &
		RepulsionIntegrals_dpss, &
		RepulsionIntegrals_dpps, &
		RepulsionIntegrals_dppp, &
		RepulsionIntegrals_dpds, &
		RepulsionIntegrals_dpdp, &
		RepulsionIntegrals_ddss, &
		RepulsionIntegrals_ddps, &
		RepulsionIntegrals_ddpp, &
		RepulsionIntegrals_ddds, &
		RepulsionIntegrals_dddp, &
		RepulsionIntegrals_dddd, &
		RepulsionIntegrals_sumMoments
		
contains
	
	!**
	!  Retorna el valor de la integral <i> (ab,cd) </i> para cuatro 
	! funciones gaussianas sin normalizar de momento angular dado.
	!
	! @param primitiveA Gausiana primitiva
	! @param primitiveB Gausiana primitiva
	! @param primitiveC Gausiana primitiva
	! @param primitiveD Gausiana primitiva
	!
	! @return Valor de la integral de repulsion
	!**
	function PrimitiveGaussian_repulsionIntegral( primitiveA , primitiveB , primitiveC ,&
		primitiveD, order) result( output )
		implicit none
		type(PrimitiveGaussian) , intent(in) :: primitiveA
		type(PrimitiveGaussian) , intent(in) :: primitiveB
		type(PrimitiveGaussian) , intent(in) :: primitiveC
		type(PrimitiveGaussian) , intent(in) :: primitiveD
		integer, intent(in), optional :: order
		real(8) :: output
		
		integer:: caseIntegral
		integer :: intOrder
 		type(Exception) :: ex
		
		intOrder = 0
		if ( present(order) ) intOrder = order
		
		!! Ordena la gausianas de entrada de mayor a menor momento
		call PrimitiveGaussian_sort( g, primitiveA,primitiveB,primitiveC,primitiveD)
		
		!! Obtiene el producto de las gausianas de entrada
 		primitiveAxB = PrimitiveGaussian_product(g(1),g(2))
 		primitiveCxD = PrimitiveGaussian_product(g(3),g(4))
		primitiveAxBxCxD = PrimitiveGaussian_product(primitiveAxB,primitiveCxD)
		
		!! Calcula el exponente reducido
		reducedExponent_AB = PrimitiveGaussian_reducedOrbitalExponent( g(1) , g(2) )
		reducedExponent_CD = PrimitiveGaussian_reducedOrbitalExponent( g(3) , g(4) )
		reducedExponent_ABCD = PrimitiveGaussian_reducedOrbitalExponent( primitiveAxB , primitiveCxD )
		
		!!**********************************************************
		!! Evalua el argumento de la funci�n gamma incompleta 
		!!
		incompleteGammaArgument = reducedExponent_ABCD  * ( &
		( primitiveAxB.origin(1) - primitiveCxD.origin(1) )**2.0_8 + &
		( primitiveAxB.origin(2) - primitiveCxD.origin(2) )**2.0_8 + &
		( primitiveAxB.origin(3) - primitiveCxD.origin(3) )**2.0_8 )
		!!**********************************************************
		
		overlapFactor = sqrt( reducedExponent_ABCD / Math_PI ) &
			* (Math_PI/primitiveAxB.orbitalExponent)**1.5_8 &
			* PrimitiveGaussian_productConstant( g(1) , g(2) ) &
			* (Math_PI/primitiveCxD.orbitalExponent)**1.5_8 &
			* PrimitiveGaussian_productConstant( g(3) , g(4) )
		 
		if ( abs(overlapFactor) > APMO_instance.INTEGRAL_THRESHOLD ) then
		
			!!*******************************************************************
			!! Seleccion del metodo adecuado para el Calculo de la integral
			!! de acuedo con el momento angular de las funciones de entrada.
			!!
					
			!! Determina el procediento de la integral
			caseIntegral =  64 * PrimitiveGaussian_getAngularMoment(g(1)) + 16 * PrimitiveGaussian_getAngularMoment(g(2)) &
					+ 4 * PrimitiveGaussian_getAngularMoment(g(3)) +  PrimitiveGaussian_getAngularMoment(g(4))
					
			select case ( caseIntegral)
			
			case(0)
				output=RepulsionIntegrals_ssss()
			
			case(64)
				output=RepulsionIntegrals_psss()
		
			case(68)
				output=RepulsionIntegrals_psps()
			
			case(80)
				output=RepulsionIntegrals_ppss()
			
			case(84)
				output=RepulsionIntegrals_ppps()
		
			case(85)
				output=RepulsionIntegrals_pppp()
			
			case(128)
				output=RepulsionIntegrals_dsss()
				
			case(132)
				output=RepulsionIntegrals_dsps()
				
			case(133)
				output=RepulsionIntegrals_dspp()
			
			case(136)
				output=RepulsionIntegrals_dsds()
				
			case(144)
				output=RepulsionIntegrals_dpss()
				
			case(148)
				output=RepulsionIntegrals_dpps(intOrder)
			
			case(149)
				output=RepulsionIntegrals_dppp(intOrder)
				
			case(152)
				output=RepulsionIntegrals_dpds(intOrder)
			
			case(153)
				output=RepulsionIntegrals_dpdp(intOrder)
			
			case(160)
				output=RepulsionIntegrals_ddss(intOrder)
			
			case(164)
				output=RepulsionIntegrals_ddps(intOrder)
			
			case(165)
				output=RepulsionIntegrals_ddpp(intOrder)
			
			case(168)
				output=RepulsionIntegrals_ddds(intOrder)
				
			case(169)
				output=RepulsionIntegrals_dddp(intOrder)
			
			case(170)
				output=RepulsionIntegrals_dddd(intOrder)
			
			case default
				output = 0.0_8
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the repulsion function" )
				call Exception_setDescription( ex, "This angular moment  isn't implemented" )
				call Exception_show( ex )
								
			end select
			!!*******************************************************************
		else
			output = 0.0_8
		end if
		
  print*, "Integral sin normalizar", output
	
	end function PrimitiveGaussian_repulsionIntegral
	
	!**
	! Retorna el valor de la integral <i> (ss,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_ssss() result( output )
		implicit none
		real(8):: output

		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
						
		!! Calcula la integral de repulsion

		output = 2.0_8 * incompletGamma_0 * overlapFactor
		
	end function RepulsionIntegrals_ssss
	
	!**
	! Retorna el valor de la integral <i> (ps,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_psss() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		
		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		
		output = 2.0_8*(incompletGamma_1 * &
			( primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))&
			+ incompletGamma_0*(-g(1).origin(alpha) &
			+ primitiveAxB.origin(alpha))) * overlapFactor
		
	end function RepulsionIntegrals_psss
	
	!**
	! Retorna el valor de la integral <i> (ps,ps) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_psps() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		
		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		
		output = overlapFactor * ((incompletGamma_1 &
			*Math_kroneckerDelta(alpha, beta))/(primitiveCxD.orbitalExponent &
			+ primitiveAxB.orbitalExponent) + 2.0_8 *(-g(3).origin(beta) &
			+ primitiveCxD.origin(beta))*(incompletGamma_1 &
			* (primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) + incompletGamma_0 &
			*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) &
			+ 2.0_8*(primitiveAxBxCxD.origin(beta) &
			- primitiveCxD.origin(beta)) * (incompletGamma_2 &
			* (primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			+ incompletGamma_1*(-g(1).origin(alpha) &
			+ primitiveAxB.origin(alpha))))

	end function RepulsionIntegrals_psps
	
	!**
	! Retorna el valor de la integral <i> (pp,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_ppss() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		
		
		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		
		
		output = overlapFactor*(((primitiveAxB.orbitalExponent &
			*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1) &
			*Math_kroneckerDelta(alpha,beta)) / primitiveAxB.orbitalExponent**2.0_8 &
			+ 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			+ incompletGamma_1*(-g(1).origin(alpha) &
			+ primitiveAxB.origin(alpha))) &
			* (primitiveAxBxCxD.origin(beta) &
			- primitiveAxB.origin(beta)) &
			+ 2.0_8 * (incompletGamma_1*(primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) + incompletGamma_0 &
			* (-g(1).origin(alpha) + primitiveAxB.origin(alpha))) &
			*(-g(2).origin(beta) + primitiveAxB.origin(beta)))

	end function RepulsionIntegrals_ppss
	
	!**
	! Retorna el valor de la integral <i> (ds,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dsss() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		
		
		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		
		output = overlapFactor*(((primitiveAxB.orbitalExponent &
			* incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1) &
			* Math_kroneckerDelta(alpha,beta)) / primitiveAxB.orbitalExponent**2.0_8 &
			+ 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) + incompletGamma_1 &
			* (-g(1).origin(alpha) + primitiveAxB.origin(alpha))) &
			* (primitiveAxBxCxD.origin(beta) &
			- primitiveAxB.origin(beta)) +  2.0_8 * (incompletGamma_1 &
			* (primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) + incompletGamma_0 &
			* (-g(1).origin(alpha) + primitiveAxB.origin(alpha))) &
			* (-g(1).origin(beta) + primitiveAxB.origin(beta)))

	end function RepulsionIntegrals_dsss
	
	!**
	! Retorna el valor de la integral <i> (pp,ps) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_ppps() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		
		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		kappa= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )

		output = overlapFactor*((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
      primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2 + & 
    2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
      primitiveAxB.origin(beta))) + & 
  (incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
        primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
    incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
        primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
       (-g(2).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))
		
	end function RepulsionIntegrals_ppps
	
	
	!**
	! Retorna el valor de la integral <i> (dp,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dpss() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		
		
		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa= PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		
		output = overlapFactor*((Math_kroneckerDelta(beta, kappa)*& 
    (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
     reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
     incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
       (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
      primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
      primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
    (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
     reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
     incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
       (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)
		
	end function RepulsionIntegrals_dpss
	
	!**
	! Retorna el valor de la integral <i> (ds,ps) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dsps() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		
		
		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		output = overlapFactor*((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta))) + & 
  (incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
        primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
    incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
        primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))
		
	end function RepulsionIntegrals_dsps
	
	!**
	! Retorna el valor de la integral <i> (pp,pp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_pppp()  result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4)
		
		
		alpha = PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta  = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		kappa = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		output = overlapFactor*((Math_kroneckerDelta(kappa, lambda)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
      primitiveAxB.origin(beta)) - & 
    (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
          beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
         primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
         primitiveAxB.origin(beta))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (Math_kroneckerDelta(beta, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(alpha, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
      (incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))) + & 
   Math_kroneckerDelta(alpha, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(beta, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_1*(-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_2*(-g(2).origin(beta) + primitiveAxB.origin(beta)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (-g(4).origin(lambda) + primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(2).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_3*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(2).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))
		
	end function RepulsionIntegrals_pppp
	
	
	!**
	! Retorna el valor de la integral <i> (dp,ps) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dpps(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		output = (overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
        primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
        incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
        incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 overlapFactor*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
  ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
 overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)
			
	end function RepulsionIntegrals_dpps
	
	!**
	! Retorna el valor de la integral <i> (ds,pp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dspp() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		output = overlapFactor*((Math_kroneckerDelta(kappa, lambda)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta)) - & 
    (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
          beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
         primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
         primitiveAxB.origin(beta))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (Math_kroneckerDelta(beta, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(alpha, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
      (incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))) + & 
   Math_kroneckerDelta(alpha, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(beta, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (-g(4).origin(lambda) + primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_3*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))
	
	end function RepulsionIntegrals_dspp
	
	!**
	! Retorna el valor de la integral <i> (dd,ss) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_ddss(order) result( output )
		implicit none
		integer :: order 
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		
		output = overlapFactor*(((Math_kroneckerDelta(kappa, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
        primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(beta, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(alpha, lambda)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(beta, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
        incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
        incompletGamma_0*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent)))/(2.0_8*primitiveAxB.orbitalExponent) + & 
 ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
 ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
      primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
       primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
       primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(-g(2).origin(lambda) + primitiveAxB.origin(lambda)))
		
	end function RepulsionIntegrals_ddss
	
	!**
	! Retorna el valor de la integral <i> (ds,ds) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dsds() result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		
		output = overlapFactor*((Math_kroneckerDelta(kappa, lambda)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
    2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta)) - & 
    (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
          beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
         primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
         primitiveAxB.origin(beta))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (Math_kroneckerDelta(beta, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(alpha, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
      (incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))) + & 
   Math_kroneckerDelta(alpha, lambda)*& 
    ((incompletGamma_2*Math_kroneckerDelta(beta, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
     2.0_8*(-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
      (incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
     2.0_8*(primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_1*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
     2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
       primitiveAxB.origin(beta))) + & 
   (incompletGamma_3*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
     incompletGamma_2*(Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))
	
	end function RepulsionIntegrals_dsds
	
	
	!**
	! Retorna el valor de la integral <i> (dd,ps) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_ddps( order ) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0 + order )
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1 + order )
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2 + order )
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3 + order )
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4 + order )
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5 + order )
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		output = ((overlapFactor*Math_kroneckerDelta(kappa, lambda)*& 
    ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
         primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
         primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
         primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
         primitiveAxB.origin(beta))) + & 
     (incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(primitiveAxBxCxD.origin(alpha) - & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
          (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_1*& 
        (Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
         Math_kroneckerDelta(alpha, gamma)*(-g(1).origin(beta) + primitiveAxB.origin(beta))))/& 
      (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
          incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
  (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
  (overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
    ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + & 
     (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
          (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
       incompletGamma_1*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
           primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
          (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
     (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
  (overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
    ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(beta, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_0*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + & 
     (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
          (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
       incompletGamma_1*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
          (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
     (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
           (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
  2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
     (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
    overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*& 
   (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
  2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
     (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
    overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*& 
   (-g(2).origin(lambda) + primitiveAxB.origin(lambda)))/2.0_8
		
	end function RepulsionIntegrals_ddps
	
	
	!**
	! Retorna el valor de la integral <i> (dp,pp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dppp(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		gamma = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )	
		
		output = (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
   ((Math_kroneckerDelta(beta, kappa)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
       reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
         (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
       reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
       incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
         (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8 - & 
    (reducedExponent_ABCD*((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (-g(4).origin(gamma) + primitiveCxD.origin(gamma))*& 
  ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
    (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
   overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
 (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
  ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
    (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
   overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
 (overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
     ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*& 
           (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
            lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
        incompletGamma_2*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
    Math_kroneckerDelta(beta, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + & 
      (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(primitiveAxBxCxD.origin(alpha) - & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
         (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
    Math_kroneckerDelta(alpha, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + & 
      (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
           (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
         (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
           (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) 


	end function RepulsionIntegrals_dppp
		
	!**
	! Retorna el valor de la integral <i> (dp,ds) </i> 
	!
	! @return Valor de la integral de repulsion
	!**
	function  RepulsionIntegrals_dpds( order ) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0 + order )
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1 + order )
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2 + order )
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3 + order )
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4 + order )
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5 + order )
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )	
		
		output = (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
   ((Math_kroneckerDelta(beta, kappa)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
       reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
       incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
         (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
        primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
       reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
       incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
         (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8 - & 
    (reducedExponent_ABCD*((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
  ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
    (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
   overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
 (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
  ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
    (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
   overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
    ((Math_kroneckerDelta(beta, kappa)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
        incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
        primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
         primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
         primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
       (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
        reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
        incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
          (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
 (overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
     ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*& 
           (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
            lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
        incompletGamma_2*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
    Math_kroneckerDelta(beta, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + & 
      (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(primitiveAxBxCxD.origin(alpha) - & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
         (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
            primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
           (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
    Math_kroneckerDelta(alpha, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + & 
      (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
           (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
         (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
           (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) 
	end function RepulsionIntegrals_dpds
	
	!**
	! Retorna el valor de la integral <i> (dp,dp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**	
	function  RepulsionIntegrals_dpdp(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
				
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5+order)
		incompletGamma_6 = Math_incompletGamma( incompleteGammaArgument,6+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		iota  = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )	
		
				
		output = (-g(4).origin(iota) + primitiveCxD.origin(iota))*& 
  ((overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8 - & 
      (reducedExponent_ABCD*((Math_kroneckerDelta(beta, kappa)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
   (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
    ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
    ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
       ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
          incompletGamma_2*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
      Math_kroneckerDelta(beta, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              lambda)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_2*(Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
      Math_kroneckerDelta(alpha, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
           (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))) + & 
 (primitiveAxBxCxD.origin(iota) - primitiveCxD.origin(iota))*& 
  ((overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8 - & 
      (reducedExponent_ABCD*((Math_kroneckerDelta(beta, kappa)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
             (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*& 
             (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
   (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
    ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
    ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_5*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_4*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_4 - reducedExponent_ABCD*incompletGamma_5)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_6*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_5*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_5*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_4*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
       ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + (incompletGamma_4*(Math_kroneckerDelta(beta, lambda)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
          incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
      Math_kroneckerDelta(beta, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(kappa, lambda)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              lambda)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
      Math_kroneckerDelta(alpha, gamma)*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(kappa, lambda)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_3*& 
           (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))) + & 
 (Math_kroneckerDelta(iota, kappa)*& 
    ((overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
          primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
          primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
             Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
           2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
              (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*& 
              (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
     (overlapFactor*Math_kroneckerDelta(beta, gamma)*& 
        ((incompletGamma_3*Math_kroneckerDelta(alpha, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
         2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))) + & 
       overlapFactor*Math_kroneckerDelta(alpha, gamma)*& 
        ((incompletGamma_3*Math_kroneckerDelta(beta, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_3*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_2*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_4*(Math_kroneckerDelta(beta, lambda)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             lambda)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_3*(Math_kroneckerDelta(beta, lambda)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))) + & 
   Math_kroneckerDelta(beta, iota)*& 
    ((overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
             Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
           2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
              (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
     (overlapFactor*Math_kroneckerDelta(gamma, kappa)*& 
        ((incompletGamma_3*Math_kroneckerDelta(alpha, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
         2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))) + & 
       overlapFactor*Math_kroneckerDelta(alpha, gamma)*& 
        ((incompletGamma_3*Math_kroneckerDelta(kappa, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
     overlapFactor*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             lambda)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_2*(Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(kappa, lambda)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             lambda)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, lambda)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))) + & 
   Math_kroneckerDelta(alpha, iota)*& 
    ((overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
          incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
             Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
           2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_3*& 
              (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
              (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
     (overlapFactor*Math_kroneckerDelta(gamma, kappa)*& 
        ((incompletGamma_3*Math_kroneckerDelta(beta, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_3*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)))) + overlapFactor*Math_kroneckerDelta(beta, gamma)*& 
        ((incompletGamma_3*Math_kroneckerDelta(kappa, lambda))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
          (incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
     overlapFactor*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(kappa, lambda)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
          (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(kappa, lambda)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_3*& 
          (Math_kroneckerDelta(kappa, lambda)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, lambda)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + & 
 (Math_kroneckerDelta(iota, lambda)*& 
    ((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) - & 
     (reducedExponent_ABCD*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
                beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(& 
                beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
               primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
                kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(& 
                kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
               primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
                kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
               incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(& 
                kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
               incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + primitiveAxB.origin(& 
                kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
         (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
              (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*& 
                g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
              (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*& 
                g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
          primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
              (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*& 
                g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
              (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*& 
                g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)))/primitiveCxD.orbitalExponent) + Math_kroneckerDelta(gamma, iota)*& 
    ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
      (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) - & 
     (reducedExponent_ABCD*((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
                beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(& 
                beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
               primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, lambda)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
                kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(& 
                kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
               incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
               primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, lambda)*& 
            (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
                kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
               incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(& 
                kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
               incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + primitiveAxB.origin(& 
                kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
         (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
         ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + & 
               primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(& 
                alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*& 
                primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
          (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*(& 
                -g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
              (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*& 
                g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(lambda) - & 
          primitiveCxD.origin(lambda))*((Math_kroneckerDelta(beta, kappa)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
              (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*& 
                g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
            (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
              (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*& 
                g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
                primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent)
	end function RepulsionIntegrals_dpdp
	
	!**
	! Retorna el valor de la integral <i> (dd,pp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**	
	function  RepulsionIntegrals_ddpp(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5+order)
		incompletGamma_6 = Math_incompletGamma( incompleteGammaArgument,6+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		iota  = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
			
		output = ((-g(4).origin(iota) + primitiveCxD.origin(iota))*& 
   ((overlapFactor*Math_kroneckerDelta(kappa, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_1*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
            incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
    (overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_1*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
         2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_0*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_1*& 
          (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
                primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
 ((primitiveAxBxCxD.origin(iota) - primitiveCxD.origin(iota))*& 
   ((overlapFactor*Math_kroneckerDelta(kappa, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (incompletGamma_4*(Math_kroneckerDelta(beta, gamma)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
            incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
    (overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
          (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
                primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_5*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_4*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_4 - reducedExponent_ABCD*incompletGamma_5)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_6*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_5*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_5*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_4*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
 (Math_kroneckerDelta(gamma, iota)*& 
   ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*(& 
                -g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(beta, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*(& 
                -g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(alpha, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_0*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent)))/(2.0_8*primitiveAxB.orbitalExponent) + overlapFactor*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*& 
     (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + overlapFactor*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*& 
     (-g(2).origin(lambda) + primitiveAxB.origin(lambda)) - & 
    (reducedExponent_ABCD*((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
                  (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
                 primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
                 incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
                 primitiveAxB.origin(beta))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(beta, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
                  (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
                 primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
                 incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
                (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent) + & 
          Math_kroneckerDelta(alpha, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_3*& 
                  (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
                 primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
                 incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
                 primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent)))/(2.0_8*primitiveAxB.orbitalExponent) + overlapFactor*& 
        ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + & 
              primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
              primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
         (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
             Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
           2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
              (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
       overlapFactor*((Math_kroneckerDelta(beta, kappa)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/primitiveCxD.orbitalExponent))/& 
  (2.0_8*primitiveCxD.orbitalExponent) + (Math_kroneckerDelta(iota, lambda)*& 
    ((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (overlapFactor*Math_kroneckerDelta(beta, iota)*& 
     ((Math_kroneckerDelta(kappa, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(alpha, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - & 
             primitiveAxB.origin(alpha)) + incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(alpha, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - & 
                primitiveAxB.origin(alpha)) + incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - & 
                primitiveAxB.origin(alpha)) + incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + (Math_kroneckerDelta(gamma, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + (Math_kroneckerDelta(alpha, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + 2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
      2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
   (overlapFactor*Math_kroneckerDelta(alpha, iota)*& 
     ((Math_kroneckerDelta(kappa, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(beta, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) - (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(beta, & 
               gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + 2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
             (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(gamma) - & 
              primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
      (Math_kroneckerDelta(gamma, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + (Math_kroneckerDelta(beta, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + 2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_3*& 
           (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
      2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
          2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
           (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
   Math_kroneckerDelta(iota, kappa)*& 
    ((overlapFactor*(Math_kroneckerDelta(gamma, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
             lambda))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
            primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
            primitiveAxB.origin(lambda))) + Math_kroneckerDelta(alpha, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, lambda))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(lambda) - & 
            primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(lambda) + & 
            primitiveAxB.origin(lambda)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, lambda)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, lambda))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, lambda))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(lambda) + primitiveAxB.origin(lambda)) + & 
          reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(lambda) + primitiveAxB.origin(lambda)) + & 
          incompletGamma_2*(reducedExponent_ABCD*g(2).origin(lambda) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(lambda) - & 
            (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(lambda))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, lambda)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, lambda))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, lambda))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(lambda) + primitiveAxB.origin(lambda)) + & 
          reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(lambda) + primitiveAxB.origin(lambda)) + & 
          incompletGamma_3*(reducedExponent_ABCD*g(2).origin(lambda) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(lambda) - & 
            (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(lambda))))/primitiveAxB.orbitalExponent**2.0_8)))/(2.0_8*primitiveAxBxCxD.orbitalExponent) 


	end function RepulsionIntegrals_ddpp
	
	!**
	! Retorna el valor de la integral <i> (dd,ds) </i> 
	!
	! @return Valor de la integral de repulsion
	!**	
	function  RepulsionIntegrals_ddds(order) result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		integer :: order
		
		
		incompletGamma_0 = Math_incompletGamma( incompleteGammaArgument,0+order)
		incompletGamma_1 = Math_incompletGamma( incompleteGammaArgument,1+order)
		incompletGamma_2 = Math_incompletGamma( incompleteGammaArgument,2+order)
		incompletGamma_3 = Math_incompletGamma( incompleteGammaArgument,3+order)
		incompletGamma_4 = Math_incompletGamma( incompleteGammaArgument,4+order)
		incompletGamma_5 = Math_incompletGamma( incompleteGammaArgument,5+order)
		incompletGamma_6 = Math_incompletGamma( incompleteGammaArgument,6+order)
		
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		
		output = ((-g(3).origin(iota) + primitiveCxD.origin(iota))*& 
   ((overlapFactor*Math_kroneckerDelta(kappa, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_1*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
            incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
    (overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_1*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
           Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
         2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_0*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_1*& 
          (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
                primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
 ((primitiveAxBxCxD.origin(iota) - primitiveCxD.origin(iota))*& 
   ((overlapFactor*Math_kroneckerDelta(kappa, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta))) + (incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
         incompletGamma_2*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))) + (incompletGamma_4*(Math_kroneckerDelta(beta, gamma)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + & 
            incompletGamma_3*(Math_kroneckerDelta(beta, gamma)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(gamma, lambda)*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
    (overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
             gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
         incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
                primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    (overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
      ((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
         2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
            (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
            (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
          (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
            (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) - & 
       (reducedExponent_ABCD*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*(& 
                primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, & 
                gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
            incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
                primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*(& 
                -g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_5*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_4*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_4 - reducedExponent_ABCD*incompletGamma_5)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_6*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_5*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_5*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_4*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
    2.0_8*((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
             primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
            primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
             incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
       (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
       ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + & 
             primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
             (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
        (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + overlapFactor*(primitiveAxBxCxD.origin(gamma) - & 
        primitiveCxD.origin(gamma))*((Math_kroneckerDelta(beta, kappa)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
          (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
            (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
            (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8))*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
 (Math_kroneckerDelta(gamma, iota)*& 
   ((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, beta))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
           primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
           primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*(& 
                -g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(beta, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_2*(& 
                -g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(alpha, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_0*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
              Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
            2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent)))/(2.0_8*primitiveAxB.orbitalExponent) + overlapFactor*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*& 
     (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + overlapFactor*& 
     ((Math_kroneckerDelta(beta, kappa)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_1*(reducedExponent_ABCD*g(1).origin(alpha) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_0 - reducedExponent_ABCD*incompletGamma_1)*& 
          Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
        2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_1*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_0*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, kappa))/& 
         primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
          primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
          primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
        (primitiveAxB.orbitalExponent*incompletGamma_0*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_2*& 
          (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_1*(reducedExponent_ABCD*g(2).origin(kappa) + & 
           primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*& 
     (-g(2).origin(lambda) + primitiveAxB.origin(lambda)) - & 
    (reducedExponent_ABCD*((overlapFactor*(Math_kroneckerDelta(kappa, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
              primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(alpha, beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
                  (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
                 primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
                 incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
                 primitiveAxB.origin(beta))))/primitiveAxB.orbitalExponent) + Math_kroneckerDelta(beta, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
              incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
                  (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
                 primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
                 incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
                (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent) + & 
          Math_kroneckerDelta(alpha, lambda)*& 
           (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(beta, & 
               kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
              primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_1*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
              primitiveAxB.origin(kappa)) - (reducedExponent_ABCD*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
                 Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*& 
                (incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_3*& 
                  (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
                 primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
                 incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
                 primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent)))/(2.0_8*primitiveAxB.orbitalExponent) + overlapFactor*& 
        ((Math_kroneckerDelta(beta, kappa)*(primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(alpha) + & 
              primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
              primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
         (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
             Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
           2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_3*& 
              (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
       overlapFactor*((Math_kroneckerDelta(beta, kappa)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
              kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
             incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
             primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
           (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
             (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*& 
             (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(& 
                kappa))))/primitiveAxB.orbitalExponent**2.0_8)*(-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/primitiveCxD.orbitalExponent))/& 
  (2.0_8*primitiveCxD.orbitalExponent) + (Math_kroneckerDelta(iota, lambda)*& 
    ((overlapFactor*(Math_kroneckerDelta(gamma, kappa)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + Math_kroneckerDelta(alpha, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_2*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, kappa)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_3*(reducedExponent_ABCD*g(2).origin(kappa) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)) + & 
   (overlapFactor*Math_kroneckerDelta(beta, iota)*& 
     ((Math_kroneckerDelta(kappa, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(alpha, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - & 
             primitiveAxB.origin(alpha)) + incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(alpha, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - & 
                primitiveAxB.origin(alpha)) + incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - & 
                primitiveAxB.origin(alpha)) + incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + (Math_kroneckerDelta(gamma, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + (Math_kroneckerDelta(alpha, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + 2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
      2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(alpha, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, & 
              gamma)*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + & 
          incompletGamma_2*(Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(alpha) + & 
              primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
   (overlapFactor*Math_kroneckerDelta(alpha, iota)*& 
     ((Math_kroneckerDelta(kappa, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(beta, gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
          (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(-g(1).origin(beta) + & 
             primitiveAxB.origin(beta))) - (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(beta, & 
               gamma))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + 2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
             (incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*(& 
                -g(1).origin(beta) + primitiveAxB.origin(beta))) + 2.0_8*(primitiveAxBxCxD.origin(gamma) - & 
              primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
              incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
      (Math_kroneckerDelta(gamma, lambda)*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
           primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
           incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
           primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + (Math_kroneckerDelta(beta, lambda)*& 
        ((incompletGamma_2*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
         2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
          (incompletGamma_2*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
           incompletGamma_1*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
         2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
             primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) - & 
         (reducedExponent_ABCD*((incompletGamma_3*Math_kroneckerDelta(gamma, kappa))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent) + & 
            2.0_8*(-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(incompletGamma_3*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_2*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
            2.0_8*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*(incompletGamma_4*(primitiveAxBxCxD.origin(kappa) - & 
                primitiveAxB.origin(kappa)) + incompletGamma_3*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)))))/& 
          primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + 2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_4*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_4*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_3*& 
           (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda)) + & 
      2.0_8*((-g(3).origin(gamma) + primitiveCxD.origin(gamma))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
            Math_kroneckerDelta(beta, kappa))/primitiveAxB.orbitalExponent**2.0_8 + & 
          2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_2*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
          2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_1*& 
             (-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, kappa))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(kappa) - & 
            primitiveAxB.origin(kappa)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(kappa) + & 
            primitiveAxB.origin(kappa))) + (incompletGamma_3*(Math_kroneckerDelta(gamma, kappa)*& 
             (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))) + incompletGamma_2*& 
           (Math_kroneckerDelta(gamma, kappa)*(-g(1).origin(beta) + & 
              primitiveAxB.origin(beta)) + Math_kroneckerDelta(beta, gamma)*& 
             (-g(2).origin(kappa) + primitiveAxB.origin(kappa))))/(primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*& 
       (-g(2).origin(lambda) + primitiveAxB.origin(lambda))))/2 + & 
   Math_kroneckerDelta(iota, kappa)*& 
    ((overlapFactor*(Math_kroneckerDelta(gamma, lambda)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, beta))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(beta) - & 
            primitiveAxB.origin(beta)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
            primitiveAxB.origin(beta))) + Math_kroneckerDelta(beta, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, & 
             lambda))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
            primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
            incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
            primitiveAxB.origin(lambda))) + Math_kroneckerDelta(alpha, gamma)*& 
         (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(beta, lambda))/& 
           primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_3*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(primitiveAxBxCxD.origin(lambda) - & 
            primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
            incompletGamma_2*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*(-g(2).origin(lambda) + & 
            primitiveAxB.origin(lambda)))))/(2.0_8*primitiveAxBxCxD.orbitalExponent) + overlapFactor*& 
      (-g(3).origin(gamma) + primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, lambda)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_3*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_2*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_1 - reducedExponent_ABCD*incompletGamma_2)*& 
           Math_kroneckerDelta(alpha, lambda))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_2*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_1*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*Math_kroneckerDelta(alpha, lambda))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_1*(-g(2).origin(lambda) + primitiveAxB.origin(lambda)) + & 
          reducedExponent_ABCD*incompletGamma_3*(-primitiveAxBxCxD.origin(lambda) + primitiveAxB.origin(lambda)) + & 
          incompletGamma_2*(reducedExponent_ABCD*g(2).origin(lambda) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(lambda) - & 
            (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(lambda))))/primitiveAxB.orbitalExponent**2.0_8) + & 
     overlapFactor*(primitiveAxBxCxD.origin(gamma) - primitiveCxD.origin(gamma))*& 
      ((Math_kroneckerDelta(beta, lambda)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_4*& 
           (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_3*(reducedExponent_ABCD*g(1).origin(alpha) + & 
            primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + & 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))*(((primitiveAxB.orbitalExponent*incompletGamma_2 - reducedExponent_ABCD*incompletGamma_3)*& 
           Math_kroneckerDelta(alpha, lambda))/primitiveAxB.orbitalExponent**2.0_8 + & 
         2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_3*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_2*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (((primitiveAxB.orbitalExponent*incompletGamma_3 - reducedExponent_ABCD*incompletGamma_4)*Math_kroneckerDelta(alpha, lambda))/& 
          primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_5*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_4*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(primitiveAxBxCxD.origin(lambda) - & 
           primitiveAxB.origin(lambda)) + 2.0_8*(incompletGamma_4*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
           incompletGamma_3*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(lambda) + & 
           primitiveAxB.origin(lambda))) + (Math_kroneckerDelta(alpha, beta)*& 
         (primitiveAxB.orbitalExponent*incompletGamma_2*(-g(2).origin(lambda) + primitiveAxB.origin(lambda)) + & 
          reducedExponent_ABCD*incompletGamma_4*(-primitiveAxBxCxD.origin(lambda) + primitiveAxB.origin(lambda)) + & 
          incompletGamma_3*(reducedExponent_ABCD*g(2).origin(lambda) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(lambda) - & 
            (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(lambda))))/primitiveAxB.orbitalExponent**2.0_8)))/(2.0_8*primitiveAxBxCxD.orbitalExponent) 


	end function RepulsionIntegrals_ddds
	
	!**
	! Retorna el valor de la integral <i> (dd,dp) </i> 
	!
	! @return Valor de la integral de repulsion
	!**	
	function  RepulsionIntegrals_dddp(order) result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		integer :: sigma
		integer :: order
		real(8) :: auxFactor
		
				
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		sigma = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		!! Calculo de factores aditivos 
		g(4).angularMomentIndex(sigma) = 0
		output = ( primitiveCxD.origin(sigma) - g(4).origin(sigma) ) &
			* RepulsionIntegrals_ddds(order+0) + (primitiveAxBxCxD.origin(sigma) &
			- primitiveCxD.origin(sigma) ) * RepulsionIntegrals_ddds(order+1)
		
		if ( abs( Math_kroneckerDelta(sigma,iota) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) - 1
			
			output = output + 0.5_8 / primitiveCxD.orbitalExponent &
				* ( RepulsionIntegrals_ddps(order+0) &
				- (reducedExponent_ABCD/ primitiveCxD.orbitalExponent) &
				* RepulsionIntegrals_ddps(order+1) )
			
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) + 1
			
			
		end if 
		
		if ( abs( Math_kroneckerDelta(sigma,gamma) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
			
			output = output + 0.5_8 / primitiveCxD.orbitalExponent &
				* ( RepulsionIntegrals_ddps(order+0) &
				- (reducedExponent_ABCD/ primitiveCxD.orbitalExponent) &
				* RepulsionIntegrals_ddps(order+1) )
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
	
		end if 
		
		auxFactor = 0.0_8
		
		if ( abs( Math_kroneckerDelta(sigma,lambda) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
			
			auxFactor = RepulsionIntegrals_dpds(order+1) 
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1

		end if
		
		if ( abs( Math_kroneckerDelta(sigma,kappa) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpds(order+1) 
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1

		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
		
		if ( abs( Math_kroneckerDelta(sigma,beta) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpds(order+1) 
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1

		end if
		
		if ( abs( Math_kroneckerDelta(sigma,alpha) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpds(order+1) 
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1

		end if
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

		call PrimitiveGaussian_interchange( g(1), g(2) )
		g(4).angularMomentIndex(sigma) = 1

	end function RepulsionIntegrals_dddp
	
	!**
	! Retorna el valor de la integral <i> (dd,dd) </i> 
	!
	! @return Valor de la integral de repulsion
	!**	
	function  RepulsionIntegrals_dddd(order) result( output )
		implicit none
		real(8) :: output
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		integer :: sigma
		integer :: mu
		integer :: order
		real(8) :: auxFactor
		
				
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		sigma = PrimitiveGaussian_getDMomentComponent( g(4).angularMomentIndex, 1 )
		mu    = PrimitiveGaussian_getDMomentComponent( g(4).angularMomentIndex, 2 )
		
		!! Calculo de factores aditivos 
		g(4).angularMomentIndex(mu) = g(4).angularMomentIndex(mu) - 1 
		output = ( primitiveCxD.origin(mu) - g(4).origin(mu) ) &
			* RepulsionIntegrals_dddp(order+0) + (primitiveAxBxCxD.origin(mu) &
			- primitiveCxD.origin(mu) ) * RepulsionIntegrals_dddp(order+1)
		
		if ( abs( Math_kroneckerDelta(mu,sigma) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(4).angularMomentIndex(sigma) = g(4).angularMomentIndex(sigma) - 1
			
			output = output + 0.5_8 / primitiveCxD.orbitalExponent &
				* ( RepulsionIntegrals_ddds(order+0) &
				- (reducedExponent_ABCD/ primitiveCxD.orbitalExponent) &
				* RepulsionIntegrals_ddds(order+1) )
			
			g(4).angularMomentIndex(sigma) = g(4).angularMomentIndex(sigma) + 1
			
		end if
		
		if ( abs( Math_kroneckerDelta(mu,iota) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) - 1
			
			output = output + 0.5_8 / primitiveCxD.orbitalExponent &
				* ( RepulsionIntegrals_ddpp(order+0) &
				- (reducedExponent_ABCD/ primitiveCxD.orbitalExponent) &
				* RepulsionIntegrals_ddpp(order+1) )
			
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) + 1
			
		end if
		
		if ( abs( Math_kroneckerDelta(mu,gamma) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
			
			output = output + 0.5_8 / primitiveCxD.orbitalExponent &
				* ( RepulsionIntegrals_ddpp(order+0) &
				- (reducedExponent_ABCD/ primitiveCxD.orbitalExponent) &
				* RepulsionIntegrals_ddpp(order+1) )
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
		end if

		auxFactor = 0.0_8
		
		if ( abs( Math_kroneckerDelta(mu,lambda) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
			
			auxFactor = RepulsionIntegrals_dpdp(order+1) 
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1

		end if
		
		if ( abs( Math_kroneckerDelta(mu,kappa) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpdp(order+1) 
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1

		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
		
		if ( abs( Math_kroneckerDelta(mu,beta) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpdp(order+1) 
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1

		end if

		if ( abs( Math_kroneckerDelta(mu,alpha) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionIntegrals_dpdp(order+1) 
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1

		end if
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

		call PrimitiveGaussian_interchange( g(1), g(2) )
		g(4).angularMomentIndex(mu) = g(4).angularMomentIndex(mu) + 1
		
	end function RepulsionIntegrals_dddd
		
	!**
	! Devuelve la suma de los momentos angulares para todas 
	! cuantro gausinas  de entrada, en este modulo tiene como objeto
	! verificar que los momentos de las gausianas sean los adecuados
	!**
	function RepulsionIntegrals_sumMoments( primitives ) result( output )
		implicit none
		type(PrimitiveGaussian):: primitives(4)
		integer :: output
	
		output = sum(   abs( primitives(1).angularMomentIndex ) ) &
				+ sum( abs( primitives(2).angularMomentIndex ) ) &
				+ sum( abs( primitives(3).angularMomentIndex ) ) &
				+ sum( abs( primitives(4).angularMomentIndex ) )
	end function RepulsionIntegrals_sumMoments
	
end module RepulsionIntegrals_
