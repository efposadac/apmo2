!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module Supramolecular_
     use CartesianCoordinate_
     use ParticleManager_
     implicit none


     !>
     !! @brief define una clase para definir matrices de conectividad 
     !!   de estruturas supramoleculares
     !<
     type, public :: Supramolecular
          character(20) :: name
          type(CartesianCoordinate) :: supramolecule
          type(CartesianCoordinate), allocatable :: fragments(:)
          integer :: numberOfFragments
          real(8) :: complexEnergy
     end type


contains

     subroutine Supramolecular_constructor( this )
          implicit none
          type(Supramolecular) :: this

          integer :: i,j,k

          this.numberOfFragments = ParticleManager_getNumberOfFragments()

          call CartesianCoordinate_constructor( this.supramolecule, &
               ParticleManager_getCartesianMatrixOfCentersOfOptimization(), &
               ParticleManager_getLabelsOfCentersOfOptimization(), &
               ParticleManager_getChargesOfCentersOfOptimization(),&
               name=trim(APMO_instance.INPUT_FILE) )

               do j=1,size(this.supramolecule.atomSymbol)

                    k=scan(this.supramolecule.atomSymbol(j),"_")
                    if( k > 0) then
                         this.supramolecule.basisSet(j)=trim(ParticleManager_getBaseNameForSpecie( &
                                trim(adjustl(this.supramolecule.atomSymbol(j))) ))
                    else
                         this.supramolecule.basisSet(j)="dirac"
                    end if

               end do



          allocate( this.fragments( this.numberOfFragments ) )

          do i=1,this.numberOfFragments

               call CartesianCoordinate_constructor( this.fragments(i), &
                    ParticleManager_getCartesianMatrixOfCentersOfOptimization(fragmentNumber=i), &
                    ParticleManager_getLabelsOfCentersOfOptimization(fragment=i), &
                    ParticleManager_getChargesOfCentersOfOptimization(fragment=i), &
                    name=trim(APMO_instance.INPUT_FILE)//trim(adjustl(trim(String_convertIntegerToString(i)))) )


                    do j=1,size(this.fragments(i).atomSymbol)

                         k=scan(this.fragments(i).atomSymbol(j),"_")
                         if( k > 0) then
                              this.fragments(i).basisSet(j)=trim(ParticleManager_getBaseNameForSpecie( &
                                     trim(adjustl(this.fragments(i).atomSymbol(j))) ))
                         else
                              this.fragments(i).basisSet(j)="dirac"
                         end if

                    end do

          end do

     end subroutine Supramolecular_constructor


     subroutine Supramolecular_destructor( this )
          implicit none
          type(Supramolecular) :: this

          integer :: i

          call CartesianCoordinate_destructor( this.supramolecule )

          do i=1,this.numberOfFragments
               call CartesianCoordinate_destructor( this.fragments(i) )
          end do

     end subroutine Supramolecular_destructor

     subroutine Supramolecular_setCoordinates( this )
          implicit none
          type(Supramolecular) :: this

          integer :: i

          this.supramolecule.coordinate = ParticleManager_getCartesianMatrixOfCentersOfOptimization()

          do i=1,this.numberOfFragments
               this.fragments(i).coordinate=ParticleManager_getCartesianMatrixOfCentersOfOptimization(fragmentNumber=i)
          end do

     end subroutine Supramolecular_setCoordinates

     subroutine Supramolecular_writeFragment(this, fragmentNumber, flags, unid )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          integer,optional :: flags
          integer, optional :: unid

          integer :: auxUnid

          auxUnid=6
          if(present(unid)) auxUnid=unid

          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_write(this.fragments(fragmentNumber), flags=flags, unid=auxUnid )
          else
               call CartesianCoordinate_write(this.supramolecule, flags=flags, unid=auxUnid )
          end if

     end subroutine Supramolecular_writeFragment

     subroutine Supramolecular_scaleChargeOfFragment(this, fragmentNumber, scaleFactor )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          real(8) :: scaleFactor


          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_scaleCharge(this.fragments(fragmentNumber), scaleFactor )
          else
               call CartesianCoordinate_scaleCharge(this.supramolecule, scaleFactor )
          end if

     end subroutine Supramolecular_scaleChargeOfFragment


     subroutine Supramolecular_scaleMassOfFragment(this, fragmentNumber, scaleFactor )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          real(8) :: scaleFactor

          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_scaleMass(this.fragments(fragmentNumber), scaleFactor )
          else
               call CartesianCoordinate_scaleMass(this.supramolecule, scaleFactor )
          end if

     end subroutine Supramolecular_scaleMassOfFragment

     subroutine Supramolecular_addPrefixToSymbolsOfFragment(this, fragmentNumber, prefix )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          character(*) :: prefix

          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_addPrefixToSymbols(this.fragments(fragmentNumber), trim(prefix) )
          else
               call CartesianCoordinate_addPrefixToSymbols(this.supramolecule, trim(prefix) )
          end if

     end subroutine Supramolecular_addPrefixToSymbolsOfFragment


     subroutine Supramolecular_addSuffixToSymbolsOfFragment(this, fragmentNumber, suffix )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          character(*) :: suffix

          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_addSuffixToSymbols(this.fragments(fragmentNumber), trim(suffix) )
          else
               call CartesianCoordinate_addSuffixToSymbols(this.supramolecule, trim(suffix) )
          end if

     end subroutine Supramolecular_addSuffixToSymbolsOfFragment



     subroutine Supramolecular_removeStringToSymbolsOfFragment(this, fragmentNumber, sstring )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          character(*) :: sstring

          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_removeStringToSymbols( this.fragments(fragmentNumber), trim(sstring) )
          else
               call CartesianCoordinate_removeStringToSymbols( this.supramolecule, trim(sstring) )
          end if

     end subroutine Supramolecular_removeStringToSymbolsOfFragment


     subroutine Supramolecular_restoreSymbolsOfFragment(this, fragmentNumber )
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber


          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_restoreSymbols(this.fragments(fragmentNumber) )
          else
               call CartesianCoordinate_restoreSymbols(this.supramolecule )
          end if

     end subroutine Supramolecular_restoreSymbolsOfFragment


     subroutine Supramolecular_changeSymbolsInFragment(this, symbol, fragmentNumber, atomNumber )
          implicit none
          type(Supramolecular) :: this
          character(*) :: symbol
          integer :: fragmentNumber
          integer :: atomNumber


          if ( fragmentNumber > 0 ) then
               call CartesianCoordinate_changeSymbolOfAtom(this.fragments(fragmentNumber), trim(symbol), atomNumber )
          else
               call CartesianCoordinate_changeSymbolOfAtom(this.supramolecule, trim(symbol), atomNumber )
          end if

     end subroutine Supramolecular_changeSymbolsInFragment

     function Supramolecular_getNumberOfQuantumNucleous(this, fragmentNumber ) result(output)
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          integer :: output


          if ( fragmentNumber > 0 ) then
               output = this.fragments(fragmentNumber).numberOfQuantumNucleous
          else
               output = this.supramolecule.numberOfQuantumNucleous
          end if

     end function Supramolecular_getNumberOfQuantumNucleous

     function Supramolecular_getNumberOfQuantumNucleones(this, fragmentNumber ) result(output)
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          integer :: output


          if ( fragmentNumber > 0 ) then
               output = this.fragments(fragmentNumber).numberOfNucleones
          else
               output = this.supramolecule.numberOfNucleones
          end if

     end function Supramolecular_getNumberOfQuantumNucleones


     function Supramolecular_getQuantumNucleousLocalization(this, fragmentNumber ) result(output)
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          character(255) :: output

          if ( fragmentNumber > 0 ) then
               output = trim(this.fragments(fragmentNumber).quantumNucleiLocalization(2: &
                    len_trim(this.fragments(fragmentNumber).quantumNucleiLocalization)))
          else
               output = trim(this.supramolecule.quantumNucleiLocalization(2: &
               len_trim(this.supramolecule.quantumNucleiLocalization) ))
          end if

     end function Supramolecular_getQuantumNucleousLocalization


     function Supramolecular_getNucleonString(this, fragmentNumber ) result(output)
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          character(255) :: output

          integer :: i

          output=""
          if ( fragmentNumber > 0 ) then

               do i=1,this.fragments(fragmentNumber).numberOfQuantumNucleous
                    output = trim(output)//trim(String_convertIntegerToString(this.fragments(fragmentNumber).numberOfNucleones))//","
               end do
               output=trim(output(1:len_trim(output)-1))
          else
               do i=1,this.supramolecule.numberOfQuantumNucleous
                    output = trim(output)//trim(String_convertIntegerToString(this.supramolecule.numberOfNucleones))//","
               end do
               output=trim(output(1:len_trim(output)-1))
          end if

     end function Supramolecular_getNucleonString


     function Supramolecular_getMultiplicity(this, fragmentNumber) result(output)
          implicit none
          type(Supramolecular) :: this
          integer :: fragmentNumber
          integer :: output

          if ( fragmentNumber > 0 ) then
               output = int( (0.5*this.fragments(fragmentNumber).numberOfQuantumNucleous )*2.0+1.0)
          else
               output = int( (0.5*this.supramolecule.numberOfQuantumNucleous )*2.0+1.0)
          end if

     end function Supramolecular_getMultiplicity

     function Supramolecular_getNumberOfFragments( this ) result (output)
          implicit none 
          type(Supramolecular) :: this
          integer :: output

          output= size(this.fragments)

     end function Supramolecular_getNumberOfFragments


end module Supramolecular_