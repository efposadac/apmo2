!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para manejo de estructuras de elementos atomicos
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see AtomicElement_ , XMLParser_
!
!**
module AtomicElementManager_
	use AtomicElement_
	use XMLParser_
	use APMO_

	implicit none
	
	type, public :: AtomicElementManager
		character :: name
	end type AtomicElementManager
	
	
	public :: 
	 	type(AtomicElement), target :: AtomicElementManager_instance
	
	logical, private :: instanced = .false.
	
	public  &
		AtomicElementManager_constructor, &
		AtomicElementManager_destructor, &
		AtomicElementManager_getCovalentRadius, &
		AtomicElementManager_getElectronAffinity, &
		AtomicElementManager_getElectronegativity, &
		AtomicElementManager_getAtomicNumber,&
		AtomicElementManager_getMeltingPoint, &
		AtomicElementManager_getBoilingPoint, &
		AtomicElementManager_getDensity, &
		AtomicElementManager_getIonizationEnergy, &
		AtomicElementManager_getAtomicRadio, &
		AtomicElementManager_getVanderWaalsRadio, &
		AtomicElementManager_loadElement
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine AtomicElementManager_constructor()
		implicit none
		
	end subroutine AtomicElementManager_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine AtomicElementManager_destructor()
		implicit none
	
	end subroutine AtomicElementManager_destructor

	!**
	! Devuelve el radio covalente para un elemento especificado
	!
	!**
	function AtomicElementManager_getCovalentRadius( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

		type(AtomicElement) :: element
		character(10) :: auxSymbol	

		call AtomicElement_constructor( element )
		auxSymbol=trim( symbolOfElement )
				
		if (  scan( auxSymbol, "_" ) /= 0 ) then
			auxSymbol = trim(auxSymbol(1: scan( auxSymbol, "_" ) - 1 ) )
		end if
				
		call AtomicElementManager_loadElement( element, auxSymbol )
		output = element.covalentRadius

		call AtomicElement_destructor(element)

	end function AtomicElementManager_getCovalentRadius

	!**
	! Devuelve la afinidad electronica para un elemento especificado
	!
	!**
	function AtomicElementManager_getElectronAffinity( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

		type(AtomicElement) :: element
		character(10) :: auxSymbol	

		call AtomicElement_constructor( element )
		auxSymbol=trim( symbolOfElement )
				
		if (  scan( auxSymbol, "_" ) /= 0 ) then
			auxSymbol = trim(auxSymbol(1: scan( auxSymbol, "_" ) - 1 ) )
		end if
				
		call AtomicElementManager_loadElement( element, auxSymbol )
		output = element.electronAffinity

		call AtomicElement_destructor(element)

	end function AtomicElementManager_getElectronAffinity

	!**
	! Devuelve la electronegatividad de Pauli para un elemento especificado
	!
	!**
	function AtomicElementManager_getElectronegativity( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

		type(AtomicElement) :: element
		character(10) :: auxSymbol	

		call AtomicElement_constructor( element )
		auxSymbol=trim( symbolOfElement )
				
		if (  scan( auxSymbol, "_" ) /= 0 ) then
			auxSymbol = trim(auxSymbol(1: scan( auxSymbol, "_" ) - 1 ) )
		end if
				
		call AtomicElementManager_loadElement( element, auxSymbol )
		output = element.electronegativity	

		call AtomicElement_destructor(element)

	end function AtomicElementManager_getElectronegativity

	!**
	! Devuelve el numero atomico para un elemento especificado
	!
	!**
	function AtomicElementManager_getAtomicNumber( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getAtomicNumber

	!**
	! Devuelve el punto de fusion para un elemento especificado
	!
	!**
	function AtomicElementManager_getMeltingPoint( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getMeltingPoint

	!**
	! Devuelve el punto de ebullicion para un elemento especificado
	!
	!**
	function AtomicElementManager_getBoilingPoint( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getBoilingPoint

	!**
	! Devuelve la densidad del elemento especificado
	!
	!**
	function AtomicElementManager_getDensity( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getDensity

	!**
	! Devuelve el potencial de inizacion del elemento especificado
	!
	!**
	function AtomicElementManager_getIonizationEnergy( symbolOfElement, ionizationIndex ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		integer :: ionizationIndex
		real(8)  :: output

	end function AtomicElementManager_getIonizationEnergy

	!**
	! Devuelve el radio atomico para el elemento especificado
	!
	!**
	function AtomicElementManager_getAtomicRadio( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getAtomicRadio

	!**
	! Devuelve el radio de van der Waals para el elemento especificado
	!
	!**
	function AtomicElementManager_getVanderWaalsRadio( symbolOfElement ) result( output )
		implicit none
		character(*),  intent( in ) :: symbolOfElement
		real(8)  :: output

	end function AtomicElementManager_getVanderWaalsRadio

	!**
	! Carga los atributos deun elemento atomico del respectivo archivo en formato XML
	!
	!**
	subroutine AtomicElementManager_loadElement ( element, elementName, massicNumber )
		implicit none
		type(AtomicElement), intent(inout) :: element
		character(*) , intent(in) :: elementName
		character(*) , intent(in), optional :: massicNumber
		
		call AtomicElement_constructor(element)
		
		if ( present( massicNumber) ) element.massicNumber = DNUM( trim(massicNumber) )

		call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // &
			trim(APMO_instance.ATOMIC_ELEMENTS_DATABASE) ), &
			trim(elementName), element=element )
		
		call AtomicElement_destructor(element)
		
	end subroutine AtomicElementManager_loadElement
	
	
	
end module AtomicElementManager_