#!/usr/bin/gawk -f

BEGIN{
	FS="[,.[:blank:]]+" ;
}

($0~/^[[:blank:]]*use/ && $3!="intrinsic" && $3!="IFPORT" && $3!="IFCORE" && $3!="omp_lib" ){
	deps[$3] = 1 ;
}
	
($1=="end" && $2=="module"){
	moduleName = $3
}

END{
# 	printf( "%s.o : %s.f90 ", moduleName, moduleName ) ;
	printf( "%s.o : ", substr( moduleName,1,length(moduleName)-1) ) ;
	 
	for( dep in deps )
		printf( "%s.o ", substr( dep,1,length(dep)-1) ) ;
	
	printf( "\n" ) ;
}
