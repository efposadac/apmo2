!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:  gaussian function, primitive gaussian, gaussian integrals         !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!** 
! @brief  Modulo para definicion funciones gausianas primitivas
!
! Este modulo define una seudoclase para funciones de distribucion del tipo gausianas
! primitivas. Estas funciones han alcanzado gran popularidad en calculos de estructura 
! ya que permiten evaluar integrales moleculares con poco esfuerzo computacional y en
! algunos casos de manera cerrada. La forma de dichas funciones
! es la siguiente: 
!
! \f[ \phi(\bf{r;n},\zeta ,\bf{R})=(x-R_x)^{n_x}(y-R_y)^{n_y}(z-R_z)^{n_z} 
! \cdot e^{-\zeta (\bf{r-R})^2} \f]
!
! Donde:
!
! <table> 
! <tr> <td> \f$ \zeta \f$ : <td> <dfn> exponente orbital. </dfn>
! <tr> <td> n :<td> <dfn> vector de enteros positivos con indice de momento angular </dfn>
! <tr> <td> R : <td> <dfn> origen de la funcion gausiana primitiva. </dfn>
! </table>
!
! Las funciones gausianas son ventajosas, sobre todo si se tiene en cuenta el
! hecho que el producto de dos o mas gausianas puede representarse por una sola exponencial
! de acuerdo con la identidad de transformacion de dos gausianas en una unica exponencial:
!
!  \f[ {\varphi}_A{\varphi}_B = E_{IJ} e^{{\alpha}_P (r -P)^2} \f]
!  \f[ e^{-{\alpha}_A{\alpha}_B({\alpha}_A+{\alpha}_B)^{-1} (R_A - R_B)^2} \f]
!
! Donde:
!
! <table>
! <tr> <td> \f$ E_{IJ} \f$ : <td>  <dfn> Constate de proporcionalidad  (No es calculada). </dfn>
! <tr> <td> \f$ {\alpha}_P \f$ : <td> <dfn> Exponente orbital de la nueva gaussiana. </dfn>
! <tr> <td> <b> P : </b> <td> <dfn> ubicacion espacial de la nueva gaussiana. </dfn>
! </table>
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacian propuesto.
!	- <tt> 2010-09-25 </tt>: Edwin F. Posada C. (efposadac@unal.edu.co)
!		 -# Cambia de indices de momento angular a capas.
!
! @warning  Modicar los parametros de una gausiana directamente, deja una constante
!           de normalizacion erronea.
! @warning  La ecuacion para calculo de constantes de normalizacion implementada
!           no se ha probado para L>=2
!**
module PrimitiveGaussian_
	use Math_
	use Exception_
	
	implicit none
	
		
	type , public :: PrimitiveGaussian
		real(8) :: origin(3)								!< Coordenadas x,y,z donde se centra la gausiana
		real(8) :: orbitalExponent							!< Exponente orbital de la gausiana.
		real(8), allocatable :: normalizationConstant(:) 	!< Constantes de normalizacion.
		integer(8) :: angularMomentIndex(3)					!< Indice de momento angular n_x,n_y y n_z.
		integer(8) :: angularMoment							!< Indice de momento angular n
		integer :: owner 									!< Define el centro a quien debe asorciaarse
		integer :: numCartesianOrbital						!< Numero orbitales cartesianos
		logical :: isVectorized								!< Indica si usa o no indice de momento angular
	end type PrimitiveGaussian
	
	
	private :: &
		PrimitiveGaussian_initializate
	
	public ::  &
		PrimitiveGaussian_constructor, &
		PrimitiveGaussian_destructor, &
		PrimitiveGaussian_copyConstructor, &
		PrimitiveGaussian_show, &
 		PrimitiveGaussian_getPrimitive,&
		PrimitiveGaussian_getOrigin,&
		PrimitiveGaussian_getAngularMomentIndex,&
		PrimitiveGaussian_getStringForAngularMoment,&
		PrimitiveGaussian_getOwner, &
		PrimitiveGaussian_getPMomentComponent,&
		PrimitiveGaussian_getDMomentComponent,&
		PrimitiveGaussian_getAngularMoment,&
		PrimitiveGaussian_getAllAngularMomentIndex, &
		PrimitiveGaussian_getNormalizationConstant, &
		PrimitiveGaussian_getValueAt, &
		PrimitiveGaussian_isEqual, &
		PrimitiveGaussian_set,&
		PrimitiveGaussian_product,&
		PrimitiveGaussian_productConstant,&
		PrimitiveGaussian_reducedOrbitalExponent,&
		PrimitiveGaussian_normalizeShell,&
		PrimitiveGaussian_normalizePrimitive,&
		PrimitiveGaussian_interchange,&
		PrimitiveGaussian_sort
		
	
contains
	
	!**
	! Define el constructor para la clase PrimitiveGaussian
	!
	!**	
	subroutine PrimitiveGaussian_constructor( this , origin , orbitalExponent , angularMoment, angularMomentIndex, numCartesianOrbital, owner,noNormalize )
		implicit none
		
		type(PrimitiveGaussian) , intent(out) :: this
		real(8) , optional , intent(in) :: origin(3)
		real(8) , optional , intent(in) :: orbitalExponent
		integer(8) , optional , intent(in) :: angularMomentIndex(3)
		integer(8) , optional , intent(in) :: angularMoment
		integer, optional, intent(in) :: numCartesianOrbital !!No deberia ser opcional...
		integer, optional, intent(in) :: owner
		logical, optional, intent(in) :: noNormalize
		
		!! Inicializa la gausiana primitiva a sus valores por defecto.
		call PrimitiveGaussian_initializate( this )
	
		!! Ajusta los parametro que se especifiquen
		if ( present( origin ) )  this%origin = origin
		if ( present( orbitalExponent ) ) this%orbitalExponent = orbitalExponent
		if ( present( angularMoment ) ) this%angularMoment = angularMoment

		this%angularMomentIndex(1) = this%angularMoment

		if ( present( angularMomentIndex ) ) then
			this%angularMomentIndex = angularMomentIndex
			this%isVectorized = .true.
		end if

		if ( present( numCartesianOrbital ) ) this%numCartesianOrbital = numCartesianOrbital
		if ( present( owner ) ) this%owner = owner
	
		!! Ajusta el tamano para las constantes de normalizacion

		allocate( this%normalizationConstant( this%numCartesianOrbital ) )
		this%normalizationConstant = 1.0_8

		!! Normaliza la gausiana especificada
		if(.not. present(noNormalize) ) then
			if (this%isVectorized) then
				call PrimitiveGaussian_normalizePrimitive(this)
			else
				call PrimitiveGaussian_normalizeShell(this)
			end if
		end if
	
	end subroutine PrimitiveGaussian_constructor
	
	!**
	! @brief Define el constructor de copia para la clase PrimitiveGaussian
	!
	!**	
	subroutine PrimitiveGaussian_copyConstructor(this, otherThis)
		implicit none
		type(PrimitiveGaussian) , intent(inout) :: this
		type(PrimitiveGaussian) , intent(in) :: otherThis
		
		this%isVectorized = otherThis%isVectorized
		this%origin = otherThis%origin
		this%orbitalExponent = otherThis%orbitalExponent
		this%angularMomentIndex = otherThis%angularMomentIndex
		this%angularMoment = otherThis%angularMoment
		this%normalizationConstant = otherThis%normalizationConstant
		this%numCartesianOrbital = otherThis%numCartesianOrbital
		this%owner = otherThis%owner

	end subroutine PrimitiveGaussian_copyConstructor
	 
	
	!**
	! Define el destructor para la clase PrimitiveGaussian
	!
	!**	
	subroutine PrimitiveGaussian_destructor(this)
		implicit none
		
		type(PrimitiveGaussian) , intent(inout) :: this

	end subroutine PrimitiveGaussian_destructor
	
	
	
	
	
	!**
	! Inicializa los atributos de la gausiana primitiva recien instanciada  a un
	! conjunto de valores por defecto, con el fin que no tomen valores invalidos
	! que puedan afectar la ejecucion de otros procedimientos.
	!
	! @param this  Gausiana primitiva
	!
	! @see #PrimitiveGaussian()
	!**
	subroutine PrimitiveGaussian_initializate( this )
		implicit none
		type(PrimitiveGaussian) :: this

		this%origin =  0.0_8
		this%orbitalExponent = 1.0_8
		this%angularMomentIndex = [0_8, 0_8, 0_8]
		this%isVectorized = .false.
		this%angularMoment = 0_8
		this%numCartesianOrbital = 1
		this%owner = 0

	end subroutine PrimitiveGaussian_initializate
	
	!**
	! Muestra en pantalla el valor de los atributos asociados a la gausiana
	! solicitada, permitiendo observar el origen, el indice de momento angular,
	! el exponente orbital y  la constante de normalizacion.
	!
	! @param this  Gausiana primitiva
	!**
	subroutine  PrimitiveGaussian_show( this ) 
		implicit none
		type(PrimitiveGaussian) , intent( in ) :: this
		
		print *,""
		print *,"================================"
		print *,"   Primitive  gaussian function:"
		print *,"================================"
		print *,""
		write ( 6 , * ) "Origin: ", this%origin( 1 ) , this%origin( 2 ),  this%origin( 3 )

		if (this%isVectorized) then
			write (6,200) this%angularMomentIndex(1), this%angularMomentIndex(2), this%angularMomentIndex(3)
200			FORMAT (' AngularMomentIndex(n):',I10.1,I10.1,I10.1)
		else
			write (6,201) this%angularMoment
201			FORMAT (' AngularMoment(n):',I10.1)
		end if
		print *,"Orbital Exponent(zeta):     " , this%orbitalExponent
		print *,"Normalization Constant(N):  " , this%normalizationConstant
		print *,"Owner Center:               " , this%owner
		print *,""
	
	end subroutine PrimitiveGaussian_show
	
	!**
	! Permite obtener un puntero a la gausiana pasada como parametro, con el
	! fin de lograrla  pasar como referencia a otras procedimientos sin necesidad
	! de duplicar el objeto.
	!
	! @param this Gausiana primitiva
	!
	! @return gaussianPointer Puntero a la gausiana primitiva
	!**
	function PrimitiveGaussian_getPrimitive( this ) result( output )
		implicit none
		type(PrimitiveGaussian) , target :: this
		type(PrimitiveGaussian) , pointer :: output

		output => null()
		output => this
	end function PrimitiveGaussian_getPrimitive
	
	!**
	! Permite obtener las  coordenadas de posicion de la gaussiana pasada como
	! parametro, en un vector de reales de tres componentes arreglado de la forma:
	! [x , y , z ].
	!
	! @param this  Gausiana primitiva
	!
	! @return origin Retorna el origen de la gausiana
	!**
	function PrimitiveGaussian_getOrigin( this ) result( output ) 
		implicit none
		type(PrimitiveGaussian), intent(in) :: this

		real( 8 ) :: output( 3 )

		output = this%origin

	end function PrimitiveGaussian_getOrigin
	
	!**
	! Permite obtener el indice de momento angular de la gaussiana pasada como 
	! parametro, el cual se devuelve en un arreglo de enteros de dimension tres
	! de la forma: [ n<sub>x</sub>, n<sub>y</sub>,n<sub>z</sub>], solo se usa si
	! la gaussiana pasada como parámetro esta vectorizada.
	!
	! @param this  Gausiana primitiva
	!
	! @return angularMomentIndex Retorna el indice de momento angular
	!**
	function PrimitiveGaussian_getAngularMomentIndex( this ) result( output ) 
		implicit none
		type(PrimitiveGaussian), intent( in ) :: this
		integer( 8 ) :: output( 3 )

		output = this%angularMomentIndex
	
	end function PrimitiveGaussian_getAngularMomentIndex
	
	!**
	! Permite obtener el indice de momento angular de la gaussiana pasada como
	! parametro, el cual se devuelve en un arreglo de enteros de dimension tres
	! de la forma: [ n<sub>x</sub>, n<sub>y</sub>,n<sub>z</sub>, cartesianas]
	! para gausianas no vectorizadas
	!
	! @param this  Gausiana primitiva
	!
	! @return angularMomentIndex Retorna el indice de momento angular
	!**
	function PrimitiveGaussian_getAllAngularMomentIndex(this) result(output)
		implicit none
		type(PrimitiveGaussian) :: this
		integer(8) :: output(3,this%numCartesianOrbital)

		integer :: counter
		integer :: x, y, z
		integer :: i, j
		counter = 1

		do i = 0 , this%angularMoment
			x = this%angularMoment - i
			do j = 0 , i
				y = i - j
				z = j

				output(1:3, counter) = [x, y, z]
				counter = counter + 1
			end do
		end do
	
	end function PrimitiveGaussian_getAllAngularMomentIndex

	!**
	! @return String Retorna un arreglo con los codigos de momento angular
	! @param this Gausiana primitiva
	! @author Edwin Posada (efposadac@unal.edu.co)
	!**
	function PrimitiveGaussian_getStringForAngularMoment( this ) result( output ) 
		implicit none

		type(PrimitiveGaussian), intent( in ) :: this
		character(9), allocatable :: output(:)

		type(Exception) :: ex

		character(1) :: indexCode(0:this%angularMoment)	!< Codigo para solo un indice de momento angular
  		character(1) :: shellCode(0:8)				    !< Codigo para una capa dada
  		character(1) :: coordCode(3)				    !< Codigo de las coordenadas cartesianas
  		integer :: nx, ny, nz	 					    !< Indices de momento angular
  		integer :: i, j, m, u, v					    !< Iteradores

		if(this%isVectorized) then

			nx = this%angularMomentIndex(1)
			ny = this%angularMomentIndex(2)
			nz = this%angularMomentIndex(3)

			!! nx
    		u = 0
    		do v = 1, nx
       			u = u + 1
       			indexCode(u) = trim(coordCode(1))
    		end do
    		!! ny
    		do v = 1, ny
       			u = u + 1
       			indexCode(u) = trim(coordCode(2))
    		end do
    		!! nz
    		do v = 1, nz
       			u = u + 1
       			indexCode(u) = trim(coordCode(3))
    		end do

    		output(1) = trim(indexCode(1)(0:this%angularMoment))

		else

			if ( this%angularMoment <= 8 ) then

		  		shellCode(0:8) = ["S", "P", "D", "F", "G", "H", "I", "J", "L"]
		  		coordCode(1:3) = ["x", "y", "z"]

		  		indexCode(0) = trim(shellCode(this%angularMoment))

		  		m = 0
		  		do i = 0 , this%angularMoment
		     		nx = this%angularMoment - i
		     		do j = 0 , i
		        		ny = i - j
		        		nz = j
		        		m = m + 1
		        		!! nx
		        		u = 0
		        		do v = 1, nx
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(1))
		        		end do
		        		!! ny
		        		do v = 1, ny
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(2))
		        		end do
		        		!! nz
		        		do v = 1, nz
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(3))
		        		end do

		        		output(m) = trim(indexCode(1)(0:this%angularMoment))

		     		end do
		  		end do

			else

				call Exception_constructor( ex , ERROR )
	 			call Exception_setDebugDescription( ex, "Class object ContractedGaussian in the getShellCode function" )
	 			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
	 			call Exception_show( ex )

	 		end if
 		end if

	end function PrimitiveGaussian_getStringForAngularMoment

	
	!**
	! Permite obtener el centro propietario de la gausiana primitiva, si
	! es que este se ha asignado.
	!
	! @param this  Gausiana primitiva
	!
	! @return angularMomentIndex Retorna el indice de momento angular
	!**
	function PrimitiveGaussian_getOwner( this ) result( output ) 
		implicit none
		type(PrimitiveGaussian), intent( in ) :: this
		integer :: output
	
		output = this%owner
	
	end function PrimitiveGaussian_getOwner

	
	!** 
	! Permite obtener el indice de una de las componentes de momento angular
	!
	!
	! @param angularMomentIndex  Indice de momento angular
	!
	! @return component Componente x, y o z del indice de momento angular
	!**
	function PrimitiveGaussian_getPMomentComponent( angularMomentIndex ) result( output )
		implicit none
		integer( 8 ) , intent( in ) :: angularMomentIndex( 3 )
		integer :: output

		integer :: i 

		output = 0

		do i =1, 3

			output = i
			if  ( angularMomentIndex(i) > 0 ) then
				exit
			end if

		end do

	end function PrimitiveGaussian_getPMomentComponent

	!** 
	! Permite obtener el indice de una de las componentes de momento angular
	!
	!
	! @param angularMomentIndex  Indice de momento angular
	!
	! @return component Componente x, y o z del indice de momento angular
	!**
	function PrimitiveGaussian_getDMomentComponent( angularMomentIndex, subIndex )  &
		result( output )
		implicit none
		integer( 8 ) , intent( in ) :: angularMomentIndex( 3 )
		integer :: output

		integer :: i
		integer :: j
		integer :: component(2)
		integer :: subIndex

		component = 0
		
		do i =1, 3
			if ( angularMomentIndex(i) == 2 ) then
				component = i
			
			else if ( angularMomentIndex(i)  > 0 ) then
				component(1) = i
				do j = i+1 , 3
					if ( angularMomentIndex(j)  > 0 ) then
						component(2) = j 
						exit
					end if
				end do
				
				exit

			end if
		end do

		output = component(subIndex)

	end function PrimitiveGaussian_getDMomentComponent
	
	!**
	! Permite obtener el momento angular de la gausiana pasada como parametro,
	! el cual se puede aproximar a la suma de cada una de las componentes del
	! indice de momento angular. Si se especifica una segunda gausiana devuelve
	! la suma de momentos para las dos gausianas. Solo para gaussianas vectorizadas
	!
	! @param thisA  Gausiana primitiva A
	! @param [thisB]  Gausiana primitiva B
	!
	! @return angularMoment Retorna el momento angular
	!
	! @see #getPrimitiveAngularMomentIndex()
	!**
	function PrimitiveGaussian_getAngularMoment( primitiveGaussianA , primitiveGaussianB ) result( output ) 
		implicit none
		type(PrimitiveGaussian) , intent( in )  :: primitiveGaussianA
		type(PrimitiveGaussian) , optional , intent( in )  :: primitiveGaussianB
		integer( 8 ) :: output
		
		output = sum ( abs( primitiveGaussianA%angularMomentIndex ) )
		
		if ( present(  primitiveGaussianB ) )  then
			output = output + sum ( abs ( primitiveGaussianB%angularMomentIndex ) )
		end if

	end function PrimitiveGaussian_getAngularMoment
	
	!**
	! Permite obtener la constante de normalizacion de la gausiana primitiva 
	! pasada como parametro. Esta funcion no realiza el caculo de la constante
	! de normalizacion, solo devuele el el valor de atributo normalizationConstant
	! del objeto. El proposito de la funcion es evitar recalcular la constante
	! cada vez que se requiera (consumo de tiempo innecesario); sin embargo se debe
	! tener el cuidado de calcular la constante de normalizacion si se ha modificado
	! algun parametro de la gausiana de manera directa utilizando sus atributos
	! publicos, por ello se aconseja hacer dichos cambios a traves del procedimiento
	! set adecuado.
	!
	! @param this  Gausiana primitiva
	!
	! @return normalizationConstant Retorna la constante de normalizacion de la funcion.
	!
	! @see #normalizeGaussian() , #setGaussian()
	!**
	function PrimitiveGaussian_getNormalizationConstant( this ) result( output ) 
		implicit none
		type(PrimitiveGaussian), intent( in ) :: this
		real(8), allocatable :: output(:)

		output = this%normalizationConstant

	end function PrimitiveGaussian_getNormalizationConstant
	
	!>
	!! @brief Devuelve el valor de la funcion en la coordenada especificada
	!! @warning Revisar!!!!! por aquello del uso de indice de momento angular!!!!
	!<
	function PrimitiveGaussian_getValueAt( this, coordinate ) result( output ) 
		implicit none
		type(PrimitiveGaussian), intent( in ) :: this
		real(8) :: coordinate(3)
		real(8) :: output(this%numCartesianOrbital)

		integer :: nx, ny, nz !< indices de momento angular
		integer :: i, j, m

        m = 0
	    do i = 0 , this%angularMoment
        	nx = this%angularMoment - i
            do j = 0 , i
	            ny = i - j
                nz = j
                m = m + 1

				output(m) = this%normalizationConstant(m) &
				    * dexp(-this%orbitalExponent &
					 *((this%origin(1)-coordinate(1))**2 &
					 + (this%origin(2)-coordinate(2))**2 &
					 + (this%origin(3)-coordinate(3))**2) ) &
				    * ( (coordinate(1)-this%origin(1))** nx ) &
				    * ( (coordinate(2)-this%origin(2))** ny ) &
				    * ( (coordinate(3)-this%origin(3))** nz )
		 	end do
	 	end do
	  
	end function PrimitiveGaussian_getValueAt

	!**
	! Compara dos gausianas primitivas devolviendo verdadero si son
	! exactamente iguales o falso en caso contrario.
	!**
	function PrimitiveGaussian_isEqual( primitiveGaussianA, primitiveGaussianB ) result( output )
		implicit none
		type(PrimitiveGaussian), intent( inout ) :: primitiveGaussianA
		type(PrimitiveGaussian), intent( inout ) :: primitiveGaussianB
		logical :: output	
		
		if(	( abs(primitiveGaussianA%origin(1)-primitiveGaussianB%origin(1))< APMO_instance%DOUBLE_ZERO_THRESHOLD) .and. &
			( abs(primitiveGaussianA%origin(2)-primitiveGaussianB%origin(2))< APMO_instance%DOUBLE_ZERO_THRESHOLD) .and. &
			( abs(primitiveGaussianA%origin(3)-primitiveGaussianB%origin(3))< APMO_instance%DOUBLE_ZERO_THRESHOLD) .and. &
			( abs(primitiveGaussianA%orbitalExponent-primitiveGaussianB%orbitalExponent)< APMO_instance%DOUBLE_ZERO_THRESHOLD) .and. &
			( abs(primitiveGaussianA%angularMoment-primitiveGaussianB%angularMoment)< APMO_instance%DOUBLE_ZERO_THRESHOLD) ) then
			
			output=.true.
		else
			output=.false.
		end if
	
	end function PrimitiveGaussian_isEqual
	
	
	!**
	! Permite ajustar los parametros de la gausiana primitiva pasada como parametro
	! segun se especifique. Esta funcion incorpora autonormalizacion de la funcion
	! tras una mdificacion por lo que se recomienda usarla cuando se desee alterar algun
	! parametro de la misma.
	!
	! @param this  Gausiana primitiva
	! @param [origin]  Origen la gausiana primitiva
	! @param [orbitalExponent]  Exponente orbital
	! @param [angularMomentIndex] Indice de momento Angular
	!**
	subroutine PrimitiveGaussian_set( this, angularMomentIndex, angularMoment, numCartesianOrbital, orbitalExponent, origin, owner )
		implicit none
		type(PrimitiveGaussian) , intent( inout ) :: this
		integer( 8 ) , optional , intent( in ) :: angularMomentIndex(3)
		integer( 8 ) , optional , intent( in ) :: angularMoment
		integer, optional, intent(in) :: numCartesianOrbital
		real( 8 ) , optional , intent( in ) :: orbitalExponent
		real( 8 ) , optional , intent( in ) :: origin( 3 )
		integer, optional, intent(in) :: owner
		
		!!**************************************************************
		!!  Ajusta los atributos de la primitiva segun se especifiquen o no
		!!
		if ( present( owner) ) this%owner = owner
		if ( present( origin ) ) this%origin = origin
		if ( present( orbitalExponent ) ) then
			this%orbitalExponent = orbitalExponent
			!! Renormaliza la funcion para hacerla consistente con el cambio.
			if (this%isVectorized) then
				call PrimitiveGaussian_normalizePrimitive(this)
			else
				call PrimitiveGaussian_normalizeShell(this)
			end if
		end if

		if ( present( numCartesianOrbital ) )  this%numCartesianOrbital = numCartesianOrbital
		if ( present( angularMoment ) ) then
			this%angularMoment = angularMoment
			this%isVectorized = .false.
			!! Renormaliza la funcion para hacerla consistente con el cambio.
			if (allocated(this%normalizationConstant)) deallocate(this%normalizationConstant)
			allocate(this%normalizationConstant( this%numCartesianOrbital ))

			call PrimitiveGaussian_normalizeShell( this )
		end if

		if ( present( angularMomentIndex ) ) then
			this%angularMomentIndex = angularMomentIndex
			this%isVectorized = .true.
			!! Renormaliza la funcion para hacerla consistente con el cambio.
			call PrimitiveGaussian_normalizePrimitive( this )
		end if
	
		!!**************************************************************
		
	end subroutine PrimitiveGaussian_set
	
	!**
	! Obtiene el producto de la parte exponencial de dos funciones gausianas,
	! de acuerdo con la identidad de transformacion de dos gausianas en una
	! unica exponencial.
	!
	! @param gA  Gausiana primitiva A
	! @param gB  Gausiana primitiva B
	!
	! @return this Gausiana primitiva transformada.
	!
	! @see #gaussianProductConstant()
	!**
	function PrimitiveGaussian_product( primitiveGaussianA , primitiveGaussianB, &
		proportConstant ) result( output )
		implicit none
		type(PrimitiveGaussian) , intent( in ) :: primitiveGaussianA , primitiveGaussianB
		type(PrimitiveGaussian) :: output
		real(8), optional, intent(inout) :: proportConstant
		
		!! Se ausume que las dos distribuciones son esfericas.
		output%angularMomentIndex = 0.0_8
		output%angularMoment = 0.0_8
		
		!! Calcula el nuevo exponente orbital
		output%orbitalExponent = primitiveGaussianA%orbitalExponent + primitiveGaussianB%orbitalExponent
		
		!!**************************************************************
		!! Determinacion de las componentes cartesianas donde se centra 
		!! la nueva funcion
		!!
		
		!! Componente x
		output%origin( 1 ) = ( primitiveGaussianA%origin( 1 ) * primitiveGaussianA%orbitalExponent &
					   + primitiveGaussianB%origin( 1 ) * primitiveGaussianB%orbitalExponent ) &
					   /  ( output%orbitalExponent )
					   
		!! Componente y
		output%origin( 2 ) = ( primitiveGaussianA%origin( 2 ) * primitiveGaussianA%orbitalExponent &
					   + primitiveGaussianB%origin( 2 ) * primitiveGaussianB%orbitalExponent ) &
					   / ( output%orbitalExponent )
					   
		!! Componente z
		output%origin( 3 ) = ( primitiveGaussianA%origin( 3 ) * primitiveGaussianA%orbitalExponent &
					   + primitiveGaussianB%origin( 3 ) * primitiveGaussianB%orbitalExponent ) &
					   / ( output%orbitalExponent )
		!!**************************************************************

		if(present(proportConstant)) then
			proportConstant = PrimitiveGaussian_productConstant( &
				primitiveGaussianA , primitiveGaussianB )
		end if
	
	end function PrimitiveGaussian_product
	
	!**
	! Obtiene  la constante de proporcionalidad para el el producto de la 
	! parte  exponencial de dos funciones gausianas.
	!
	! @param gA  Gausiana primitiva A
	! @param gB  Gausiana primitiva B
	!
	! @return gaussianProductConstant Constate de proporcionalidad
	!
	! @see gaussianProduct()
	!**

	function PrimitiveGaussian_productConstant( primitiveGaussianA , primitiveGaussianB ) result( output )
		type( PrimitiveGaussian ) , intent( in ) :: primitiveGaussianA , primitiveGaussianB
		real(8) :: output
	
		real(8) :: distanceGaussians 
		real(8) :: orbitalExponent
		
		!! calcula el nuevo exponente orbital 
		orbitalExponent = primitiveGaussianA%orbitalExponent + primitiveGaussianB%orbitalExponent
		
		!! Calcula distancia entre gausianas
		distanceGaussians = ( ( primitiveGaussianA%origin( 1 ) - primitiveGaussianB%origin( 1 ) ) ** 2.0_8 &
				    + ( primitiveGaussianA%origin( 2 ) - primitiveGaussianB%origin( 2 ) ) ** 2.0_8 &
				    + ( primitiveGaussianA%origin( 3 ) - primitiveGaussianB%origin( 3 ) ) ** 2.0_8 )
		
		!! Calcula la constante de proporcionalidad
		output = exp( - ( ( primitiveGaussianA%orbitalExponent * primitiveGaussianB%orbitalExponent ) &
		 			  / orbitalExponent ) * distanceGaussians )

	end function PrimitiveGaussian_productConstant
	
	!**
	! Obtiene  el "exponente reducido" entre dos funciones gausianas.
	! este se define como:
	!
	! \f[ \xi = \frac {{\zeta}_a *{\zeta}_b} { {\zeta}_a + {\zeta}_b} \f]
	!
	! @param gA  Gausiana primitiva A
	! @param gB  Gausiana primitiva B
	!
	! @return reducedExponent  Exponente reducido
	!
	!**
	function PrimitiveGaussian_reducedOrbitalExponent( primitiveGaussianA , primitiveGaussianB ) result ( output )
		implicit none
		type( PrimitiveGaussian ) , intent( in ) :: primitiveGaussianA, primitiveGaussianB
		real(8) :: output

		output = ( primitiveGaussianA%orbitalExponent   *primitiveGaussianB%orbitalExponent ) &
				  / ( primitiveGaussianA%orbitalExponent  + primitiveGaussianB%orbitalExponent )
	
	end function PrimitiveGaussian_reducedOrbitalExponent
		
	!**
	! Permite calcular la constante de normalizacion de la gausiana de pasada
	! como parametro.
	!
	! @param this Gausiana primitiva
	!
	! @return  normalizeGaussian Constante de normalizacion
	!**
	subroutine PrimitiveGaussian_normalizeShell( this )
		implicit none
		type(PrimitiveGaussian) , intent( inout ) :: this
		
		integer(8) :: angularMomentIndex(3,this%numCartesianOrbital)
		integer :: i, j, m

		angularMomentIndex = PrimitiveGaussian_getAllAngularMomentIndex( this )

		m = 1

		do i = 1, this%numCartesianOrbital

			this%normalizationConstant(m) = ( ( 2.0_8*this%orbitalExponent/Math_PI )**0.75_8 ) &
			       / sqrt( &
			         Math_factorial( 2_8 * angularMomentIndex(1, i) - 1_8,2 )&
			       * Math_factorial( 2_8 * angularMomentIndex(2, i) - 1_8,2 )&
			       * Math_factorial( 2_8 * angularMomentIndex(3, i) - 1_8,2 )/&
			       ((4.0_8*this%orbitalExponent)**this%angularMoment))

			m = m + 1

	    end do

	end subroutine PrimitiveGaussian_normalizeShell
	
		!**
	! Permite calcular la constante de normalizacion de la gausiana de pasada
	! como parametro.
	!
	! @param this Gausiana primitiva
	!
	! @return  normalizeGaussian Constante de normalizacion
	!**
	subroutine PrimitiveGaussian_normalizePrimitive( this )
		implicit none
		type(PrimitiveGaussian) , intent( inout ) :: this

		this%normalizationConstant(1) = ( ( 2.0_8*this%orbitalExponent/Math_PI )**0.75_8 ) &
				       / sqrt( &
				         Math_factorial( 2_8 * this%angularMomentIndex(1) - 1_8,2 )&
				       * Math_factorial( 2_8 * this%angularMomentIndex(2) - 1_8,2 )&
				       * Math_factorial( 2_8 * this%angularMomentIndex(3) - 1_8,2 )/&
				       ((4.0_8*this%orbitalExponent)**sum(this%angularMomentIndex)))

	end subroutine PrimitiveGaussian_normalizePrimitive
	!**
	! Intercambia los parametros de dos gausianas especificadas
	!
	! @param thisA Gausiana primitiva
	! @param thisB Gausiana primitiva
	!**
	subroutine PrimitiveGaussian_interchange( primitiveGaussianA, primitiveGaussianB )
		implicit none
		type(PrimitiveGaussian) , intent(inout) :: primitiveGaussianA, primitiveGaussianB

		type(PrimitiveGaussian) :: aux
		
		aux = primitiveGaussianA
		primitiveGaussianA = primitiveGaussianB
		primitiveGaussianB = aux

	end subroutine PrimitiveGaussian_interchange
	
	!**
	! Ordena las gaussianas A,B,C,D pasadas como par�metro en orden descendente
	! de momento angular, devolviendolas en un arreglo din�mico cuyo tama�o depende
	! del n�mero de gaussianas especificadas. La funci�n tiene en cuenta la dependencia
	! funcional de r<sub>1</sub> o r <sub>2</sub> de acuerdo a la notaci�n de qu�micos
	! para integrales moleculares de cuatro centros.
	!
	! @param gA Gausiana primitiva
	! @param gB Gausiana primitiva
	! @param [gC] Gausiana primitiva
	! @param [gD] Gausiana primitiva
	!
	! @return  sortGaussians Arreglo ordenado de gausianas A , B , C y D
	!**
	subroutine PrimitiveGaussian_sort(primitivesInOrder, primitiveGaussianA , primitiveGaussianB , primitiveGaussianC , primitiveGaussianD ) 
		implicit none
		type(PrimitiveGaussian) :: primitivesInOrder(:)
		type(PrimitiveGaussian) , intent( in ) :: primitiveGaussianA , primitiveGaussianB 
		type(PrimitiveGaussian) , optional , intent( in ) :: primitiveGaussianC , primitiveGaussianD
		

		type(PrimitiveGaussian) :: aux
	

		
		!!************************************************************
		!! Ordena de forma descendente de momento angular las gausianas gA y gB
		!!
		if ( PrimitiveGaussian_getAngularMoment( primitiveGaussianA ) < &
			PrimitiveGaussian_getAngularMoment( primitiveGaussianB ) ) then
			
			primitivesInOrder( 1 ) = primitiveGaussianB
			primitivesInOrder( 2 ) = primitiveGaussianA
			
		else
			
			primitivesInOrder( 1 ) = primitiveGaussianA
			primitivesInOrder( 2 ) = primitiveGaussianB
			
		end if
		!!************************************************************
		
		if ( present( primitiveGaussianC ) .and. present( primitiveGaussianD ) )  then
			
			!!************************************************************
			!! Ordena de forma descendente de momento angular las gausianas gC y gD
			!!
			if (PrimitiveGaussian_getAngularMoment( primitiveGaussianC ) < &
				PrimitiveGaussian_getAngularMoment( primitiveGaussianD ) ) then
				primitivesInOrder( 3 ) = primitiveGaussianD
				primitivesInOrder( 4 ) = primitiveGaussianC
			else
				primitivesInOrder( 3 ) = primitiveGaussianC
				primitivesInOrder( 4 ) = primitiveGaussianD
			end if
			!!************************************************************
			
			!!************************************************************
			!! Ordena de forma descendente de momento angular las gausianas
			!! los pares de gausianas gA, gB y gC,gD
			!!
			if ( ( PrimitiveGaussian_getAngularMoment(primitivesInOrder( 1 ) ) < PrimitiveGaussian_getAngularMoment ( primitivesInOrder(3) ) )  &
				.or. ( ( PrimitiveGaussian_getAngularMoment( primitivesInOrder(1) )== PrimitiveGaussian_getAngularMoment( primitivesInOrder( 3 ) ) ) &
				.and. ( PrimitiveGaussian_getAngularMoment( primitivesInOrder( 2 ) ) <   PrimitiveGaussian_getAngularMoment( primitivesInOrder(4))) ) )  then
				
				aux = primitivesInOrder( 1 )
				primitivesInOrder( 1 ) = primitivesInOrder( 3 )
				primitivesInOrder( 3 ) = aux
				aux = primitivesInOrder( 2 )
				primitivesInOrder( 2 ) = primitivesInOrder( 4 )
				primitivesInOrder( 4 ) = aux

			end if
			!!************************************************************
		end if

	end subroutine PrimitiveGaussian_sort
	
end module PrimitiveGaussian_
