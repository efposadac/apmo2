!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008 by                                                        !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Clase estatica que contiene la interface para utilizar lapack
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-19
!   - <tt> 2008-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!           tomadas de (http://www.netlib.org/blas/)
!**
module LapackInterface_
	
	implicit none
	
	interface LapackInterface
	
		!**
		! Calcula todos los valores propios y opcionalmente los eigenvectores 
		! de una matriz real y simetrica. Los eigenvetores estan ortonormalizados.
		! Para detalles consultar documentacion correspondiente.
		!**
		subroutine dsyev(JOBZ, UPLO, N, A, LDA, W, WORK, LWORK, INFO)
			character :: JOBZ, UPLO
			integer :: INFO, LDA, LWORK, N
			real(8) :: A(LDA,*), W(*), WORK(*)
		end subroutine dsyev
	
		!**
		! Calcula todos los valores propios y opcionalmente los eigenvectores 
		! por la izquieda o derecha de una matriz cuadrada real y no-simetrica. 
		! Los eigenvectores se normalizan para tener norma euclidiana igual a 1 
		! y la   mas grande componente real. Para detalles consultar documentacion
		! correspondiente.
		!**
		subroutine dgeev(JOBVL, JOBVR, N, A, LDA, WR, WI, VL, LDVL, VR, LDVR,&
			WORK, LWORK, INFO)
			character :: JOBVL, JOBVR
			integer :: INFO, LDA, LDVL, LDVR, LWORK, N
			real(8) :: A(LDA,*), VL(LDVL,*), VR(LDVR,*), WI(*), WORK(*), WR(*)
		end subroutine dgeev
		
		!**
		! Factoriza una matriz de N x M mediante factorizacion LU empleando pivoteo parcial
		!**
		subroutine dgetrf( M, N, A, LDA, IPIV, INFO )
			integer :: INFO, LDA, M, N
			integer :: IPIV( * )
			real(8) :: A(LDA, *)
		end subroutine dgetrf
		
		!**
		! Calcula la inversa de una matriz previamente factorizada por pivoteo parcial
		!**
		subroutine  dgetri( N, A, LDA, IPIV, WORK, LWORK, INFO )
			integer :: INFO, LDA, LWORK, N
			integer :: IPIV(*)
			real(8) :: A(LDA,*), WORK(*)
		end subroutine dgetri
		
		!>
		!! @brief Calcula la descomposicion en valore simples de una matriz real de MxN
		!<
		subroutine  dgesvd( JOBU, JOBVT, M, N, A, LDA, S, U, LDU, VT, LDVT,&
			WORK, LWORK, INFO )
			character :: JOBU, JOBVT
			integer :: INFO, LDA, LDU, LDVT, LWORK, M, N
			real(8) :: A(  LDA,  *  ),  S( * ), U( LDU, * ), VT(LDVT, * ), WORK( * )
		end subroutine dgesvd

	end interface LapackInterface
	
end module LapackInterface_
