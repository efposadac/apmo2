!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicoion de funciones de onda RHF
! 
!  Este modulo define funciones de onda del tipo "Restricted Open Shell Hartree-Fock "
!  (RHF) para diferentes especies cuanticas dentro del formalismo OMNE
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-14
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-14 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulo y m�todos
!
	! @todo Falta por Implementar
!**
module ROHFWaveFunction_
	use Matrix_
	use Vector_
	use ParticleManager_
	use IntegralManager_

	implicit none
	
	type, public :: ROHFWaveFunction
		
		character(30) :: name
		
		!!**************************************************************
		!! Matrices de inter�s asociadas al calculo de funciones de onda RHF
		!!
		type(Matrix) :: waveFunctionCoefficients
		type(Vector) :: molecularOrbitalsEnergy 
		type(Matrix) :: independentParticleMatrix
	 	type(Matrix) :: densityMatrix 
		type(Matrix) :: transformationMatrix
		type(Matrix) :: twoParticlesMatrix
		type(Matrix) :: couplingMatrix
		type(Matrix) :: fockMatrix
		
		type(Matrix), pointer :: kineticMatrixValuesPtr
		type(Matrix), pointer :: puntualInteractionMatrixValuesPtr
		type(Matrix), pointer :: overlapMatrixValuesPtr
		!!**************************************************************
		
	 
	 	!!**************************************************************
		!! Atributos asociados a los valores de energia al final de 
		!! la funcion de onda RHF
		real(8) :: specieEnergy
		real(8) :: kineticEnergy
		real(8) :: puntualInteractionEnergy
		real(8) :: independentParticleEnergy
		real(8) :: repulsionEnergy
		real(8) :: couplingEnergy
		!!**************************************************************
		
		!!**************************************************************
		!!  Variables requeridas por conveniencia
		!!
		logical :: addTwoParticlesMatrix !< Variable por conveniencia
		logical :: addCouplingMatrix 	!< Variable por conveniencia
		logical :: wasBuiltFockMatrix 	!< Variable por conveniencia
		!!**************************************************************
		
	end type ROHFWaveFunction
	
	private ::
		type(matrix) :: auxMatrix
	
	
	type(ROHFWaveFunction), public, allocatable, target :: ROHFWaveFunction_instance(:)
	
	
	public :: &
		ROHFWaveFunction_constructor, &
		ROHFWaveFunction_destructor, &
		ROHFWaveFunction_show, &
		ROHFWaveFunction_builtDensityMatrix, &
		ROHFWaveFunction_buildFockMatrix, &
		ROHFWaveFunction_buildTwoParticlesMatrix, &
		ROHFWaveFunction_isInstanced, &
		ROHFWaveFunction_getWaveFunctionCoefficients,&
		ROHFWaveFunction_getMolecularOrbitalsEnergy, &
		ROHFWaveFunction_getEnergy, &
		ROHFWaveFunction_obtainTotalEnergyForSpecie
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	! @todo Falta por Implementar
	!**
	subroutine ROHFWaveFunction_constructor( )
		implicit none
				
	end subroutine ROHFWaveFunction_constructor
	
	!**
	! Define el destructor para clase
	!
	! @todo Falta por Implementar
	!**
	subroutine ROHFWaveFunction_destructor( )
		implicit none
				

	end subroutine ROHFWaveFunction_destructor
	
	!**
	! @brief 	Retorna el numero de iteraciones realizadas para una especie especificada
	!
	!**
	subroutine ROHFWaveFunction_show( nameOfSpecie ) 
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie

		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		call Matrix_show( ROHFWaveFunction_instance( specieID ).waveFunctionCoefficients )

	end subroutine ROHFWaveFunction_show

	!**
	! @brief Calcula la matriz de densidad para una especie especificada
	!
	! @todo Falta por implementar
	!**
	subroutine ROHFWaveFunction_builtDensityMatrix( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
				
	end subroutine ROHFWaveFunction_builtDensityMatrix
	
	!**
	! @brief Contruye la matrix de Fock para la especie especificada
	!
	!**
	subroutine ROHFWaveFunction_buildTwoParticlesMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		type(Exception) :: ex
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
			 
	end subroutine ROHFWaveFunction_buildTwoParticlesMatrix

	
	!**
	! @brief Contruye la matrix de Fock para la especie especificada
	!
	!**
	subroutine ROHFWaveFunction_buildFockMatrix( nameOfSpecie ) 
		implicit none
		character(*), optional :: nameOfSpecie
		
		type(Exception) :: ex
		character(30) :: nameOfSpecieSelected
		integer :: specieID
			
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
			 
	end subroutine ROHFWaveFunction_buildFockMatrix


	!**
	! @brief indica el objeto ha sido instanciado 
	!
	!**					
	function ROHFWaveFunction_isInstanced() result(output)
		implicit none
		logical :: output
		
		if ( allocated( ROHFWaveFunction_instance ) ) then
			output = .true.
		else
			output = .false.
		end if
	
	end function ROHFWaveFunction_isInstanced

	
	!**
	! @brief Retorna la matriz  de coeficientes de combinacion
	!
	!**
	function ROHFWaveFunction_getWaveFunctionCoefficients( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Matrix) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( allocated( ROHFWaveFunction_instance(specieID).waveFunctionCoefficients.values) ) then		
			
			call Matrix_copyConstructor( output, ROHFWaveFunction_instance(specieID).waveFunctionCoefficients )
			
		end if
				
	end function ROHFWaveFunction_getWaveFunctionCoefficients
	
	!**
	! @brief Retorna la matriz  de coeficientes de combinacion
	!
	!**
	function ROHFWaveFunction_getMolecularOrbitalsEnergy( nameOfSpecie ) result( output )
		implicit none
		character(*), optional :: nameOfSpecie
		type(Vector) ::  output
		
		character(30) :: nameOfSpecieSelected
		integer :: specieID
				
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		if ( allocated( ROHFWaveFunction_instance(specieID).molecularOrbitalsEnergy.values) ) then		
			
			call Vector_copyConstructor( output, ROHFWaveFunction_instance(specieID).molecularOrbitalsEnergy )
			
		end if
				
	end function ROHFWaveFunction_getMolecularOrbitalsEnergy
	
	!**
	! @brief Retorna la energia calculada en la ultima iteracion
	!**
	function ROHFWaveFunction_getEnergy( nameOfSpecie, typeOfEnergy ) result( output )
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie
		character(*), optional, intent(in) :: typeOfEnergy
		real(8) :: output
		
		integer :: specieID
		character(30) :: nameOfSpecieSelected

		
		nameOfSpecieSelected = "e-"
		if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
		
		specieID = ParticleManager_getSpecieID( nameOfSpecie=nameOfSpecieSelected )
		
		select case(trim(typeOfEnergy))
		
			case("specieEnergy")
				output = ROHFWaveFunction_instance(specieID).specieEnergy
			
			case("kineticEnergy")
				output = ROHFWaveFunction_instance(specieID).kineticEnergy
			
			case("puntualInteractionEnergy")
				output = ROHFWaveFunction_instance(specieID).puntualInteractionEnergy
		
			case("independentParticleEnergy")
				output = ROHFWaveFunction_instance(specieID).independentParticleEnergy
		
			case("repulsionEnergy")
				output = ROHFWaveFunction_instance(specieID).repulsionEnergy
				
			case("couplingEnergy")
				output = ROHFWaveFunction_instance(specieID).couplingEnergy

			case default
				output = ROHFWaveFunction_instance(specieID).specieEnergy
		
		end select
		
	end function ROHFWaveFunction_getEnergy
	
	!**
	! @brief calcula la energia total para una especie especificada
	!
	!**					
	subroutine ROHFWaveFunction_obtainTotalEnergyForSpecie( nameOfSpecie )
		implicit none
		character(*), optional :: nameOfSpecie
		

	end subroutine ROHFWaveFunction_obtainTotalEnergyForSpecie


		
end module ROHFWaveFunction_