!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para calculo de propiedades de la funci�n de onda
! 
!  Este modulo define una seudoclase para calculo de propiedades derivadas de 
! la funci�n de onda como cargas, dipolos, polarizabilidades, etc.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-09-18
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-09-18 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos b�sicos.
!
! @todo Traer algoritmo de calulo de poblaciones de MolecularSystem a este modulo
!
!**
module CalculateProperties_
	use MolecularSystem_
	use ParticleManager_
	use IntegralManager_
	use Matrix_
	use Units_
	use Exception_

	implicit none
	
	type, public :: CalculateProperties
		character(30) :: name
		type(Matrix) :: contributionsOfdipoleMoment	
	end type 
	
	type(CalculateProperties) :: CalculateProperties_instance
	
	private :: &
		CalculateProperties_getDipoleOfPuntualCharges, &
		CalculateProperties_getDipoleOfQuantumSpecie
	
	public :: &
		CalculateProperties_constructor, &
		CalculateProperties_destructor, &
		CalculateProperties_dipole, &
		CalculateProperties_showContributionsToElectrostaticMoment
		

contains

	!**
	! @brief Constructor para la clase
	!**
	subroutine CalculateProperties_constructor( this )
		implicit none
		type(CalculateProperties) :: this
		
	end subroutine CalculateProperties_constructor
	
	!**
	! @brief Destructor para la clase
	!**
	subroutine CalculateProperties_destructor( this )
		implicit none
		type(CalculateProperties) :: this
		
	end subroutine CalculateProperties_destructor

	!**
	! @brief  Calcula las componentes del dipolo debido a las diferentes
	!		especies y cargas puntules presentes
	!**
	subroutine CalculateProperties_dipole( this )
		implicit none
		type(CalculateProperties) :: this
		
		integer :: i
		integer :: numberOfSpecies
		
		numberOfSpecies = ParticleManager_getNumberOfQuantumSpecies()
		
		call Matrix_constructor( this.contributionsOfdipoleMoment, numberOfSpecies + 2_8, 3_8, 0.0_8 )
		
		do i=1, numberOfSpecies
		 
			this.contributionsOfdipoleMoment.values(i,:) = CalculateProperties_getDipoleOfQuantumSpecie( i )
			
		end do
		
		this.contributionsOfdipoleMoment.values(numberOfSpecies + 1,:) = CalculateProperties_getDipoleOfPuntualCharges()
		
		do i=1, numberOfSpecies + 1
		
			this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,1) = &
				this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,1) + this.contributionsOfdipoleMoment.values(i,1)
			this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,2) = &
				this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,2) + this.contributionsOfdipoleMoment.values(i,2)
			this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,3) = &
				this.contributionsOfdipoleMoment.values(numberOfSpecies + 2,3) + this.contributionsOfdipoleMoment.values(i,3)
		
		end do
		this.contributionsOfdipoleMoment.values = this.contributionsOfdipoleMoment.values *DEBYES
	end subroutine CalculateProperties_dipole
	
	!**
	! @brief Muestra las contrinuciones al dipolo de cada especie 
	!**
	subroutine CalculateProperties_showContributionsToElectrostaticMoment(this)
		implicit none
		type(CalculateProperties) :: this
		
		integer :: i
		integer :: j
		integer ::ssize
		
		type(Exception) :: ex

		ssize = size(this.contributionsOfdipoleMoment.values, dim=1)
		print *,""
		print *," ELECTROSTATIC MOMENTS:"
		print *,"======================"
		print *,""
		print *,"DIPOLE: (DEBYE)"
		print *,"------"
		print *,""
		write (6,"(T19,<4>A9)") "<Dx>","<Dy>", "<Dz>"," |D|"
		do i=1, ssize - 2 
 			write (6,"(T5,A15,<3>F9.4)") trim(ParticleManager_getNameOfSpecie( i )), ( this.contributionsOfdipoleMoment.values(i,j),j=1,3 )
 		end do
		write (6,"(T5,A15,<3>F9.4)") "Fixed ", (this.contributionsOfdipoleMoment.values(ssize-1, j), j=1,3)
		write (6,"(T22,A28)") "___________________________________"
 		write (6,"(T5,A15,<3>F9.4, F9.4)") "Total ", (this.contributionsOfdipoleMoment.values(ssize, j), j=1,3), &
			dsqrt(sum(this.contributionsOfdipoleMoment.values(ssize,:)**2.0) )
		
		print *,""
		print *,"END ELECTROSTATIC MOMENTS"
		print *,""
		
		
	end subroutine CalculateProperties_showContributionsToElectrostaticMoment
	
	!**
	! @brief Calcula el aporte al dipolo de las cargas puntuales presentes
	!**
	function CalculateProperties_getDipoleOfPuntualCharges() result( output ) 
		implicit none
		real(8) :: output(3)
		
		integer :: i
		real(8) :: origin(3)
		output = 0.0_8
		
		do i=1, ParticleManager_getNumberOfPuntualParticles()
			
			origin = ParticleManager_getOriginOfPuntualParticle( i )
			
			output(1) = output(1) + origin(1) * ParticleManager_getChargeOfPuntualParticle(i)
			output(2) = output(2) + origin(2) * ParticleManager_getChargeOfPuntualParticle(i)
			output(3) = output(3) + origin(3) * ParticleManager_getChargeOfPuntualParticle(i)
		
		end do
		
	end function CalculateProperties_getDipoleOfPuntualCharges
	
	
	!**
	! @brief Calcula el aporte al dipolo debido a particulas no fijas
	!**
	function CalculateProperties_getDipoleOfQuantumSpecie(  specieID ) result( output ) 
		implicit none
		integer, intent(in), optional  :: specieID
		real(8) :: output(3)
		
		
		type(Matrix) :: densityMatrix
		type(Matrix) :: momentMatrix
		character(30) :: nameOfSpecieSelected
		
		nameOfSpecieSelected = trim( ParticleManager_getNameOfSpecie( specieID ) )
		
		densityMatrix = MolecularSystem_getDensityMatrix( trim(nameOfSpecieSelected) )
			
		output = 0.0_8
		
		!! Calcula integrales asociadas al momento electrico dipolar en la direccion x
		call IntegralManager_buildMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected), component = 1  )
		momentMatrix = IntegralManager_getMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected)  )
		output(1) =sum( densityMatrix.values * momentMatrix.values )
 
					
		!! Calcula integrales asociadas al momento electrico dipolar en la direccion y
		call IntegralManager_buildMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected), component = 2  )
		momentMatrix = IntegralManager_getMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected)  )
		output(2) =sum( densityMatrix.values * momentMatrix.values )
		
		!! Calcula integrales asociadas al momento electrico dipolar en la direccion z
		call IntegralManager_buildMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected), component = 3  )
		momentMatrix = IntegralManager_getMatrix( MOMENT_INTEGRALS, trim(nameOfSpecieSelected)  )
		output(3) =sum( densityMatrix.values * momentMatrix.values )
		
		output = output * ParticleManager_getCharge( specieID )
		
		call Matrix_destructor( densityMatrix )
		call Matrix_destructor( momentMatrix )
		
	end function CalculateProperties_getDipoleOfQuantumSpecie

end module CalculateProperties_
