!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module GamessInterface_
     use ParticleManager_
     use MolecularSystem_
     use Exception_
     use APMO_
     use Matrix_
     use String_
     use IFPORT
     implicit none


        !< enum fragments {
	integer, parameter, public :: FRAGMENT_WITH_FRAGMENT_BASIS = 0
	integer, parameter, public :: FRAGMENT_WITH_COMPLEX_BASIS  = 1
	!< }

     type, private :: zMatrix
          character(255) :: name
          character(5), allocatable :: atomsSymbol(:)
          real(8), allocatable :: nuclearCharge(:)
          type(Matrix) :: cartessianMatrix
     end type

     type, private :: GamessInterface
          character(20) :: name='singleton'
          character(10) :: theory
          character(255) :: nuclearBasisSet
          character(255) :: electronicBasisSet
          type(zMatrix) :: complexZMatrix
          type(zMatrix), allocatable :: fragmentsZMatrix(:)
          type(MolecularSystem), pointer :: apmoMolecularSystemPtr
          real(8) :: totalEnergy
          integer :: charge
          integer :: multiplicity
          integer :: mp2Correction
          integer :: numberOfQuantumSpecies
          integer :: numberOfQuantumNucleous
          integer :: numberOfFragments
          integer :: isotopeMass


     end type

     type(GamessInterface), private :: GamessInterface_singletonInstance

contains


     subroutine GamessInterface_constructor( apmoMolecularSystem, theory, mp2Correction )
          implicit none
          type(MolecularSystem), target :: apmoMolecularSystem
          character(*), optional :: theory
          integer, optional :: mp2Correction

          character(255) :: line
          integer :: existFile
          real(8), allocatable :: charges(:)
          character(10),allocatable :: labels(:)
          integer :: i

          GamessInterface_singletonInstance.theory="rhf"
          GamessInterface_singletonInstance.charge = 0
          GamessInterface_singletonInstance.multiplicity=1
          GamessInterface_singletonInstance.mp2Correction=0
          GamessInterface_singletonInstance.apmoMolecularSystemPtr => apmoMolecularSystem
          GamessInterface_singletonInstance.electronicBasisSet=""
          GamessInterface_singletonInstance.nuclearBasisSet=""
          GamessInterface_singletonInstance.numberOfFragments=ParticleManager_getNumberOfFragments()

          if( present(theory) ) GamessInterface_singletonInstance.theory = trim(theory(1:3))
          if( present(mp2Correction) ) GamessInterface_singletonInstance.mp2Correction = mp2Correction

          inquire(FILE = trim(APMO_instance.INPUT_FILE)//"gms.bs", EXIST = existFile )

          if ( existFile ) then

	       open(UNIT=34,FILE=trim(APMO_instance.INPUT_FILE)//"gms.bs",STATUS='unknown',ACCESS='sequential')
	       if ( .not.eof(34) ) then
		    read(34,'(A)') line
		    GamessInterface_singletonInstance.electronicBasisSet=trim(adjustl(trim(line)))
               else

                    call GamessInterface_exception(ERROR,"Electronic basis set was not found","Class object GamessInterface in constructor() function")

	       end if

	       if ( .not.eof(34) ) then
		    read(34,'(A)') line
		    GamessInterface_singletonInstance.nuclearBasisSet=trim(adjustl(trim(line)))
	       end if

	       close(34)
          else
               call GamessInterface_exception(ERROR,"You should to define a "//trim(APMO_instance.INPUT_FILE)//"gms.bs"//" file for the gamess basis set","Class object GamessInterface in constructor() function")
          end if



          !!******************************************************
          !! Obtiene informacion para el complejo molecular
          !!****
          GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix=ParticleManager_getCartesianMatrixOfCentersOfOptimization()
          GamessInterface_singletonInstance.complexZMatrix.name=trim(APMO_instance.INPUT_FILE)//"gms."
          allocate( GamessInterface_singletonInstance.complexZMatrix.atomsSymbol( &
               size(GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix.values,dim=1) ) )
          allocate( GamessInterface_singletonInstance.complexZMatrix.nuclearCharge( &
               size( GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix.values,dim=1) ) )

          GamessInterface_singletonInstance.complexZMatrix.atomsSymbol=ParticleManager_getLabelsOfCentersOfOptimization()	
          GamessInterface_singletonInstance.complexZMatrix.nuclearCharge=ParticleManager_getChargesOfCentersOfOptimization()

          !!
          !!*******************************************************


          !!******************************************************
          !! Obtiene informacion para los fragmentos moleculares
          !!****
          if ( GamessInterface_singletonInstance.numberOfFragments > 1 ) then

               allocate( GamessInterface_singletonInstance.fragmentsZMatrix( GamessInterface_singletonInstance.numberOfFragments ))

               do i=1,GamessInterface_singletonInstance.numberOfFragments
                    GamessInterface_singletonInstance.fragmentsZMatrix(i).name=trim(APMO_instance.INPUT_FILE)//&
                         trim(adjustl(trim(String_convertIntegerToString(i))))//".gms."
                    GamessInterface_singletonInstance.fragmentsZMatrix(i).cartessianMatrix= &
                    ParticleManager_getCartesianMatrixOfCentersOfOptimization(i)
                    allocate( GamessInterface_singletonInstance.fragmentsZMatrix(i).atomsSymbol( &
                         size(GamessInterface_singletonInstance.fragmentsZMatrix(i).cartessianMatrix.values,dim=1) ) )
                    allocate( GamessInterface_singletonInstance.fragmentsZMatrix(i).nuclearCharge( &
                         size( GamessInterface_singletonInstance.fragmentsZMatrix(i).cartessianMatrix.values,dim=1) ) )

                    GamessInterface_singletonInstance.fragmentsZMatrix(i).atomsSymbol = &
                         ParticleManager_getLabelsOfCentersOfOptimization(fragment=i)
                    GamessInterface_singletonInstance.fragmentsZMatrix(1).nuclearCharge = &
                         ParticleManager_getChargesOfCentersOfOptimization(fragment=i)
               end do
          end if

          !!
          !!*******************************************************

     end subroutine GamessInterface_constructor


     subroutine GamessInterface_destructor()
          implicit none

          integer :: i


          if( allocated( GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix.values)) then

               call Matrix_destructor(GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix)
               deallocate(GamessInterface_singletonInstance.complexZMatrix.atomsSymbol)
               deallocate(GamessInterface_singletonInstance.complexZMatrix.nuclearCharge)

	       GamessInterface_singletonInstance.theory=""
	       GamessInterface_singletonInstance.charge = 0
	       GamessInterface_singletonInstance.multiplicity=0
	       GamessInterface_singletonInstance.mp2Correction=0
               GamessInterface_singletonInstance.apmoMolecularSystemPtr => null()


               if ( allocated(GamessInterface_singletonInstance.fragmentsZMatrix) ) then

                    do i=1,size(GamessInterface_singletonInstance.fragmentsZMatrix)

                         if( allocated(GamessInterface_singletonInstance.fragmentsZMatrix(i).cartessianMatrix.values) ) then
                              call Matrix_destructor(GamessInterface_singletonInstance.fragmentsZMatrix(i).cartessianMatrix)
                              deallocate(GamessInterface_singletonInstance.fragmentsZMatrix(i).atomsSymbol)
                              deallocate(GamessInterface_singletonInstance.fragmentsZMatrix(i).nuclearCharge)
                         end if

                    end do

               end if

          end if

     end subroutine GamessInterface_destructor


     subroutine GamessInterface_builtInput( nameOfFile,flags, fragment )
          implicit none
          character(*) :: nameOfFile
          integer, optional, intent(in) :: flags
          integer, optional, intent(in) :: fragment


          character(10) :: symbol
          integer :: i
          integer :: j

          if ( GamessInterface_singletonInstance.numberOfQuantumSpecies > 2) &
               call GamessInterface_exception(ERROR, "Mixtures of quantum nucleous are not accepted in gamess-neo", &
				"Class object GamessInterface in builtInput() function")

	  open( UNIT=34,FILE=trim(nameOfFile),STATUS='REPLACE', &
	       ACCESS='SEQUENTIAL', FORM='FORMATTED' )

	  write (34,"(A,I1.1,A,I2.1,A)") " $CONTRL SCFTYP="//trim(GamessInterface_singletonInstance.theory)// &
	       " RUNTYP=energy MULT=",GamessInterface_singletonInstance.multiplicity,&
	       " ICHARG=",GamessInterface_singletonInstance.charge," $END"
               write (34,"(A)") " $CONTRL UNITS=BOHR $END"
	  if ( GamessInterface_singletonInstance.mp2Correction > 0 ) then
               write (34,"(A)") " $CONTRL MPLEVL=2 $END"
               write (34,"(A)") " $MP2 MP2PRP=.T. NACORE=0 CUTOFF=1.0E-12 $END"
	  end if


          write (34,"(A)") " $BASIS "//trim(GamessInterface_singletonInstance.electronicBasisSet)//" $END"
          write (34,"(A)") " $SYSTEM  MWORDS=200 TIMLIM=20000000 $END"

          if ( GamessInterface_singletonInstance.numberOfQuantumSpecies > 1 ) then
               write (34,"(A)") " $SCF SOSCF=.F. ETHRSH=1.5 DIIS=.T. $END"
               write (34,"(A)") " $INTGRL SCHWRZ=.F. $END"
               write (34,"(A)") " $NEO  NEOSCF=RONHF BASNUC=DZSPDN NUNIQN=4 IUNIQN(1)=2,3,5,6"
               write (34,"(A)") "       IUNIQT(1)=2,2,2,2 NUMULT=5 NEMPLV=2  $END"
          end if
          write (34,"(A)") " $DATA"
          write (34,"(A)") " ----- SP calculation from APMO --------"
          write (34,"(A)") " C1"


          if ( present(fragment) .and. fragment > 0 ) then


               GamessInterface_singletonInstance.fragmentsZMatrix(fragment).cartessianMatrix =  &
                    ParticleManager_getCartesianMatrixOfCentersOfOptimization(fragment)

               do i=1,size(GamessInterface_singletonInstance.fragmentsZMatrix(fragment).cartessianMatrix.values,dim=1)

                    symbol=GamessInterface_singletonInstance.fragmentsZMatrix(fragment).atomsSymbol(i)
                    if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
                    write(34,"(A,F4.1,<3>F15.10)") trim(symbol)//"    ", &
                    abs(GamessInterface_singletonInstance.fragmentsZMatrix(fragment).nuclearCharge(i)), &
                    GamessInterface_singletonInstance.fragmentsZMatrix(fragment).cartessianMatrix.values(i,1:3)
               end do


               select case(flags)

                    case(FRAGMENT_WITH_FRAGMENT_BASIS)

                    case(FRAGMENT_WITH_COMPLEX_BASIS)


                         do j=1,GamessInterface_singletonInstance.numberOfFragments
                              if( j /= fragment) then
                                   GamessInterface_singletonInstance.fragmentsZMatrix(j).cartessianMatrix =  &
                                        ParticleManager_getCartesianMatrixOfCentersOfOptimization(j)

                                   do i=1,size(GamessInterface_singletonInstance.fragmentsZMatrix(j).cartessianMatrix.values,dim=1)

                                        symbol=GamessInterface_singletonInstance.fragmentsZMatrix(j).atomsSymbol(i)
                                        if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
                                        write(34,"(A,F4.1,<3>F15.10)") trim(symbol)//"    ", &
                                        -abs(GamessInterface_singletonInstance.fragmentsZMatrix(j).nuclearCharge(i)), &
                                        GamessInterface_singletonInstance.fragmentsZMatrix(j).cartessianMatrix.values(i,1:3)
                                   end do
                              end if
                         end do

                    case default

               end select


          else

               GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix =  &
                    ParticleManager_getCartesianMatrixOfCentersOfOptimization()

               do i=1,size(GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix.values,dim=1)

                    symbol=GamessInterface_singletonInstance.complexZMatrix.atomsSymbol(i)
                    if(scan(symbol,"_") /=0) symbol=symbol(1:scan(symbol,"_")-1)
                    write(34,"(A,F4.1,<3>F15.10)") trim(symbol)//"    ", &
                         abs(GamessInterface_singletonInstance.complexZMatrix.nuclearCharge(i)), &
                    GamessInterface_singletonInstance.complexZMatrix.cartessianMatrix.values(i,1:3)

               end do

          end if
          write (34,"(A)") " $END"
          write (34,"(A)") ""

          close(34)


     end subroutine GamessInterface_builtInput


     function GamessInterface_isInstanced() result (output)
          implicit none
          logical :: output

          output=.false.
          if ( allocated(GamessInterface_singletonInstance.complexZMatrix.atomsSymbol) ) output = .true.

     end function GamessInterface_isInstanced

     function GamessInterface_getEnergyFromOutput( nameOfFile ) result(output)
          implicit none
          character(*) :: nameOfFile
          real(8) :: output

          character(255) :: outputFile
          character(255) :: line
          integer :: existFile

          inquire(FILE = trim(nameOfFile)//"log", EXIST = existFile )
          if ( existFile ) then
               outputFile = trim(nameOfFile)//"log"
          else
               inquire(FILE = trim(nameOfFile)//"out", EXIST = existFile )

               if (  existFile ) then
                    outputFile = trim(nameOfFile)//"out"
               else
                    call GamessInterface_exception(ERROR,"The gamess output file "//trim(nameOfFile)//"out was not found",&
                    "Class object GamessInterface in getEnergyFromOutput() function")
               end if

          end if

          open(UNIT=34,FILE=trim(outputFile),STATUS='unknown',ACCESS='sequential')

          if (.not.eof(34)) then
               read(34,'(A)') line
               line=trim(adjustl(trim(line)))
          else

               call GamessInterface_exception(ERROR,"The output file is empty",&
                    "Class object GamessInterface in getEnergyFromOutput() function")

          end if


          if ( GamessInterface_singletonInstance.mp2Correction == 2 ) then

               do while( (line(1:7)) /= "E(MP2)=" )
                    read(34,'(A)') line
                    line=trim(adjustl(trim(line)))

               end do

               if ( (line(1:7)) == "E(MP2)=" )  &
               output = dnum(line(scan(trim(line),"=" )+1:len_trim(line)))

          else
               do while((line(1:13)) /= "TOTAL ENERGY=")
                    read(34,'(A)') line
                    line=trim(adjustl(trim(line)))

               end do

               if ( (line(1:13)) == "TOTAL ENERGY=" )  &
                    output = dnum(line(scan(trim(line),"=" )+1:len_trim(line)))
          end if

          close(34)

     end function GamessInterface_getEnergyFromOutput

     function GamessInterface_getEnergy( withCounterPoiseCorrection ) result( output )
          implicit none
          logical, intent(in), optional :: withCounterPoiseCorrection
          real(8) :: output

          integer :: status
          integer :: i



          if ( trim(APMO_instance.GAMESS_COMMAND) /= "" ) then


               call GamessInterface_builtInput(trim(GamessInterface_singletonInstance.complexZMatrix.name)//"inp")

               status= system(trim(APMO_instance.GAMESS_COMMAND)//" "//trim(GamessInterface_singletonInstance.complexZMatrix.name)//"inp")

               output = GamessInterface_getEnergyFromOutput( trim(GamessInterface_singletonInstance.complexZMatrix.name) )



               if( withCounterPoiseCorrection .and. GamessInterface_singletonInstance.numberOfFragments > 1) then

                    do i=1,GamessInterface_singletonInstance.numberOfFragments

                         call GamessInterface_builtInput(trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name)//"inp",&
                              fragment=i, flags=FRAGMENT_WITH_COMPLEX_BASIS)

                         status= system(trim(APMO_instance.GAMESS_COMMAND)// &
                              "  "//trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name)//"inp")

                         output = output - GamessInterface_getEnergyFromOutput( &
                               trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name) )


                         call GamessInterface_builtInput(trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name)//"inp",&
                              fragment=i, flags=FRAGMENT_WITH_FRAGMENT_BASIS)


                         status= system(trim(APMO_instance.GAMESS_COMMAND)// &
                              "  "//trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name)//"inp")

                         output = output + GamessInterface_getEnergyFromOutput( &
                               trim(GamessInterface_singletonInstance.fragmentsZMatrix(i).name) )



                    end do

               end if

               print "(A1$)","."


          else

               call GamessInterface_exception(ERROR,"The command for gamess has not been defined",&
                    "Class object GamessInterface in getEnergy() function")

          end if

     end function GamessInterface_getEnergy

     !>
     !! @brief  Maneja excepciones de la clase
     !<
     subroutine GamessInterface_exception(typeMessage, description, debugDescription)
          implicit none
          integer :: typeMessage
          character(*) :: description
          character(*) :: debugDescription

          type(Exception) :: ex

          call Exception_constructor( ex , typeMessage )
          call Exception_setDebugDescription( ex, debugDescription )
          call Exception_setDescription( ex, description )
          call Exception_show( ex )
          call Exception_destructor( ex )

     end subroutine GamessInterface_exception


end module GamessInterface_