!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para implementacion de pilas 
!
! Modulo para manejo de pilas de datos, esta implementacion esta basada en 
! una estructura de datos tipo lista enlazada. 
!
! @author Sergio A. Gonzalez
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
! @todo verificar la liberacion de memoria al eliminar nodeos de la pila
!**
module Stack_
	use Exception_
	
	implicit none
	
	type, public :: StackNode
		real(8) :: value
		integer :: ssize
		type (StackNode), pointer :: next
		type (StackNode), pointer :: before
	end type StackNode

	type, public :: Stack
	
		character(30) :: name
		
		type(StackNode), pointer :: first
		type(StackNode), pointer :: last
		type(StackNode), pointer :: node
		integer :: maxSize
		
	end type Stack
	
	
	public :: &
		Stack_constructor, &
		Stack_destructor, &
		Stack_size, &
		Stack_empty, &
		Stack_push, &
		Stack_pop, &
		Stack_top 
		

contains

	!**
	! @brief Define el constructor para la clase
	!**
	subroutine Stack_constructor( this, name, ssize ) 
		implicit none
		type(Stack), intent(inout) :: this
		character(*),optional :: name
		integer, optional, intent(in) :: ssize
	
		this.maxSize =10
		this.name = "undefined"
		
		if ( present(name) ) this.name = trim(name)
		if (present(ssize)) this.maxSize =ssize
		
		
	end  subroutine Stack_constructor

	!**
	! @brief Define el destructor para la clase
	!
	! @ Verificar que la memoria priviamente separada sea liberada
	!**
	subroutine Stack_destructor( this ) 
		implicit none
		type(Stack), intent(inout) :: this
		
		integer i
	
		if (associated(this.first)) then
	
			do i=1,Stack_size(this)
	
				call Stack_pop( this )
	
			end do
	
		end if
		
	end  subroutine Stack_destructor

	!**
	! @brief Indica si la pila tiene o no nodeos
	!**
	function Stack_empty( this) result(output)
		implicit none
		type(Stack), intent(in) :: this
		logical :: output
	
		output=.false.
	
		if( .not.associated(this.first) ) output =.true.
		
	end function Stack_empty

	!**
	! @brief Adiciona un nodeo a la pila
	!
	! @ Verificar que la memoria de cada nodo sea liberada cuando �ste se elimine
	!**
	subroutine Stack_push( this, data )
		implicit none
		type(Stack), intent(inout) :: this
		real(8),intent(in) :: data
		
		type(Exception) :: ex
	
		if ( .not.associated(this.first) ) then
			allocate(this.node)
			nullify(this.node.next)
			this.node.ssize =1
			this.first=>this.node
			this.node.value=data
			allocate(this.node.next)
			this.node.before=>null()
			this.node=>this.node.next
			nullify(this.node.next)
			this.node.before=>this.first
			this.last=>this.first
			this.node.ssize= 2
		else
	
			if( this.last.ssize < this.maxSize ) then
				this.node.value=data
				allocate(this.node.next)
				this.last=>this.node
				this.node=>this.node.next
				nullify(this.node.next)
				this.node.before=>this.last
				this.node.ssize=this.last.ssize+1
			else
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object Stack in the push() function" )
				call Exception_setDescription( ex, "Overflow in the Stack" )
				call Exception_show( ex )
				
			end if
		end if
	
	end subroutine Stack_push

	!**
	! @brief Elimina el �ltimo nodeo de la pila
	!**
	subroutine Stack_pop( this )
		implicit none
		type(Stack), intent(inout) :: this
		
		type(Exception) :: ex
	
		if ( associated(this.first) ) then
			if( loc(this.first) /= loc(this.last) ) then
	
				nullify(this.last.next)
				deallocate(this.last.next)
				deallocate(this.node.before)
				nullify(this.node.before)
				nullify(this.node)
				deallocate(this.node)
				this.last=> this.last.before
				this.node=>this.last.next
				nullify(this.node.next)
	
			else
				nullify(this.node)
				deallocate(this.node)
				nullify(this.last.next)
				deallocate(this.last.next)
				nullify(this.last)
				nullify(this.first )
				deallocate(this.first )
			end if
		else
			call Exception_constructor( ex , WARNING )
			call Exception_setDebugDescription( ex, "Class object Stack in the pop() function" )
			call Exception_setDescription( ex, "The stack is empty" )
			call Exception_show( ex )
		end if
		
		return
	
	end subroutine Stack_pop

	!**
	! @brief Retorna el tama�o actual de la pila
	!**
	function Stack_size( this) result(output)
		implicit none
		type(Stack),  intent(in) :: this
		integer :: output
	
		output=0
	
		if( associated(this.last) ) then
			
			output =this.last.ssize
	
		end if
		
	end function Stack_size

	!**
	! @brief Retorna el �ltimo nodeo de la pila
	!
	! @todo Permitir que el retorno de la funcion sea una referencia y no valor
	!**
	function Stack_top( this ) result(output)
		implicit none
		type(Stack),  intent(in) :: this
		real(8) :: output
	
		if( associated(this.last) ) then
			
			output=this.last.value
			
		end if
		
	end function Stack_top

end module Stack_