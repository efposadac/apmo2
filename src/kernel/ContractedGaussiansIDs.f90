!!**********************************************************************************
!!    Copyright (C) 2008 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de contractiones de punteros de gaussianas contraidas
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-26
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-08-26 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion del modulo
!
! @todo Adicionar metodos push y pop back
!**
module ContractedGaussiansIDs_
	use ContractedGaussian_
	use Exception_
	
	implicit none
	

	type, public :: IDs
		integer :: particleID
		integer :: contractionIDInParticle
	end type IDs
	
	type, public :: ContractedGaussiansIDs
		type(IDs), allocatable :: contractionID(:)
	end type ContractedGaussiansIDs

	
	public :: &
		ContractedGaussiansIDs_constructor, &
		ContractedGaussiansIDs_destructor, &
		ContractedGaussiansIDs_addVault, &
		ContractedGaussiansIDs_removeVault, &
		ContractedGaussiansIDs_add, &
		ContractedGaussiansIDs_remove, &
		ContractedGaussiansIDs_getSize
		
	
	contains
	
	!**
	! @brief Define el metodo para el constructor de la clase.
	!
	!**
	subroutine ContractedGaussiansIDs_constructor( this, ssize )
		implicit none
		type(ContractedGaussiansIDs), intent(inout), allocatable :: this(:)
		integer, intent(in) :: ssize
		
		if ( .not.allocated(this) ) allocate(this(ssize)) 
		
	end subroutine ContractedGaussiansIDs_constructor
	

	!**
	! @brief Define el metodo para el destructor de la clase.
	!
	!**
	subroutine ContractedGaussiansIDs_destructor(this)
		implicit none
		type(ContractedGaussiansIDs), allocatable, intent(inout) :: this(:)
		
		integer :: i
		if ( allocated(this) ) then
		 
		 	do i=1, size(this)
		 		if ( allocated(this(i).contractionID) ) deallocate(this(i).contractionID)
			end do
			
			deallocate(this)
			
		end if
				
			
	end subroutine ContractedGaussiansIDs_destructor
	
	!**
	! @brief Define el metodo para el destructor de la clase.
	!
	!**
	subroutine ContractedGaussiansIDs_addVault(this)
		implicit none
		type(ContractedGaussiansIDs), allocatable, intent(inout) :: this(:)
		
		integer :: i
		integer :: j
		integer :: ssize
		integer :: ssizeB
		type(ContractedGaussiansIDs), allocatable :: auxThis(:)
		
		if ( allocated(this) ) then
		 
		 	ssize = size( this )
			allocate( auxThis(ssize) )
			do i=1, ssize
				
				ssizeB= size( this(i).contractionID)
				allocate( auxThis(i).contractionID(ssizeB) )
				
				do j=1, ssizeB
				
					auxThis(i).contractionID(j).particleID = this(i).contractionID(j).particleID
					auxThis(i).contractionID(j).contractionIDInParticle = this(i).contractionID(j).contractionIDInParticle
					
				end do
				
			end do
			
			call ContractedGaussiansIDs_destructor(this)
			allocate( this(ssize+1) )
			
		 	do i=1, ssize
				
				ssizeB= size( auxThis(i).contractionID)
				allocate( this(i).contractionID(ssizeB) )
				
				do j=1, ssizeB
				
					this(i).contractionID(j).particleID = auxThis(i).contractionID(j).particleID
					this(i).contractionID(j).contractionIDInParticle = auxThis(i).contractionID(j).contractionIDInParticle
					
				end do
				
			end do
			
			call ContractedGaussiansIDs_destructor( auxThis )
		else
			
			allocate( this(1) )
			
		end if
				
			
	end subroutine ContractedGaussiansIDs_addVault

	!**
	! @brief Remueve un elemeto del arreglo
	!
	! @todo Falta implemetacion completa
	!**
	subroutine ContractedGaussiansIDs_removeVault(this)
		implicit none
		type(ContractedGaussiansIDs), allocatable, intent(inout) :: this(:)
		
				
			
	end subroutine ContractedGaussiansIDs_removeVault


	
	!**
	! @brief Adiciona un nuevo elemento al vector
	!
	! @todo ver manual de fortran para copia de estructuras, con el fin de 
	!		de mejorar este algoritmo
	!**
	subroutine ContractedGaussiansIDs_add(this, particleID, contractionIDInParticle )
		implicit none
		type(ContractedGaussiansIDs), intent(inout) :: this
		integer, intent(in) :: particleID
		integer, intent(in) :: contractionIDInParticle
		
		type(ContractedGaussiansIDs) :: auxThis
		integer :: i
		integer :: ssize
		type(Exception) :: ex
		
		if ( allocated( this.contractionID ) ) then
			
			ssize = size( this.contractionID )
			allocate ( auxThis.contractionID( ssize ) )
			
			do i = 1, ssize
				auxThis.contractionID(i).particleID = this.contractionID(i).particleID
				auxThis.contractionID(i).contractionIDInParticle = this.contractionID(i).contractionIDInParticle
			end do
			
			deallocate( this.contractionID )
			allocate( this.contractionID(ssize + 1 ) )
			
			!! Este ciclo puede ser evitado, revice la documentacion de fortran
			!! asociada con copia de arreglos de estructuras
			do i = 1, ssize
				this.contractionID(i).particleID = auxThis.contractionID(i).particleID
				this.contractionID(i).contractionIDInParticle = auxThis.contractionID(i).contractionIDInParticle
			end do
			
			deallocate( auxThis.contractionID )
			
			this.contractionID(ssize + 1).particleID = particleID
			this.contractionID(ssize + 1).contractionIDInParticle = contractionIDInParticle
		
		else
			
			allocate( this.contractionID(1) )
			this.contractionID(1).particleID = particleID
			this.contractionID(1).contractionIDInParticle = contractionIDInParticle
			
		end if
				
			
	end subroutine ContractedGaussiansIDs_add
	
	
	!**
	! @brief Adiciona un nuevo elemento al vector
	!
	! @todo Falta implementacion completa
	!**
	subroutine ContractedGaussiansIDs_remove(this, ID )
		implicit none
		type(ContractedGaussiansIDs),  intent(inout) :: this
		integer, intent(in) :: ID
				
			
	end subroutine ContractedGaussiansIDs_remove
	
	!**
	! @brief Adiciona un nuevo elemento al vector
	!
	! @todo Falta implementacion completa
	!**
	function ContractedGaussiansIDs_getSize(this ) result(output)
		implicit none
		type(ContractedGaussiansIDs),  intent(inout) :: this
		integer :: output
		
		output=size( this.contractionID )
			
	end function ContractedGaussiansIDs_getSize
	
end module ContractedGaussiansIDs_	
