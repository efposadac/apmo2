!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para realizacion de iteraciones SCF sobre diferentes especies cuanticas
! 
!  Este modulo define una seudoclase para realizacion de ciclos SCF sobre todas las
! especies cuanticas.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-30
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!
!**
module SCFInterspecies_
	use APMO_
	use Exception_
	use WaveFunction_
	use List_
	use SCFIntraspecies_
	use ParticleManager_
	
	implicit none
	
	
	!< enum Convergece_status_type {
	integer, parameter :: SCF_INTERSPECIES_CONVERGENCE_FAILED	=  0
	integer, parameter :: SCF_INTERSPECIES_CONVERGENCE_CONTINUE	=  1
	integer, parameter :: SCF_INTERSPECIES_CONVERGENCE_SUCCESS	=  2
	!< }
	
	!< enum Iteration_scheme_type {
	integer, parameter :: SCHEME_NONELECRONIC_FULLY_CONVERGED_BY_ELECTRONIC_ITERATION =  0
	integer, parameter :: SCHEME_ELECTRONIC_FULLY_CONVERGED_BY_NONELECRONIC_ITERATION =  1
	integer, parameter :: SCHEME_SPECIE_FULLY_CONVERGED_INDIVIDIALLY =  2
	integer, parameter :: SCHEME_SIMULTANEOUS =  3
	!< }
	
	
	type, public :: SCFInterspecies
		
		character(30) :: name
		integer :: numberOfIterations
		type(List) :: energyOMNE
		integer :: status
		real(8) :: electronicTolerance
		real(8) :: nonelectronicTolerance
		
	end type SCFInterspecies
	
	
	type(SCFInterspecies), public, target :: SCFInterspecies_instance
	
	private :: &
		SCFInterspecies_iterateNonElectronicFullyByElectronicIteration, &
		SCFInterspecies_iterateElectronicFullyByNonElectronicIteration, &
		SCFInterspecies_iterateUniqueSpecie, &
		SCFInterspecies_iterateSpecieFullyConvergedIndividually
	
	public :: &
		SCFInterspecies_constructor, &
		SCFInterspecies_destructor, &
		SCFInterspecies_setStopingThreshold, &
		SCFInterspecies_getNumberOfIterations, &
		SCFInterspecies_getLastEnergy, &
		SCFInterspecies_iterate, &
		SCFInterspecies_restart, &
		SCFInterspecies_reset, &
		SCFInterspecies_testEnergyChange
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine SCFInterspecies_constructor()
		implicit none
		
		call SCFIntraspecies_constructor()
		SCFInterspecies_instance.numberOfIterations = 0
		call List_constructor( SCFInterspecies_instance.energyOMNE,"energy", APMO_instance.LISTS_SIZE)
		
		if ( APMO_instance.OPTIMIZE .and.  .not.APMO_instance.MINIMIZATION_WITH_SINGLE_POINT ) then
			
			SCFInterspecies_instance.electronicTolerance = APMO_instance.SCF_ELECTRONIC_ENERGY_TOLERANCE
			SCFInterspecies_instance.nonelectronicTolerance = APMO_instance.SCF_NONELECTRONIC_ENERGY_TOLERANCE
		
		else
					
			if ( trim(APMO_instance.SCF_CONVERGENCE_CRITERIUM) == "density" ) then
				
				SCFInterspecies_instance.electronicTolerance = APMO_instance.ELECTRONIC_DENSITY_MATRIX_TOLERANCE
				SCFInterspecies_instance.nonelectronicTolerance = APMO_instance.NONELECTRONIC_DENSITY_MATRIX_TOLERANCE
			
			else
				
				SCFInterspecies_instance.electronicTolerance = APMO_instance.SCF_ELECTRONIC_ENERGY_TOLERANCE
				SCFInterspecies_instance.nonelectronicTolerance = APMO_instance.SCF_NONELECTRONIC_ENERGY_TOLERANCE
	
			end if
			
		end if
		
	end subroutine SCFInterspecies_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine SCFInterspecies_destructor()
		implicit none
				
		call SCFIntraspecies_destructor()
		call List_destructor( SCFInterspecies_instance.energyOMNE )
		
	end subroutine SCFInterspecies_destructor
	
	!**
	! @brief 	Ajusta el criterio de parada que se emplea en los ciclos SCF de 
	!		entre particulas de la misma especie.
	!**
	subroutine SCFInterspecies_setStopingThreshold( electronicThreshold, nonElectronicThreshold )
		implicit none
		real(8), intent(in) :: electronicThreshold
		real(8), intent(in) :: nonElectronicThreshold
		
			
		SCFInterspecies_instance.electronicTolerance = electronicThreshold
		SCFInterspecies_instance.nonelectronicTolerance = nonElectronicThreshold
		
	
	end subroutine SCFInterspecies_setStopingThreshold
	
	!**
	! @brief Retorna la energia obtenida en la ultima iteracion iter-especies
	!**
	function SCFInterspecies_getNumberOfIterations() result( output )
		implicit none

		integer :: output
		
		output= SCFInterspecies_instance.numberOfIterations
		
	end function SCFInterspecies_getNumberOfIterations
	
	!**
	! @brief Retorna la energia obtenida en la ultima iteracion iter-especies
	!**
	function SCFInterspecies_getLastEnergy() result( output )
		implicit none

		real(8) :: output
		
		call List_end( SCFInterspecies_instance.energyOMNE )
		output= List_current( SCFInterspecies_instance.energyOMNE )
		
	end function SCFInterspecies_getLastEnergy
	
	!**
	! @brief Realiza esquema de iteracion SCF para todas las especies cuanticas presentes
	!
	! @param nameOfSpecie nombre de la especie seleccionada.
	!
	! @todo Adicionar condiciones de existencia de electrones en caso 1 y 2
	!		y plantear caso analogo iterando sobre la particula mas ligera
	!**
	subroutine SCFInterspecies_iterate( iterationScheme)
		implicit none
		integer, intent(in) :: iterationScheme
		
		select  case( iterationScheme )
		
			case( SCHEME_NONELECRONIC_FULLY_CONVERGED_BY_ELECTRONIC_ITERATION )
				
				call SCFInterspecies_iterateNonElectronicFullyByElectronicIteration()
			
			case( SCHEME_ELECTRONIC_FULLY_CONVERGED_BY_NONELECRONIC_ITERATION )
			
				call SCFInterspecies_iterateElectronicFullyByNonElectronicIteration()
			
			case( SCHEME_SPECIE_FULLY_CONVERGED_INDIVIDIALLY )
			
				call SCFInterspecies_iterateSpecieFullyConvergedIndividually()

			case( SCHEME_SIMULTANEOUS )
	
				call SCFInterspecies_iterateSimultaneous()

			case default
			
				call SCFInterspecies_iterateElectronicFullyByNonElectronicIteration()
				
		end select
	
	end subroutine SCFInterspecies_iterate
	
	!**
	! @brief Reinicia el proceso de iteracion SCF para la especie especificada
	!
	!**
	subroutine SCFInterspecies_restart() 
		implicit none
		
		SCFInterspecies_instance.numberOfIterations = 0
		
		call List_clear( SCFInterspecies_instance.energyOMNE )

	end subroutine SCFInterspecies_restart
	
	!**
	! @brief Reinicia el proceso de iteracion SCF para la especie especificada
	!
	!**
	subroutine SCFInterspecies_reset() 
		implicit none
		
		SCFInterspecies_instance.numberOfIterations = 0
		
		call List_clear( SCFInterspecies_instance.energyOMNE )
		SCFInterspecies_instance.status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE
		call SCFIntraspecies_reset()
		
		
	end subroutine SCFInterspecies_reset
	
	!**
	! @brief 	Realiza la convergencia completa de la parte no electronica por cada iteracion
	!		SCF de la parte electronica
	!
	! @todo falta implementacion completa 
	!**
	subroutine SCFInterspecies_iterateNonElectronicFullyByElectronicIteration()
		implicit none
	end subroutine SCFInterspecies_iterateNonElectronicFullyByElectronicIteration
	
	!**
	! @brief 	Realiza la convergencia completa de la parte electronica por cada iteracion
	!		SCF de la parte no electronica
	!
	!**
	subroutine SCFInterspecies_iterateElectronicFullyByNonElectronicIteration()
		implicit none
		
		integer :: iteratorOfSpecie
		character(30) :: nameOfSpecie
		
		SCFInterspecies_instance.status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE

		call ParticleManager_rewindSpecies()
		nameOfSpecie = trim( ParticleManager_iterateSpecie() )

		species_cycle : &
		do
		 
			if( ParticleManager_getNumberOfQuantumSpecies() > 1 ) then
			
		
				if( ParticleManager_findSpecie( "e-" )> 0 ) then
				!!call SCFIntraspecies_restart( nameOfSelectedSpecie )
				electronic_cycle : &
				do while ( ( SCFInterspecies_instance.status ==  SCF_INTRASPECIES_CONVERGENCE_CONTINUE ) .and. &
						(SCFIntraspecies_getNumberOfIterations(nameOfSpecie) <= APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) )
					
						
						call SCFIntraspecies_iterate("e-")
						
						if ( trim(APMO_instance.SCF_CONVERGENCE_CRITERIUM) == "density" ) then

							SCFInterspecies_instance.status = SCFIntraspecies_testDensityMatrixChange( "e-", &
							SCFInterspecies_instance.electronicTolerance )

						else

							SCFInterspecies_instance.status =  SCFIntraspecies_testEnergyChange( "e-", & 
							SCFInterspecies_instance.electronicTolerance )
							
						end if
						
				end do electronic_cycle
			end if
				
				if( trim(nameOfSpecie)=="e-" ) nameOfSpecie = trim( ParticleManager_iterateSpecie() )

				!! Realiza iteracion SCF para una especie cuantica particular
				call SCFIntraspecies_iterate( nameOfSpecie )
								
				if ( trim(APMO_instance.SCF_CONVERGENCE_CRITERIUM) == "density" ) then				
					
					SCFInterspecies_instance.status = SCFIntraspecies_testDensityMatrixChange( nameOfSpecie, &
						SCFInterspecies_instance.nonelectronicTolerance )
				else

					SCFInterspecies_instance.status =  SCFIntraspecies_testEnergyChange( nameOfSpecie, &
						SCFInterspecies_instance.nonelectronicTolerance )
				
				end if

				if ( ( SCFInterspecies_instance.status ==  SCF_INTRASPECIES_CONVERGENCE_CONTINUE ) .and. &
					(SCFIntraspecies_getNumberOfIterations(nameOfSpecie) <= APMO_instance.SCF_NONELECTRONIC_MAX_ITERATIONS ) ) then
										
					cycle species_cycle
				
				else if ( SCFInterspecies_instance.status == SCF_INTRASPECIES_CONVERGENCE_SUCCESS ) then
					
						nameOfSpecie = trim( ParticleManager_iterateSpecie() )
						
						if ( trim(nameOfSpecie) == "e-" ) then
							
							!! Finaliza un ciclo SCF completo para para todas la especies presentes
							call WaveFunction_obtainTotalEnergy()
							call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
							SCFInterspecies_instance.numberOfIterations = SCFInterspecies_instance.numberOfIterations + 1
							return
							
						else
								
							cycle species_cycle
						
						end if
						
				end if
				
			else
				if (.not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS) then !1
					call SCFInterspecies_iterateUniqueSpecie(  nameOfSpecie )
				end if
				call WaveFunction_obtainTotalEnergy()
				call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
				exit species_cycle
					
			end if
	
		end do species_cycle
		
	end subroutine SCFInterspecies_iterateElectronicFullyByNonElectronicIteration
	
	!**
	! @brief 	Realiza la convergencia completa de cada especie de manera individual
	!
	!**
	subroutine SCFInterspecies_iterateSpecieFullyConvergedIndividually()
		implicit none
		integer :: iteratorOfSpecie
		character(30) :: nameOfSpecie
		character(30) :: nameOfInitialSpecie
		
		
		SCFInterspecies_instance.status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE

		call ParticleManager_rewindSpecies()
		nameOfSpecie = trim( ParticleManager_iterateSpecie() )
		nameOfInitialSpecie = trim( nameOfSpecie )

		species_cycle : &
		do
		 
			if( ParticleManager_getNumberOfQuantumSpecies() > 1 ) then
			
				!!call SCFIntraspecies_restart( nameOfSelectedSpecie )
				individualSpecie_cycle : &
				do while ( ( SCFInterspecies_instance.status ==  SCF_INTRASPECIES_CONVERGENCE_CONTINUE ) .and. &
						(SCFIntraspecies_getNumberOfIterations(nameOfSpecie) <= APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) )
					
						
						call SCFIntraspecies_iterate( nameOfSpecie )

						if ( trim(APMO_instance.SCF_CONVERGENCE_CRITERIUM) == "density" ) then

							SCFInterspecies_instance.status = SCFIntraspecies_testDensityMatrixChange( nameOfSpecie, &
							SCFInterspecies_instance.electronicTolerance )

						else

							SCFInterspecies_instance.status =  SCFIntraspecies_testEnergyChange( nameOfSpecie, & 
							SCFInterspecies_instance.electronicTolerance )
							
						end if
						
				end do individualSpecie_cycle
					
				
				nameOfSpecie = trim( ParticleManager_iterateSpecie() )
						
				if ( trim(nameOfInitialSpecie) == trim(nameOfSpecie) ) then
							
					!! Finaliza un ciclo SCF completo para para todas la especies presentes
					call WaveFunction_obtainTotalEnergy()
					call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
					SCFInterspecies_instance.numberOfIterations = SCFInterspecies_instance.numberOfIterations + 1
					return
					
				else
					SCFInterspecies_instance.status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE
					cycle species_cycle
				
				end if
				
				
			else
				if (.not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS) then !2
					call SCFInterspecies_iterateUniqueSpecie( nameOfSpecie )
				end if
				call WaveFunction_obtainTotalEnergy()
				call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
				exit species_cycle
					
			end if
	
		end do species_cycle


	end subroutine SCFInterspecies_iterateSpecieFullyConvergedIndividually


	subroutine SCFInterspecies_iterateSimultaneous()
		implicit none

		integer :: i
		integer :: numberOfSpecies
		character(30) :: nameOfSpecie
		character(30) :: nameOfInitialSpecie
		logical :: auxValue
		
		auxValue = APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS
		
		SCFInterspecies_instance.status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE
		numberOfSpecies = ParticleManager_getNumberOfQuantumSpecies()

		nameOfSpecie = trim( ParticleManager_iterateSpecie() )

		if( numberOfSpecies > 1 ) then

			call ParticleManager_rewindSpecies()
			nameOfSpecie = trim( ParticleManager_iterateSpecie() )
			
			do i=1, numberOfSpecies
				!!Especifica el procedimiento a seguir si lo electrones han sido congelados
				if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
					if(ParticleManager_isFrozen(trim(nameOfSpecie))) then
						APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = .true.
					else
						APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxValue
					end if
				end if
				
				call SCFIntraspecies_iterate( nameOfSpecie, actualizeDensityMatrix=.false. )
				nameOfSpecie = trim( ParticleManager_iterateSpecie() )
			end do
			
			call ParticleManager_rewindSpecies()
			nameOfSpecie = trim( ParticleManager_iterateSpecie() )
			
			do i=1, numberOfSpecies
				call SCFIntraspecies_actualizeDensityMatrix( nameOfSpecie )
				nameOfSpecie = trim( ParticleManager_iterateSpecie() )
			end do

			call WaveFunction_obtainTotalEnergy()
			call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
			
			SCFInterspecies_instance.numberOfIterations = SCFInterspecies_instance.numberOfIterations + 1
			
			APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxValue
			return
			
		else
			!!Especifica el procedimiento a seguir si lo electrones han sido congelados
			if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
				if(ParticleManager_isFrozen(trim(nameOfSpecie))) then
					APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = .true.
				else
					APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxValue
				end if
			end if
			
			if (.not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS) then !3
				call SCFInterspecies_iterateUniqueSpecie( nameOfSpecie )
			end if
			
			APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxValue
			
			call WaveFunction_obtainTotalEnergy()
			call List_push_back( SCFInterspecies_instance.energyOMNE, WaveFunction_getTotalEnergy() )
				
		end if
	
	end subroutine SCFInterspecies_iterateSimultaneous
	
	!**
	! @brief 	Realiza la convergencia completa de la unica especie presente
	!
	!**
	subroutine SCFInterspecies_iterateUniqueSpecie( nameOfSpecie )
		implicit none
		character(*) :: nameOfSpecie
	
		real(8) :: tolerace
		real(8) :: diisError
		character :: typeConvergence
		
		if ( trim(nameOfSpecie) == "e-" ) then
			tolerace = SCFInterspecies_instance.electronicTolerance
		else
			tolerace = SCFInterspecies_instance.nonelectronicTolerance
		end if
		
		!! Realiza ciclo de convergencia SCF para la unica especie cuantica presente
		
		if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
			print *,"Begin SCF calculation by: ",trim(nameOfSpecie)
			print *,"-------------------------"
			print *,""
			print *,"-----------------------------------------------------------------"
			write (6,"(A10,A12,A25,A20)") "Iteration", "Energy", " Density Change","         DIIS Error "
			print *,"-----------------------------------------------------------------"
		end if
		
		do while ( ( SCFInterspecies_instance.status ==  SCF_INTRASPECIES_CONVERGENCE_CONTINUE ) .and. &
				(SCFIntraspecies_getNumberOfIterations( nameOfSpecie ) <= APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) )
		
			call WaveFunction_builtFockMatrix( nameOfSpecie )
			call SCFIntraspecies_iterate( nameOfSpecie )
			


			diisError =SCFIntraspecies_getDiisError( nameOfSpecie )
			
			
			if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then

				typeConvergence = " "
				if ( diisError > APMO_instance.DIIS_SWITCH_THRESHOLD ) typeConvergence = "*"
				
				if (abs(diisError) < APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
					write (6,"(I5,F20.10,F20.10,A20,A1)") SCFIntraspecies_getNumberOfIterations( nameOfSpecie ), &
					SCFIntraspecies_getLastEnergy( nameOfSpecie), &
					SCFIntraspecies_getStandardDeviationOfDensistyMatrix( nameOfSpecie ), "         --         ",typeConvergence
				else
					write (6,"(I5,F20.10,F20.10,F20.10,A1)") SCFIntraspecies_getNumberOfIterations( nameOfSpecie ), &
					SCFIntraspecies_getLastEnergy( nameOfSpecie), &
					SCFIntraspecies_getStandardDeviationOfDensistyMatrix( nameOfSpecie ), diisError,typeConvergence
				end if



				
					
			end if
				
			SCFInterspecies_instance.status = SCFIntraspecies_testDensityMatrixChange( nameOfSpecie, tolerace )
			
		end do
		
		if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
			call SCFIntraspecies_showIteratiosStatus( SCFInterspecies_instance.status, nameOfSpecie )
			print *,"... end SCF calculation"
		end if
	
	end subroutine SCFInterspecies_iterateUniqueSpecie
	
	!**
	! @brief Prueba si la energia del la ultima iteracion a sufrido un cambio por debajo de cierta tolerancia
	!**
	function SCFInterspecies_testEnergyChange(  tolerace ) result( output )
		implicit none
		real(8), intent(in) :: tolerace
		integer :: output
		
		character(30) :: nameOfSpecie
		real(8) :: deltaEnergy
		real(8) :: finalEnergy
		real(8) :: toleraceOfSpecie
		type(Exception) :: ex
		integer :: i
		logical :: auxVar
		
		if ( SCFInterspecies_instance.numberOfIterations > APMO_instance.SCF_INTERSPECIES_MAXIMUM_ITERATIONS ) then
			 
			 output =	SCF_INTRASPECIES_CONVERGENCE_FAILED
			 call List_end( SCFInterspecies_instance.energyOMNE )
			finalEnergy= List_current( SCFInterspecies_instance.energyOMNE )
				
			!! Obtiene el valor  de energia anterior a la ultima iteracion
			call List_iterate( SCFInterspecies_instance.energyOMNE, -1 )
				
			!! Obtiene el cambio de energia en las ultimas dos iteraciones
			deltaEnergy = finalEnergy - List_current( SCFInterspecies_instance.energyOMNE )
			print *,""
			write (6,"(A20,A20)") "   Current Energy   ", "   Current Change   "
			print *,"-----------------------------------------------"
			write (6,"(F20.10,F20.10)") finalEnergy, deltaEnergy

			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object SCFInterspecies in the testEnergyChange function" )
			call Exception_setDescription( ex, "The number of Iterations was exceded, the convergence had failed" )
			call Exception_show( ex )
			 
		else

			if ( SCFInterspecies_instance.numberOfIterations > 1 ) then
				
				auxVar=.true.	
				call ParticleManager_rewindSpecies()
				do i=1,ParticleManager_getNumberOfQuantumSpecies()
			
					nameOfSpecie = trim( ParticleManager_iterateSpecie() )
					toleraceOfSpecie = SCFInterspecies_instance.electronicTolerance
					if ( trim(nameOfSpecie) /= "e-" ) toleraceOfSpecie = SCFInterspecies_instance.nonelectronicTolerance

					if ( SCFIntraspecies_testDensityMatrixChange( nameOfSpecie, &
							toleraceOfSpecie ) == SCF_INTRASPECIES_CONVERGENCE_SUCCESS ) then
							auxVar= auxVar .and. .true.
					else
							auxVar = auxVar .and. .false.
					end if
					
				end do
				
				call List_end( SCFInterspecies_instance.energyOMNE )
				deltaEnergy= List_current( SCFInterspecies_instance.energyOMNE )
				
				!! Obtiene el valor  de energia anterior a la ultima iteracion
				call List_iterate( SCFInterspecies_instance.energyOMNE, -1 )
				
				!! Obtiene el cambio de energia en las ultimas dos iteraciones
				deltaEnergy = deltaEnergy - List_current( SCFInterspecies_instance.energyOMNE )
				
				if( ( ( abs( deltaEnergy ) < tolerace ) .and. auxVar ) .or. abs(deltaEnergy) < APMO_instance.STRONG_ENERGY_TOLERANCE ) then
					output =	SCF_INTERSPECIES_CONVERGENCE_SUCCESS	
				else
					output =	SCF_INTERSPECIES_CONVERGENCE_CONTINUE
				end if
			
			else
			
				output =	SCF_INTERSPECIES_CONVERGENCE_CONTINUE
			
			end if
		
		end if
			
	end function SCFInterspecies_testEnergyChange
	
	
end module SCFInterspecies_
