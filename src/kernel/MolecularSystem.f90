!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia
!!    http://www.unal.edu.co
!!
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>
!!    Keywords:
!!
!!    thisPointer files is part of nonBOA
!!
!!    thisPointer program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    thisPointer program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with thisPointer program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************

module MolecularSystem_
	use IntegralManager_
	use InternalCoordinates_
	use MecanicProperties_
	use ParticleManager_
	use ExternalPotentialManager_
	use InputParser_
	use Matrix_
	use Exception_
	use String_
	use Units_

	implicit none

	!>
	!! @brief Modulo para definicion de datos de elemetos atomicos.
	!! 
	!!  Este modulo define una seudoclase para manejo de datos atomicos, correspondientes
	!!  a elementos atomicos.
	!!
	!! @author Sergio A. Gonzalez Monico
	!!
	!! <b> Fecha de creacion : </b> 2008-08-05
	!!
	!! <b> Historial de modificaciones: </b>
	!!
	!!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
	!!        -# Propuso estandar de codificacion.
	!!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
	!!        -# Se adapto al estandar de codificacion propuesto.
	!!
	!! @see XMLParser_
	!!
	!<
	type, public :: MolecularSystem
		
		character(50) :: name
		character(30) :: puntualGroup
		real(8) :: charge
		real(8) :: totalEnergy
		real(8) :: kineticEnergy
		real(8) :: quantumPuntualInteractionEnergy
		real(8) :: puntualInteractionEnergy
		real(8) :: couplingEnergy
		real(8) :: repulsionEnergy
		real(8) :: potentialEnergy
		integer ::  numberOfParticles
		
		!! Atributos empleados por conveniencia (para evitar dependencias ciclicas),
		!! debe ser cuidadoso al cambiar la interfase publica de WaveFunction
		character(30), pointer :: nameOfSpeciesPtr(:)
		real(8), pointer :: kineticEnergyPtr(:)
		real(8), pointer :: repulsionEnergyPtr(:)
		real(8), pointer :: couplingEnergyPtr(:)
		real(8), pointer :: quantumPuntualInteractionEnergyPtr(:)
		type(Matrix), pointer :: coefficientsOfCombinationPtr(:)
		type(Matrix), pointer :: densityMatrixPtr(:)
		type(Vector), pointer :: energyOfmolecularOrbitalPtr(:)
		type(MecanicProperties) :: mecProperties
		type(InternalCoordinates) :: intCoordinates
		character(50) :: methodName
		
		logical :: isInstanced
		
	end type MolecularSystem

	integer, parameter, public :: MULLIKEN	=  1
	integer, parameter, public :: LOWDIN	=  2
	type(MolecularSystem), public, target :: MolecularSystem_instance
	
	type(MolecularSystem), pointer, private :: MolecularSystem_instancePtr

	
	public :: &
		MolecularSystem_constructor, &
		MolecularSystem_destructor, &
		MolecularSystem_showInformation, &
		MolecularSystem_showCartesianMatrix, &
		MolecularSystem_showZMatrix,&
		MolecularSystem_showDistanceMatrix, &
		MolecularSystem_showEnergyComponents,&
		MolecularSystem_showEigenVectors, &
		MolecularSystem_showPopulationAnalyses, &
		MolecularSystem_showCharges, &
		MolecularSystem_setName, &
		MolecularSystem_setMethodName, &
		MolecularSystem_setTotalEnergy,&
		MolecularSystem_setTotalCouplingEnergy, &
		MolecularSystem_setPuntualGroup, &
		MolecularSystem_setCoefficientsOfCombinationPtr, &
		MolecularSystem_setDensityMatrixPtr, &
		MolecularSystem_setEnergyOfMolecularOrbitalsPtr, &
		MolecularSystem_setEnergyComponentsPtr, &
		MolecularSystem_setOrigin, &
		MolecularSystem_loadOfInput, &
		MolecularSystem_obtainEnergyComponents, &
		MolecularSystem_getName, &
		MolecularSystem_getMethodName, &
		MolecularSystem_getTotalEnergy,&
		MolecularSystem_getPuntualGroup, &
		MolecularSystem_getEnergy, &
		MolecularSystem_getEigenVectors, &
		MolecularSystem_getEigenValues, &
		MolecularSystem_getDensityMatrix, &
		MolecularSystem_getPopulation, &
		MolecularSystem_getPartialCharges, &
		MolecularSystem_getCharge, &
		MolecularSystem_isInstanced, &
		MolecularSystem_moveToCenterOfMass, &
		MolecularSystem_rotateOnPrincipalAxes, &
		MolecularSystem_calculateElectronicEffect
		
contains
	
	!>
	!! @brief Define el constructor para la clase
	!!
	!<
	subroutine MolecularSystem_constructor( this, name, puntualGroup )
		implicit none
		type(MolecularSystem), target :: this
		character(*), intent(in), optional :: name
		character(*), intent(in), optional :: puntualGroup
		
		this%name = "undefined"
		if ( present(name) ) this%name = trim(name)
		
		this%puntualGroup = "undefined"
		this%methodName = "undefined"
		if ( present( puntualGroup ) )	this%name = trim( puntualGroup )
!		call MecanicProperties_constructor( this%mecProperties )
		this%isInstanced = .true.
		
		MolecularSystem_instancePtr => null()
		MolecularSystem_instancePtr => this
		this%numberOfParticles = 0
			
	end subroutine MolecularSystem_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine MolecularSystem_destructor( this )
		implicit none
		type(MolecularSystem) :: this
		
		this%isInstanced = .false.
		MolecularSystem_instancePtr => null()
		this%numberOfParticles = 0

		if ( MecanicProperties_isInstanced(this%mecProperties) ) &
			call MecanicProperties_destructor( this%mecProperties )
			
		call ExternalPotentialManager_destructor(externalPotentialManager_instance)

	end subroutine MolecularSystem_destructor
	
	!**
	! @brief Muestra cierta informacion asociada al sistema molecular 
	!
	!**
	subroutine MolecularSystem_showInformation()
		implicit none
		
		type(Exception) :: ex
		
		if ( MolecularSystem_instance%isInstanced ) then
		
			print *,""
			print *," MOLECULAR SYSTEM: ",trim(MolecularSystem_instance%name)
			print *,"-----------------"
			print *,""
			write (6,"(T5,A16,A)") "DESCRIPTION   : ", trim( InputParser_getSystemDescription() )
			write (6,"(T5,A16,F5.1)") "CHARGE        : ",MolecularSystem_instance%charge
			write (6,"(T5,A16,A30)") "PUNTUAL GROUP : ",MolecularSystem_instance%puntualGroup
			print *,""

		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the ShowInformation function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end subroutine MolecularSystem_showInformation
	
	
	!>
	!! @brief Muestra una matriz cartesianas de las particulas del sistema
	!<
	subroutine MolecularSystem_showCartesianMatrix()
		implicit none
		
		integer :: i
		real(8) :: origin(3)
		
		write (6,"(A10,A16,A20,A20)") " ","<x>","<y>","<z>"
		do i = 1, ParticleManager_getTotalNumberOfParticles()
			
				origin = ParticleManager_getOrigin( iterator = i ) * AMSTRONG
				
				write (6,"(A10,<3>F20.10)") trim( ParticleManager_getSymbol( iterator = i ) ), origin(1), origin(2), origin(3)
		
		end do
		print *," "

	end subroutine MolecularSystem_showCartesianMatrix
	
	!>
	!! @Construye y mustra una matriz Z de coordenadas internas
	!>
	subroutine MolecularSystem_showZMatrix( this )
		implicit none
		type(MolecularSystem) :: this
		
		call InternalCoordinates_constructor(  this%intCoordinates )
		call InternalCoordinates_obtainCoordinates( this%intCoordinates )
		call InternalCoordinates_destructor(  this%intCoordinates )
		
	end subroutine MolecularSystem_showZMatrix
	
	!>
	!! @brief Imprime una matriz de distancias entre particulas presentes en el sistema
	!<
	subroutine MolecularSystem_showDistanceMatrix()
		implicit none
		
		type(Matrix) :: auxMatrix
		write (6,*) ""
		write (6,"(T20,A30)") " MATRIX OF DISTANCE: AMSTRONG"	
		write (6,"(T18,A35)") "------------------------------------------"
		
		auxMatrix=ParticleManager_getDistanceMatrix()
		auxMatrix%values = auxMatrix%values * AMSTRONG
		call Matrix_show(auxMatrix, rowKeys=ParticleManager_getLabelsOfCentersOfOptimization( LABELS_NUMERATED ),&
			 columnKeys = ParticleManager_getLabelsOfCentersOfOptimization( LABELS_NUMERATED), flags=WITH_BOTH_KEYS  )
	
		call Matrix_destructor(auxMatrix)
		
	end subroutine MolecularSystem_showDistanceMatrix


	
	!**
	! @brief Muestra cierta informacion asociada al sistema molecular 
	!
	!**
	subroutine MolecularSystem_showEigenVectors()
		implicit none
		
		type(Exception) :: ex
		integer :: i
		integer :: j
		character(20) :: auxString
		character(12),allocatable :: labels(:)
		
		if ( MolecularSystem_instance%isInstanced .and. associated(MolecularSystem_instance%coefficientsOfCombinationPtr) ) then
			
			 
			
			print *,""
			print *," EIGENVALUES AND EIGENVECTORS BY MOLECULAR SYSTEM: "
			print *,"================================================="
			print *,""
			
			do i= 1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
				
				
				print *,""
				print *," Eigenvectors for: ", trim( MolecularSystem_instance%nameOfSpeciesPtr(i) )
				print *,"-----------------"
				print *,""
				call Matrix_show( molecularsystem_instance%coefficientsofcombinationptr(i), &
					rowkeys = particlemanager_getlabelsofcontractions( i ), &
					columnkeys=string_convertvectorofrealstostring( molecularsystem_instance%energyofmolecularorbitalptr(i) ),&
					flags=WITH_BOTH_KEYS)

				print *,""
				print *," end of eigenvectors "

			end do
		
			print *,""
			print *," END OF EIGENVALUES AND EIGENVECTORS"
			print *,""
		
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the showWaveFucntion function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end subroutine MolecularSystem_showEigenVectors
	
	!**
	! @brief Muestra cierta informacion asociada al sistema molecular 
	!
	!**
	subroutine MolecularSystem_showEnergyComponents()
		implicit none
		
		type(Exception) :: ex
		integer :: i
		
		if ( MolecularSystem_instance%isInstanced .and. associated(MolecularSystem_instance%coefficientsOfCombinationPtr) ) then
			
			print *,""
			print *," ENERGY COMPONENTS: "
			print *,"=================="
			print *,""
			write (6,"(T10,A28,F20.10)") "TOTAL KINETIC ENERGY      = ", MolecularSystem_instance%kineticEnergy
			write (6,"(T10,A28,F20.10)") "TOTAL POTENTIAL ENERGY    = ", MolecularSystem_instance%potentialEnergy
			write (6,"(T10,A50)") "________________"
			write (6,"(T10,A28,F20.12)") "TOTAL ENERGY = ", MolecularSystem_instance%totalEnergy
			print *,""
			write (6,"(T10,A28,F20.10)") "VIRIAL RATIO (V/T) = ", - ( MolecularSystem_instance%potentialEnergy / MolecularSystem_instance%kineticEnergy)
			print *,""
			
			print *," COMPONENTS OF KINETIC ENERGY: "
			print *,"-----------------------------"
			print *,""
			
			do i=1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
				
				write (6,"(T10,A8,A20,F20.10)") trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ), &
					" Kinetic energy   = ", MolecularSystem_instance%kineticEnergyPtr(i)
			end do
			
			write (6,"(T10,A50)") "________________"
			write (6,"(T10,A28,F20.10)") "Total kinetic energy = ", MolecularSystem_instance%kineticEnergy
		
			print *,""
			print *," COMPONENTS OF POTENTIAL ENERGY: "
			print *,"-------------------------------"
			print *,""
			write (6,"(T10,A28,F20.10)") "Fixed potential energy    = ", MolecularSystem_instance%puntualInteractionEnergy
			write (6,"(T10,A28,F20.10)") "Q/Fixed potential energy  = ", MolecularSystem_instance%quantumPuntualInteractionEnergy
			write (6,"(T10,A28,F20.10)") "Coupling energy           = ", MolecularSystem_instance%couplingEnergy
			write (6,"(T10,A28,F20.10)") "Repulsion energy          = ", MolecularSystem_instance%repulsionEnergy
			write (6,"(T10,A50)") "________________"
			write (6,"(T10,A28,F20.10)") "Total potential energy = ", MolecularSystem_instance%potentialEnergy
			
			print *,""
			print *," Repulsion energy: "
			print *,"------------------"
			print *,""
			do i=1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
				
				write (6,"(T10,A26,A2,F20.10)") trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ) // "/" // &
					trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ) // &
					" Repulsion energy  ","= ", MolecularSystem_instance%repulsionEnergyPtr(i)
			end do
			
			print *,""
			print *," Coupling energy: "
			print *,"----------------"
			print *,""
			do i=1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
				
				write (6,"(T10,A26,A2,F20.10)") trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ) // &
					" Coupling energy  ","= ", MolecularSystem_instance%couplingEnergyPtr(i)
			end do
			
			print *,""
			print *," Quantum/Fixed interaction energy: "
			print *,"-----------------------"
			print *,""
			do i=1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
				
				write (6,"(T10,A26,A2,F20.10)") trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ) // &
					"/Fixed interact. energy ","= ", MolecularSystem_instance%quantumPuntualInteractionEnergyPtr(i)
			end do
			
			print *,""
			print *," END ENERGY COMPONENTS"
			print *,""
		
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the showWaveFucntion function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end subroutine MolecularSystem_showEnergyComponents
	
	
	subroutine MolecularSystem_showPopulationAnalyses()
		implicit none
		
		real(8) :: total
		character(10) :: specieName		
		integer :: i,specieID
		logical :: showPopulations
		
		specieID=1
		!! Recorre las especies buscando electrones
		
		search_specie: do i = 1, ParticleManager_getNumberOfQuantumSpecies()
			specieName=""
			specieName = trim(ParticleManager_getNameOfSpecie( i ) )
			
			if( scan(trim(specieName),"e")==1 ) then
				if( scan(trim(specieName),"-")>1 ) then
					showPopulations=.true.
					specieID=i
					exit search_specie
				end if
			else
				showPopulations=.false.
			end if
		
		end do search_specie
		
		if( showPopulations ) then
			!!Obtiene Poblaciones de Mulliken
			print *,""
			print *," POPULATION ANALYSES: "
			print *,"===================="
			print *,""
			print *, " Mulliken Population:"
			print *,"---------------------"
			print *,""
			call Vector_show( MolecularSystem_getPopulation( MULLIKEN, total,trim(specieName)),&
				flags = VERTICAL+WITH_KEYS, keys=ParticleManager_getLabelsOfContractions( specieID ) )
			
			write (6,"(T25,A10)") "__________"
			write (6,"(T10,A15,F10.6)") "Total = ", total		
			print *,""
			print *,"...end of Mulliken Population"
			print *,""
			print *, " Lowdin Population:"
			print *,"---------------------"
			print *,""
			call Vector_show( MolecularSystem_getPopulation( LOWDIN, total,trim(specieName)),&
				flags = VERTICAL+WITH_KEYS, keys=ParticleManager_getLabelsOfContractions( specieID ) )
			write (6,"(T25,A10)") "__________"
			write (6,"(T10,A15,F10.6)") "Total = ", total		
			print *,""
			print *,"...end of Lowdin Population"
			print *,""
			print *,"END POPULATION ANALYSES "
			print *,""
		end if
				
	end subroutine MolecularSystem_showPopulationAnalyses
	
	!**
	! @brief Muestra cargas parciales
	!
	!**
	subroutine MolecularSystem_showCharges()
		implicit none
		
		real(8) :: total
				integer :: particlesIterator
		integer :: owner
		integer :: numberOfNames
		integer :: numberOfParticles
		character(10), allocatable :: names(:)
		character(10), allocatable :: auxNames(:)
		
		numberOfParticles = ParticleManager_getTotalNumberOfParticles()
		numberOfNames = 0	
		allocate( auxNames(numberOfParticles) )
		
		do particlesIterator = 1 , numberOfParticles
			
			owner= ParticleManager_getOwnerCenter( particlesIterator)
				
			if ( particlesIterator == owner ) then
				
				numberOfNames = numberOfNames + 1
				write (auxNames(numberOfNames), "(A10)" ) trim( ParticleManager_getSymbol( particlesIterator ) )
			
			end if
			
		end do
		
		allocate( names(numberOfNames) )
		names=auxNames(1:numberOfNames)
		deallocate( auxNames )
		
		!!Obtiene Poblaciones de Mulliken
		print *,""
		print *," PARTIAL CHARGES: "
		print *,"===================="
		print *,""
		print *, " Mulliken Charges:"
		print *,"---------------------"
		print *,""
		call Vector_show( MolecularSystem_getPartialCharges( MULLIKEN), flags = VERTICAL+WITH_KEYS, keys=names )
		print *,""
		print *, " Lowdin Charges:"
		print *,"---------------------"
		print *,""
		call Vector_show( MolecularSystem_getPartialCharges( LOWDIN), flags = VERTICAL+WITH_KEYS, keys=names )
		print *,""
		print *,"END PARTIAL CHARGES "
		print *,""
				
		deallocate( names )
				
	end subroutine MolecularSystem_showCharges
	
	!**
	! @brief Ajusta el nombre del sistema especificado
	!
	!**
	subroutine MolecularSystem_setName( nameOfMolecularSystem )
		implicit none
		character(*) :: nameOfMolecularSystem
		
		MolecularSystem_instance%name = trim(nameOfMolecularSystem)

	end subroutine MolecularSystem_setName
	
	!**
	! @brief Ajusta el nombre del sistema especificado
	!
	!**
	subroutine MolecularSystem_setMethodName( nameOfMethod )
		implicit none
		character(*) :: nameOfMethod
		
		MolecularSystem_instance%methodName = trim(nameOfMethod)

	end subroutine MolecularSystem_setMethodName

	
	!**
	! @brief Ajusta la energia total sistema especificado
	!
	!**
	subroutine MolecularSystem_setTotalEnergy( totalEnergy )
		implicit none
		real(8), intent(in) :: totalEnergy
		
		MolecularSystem_instance%totalEnergy = totalEnergy

	end subroutine MolecularSystem_setTotalEnergy
	
	!**
	! @brief Ajusta la energia total de acoplamiento  
	!
	!**
	subroutine MolecularSystem_setTotalCouplingEnergy( totalCouplingEnergy )
		implicit none
		real(8), intent(in) :: totalCouplingEnergy
		
		MolecularSystem_instance%couplingEnergy = totalCouplingEnergy

	end subroutine MolecularSystem_setTotalCouplingEnergy
	
	!**
	! @brief Ajusta el nombre del sistema especificado
	!
	!**
	subroutine MolecularSystem_setPuntualGroup(puntualGroup )
		implicit none
		character(*) :: puntualGroup
		
		MolecularSystem_instance%puntualGroup = trim(puntualGroup)

	end subroutine MolecularSystem_setPuntualGroup
	
	!**
	! @brief 	Ajusta apuntadores asociados a vectores propios de
	!		la matriz de Fock
	!
	!**
	subroutine MolecularSystem_setCoefficientsOfCombinationPtr(  coefficientsOfCombination )
		implicit none
		type(Matrix), target, optional :: coefficientsOfCombination(:)
		
		if ( present( coefficientsOfCombination) ) then
			MolecularSystem_instance%coefficientsOfCombinationPtr => coefficientsOfCombination
		else
			MolecularSystem_instance%coefficientsOfCombinationPtr => null()
		end if

	end subroutine MolecularSystem_setCoefficientsOfCombinationPtr
	
	!**
	! @brief 	Ajusta apuntadores a matrices de densidas de las especies presentes
	!		en el sistema 
	!
	!**
	subroutine MolecularSystem_setDensityMatrixPtr(  densityMatrices )
		implicit none
		type(Matrix), target, optional :: densityMatrices(:)
		
		if ( present(densityMatrices) ) then
			MolecularSystem_instance%densityMatrixPtr => densityMatrices
		else
			MolecularSystem_instance%densityMatrixPtr=> null()
		end if
		
	end subroutine MolecularSystem_setDensityMatrixPtr
	
	!**
	! @brief 	Ajusta apuntadores asociados a valores propios de
	!		la matriz de Fock
	!
	!**
	subroutine MolecularSystem_setEnergyOfMolecularOrbitalsPtr(  energyOfmolecularOrbitals )
		implicit none
		type(Vector), target,optional :: energyOfmolecularOrbitals(:)
		
		if ( present(energyOfmolecularOrbitals) ) then
			MolecularSystem_instance%energyOfmolecularOrbitalPtr => energyOfmolecularOrbitals
		
		else

			MolecularSystem_instance%energyOfmolecularOrbitalPtr => null()
		
		end if
		

	end subroutine MolecularSystem_setEnergyOfMolecularOrbitalsPtr
	
	!**
	! @brief 	Ajusta apuntadores asociados a valores propios de
	!		la matriz de Fock
	!
	!**
	subroutine MolecularSystem_setEnergyComponentsPtr(  nameOfSpecies, kineticEnergy, repulsionEnergy, &
				couplingEnergy, quantumPuntualInteractionEnergy )
		implicit none
		character(30), target, optional :: nameOfSpecies(:)
		real(8), target, optional  :: kineticEnergy(:)
		real(8), target, optional :: repulsionEnergy(:)
		real(8), target, optional :: couplingEnergy(:)
		real(8), target, optional  :: quantumPuntualInteractionEnergy(:)
		
		if ( present(nameOfSpecies) ) then
			MolecularSystem_instance%nameOfSpeciesPtr=>null()
			MolecularSystem_instance%nameOfSpeciesPtr=> nameOfSpecies
		else
			MolecularSystem_instance%nameOfSpeciesPtr=>null()
		end if 
		
		if ( present( kineticEnergy) ) then
			MolecularSystem_instance%kineticEnergyPtr => null()
			MolecularSystem_instance%kineticEnergyPtr => kineticEnergy
		else
			MolecularSystem_instance%kineticEnergyPtr => null()
		end if
		
		if ( present(repulsionEnergy) ) then
			MolecularSystem_instance%repulsionEnergyPtr => null()
			MolecularSystem_instance%repulsionEnergyPtr => repulsionEnergy
		else
			MolecularSystem_instance%repulsionEnergyPtr => null()
		end if
			
		if ( present(couplingEnergy) ) then
			MolecularSystem_instance%couplingEnergyPtr => null()
			MolecularSystem_instance%couplingEnergyPtr =>couplingEnergy
		else
			MolecularSystem_instance%couplingEnergyPtr => null()
		end if
			
		if ( present( quantumPuntualInteractionEnergy ) ) then
			MolecularSystem_instance%quantumPuntualInteractionEnergyPtr => null()
			MolecularSystem_instance%quantumPuntualInteractionEnergyPtr => quantumPuntualInteractionEnergy
		else
			MolecularSystem_instance%quantumPuntualInteractionEnergyPtr => null()
		end if

	end subroutine MolecularSystem_setEnergyComponentsPtr
	
	!**
	! @brief 	Reubicar las particulas del sistema molecular
	!
	!**
	subroutine MolecularSystem_setOrigin( cartesianVector )
		implicit none
 		type(Vector),optional :: cartesianVector

	end subroutine MolecularSystem_setOrigin
	
	!**
	! @brief Retorna el grupo puntual del sistema molecular
	!
	!**
	subroutine MolecularSystem_loadOfInput()
		implicit none
		character(30) :: output
		
		type(Exception) :: ex
		type(InputParser_Particle) :: inputParticle
		type(InputParser_ExternalPotInfo) :: externalPotInfo
		logical :: isEndParticle
		integer :: i
		character(50), allocatable :: potentialName(:),interactionName(:), interactionType(:)
		


		if ( MolecularSystem_instance%isInstanced ) then
			
			call InputParser_constructor()
			call InputParser_setApmoParameters()
			call ParticleManager_constructor( InputParser_getNumberOfParticles() )
			MolecularSystem_instance%name = trim( InputParser_getSystemName() )
			MolecularSystem_instance%charge = InputParser_getSystemCharge()
			externalPotentialManager_instance%numberOfPots=InputParser_getNumberOfExtenalPots()

			!!*****************************************************************
			!! Carga el sistema molecular desde el archivo de entrada
			!!
			isEndParticle = .false.
			do while( .not.isEndParticle ) 
				
				inputParticle=InputParser_getParticle( isEndParticle )

				call ParticleManager_addParticle( name = inputParticle%name, baseName = inputParticle%basisSetName, &
					origin = inputParticle%origin, fix=inputParticle%fixedCoordinates, addParticles=inputParticle%additionOfParticles, &
					multiplicity=inputParticle%multiplicity, fragmentNumber=inputParticle%fragmentNumber )
						
			end do
			!!
			!!*****************************************************************
			
			
			!!*****************************************************************
			!! Carga potenciales externos
			!!
			if (externalPotentialManager_instance%numberOfPots > 0) then
				
				allocate(potentialName(externalPotentialManager_instance%numberOfPots))
				allocate(interactionName(externalPotentialManager_instance%numberOfPots))
				allocate(interactionType(externalPotentialManager_instance%numberOfPots))
				
				isEndParticle = .false.
				i=0
				do while( .not.isEndParticle ) 
					i=i+1
					externalPotInfo=InputParser_getExternalPotential( isEndParticle )
					interactionName(i)=trim(externalPotInfo%interactionName)
					interactionType(i)=trim(externalPotInfo%interactionType)
					potentialName(i)=trim(externalPotInfo%name)
				end do
		
				call ExternalPotentialManager_constructor( externalPotentialManager_instance,&
					potentialName, interactionName, interactionType )
				
				deallocate(potentialName)
				deallocate(interactionName)
				deallocate(interactionType)
			else
				externalPotentialManager_instance%isInstanced=.false.
			end if
			!!
			!!*****************************************************************
			
 			call InputParser_setExternalBasis()
			
			call InputParser_destructor()
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getPuntualGroup function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end subroutine MolecularSystem_loadOfInput
	
	!**
	! @brief Retorna el grupo puntual del sistema molecular
	!
	!**
	subroutine MolecularSystem_obtainEnergyComponents()
		implicit none
		
		type(Exception) :: ex
		
		if ( MolecularSystem_instance%isInstanced ) then
			
			MolecularSystem_instance%kineticEnergy = sum( MolecularSystem_instance%kineticEnergyPtr )
			MolecularSystem_instance%repulsionEnergy = sum( MolecularSystem_instance%repulsionEnergyPtr )
			MolecularSystem_instance%quantumPuntualInteractionEnergy = sum ( MolecularSystem_instance%quantumPuntualInteractionEnergyPtr )
			MolecularSystem_instance%puntualInteractionEnergy = ParticleManager_puntualParticlesEnergy()
			MolecularSystem_instance%potentialEnergy = 	MolecularSystem_instance%repulsionEnergy &
												+ MolecularSystem_instance%puntualInteractionEnergy &
												+ MolecularSystem_instance%quantumPuntualInteractionEnergy &
												+ MolecularSystem_instance%couplingEnergy

		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the obtainEnerguComponents function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end subroutine MolecularSystem_obtainEnergyComponents
	
	!**
	! @brief Retorna el nombre del sistema molecular
	!
	!**
	function MolecularSystem_getName() result( output )
		implicit none
		character(30) :: output
		
		type(Exception) :: ex
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output = trim( MolecularSystem_instancePtr%name )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getName function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getName
	
	!**
	! @brief Retorna el nombre del sistema molecular
	!
	!**
	function MolecularSystem_getMethodName() result( output )
		implicit none
		character(30) :: output
		
		type(Exception) :: ex
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output = trim( MolecularSystem_instancePtr%methodName )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getMethodName function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getMethodName
	
	
	!**
	! @brief Retorna la energia total del sistema molecular
	!
	!**
	function MolecularSystem_getTotalEnergy() result( output )
		implicit none
		real(8) :: output
		
		type(Exception) :: ex
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output = MolecularSystem_instance%totalEnergy
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getTotalEnergy function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getTotalEnergy
	
	!**
	! @brief Retorna el grupo puntual del sistema molecular
	!
	!**
	function MolecularSystem_getPuntualGroup() result( output )
		implicit none
		character(30) :: output
		
		type(Exception) :: ex
		
		if ( MolecularSystem_instance%isInstanced ) then
			
			output = trim( MolecularSystem_instancePtr%puntualGroup )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getPuntualGroup function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getPuntualGroup
	
	!**
	! @brief Retorna una matrix con los valores propios previamente calculados 
	! 	para una especie especificada
	!
	!**
	function MolecularSystem_getEigenVectors(speciesID) result( output )
		implicit none
		integer, intent(in) :: speciesID 
		type(Matrix) :: output
		
		type(Exception) :: ex
		
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output = MolecularSystem_instance%coefficientsOfCombinationPtr(speciesID)
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getEigenVectors(i) function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getEigenVectors
	
	!**
	! @brief Retorna una matrix con los valores propios previamente calculados 
	! 	para una especie especificada
	!
	!**
	function MolecularSystem_getEigenValues(speciesID) result( output )
		implicit none
		integer, intent(in) :: speciesID 
		type(Vector) :: output
		
		type(Exception) :: ex
		
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output = MolecularSystem_instance%energyOfmolecularOrbitalPtr(speciesID)
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getEigenValues(i) function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getEigenValues
	
	!**
	! @brief Retorna la energia del sistema molecular o la asociada 
	! 	a una especie especificad
	!
	!**
	function MolecularSystem_getEnergy(nameOfSpecie,typeOfEnergy) result( output )
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie 
		integer, optional, intent(in) :: typeOfEnergy
		real(8) :: output
		
		type(Exception) :: ex
		
		
		if ( associated(MolecularSystem_instancePtr) ) then
			
			output =0.0_8
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getEnergy(i) function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getEnergy
	
		
	
	!**
	! @brief  Retorna la matrix densidad aociada a la especie cu�ntica especificada 
	 function MolecularSystem_getDensityMatrix( nameOfSpecie ) result( output )
		implicit none
		character(*), optional, intent(in) :: nameOfSpecie 
		type(Matrix) :: output
		
		type(Exception) :: ex
		integer :: speciesID
		character(30) :: nameOfSpecieSelected

		if ( associated(MolecularSystem_instancePtr) ) then
			
			nameOfSpecieSelected = "e-"
			if ( present( nameOfSpecie ) )  nameOfSpecieSelected= trim( nameOfSpecie )
			speciesID =ParticleManager_getSpecieID(  nameOfSpecie = trim(nameOfSpecieSelected) )
			
			call Matrix_copyConstructor( output, MolecularSystem_instance%densityMatrixPtr(speciesID) )
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getDipoleMoment(i) function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getDensityMatrix
	
	
	!**
	! @brief Retorna la poblacion de Mulliken o Lowdin del sistema molecular 
	!**
	function MolecularSystem_getPopulation( typeOfPopulation, totalSum, nameOfSpecie) result( output )
		implicit none
		integer :: typeOfPopulation
		real(8), optional, intent(out) :: totalSum
		character(*),optional  :: nameOfSpecie
		type(Vector) :: output
		
		type(Matrix) :: densityMatrix
		type(Matrix) :: overlapMatrix
		type(Matrix) :: auxMatrix
		type(Matrix) :: auxMatrixB
		character(5) :: auxNameOfSpecie
		integer :: numberOfcontractions
		integer :: speciesID
		integer :: i
	
		type(Exception) :: ex
		
		auxNameOfSpecie="e-"
		if (present( nameOfSpecie ) )	then
			auxNameOfSpecie = trim(nameOfSpecie)
		end if
		
		if ( associated(MolecularSystem_instancePtr) ) then
			speciesID =ParticleManager_getSpecieID(  nameOfSpecie = trim(auxNameOfSpecie) )
			numberOfcontractions = ParticleManager_getTotalNumberOfContractions( speciesID )
			call Matrix_constructor( auxMatrix, int( numberOfcontractions, 8), int( numberOfcontractions, 8) )
			call Vector_constructor( output, numberOfcontractions   )
			
			
			select case( typeOfPopulation )
			
				case( MULLIKEN )
			
					overlapMatrix = IntegralManager_getMatrix( OVERLAP_INTEGRALS, trim(auxNameOfSpecie)  )
					densityMatrix = MolecularSystem_instance%densityMatrixPtr(speciesID)
					auxMatrix%values = matmul(densityMatrix%values, overlapMatrix%values )
				
				case (LOWDIN)
					
					overlapMatrix = IntegralManager_getMatrix( OVERLAP_INTEGRALS, trim(auxNameOfSpecie)  )
					densityMatrix = MolecularSystem_instance%densityMatrixPtr(speciesID)
					auxMatrix%values = matmul(densityMatrix%values, overlapMatrix%values )
					
					auxMatrix = Matrix_pow( overlapMatrix, 0.5_8 )
					auxMatrixB = auxMatrix
					auxMatrix%values = matmul( matmul( auxMatrixB%values , densityMatrix%values), auxMatrixB%values )

					call Matrix_destructor(auxMatrixB)	
					
				case default
			
			end select
			
			
			
			do i=1, numberOfcontractions
				output%values(i) = auxMatrix%values(i,i)
			end do
			
			if ( present( totalSum ) ) totalSum = sum(output%values)
			
			call Matrix_destructor(overlapMatrix)
			call Matrix_destructor(auxMatrix)
			call Matrix_destructor(auxMatrixB)
			call Matrix_destructor(densityMatrix)
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getPopulation function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getPopulation
	
	
	!**
	! @brief Retorna las cargas parciales de Mulliken o Lowdin del sistema molecular 
	!**
	function MolecularSystem_getPartialCharges( typeOfCharge ) result( output )
		implicit none
		integer, optional :: typeOfCharge
		type(Vector) :: output

		
		type(Vector) :: population
		type(Vector) :: auxVector
		character(30) :: nameOfSpecie
		integer :: auxtypeOfCharge
		integer :: owner
		integer :: otherOwner
		integer :: specieID
		integer :: speciesIterator
		integer :: numberOfcontractions
		integer :: numberOfCenters
		real(8) :: auxCharge
		integer :: i, k
		integer :: j
		integer, allocatable :: labels(:)
		
		type(Exception) :: ex
		
		
		numberOfCenters = 0
		auxtypeOfCharge = MULLIKEN
		if ( present( typeOfCharge) ) auxtypeOfCharge = typeOfCharge
		
		call Vector_constructor( auxVector,ParticleManager_getTotalNumberOfParticles() )
		
		if ( associated(MolecularSystem_instancePtr) ) then
		
			do speciesIterator = 1 , ParticleManager_getTotalNumberOfParticles()
	
				owner= ParticleManager_getOwnerCenter( speciesIterator )
				
				!! Limita el calculo a particulas propietarias
				if ( speciesIterator == owner ) then
					
					numberOfCenters = numberOfCenters + 1
					
					auxVector%values(numberOfCenters) =0.0_8
					if( .not.ParticleManager_isQuantum( speciesIterator ) ) &
						auxVector%values(numberOfCenters) = ParticleManager_getCharge( iterator=speciesIterator)

					do i = 1, ParticleManager_getNumberOfQuantumSpecies()
					
						specieID = i
						nameOfSpecie = trim(ParticleManager_getNameOfSpecie( specieID ) )
						numberOfContractions = ParticleManager_getNumberOfContractions( specieID )

						!!Contraction labels for integrals
						if(allocated(labels)) deallocate(labels)
						allocate(labels(numberOfContractions))

						labels = IntegralManager_getLabels(specieID, numberOfContractions)
						
						if ( auxtypeOfCharge ==MULLIKEN  ) then
							population = MolecularSystem_getPopulation(MULLIKEN,nameOfSpecie=trim(nameOfSpecie) )
						else
							population = MolecularSystem_getPopulation(LOWDIN,nameOfSpecie=trim(nameOfSpecie) )
						end if
			
						auxCharge = 0.0_8
						do j =1, numberOfContractions
							
							if( owner == ContractedGaussian_getOwner( &
								ParticleManager_getContractionPtr( &
								specieID,numberOfContraction=j) ) ) then

								do k=1, ContractedGaussian_getNumberOfCartesianOrbitals(ParticleManager_getContractionPtr( specieID,  numberOfContraction=j ))

									auxCharge = auxCharge + population%values(labels(j) + k -1)

								end do

							end if
				
						end do
						
						auxVector%values(numberOfCenters) = auxVector%values(numberOfCenters) &
							+ ParticleManager_getCharge( specieID ) * auxCharge
						
					end do
					
					call Vector_destructor(population)

				end if

			end do
					
			

			call Vector_constructor(output,numberOfCenters)
			output%values(1:numberOfCenters) = auxVector%values(1:numberOfCenters)
			call Vector_destructor(auxVector)
			
			
		else
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object MolecularSystem in the getPopulation function" )
			call Exception_setDescription( ex, "You should instance the molecular system before use this function" )
			call Exception_show( ex )
		end if

	end function MolecularSystem_getPartialCharges
	
	!**
	! @brief Devuelve la carga del sistema molecular 
	! 	a una especie especificad
	!
	! @todo Falta por implementar
	!**
	function MolecularSystem_getCharge() result( output )
		implicit none
		real(8) :: output
		
		type(Exception) :: ex
		
	
		output = MolecularSystem_instance%charge
			

	end function MolecularSystem_getCharge
	
	
	!**
	! @brief Indica si el sistema molecula ha sido intanciado
	!**
	function MolecularSystem_isInstanced() result(output)
		implicit none
		logical :: output
		
		output = MolecularSystem_instance%isInstanced
	
	end function MolecularSystem_isInstanced
	
	!>
	!! @brief Traslada el origen del sistema molecular al centro de masa
	!>
	subroutine MolecularSystem_moveToCenterOfMass(this)
		implicit none
		type(MolecularSystem) :: this

		if ( .not.MecanicProperties_isInstanced(this%mecProperties) ) &
			call MecanicProperties_constructor( this%mecProperties )
		
		this%mecProperties%centerOfMass = MecanicProperties_getCenterOfMass( this%mecProperties )
		call ParticleManager_changeOriginOfSystem( this%mecProperties%centerOfMass )
		
	end subroutine MolecularSystem_moveToCenterOfMass
	
	!>
	!! @brief Traslada el origen del sistema molecular al centro de masa
	!>
	subroutine MolecularSystem_rotateOnPrincipalAxes(this)
		implicit none
		type(MolecularSystem) :: this

		type(Matrix) :: matrixOfCoordinates
		type(Vector) :: vectorOfCoordinates
		real(8), allocatable :: auxVector(:)
		real(8) :: coordinates(3)
		integer :: i
		integer :: numOfzeros(2)

		if ( .not.MecanicProperties_isInstanced(this%mecProperties) ) &
			call MecanicProperties_constructor( this%mecProperties )
		
		this%mecProperties%molecularInertiaTensor = MecanicProperties_getMolecularInertiaTensor( this%mecProperties )
		matrixOfCoordinates = ParticleManager_getCartesianMatrixOfCentersOfOptimization()

		do i=1, size(matrixOfCoordinates%values,dim=1)

			!!
			!! Proyecta las coordenadas cartesianas sobre el tensor de inercia molecular
			!!
			coordinates(1)=dot_product(matrixOfCoordinates%values(i,:),this%mecProperties%molecularInertiaTensor%values(:,3) )
			coordinates(2)=dot_product(matrixOfCoordinates%values(i,:),this%mecProperties%molecularInertiaTensor%values(:,2) )
			coordinates(3)=dot_product(matrixOfCoordinates%values(i,:),this%mecProperties%molecularInertiaTensor%values(:,1) )
			matrixOfCoordinates%values(i,:) = coordinates
		end do
		
		numOfzeros=0
		do i=1, size(matrixOfCoordinates%values,dim=1)
			if( abs(matrixOfCoordinates%values(i,3)) < 1.0D-6  ) numOfzeros(1) = numOfzeros(1)+1
			if( abs(matrixOfCoordinates%values(i,2)) < 1.0D-6  ) numOfzeros(2) = numOfzeros(2)+1
		end do
		
		if ( numOfzeros(1) > numOfzeros(2) ) then
			allocate( auxVector( size(matrixOfCoordinates%values,dim=1) ) )
			auxVector=matrixOfCoordinates%values(:,2)
			matrixOfCoordinates%values(:,2) = matrixOfCoordinates%values(:,3)
			matrixOfCoordinates%values(:,3) = auxVector
			deallocate( auxVector )
		end if

		!! Invierte la orientacion sobre el eje Z
		matrixOfCoordinates%values(:,3)=-1.0*matrixOfCoordinates%values(:,3)
				
		call Vector_constructor( vectorOfCoordinates, size( matrixOfCoordinates%values,dim=1)*3 )
		do i=1, size(matrixOfCoordinates%values,dim=1)
			vectorOfCoordinates%values(3*i-2:3*i)=matrixOfCoordinates%values(i,:)
		end do
		

	

		!! Realiza la rotacion de las pariculas
		call ParticleManager_setParticlesPositions( vectorOfCoordinates )
		
		call Vector_destructor( vectorOfCoordinates )
		call Matrix_destructor( matrixOfCoordinates )

	end subroutine MolecularSystem_rotateOnPrincipalAxes


	subroutine MolecularSystem_calculateElectronicEffect( totalEnergy )
		implicit none
		real(8) :: totalEnergy

		real(8) :: puntualEnergy
		real(8) :: atractionEnergy
		real(8), allocatable :: interactionMatrix(:,:)
		integer :: specieIndex
		integer :: i

		print *,""
		print *,"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		print *,"+     ENERGY ANALYSIS ON ELECTRONIC WAVE FUNCTION / FIXING NUCLEOUS     +"
		print *,"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		print *,""

		
		call  ParticleManager_fixAllNucleous()
		puntualEnergy=ParticleManager_puntualParticlesEnergy()
		totalEnergy=puntualEnergy
	
		write (6,"(T10,A26,A2,F20.10)") " Nuclear repulsion energy  ","= ", puntualEnergy
		do i=1, size(MolecularSystem_instance%coefficientsOfCombinationPtr)
		
			if ( trim( MolecularSystem_instance%nameOfSpeciesPtr(i) ) =="e-") then
				write (6,"(T10,A26,A2,F20.10)")  &
					" Kinetic energy ","= ", MolecularSystem_instance%kineticEnergyPtr(i)
				totalEnergy=totalEnergy+ MolecularSystem_instance%kineticEnergyPtr(i)
				write (6,"(T10,A26,A2,F20.10)")  &
					" Repulsion energy ","= ", MolecularSystem_instance%repulsionEnergyPtr(i)
				totalEnergy=totalEnergy+ MolecularSystem_instance%repulsionEnergyPtr(i)

				exit
			end if
		end do

		specieIndex =ParticleManager_getSpecieID(  nameOfSpecie ="e-" )
		call IntegralManager_calculateInteractionMatrix("e-", interactionMatrix )
		atractionEnergy=sum( transpose( MolecularSystem_instance%densityMatrixPtr(specieIndex)%values ) * interactionMatrix )
		totalEnergy = totalEnergy + atractionEnergy
		write (6,"(T10,A26,A2,F20.10)") " Atraction energy ","= ", atractionEnergy
		write (6,"(T10,A50)") "______________________"
		write (6,"(T10,A28,ES20.12)") "TOTAL ELECTRONIC ENERGY = ", totalEnergy
		print *,""

	end subroutine MolecularSystem_calculateElectronicEffect

end module MolecularSystem_
