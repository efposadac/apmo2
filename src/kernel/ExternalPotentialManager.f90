!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module ExternalPotentialManager_
	use ExternalPotential_
	use XMLParser_
	use Exception_
	use InputOutput_
	use APMO_
	use Units_
	implicit none

	!>
	!! @brief Description
	!!
	!! @author authorName
	!!
	!! <b> Creation data : </b> MM-DD-YYYYY
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> MM-DD-YYYYY </tt>:  authorName ( email@server )
	!!        -# description.
	!!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
	!!        -# description
	!!
	!<
	type, public :: ExternalPotentialManager
		character(20) :: name
		logical :: isInstanced
		integer :: numberOfPots
		type(ExternalPotential), allocatable :: externalsPots(:)
	end type

	type(ExternalPotentialManager), public :: externalPotentialManager_instance

	public :: &
		ExternalPotentialManager_constructor, &
		ExternalPotentialManager_destructor, &
		ExternalPotentialManager_show, &
		ExternalPotentialManager_draw2DPotential
		
private		
contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine ExternalPotentialManager_constructor(this, potentialName, interactionsName,  interactionsType)
		implicit none
		type(ExternalPotentialManager) :: this
		character(*) :: potentialName(:)
		character(*) :: interactionsName(:)
		character(*) :: interactionsType(:)
		integer :: i
		
		this.numberOfPots=size(interactionsName)
		this.isInstanced = .true.
		
		allocate(this.externalsPots(this.numberOfPots))
		
		do i=1,this.numberOfPots
		
			call ExternalPotential_constructor( this.externalsPots(i), trim(potentialName(i)), trim(interactionsType(i)) )

			call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // &
          			trim(APMO_instance.POTENTIALS_DATABASE)//trim(potentialName(i))// ".xml"), &
          			trim(interactionsName(i)), eexternalPotential=this.externalsPots(i) )

		end do
	
	end subroutine ExternalPotentialManager_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine ExternalPotentialManager_destructor(this)
		implicit none
		type(ExternalPotentialManager) :: this
		
		integer :: i

		if( this.isInstanced ) then
		
			do i=1,this.numberOfPots
		
				call ExternalPotential_destructor( this.externalsPots(i) )

			end do
		
		end if


		this.isInstanced = .false.

	end subroutine ExternalPotentialManager_destructor

	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	subroutine ExternalPotentialManager_show(this)
		implicit none
		type(ExternalPotentialManager) :: this
	end subroutine ExternalPotentialManager_show

	!!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function ExternalPotentialManager_isInstanced( this ) result( output )
		implicit  none
		type(ExternalPotentialManager), intent(in) :: this
		logical :: output
		
		output = this.isInstanced
	
	end function ExternalPotentialManager_isInstanced
	
	
	!!>
	!! @brief genera curvas de los potenciales externos
	!!
	!<
	subroutine ExternalPotentialManager_draw2DPotential( this )
		implicit none
		type(ExternalPotentialManager) :: this
		integer :: i,j,status
		character(100) :: fileName

		    do i=1,size(this.externalsPots)

			 fileName= trim(this.externalsPots(i).name)//'-'//trim(this.externalsPots(i).interactionName)
			 open ( 5,FILE=trim(fileName)//".dat", STATUS='REPLACE',ACTION='WRITE')
			 do j=-APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,&
			      APMO_instance.MAXIMUM_RANGE_OF_GRAPHS,1
				   write (5,"(ES20.10,ES20.10)") j*APMO_instance.STEP_OF_GRAPHS, &
				   ExternalPotential_getPotential(this.externalsPots(i),[j*APMO_instance.STEP_OF_GRAPHS,0.0_8,0.0_8])
			 end do
			 close(5)
			 call InputOutput_make2DGraph(trim(fileName),&
			      "GUAUSSIAN POTENTIAL MODEL",&
			      "r / Bohr",&
			      "U /Hartree")
		    end do
		    
	
	end subroutine ExternalPotentialManager_draw2DPotential
	
	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine ExternalPotentialManager_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription
	
		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )
	
	end subroutine ExternalPotentialManager_exception
	
	
end module ExternalPotentialManager_
