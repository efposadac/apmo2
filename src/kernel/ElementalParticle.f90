!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    thisPtr files is part of nonBOA                                                 !
!!                                                                                 !
!!    thisPtr program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    thisPtr program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with thisPtr program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de datos de particulas elemetales
! 
!  Este modulo define una seudoclase para manejo de datos  correspondientes
!  a particulas elementales.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see XMLParser_
!
!**
module ElementalParticle_

	implicit none
	
	type , public :: ElementalParticle
		character(30) :: name
		character(30) :: symbol
		character(30) :: category
		real(8) :: mass
		real(8) :: charge
		real(8) :: spin
		
		logical :: isInstanced	!< Dispuesta por razones de conveniencia
		
	end type ElementalParticle

	private ::
		type(ElementalParticle), pointer :: thisPtr
		integer :: i
		integer :: j
		character(20) :: bblockName
		character(20) :: currentBlockName
		character(20) :: currentBlockSymbol
	
	public :: &
		ElementalParticle_constructor, &
		ElementalParticle_destructor, &
		ElementalParticle_show, &
		ElementalParticle_XMLParser_startElement, &
		ElementalParticle_XMLParser_endElement
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine ElementalParticle_constructor(this)
		implicit none
		type(ElementalParticle) :: this
		
		this.name	=	""
		this.symbol	=	""
		this.category	=	""
		this.mass		=	0.0_8
		this.charge	=	0.0_8
		this.spin		=	0.0_8
		this.isInstanced=	.false.
		
	end subroutine ElementalParticle_constructor
	
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine ElementalParticle_destructor()
		implicit none
		
		if( associated(thisPtr) ) nullify(thisPtr)
		
	end subroutine ElementalParticle_destructor
	
		
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine ElementalParticle_show( this )
		implicit none
		type(ElementalParticle) , intent(in) :: this
		
		
		print *,""
		print *,"====================="
		print *,"  Particle Properties"
		print *,"====================="
		print *,""
		write (6,"(T10,A10,A12)") "Name    = ",this.name
		write (6,"(T10,A10,A12)") "Symbol  = ",this.symbol
		write (6,"(T10,A10,F12.5)") "Mass    = ",this.mass
		write (6,"(T10,A10,F12.5)") "Charge  = ",this.charge
		write (6,"(T10,A10,F12.5)") "Spin    = ",this.spin
		print *,""
		
	end subroutine ElementalParticle_show

	
	subroutine ElementalParticle_XMLParser_startElement (this, tagName, dataName, dataValue, blockName)
		implicit none
		type(ElementalParticle), target, intent(inout) :: this
		character(*) :: tagName
		character(*) :: dataName
		character(*) :: dataValue
		character(*) :: blockName
		
		logical :: aux = .false.
		
		if( associated( thisPtr) ) nullify(thisPtr)
		thisPtr => this
		
	
		select case ( trim(tagName) ) 

			case ("particle")
				
				select case ( trim(dataName) )
					
					case("name")
									
						currentBlockName = trim(dataValue)
						
					case("symbol")
									
						currentBlockSymbol = trim(dataValue)
						
				end select
		end select
		
		if ( ( trim( blockName ) == currentBlockName ) .or. ( trim( blockName ) == currentBlockSymbol ) ) then
		
			
			select case ( trim(tagName) )
			
				case ("particle")
				
					
					select case ( trim(dataName) )
						
						
						case("name")
							
							thisPtr.name = trim(dataValue)
						
						case("symbol")
								
							thisPtr.symbol = trim(dataValue)
							thisPtr.name = currentBlockName
							
						case("category")
										
							thisPtr.category= trim( dataValue )
							
					end select
	
				case ("parameters")
				
					select case ( trim(dataName) )
						
						case("charge")
										
							thisPtr.charge = DNUM(dataValue)
							
						case("mass")
										
							thisPtr.mass = DNUM(dataValue)
							
						case("spin")
										
							thisPtr.spin = DNUM( dataValue )
							
					end select
	
			end select
		
		end if
		
	end subroutine ElementalParticle_XMLParser_startElement
	
	subroutine ElementalParticle_XMLParser_endElement( tagName)
		implicit none
		character(*) :: tagName
		
		select case ( trim(tagName) ) 

			case ("particle")
				
				currentBlockName = ""
				currentBlockSymbol = ""
				 
				 if ( thisPtr.name /= "")  then
				 	thisPtr.isInstanced = .true.
			  	end if
				
		end select
		
	end subroutine ElementalParticle_XMLParser_endElement
	
end module ElementalParticle_