!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief 	Permite definicr interfases ha miminizadores internos y/o externos
! 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-10 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulo y m�todos
!
!**
module Minimizer_
	use APMO_
	use ParticleManager_
	use RHFSolver_
	use EnergyDerivator_
	use GenericMinimizer_
	use GSLInterface_
	use Exception_
	use MollerPlesset_
	
	implicit none
	
	!< enum Matrix_type {
	integer, parameter, public :: MINIMIZER_FUNCTION_OF_ENERGY =  0
	integer, parameter, public :: MINIMIZER_FUNCTION_OF_DENSITY =  1
	!< }
	
	type, public :: Minimizer
		
		character(30) :: name
		character(30) :: libray
		
		integer :: numberOfIndependVariables
		integer :: functionToMinimize
		type(Vector) :: valuesOfIndependentVariables

		logical :: isInitialExecution !! Variable por propositos de conveniencia
	
	end type
	
	type(Minimizer) :: Minimizer_instance
	

	public :: &
		Minimizer_constructor, &
		Minimizer_destructor, &
		Minimizer_info, &
		Minimizer_run, &
		Minimizer_showInformationOfIteration, &
		Minimizer_getFunctionValue, &
		Minimizer_getGradient, &
		Minimizer_getHessiane, &
		Minimizer_getLocatedPoint, &
		Minimizer_setName, &
		Minimizer_setNumberOfIndependVariables,&
		Minimizer_setFuntionToMinimize, &
		Minimizer_setValuesOfIndependentVariables


contains
		!**
		! @brief Define el constructor
		!**
		subroutine Minimizer_constructor()
			implicit none
			
			Minimizer_instance.name = trim(APMO_instance.MINIMIZATION_METHOD)
			Minimizer_instance.functionToMinimize = MINIMIZER_FUNCTION_OF_ENERGY
			Minimizer_instance.libray = trim(APMO_instance.MINIMIZATION_LIBRARY)
			Minimizer_instance.isInitialExecution = .true.
 			call GenericMinimizer_constructor()
			call GenericMinimizer_setNumberOfFreeCoordinates( ParticleManager_getNumberOfFreeCoordinates() )
		end subroutine Minimizer_constructor
		
		!**
		! @brief Define el destructor
		!**
		subroutine Minimizer_destructor()
			implicit none
			
			call GenericMinimizer_destructor()
					
			select case( Minimizer_instance.functionToMinimize)
			
				case( MINIMIZER_FUNCTION_OF_ENERGY )
					
					select case ( trim(MolecularSystem_getMethodName() ) )
					
						case ("RHF")
							
							call RHFSolver_destructor()
							if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) call MollerPlesset_destructor()
				
					 	case ("ROHF")
							
						case ("UHF")

						case default
						
					end select
					
				case( MINIMIZER_FUNCTION_OF_DENSITY)
				
				case default
			
			end select

			if ( EnergyDerivator_isInstanced() ) call EnergyDerivator_destructor() 
			
		end subroutine Minimizer_destructor
		
		subroutine Minimizer_info()	
			implicit none
			
			write (6,"(T5,A30,A)") "GRADIENT METHOD             : ",trim( EnergyDerivator_getTypeGradient() )
			write (6,"(T5,A30,A)") "LIBRARY                     : ",trim(Minimizer_instance.libray)
			
			select case( trim( Minimizer_instance.libray ) )
			
				case( "GSL")
				
					!! call GSLInterface_info()				
				
				case("GENERIC")
				
					call GenericMinimizer_info()
					
			end select
		
			
		end subroutine Minimizer_info
		
		!**
		! @brief Define el destructor
		!**
		subroutine Minimizer_run()
			implicit none
			
			type(Exception) :: ex
			type(arrayStructure) :: cvaluesOfIndependentVariables
			integer :: state
			real(8) :: auxValue
	
			select case( trim( Minimizer_instance.libray ) )
			
				case( "GSL")
				
					call gsl_minimizer_run( cvaluesOfIndependentVariables, state)
				
				case("GENERIC")
				
					!! Inicializa el minimizador (usar el contructor)
					
					call GenericMinimizer_setInitialPoint( Minimizer_instance.valuesOfIndependentVariables )
					call GenericMinimizer_run()
					Minimizer_instance.valuesOfIndependentVariables = GenericMinimizer_getCurrentPoint()
					 
					if ( GenericMinimizer_instance.status == GENERIC_MINIMIZER_SUCCESS ) then
				
						print *,""
						print *,"   !!!!!!!!!!!!! SEARCH LOCATED !!!!!!!!!!!!!!!!!!!"
						print *,""
						call Minimizer_showInformationOfIteration( GenericMinimizer_instance.gradientVector, &
									GenericMinimizer_instance.functionValue, &
									GenericMinimizer_instance.numberOfCallsToFunctionValue )
							
					else
						
						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object Minimizer in the GenericMinimizer Module" )
						call Exception_setDescription( ex, "GenericMinimizer finished with errors" )
						call Exception_show( ex )
					
					end if
					
				case default
				
					call Exception_constructor( ex , ERROR )
					call Exception_setDebugDescription( ex, "Class object Minimizer in the run function" )
					call Exception_setDescription( ex, "This library isn't avaliable" )
					call Exception_show( ex )
			
			end select
		
		end subroutine Minimizer_run
		
		!**
		! @brief Muestra informacion de gradiente y energia asociada a una iteracion
		!
		! @todo Optimizar el algortimo de organizacion de componentes de gradiente
		!**
		subroutine Minimizer_showInformationOfIteration( gradientVector, functionValue, numberOfIterations )
			implicit none
			type(Vector), intent(in) :: gradientVector
			real(8), intent(in) :: functionValue
			integer, intent(in) :: numberOfIterations 
			
			
		end subroutine Minimizer_showInformationOfIteration
		
		
		!**
		! @brief Ajusta el nombre del minimizador empleado
		!**
		subroutine Minimizer_setName( name )
			implicit none
			character(*) :: name
			
			Minimizer_instance.name = trim(name)
		
		end subroutine Minimizer_setName
		
		!**
		! @brief Ajusta el nombre del minimizador empleado
		!**
		subroutine Minimizer_setFuntionToMinimize( typeOfFunction )
			implicit none
			integer :: typeOfFunction
			
			Minimizer_instance.functionToMinimize = typeOfFunction
			
			select case( Minimizer_instance.functionToMinimize)
			
				case( MINIMIZER_FUNCTION_OF_ENERGY )
					
					select case ( trim( APMO_instance.METHOD ) )
					
						case ("RHF")

							call RHFSolver_constructor()
							if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) &
								call MollerPlesset_constructor(APMO_instance.MOLLER_PLESSET_CORRECTION)
				
					 	case ("ROHF")
							
						case ("UHF")

						case default
						
					end select

					call EnergyDerivator_constructor()
					if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) call EnergyDerivator_setNumeric()
					
					
				case( MINIMIZER_FUNCTION_OF_DENSITY)
				
				
				case default
			
			end select
		
		end subroutine Minimizer_setFuntionToMinimize
		
		!**
		! @brief Ajusta el numero de variables independientes
		!**
		subroutine Minimizer_setNumberOfIndependVariables( numberOfIndependVariables )
			implicit none
			integer :: numberOfIndependVariables
			
			Minimizer_instance.numberOfIndependVariables = numberOfIndependVariables
		
		end subroutine Minimizer_setNumberOfIndependVariables
		
		!**
		! @brief 	Ajusta el valor inicial de las variables independientes 
		!**
		subroutine Minimizer_setValuesOfIndependentVariables( valuesOfIndependentVariables ) 
			implicit none
			type(Vector), intent(in) :: valuesOfIndependentVariables
			
			Minimizer_instance.valuesOfIndependentVariables = valuesOfIndependentVariables
			Minimizer_instance.numberOfIndependVariables = size( valuesOfIndependentVariables.values )
			
		end subroutine Minimizer_setValuesOfIndependentVariables
		
		
		!**
		! @brief 	Retorna el valor de la funcion que se desea minimizar en 
		!		el punto especificado por las variables independientes
		!**
		function Minimizer_getFunctionValue( valuesOfIndependentVariables ) result (output)
			implicit none
			type(Vector) :: valuesOfIndependentVariables
			real(8) :: output

			select case( Minimizer_instance.functionToMinimize)
			
				case( MINIMIZER_FUNCTION_OF_ENERGY )
				
					!! Ajusta el origen de las particulas presentes en el sistema
					call ParticleManager_setParticlesPositions( valuesOfIndependentVariables)

					select case ( trim(MolecularSystem_getMethodName() ) )
					
						case ("RHF")
							
							if ( .not.Minimizer_instance.isInitialExecution ) call RHFSolver_reset()
							call RHFSolver_run()
							output = MolecularSystem_getTotalEnergy()

							!! Realiza correccion de la energia MPn
							if ( APMO_instance.MOLLER_PLESSET_CORRECTION >= 2) then
							
								call MollerPlesset_run()
								output = MollerPlesset_getTotalEnergy()
							
							end if
							
							Minimizer_instance.isInitialExecution=.false.
							
					 	case ("ROHF")
							
!! 							call ROHFSolver_reset()
!! 							call ROHFSolver_run() 
!! 							output = MolecularSystem_getTotalEnergy()
							
							
						case ("UHF")
							
!! 							call UHFSolver_reset()
!! 							call UHFSolver_run() 
!! 							output = MolecularSystem_getTotalEnergy()

						case default
						
					end select
							
					 
				case( MINIMIZER_FUNCTION_OF_DENSITY)
				
					output = 0.0_8
				
				case default
				
					output = 0.0_8
			
			end select
			
			print "(A1$)","."
			
		end function Minimizer_getFunctionValue
		
		!**
		! @brief 	Retorna el gradiente de la funcion que se desea minimizar
		!**
		function Minimizer_getGradient( valuesOfIndependentVariables ) result (output)
			implicit none
			type(Vector) :: valuesOfIndependentVariables
			type(Vector) :: output
			
			integer :: i
			type(Exception) :: ex 
			
! 			if( .not.allocated( output.values ) ) &
! 			call Vector_constructor(output, Minimizer_instance.numberOfIndependVariables )
! 			
! 			select case( Minimizer_instance.functionToMinimize)
! 			
! 				case( MINIMIZER_FUNCTION_OF_ENERGY )
! 
! 					do i=1, Minimizer_instance.numberOfIndependVariables
! 
! 						output.values(i) = EnergyDerivator_getDerivative(valuesOfIndependentVariables , [i] )
! 						print "(A1$)",":"
! 					end do
! 
! 				case( MINIMIZER_FUNCTION_OF_DENSITY)
! 				
! 					call Exception_constructor( ex , ERROR )
! 					call Exception_setDebugDescription( ex, "Class object Minimizer in the getGradient function" )
! 					call Exception_setDescription( ex, "This method isn't implemented" )
! 					call Exception_show( ex )
! 
! 				
! 				case default
! 				
! 					call Exception_constructor( ex , ERROR )
! 					call Exception_setDebugDescription( ex, "Class object Minimizer in the getGradient function" )
! 					call Exception_setDescription( ex, "This method isn't implemented" )
! 					call Exception_show( ex )
! 
! 			
! 			end select
			
		end function Minimizer_getGradient
		
		!**
		! @brief 	Retorna la hessiana de la funcion que se desea minimizar
		!**
		function Minimizer_getHessiane( valuesOfIndependentVariables ) result (output)
			implicit none
			type(Vector) :: valuesOfIndependentVariables
			type(Matrix) ::  output
			
			integer :: i
			integer :: j
			type(Exception) :: ex 
			
! 			call Matrix_constructor(output, int(Minimizer_instance.numberOfIndependVariables,8), &
! 				int(Minimizer_instance.numberOfIndependVariables,8) )
! 			
! 			select case( Minimizer_instance.functionToMinimize)
! 			
! 				case( MINIMIZER_FUNCTION_OF_ENERGY )
! 
! 										
! 					do i=1, Minimizer_instance.numberOfIndependVariables
! 						
! 						do j=1, Minimizer_instance.numberOfIndependVariables
! 
! 							output.values(i,j) = EnergyDerivator_getDerivative(valuesOfIndependentVariables , [i,j] )
! 						
! 						end do
! 			
! 					end do
! 
! 					
! 				case( MINIMIZER_FUNCTION_OF_DENSITY)
! 				
! 					call Exception_constructor( ex , ERROR )
! 					call Exception_setDebugDescription( ex, "Class object Minimizer in the getHessiane function" )
! 					call Exception_setDescription( ex, "This method isn't implemented" )
! 					call Exception_show( ex )
! 
! 				
! 				case default
! 				
! 					call Exception_constructor( ex , ERROR )
! 					call Exception_setDebugDescription( ex, "Class object Minimizer in the getHessiane function" )
! 					call Exception_setDescription( ex, "This method isn't implemented" )
! 					call Exception_show( ex )
! 
! 			
! 			end select
		
		end function Minimizer_getHessiane

		
		function Minimizer_getLocatedPoint() result( output )
			implicit none
			type(Vector) :: output
		
			output=Minimizer_instance.valuesOfIndependentVariables
			
		end function Minimizer_getLocatedPoint
				
end module Minimizer_