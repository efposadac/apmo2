!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************
!**
! @brief Permite obtener informacion del sistema de una arvivo de entrada
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-30
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!   - <tt> 2011-01-20 </tt>: Edwin F. Posada C. ( efposadac@unal.edu.co )
!        -# Adición de métodos y atributos de la clase
!
!**
module InputParser_
	use Units_
	use APMO_
	use IFPORT
	use Exception_
    use String_
    use Math_
	implicit none

	type, public :: InputParser
		
		character(100) :: fileName
		character(100) :: systemName
		character(255) :: systemDescription
		integer :: numberOfParticles
		integer :: numberOfExtenalPots
		integer :: currentParticle
		integer :: currentExternalPot
		real(8) :: systemCharge
		
		logical :: isInstanced
		
	end type InputParser


	type, public :: InputParser_Particle
		
		character(15) :: name
		character(30) :: basisSetName
		real(8) :: origin(3)
		character(3) :: fixedCoordinates
		real(8) :: multiplicity
		integer :: additionOfParticles
        integer :: fragmentNumber
	
	end type InputParser_Particle

	type, public :: InputParser_ExternalPotInfo
		
		character(50) :: name
		character(50) :: interactionName
		character(50) :: interactionType
	
	end type InputParser_ExternalPotInfo
      
 	type, public :: InputParser_ExternalLowdin
 		
 		character(5) :: atom
 		character(10) :: basisSetName
 		real(8) :: isotopicMass
 		integer :: initialAtom
 		integer :: finalAtom
 		integer :: numberOfExternalLowdin
 		
 	end type InputParser_ExternalLowdin
	

	type(InputParser), private :: InputParser_instance
		
 		character(10) :: Lowdin_basisSetName		
 		character(5) :: Lowdin_atom
 		real(8) :: Lowdin_isotopicMass
 		integer :: Lowdin_initialAtom
 		integer :: Lowdin_finalAtom
 		integer :: Lowdin_numberOfBasis
 		
 		NAMELIST /LLowdin/ &
 			  Lowdin_atom, &
 			  Lowdin_isotopicMass, &
 			  Lowdin_initialAtom, &
 			  Lowdin_finalAtom, &
 			  Lowdin_basisSetName, &
 			  Lowdin_numberOfBasis
		
		
		character(50) :: ExternalPot_name
		character(50) :: ExternalPot_interaction
		character(50) :: ExternalPot_interactionType
		
		NAMELIST /ExternalPot/ &
			ExternalPot_name, &
			ExternalPot_interaction, &
			ExternalPot_interactionType
	
	
		character(15) :: InputParticle_name
		character(30) :: InputParticle_basisSetName
		real(8) :: InputParticle_origin(3)
		character(3) InputParticle_fixedCoordinates
		real(8) :: InputParticle_multiplicity
		integer :: InputParticle_addParticles
		integer :: InputParticle_fragmentNumber
		
		NAMELIST /InputParticle/ &
			InputParticle_name, &
			InputParticle_basisSetName, &
			InputParticle_origin, &
			InputParticle_fixedCoordinates, &
			InputParticle_multiplicity, &
			InputParticle_addParticles, &
			InputParticle_fragmentNumber
		
		character(255) :: InputSystem_description
		integer :: InputSystem_numberOfParticles
		integer :: InputSystem_numberOfExternalPots
		real(8) :: InputSystem_charge
		
		NAMELIST /InputSystem/ &
			InputSystem_numberOfParticles, &
			InputSystem_numberOfExternalPots, &
			InputSystem_description, &
			InputSystem_charge
		
		real(8) :: InputApmoParameters_tv
		real(8) :: InputApmoParameters_integralThreshold
		real(8) :: InputApmoParameters_scfNonelectronicEnergyTolerance
		real(8) :: InputApmoParameters_scfElectronicEnergyTolerance
		real(8) :: InputApmoParameters_nonelectronicDensityMatrixTolerance
		real(8) :: InputApmoParameters_electronicDensityMatrixTolerance
		real(8) :: InputApmoParameters_totalEnergyTolerance
		real(8) :: InputApmoParameters_strongEnergyTolerance
		real(8) :: InputApmoParameters_densityFactorThreshold
		real(8) :: InputApmoParameters_numericalDerivativeDelta
		real(8) :: InputApmoParameters_minimizationInitialStepSize
		real(8) :: InputApmoParameters_minimizationLineTolerance
		real(8) :: InputApmoParameters_minimizationToleranceGradient
		real(8) :: InputApmoParameters_maximumRangeForGraphs
		real(8) :: InputApmoParameters_waveFunctionScale
		real(8) :: InputApmoParameters_doubleZeroThreshold
		real(8) :: InputApmoParameters_diisSwitchThreshold
		integer :: InputApmoParameters_scfNonelectronicMaxIterations
		integer :: InputApmoParameters_scfElectronicMaxIterations
		integer :: InputApmoParameters_scfMaxIterations
		integer :: InputApmoParameters_scfInterspeciesMaximumIterations
		integer :: InputApmoParameters_graphForNumOrbital
		integer :: InputApmoParameters_minimizationMaxIteration
		integer :: InputApmoParameters_iterationScheme
		integer :: InputApmoParameters_convergenceMethod
		integer :: InputApmoParameters_mpFrozenCoreBoundary
		integer :: InputApmoParameters_formatNumberOfColumns
		character(10) :: InputApmoParameters_scfElectronicTypeGuess
		character(10) :: InputApmoParameters_scfNonelectronicTypeGuess
		character(10) :: InputApmoParameters_scfConvergenceCriterium
		character(10) :: InputApmoParameters_minimizationMethod
		character(10) :: InputApmoParameters_minimizationLibrary
		character(50) :: InputApmoParameters_units
		character(50) :: InputApmoParameters_coordinates
		character(20) :: InputApmoParameters_energyCalculator
		character(20) :: InputApmoParameters_frozen(5)
		logical :: InputApmoParameters_analyticGradient
		logical :: InputApmoParameters_optimizeWithCpCorrection
		logical :: InputApmoParameters_cpCorrection
		logical :: InputApmoParameters_mpOnlyElectronicCorrection
		logical :: InputApmoParameters_debugScfs
		logical :: InputApmoParameters_minimizationWithSinglePoint
		logical :: InputApmoParameters_useSymmetryInMatrices
		logical :: InputApmoParameters_restartOptimization
		logical :: InputApmoParameters_removeTranslationalContamination
		logical :: InputApmoParameters_transformToCenterOfMass
		logical :: InputApmoParameters_diisErrorInDamping
		logical ::  InputApmoParameters_readCoefficients
		logical :: InputApmoParameters_optimizeGeometryWithMP
		logical :: InputApmoParameters_onlyElectronicEffect
		logical :: InputApmoParameters_projectHessiane
		logical :: InputApmoParameters_electronicWaveFunctionAnalysis
		logical :: InputApmoParameters_apmolpro
		

		NAMELIST /InputApmoParameters/ &
			InputApmoParameters_tv, &
			InputApmoParameters_integralThreshold, &
			InputApmoParameters_scfNonelectronicEnergyTolerance, &
			InputApmoParameters_scfElectronicEnergyTolerance, &
			InputApmoParameters_nonelectronicDensityMatrixTolerance, &
			InputApmoParameters_electronicDensityMatrixTolerance, &
			InputApmoParameters_totalEnergyTolerance, &
			InputApmoParameters_strongEnergyTolerance, &
			InputApmoParameters_densityFactorThreshold, & 
			InputApmoParameters_scfNonelectronicMaxIterations, &
			InputApmoParameters_scfElectronicMaxIterations, &
			InputApmoParameters_scfMaxIterations, &
			InputApmoParameters_scfInterspeciesMaximumIterations, &
			InputApmoParameters_doubleZeroThreshold, &
			InputApmoParameters_convergenceMethod, &
			InputApmoParameters_scfElectronicTypeGuess, &
			InputApmoParameters_scfNonelectronicTypeGuess, &
			InputApmoParameters_scfConvergenceCriterium, &
			InputApmoParameters_numericalDerivativeDelta, &
			InputApmoParameters_minimizationInitialStepSize, & 
			InputApmoParameters_minimizationLineTolerance, &
			InputApmoParameters_minimizationToleranceGradient, &
			InputApmoParameters_maximumRangeForGraphs, &
			InputApmoParameters_waveFunctionScale, &
			InputApmoParameters_graphForNumOrbital, &
			InputApmoParameters_diisSwitchThreshold, &
			InputApmoParameters_minimizationMaxIteration, &
			InputApmoParameters_iterationScheme, &
			InputApmoParameters_minimizationMethod, &
			InputApmoParameters_minimizationLibrary, &
			InputApmoParameters_analyticGradient, &
			InputApmoParameters_optimizeWithCpCorrection, &
			InputApmoParameters_cpCorrection, &
			InputApmoParameters_mpFrozenCoreBoundary, &
			InputApmoParameters_mpOnlyElectronicCorrection, &
			InputApmoParameters_formatNumberOfColumns, &
			InputApmoParameters_units, &
			InputApmoParameters_coordinates, &
			InputApmoParameters_energyCalculator, &
			InputApmoParameters_debugScfs, &
			InputApmoParameters_minimizationWithSinglePoint, &
			InputApmoParameters_useSymmetryInMatrices, &
			InputApmoParameters_restartOptimization, &
			InputApmoParameters_removeTranslationalContamination, &
			InputApmoParameters_transformToCenterOfMass, &
			InputApmoParameters_diisErrorInDamping, &
			InputApmoParameters_readCoefficients, &
			InputApmoParameters_optimizeGeometryWithMP, &
			InputApmoParameters_onlyElectronicEffect, &
			InputApmoParameters_projectHessiane, &
			InputApmoParameters_electronicWaveFunctionAnalysis, &
			InputApmoParameters_frozen, &
			InputApmoParameters_apmolpro
			
			character(50) :: InputTasks_method
			integer :: InputTasks_mollerPlessetCorrection
			logical :: InputTasks_optimizeGeometry
			
			NAMELIST /InputTasks/ &
			InputTasks_method, &
			InputTasks_mollerPlessetCorrection, &
			InputTasks_optimizeGeometry
			
			
	public :: &
		InputParser_constructor,&
		InputParser_destructor, &
		InputParser_getSystemName, &
		InputParser_getSystemDescription, &
		InputParser_getSystemCharge, &
		InputParser_getParticle, &
		InputParser_getExternalPotential, &
		InputParser_getNumberOfParticles, &
		InputParser_getNumberOfExtenalPots, &
		InputParser_setApmoParameters,&
		InputParser_writeNameList,&
 		InputParser_setExternalBasis
		
 		type(InputParser_ExternalLowdin), allocatable, public :: externalLowdin(:)

contains
		
	!**
	! @brief Define el constructor para el modulo
	!**
	subroutine InputParser_constructor()
		implicit none

		integer :: status
		integer :: existFile
		type(Exception) :: ex
		
		!! Obtiene el nombre del archivo de entrada
		if ( .not.InputParser_instance%isInstanced ) then
			
			call get_command_argument (1,value=InputParser_instance%fileName)
			InputParser_instance%fileName = InputParser_instance%fileName(1:len_trim(InputParser_instance%fileName)-3)
			InputParser_instance%currentParticle  = 0
			
		
				
			inquire(FILE = trim(InputParser_instance%fileName)//"aux", EXIST = existFile )
			
			if ( existFile ) then
				
				open (unit=4, file=trim(InputParser_instance%fileName)//"aux")
				
				InputSystem_charge =0.0_8
				InputSystem_numberOfExternalPots=0
				
				read(4,NML=InputSystem)
				
				InputParser_instance%systemName = trim(InputParser_instance%fileName(1:len_trim(InputParser_instance%fileName)-1))
				InputParser_instance%systemDescription = trim(InputSystem_description)
				InputParser_instance%numberOfParticles = InputSystem_numberOfParticles
				InputParser_instance%systemCharge = InputSystem_charge
				InputParser_instance%numberOfExtenalPots = InputSystem_numberOfExternalPots
				
				rewind(4)	
				
				InputParser_instance%isInstanced = .true.
			else 
				
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object InputParser in the constructor function" )
				call Exception_setDescription( ex, "The input file "// trim(InputParser_instance%fileName)//"aux" //" isn't defined" )
				call Exception_show( ex )
				
			end if

		end if

	end subroutine InputParser_constructor
	
	
	!**
	! @brief Define el destructor para el modulo
	!**
	subroutine InputParser_destructor
		implicit none
		
		if ( InputParser_instance%isInstanced ) then
			close(4,status='DELETE')
			InputParser_instance%isInstanced = .false.
		end if
		
	end subroutine InputParser_destructor
	
	!**
	! @brief Retorna el nombre asignado al sistema que se calcula
	!**
	function InputParser_getSystemName() result( output )
		implicit none
		character(50) :: output
		
		output = trim(InputParser_instance%systemName)
		
	end function InputParser_getSystemName
	
	!**
	! @brief Retorna el una description del sistema
	!**
	function InputParser_getSystemDescription() result( output )
		implicit none
		character(255) :: output
		
		output = trim( InputParser_instance%systemDescription)

	end function InputParser_getSystemDescription
	
	!**
	! @brief Retorna el nombre asignado al sistema que se calcula
	!**
	function InputParser_getSystemCharge() result( output )
		implicit none
		real(8) :: output
				
		output = InputParser_instance%systemCharge

	end function InputParser_getSystemCharge
	

	!**
	! @brief Retorna informacion sobre los parametros de una particula de entrada
	!**
	function InputParser_getParticle( isEndParticle ) result( output )
		implicit none
		logical, intent(out) :: isEndParticle
		type(InputParser_Particle) :: output
	
		integer :: from
		integer :: to
		integer :: i

		type(Exception) :: ex
		
		if ( InputParser_instance%isInstanced ) then
		
			InputParser_instance%currentParticle = InputParser_instance%currentParticle + 1
			if ( InputParser_instance%currentParticle > InputParser_instance%numberOfParticles ) InputParser_instance%currentParticle = 1
						
			rewind(4)
			do i=1, InputParser_instance%currentParticle
			
				!! Borra los atributos anteriores
				InputParticle_name=""
				InputParticle_basisSetName=""
				InputParticle_origin =0.0_8
				InputParticle_fixedCoordinates=""
				InputParticle_multiplicity=0.0_8
				InputParticle_addParticles = 0
				InputParticle_fragmentNumber = 1
			
				read(4,NML=InputParticle)
			
				if ( trim(APMO_instance%UNITS)=="ANGSTROMS") then
					InputParticle_origin= InputParticle_origin / AMSTRONG
				end if
			
			end do
			
			if( String_findSubstring( trim(InputParticle_name), "e-") == 1 ) then
                                   from = String_findSubstring( trim(InputParticle_name), "[") + 1
                                   to = String_findSubstring( trim(InputParticle_name), "]") - 1
                                   output%name = trim(String_getUppercase( trim(InputParticle_name) , from, to))
                                   if( from  /= to ) then
                                      output%name = trim(String_getLowercase( trim(output%name), from + 1, to ))
				end if
                        if(APMO_instance%APMOLPRO) then
                            output%name = String_getUppercase(trim(output%name),from,to)
                        end if

			else
				from =1
				to = String_findSubstring( trim(InputParticle_name), "_") - 1
				if(to > len_trim(InputParticle_name)) to = len_trim(InputParticle_name)
				output%name = trim(String_getUppercase( trim(InputParticle_name) , from, to))

				if( from  /= to ) then
					output%name = trim(String_getLowercase( trim(output%name), from+1, to ))
				end if

			end if
   
			output%basisSetName = String_getLowercase(trim(InputParticle_basisSetName))
			output%origin = InputParticle_origin
			output%fixedCoordinates = trim(InputParticle_fixedCoordinates)
			output%multiplicity=InputParticle_multiplicity
			output%additionOfParticles=InputParticle_addParticles
			output%fragmentNumber = InputParticle_fragmentNumber
		
			isEndParticle = .false.
			if ( InputParser_instance%currentParticle == InputParser_instance%numberOfParticles ) 	isEndParticle = .true.
		
		else 
				
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object InputParser in the getParticle function" )
			call Exception_setDescription( ex, "The InputParser module wasn't instanced" )
			call Exception_show( ex )

		end if
		
	end function InputParser_getParticle


	!**
	! @brief Retorna informacion sobre un potencial externo 
	!**
	function InputParser_getExternalPotential( isEndPotential ) result( output )
		implicit none
		logical, intent(out) :: isEndPotential
		type(InputParser_ExternalPotInfo) :: output
	
		integer :: i
		type(Exception) :: ex
		
		if ( InputParser_instance%isInstanced ) then
		
			InputParser_instance%currentExternalPot = InputParser_instance%currentExternalPot + 1
			if ( InputParser_instance%currentExternalPot > InputParser_instance%numberOfExtenalPots ) InputParser_instance%currentExternalPot = 1
						
			rewind(4)
			do i=1, InputParser_instance%currentExternalPot
			
				!! Borra los atributos anteriores
				ExternalPot_name=""
				ExternalPot_interaction=""
				ExternalPot_interactionType=""
			
				read(4,NML=ExternalPot)
			
			end do
			
			output%name = trim(ExternalPot_name)
			output%interactionName = trim(ExternalPot_interaction)
			output%interactionType = trim(ExternalPot_interactionType)
		
			isEndPotential = .false.
			if ( InputParser_instance%currentExternalPot == InputParser_instance%numberOfExtenalPots ) 	isEndPotential = .true.
			
			if(InputSystem_numberOfExternalPots>0) &
					APMO_instance%EXTERNAL_POTENTIAL=trim(output%name)
					APMO_instance%IS_THERE_EXTERNAL_POTENTIAL=.true.
		
		else 
				
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object InputParser in the getParticle function" )
			call Exception_setDescription( ex, "The InputParser module wasn't instanced" )
			call Exception_show( ex )

		end if
		
	end function InputParser_getExternalPotential






	function InputParser_getNumberOfParticles() result( output )
		implicit none
		integer :: output
		
		output= InputParser_instance%numberOfParticles

	end function InputParser_getNumberOfParticles
	
	
	function InputParser_getNumberOfExtenalPots() result( output )
		implicit none
		integer :: output
		
		output= InputParser_instance%numberOfExtenalPots

	end function InputParser_getNumberOfExtenalPots
	
	!**
	! @brief Ajusta los parametros de control del program
	!**
	subroutine InputParser_setApmoParameters()
		implicit none
		
		type(Exception) :: ex
		integer :: auxInt
	
		if ( InputParser_instance%isInstanced ) then
		
			InputApmoParameters_tv = 0.0_8
			InputApmoParameters_integralThreshold = 0.0_8
			InputApmoParameters_scfNonelectronicEnergyTolerance = 0.0_8
			InputApmoParameters_scfElectronicEnergyTolerance = 0.0_8
			InputApmoParameters_nonelectronicDensityMatrixTolerance = 0.0_8
			InputApmoParameters_electronicDensityMatrixTolerance = 0.0_8
			InputApmoParameters_totalEnergyTolerance = 0.0_8
			InputApmoParameters_strongEnergyTolerance = 0.0_8
			InputApmoParameters_densityFactorThreshold = 0.0_8
			InputApmoParameters_scfNonelectronicMaxIterations = 0
			InputApmoParameters_scfElectronicMaxIterations = 0
			InputApmoParameters_scfMaxIterations = 0
			InputApmoParameters_scfInterspeciesMaximumIterations = 0
			InputApmoParameters_doubleZeroThreshold = 0.0_8
			InputApmoParameters_convergenceMethod = 0
			InputApmoParameters_scfElectronicTypeGuess = ""
			InputApmoParameters_scfNonelectronicTypeGuess = ""
			InputApmoParameters_scfConvergenceCriterium = ""
			InputApmoParameters_numericalDerivativeDelta = 0.0_8
			InputApmoParameters_minimizationInitialStepSize = 0.0_8
			InputApmoParameters_minimizationLineTolerance = 0.0_8
			InputApmoParameters_minimizationToleranceGradient = 0.0_8
			InputApmoParameters_maximumRangeForGraphs = 0.0_8
			InputApmoParameters_waveFunctionScale=0.0_8
			InputApmoParameters_graphForNumOrbital=1
			InputApmoParameters_densityFactorThreshold = 0.0_8
			InputApmoParameters_minimizationMaxIteration = 0
			InputApmoParameters_iterationScheme=-1
			InputApmoParameters_minimizationMethod = ""
			InputApmoParameters_minimizationLibrary = ""
			InputApmoParameters_analyticGradient = .true.
			InputApmoParameters_optimizeWithCpCorrection = .false.
			InputApmoParameters_cpCorrection =.false.
			InputApmoParameters_mpFrozenCoreBoundary = 0
			InputApmoParameters_mpOnlyElectronicCorrection = .false.
			InputApmoParameters_formatNumberOfColumns = 0
			InputApmoParameters_units = ""
			InputApmoParameters_coordinates = ""
			InputApmoParameters_energyCalculator = "internal"
			InputApmoParameters_debugScfs = .false.
			InputApmoParameters_minimizationWithSinglePoint =.false.
			InputApmoParameters_useSymmetryInMatrices = .false.
			InputApmoParameters_restartOptimization = .false.
			InputApmoParameters_removeTranslationalContamination = .false.
			InputApmoParameters_transformToCenterOfMass = .true.
			InputApmoParameters_diisErrorInDamping = .false.
			InputApmoParameters_readCoefficients = .true.
			InputApmoParameters_optimizeGeometryWithMP = .false.
			InputApmoParameters_onlyElectronicEffect = .false.
			InputApmoParameters_projectHessiane = .true.
			InputApmoParameters_electronicWaveFunctionAnalysis = .false.
			InputApmoParameters_frozen = ""
			InputApmoParameters_apmolpro = .false.
		
			rewind(4)
			read(4,NML=InputApmoParameters)
			
			InputTasks_method = ""
			InputTasks_mollerPlessetCorrection = 0
			InputTasks_optimizeGeometry = .false.
			rewind(4)
			read(4,NML=InputTasks)
		
		
			if ( InputApmoParameters_tv > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%TV = InputApmoParameters_tv
			
			if ( InputApmoParameters_integralThreshold > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%INTEGRAL_THRESHOLD = InputApmoParameters_integralThreshold
			
			if ( InputApmoParameters_scfNonelectronicEnergyTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%SCF_NONELECTRONIC_ENERGY_TOLERANCE = InputApmoParameters_scfNonelectronicEnergyTolerance
			
			if ( InputApmoParameters_scfElectronicEnergyTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%SCF_ELECTRONIC_ENERGY_TOLERANCE = InputApmoParameters_scfElectronicEnergyTolerance
			
			if ( InputApmoParameters_nonelectronicDensityMatrixTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE = InputApmoParameters_nonelectronicDensityMatrixTolerance
			
			if ( InputApmoParameters_electronicDensityMatrixTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE = InputApmoParameters_electronicDensityMatrixTolerance
			
			if ( InputApmoParameters_totalEnergyTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%TOTAL_ENERGY_TOLERANCE = InputApmoParameters_totalEnergyTolerance
			
			if ( InputApmoParameters_strongEnergyTolerance > 1.0D-14  ) &
				APMO_instance%STRONG_ENERGY_TOLERANCE = InputApmoParameters_strongEnergyTolerance
				
			if ( InputApmoParameters_densityFactorThreshold > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%DENSITY_FACTOR_THRESHOLD = InputApmoParameters_densityFactorThreshold
			
			if ( InputApmoParameters_scfNonelectronicMaxIterations /= 0 ) &
				APMO_instance%SCF_NONELECTRONIC_MAX_ITERATIONS = InputApmoParameters_scfNonelectronicMaxIterations
			
			if ( InputApmoParameters_scfElectronicMaxIterations /= 0 ) &
				APMO_instance%SCF_ELECTRONIC_MAX_ITERATIONS = InputApmoParameters_scfElectronicMaxIterations
			
			if ( InputApmoParameters_scfMaxIterations /= 0 ) &
				APMO_instance%SCF_MAX_ITERATIONS = InputApmoParameters_scfMaxIterations
			
			if ( InputApmoParameters_scfInterspeciesMaximumIterations /= 0 ) & 
				APMO_instance%SCF_INTERSPECIES_MAXIMUM_ITERATIONS = InputApmoParameters_scfInterspeciesMaximumIterations
			
			if ( InputApmoParameters_doubleZeroThreshold > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%DOUBLE_ZERO_THRESHOLD = InputApmoParameters_doubleZeroThreshold
			
			if ( InputApmoParameters_convergenceMethod /= 0 ) &
				APMO_instance%CONVERGENCE_METHOD = InputApmoParameters_convergenceMethod

			if ( InputApmoParameters_scfElectronicTypeGuess /= "" ) &
				InputApmoParameters_scfElectronicTypeGuess = trim(APMO_instance%SCF_ELECTRONIC_TYPE_GUESS)
			
			if ( InputApmoParameters_scfNonelectronicTypeGuess /= "" ) &
				APMO_instance%SCF_NONELECTRONIC_TYPE_GUESS = trim(InputApmoParameters_scfNonelectronicTypeGuess)
			
			if ( InputApmoParameters_scfConvergenceCriterium /= "" ) &
				APMO_instance%SCF_CONVERGENCE_CRITERIUM = trim(InputApmoParameters_scfConvergenceCriterium )
			
			if ( InputApmoParameters_numericalDerivativeDelta > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%NUMERICAL_DERIVATIVE_DELTA = InputApmoParameters_numericalDerivativeDelta
			
			if ( InputApmoParameters_minimizationInitialStepSize > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%MINIMIZATION_INITIAL_STEP_SIZE = InputApmoParameters_minimizationInitialStepSize
			
			if ( InputApmoParameters_minimizationLineTolerance > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%MINIMIZATION_LINE_TOLERANCE = InputApmoParameters_minimizationLineTolerance
			
			if ( InputApmoParameters_minimizationToleranceGradient > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%MINIMIZATION_TOLERANCE_GRADIENT = InputApmoParameters_minimizationToleranceGradient

			 if ( InputApmoParameters_maximumRangeForGraphs > &
			      APMO_instance%DOUBLE_ZERO_THRESHOLD  ) then
			      
			      call Math_numberRepresentation(&
				   InputApmoParameters_maximumRangeForGraphs, &
				   InputApmoParameters_maximumRangeForGraphs, &
				   auxInt)
				   APMO_instance%STEP_OF_GRAPHS=1.0/(10.0**(auxInt+2))
				   APMO_instance%MAXIMUM_RANGE_OF_GRAPHS = &
					int(ceiling(InputApmoParameters_maximumRangeForGraphs)*100)
				   
			end if


			 if ( InputApmoParameters_waveFunctionScale >  APMO_instance%DOUBLE_ZERO_THRESHOLD  ) &
				APMO_instance%WAVE_FUNCTION_SCALE=InputApmoParameters_waveFunctionScale

			if ( InputApmoParameters_graphForNumOrbital /= 1  ) &
				APMO_instance%GRAPH_FOR_NUM_ORBITAL=InputApmoParameters_graphForNumOrbital

			if ( InputApmoParameters_diisSwitchThreshold > APMO_instance%DOUBLE_ZERO_THRESHOLD  ) then
				APMO_instance%DIIS_SWITCH_THRESHOLD = InputApmoParameters_diisSwitchThreshold
				APMO_instance%DIIS_SWITCH_THRESHOLD_BKP = InputApmoParameters_diisSwitchThreshold
			end if

			if ( InputApmoParameters_minimizationMaxIteration /= 0 ) &
				APMO_instance%MINIMIZATION_MAX_ITERATION = InputApmoParameters_minimizationMaxIteration
			
			if ( InputApmoParameters_iterationScheme /= -1 ) &
				APMO_instance%ITERATION_SCHEME = InputApmoParameters_iterationScheme
			
			if ( InputApmoParameters_minimizationMethod /= "" ) &
				APMO_instance%MINIMIZATION_METHOD = trim(InputApmoParameters_minimizationMethod)
			
			if ( InputApmoParameters_minimizationLibrary /= "" ) &
				APMO_instance%MINIMIZATION_LIBRARY = trim(InputApmoParameters_minimizationLibrary)
			
			if ( .not.InputApmoParameters_analyticGradient  ) &
				APMO_instance%ANALYTIC_GRADIENT = InputApmoParameters_analyticGradient

            if ( InputApmoParameters_optimizeWithCpCorrection ) &
                    APMO_instance%OPTIMIZE_WITH_CP_CORRECTION = InputApmoParameters_optimizeWithCpCorrection
				
			if( InputApmoParameters_cpCorrection ) &
				APMO_instance%CP_CORRECTION = InputApmoParameters_cpCorrection
			
			if ( InputApmoParameters_mpFrozenCoreBoundary /= 0 ) &
				APMO_instance%MP_FROZEN_CORE_BOUNDARY = InputApmoParameters_mpFrozenCoreBoundary
			
			if ( InputApmoParameters_mpOnlyElectronicCorrection ) &
				APMO_instance%MP_ONLY_ELECTRONIC_CORRECTION = InputApmoParameters_mpOnlyElectronicCorrection
			
			if ( InputApmoParameters_formatNumberOfColumns /= 0 ) &
				APMO_instance%FORMAT_NUMBER_OF_COLUMNS = InputApmoParameters_formatNumberOfColumns
			
			if ( InputApmoParameters_units /= "" ) &
				APMO_instance%UNITS = trim(InputApmoParameters_units)

			if ( trim(InputApmoParameters_coordinates) /= "" ) &
				APMO_instance%COORDINATES = trim( InputApmoParameters_coordinates )

            if (  trim(InputApmoParameters_energyCalculator) /= "internal" ) &
                    APMO_instance%ENERGY_CALCULATOR = trim(InputApmoParameters_energyCalculator)
				
			if ( InputApmoParameters_debugScfs ) &
				APMO_instance%DEBUG_SCFS = .true.
				
			if ( InputApmoParameters_minimizationWithSinglePoint ) &
				APMO_instance%MINIMIZATION_WITH_SINGLE_POINT = .true.
				
			if ( InputApmoParameters_useSymmetryInMatrices ) &
				APMO_instance%USE_SYMMETRY_IN_MATRICES =.true.

			if ( InputApmoParameters_restartOptimization ) &
				APMO_instance%RESTART_OPTIMIZATION =.true.
			
			if ( InputApmoParameters_removeTranslationalContamination ) &
				APMO_instance%REMOVE_TRANSLATIONAL_CONTAMINATION = .true.

			if ( .not.InputApmoParameters_transformToCenterOfMass ) &
				APMO_instance%TRANSFORM_TO_CENTER_OF_MASS = InputApmoParameters_transformToCenterOfMass

			if ( InputApmoParameters_diisErrorInDamping ) &
				APMO_instance%DIIS_ERROR_IN_DAMPING = .true.

			if(  .not.InputApmoParameters_readCoefficients ) &
				APMO_instance%READ_COEFFICIENTS = InputApmoParameters_readCoefficients

			if ( InputTasks_method /= "" ) &
				APMO_instance%METHOD = String_getUppercase( trim(InputTasks_method) )

			if ( InputTasks_mollerPlessetCorrection /= 0 ) then
				APMO_instance%MOLLER_PLESSET_CORRECTION = InputTasks_mollerPlessetCorrection
				APMO_instance%METHOD=trim(APMO_instance%METHOD)//"-MP2"
			end if
			
			if ( InputTasks_optimizeGeometry  ) APMO_instance%OPTIMIZE = .true.

			if ( InputApmoParameters_optimizeGeometryWithMP  ) APMO_instance%OPTIMIZE_WITH_MP = .true.
			if ( InputApmoParameters_onlyElectronicEffect  ) APMO_instance%ONLY_ELECTRONIC_EFFECT = .true.
			if ( .not.InputApmoParameters_projectHessiane ) APMO_instance%PROJECT_HESSIANE = .false.

			if ( InputApmoParameters_electronicWaveFunctionAnalysis ) APMO_instance%ELECTRONIC_WAVEFUNCTION_ANALYSIS = .true.

			if ( InputApmoParameters_apmolpro ) APMO_instance%APMOLPRO = .true.

			if ( InputApmoParameters_frozen(1) /= "" ) then
				APMO_instance%FROZEN_PARTICLE=InputApmoParameters_frozen
				APMO_instance%IS_THERE_FROZEN_PARTICLE = .true.
			end if

			APMO_instance%INPUT_FILE = trim(InputParser_instance%fileName)

			if (APMO_instance%CONVERGENCE_METHOD >= 2) then
				APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE=APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE/2.5
				APMO_instance%SCF_NONELECTRONIC_ENERGY_TOLERANCE=APMO_instance%SCF_NONELECTRONIC_ENERGY_TOLERANCE/2.5
				APMO_instance%SCF_ELECTRONIC_ENERGY_TOLERANCE=APMO_instance%SCF_ELECTRONIC_ENERGY_TOLERANCE/2.5
				APMO_instance%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE=APMO_instance%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE/2.5
				APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE=APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE/2.5
				APMO_instance%TOTAL_ENERGY_TOLERANCE=APMO_instance%TOTAL_ENERGY_TOLERANCE/2.5
			end if
			
		else 
				
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object InputParser in the getParticle function" )
			call Exception_setDescription( ex, "The InputParser module wasn't instanced" )
			call Exception_show( ex )

		end if
		
	end subroutine InputParser_setApmoParameters
	
 	subroutine InputParser_setExternalBasis()
 		implicit none
 		integer :: counter
 		logical :: isEnd
 		
 		integer :: i
 		
 		if (APMO_instance%ENERGY_CALCULATOR == "external") then
 
 			
 			select case(String_getLowercase( trim( APMO_instance%EXTERNAL_SOFTWARE_NAME )))
 				case ("gamess")
 				
 				case ("apmo")
 				
 				case ("lowdin")
 					Lowdin_atom = ""
 					Lowdin_isotopicMass = 0.0_8
 					Lowdin_initialAtom = 0
 					Lowdin_finalAtom = 0
 					Lowdin_basisSetName = ""
 					Lowdin_numberOfBasis = 0
 					
 					externalLowdin%numberOfExternalLowdin = 0
 					isEnd = .false.
 					
 					rewind(4)
 					
 					do while (.not. isEnd)
 						read(4,NML=LLowdin)
 						if (Lowdin_numberOfBasis /= 0 ) then
 							isEnd = .true.
 							cycle
 						end if
 						counter = counter + 1
 					end do
 					
 					if(allocated(externalLowdin)) deallocate(externalLowdin)
 					allocate(externalLowdin(counter))
 					
 					rewind(4)
 					
 					do i = 1, counter
 						read(4,NML=LLowdin)
 						externalLowdin(i)%atom = trim(Lowdin_atom)
 						externalLowdin(i)%isotopicMass = Lowdin_isotopicMass
 						externalLowdin(i)%initialAtom = Lowdin_initialAtom
 						externalLowdin(i)%finalAtom = Lowdin_finalAtom
 						externalLowdin(i)%basisSetName = trim(Lowdin_basisSetName)
 						externalLowdin(i)%numberOfExternalLowdin = counter
 					end do
 			end select
 		end if
 		
 	end subroutine InputParser_setExternalBasis
	
	subroutine InputParser_writeNameList( unit, nameOfNameList )
		implicit none
		integer :: unit
		character(*) :: nameOfNameList
		
		select case(trim(nameOfNameList) )
		
			case("InputApmoParameters")
				write(UNIT=unit, NML=InputApmoParameters )
				
			case("InputTasks")
				write(UNIT=unit, NML=InputTasks )
				
			case("InputSystem")
				write(UNIT=unit, NML=InputSystem )
		
		end select
		
	end subroutine InputParser_writeNameList

end module InputParser_
