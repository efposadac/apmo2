!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, derivatives of repulsion integrals,           !
!!		recursive integrals,                                               !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief   Modulo para calculo de derivadas de integrales de atraccion/repulsion cuanticas. 
!
! Este modulo define una seudoclase cuyos metodos devuelven dericada de la
! la integral de atraccion o repulsion entre particulas cuanticas descrita mediante
! una distribucion gausiana sin normalizar. 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-01-15
!
! <b> Historial de modificaciones: </b>
!
! @see PrimitiveGaussian_, Math_ , PrimitiveGaussian_product() , Math_incompleteGamma()
!
! @todo Implementar integrales para funciones de momento angular 3.
!**
module RepulsionDerivatives_
	use PrimitiveGaussian_
	use RepulsionIntegrals_
	use Exception_
	
	implicit none
	
	private ::
		type(PrimitiveGaussian) :: primitiveAxB
		type(PrimitiveGaussian) :: primitiveCxD
		type(PrimitiveGaussian) :: primitiveAxBxCxD
		type(PrimitiveGaussian) :: g(4)
		real(8) :: reducedExponent_AB
		real(8) :: reducedExponent_CD
		real(8) :: reducedExponent_ABCD
		real(8) :: incompleteGammaArgument
		real(8) :: factorA, factorB
		integer :: nu  !! N�cleo respecto al que se deriva
		integer :: chi !! componente respecto a la cual se deriva.
		
		real(8) :: incompletGamma_0
		real(8) :: incompletGamma_1
		real(8) :: incompletGamma_2
		real(8) :: incompletGamma_3
		real(8) :: incompletGamma_4
		real(8) :: incompletGamma_5
		real(8) :: incompletGamma_6
		real(8) :: incompletGamma_7
		real(8) :: incompletGamma_8
		real(8) :: incompletGamma_9
			
		real(8) :: overlapFactor
	
	public ::  &
		PrimitiveGaussian_repulsionDerivative
		
	private :: &
		RepulsionDerivatives_factorA, &
		RepulsionDerivatives_factorB, &
		RepulsionDerivatives_adjustFactors, &
		RepulsionDerivatives_ssss, &
		RepulsionDerivatives_psss, &
		RepulsionDerivatives_psps, &
		RepulsionDerivatives_ppss, &
		RepulsionDerivatives_ppps, &
		RepulsionDerivatives_pppp, &
		RepulsionDerivatives_dsss, &
		RepulsionDerivatives_dsps, &
		RepulsionDerivatives_dspp, &
		RepulsionDerivatives_dsds, &
		RepulsionDerivatives_dpss, &
		RepulsionDerivatives_dpps, &
		RepulsionDerivatives_dppp, &
		RepulsionDerivatives_dpds, &
		RepulsionDerivatives_dpdp, &
		RepulsionDerivatives_ddss, &
		RepulsionDerivatives_ddps, &
		RepulsionDerivatives_ddpp, &
		RepulsionDerivatives_ddds, &
		RepulsionDerivatives_dddp, &
		RepulsionDerivatives_dddd
		
contains
	
	!**
	!  Retorna el valor de la integral <i> (ab,cd) </i> para cuatro 
	! funciones gaussianas sin normalizar de momento angular dado.
	!
	! @param primitiveA Gausiana primitiva
	! @param primitiveB Gausiana primitiva
	! @param primitiveC Gausiana primitiva
	! @param primitiveD Gausiana primitiva
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function PrimitiveGaussian_repulsionDerivative( primitiveA , primitiveB , primitiveC ,&
		primitiveD, center, component) result( output )
		implicit none
		type(PrimitiveGaussian) , intent(in) :: primitiveA
		type(PrimitiveGaussian) , intent(in) :: primitiveB
		type(PrimitiveGaussian) , intent(in) :: primitiveC
		type(PrimitiveGaussian) , intent(in) :: primitiveD
		real(8) :: output
		
		integer :: caseIntegral
		integer :: center
		integer :: component
		type(Exception) :: ex
	
		nu = center
		chi = component

		!! Ordena la gausianas de entrada de mayor a menor momento
		call PrimitiveGaussian_sort(g, primitiveA,primitiveB,primitiveC,primitiveD)
		
		!! Obtiene el producto de las gausianas de entrada
		primitiveAxB = PrimitiveGaussian_product(g(1),g(2))
		primitiveCxD = PrimitiveGaussian_product(g(3),g(4))
		primitiveAxBxCxD = PrimitiveGaussian_product(primitiveAxB,primitiveCxD)
	
		!! Calcula el exponente reducido
		reducedExponent_AB = PrimitiveGaussian_reducedOrbitalExponent( g(1) , g(2) )
		reducedExponent_CD = PrimitiveGaussian_reducedOrbitalExponent( g(3) , g(4) )
		reducedExponent_ABCD = PrimitiveGaussian_reducedOrbitalExponent( primitiveAxB , primitiveCxD )
		
		!!**********************************************************
		!! Evalua el argumento de la funci�n gamma incompleta 
		!!
		incompleteGammaArgument = reducedExponent_ABCD  * ( &
		( primitiveAxB.origin(1) - primitiveCxD.origin(1) )**2.0_8 + &
		( primitiveAxB.origin(2) - primitiveCxD.origin(2) )**2.0_8 + &
		( primitiveAxB.origin(3) - primitiveCxD.origin(3) )**2.0_8 )
		!!**********************************************************
		
		overlapFactor = sqrt( reducedExponent_ABCD / Math_PI ) &
			* (Math_PI/primitiveAxB.orbitalExponent)**1.5_8 &
			* PrimitiveGaussian_productConstant( g(1) , g(2) ) &
			* (Math_PI/primitiveCxD.orbitalExponent)**1.5_8 &
			* PrimitiveGaussian_productConstant( g(3) , g(4) )
		
 		
		!!*******************************************************************
		!! Seleccion del m�todo adecuado para el c�lculo de la integral
		!! de acuedo con el momento angular de las funciones de entrada.
		!!
		
		if ( abs(overlapFactor) > APMO_instance.INTEGRAL_THRESHOLD ) then
		
			!! Determina el procediento de la integral
			caseIntegral =  64 * PrimitiveGaussian_getAngularMoment(g(1)) + 16 * PrimitiveGaussian_getAngularMoment(g(2)) &
					+ 4 * PrimitiveGaussian_getAngularMoment(g(3)) +  PrimitiveGaussian_getAngularMoment(g(4))
			
			select case ( caseIntegral )
			
			case(0)
				output=RepulsionDerivatives_ssss(0)
			
			case(64)
				output=RepulsionDerivatives_psss(0)
		
			case(68)
				output=RepulsionDerivatives_psps(0)	
			
			case(80)
				output=RepulsionDerivatives_ppss(0)
			
			case(84)
				output=RepulsionDerivatives_ppps(0)
		
			case(85)
				output=RepulsionDerivatives_pppp(0)
			
			case(128)
				output=RepulsionDerivatives_dsss(0)
				
			case(132)
				output=RepulsionDerivatives_dsps(0)
				
			case(133)
				output=RepulsionDerivatives_dspp(0)
			
			case(136)
				output=RepulsionDerivatives_dsds(0)
				
			case(144)
				output=RepulsionDerivatives_dpss(0)
				
			case(148)
				output=RepulsionDerivatives_dpps(0)
			
			case(149)
				output=RepulsionDerivatives_dppp(0)
				
			case(152)
				output=RepulsionDerivatives_dpds(0)
			
			case(153)
				output=RepulsionDerivatives_dpdp(0)
			
			case(160)
				output=RepulsionDerivatives_ddss(0)
			
			case(164)
				output=RepulsionDerivatives_ddps(0)
			
			case(165)
				output=RepulsionDerivatives_ddpp(0)
			
			case(168)
				output=RepulsionDerivatives_ddds(0)
				
			case(169)
				output=RepulsionDerivatives_dddp(0)
			
			case(170)
				output=RepulsionDerivatives_dddd(0)
			
			case default
			
				output = 0.0_8
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the attraction derivative function" )
				call Exception_setDescription( ex, "This angular moment  isn't implemented" )
				call Exception_show( ex )
				
			end select
			!!*******************************************************************
			
		else
			output = 0.0_8
		end if
		
		
	
	end function PrimitiveGaussian_repulsionDerivative
	
	!**
	! Retorna el valor de la derivada de la integral <i> d(ps,ss)/dRa </i> 
	!
	! @return Valor de la derivada de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_ssss( order ) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
						
		!! Calcula la integral de repulsion
		output = 4.0_8 * overlapFactor &
			* (-(incompletGamma_A * (reducedExponent_AB &
			* Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) &
			- g(2).origin(chi)) + reducedExponent_AB &
			* Math_kroneckerDelta(g(2).owner, nu) * (-g(1).origin(chi) &
			+ g(2).origin(chi)) + reducedExponent_CD &
			* ( Math_kroneckerDelta(g(3).owner, nu) &
			- Math_kroneckerDelta(g(4).owner, nu)) * (g(3).origin(chi) &
			- g(4).origin(chi)))) + ( reducedExponent_ABCD * incompletGamma_B &
			* (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))&
			* (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu) &
			* g(1).orbitalExponent +   primitiveCxD.orbitalExponent &
			* Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent &
			- primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu) &
			* g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu) &
			* g(4).orbitalExponent))) &
			/ (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))
			 
		
	end function RepulsionDerivatives_ssss
	
	!**
	! Retorna el valor de la integral <i> (ps,ss) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_psss(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		integer :: alpha
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)

		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		
		factorA = RepulsionDerivatives_factorA()
		 
		output = overlapFactor * (2.0_8 * factorA * incompletGamma_B &
			* Math_kroneckerDelta(alpha, chi) + (2.0_8 * incompletGamma_A &
			* (-Math_kroneckerDelta(g(1).owner, nu) &
			+ Math_kroneckerDelta(g(2).owner, nu)) &
			* Math_kroneckerDelta(alpha, chi) * g(2).orbitalExponent) &
			/ primitiveAxB.orbitalExponent + 4.0_8 *(-g(1).origin(alpha) &
			+ primitiveAxB.origin(alpha)) * (-(incompletGamma_A &
			* (reducedExponent_AB*Math_kroneckerDelta(g(1).owner,nu) &
			* (g(1).origin(chi) - g(2).origin(chi)) &
			+ reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu) &
			* (-g(1).origin(chi) + g(2).origin(chi)) &
			+ reducedExponent_CD * ( Math_kroneckerDelta(g(3).owner, nu) &
			- Math_kroneckerDelta(g(4).owner, nu)) * (g(3).origin(chi) &
			- g(4).origin(chi)))) +  (reducedExponent_ABCD &
			* incompletGamma_B*(primitiveCxD.origin(chi) &
			- primitiveAxB.origin(chi)) * (primitiveCxD.orbitalExponent &
			* Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent &
			+ primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu) &
			* g(2).orbitalExponent - primitiveAxB.orbitalExponent &
			* (Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent &
			+ Math_kroneckerDelta(g(4).owner, nu) * g(4).orbitalExponent))) &
			/ (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) &
			+ 4.0_8*(primitiveAxBxCxD.origin(alpha) &
			- primitiveAxB.origin(alpha)) * (-(incompletGamma_B &
			* (reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu) &
			* (g(1).origin(chi) - g(2).origin(chi)) &
			+ reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu) &
			* (-g(1).origin(chi) + g(2).origin(chi)) &
			+ reducedExponent_CD * (Math_kroneckerDelta(g(3).owner, nu) &
			- Math_kroneckerDelta(g(4).owner, nu)) * (g(3).origin(chi) &
			- g(4).origin(chi))))+(reducedExponent_ABCD*incompletGamma_C &
			* (primitiveCxD.origin(chi)-primitiveAxB.origin(chi))&
			* (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu) &
			* g(1).orbitalExponent + primitiveCxD.orbitalExponent &
			* Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent &
			- primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu) &
			* g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu) &
			* g(4).orbitalExponent))) &
			/ (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) 
			
	end function RepulsionDerivatives_psss
	
	!**
	! Retorna el valor de la integral <i> (ps,ps) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_psps(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		integer :: alpha
		integer :: beta
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)

		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors()
		
		output = overlapFactor*(2.0_8*factorB*Math_kroneckerDelta(beta, chi)*& 
   			(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) &
   			- primitiveAxB.origin(alpha)) + incompletGamma_B &
   			* (-g(1).origin(alpha) + primitiveAxB.origin(alpha))) &
   			+ ( 2.0_8*(Math_kroneckerDelta(g(3).owner, nu) &
   			- Math_kroneckerDelta(g(4).owner, nu)) * Math_kroneckerDelta(beta, chi) &
   			* (incompletGamma_A*(g(1).origin(alpha) &
   			- primitiveAxB.origin(alpha)) + incompletGamma_B &
   			* (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha))) &
   			* g(4).orbitalExponent)/primitiveCxD.orbitalExponent &
   			+ ( 2.0_8*Math_kroneckerDelta(alpha, beta) * (-(incompletGamma_B*(reducedExponent_AB &
   			* Math_kroneckerDelta(g(1).owner, nu) * (g(1).origin(chi) &
   			- g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
  (-g(3).origin(beta) + primitiveCxD.origin(beta))*& 
   (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
    (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
           g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
    4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
  (primitiveAxBxCxD.origin(beta) - primitiveCxD.origin(beta))*(2.0_8*factorA*incompletGamma_C*& 
     Math_kroneckerDelta(alpha, chi) + & 
    (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
    4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))

	end function RepulsionDerivatives_psps
	
	!**
	! Retorna el valor de la integral <i> (pp,ss) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_ppss(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		integer :: alpha
		integer :: beta
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)

		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		
		factorA = RepulsionDerivatives_factorA() 
		
		output = overlapFactor*(2.0_8*factorA*Math_kroneckerDelta(beta, chi)*& 
  (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
   incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
 (2.0_8*(Math_kroneckerDelta(g(1).owner, nu) - & 
    Math_kroneckerDelta(g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*& 
   (incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
    incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
 (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
   (4.0_8*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
    (4.0_8*reducedExponent_ABCD*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
 (-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
  (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
   (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, nu))*& 
     Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
          g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
   4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
 (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
  (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
   (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
   4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) 
		
	end function RepulsionDerivatives_ppss
	
	!**
	! Retorna el valor de la integral <i> (ds,ss) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dsss(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		integer :: alpha
		integer :: beta
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)

		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		
		factorA = RepulsionDerivatives_factorA() 
		
		output = overlapFactor*(2.0_8*factorA*Math_kroneckerDelta(beta, chi)*& 
  (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
   incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
 (2.0_8*(Math_kroneckerDelta(g(1).owner, nu) - & 
    Math_kroneckerDelta(g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*& 
   (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
    incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
 (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
   (4.0_8*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
    (4.0_8*reducedExponent_ABCD*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
 (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
  (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
   (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, nu))*& 
     Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
          g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
   4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
 (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
  (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
   (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
   4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
    (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
         (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
         (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
         (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
         (g(3).origin(chi) - g(4).origin(chi)))) + & 
     (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
       (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
        primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
        primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
          Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))

	end function RepulsionDerivatives_dsss
	
	!**
	! Retorna el valor de la integral <i> (pp,ps) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_ppps(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)

		alpha= PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		kappa= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors() 

		output = overlapFactor*(factorB*Math_kroneckerDelta(kappa, chi)*& 
  (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, beta))/& 
    primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
   2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
 ((-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
     nu))*Math_kroneckerDelta(kappa, chi)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
     primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
      primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
  (2.0_8*factorA*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*reducedExponent_ABCD*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   (-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
            g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
 (0.5_8*(Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_B*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
  (2.0_8*factorA*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*reducedExponent_ABCD*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   (-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))
		
	end function RepulsionDerivatives_ppps
	
	
	!**
	! Retorna el valor de la integral <i> (dp,ss) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dpss(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)

		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa= PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )

		factorA = RepulsionDerivatives_factorA() 
		
		output = factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
  (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, beta))/& 
    primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
   2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
 (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, nu))*& 
   Math_kroneckerDelta(kappa, chi)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
     primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
 (overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
   (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
    (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
           g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
    4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
     (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
 (-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
  (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*overlapFactor*reducedExponent_ABCD*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
            g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
 (overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
   (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(beta, chi) + & 
    (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
           g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
    4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
          (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
          (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
          (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
          (g(3).origin(chi) - g(4).origin(chi)))) + & 
      (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
        (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
         primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
         primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
           Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
 (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
  (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*overlapFactor*reducedExponent_ABCD*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))
		
	end function RepulsionDerivatives_dpss
	
	!**
	! Retorna el valor de la integral <i> (ds,ps) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dsps( order ) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)

		alpha= PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors() 
		
		output = factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
  (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, beta))/& 
    primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
   2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
    (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
 (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
     nu))*Math_kroneckerDelta(kappa, chi)*& 
   (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
     primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
     (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
    2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
      primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
  (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*overlapFactor*reducedExponent_ABCD*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
            g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
 (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
  (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
    (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
     incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
   (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
     (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (4.0_8*overlapFactor*reducedExponent_ABCD*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
   overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
   overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
     (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
     4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
      (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
           (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
           (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
           (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
           (g(3).origin(chi) - g(4).origin(chi)))) + & 
       (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
         (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
          primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
          primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
            Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))
		
	end function RepulsionDerivatives_dsps
	
	!**
	! Retorna el valor de la integral <i> (pp,pp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_pppp(order)  result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		real(8) :: incompletGamma_F
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)
		incompletGamma_F = Math_incompletGamma( incompleteGammaArgument,order+5)

		alpha = PrimitiveGaussian_getPMomentComponent( g(1).angularMomentIndex )
		beta  = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		kappa = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors() 
		
		output = factorB*overlapFactor*Math_kroneckerDelta(lambda, chi)*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(beta) + primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
   (incompletGamma_D*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_C*& 
      (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
       Math_kroneckerDelta(alpha, kappa)*(-g(2).origin(beta) + primitiveAxB.origin(beta))))/& 
    (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + (overlapFactor*(Math_kroneckerDelta(g(3).owner, nu) - & 
    Math_kroneckerDelta(g(4).owner, nu))*Math_kroneckerDelta(lambda, chi)*& 
   ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
        primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
    (incompletGamma_C*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
          primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
         (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_B*& 
       (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        Math_kroneckerDelta(alpha, kappa)*(-g(2).origin(beta) + primitiveAxB.origin(beta))))/& 
     (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*g(3).orbitalExponent)/primitiveCxD.orbitalExponent + (0.5_8*Math_kroneckerDelta(kappa, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(beta, chi)*& 
         (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
           (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveCxD.orbitalExponent))/primitiveCxD.orbitalExponent + & 
 (-g(4).origin(lambda) + primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(beta) + & 
        primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_B*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
              g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
             (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
            nu))*Math_kroneckerDelta(beta, chi)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) + & 
 (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.*Math_kroneckerDelta(alpha, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
    overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_C*& 
         (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(2).origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.*Math_kroneckerDelta(beta, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
            nu))*Math_kroneckerDelta(beta, chi)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
            nu))*Math_kroneckerDelta(beta, chi)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(2).origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
   (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
            nu))*Math_kroneckerDelta(beta, chi)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_E*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_D*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_E*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_F*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))
		
	end function RepulsionDerivatives_pppp
	
	
	!**
	! Retorna el valor de la integral <i> (dp,ps) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dpps(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		real(8) :: incompletGamma_F
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)
		incompletGamma_F = Math_incompletGamma( incompleteGammaArgument,order+5)

		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors() 
			
		output = factorB*overlapFactor*Math_kroneckerDelta(lambda, chi)*& 
  ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_C*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
   (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_B*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_D*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_C*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
 (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
     nu))*Math_kroneckerDelta(lambda, chi)*& 
   ((Math_kroneckerDelta(beta, kappa)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_C*& 
        (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_B*& 
        (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
          primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
    (Math_kroneckerDelta(alpha, beta)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_A*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_C*& 
        (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_B*& 
        (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
          primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
 (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
  (factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (-g(2).origin(kappa) + primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
         (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_B*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
              g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
             (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) + & 
 (0.5_8*(Math_kroneckerDelta(kappa, lambda)*& 
     (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
      (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*& 
        (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
        (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
               (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
         (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(& 
                 g(1).owner, nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*& 
                Math_kroneckerDelta(g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
    Math_kroneckerDelta(beta, lambda)*(2.0_8*factorA*overlapFactor*& 
       Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
      (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      (0.5_8*Math_kroneckerDelta(alpha, kappa)*& 
        (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
               (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
         (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(& 
                 g(1).owner, nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*& 
                Math_kroneckerDelta(g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
    Math_kroneckerDelta(alpha, lambda)*& 
     (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_C*& 
         (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
      (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_C*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_B*& 
          (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      (0.5_8*Math_kroneckerDelta(beta, kappa)*& 
        (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
               (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
         (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(& 
                 g(1).owner, nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*& 
                Math_kroneckerDelta(g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  (factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (-g(2).origin(kappa) + primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_E*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_D*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_E*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_F*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))
			
	end function RepulsionDerivatives_dpps
	
	!**
	! Retorna el valor de la integral <i> (ds,pp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dspp(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		real(8) :: incompletGamma_F
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)
		incompletGamma_F = Math_incompletGamma( incompleteGammaArgument,order+5)

		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		call RepulsionDerivatives_adjustFactors() 
		
		output = factorB*overlapFactor*Math_kroneckerDelta(lambda, chi)*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (incompletGamma_D*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_C*& 
      (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
       Math_kroneckerDelta(alpha, kappa)*(-g(1).origin(beta) + primitiveAxB.origin(beta))))/& 
    (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + (overlapFactor*(Math_kroneckerDelta(g(3).owner, nu) - & 
    Math_kroneckerDelta(g(4).owner, nu))*Math_kroneckerDelta(lambda, chi)*& 
   ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
    (incompletGamma_C*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
          primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
         (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_B*& 
       (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        Math_kroneckerDelta(alpha, kappa)*(-g(1).origin(beta) + primitiveAxB.origin(beta))))/& 
     (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*g(3).orbitalExponent)/primitiveCxD.orbitalExponent + (Math_kroneckerDelta(kappa, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(beta, chi)*& 
         (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveCxD.orbitalExponent))/(2.0_8*primitiveCxD.orbitalExponent) + & 
 (-g(4).origin(lambda) + primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
         (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_B*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
              g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
             (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) + & 
 (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.0_8*Math_kroneckerDelta(alpha, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
    overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_C*& 
         (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(1).origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.0_8*Math_kroneckerDelta(beta, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
   (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_E*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_D*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_E*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_F*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))
	
	end function RepulsionDerivatives_dspp
	
	!**
	! Retorna el valor de la integral <i> (dd,ss) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_ddss(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		real(8) :: incompletGamma_F
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)
		incompletGamma_F = Math_incompletGamma( incompleteGammaArgument,order+5)

		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		
		factorA = RepulsionDerivatives_factorA() 
		
		output = factorA*overlapFactor*Math_kroneckerDelta(lambda, chi)*& 
  ((Math_kroneckerDelta(beta, kappa)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
      reducedExponent_ABCD*incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + & 
      incompletGamma_C*(reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
   (Math_kroneckerDelta(alpha, beta)*& 
     (primitiveAxB.orbitalExponent*incompletGamma_B*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + & 
      reducedExponent_ABCD*incompletGamma_D*(-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + & 
      incompletGamma_C*(reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - & 
        (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8) + & 
 (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, nu))*& 
   Math_kroneckerDelta(lambda, chi)*& 
   ((Math_kroneckerDelta(beta, kappa)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + reducedExponent_ABCD*incompletGamma_C*& 
        (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)) + incompletGamma_B*& 
        (reducedExponent_ABCD*g(1).origin(alpha) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(alpha) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
          primitiveAxB.origin(alpha))))/primitiveAxB.orbitalExponent**2.0_8 + (-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, kappa))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(2).origin(kappa) + & 
        primitiveAxB.origin(kappa))) + (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         kappa))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(2).origin(kappa) + primitiveAxB.origin(kappa))) + & 
    (Math_kroneckerDelta(alpha, beta)*& 
      (primitiveAxB.orbitalExponent*incompletGamma_A*(-g(2).origin(kappa) + primitiveAxB.origin(kappa)) + reducedExponent_ABCD*incompletGamma_C*& 
        (-primitiveAxBxCxD.origin(kappa) + primitiveAxB.origin(kappa)) + incompletGamma_B*& 
        (reducedExponent_ABCD*g(2).origin(kappa) + primitiveAxB.orbitalExponent*primitiveAxBxCxD.origin(kappa) - (primitiveAxB.orbitalExponent + reducedExponent_ABCD)*& 
          primitiveAxB.origin(kappa))))/primitiveAxB.orbitalExponent**2.0_8)*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
 (0.5_8*Math_kroneckerDelta(kappa, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(beta, chi)*& 
         (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
 (-g(2).origin(lambda) + primitiveAxB.origin(lambda))*& 
  (factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (-g(2).origin(kappa) + primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
         (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_B*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
              g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
             (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) + & 
 (0.5_8*Math_kroneckerDelta(beta, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(kappa, chi)*& 
      (incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(alpha, kappa)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(kappa, chi)*& 
         (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
           (-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(alpha, kappa)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
 (0.5_8*Math_kroneckerDelta(alpha, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      incompletGamma_B*(-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(kappa, chi)*& 
      (incompletGamma_B*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
       incompletGamma_A*(-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(beta, kappa)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_C*& 
          (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(kappa, chi)*& 
         (incompletGamma_C*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_B*& 
           (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(beta, kappa)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveAxB.origin(lambda))*& 
  (factorA*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(1).orbitalExponent)/primitiveAxB.orbitalExponent + & 
   (overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (-g(2).origin(kappa) + primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
      (reducedExponent_ABCD*(2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))/primitiveAxB.orbitalExponent))/(2.0_8*primitiveAxB.orbitalExponent) + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveAxB.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_E*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_D*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_E*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_F*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) 

		
	end function RepulsionDerivatives_ddss
	
	!**
	! Retorna el valor de la integral <i> (ds,ds) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dsds(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		real(8) :: incompletGamma_A
		real(8) :: incompletGamma_B
		real(8) :: incompletGamma_C
		real(8) :: incompletGamma_D
		real(8) :: incompletGamma_E
		real(8) :: incompletGamma_F
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		incompletGamma_A = Math_incompletGamma( incompleteGammaArgument,order)
		incompletGamma_B = Math_incompletGamma( incompleteGammaArgument,order+1)
		incompletGamma_C = Math_incompletGamma( incompleteGammaArgument,order+2)
		incompletGamma_D = Math_incompletGamma( incompleteGammaArgument,order+3)
		incompletGamma_E = Math_incompletGamma( incompleteGammaArgument,order+4)
		incompletGamma_F = Math_incompletGamma( incompleteGammaArgument,order+5)

		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		
		call RepulsionDerivatives_adjustFactors() 
		
		output = factorB*overlapFactor*Math_kroneckerDelta(lambda, chi)*& 
  ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (incompletGamma_D*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
         primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
        (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_C*& 
      (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
       Math_kroneckerDelta(alpha, kappa)*(-g(1).origin(beta) + primitiveAxB.origin(beta))))/& 
    (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent)) + (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + & 
    Math_kroneckerDelta(g(4).owner, nu))*Math_kroneckerDelta(lambda, chi)*& 
   ((-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta))) + (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
    (incompletGamma_C*(Math_kroneckerDelta(beta, kappa)*(primitiveAxBxCxD.origin(alpha) - & 
          primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, kappa)*& 
         (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))) + incompletGamma_B*& 
       (Math_kroneckerDelta(beta, kappa)*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)) + & 
        Math_kroneckerDelta(alpha, kappa)*(-g(1).origin(beta) + primitiveAxB.origin(beta))))/& 
     (primitiveCxD.orbitalExponent + primitiveAxB.orbitalExponent))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (0.5_8*Math_kroneckerDelta(kappa, lambda)*& 
   (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
     (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
      incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
    (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
        nu))*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
    (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
      (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
       (4.0_8*overlapFactor*reducedExponent_ABCD*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
    overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
             g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
            (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
    overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
     (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
      (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
          nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
      4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
      4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
       (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
            (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
             nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
            (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
            (g(3).origin(chi) - g(4).origin(chi)))) + & 
        (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
          (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
           primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
           primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + & 
             Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) - & 
    (reducedExponent_ABCD*(2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
        (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
       (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(beta, chi)*& 
         (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
          incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
         (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                 nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                 g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
               reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                  g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
            (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
              (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                 g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                  g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
             (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - (4.0_8*overlapFactor*reducedExponent_ABCD*& 
            (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
                 (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
                  nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
                 (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, & 
                   nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
             (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(& 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
                primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
                primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                    g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + & 
       overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
       overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
        (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
         (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
             g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
         4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
         4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
          (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
                g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*(& 
                -g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*(& 
                Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*(& 
                g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
             (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
               g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
              primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                  g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveCxD.orbitalExponent))/primitiveCxD.orbitalExponent + & 
 (-g(3).origin(lambda) + primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_A - reducedExponent_ABCD*incompletGamma_B)*Math_kroneckerDelta(alpha, beta))/& 
       primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_B*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_A*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*(-g(1).origin(beta) + & 
        primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*& 
    (2.0_8*factorA*overlapFactor*Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_A*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + incompletGamma_B*& 
         (-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_B*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_B*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_A*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_A*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*(g(1).origin(chi) - & 
              g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, nu)*& 
             (-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_B*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))) + & 
 (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.0_8*Math_kroneckerDelta(alpha, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
    overlapFactor*Math_kroneckerDelta(alpha, lambda)*& 
     (2.0_8*factorB*Math_kroneckerDelta(kappa, chi)*& 
       (incompletGamma_D*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + incompletGamma_C*& 
         (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
      (2.0_8*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
        Math_kroneckerDelta(kappa, chi)*& 
        (incompletGamma_B*(g(1).origin(beta) - primitiveAxB.origin(beta)) + & 
         incompletGamma_C*(-primitiveAxBxCxD.origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
      (2.0_8*Math_kroneckerDelta(beta, kappa)*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxBxCxD.orbitalExponent + & 
      (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_C*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*incompletGamma_D*& 
         Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))))))/primitiveAxBxCxD.orbitalExponent + & 
 (primitiveAxBxCxD.origin(lambda) - primitiveCxD.origin(lambda))*& 
  (factorB*overlapFactor*Math_kroneckerDelta(kappa, chi)*& 
    (((primitiveAxB.orbitalExponent*incompletGamma_C - reducedExponent_ABCD*incompletGamma_D)*Math_kroneckerDelta(alpha, & 
        beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
     2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
      (-g(1).origin(beta) + primitiveAxB.origin(beta))) + & 
   (overlapFactor*(-Math_kroneckerDelta(g(3).owner, nu) + Math_kroneckerDelta(g(4).owner, & 
       nu))*Math_kroneckerDelta(kappa, chi)*& 
     (((primitiveAxB.orbitalExponent*incompletGamma_B - reducedExponent_ABCD*incompletGamma_C)*Math_kroneckerDelta(alpha, & 
         beta))/primitiveAxB.orbitalExponent**2.0_8 + 2.0_8*(incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta)) + & 
      2.0_8*(incompletGamma_C*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_B*(-g(1).origin(alpha) + primitiveAxB.origin(alpha)))*& 
       (-g(1).origin(beta) + primitiveAxB.origin(beta)))*g(4).orbitalExponent)/primitiveCxD.orbitalExponent + & 
   (-g(3).origin(kappa) + primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_D*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_C*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_B*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_C*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_C*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_C*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_B*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_B*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_C*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))) + & 
   (0.5_8*(overlapFactor*Math_kroneckerDelta(beta, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
      overlapFactor*Math_kroneckerDelta(alpha, kappa)*& 
       (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(beta, chi) + & 
        (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(& 
            g(2).owner, nu))*Math_kroneckerDelta(beta, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
        4.0_8*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
         (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
        4.0_8*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
         (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_E*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))/primitiveAxBxCxD.orbitalExponent + & 
   (primitiveAxBxCxD.origin(kappa) - primitiveCxD.origin(kappa))*(2.0_8*factorA*overlapFactor*& 
      Math_kroneckerDelta(beta, chi)*& 
      (incompletGamma_E*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha)) + & 
       incompletGamma_D*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))) + & 
     (2.0_8*overlapFactor*(Math_kroneckerDelta(g(1).owner, nu) - Math_kroneckerDelta(g(2).owner, & 
         nu))*Math_kroneckerDelta(beta, chi)*& 
       (incompletGamma_C*(g(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        incompletGamma_D*(-primitiveAxBxCxD.origin(alpha) + primitiveAxB.origin(alpha)))*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
     (0.5_8*Math_kroneckerDelta(alpha, beta)*& 
       (4.0_8*overlapFactor*(-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
              (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
               nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
              (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
              (g(3).origin(chi) - g(4).origin(chi)))) + (reducedExponent_ABCD*incompletGamma_D*& 
            (primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*(primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*& 
              g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
             primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                 g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) - & 
        (4.0_8*overlapFactor*reducedExponent_ABCD*(-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, & 
                nu)*(g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(& 
                g(2).owner, nu)*(-g(1).origin(chi) + g(2).origin(chi)) + & 
              reducedExponent_CD*(Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(& 
                 g(4).owner, nu))*(g(3).origin(chi) - g(4).origin(chi)))) + & 
           (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
             (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + primitiveCxD.orbitalExponent*Math_kroneckerDelta(& 
                g(2).owner, nu)*g(2).orbitalExponent - primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*& 
                 g(3).orbitalExponent + Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent)))/& 
            (primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))/primitiveAxB.orbitalExponent))/primitiveAxB.orbitalExponent + overlapFactor*(-g(1).origin(beta) + primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_D*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_C*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_C*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_D*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent))) + & 
     overlapFactor*(primitiveAxBxCxD.origin(beta) - primitiveAxB.origin(beta))*& 
      (2.0_8*factorA*incompletGamma_E*Math_kroneckerDelta(alpha, chi) + & 
       (2.0_8*incompletGamma_D*(-Math_kroneckerDelta(g(1).owner, nu) + Math_kroneckerDelta(g(2).owner, & 
           nu))*Math_kroneckerDelta(alpha, chi)*g(2).orbitalExponent)/primitiveAxB.orbitalExponent + & 
       4.0_8*(-g(1).origin(alpha) + primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_D*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_E*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)) + & 
       4.0_8*(primitiveAxBxCxD.origin(alpha) - primitiveAxB.origin(alpha))*& 
        (-(incompletGamma_E*(reducedExponent_AB*Math_kroneckerDelta(g(1).owner, nu)*& 
             (g(1).origin(chi) - g(2).origin(chi)) + reducedExponent_AB*Math_kroneckerDelta(g(2).owner, & 
              nu)*(-g(1).origin(chi) + g(2).origin(chi)) + reducedExponent_CD*& 
             (Math_kroneckerDelta(g(3).owner, nu) - Math_kroneckerDelta(g(4).owner, nu))*& 
             (g(3).origin(chi) - g(4).origin(chi)))) + & 
         (reducedExponent_ABCD*incompletGamma_F*(primitiveCxD.origin(chi) - primitiveAxB.origin(chi))*& 
           (primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent + & 
            primitiveCxD.orbitalExponent*Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent - & 
            primitiveAxB.orbitalExponent*(Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent + Math_kroneckerDelta(& 
                g(4).owner, nu)*g(4).orbitalExponent)))/(primitiveCxD.orbitalExponent*primitiveAxB.orbitalExponent)))))
	
	end function RepulsionDerivatives_dsds
	
	
	!**
	! Retorna el valor de la integral <i> (dd,ps) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_ddps(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		
		g(3).angularMomentIndex(gamma) = 0
		
		output = (primitiveCxD.origin(gamma) - g(3).origin(gamma)) &
			* RepulsionDerivatives_ddss(order) + ( primitiveAxBxCxD.origin(gamma) &
			- primitiveCxD.origin(gamma) ) * RepulsionDerivatives_ddss(order+1)
			 
		if ( abs( Math_kroneckerDelta(gamma,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + RepulsionDerivatives_factorB() &
				* PrimitiveGaussian_repulsionIntegral( g(1), g(2), g(3), g(4), order + 1) &
				+ ( g(4).orbitalExponent / primitiveCxD.orbitalExponent ) &
				* (Math_kroneckerDelta(g(4).owner, nu) &
				- Math_kroneckerDelta(g(3).owner, nu)) &
				* PrimitiveGaussian_repulsionIntegral( g(1), g(2), g(3), g(4),order )
				
		end if
		
		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(gamma,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpss(order+1)
		
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(gamma,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpss(order+1)
		
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
				
		if ( abs( Math_kroneckerDelta(gamma,beta) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_dpss(order+1) 
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1
			
		end if

		if ( abs( Math_kroneckerDelta(gamma,alpha) - 1.0_8) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_dpss(order+1) 
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
		
		g(3).angularMomentIndex(gamma) = 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor
		
	end function RepulsionDerivatives_ddps
	
	
	!**
	! Retorna el valor de la integral <i> (dp,pp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dppp(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		gamma = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		g(4).angularMomentIndex(gamma) = 0
		
		output = (primitiveCxD.origin(gamma) - g(4).origin(gamma)) &
			* RepulsionDerivatives_dpps(order) + ( primitiveAxBxCxD.origin(gamma) &
			- primitiveCxD.origin(gamma) ) * RepulsionDerivatives_dpps(order+1)
		
		if ( abs( Math_kroneckerDelta(gamma,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(3).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(3).owner,nu) &
			- Math_kroneckerDelta(g(4).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
		
		if ( abs(Math_kroneckerDelta(gamma,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(lambda) = g(3).angularMomentIndex(lambda) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_dpss(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_dpss(order+1) )
				
			g(3).angularMomentIndex(lambda) = g(3).angularMomentIndex(lambda) + 1
			
		end if

		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(gamma,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_dsps(order +1 )
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_ppps( order +1 )
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_ppps( order +1 )
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) + 1
		
		end if

		g(4).angularMomentIndex(gamma) = 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor


	end function RepulsionDerivatives_dppp
		
	!**
	! Retorna el valor de la integral <i> (dp,ds) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**
	function  RepulsionDerivatives_dpds(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		
		g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
		output = (primitiveCxD.origin(gamma) - g(3).origin(gamma)) &
			* RepulsionDerivatives_dpps(order) + ( primitiveAxBxCxD.origin(gamma) &
			- primitiveCxD.origin(gamma) ) * RepulsionDerivatives_dpps(order+1)
			
		if ( abs( Math_kroneckerDelta(gamma,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(4).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(4).owner,nu) &
			- Math_kroneckerDelta(g(3).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
		
		if ( abs(Math_kroneckerDelta(gamma,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(lambda) = g(3).angularMomentIndex(lambda) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_dpss(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_dpss(order+1) )
				
			g(3).angularMomentIndex(lambda) = g(3).angularMomentIndex(lambda) + 1
			
		end if
			
		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(gamma,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_dsps(order +1 )
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_ppps( order +1 )
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_ppps( order +1 )
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) + 1
		
		end if
		
		g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor
	

	end function RepulsionDerivatives_dpds	
	
	!**
	! Retorna el valor de la integral <i> (dp,dp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**	
	function  RepulsionDerivatives_dpdp(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota 
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( g(2).angularMomentIndex )
		lambda= PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		iota  = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
		output = (primitiveCxD.origin(gamma) - g(3).origin(gamma)) &
			* RepulsionDerivatives_dppp(order) + ( primitiveAxBxCxD.origin(gamma) &
			- primitiveCxD.origin(gamma) ) * RepulsionDerivatives_dppp(order+1)
			
		if ( abs( Math_kroneckerDelta(gamma,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(4).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(4).owner,nu) &
			- Math_kroneckerDelta(g(3).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
			
		if ( abs(Math_kroneckerDelta(gamma,iota) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(4).angularMomentIndex(iota) = g(4).angularMomentIndex(iota) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_dpps(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_dpps(order+1) )
				
			g(4).angularMomentIndex(iota) = g(4).angularMomentIndex(iota) + 1
			
		end if
	
		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(gamma,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_dspp(order +1 )
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_pppp( order +1 )
			
			g(1).angularMomentIndex(beta) = g(1).angularMomentIndex(beta) + 1
		
		end if

		if ( abs(Math_kroneckerDelta(gamma,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) - 1
			
			auxFactor = auxFactor + RepulsionDerivatives_pppp( order +1 )
			
			g(1).angularMomentIndex(alpha) = g(1).angularMomentIndex(alpha) + 1
		
		end if
		
		call PrimitiveGaussian_interchange( g(3), g(4) )
				
		if ( abs(Math_kroneckerDelta(gamma,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(4).angularMomentIndex(lambda) = g(4).angularMomentIndex(lambda) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_dpps(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_dpps(order+1) )
				
			g(4).angularMomentIndex(lambda) = g(4).angularMomentIndex(lambda) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(3), g(4) )
		g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1		
				
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor
	
	end function RepulsionDerivatives_dpdp
	
	!**
	! Retorna el valor de la integral <i> (dd,pp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**	
	function  RepulsionDerivatives_ddpp(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getPMomentComponent( g(3).angularMomentIndex )
		iota  = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		g(4).angularMomentIndex(iota) = 0
		
		output = (primitiveCxD.origin(iota) - g(4).origin(iota)) &
			* RepulsionDerivatives_ddps(order) + ( primitiveAxBxCxD.origin(iota) &
			- primitiveCxD.origin(iota) ) * RepulsionDerivatives_ddps(order+1)
			
		if ( abs( Math_kroneckerDelta(iota,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(3).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(3).owner,nu) &
			- Math_kroneckerDelta(g(4).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
			
		if ( abs(Math_kroneckerDelta(iota,gamma) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddss(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddss(order+1) )
				
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
		end if

		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(iota,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1
			
		end if

		if ( abs(Math_kroneckerDelta(iota,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
		
		if ( abs(Math_kroneckerDelta(iota,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(iota,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1
			
		end if

		call PrimitiveGaussian_interchange( g(1), g(2) )
		g(4).angularMomentIndex(iota) = 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

	end function RepulsionDerivatives_ddpp
	
	!**
	! Retorna el valor de la integral <i> (dd,ds) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**	
	function  RepulsionDerivatives_ddds(order) result( output )
		implicit none
		real(8) :: output
		integer :: order
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota 
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		
		g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) - 1
		
		output = (primitiveCxD.origin(iota) - g(3).origin(iota)) &
			* RepulsionDerivatives_ddps(order) + ( primitiveAxBxCxD.origin(iota) &
			- primitiveCxD.origin(iota) ) * RepulsionDerivatives_ddps(order+1)
			
		if ( abs( Math_kroneckerDelta(iota,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(4).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(4).owner,nu) &
			- Math_kroneckerDelta(g(3).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
			
		if ( abs(Math_kroneckerDelta(iota,gamma) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddss(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddss(order+1) )
				
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
		end if

		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(iota,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1
			
		end if

		if ( abs(Math_kroneckerDelta(iota,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )
		
		if ( abs(Math_kroneckerDelta(iota,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(iota,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpps(order +1 ) 
				
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1
			
		end if

		call PrimitiveGaussian_interchange( g(1), g(2) )
		g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) + 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

	end function RepulsionDerivatives_ddds
	
	!**
	! Retorna el valor de la integral <i> (dd,dp) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**	
	function  RepulsionDerivatives_dddp(order) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		integer :: sigma 
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		sigma = PrimitiveGaussian_getPMomentComponent( g(4).angularMomentIndex )
		
		g(4).angularMomentIndex(sigma) = 0
		
		output = (primitiveCxD.origin(sigma) - g(4).origin(sigma)) &
			* RepulsionDerivatives_ddds(order) + ( primitiveAxBxCxD.origin(sigma) &
			- primitiveCxD.origin(sigma) ) * RepulsionDerivatives_ddds(order+1)
			
		if ( abs( Math_kroneckerDelta(sigma,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(3).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(3).owner,nu) &
			- Math_kroneckerDelta(g(4).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
		
		if ( abs(Math_kroneckerDelta(sigma,iota) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddps(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddps(order+1) )
				
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(sigma,gamma) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddps(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddps(order+1) )
				
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
		end if
		
		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(sigma,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpds(order +1 )
							
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(sigma,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpds(order +1 )
							
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
			
		end if
		
		call PrimitiveGaussian_interchange(g(1), g(2))
		
		if ( abs(Math_kroneckerDelta(sigma,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpds(order +1 )
							
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(sigma,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpds(order +1 )
							
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1
			
		end if
		
		call PrimitiveGaussian_interchange(g(1), g(2))
		g(4).angularMomentIndex(sigma) = 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

	end function RepulsionDerivatives_dddp
	
	!**
	! Retorna el valor de la integral <i> (dd,dd) </i> 
	!
	! @return Valor de la derivada de la integral de repulsion
	!**	
	function  RepulsionDerivatives_dddd( order ) result( output )
		implicit none
		integer :: order
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		integer :: gamma
		integer :: iota
		integer :: sigma 
		integer :: mu
		real(8) :: auxFactor
		
		alpha = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( g(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( g(2).angularMomentIndex, 2 )
		gamma = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 1 )
		iota  = PrimitiveGaussian_getDMomentComponent( g(3).angularMomentIndex, 2 )
		sigma = PrimitiveGaussian_getDMomentComponent( g(4).angularMomentIndex, 1 )
		mu    = PrimitiveGaussian_getDMomentComponent( g(4).angularMomentIndex, 2 )
		
		g(4).angularMomentIndex(mu) = g(4).angularMomentIndex(mu) - 1 
		
		output = (primitiveCxD.origin(mu) - g(4).origin(mu)) &
			* RepulsionDerivatives_dddp(order) + ( primitiveAxBxCxD.origin(mu) &
			- primitiveCxD.origin(mu) ) * RepulsionDerivatives_dddp(order+1)
			
		if ( abs( Math_kroneckerDelta(mu,chi) - 1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
		
			output = output + ( g(3).orbitalExponent &
			/ primitiveCxD.orbitalExponent ) * ( Math_kroneckerDelta(g(3).owner,nu) &
			- Math_kroneckerDelta(g(4).owner,nu) ) &
			* PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order) &
			+ RepulsionDerivatives_factorB() * PrimitiveGaussian_repulsionIntegral(g(1), g(2), g(3), g(4), order + 1)
		
		end if
		
		if ( abs(Math_kroneckerDelta(mu,sigma) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(4).angularMomentIndex(sigma) = g(4).angularMomentIndex(sigma) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddds(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddds(order+1) )
				
			g(4).angularMomentIndex(sigma) = g(4).angularMomentIndex(sigma) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(mu,iota) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddpp(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddpp(order+1) )
				
			g(3).angularMomentIndex(iota) = g(3).angularMomentIndex(iota) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(mu,gamma) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) - 1
		
			output = output + ( 0.5_8 / primitiveCxD.orbitalExponent ) &
				* ( RepulsionDerivatives_ddpp(order) - ( reducedExponent_ABCD &
				/ primitiveCxD.orbitalExponent ) * RepulsionDerivatives_ddpp(order+1) )
				
			g(3).angularMomentIndex(gamma) = g(3).angularMomentIndex(gamma) + 1
			
		end if

		auxFactor = 0.0_8
		
		if ( abs(Math_kroneckerDelta(mu,lambda) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpdp( order + 1)
					
			g(2).angularMomentIndex(lambda) = g(2).angularMomentIndex(lambda) + 1
			
		end if
		
		if ( abs(Math_kroneckerDelta(mu,kappa) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpdp( order + 1)
					
			g(2).angularMomentIndex(kappa) = g(2).angularMomentIndex(kappa) + 1
			
		end if
		
		call PrimitiveGaussian_interchange( g(1), g(2) )

		if ( abs(Math_kroneckerDelta(mu,beta) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpdp( order + 1)
					
			g(2).angularMomentIndex(beta) = g(2).angularMomentIndex(beta) + 1
			
		end if

		if ( abs(Math_kroneckerDelta(mu,alpha) -1.0_8 ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
			
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) - 1
		
			auxFactor = auxFactor + RepulsionDerivatives_dpdp( order + 1)
					
			g(2).angularMomentIndex(alpha) = g(2).angularMomentIndex(alpha) + 1
			
		end if

		call PrimitiveGaussian_interchange( g(1), g(2) )
		g(4).angularMomentIndex(mu) = g(4).angularMomentIndex(mu) + 1
		
		output = output + ( 0.5_8 / primitiveAxBxCxD.orbitalExponent ) &
			* auxFactor

	end function RepulsionDerivatives_dddd
	
	function RepulsionDerivatives_factorA() result ( output )
		implicit none
		real(8) output
		
		output = ( Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent &
			+ Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent &
			+ Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent &
			+ Math_kroneckerDelta(g(4).owner,nu)*g(4).orbitalExponent) &
			/ primitiveAxBxCxD.orbitalExponent &
			-((Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent &
			+ Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent) &
			/ primitiveAxB.orbitalExponent)
	
	end function RepulsionDerivatives_factorA 
	
	function RepulsionDerivatives_factorB() result ( output )
		implicit none
		real(8) output
		
		output = ( Math_kroneckerDelta(g(1).owner, nu)*g(1).orbitalExponent &
			+ Math_kroneckerDelta(g(2).owner, nu)*g(2).orbitalExponent &
			+ Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent &
			+ Math_kroneckerDelta(g(4).owner,nu)*g(4).orbitalExponent) &
			/ primitiveAxBxCxD.orbitalExponent &
			-((Math_kroneckerDelta(g(3).owner, nu)*g(3).orbitalExponent &
			+ Math_kroneckerDelta(g(4).owner, nu)*g(4).orbitalExponent) &
			/ primitiveCxD.orbitalExponent)
	
	end function RepulsionDerivatives_factorB
	
	subroutine RepulsionDerivatives_adjustFactors()
		
		factorA = RepulsionDerivatives_factorA()
		factorB = RepulsionDerivatives_factorB()
	
	end subroutine RepulsionDerivatives_adjustFactors
	
end module RepulsionDerivatives_
