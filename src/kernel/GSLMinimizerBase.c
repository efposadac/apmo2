/*!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************/

/**
 *  @brief Permite el acceso a libreria GSL
 *
 *  Permite accesar a metodos de GSL desde Fortran
 * 
 *  @author Sergio A. Gonz�lez
 *	
 *  <b> Fecha de creaci�n : </b> 2007-08-22
 *
 *
 *  <b> Historial de modificaciones : </b>
 *    - <tt>2007-08-24</tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
 *         -# Incoporo m�todos de acceso a funciones definidas en FORTRAN desde C
 *         -# Incoporo m�todos de acceso desde FORTRAN a C
 *
 **/
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_math.h>
#include <stdlib.h>
#include <math.h>
#include <cstdlib>
#include <stdio.h>
#include <time.h>


#ifdef __cplusplus
extern "C"
{
#endif

	#ifndef _ARCH_H_
	#define _ARCH_H_
	#define FNAME(x,y) x##_mp_##y##_
	#endif

	#define fgetfunctionValue FNAME(minimizer,minimizer_getfunctionvalue)
	#define fgetDerivative FNAME(minimizer,minimizer_getgradient)

	double fgetfunctionValue( double x[] ) ;
	double fgetDerivative( double x[] , int *component) ;


#ifdef __cplusplus
}
#endif

	struct structureOfArray{
		int sizeOfArray;
		double *elementsOfArray ;
	};
		
	/**
	* @brief Permite ejecutar rutinas de minimizacion de GSL
	*/
	extern "C" void gsl_minimizer_run( struct structureOfArray *valuesOfIndependentVariables , int *state) {
		
			
	}