!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************
!**
! @brief Permite obtener colocar informacion en  salidas o entradas del sistema
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2009-02-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2009-02-25 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!
!**
module InputOutput_
	use IFPORT
	use IFCORE
	use Matrix_
	use Vector_
	use Exception_
	use String_
	
	implicit none

	type, public :: InputOutput
		
		character(50) :: name
		character(50) :: fileName
		character(255) :: unit
		
		
		logical :: isInstanced
		
	end type InputOutput

	private ::
	 	type(InputOutput) :: InputOutput_instance
	
		
			
	public :: &
		InputOutput_constructor,&
		InputOutput_destructor, &
		InputOutput_writeMatrix, &
		InputOutput_writeVector, &
		InputOutput_make2DGraph, &
		InputOutput_writeString, &
		InputOutput_getMatrix, &
		InputOutput_getVector

contains
		
	!**
	! @brief Define el constructor para el modulo
	!**
	subroutine InputOutput_constructor()
		implicit none

	end subroutine InputOutput_constructor
	
	
	!**
	! @brief Define el destructor para el modulo
	!**
	subroutine InputOutput_destructor()
		implicit none
		
		if ( InputOutput_instance.isInstanced ) then
			InputOutput_instance.isInstanced = .false.
		end if
		
	end subroutine InputOutput_destructor
	
	!**
	! @brief Escribe una matriz en el luger especificado
	!**
	subroutine InputOutput_writeMatrix( mmatrix, unit, file, binary)
		implicit none
		type(Matrix) :: mmatrix
		integer, optional :: unit
		character(*), optional :: file
		logical, optional :: binary
		
		integer :: rows
		integer :: columns
		integer :: status
		integer :: m
		integer :: n
		logical :: bbinary

		bbinary = .false.
		if (present(binary)) bbinary = binary

		if (bbinary)  then

			if ( present( unit ) ) then

				!! Falta implementar

			else if ( present(file) ) then

	 				open ( 4,FILE=trim(file),STATUS='REPLACE',ACTION='WRITE', FORM ='UNFORMATTED')
					write(4), int(size(mmatrix.values), 8)
					write(4), mmatrix.values
					close(4)
			end if

		else
		
			if ( present( unit ) ) then

				!! Falta implementar

			else if ( present(file) ) then

	 				open ( 4,FILE=trim(file),STATUS='REPLACE',ACTION='WRITE')
					rows = size( mmatrix.values , DIM=1 )
					columns = size( mmatrix.values , DIM=2 )
					write (4,"(<columns>ES15.8)") ((mmatrix.values(m,n), n=1 ,columns) , m=1 , rows)
					close(4)
				

			end if
		end if

		
	end subroutine InputOutput_writeMatrix

	!**
	! @brief Escribe un vector en el luger especificado
	!**
	subroutine InputOutput_writeVector(vvector, unit, file, binary)
		implicit none

		type(Vector) :: vvector
		integer, optional :: unit
		character(*), optional :: file
		logical, optional :: binary
		
		integer :: elementsNum
 		integer :: status
		integer :: n
		logical :: bbinary
		
		bbinary = .false.
		if(present(binary)) bbinary = binary
		
		
		if ( present( unit ) ) then
			
			!! Falta implementar
			
		else if ( present(file) ) then
			if(bbinary) then
				open ( 4,FILE=trim(file),STATUS='REPLACE',ACTION='WRITE', FORM ='UNFORMATTED')
				write(4), int(size(vvector.values), 8)
				write(4), vvector.values
				close(4)
			
			else
			
				open ( 4,FILE=trim(file),STATUS='REPLACE',ACTION='WRITE')
				elementsNum = size( vvector.values )
				write (4,"(<elementsNum>ES15.8)") (vvector.values(n), n=1 , elementsNum)
				close(4)
			end if

		end if

	end subroutine InputOutput_writeVector
	
	subroutine InputOutput_make2DGraph(fileName, title, x_title, y_title,&
	       x_format, y_format, x_range, y_range, numOfGraphs)
	       implicit none
	       character(*) :: fileName
	       character(*) :: title
	       character(*) :: x_title
	       character(*) :: y_title
	       character(*), optional :: x_format
	       character(*), optional :: y_format
	       character(*), optional :: x_range
	       character(*), optional :: y_range
	       integer, optional :: numOfGraphs

	       integer :: i,status
	       character(20) :: charNumOfGraph
	       character(20) :: auxXformat
	       character(20) :: auxYformat
       	       character(20) :: auxXRange
	       character(20) :: auxYRange

	       integer :: auxNumOfGraphs
	       
	       auxXformat="%.2f"
	       if(present(x_format)) auxXformat=trim(x_format)
	       
	       auxYformat="%.2f"
	       if(present(y_format)) auxYformat=trim(y_format)
	       
	       auxXRange=" [] "
	       if(present(x_range)) auxXRange=' ['//trim(x_range)//'] '
	       
	       auxYRange="[] "
	       if(present(y_range)) auxYRange='['//trim(y_range)//'] '
	       
	       auxNumOfGraphs=1
	       if(present(numOfGraphs)) auxNumOfGraphs=numOfGraphs
	       
	       open ( 5,FILE=trim(fileName)//".gnp", STATUS='REPLACE',ACTION='WRITE')
	       write (5,"(A)") 'set term post eps enh color dashed rounded dl 4 "Times-Bold" 15'
	       write (5,"(A)") 'set output "'//trim(fileName)//'.eps"'
	       write (5,"(A)") 'set encoding iso_8859_1'
	       write (5,"(A)") 'set title "'//trim(title)//'"'
	       write (5,"(A)") 'set xlabel "'//trim(x_title)//'"'
	       write (5,"(A)") 'set format x "'//trim(auxXformat)//'"'
	       write (5,"(A)") 'set ylabel "'//trim(y_title)//'"'
	       write (5,"(A)") 'set format y "'//trim(auxYformat)//'"'
	       if( auxNumOfGraphs >1) then
		    write (5,"(A$)") 'plot '//trim(auxXRange)//trim(auxYRange)//' "'//trim(fileName)//'.dat"'//' using 1:2 w l title ""'
		    do i=2, auxNumOfGraphs
			 charNumOfGraph=String_convertIntegerToString(i+1)
			 write (5,"(A$)") ', "'//trim(fileName)//'.dat"'//' using 1:'//trim(charNumOfGraph)//' w l  title ""'
		    end do
		    write (5,"(A)") ""
	       else
		    write (5,"(A)") 'plot '//trim(auxXRange)//trim(auxYRange)//' "'//trim(fileName)//'.dat"'//' w l title ""'
	       end if
	       write (5,"(A)") 'set output'
	       close(5)

	       status= system("gnuplot "//trim(fileName)//".gnp")
	  
	end subroutine InputOutput_make2DGraph

	
	!**
	! @brief Escribe una cadena de caracteres en el luger especificado
	!**
	subroutine InputOutput_writeString()
		implicit none
		
		
	end subroutine InputOutput_writeString
	
	!**
	! @brief Obtiene una matriz del lugar especificado
	!**
	function InputOutput_getMatrix(rows, columns, unit, file, binary) result( output )
		implicit none
		integer, intent(in) :: rows
		integer, intent(in) :: columns
		integer, optional :: unit
 		character(*), optional :: file
 		logical, optional :: binary
		type(Matrix) :: output
		
		type(Exception) :: ex
		character(5000) :: line
		real(8), allocatable :: values(:)
		integer(8) :: totalSize
		integer :: status
		integer :: existFile
		integer :: m, i
		integer :: n, j
		logical :: bbinary

		bbinary = .false.
		if(present(binary)) bbinary = binary

		if(bbinary) then
			if ( present( unit ) ) then

				!! Falta implementar

			else if ( present(file) ) then

				inquire( FILE = trim(file), EXIST = existFile )

				if ( existFile ) then

					open( 4, FILE=trim(file), ACTION='read', FORM='unformatted', STATUS='old' )

					!!Comprueba el tamanho de las matrices
					read(4) totalSize

					if(totalSize == int(columns*rows,8)) then

						call Matrix_constructor( output, int(rows,8), int(columns,8) )

						if (allocated(values)) deallocate(values)
						allocate( values(totalSize) )
						read(4) values
						close(4)

						m = 1
						do i=1,columns
							do j=1,columns
								output%values(j, i) = values(m)
								m = m + 1
							end do
						end do

					else
						close(4)

						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object InputOutput  in the getMatrix() function" )
						call Exception_setDescription( ex, "The dimensions of the matrix "//trim(file)//" are wrong " )
						call Exception_show( ex )
						call Exception_destructor( ex )

					end if
				else

					call Exception_constructor( ex , ERROR )
					call Exception_setDebugDescription( ex, "Class object InputOutput  in the getMatrix() function" )
					call Exception_setDescription( ex, "The file "//trim(file)//" don't exist " )
					call Exception_show( ex )
					call Exception_destructor( ex )

				end if
			end if

		else

			if ( present( unit ) ) then

				!! Falta implementar

			else if ( present(file) ) then
			
				inquire( FILE = trim(file), EXIST = existFile )

				if ( existFile ) then

					call Matrix_constructor( output, int(rows,8), int(columns,8) )
					open ( 4,FILE=trim(file),STATUS='unknown',ACCESS='sequential' )
					!! verifica el tamahno de las matrice
					read (4,"(A)") line
					if ( len_trim(line) /=columns*15) then

						close(4)

						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object InputOutput  in the getMatrix() function" )
						call Exception_setDescription( ex, "The dimensions of the matrix "//trim(file)//" are wrong " )
						call Exception_show( ex )
						call Exception_destructor( ex )
					else
						rewind(4)
					end if
					read ( 4,"(<columns>ES15.8)") ( (output.values(m,n), n=1 ,columns) , m=1, rows )
					close(4)

				else

					call Exception_constructor( ex , ERROR )
					call Exception_setDebugDescription( ex, "Class object InputOutput  in the getMatrix() function" )
					call Exception_setDescription( ex, "The file "//trim(file)//" don't exist " )
					call Exception_show( ex )
					call Exception_destructor( ex )

				end if

			end if

		end if

	end function InputOutput_getMatrix
	
	!**
	! @brief Obtiene un vector  del lugar especificado
	!**
	function InputOutput_getVector(elementsNum, unit, file, binary) result( output )
		implicit none
		integer, intent(in) :: elementsNum
		integer, optional :: unit
 		character(*), optional :: file
		logical, optional :: binary
		type(Vector) :: output
		
		type(Exception) :: ex
		character(5000) :: line
		integer :: status
		integer :: existFile
		integer :: n
		integer(8) :: totalSize
		logical :: bbinary
		
		bbinary = .false.
		if(present(binary)) bbinary = binary

		if ( present( unit ) ) then
			
			!! Falta implementar
			
		else if ( present(file) ) then
		
			inquire( FILE = trim(file), EXIST = existFile )
			
			if ( existFile ) then
			
				if(bbinary) then
					open( 4, FILE=trim(file), ACTION='read', FORM='unformatted', STATUS='old' )
					
					read(4) totalSize
					if(totalSize == int(elementsNum,8)) then
					
						call Vector_constructor( output, elementsNum )

						read(4) output.values
						close(4)
						
						!print*, output.values
						
					else
						close(4)

						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object InputOutput  in the getVector() function" )
						call Exception_setDescription( ex, "The dimensions of the vector in "//trim(file)//" are wrong " )
						call Exception_show( ex )
						call Exception_destructor( ex )
					end if
				else
			
					call Vector_constructor( output, elementsNum )
					open ( 4,FILE=trim(file),STATUS='unknown',ACCESS='sequential' )
					!! verifica el tamahno de las matrice
					read (4,"(A)") line
					if ( len_trim(line) /=elementsNum*15) then
						close(4)

	!!					call tracebackqq(string="Probando trace ",USER_EXIT_CODE=-1,EPTR=%LOC(ExceptionInfo))


						call Exception_constructor( ex , ERROR )
						call Exception_setDebugDescription( ex, "Class object InputOutput  in the getVector() function" )
						call Exception_setDescription( ex, "The dimensions of the vector in "//trim(file)//" are wrong " )
						call Exception_show( ex )
						call Exception_destructor( ex )
					else
						rewind(4)
					end if
					read ( 4,"(<elementsNum>ES15.8)") (output.values(n), n=1 ,elementsNum)
					close(4)
					
				end if
			else
			
				call Exception_constructor( ex , ERROR )
				call Exception_setDebugDescription( ex, "Class object InputOutput  in the getVector() function" )
				call Exception_setDescription( ex, "The file "//trim(file)//" don't exist " )
				call Exception_show( ex )
				call Exception_destructor( ex )
			
			end if

		end if

		
	end function InputOutput_getVector

end module InputOutput_
