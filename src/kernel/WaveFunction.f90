!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Modulo para manejo global de funciones de onda 
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-10-01
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-10-01 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos
!
!**
module WaveFunction_
	use ParticleManager_
	use MolecularSystem_
	use RHFWaveFunction_
	use ROHFWaveFunction_
	use Matrix_
	use Vector_	
	implicit none
	
	type, public :: WaveFunction
		
		character(30) :: name
		
		type(RHFWaveFunction), pointer :: rhfWaveFunctionPtr(:)
		type(ROHFWaveFunction), pointer :: rohfWaveFunctionPtr(:)
		
		logical :: isInstanced 	!< Variable por conveniencia
	
	end type WaveFunction
	
	
	type(WaveFunction), public, target :: WaveFunction_instance
	
	public :: &
		WaveFunction_constructor, &
		WaveFunction_destructor, &
		WaveFunction_builtDensityMatrix, &
		WaveFunction_builtFockMatrix, &
		WaveFunction_isInstanced, &
		WaveFunction_getFockMatrixPtr, &
		WaveFunction_getDensityMatrixPtr, &
		WaveFunction_getTransformationMatrixPtr, &
		WaveFunction_getOverlapMatrixPtr, &
		WaveFunction_getWaveFunctionCoefficientsPtr, &
		WaveFunction_getWaveFunctionCoefficients,&
		WaveFunction_getMolecularOrbitalsEnergyPtr, &
		WaveFunction_getMolecularOrbitalsEnergy, &
		WaveFunction_getEnergy, &
		WaveFunction_getTotalEnergy, &
		WaveFunction_obtainEnergy, &
		WaveFunction_obtainTotalEnergy
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine WaveFunction_constructor( )
		implicit none
		
		
		if ( .not.WaveFunction_instance.isInstanced ) then
		
			if ( RHFWaveFunction_isInstanced() ) then
				
				WaveFunction_instance.rhfWaveFunctionPtr => RHFWaveFunction_instance
 				call MolecularSystem_setCoefficientsOfCombinationPtr( RHFWaveFunction_getWaveFunctionCoefficientsPtr() )
				call MolecularSystem_setDensityMatrixPtr( RHFWaveFunction_getDensityMatrixPtr() )
				call MolecularSystem_setEnergyOfMolecularOrbitalsPtr( RHFWaveFunction_getMolecularOrbitalsEnergyPtr() )
				call MolecularSystem_setEnergyComponentsPtr( RHFWaveFunction_getNameOfSpeciesPtr(),RHFWaveFunction_getKineticEnergyPtr(), &
					RHFWaveFunction_getRepulsionEnergyPtr(), RHFWaveFunction_getCouplingEnergyPtr(), RHFWaveFunction_getQuantPuntualEnergyPtr() )
				
					
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				WaveFunction_instance.rohfWaveFunctionPtr => ROHFWaveFunction_instance
			
			end if
			
			WaveFunction_instance.isInstanced=.true.
			
		end if
				
	end subroutine WaveFunction_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine WaveFunction_destructor( )
		implicit none
		
		if ( WaveFunction_instance.isInstanced ) then
			if ( RHFWaveFunction_isInstanced() ) then
				
				WaveFunction_instance.rhfWaveFunctionPtr => null()
 				
				call MolecularSystem_setCoefficientsOfCombinationPtr()
				call MolecularSystem_setDensityMatrixPtr()
				call MolecularSystem_setEnergyOfMolecularOrbitalsPtr()
				call MolecularSystem_setEnergyComponentsPtr()
				
					
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				WaveFunction_instance.rohfWaveFunctionPtr => null()
			
			end if
			
			WaveFunction_instance.isInstanced=.false.
			
		end if

	end subroutine WaveFunction_destructor
	
	!**
	! @brief Retorna una apuntador a la matrix de valores propios de matriz de Fock
	!
	!**
	subroutine WaveFunction_builtDensityMatrix( nameOfSpecie )
		implicit none
		character(*) :: nameOfSpecie
		
		if ( WaveFunction_instance.isInstanced ) then
		
			if ( RHFWaveFunction_isInstanced() ) then

				call RHFWaveFunction_builtDensityMatrix( nameOfSpecie )
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				call ROHFWaveFunction_builtDensityMatrix( nameOfSpecie )
			
			end if
			
		end if
				
	end subroutine WaveFunction_builtDensityMatrix
	
	!**
	! @brief Retorna una apuntador a la matrix de valores propios de matriz de Fock
	!
	!  @todo Debe incluirse la suma del termino de acoplamiento en el caso ROHF
	!**
	subroutine WaveFunction_builtFockMatrix( nameOfSpecie )
		implicit none
		character(*) :: nameOfSpecie
		
		if ( WaveFunction_instance.isInstanced ) then
		
			if ( RHFWaveFunction_isInstanced() ) then
 				!! Construye la matriz de dos particulas, matriz de acoplamiento  y la matriz de Fock
				call RHFWaveFunction_buildTwoParticlesMatrix(nameOfSpecie)
				call RHFWaveFunction_buildCouplingMatrix(nameOfSpecie)
 				call RHFWaveFunction_buildFockMatrix(nameOfSpecie) 
				
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				!! Construye la matriz de dos particulas y la matriz de Fock
 				call ROHFWaveFunction_buildTwoParticlesMatrix(nameOfSpecie)
				call ROHFWaveFunction_buildFockMatrix(nameOfSpecie) 
			
			end if
			
		end if
				
	end subroutine WaveFunction_builtFockMatrix

	
	!**
	! @brief indica el objeto ha sido instanciado 
	!
	!**					
	function WaveFunction_isInstanced() result(output)
		implicit none
		logical :: output
		
		output = WaveFunction_instance.isInstanced
	
	end function WaveFunction_isInstanced
	
	!**
	! @brief Retorna una apuntador a la matrix de Fock solicitada
	!
	!**
	function WaveFunction_getBuiltStatePtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		logical, pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
		
			if ( RHFWaveFunction_isInstanced() ) then
				
				output => RHFWaveFunction_instance(specieID).wasBuiltFockMatrix
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).wasBuiltFockMatrix
			
			end if
			
		end if
				
	end function WaveFunction_getBuiltStatePtr
	
	
	!**
	! @brief Retorna una apuntador a la matrix de Fock solicitada
	!
	!**
	function WaveFunction_getFockMatrixPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
		
			if ( RHFWaveFunction_isInstanced() ) then
				
				output => RHFWaveFunction_instance(specieID).fockMatrix
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).fockMatrix
			
			end if
			
		end if
				
	end function WaveFunction_getFockMatrixPtr
	
	!**
	! @brief Retorna una apuntador a la matrix de densidad solicitada
	!
	!**
	function WaveFunction_getDensityMatrixPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
			
			if ( RHFWaveFunction_isInstanced() ) then

				output => RHFWaveFunction_instance(specieID).densityMatrix
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).densityMatrix
			
			end if
			
		end if
				
	end function WaveFunction_getDensityMatrixPtr
	
	
	!**
	! @brief Retorna una apuntador a la matrix de densidad solicitada
	!
	!**
	function WaveFunction_getTransformationMatrixPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
			
			if ( RHFWaveFunction_isInstanced() ) then

				output => RHFWaveFunction_instance(specieID).transformationMatrix
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).transformationMatrix
			
			end if
			
		end if
				
	end function WaveFunction_getTransformationMatrixPtr
	

	!**
	! @brief Retorna una apuntador a la matrix de densidad solicitada
	!
	!**
	function WaveFunction_getOverlapMatrixPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
			
			if ( RHFWaveFunction_isInstanced() ) then

				output => RHFWaveFunction_instance(specieID).overlapMatrixValuesPtr
			
			end if
			
		end if
				
	end function WaveFunction_getOverlapMatrixPtr
	


	
	!**
	! @brief Retorna la matrix de coeficientes de combinacion
	!
	!**
	function WaveFunction_getWaveFunctionCoefficients( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix) :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			
			if ( RHFWaveFunction_isInstanced() ) then

				output = RHFWaveFunction_instance(specieID).waveFunctionCoefficients
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output = ROHFWaveFunction_instance(specieID).waveFunctionCoefficients
			
			end if
			
		end if
				
	end function WaveFunction_getWaveFunctionCoefficients
	
	!**
	! @brief Retorna una apuntador a la matrix de coeficientes de combinacion
	!
	!**
	function WaveFunction_getWaveFunctionCoefficientsPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Matrix), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
			
			if ( RHFWaveFunction_isInstanced() ) then

				output => RHFWaveFunction_instance(specieID).waveFunctionCoefficients
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).waveFunctionCoefficients
			
			end if
			
		end if
				
	end function WaveFunction_getWaveFunctionCoefficientsPtr

		
	!**
	! @brief 	Retorna un apuntador al vector de valores propios de la matriz de Fock
	!		para la especie seleccionada
	!
	!**
	function WaveFunction_getMolecularOrbitalsEnergyPtr( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Vector), pointer :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output => null()
			
			if ( RHFWaveFunction_isInstanced() ) then

				output => RHFWaveFunction_instance(specieID).molecularOrbitalsEnergy
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output => ROHFWaveFunction_instance(specieID).molecularOrbitalsEnergy
			
			end if
			
		end if
				
	end function WaveFunction_getMolecularOrbitalsEnergyPtr
	
	!**
	! @brief 	Retorna el vector de valores propios de la matriz de Fock para
	!		la especie seleccionada
	!
	!**
	function WaveFunction_getMolecularOrbitalsEnergy( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		type(Vector):: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			
			if ( RHFWaveFunction_isInstanced() ) then

				call Vector_copyConstructor(output, RHFWaveFunction_instance(specieID).molecularOrbitalsEnergy )
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				call Vector_copyConstructor(output, ROHFWaveFunction_instance(specieID).molecularOrbitalsEnergy )
			
			end if
			
		end if
				
	end function WaveFunction_getMolecularOrbitalsEnergy
	
	
	!**
	! @brief Retorna una apuntador a la matrix de valores propios de matriz de Fock
	!
	!**
	function WaveFunction_getEnergy( specieID )  result( output )
		implicit none
		integer, intent(in) :: specieID
		real(8) :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			
			if ( RHFWaveFunction_isInstanced() ) then

				output = RHFWaveFunction_instance( specieID ).totalEnergyForSpecie
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				output = ROHFWaveFunction_instance(specieID).specieEnergy
			
			end if
			
		end if
				
	end function WaveFunction_getEnergy

	!**
	! @brief Retorna el valor total de la energia para el sistema estudiado
	!
	!**
	function WaveFunction_getTotalEnergy()  result( output )
		implicit none
		real(8) :: output
		
		if ( WaveFunction_instance.isInstanced ) then
		
			output = MolecularSystem_getTotalEnergy()
			
		end if
				
	end function WaveFunction_getTotalEnergy
	
	!**
	! @brief Retorna una apuntador a la matrix de valores propios de matriz de Fock
	!
	!**
	subroutine WaveFunction_obtainEnergy( nameOfSpecie )
		implicit none
		character(*) :: nameOfSpecie
		
		if ( WaveFunction_instance.isInstanced ) then
		
			if ( RHFWaveFunction_isInstanced() ) then

				call RHFWaveFunction_obtainTotalEnergyForSpecie( nameOfSpecie )
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				call ROHFWaveFunction_obtainTotalEnergyForSpecie( nameOfSpecie )
			
			end if
			
		end if
				
	end subroutine WaveFunction_obtainEnergy

	!**
	! @brief Calcula la energia OMNE total para el sistema estudiado
	!
	! @todo Falta implementacion para ROHF
	!**
	subroutine WaveFunction_obtainTotalEnergy( )
		implicit none
		
		integer :: speciesIterator
		character(30) :: nameOfSpecieSelected
		real(8) :: totalEnergy
		real(8) :: totalCouplingEnergy
		
		totalEnergy = 0.0_8
		
		if ( WaveFunction_instance.isInstanced ) then
		
			if ( RHFWaveFunction_isInstanced() ) then

				call ParticleManager_rewindSpecies()
		
				do speciesIterator = 1, ParticleManager_endSpecie()
				
					nameOfSpecieSelected =  trim( ParticleManager_iterateSpecie() )
					call RHFWaveFunction_obtainEnergyComponents( trim(nameOfSpecieSelected ) )
					totalEnergy = totalEnergy + RHFWaveFunction_getEnergy( trim(nameOfSpecieSelected), "independentSpecieEnergy" )

				end do

				!! Adicionado energia de interaccion entre particulas puntuales
				totalEnergy = totalEnergy + ParticleManager_puntualParticlesEnergy()
				!! Adicionar  energia de acoplamiento
				totalCouplingEnergy = RHFWaveFunction_getTotalCoupligEnergy()
				totalEnergy = totalEnergy +  totalCouplingEnergy

				call MolecularSystem_setTotalEnergy( totalEnergy )
				call MolecularSystem_setTotalCouplingEnergy( totalCouplingEnergy )
			
			else if ( ROHFWaveFunction_isInstanced() ) then
				
				call MolecularSystem_setTotalEnergy(0.0_8)
			
			end if

		end if
				
	end subroutine WaveFunction_obtainTotalEnergy


		
end module WaveFunction_
