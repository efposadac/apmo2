!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Procediminetos RHF dentro del formalismo OMNE 
! 
!  Este modulo permite realizar calculos de funcion de onda dentro del 
! formlismo OMNE
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-09-17
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2008-09-17 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci�n de modulos y metodos basicos
!
!**
module RHFSolver_
	use SCFInterspecies_
	use MolecularSystem_
	use ParticleManager_
	use IntegralManager_
	use DensityMatrixSCFGuess_
	use SCFConvergence_
	use InputOutput_
	use ExternalPotentialManager_
	use RHFWaveFunction_
	use Exception_
	implicit none
	
	type, public :: RHFSolver
		
		character(30) :: name
	
	end type RHFSolver
	
	
	public :: &
		RHFSolver_constructor, &
		RHFSolver_destructor, &
		RHFSolver_setInitialGuess, &
		RHFSolver_run, &
		RHFSolver_showCyclesSCFForSpecies, &
		RHFSolver_reset
	
contains
	
	subroutine RHFSolver_constructor()
		implicit none
		

		call RHFWaveFunction_constructor()
		call SCFInterspecies_constructor()
		call MolecularSystem_setMethodName("RHF")
		
	end subroutine RHFSolver_constructor
	
	subroutine RHFSolver_destructor()
		implicit none
		
		call WaveFunction_destructor()
		call RHFWaveFunction_destructor()
		call SCFInterspecies_destructor()
		call MolecularSystem_setMethodName("")
		
	end subroutine RHFSolver_destructor
	
	subroutine RHFSolver_setInitialGuess( densityType, nameOfSpecie )
		implicit none
		character(*), intent(in) :: densityType
		character(*), intent(in), optional :: nameOfSpecie
		
		character(30) :: nameOfSpecieSelected
		type(Matrix) :: auxDensity
		integer :: specieID
		integer(8) :: orderOfMatrix
		integer :: existFile
		
		nameOfSpecieSelected="e-"
		if ( present(nameOfSpecie) ) nameOfSpecieSelected=trim(nameOfSpecie)

		auxDensity=DensityMatrixSCFGuess_getGuess( trim(densityType), trim(nameOfSpecieSelected) )

		call RHFWaveFunction_setDensityMatrix(  auxDensity, trim(nameOfSpecieSelected) )

		call Matrix_destructor(auxDensity)

	end subroutine RHFSolver_setInitialGuess
	
	subroutine RHFSolver_run()
		implicit none
		
		character(30) :: nameOfSpecieSelected
		character(30) :: nameOfOtherSpecie
		real(8) :: auxValue
		real(8) :: deltaEnergy
		integer :: speciesIterator
		integer :: status
		integer :: numberOfSpecies
		integer :: i,j
		logical :: auxVal


		auxValue = 0.0_8
		auxVal = APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS
		call ParticleManager_rewindSpecies()
		
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!! Realiza ciclo inicial sobre cada especie individualmente
		!!
		do speciesIterator = ParticleManager_beginSpecie(), ParticleManager_endSpecie()
		
			nameOfSpecieSelected =  trim( ParticleManager_iterateSpecie() )
			
			!!Especifica el procedimiento a seguir si lo electrones han sido congelados
			if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
				if(ParticleManager_isFrozen(trim(nameOfSpecieSelected))) then
					write(*,*) trim(nameOfSpecieSelected)//" HAVE BEEN FROZEN !!!"
					APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = .true.
				else
					APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxVal
				end if
			end if
			
			!! Construye matrix de overlap
			call RHFWaveFunction_buildOverlapMatrix( trim( nameOfSpecieSelected ) )
			
			!! Construye matrix de transformacion
 			call RHFWaveFunction_buildTransformationMatrix( trim( nameOfSpecieSelected ), 2 )

			!! Construye matrix de Hcore
			call RHFWaveFunction_buildIndependentParticleMatrix( trim( nameOfSpecieSelected ) )
			
			if( externalPotentialManager_instance.isInstanced ) then
				call RHFWaveFunction_buildExternalPotentialMatrix( trim( nameOfSpecieSelected ) )
			end if

			!!**********************************************************
			!! Ajuste de matrices de densidad inicial
			!!
			if ( trim( nameOfSpecieSelected ) == "e-" ) then
				call RHFSolver_setInitialGuess(APMO_instance.SCF_ELECTRONIC_TYPE_GUESS,"e-")
			else
				call RHFSolver_setInitialGuess( APMO_instance.SCF_NONELECTRONIC_TYPE_GUESS, &
											trim( nameOfSpecieSelected ) )
			end if
			!!**********************************************************
			
			!! Construye matrix de dos Particulas
			call IntegralManager_writeIntraspecieRepulsionIntegral(speciesIterator, trim( nameOfSpecieSelected ) )
			call RHFWaveFunction_buildTwoParticlesMatrix( trim( nameOfSpecieSelected ) )
			
			!! Construye matrix de Fock (en la primera iteracion no se adiciona el termino de acoplamiento)
			call RHFWaveFunction_buildFockMatrix( trim( nameOfSpecieSelected ) )
			
			!!Inicia la iteracion entre especies
			if (APMO_instance%IS_THERE_FROZEN_PARTICLE) then
				if(ParticleManager_isFrozen(trim(nameOfSpecieSelected))) then
					call SCFIntraspecies_iterate( trim( nameOfSpecieSelected ), actualizeDensityMatrix=.false. )
				else
					call SCFIntraspecies_iterate( trim( nameOfSpecieSelected ) )
				end if
			else
				call SCFIntraspecies_iterate( trim( nameOfSpecieSelected ) )
			end if


				
			if ((.not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS)) then
			
				call SCFIntraspecies_restart( trim( nameOfSpecieSelected ) )
			else

			end if

		end do

		!!Resetea la variable APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS
		APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxVal
		
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
			print *,""
			print *,"RHF SCF CALCULATION: "
			print *,"===================="
			print *,""
		end if

		numberOfSpecies = ParticleManager_getNumberOfQuantumSpecies()

		if( numberOfSpecies > 1 ) then
			status = SCF_INTERSPECIES_CONVERGENCE_CONTINUE
			
			
			if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
				print *,"Begin Global SCF calculation:"
				print *,""
				print *,"---------------------------------------------------------"
				write (6,"(A10,A12,A25)") "Iteration", "Energy","Energy Change"!, "Density Change"
				print *,"---------------------------------------------------------"
			end if

			!! Calcula integrales de interaccion inter-especies
			do i=1, numberOfSpecies
				nameOfSpecieSelected = ParticleManager_getNameOfSpecie( i )
				do j=i+1,numberOfSpecies
					nameOfOtherSpecie = ParticleManager_getNameOfSpecie( j )
					call IntegralManager_writeInterspecieRepulsionIntegral( i, j,nameOfSpecieSelected, nameOfOtherSpecie  )
				end do
			end do

			auxValue = 0.0_8
			do while( status == SCF_INTERSPECIES_CONVERGENCE_CONTINUE .and. &
					SCFInterspecies_getNumberOfIterations() <= APMO_instance.SCF_INTERSPECIES_MAXIMUM_ITERATIONS )
				
				call SCFInterspecies_iterate( APMO_instance.ITERATION_SCHEME )
				deltaEnergy = auxValue-SCFInterspecies_getLastEnergy()


				if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
					write (6,"(I5,F20.10,F20.10)") SCFInterspecies_getNumberOfIterations(), &
						SCFInterspecies_getLastEnergy(),deltaEnergy
				end if

				status = SCFInterspecies_testEnergyChange(APMO_instance.TOTAL_ENERGY_TOLERANCE  )
				auxValue=SCFInterspecies_getLastEnergy()
                                
			end do
			
			
			
			
			if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS ) then
				print *,""
				print *,"...end Global SCF calculation"
				!call RHFSolver_showCyclesSCFForSpecies()
			end if

		else

			call SCFInterspecies_iterate( APMO_instance.ITERATION_SCHEME )

		end if
		
		
		if ( .not.APMO_instance.OPTIMIZE .or. APMO_instance.DEBUG_SCFS) then
			print *,""
			print *,"END OF RHF SCF CALCULATION"
		end if
		
		call MolecularSystem_obtainEnergyComponents()
		
		!!Resetea la variable APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS
		APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS = auxVal
			
		if ((.not.APMO_instance.ELECTRONIC_WAVEFUNCTION_ANALYSIS)) then
			!!************************************************************
			!!Escribe en disco los vectores propios de cada especie
			!!****
			do i=1, numberOfSpecies
				nameOfSpecieSelected = ParticleManager_getNameOfSpecie( i )
				
				if (ParticleManager_isFrozen(trim(nameOfSpecieSelected))) cycle
				
				call InputOutput_writeMatrix( MolecularSystem_instance.coefficientsOfCombinationPtr(i), &
				file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecieSelected)//".vec", &
				binary =.true. )
					
				call InputOutput_writeVector( MolecularSystem_instance.energyOfmolecularOrbitalPtr(i), &
					file=trim(APMO_instance.INPUT_FILE)//trim(nameOfSpecieSelected)//".val", binary=.true. )
				
				if( APMO_instance.IS_THERE_EXTERNAL_POTENTIAL ) then
				   !! Grafica los orbitales moleculares especificados
				   call RHFWaveFunction_draw2DOrbital( trim(nameOfSpecieSelected), &
				   	APMO_instance.GRAPH_FOR_NUM_ORBITAL,&
				   	ORBITAL_WITH_POTENTIAL )
				end if

				
! 				if( trim(nameOfSpecieSelected)=="H_1" ) then
! 				   call RHFWaveFunction_draw2DOrbital( trim(nameOfSpecieSelected),1,ORBITAL_ALONE )
! 				end if
				
			end do
			!!
			!!************************************************************
		end if
		
 		call RHFSolver_writeMatrices()

		
	end subroutine RHFSolver_run

	subroutine RHFSolver_writeMatrices()
		type(Matrix) :: A
		type(Matrix) :: P, Pnew
		type(Matrix) :: C, Cnew
		type(Matrix) :: S, Snew
		type(Matrix) :: K, Knew
		type(Matrix) :: Jcoup, JcoupNew
		type(Matrix) :: Icoup, IcoupNew
		type(Exception) :: ex

		character(20) :: fileName

		integer :: numberOfContractions
		integer :: totalNumberOfContractions
		integer :: specieID
		integer :: st(1), pt(3), dt(6), ft(10), gt(15), ht(21)
		integer :: angularMoment
		integer, allocatable :: trans(:)
		integer :: i, j
		integer :: u, v
		real(8) :: Epn, Ecn
		real(8) :: aVal, bVal

		fileName = APMO_instance%INPUT_FILE

		!!Id de la especie seleccionada (por defecto e-)
		specieID = ParticleManager_getSpecieID(nameOfSpecie = "e-")

		!!Numero total de contracciones
		numberOfContractions = ParticleManager_getNumberOfContractions(specieID)
		totalNumberOfContractions = ParticleManager_getTotalNumberOfContractions(specieID)

		!!Tamano del arreglo de nuevas etiquetas
		if(allocated(trans)) deallocate(trans)
		allocate(trans(totalNumberOfContractions))

		!! Reglas de transformacion de indices
		st(1)    = 1
		pt(1:3)  = [1, 2, 3]
		dt(1:6)  = [1, 4, 5, 2, 6, 3]
		ft(1:10) = [1, 4, 5, 6, 10, 8, 2, 7, 9, 3]
		gt(1:15) = [1, 4, 5, 10, 13, 11, 6, 14, 15, 8, 2, 7, 12, 9, 3]
                ht(1:21) = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
		write(*,*) "RULES FOR INDEX TRANSFORMATION"
		write(*,*) "================================"
		write(*,*) ""
		write(*,"(' s =',I3)") st(1)
		write(*,"(' p =',3I3)") ( pt(i), i=1,3 )
		write(*,"(' d =',6I3)") ( dt(i), i=1,6 )
		write(*,"(' f =',10I3)") ( ft(i), i=1,10 )
		write(*,"(' g =',15I3)") ( gt(i), i=1,15 )
                write(*,"(' h =',21I3)") ( ht(i), i=1,21 )
		write(*,*) ""

		trans = 0
		u = 1
		v = 0

		do i = 1, numberOfContractions
			angularMoment = ParticleManager_instance%particlesPtr(ParticleManager_instance%idsOfContractionsForSpecie(&
							 specieID)%contractionID(i)%particleID)%basis%contractions( &
							 ParticleManager_instance%idsOfContractionsForSpecie(specieID)%contractionID(&
							 i)%contractionIDInParticle)%angularMoment

			select case(angularMoment)
				case(0)
					trans(u) = st(1) + v
					u = u + 1

				case(1)
					do j = 1, 3
						trans(u) = pt(j) + v
						u = u + 1
					end do

				case(2)
					do j = 1, 6
						trans(u) = dt(j) + v
						u = u + 1
					end do

				case(3)
					do j = 1, 10
						trans(u) = ft(j) + v
						u = u + 1
					end do

				case(4)
					do j = 1, 15
						trans(u) = gt(j) + v
						u = u + 1
					end do
                                
                                case(5)
                                        do j = 1, 21
                                                trans(u) = ht(j) + v
                                                u = u + 1
                                        end do

				case default
					call Exception_constructor( ex , WARNING )
					call Exception_setDebugDescription( ex, "Class object RHFSolver in the writeMatrices function" )
					call Exception_setDescription( ex, "this angular moment is not implemented (l>4)" )
					call Exception_show( ex )

					return

			end select
			v = u - 1
		end do

		write(*,"(' trans =',<totalNumberOfContractions>I3)") ( trans(i), i=1,totalNumberOfContractions )

		Epn = MolecularSystem_instance.repulsionEnergyPtr(2)
		Ecn = MolecularSystem_instance.kineticEnergyPtr(2)

		write(*,*) "Epn = ", Epn
		write(*,*) "Ecn = ", Ecn

		P = RHFWaveFunction_getDensityMatrix( "e-" )
		C = RHFWaveFunction_getWaveFunctionCoefficients( "e-" )
		S = RHFWaveFunction_getOverlapMatrix( "e-" )
		K = RHFWaveFunction_getKineticMatrix( "e-" )
		Jcoup = RHFWaveFunction_getCouplingMatrix( "e-" )
		Icoup = RHFWaveFunction_getPuntualParticleMatrix( "e-" )

		call Matrix_constructor( Pnew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )
		call Matrix_constructor( Cnew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )
		call Matrix_constructor( Snew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )
		call Matrix_constructor( Knew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )
		call Matrix_constructor( JcoupNew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )
		call Matrix_constructor( IcoupNew, int(totalNumberOfContractions, 8), int(totalNumberOfContractions, 8) )

		do i=1,totalNumberOfContractions
			do j=1,totalNumberOfContractions
				Pnew.values( trans(i), trans(j) ) = P.values( i, j )
				Cnew.values( trans(i), trans(j) ) = C.values( i, j )
				Snew.values( trans(i), trans(j) ) = S.values( i, j )
				Knew.values( trans(i), trans(j) ) = K.values( i, j )
				JcoupNew.values( trans(i), trans(j) ) = Jcoup.values( i, j )
				IcoupNew.values( trans(i), trans(j) ) = Icoup.values( i, j )
			end do
		end do

! 		do i=1,totalNumberOfContractions
! 			aVal = Pnew.values( 3, i )
! 			bVal = Pnew.values( 6, i )
! 			Pnew.values( 6, i ) = aVal
! 			Pnew.values( 3, i ) = bVal
!
! 			aVal = Cnew.values( 3, i )
! 			bVal = Cnew.values( 6, i )
! 			Cnew.values( 6, i ) = aVal
! 			Cnew.values( 3, i ) = bVal
!
! 			aVal = Snew.values( 3, i )
! 			bVal = Snew.values( 6, i )
! 			Snew.values( 6, i ) = aVal
! 			Snew.values( 3, i ) = bVal
!
! 			aVal = JcoupNew.values( 3, i )
! 			bVal = JcoupNew.values( 6, i )
! 			JcoupNew.values( 6, i ) = aVal
! 			JcoupNew.values( 3, i ) = bVal
!
! 			aVal = IcoupNew.values( 3, i )
! 			bVal = IcoupNew.values( 6, i )
! 			IcoupNew.values( 6, i ) = aVal
! 			IcoupNew.values( 3, i ) = bVal
! 		end do

		write(*,"(3A)", advance="no") " Saving density matrix ( ", trim(trim(fileName)//"dens"), " ) ... "
		open( 20, file=trim(trim(fileName)//"dens"), action='write', form='unformatted' )
		write(20) int(size(Pnew.values), 8)
		write(20) Pnew.values
		close(20)
		write(*,*) "OK"

		write(*,"(3A)", advance="no") " Saving coefficients matrix ( ", trim(trim(fileName)//"coeff"), " ) ... "
		open( 20, file=trim(trim(fileName)//"coeff"), action='write', form='unformatted' )
		write(20) int(size(Cnew.values), 8)
		write(20) Cnew.values
		close(20)
		write(*,*) "OK"

!		write(*,*) "Jcoup ="
!		call Matrix_show( JcoupNew )

!		write(*,*) "Icoup ="
!		call Matrix_show( IcoupNew )

		write(*,"(3A)", advance="no") " Saving overlap matrix ( ", trim(trim(fileName)//"over"), " ) ... "
		open( 20, file=trim(trim(fileName)//"over"), action='write', form='unformatted' )
		write(20) int(size(Snew.values), 8)
		write(20) Snew.values
		close(20)
		write(*,*) "OK"
		
		write(*,"(3A)", advance="no") " Saving kinetic matrix ( ", trim(trim(fileName)//"kin"), " ) ... "
		open( 20, file=trim(trim(fileName)//"kin"), action='write', form='unformatted' )
		write(20) int(size(Snew.values), 8)
		write(20) Knew.values
		close(20)
		write(*,*) "OK"

		write(*,"(3A)", advance="no") " Saving coupling matrix ( ", trim(trim(fileName)//"coup"), " ) ... "
		open( 20, file=trim(trim(fileName)//"jcoup"), action='write', form='unformatted' )
		write(20) int(size(JcoupNew.values), 8)
		write(20) JcoupNew.values
		close(20)
		write(*,*) "OK"

		write(*,"(3A)", advance="no") " Saving fixed interaction matrix ( ", trim(trim(fileName)//"over"), " ) ... "
		open( 20, file=trim(trim(fileName)//"icoup"), action='write', form='unformatted' )
		write(20) int(size(IcoupNew.values), 8)
		write(20) IcoupNew.values
		close(20)
		write(*,*) "OK"

	end subroutine RHFSolver_writeMatrices
	


	subroutine RHFSolver_showCyclesSCFForSpecies()
		implicit none
		
		character(30) :: nameOfSpecieSelected
		character :: typeConvergence
		integer :: numberOfIterations
		integer :: speciesIterator
		integer :: specieID
		integer :: i
		real(8) :: diisError

		call ParticleManager_rewindSpecies()
		do speciesIterator =1, ParticleManager_getNumberOfQuantumSpecies()
			
			nameOfSpecieSelected =  trim( ParticleManager_iterateSpecie() )
			specieID =ParticleManager_getSpecieID( nameOfSpecie = trim( nameOfSpecieSelected ) )
			numberOfIterations = List_size( SCFIntraspecies_instance( specieID ).energySCF )
			call List_begin( SCFIntraspecies_instance( specieID ).energySCF )
			call List_begin( SCFIntraspecies_instance( specieID ).diisError )
			call List_begin( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
			
			print *,""
			print *,"Begin SCF calculation by: ",trim(nameOfSpecieSelected)
			print *,"-------------------------"
			print *,""
			print *,"-----------------------------------------------------------------"
			write (6,"(A10,A12,A25,A20)") "Iteration", "Energy", " Density Change","         DIIS Error "
			print *,"-----------------------------------------------------------------"		
			
			do i=1, numberOfIterations-1

				call List_iterate( SCFIntraspecies_instance( specieID ).energySCF )
				call List_iterate( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements )
				call List_iterate( SCFIntraspecies_instance( specieID ).diisError )
				diisError =List_current( SCFIntraspecies_instance( specieID ).diisError )

				typeConvergence = " "
				if ( diisError > APMO_instance.DIIS_SWITCH_THRESHOLD ) typeConvergence = "*"
				if (abs(diisError) < APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
					write (6,"(I5,F20.10,F20.10,A20,A1)") i,  List_current( SCFIntraspecies_instance( specieID ).energySCF ),&
						List_current( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements ), &
						"         --         ",typeConvergence
				else
					write (6,"(I5,F20.10,F20.10,F20.10,A1)") i,  List_current( SCFIntraspecies_instance( specieID ).energySCF ),&
						List_current( SCFIntraspecies_instance( specieID ).standartDesviationOfDensityMatrixElements ), &
						diisError,typeConvergence
				end if
			end do
			print *,""
			print *,"... end SCF calculation"
			
		end do

	end subroutine RHFSolver_showCyclesSCFForSpecies
	

	subroutine RHFSolver_reset()
		implicit none
		

 		call IntegralManager_reset()
		call RHFWaveFunction_reset()
		call SCFInterspecies_reset()
	

	end subroutine RHFSolver_reset
	
	
end module RHFSolver_
