!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�ez M�ico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief  Modulo para definicion y ajuste de constantes globales
!
! Este modulo contiene la definicion de constantes globales utilizadas dentro de
! modulos y sus procedimiento asociados, al igual que algunos meodos que ajustan
! sus valores de forma adecuada en tiempo de ejecucion.
!
! @author Sergio A. Gonz\'alez M\'onico
!
! <b> Fecha de creaciacion : </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonz\'alez M. ( sagonzalezm@unal.edu.co )
!        -# Creaci\'on de m\'odulo y procedimientos b\'asicos
!**
module Units_
		
	!****************************************************************************
	!! Definici\'on de factores de conversi\'on
	!!
	real(8) , parameter :: HARTREE = 1.0_8
	real(8) , parameter :: AMSTRONG = 0.52917724924_8
	real(8) , parameter :: DEBYES = 2.541764_8
	real(8) , parameter :: ELECTRON_REST = 1.0_8
	real(8) , parameter :: AMU = 1.0_8/1822.88850065855_8
	real(8) , parameter :: kg = 9.109382616D-31
	real(8) , parameter :: DEGREES = 57.29577951_8
	real(8) , parameter :: CM_NEG1 = 219476.0_8
	
	!!***************************************************************************

	
end module Units_
