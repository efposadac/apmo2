!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module RadialHamiltonian_
        use ExternalPotential_
	use Exception_
	use APMO_
        use ContractedGaussian_
	use Matrix_
	use BasisSet_
	use XMLParser_
	implicit none


	!>
	!! @brief Maneja potenciales externos
	!!
	!! @author Sergio A. Gonzalez
	!!
	!! <b> Creation data : </b> 06-08-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 06-08-10 </tt>:  Sergio A. Gonzalez ( sergmonic@gmail.com )
	!!        -# Creacioon del modulo y metodos basicos
	!!
	!<
	type, public :: RadialHamiltonian
		character(20) :: name
		type(BasisSet), pointer  :: gaussianBasisSetPointer
		type(ExternalPotential) :: potential
		type(Matrix) :: kineticMtx
		type(Matrix) :: repulsionMtx
		type(Matrix) :: interactionMtx
		type(Matrix) :: Hmtx
		logical :: isInstanced
	end type

	public :: &
		RadialHamiltonian_constructor, &
		RadialHamiltonian_destructor, &
		RadialHamiltonian_show, &
		RadialHamiltonian_calculateInteractionMtx, &
		RadialHamiltonian_calculateKineticMtx, &
		RadialHamiltonian_calculateHMtx
		
private		
contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine RadialHamiltonian_constructor(this, potentialName, interactionName, &
		bbasisSet )
		implicit none
		type(RadialHamiltonian) :: this
		character(*) :: potentialName
		character(*) :: interactionName
		type(BasisSet), target :: bbasisSet
		
		call RadialHamiltonian_loadPotential(this,&
			trim(potentialName),trim(interactionName))

		if( BasisSet_isInstanced(bbasisSet) ) then
			this.gaussianBasisSetPointer=>bbasisSet
		else

			call RadialHamiltonian_exception(ERROR,"",&
				"Class object RadialHamiltonian in the constructor function")
			
		end if

		this.isInstanced=.true.

	end subroutine RadialHamiltonian_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine RadialHamiltonian_destructor(this)
		implicit none
		type(RadialHamiltonian) :: this
	end subroutine RadialHamiltonian_destructor

	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	subroutine RadialHamiltonian_show(this)
		implicit none
		type(RadialHamiltonian) :: this
	end subroutine RadialHamiltonian_show


	!>
	!! @brief Carga un potencial gausiano de un archivo de entrada
	!!
	!! @param this Instancia de la clase ExternalPotential
	!<
	subroutine RadialHamiltonian_loadPotential(this, potentialName, interactionName)
		implicit none
		type(RadialHamiltonian) :: this
		character(*) :: potentialName
		character(*) :: interactionName

		call ExternalPotential_constructor( this.potential )
		
		call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // &
			trim(APMO_instance.POTENTIALS_DATABASE)//trim(potentialName) // ".xml"), &
			trim(interactionName), eexternalPotential=this.potential )

	end subroutine RadialHamiltonian_loadPotential

	!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function RadialHamiltonian_isInstanced( this ) result( output )
		implicit  none
		type(RadialHamiltonian), intent(in) :: this
		logical :: output
		
		output = this.isInstanced
	
	end function RadialHamiltonian_isInstanced


	!>
	!! @brief  
	!<
	subroutine RadialHamiltonian_calculateInteractionMtx(this)
		implicit none
		type(RadialHamiltonian), intent(inout) :: this

		this.interactionMtx=ExternalPotential_getInteractionMtx(this.potential, &
			this.gaussianBasisSetPointer.contractions)
		call Matrix_show(this.interactionMtx)

	end subroutine RadialHamiltonian_calculateInteractionMtx


	!>
	!! @brief  
	!<
	subroutine RadialHamiltonian_calculateKineticMtx(this)
		implicit none
		type(RadialHamiltonian), intent(inout) :: this

		integer :: i
		integer :: j
		integer :: numContrations
		real(8) :: auxVal
		type(ContractedGaussian) :: auxContract


		numContrations= size(this.gaussianBasisSetPointer.contractions)

		call Matrix_constructor(this.kineticMtx,int(numContrations,8),int(numContrations,8))

		do i=1, numContrations

			do j=1, numContrations
				this.kineticMtx.values(i,j)= ContractedGaussian_kineticIntegral( &
					this.gaussianBasisSetPointer.contractions(i), &
					this.gaussianBasisSetPointer.contractions(j) )
			end do

		end do

		this.kineticMtx.values=this.kineticMtx.values/1836.0

		call Matrix_show(this.kineticMtx)

	end subroutine RadialHamiltonian_calculateKineticMtx


	!>
	!! @brief  
	!<
	subroutine RadialHamiltonian_calculateHMtx(this)
		implicit none
		type(RadialHamiltonian), intent(inout) :: this

		integer :: i
		integer :: j
		integer :: numContrations
		real(8) :: auxVal
		type(ContractedGaussian) :: auxContract


		numContrations= size(this.gaussianBasisSetPointer.contractions)

		call Matrix_constructor(this.HMtx,int(numContrations,8),int(numContrations,8))

		this.HMtx.values=this.kineticMtx.values+this.interactionMtx.values

		call Matrix_show(this.HMtx)

	end subroutine RadialHamiltonian_calculateHMtx



	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine RadialHamiltonian_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription
	
		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )
	
	end subroutine RadialHamiltonian_exception

end module RadialHamiltonian_
