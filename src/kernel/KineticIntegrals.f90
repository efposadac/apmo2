!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, kinetic integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

module KineticIntegrals_
	use PrimitiveGaussian_
	use Exception_

	implicit none 	

	!>
	!! @brief Modulo para calculo de integrales de energia cinetica. 
	!!
	!! Este modulo define una seudoclase cuyos metodos devuelven la integral de
	!! energia cinetica  para una particula descrita mediante una distribucion
	!! gausiana de sin normalizar. La integral es de la forma:
	!!
	!! \f[(\bf{a} \parallel T \parallel \bf{b}) = \int_{TE} {{\varphi(\bf{r};
	!!  {\zeta}_a, \bf{n_a,R_A})}{(-\frac{1}{2}) {\nabla}^2} {\varphi(\bf{r};
	!!  {\zeta}_b , \bf{n_b,R_B})}}\,dr\f]
	!!
	!! Este tipo de integral corresponde a una integral de dos centros calalada de
	!! forma cerrada.
	!!
	!! Donde:
	!!
	!! <table>
	!! <tr> <td> \f$ \zeta \f$ : <td> <dfn> exponente orbital. </dfn>
	!! <tr> <td> \f$ r \f$ : <td>  <dfn> coordenas espaciales de la funcion. </dfn>
	!! <tr> <td> \f$ n_a , n_b \f$ : <td> <dfn> indice de momento angular.</dfn>
	!! <tr> <td> \f$ R_A , R_B \f$ : <td> <dfn>origen de las gausianas </dfn>
	!! </table>
	!!
	!! La integral de energia cinetica para la particula dada se calcula de acuerdo
	!! metodo recursivo propuesto por Obara-Sayka, cuya expresion general para la
	!! integral es:
	!!
	!! \f[({\bf{a + 1_i}} \parallel T \parallel {\bf{b}}) = \f] 
	!! \f[ (P_i -A_i) ({\bf{a}} \parallel T \parallel {\bf{b}} ) + \frac{1}
	!! {2 \zeta} N_i(\bf{a}) ({\bf{a-1_i}} \parallel T \parallel {\bf{b}} )
	!! + \frac{1}{2 \zeta} N_i(\bf{b}) + ({\bf{a}} \parallel T \parallel 
	!! {\bf{b-1_i}}) + 2  \xi \left[({\bf{a - 1_i}} \parallel {\bf{b}}) 
	!! \frac{1}{2 {\zeta}_a} N_i(\bf{a}) ({\bf{a - 1_i}} \parallel {\bf{b}} )
	!!  \right]\f]
	!!
	!! Los parametros <b> P </b> , \f$ \xi \f$ y \f$ \zeta\f$ de la expresion provienen del
	!! producto de dos funciones gaussianas.
	!!
	!! @author Edwin Posada
	!!
	!! <b> Fecha de creacion : </b> 2010-03-08
	!!
	!! <b> Historial de modificaciones: </b>
	!!
	!! \see PrimitiveGaussian_ , gaussianProduct()
	!!
	!<
	type , private :: KineticIntegrals
		character(30) :: name
	end type

	public :: &
		PrimitiveGaussian_kineticIntegral

	private :: &
		KineticIntegrals_oSRecursion
		
contains
	
	!<
	!@brief Evalua integrales overlap para cualquier momento angular
	!@author Edwin Posada, 2010
	!@return devuelve los valores de integrales de atracción (output)
	!>
	subroutine PrimitiveGaussian_kineticIntegral(ami, amj, nprim1, nprim2, A, B, exp1, exp2, coef1, coef2, norm1, norm2, buffer)
	  	implicit none

	  	integer, intent(in) :: ami(0:3), amj(0:3)
	  	integer, intent(in) :: nprim1, nprim2
	  	real(8), intent(in) :: A(0:3), B(0:3)
	  	real(8), intent(in) :: exp1(0:nprim1), exp2(0:nprim2)
	  	real(8), intent(in) :: coef1(0:nprim1), coef2(0:nprim2)
	  	real(8), intent(in) :: norm1(0:nprim1), norm2(0:nprim2)
	  	real(8), intent(out) :: buffer

	  	real(8), allocatable ::  x(:,:), y(:,:), z(:,:)
	  	real(8) :: AB2
	  	real(8) :: a1, c1, d1
	  	real(8) :: a2, c2, d2
	  	real(8) :: gam, oog
	  	real(8) :: PA(0:3), PB(0:3), P(0:3)
	  	real(8) :: over_pf
	  	real(8) :: x0, y0, z0
	  	real(8) :: I1, I2, I3, I4
	  	real(8) :: Ix, Iy, Iz

	  	integer :: am1, am2
	  	integer :: mmax
	  	integer :: p1, p2
	  	integer :: ao12
	  	integer :: ii, jj, kk, ll
	  	integer :: l1, m1, n1
	  	integer :: l2, m2, n2

		buffer = 0.0_8

	  	am1 = sum(ami)
	  	am2 = sum(amj)

	  	mmax = max(am1, am2) + 1

	  	allocate(x(0:mmax+2, 0:mmax+2), y(0:mmax+2, 0:mmax+2), z(0:mmax+2, 0:mmax+2))

!      	calcular intermediarios

	  	AB2 = 0.0_8
	  	AB2 = AB2 + (A(0) - B(0)) * (A(0) - B(0))
	  	AB2 = AB2 + (A(1) - B(1)) * (A(1) - B(1))
	  	AB2 = AB2 + (A(2) - B(2)) * (A(2) - B(2))

	  	do p1=0, nprim1 - 1
	      	a1 = exp1(p1)
	      	c1 = coef1(p1)
	      	d1 = norm1(p1)
	      	do p2=0, nprim2 - 1
		  		a2 = exp2(p2)
		  		c2 = coef2(p2)
		  		d2 = norm2(p2)
		  		gam = a1 + a2
		  		oog = 1.0/gam

		  		P(0) = (a1*A(0) + a2*B(0))*oog
		  		P(1) = (a1*A(1) + a2*B(1))*oog
		  		P(2) = (a1*A(2) + a2*B(2))*oog
		  		PA(0) = P(0) - A(0)
		  		PA(1) = P(1) - A(1)
		  		PA(2) = P(2) - A(2)
		  		PB(0) = P(0) - B(0)
		  		PB(1) = P(1) - B(1)
		  		PB(2) = P(2) - B(2)

		  		over_pf = exp(-a1*a2*AB2*oog) * sqrt(Math_PI*oog) * Math_PI * oog * c1 * c2 * d1 * d2

!			  	recursion
			  	call KineticIntegrals_oSRecursion(x, y, z, PA, PB, gam, am1+2, am2+2)

		  		if (ami(0) == 0 .or. amj(0) == 0) then
		    		I1 = 0.0_8
		  		else
		    		I1 = x(ami(0)-1,amj(0)-1) * y(ami(1),amj(1)) * z(ami(2),amj(2)) * over_pf
		  		end if

		  		I2 = x(ami(0)+1,amj(0)+1) * y(ami(1),amj(1)) * z(ami(2),amj(2)) * over_pf

		  		if (amj(0) == 0) then
		    		I3 = 0.0_8
		  		else
		  	  		I3 = x(ami(0)+1,amj(0)-1) * y(ami(1),amj(1)) * z(ami(2),amj(2)) * over_pf
				end if

		  		if (ami(0) == 0) then
		    		I4 = 0.0_8
		  		else
		    		I4 = x(ami(0)-1,amj(0)+1) * y(ami(1),amj(1)) * z(ami(2),amj(2)) * over_pf
		  		end if

		  		Ix = 0.5 * ami(0) * amj(0) * I1 + 2.0 * a1 * a2 * I2 - a1 * amj(0) * I3 - ami(0) * a2 * I4

		  		if (ami(1) == 0 .or. amj(1) == 0) then
		    		I1 = 0.0_8
		  		else
		    		I1 = x(ami(0),amj(0)) * y(ami(1)-1,amj(1)-1) * z(ami(2),amj(2)) * over_pf
		  		end if

		  		I2 = x(ami(0),amj(0)) * y(ami(1)+1,amj(1)+1) * z(ami(2),amj(2)) * over_pf

		  		if (amj(1) == 0) then
		    		I3 = 0.0_8
		  		else
		    		I3 = x(ami(0),amj(0)) * y(ami(1)+1,amj(1)-1) * z(ami(2),amj(2)) * over_pf
		  		end if

		  		if (ami(1) == 0) then
		    		I4 = 0.0_8
		  		else
		    		I4 = x(ami(0),amj(0)) * y(ami(1)-1,amj(1)+1) * z(ami(2),amj(2)) * over_pf
		  		end if

		  		Iy = 0.5 * ami(1) * amj(1) * I1 + 2.0 * a1 * a2 * I2 - a1 * amj(1) * I3 - ami(1) * a2 * I4

		  		if (ami(2) == 0 .or. amj(2) == 0) then
		    		I1 = 0.0_8
		  		else
		    		I1 = x(ami(0),amj(0)) * y(ami(1),amj(1)) * z(ami(2)-1,amj(2)-1) * over_pf
		  		end if

		  		I2 = x(ami(0),amj(0)) * y(ami(1),amj(1)) * z(ami(2)+1,amj(2)+1) * over_pf

		  		if (amj(2) == 0) then
		    		I3 = 0.0_8
		  		else
		    		I3 = x(ami(0),amj(0)) * y(ami(1),amj(1)) * z(ami(2)+1,amj(2)-1) * over_pf
		  		end if

		  		if (ami(2) == 0) then
		    		I4 = 0.0_8
		  		else
		    		I4 = x(ami(0),amj(0)) * y(ami(1),amj(1)) * z(ami(2)-1,amj(2)+1) * over_pf
		  		end if

		  		Iz = 0.5 * ami(2) * amj(2) * I1 + 2.0 * a1 * a2 * I2 - a1 * amj(2) * I3 - ami(2) * a2 * I4

		  		buffer = buffer + (Ix + Iy + Iz)

      		end do
  		end do

	  	deallocate(x)
	  	deallocate(y)
	  	deallocate(z)

	end subroutine PrimitiveGaussian_kineticIntegral

	subroutine KineticIntegrals_oSRecursion(orbitalIntegralsX, orbitalIntegralsY, orbitalIntegralsZ,&
												 PA, PB, zeta, angularMomentIndexA, angularMomentIndexB)
		implicit none

	  	real(8), intent(inout), allocatable :: orbitalIntegralsX(:,:), orbitalIntegralsY(:,:), orbitalIntegralsZ(:,:)
	  	real(8), intent(in) :: PA(0:3), PB(0:3)
	  	real(8), intent(in) :: zeta
	  	integer, intent(in) :: angularMomentIndexA, angularMomentIndexB

	  	real(8) :: twoZetaInv
	  	integer :: i, j, k

	  twoZetaInv = 1_8/(2_8*zeta)

	  	orbitalIntegralsX(0,0) = 1.0_8
	  	orbitalIntegralsY(0,0) = 1.0_8
	  	orbitalIntegralsZ(0,0) = 1.0_8

      	!! Upward recursion in j for i=0

	  	orbitalIntegralsX(0,1) = PB(0)
	  	orbitalIntegralsY(0,1) = PB(1)
	  	orbitalIntegralsZ(0,1) = PB(2)

	  	do j=1, angularMomentIndexB -1
	    	orbitalIntegralsX(0,j+1) = PB(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(0,j+1) = PB(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(0,j+1) = PB(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(0,j+1) = orbitalIntegralsX(0,j+1) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(0,j+1) = orbitalIntegralsY(0,j+1) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(0,j+1) = orbitalIntegralsZ(0,j+1) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

      	!! Upward recursion in i for all j's

	  	orbitalIntegralsX(1,0) = PA(0)
	  	orbitalIntegralsY(1,0) = PA(1)
	  	orbitalIntegralsZ(1,0) = PA(2)

	  	do j=1, angularMomentIndexB
	    	orbitalIntegralsX(1,j) = PA(0)*orbitalIntegralsX(0,j)
	    	orbitalIntegralsY(1,j) = PA(1)*orbitalIntegralsY(0,j)
	    	orbitalIntegralsZ(1,j) = PA(2)*orbitalIntegralsZ(0,j)
	    	orbitalIntegralsX(1,j) = orbitalIntegralsX(1,j) + j*twoZetaInv*orbitalIntegralsX(0,j-1)
	    	orbitalIntegralsY(1,j) = orbitalIntegralsY(1,j) + j*twoZetaInv*orbitalIntegralsY(0,j-1)
	    	orbitalIntegralsZ(1,j) = orbitalIntegralsZ(1,j) + j*twoZetaInv*orbitalIntegralsZ(0,j-1)
	  	end do

	  	do i=1, angularMomentIndexA - 1
	    	orbitalIntegralsX(i+1,0) = PA(0)*orbitalIntegralsX(i,0)
	    	orbitalIntegralsY(i+1,0) = PA(1)*orbitalIntegralsY(i,0)
	    	orbitalIntegralsZ(i+1,0) = PA(2)*orbitalIntegralsZ(i,0)
	    	orbitalIntegralsX(i+1,0) = orbitalIntegralsX(i+1,0) + i*twoZetaInv*orbitalIntegralsX(i-1,0)
	    	orbitalIntegralsY(i+1,0) = orbitalIntegralsY(i+1,0) + i*twoZetaInv*orbitalIntegralsY(i-1,0)
	    	orbitalIntegralsZ(i+1,0) = orbitalIntegralsZ(i+1,0) + i*twoZetaInv*orbitalIntegralsZ(i-1,0)
	    	do j=1, angularMomentIndexB
	      		orbitalIntegralsX(i+1,j) = PA(0)*orbitalIntegralsX(i,j)
	      		orbitalIntegralsY(i+1,j) = PA(1)*orbitalIntegralsY(i,j)
	      		orbitalIntegralsZ(i+1,j) = PA(2)*orbitalIntegralsZ(i,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + i*twoZetaInv*orbitalIntegralsX(i-1,j)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + i*twoZetaInv*orbitalIntegralsY(i-1,j)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + i*twoZetaInv*orbitalIntegralsZ(i-1,j)
	      		orbitalIntegralsX(i+1,j) = orbitalIntegralsX(i+1,j) + j*twoZetaInv*orbitalIntegralsX(i,j-1)
	      		orbitalIntegralsY(i+1,j) = orbitalIntegralsY(i+1,j) + j*twoZetaInv*orbitalIntegralsY(i,j-1)
	      		orbitalIntegralsZ(i+1,j) = orbitalIntegralsZ(i+1,j) + j*twoZetaInv*orbitalIntegralsZ(i,j-1)
	    	end do
	  	end do

	end subroutine KineticIntegrals_oSRecursion


end module KineticIntegrals_
	

