!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para manejo de estructuras de elementos atomicos
! 
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see ElementalParticle_ , XMLParser_
!
!**
module ElementalParticleManager_
	use ElementalParticle_
	use XMLParser_
	use APMO_
	
	implicit none
	
	type, public :: ElementalParticleManager
		character :: name	
		logical :: instanced
	end type ElementalParticleManager
	
	public  &
		ElementalParticleManager_constructor, &
		ElementalParticleManager_destructor, &
		ElementalParticleManager_loadParticle
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine ElementalParticleManager_constructor( this )
		implicit none
		type(ElementalParticle), intent(inout) :: this
		
	end subroutine ElementalParticleManager_constructor
	
	!**
	! Define el destructor para clase
	!
	!**
	subroutine ElementalParticleManager_destructor( this )
		implicit none
		type(ElementalParticle), intent(inout) :: this
	
	end subroutine ElementalParticleManager_destructor
	
	!**
	! Carga los atributos deun elemento atomico del respectivo archivo en formato XML
	!
	!**
	subroutine ElementalParticleManager_loadParticle ( this, particleName )
		implicit none
		type(ElementalParticle), intent(inout) :: this
		character(*) , intent(in) :: particleName
		
		call ElementalParticle_constructor(this)
		
		call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // &
			trim(APMO_instance.ELEMENTAL_PARTICLES_DATABASE) ), &
			trim(particleName), pparticle=this )
		
		call ElementalParticle_destructor()
			
	end subroutine ElementalParticleManager_loadParticle
	
	
	
end module ElementalParticleManager_