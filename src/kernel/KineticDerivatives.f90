!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, kinetic integrals, recursive integrals,       !
!!              gaussian functions, analyticderivatives .                          !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para calculo de derivadas de integrales de energia cinetica 
! 
! Este modulo contiene los algoritmos necesarios para la evaluacion de derivadas
! de integrales de energia cinetica entre pares de funciones gaussianas primitivas 
! (PrimitiveGaussian_), sin normalizar.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-12-03
!
! @see gaussianProduct, PrimitiveGaussian_, KineticIntegrals_ 
!
!**
module KineticDerivatives_
	use PrimitiveGaussian_
	use Exception_

	private ::
		real(8) :: commonFactor
		real(8) :: reducedOrbExponent
		real(8) :: originFactor 
		integer :: component
		integer :: center
		integer :: indxCenterA
		integer :: indxCenterB
		type(PrimitiveGaussian) :: primitives(2)
		type(PrimitiveGaussian) :: primitiveAxB

	public :: &
		PrimitiveGaussian_kineticDerivative
		
	private :: &
		KineticDerivatives_ss, &
		KineticDerivatives_ps, &
		KineticDerivatives_pp, &
		KineticDerivatives_ds, &
		KineticDerivatives_dp, &
		KineticDerivatives_dd
		
contains

	!**
	! Retorna el valor de la derivada de integrales de energia cinetica entre un par de 
	! funciones gausianas sin normalizar, pasadas como parametro.
	! Esta funcion se encarga de utilizar el procedimineto adecuado
	! de acuerdo al momento angular de las funciones de entrada
	!
	! @param primitiveA Gausiana primitiva 
	! @param primitiveB Gausiana primitiva 
	!
	! @return Valor de la integral de overlap
	!**
	function PrimitiveGaussian_kineticDerivative( primitiveA , primitiveB , center_, component_) &
		result( output )
		implicit none
		type(PrimitiveGaussian), intent(in) :: primitiveA
		type(PrimitiveGaussian), intent(in) :: primitiveB
		integer, intent(in) :: center_
		integer, intent(in) :: component_
		real(8) :: output
		
		integer :: caseIntegral
		type(Exception) :: ex
		
		!!*******************************************************************
		!! Seleccion el m�todo adecuado para el calculo de la integral
		!! de acuedo con el momento angular de las funciones de entrada.
		!!
		
		!! Ordena la gausianas de entrada de mayor a menor momento
		call  PrimitiveGaussian_sort( primitives,primitiveA, primitiveB )
		
		center = center_
		component = component_
		indxCenterA=primitives(1).owner
		indxCenterB=primitives(2).owner
		
		!! Obtiene el producto de la dos gausianas de entrada
		primitiveAxB = PrimitiveGaussian_product(primitiveA, primitiveB)
		reducedOrbExponent = PrimitiveGaussian_reducedOrbitalExponent( primitives(1) , primitives(2) )
		commonFactor = ( Math_PI/primitiveAxB.orbitalExponent )**1.5_8 * &
			PrimitiveGaussian_productConstant( primitives(1) , primitives(2) ) 
			
			
		originFactor= ( (primitiveA.origin(1) & 
				- primitiveB.origin(1) ) ** 2.0_8 &
				+ ( primitiveA.origin(2) &
				- primitiveB.origin(2) ) ** 2.0_8 &
				+ (primitiveA.origin(3) &
				- primitiveB.origin(3) ) ** 2.0_8 ) * reducedOrbExponent
		
		!! Determina la integral que se debe usar
		caseIntegral = 4*PrimitiveGaussian_getAngularMoment(primitives(1)) + PrimitiveGaussian_getAngularMoment(primitives(2))
		 
		select case (caseIntegral)
		
		case(0) 
			output = KineticIntegrals_ss()
		
		case(4) 
			output = KineticIntegrals_ps()
				
		case(5) 
			output = KineticIntegrals_pp()
		
		case(8)
			output = KineticIntegrals_ds()
			
		case(9) 
			output = KineticIntegrals_dp()
			
		case(10) 
			output = KineticIntegrals_dd()
		
		case default
			output = 0.0_8
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the kinetic derivative function" )
			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
			call Exception_show( ex )
		
		end select
		!!*******************************************************************
		
	end function PrimitiveGaussian_kineticDerivative


	!**
	! Retorna el valor de la derivada integral <i> d(s|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_ss() result( output )
		implicit none
		real(8) :: output
		
		!! Calcula la integral de overlap
		output= (2.0_8*reducedOrbExponent**2.0_8*(-5.0_8 &
			 + 2.0_8*originFactor)*(Math_kroneckerDelta(indxCenterA, center) &
			 - Math_kroneckerDelta(indxCenterB, center)) &
			 * (primitives(1).origin(component) &
			 - primitives(2).origin(component))) * commonFactor
				
	end function KineticIntegrals_ss

	!**
	! Retorna el valor de la derivada integral <i> d(p|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_ps() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output= -((reducedOrbExponent &
			* (Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) &
			* ( 2.0_8 * primitiveAxB.orbitalExponent*reducedOrbExponent &
			* (-7.0_8 + 2.0_8 * originFactor) * (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) &
			- (-5.0_8 + 2.0_8*originFactor ) * Math_kroneckerDelta(alpha,component) &
			* primitives(2).orbitalExponent))* commonFactor/(primitiveAxB.orbitalExponent))
				
	end function KineticIntegrals_ps
	
	!**
	! Retorna el valor de la derivada integral <i> d(p|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_pp() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		beta  = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output= (reducedOrbExponent*(Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) * (reducedOrbExponent &
			* (-7.0_8 + 2.0_8*originFactor)*Math_kroneckerDelta(alpha, beta) &
			* ( primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			+ primitiveAxB.origin(alpha) &
			* (-2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent &
			*(-9.0_8 + 2.0_8*originFactor)*(primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(2).origin(beta) - primitiveAxB.origin(beta)) &
			- (-7.0_8 + 2.0_8*originFactor) * Math_kroneckerDelta(beta, component) &
			* primitives(1).orbitalExponent) + primitives(1).origin(alpha) &
			* (2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-9.0_8 &
			+ 2.0_8*originFactor)*(primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(2).origin(beta) - primitiveAxB.origin(beta)) &
			+ (-7.0_8 + 2.0_8 * originFactor) * Math_kroneckerDelta(beta, component) &
			* primitives(1).orbitalExponent) - (-7.0_8 + 2.0_8 * originFactor ) &
			* Math_kroneckerDelta(alpha, component) * (primitives(2).origin(beta) &
			- primitiveAxB.origin(beta)) * primitives(2).orbitalExponent)) &
			* commonFactor / primitiveAxB.orbitalExponent
				
	end function KineticIntegrals_pp
	
	!**
	! Retorna el valor de la derivada integral <i> d(d|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_ds() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		
		!! Calcula la integral de overlap
		output = (reducedOrbExponent*(Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) &
			* (reducedOrbExponent*Math_kroneckerDelta(alpha, beta) &
			*(primitives(1).origin(component)  &
			- primitives(2).origin(component)) &
			* ( 2.0_8*primitiveAxB.orbitalExponent + (-7.0_8 &
			+ 2.0_8 * originFactor)*primitives(1).orbitalExponent) &
			+ primitives(1).orbitalExponent * (2.0_8 * primitiveAxB.orbitalExponent &
			* reducedOrbExponent*(-9.0_8 + 2.0_8 * originFactor) &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(1).origin(alpha) - primitiveAxB.origin(alpha))&
			* (primitives(1).origin(beta) - primitiveAxB.origin(beta)) &
			+ (-7.0_8 + 2.0_8*originFactor)*(Math_kroneckerDelta(beta, component) &
			* (-primitives(1).origin(alpha)+primitiveAxB.origin(alpha)) &
			+ Math_kroneckerDelta(alpha, component) * (-primitives(1).origin(beta) &
			+ primitiveAxB.origin(beta)))*primitives(2).orbitalExponent))) &
			* commonFactor /(primitiveAxB.orbitalExponent*primitives(1).orbitalExponent)
				
	end function KineticIntegrals_ds


	!**
	! Retorna el valor de la derivada integral <i> d(d|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_dp() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output= (-0.5_8*reducedOrbExponent*(Math_kroneckerDelta(indxCenterA, center) &
		- Math_kroneckerDelta(indxCenterB, center))*(Math_kroneckerDelta(alpha, beta) &
		* ( -4.0_8 * primitiveAxB.orbitalExponent ** 2.0_8 &
		* reducedOrbExponent*primitives(2).origin(component) &
		* (primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) - & 
     4.0_8*primitiveAxB.orbitalExponent*(-0.5_8*Math_kroneckerDelta(kappa, component) + reducedOrbExponent*primitives(2).origin(component)*& 
        ((-4.5_8 + originFactor)*primitives(2).origin(kappa) + (4.5_8 - originFactor)*& 
          primitiveAxB.origin(kappa)))*primitives(1).orbitalExponent + 2.0_8*(-3.5_8 + originFactor)*& 
      Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent**2.0_8 + & 
     primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*(primitiveAxB.origin(kappa)*(-4.0_8*primitiveAxB.orbitalExponent + (18. - 4.0_8*originFactor)*& 
          primitives(1).orbitalExponent) + primitives(2).origin(kappa)*(4.0_8*primitiveAxB.orbitalExponent + (-18. + 4.0_8*originFactor)*& 
          primitives(1).orbitalExponent))) + primitives(1).orbitalExponent*(Math_kroneckerDelta(beta, kappa)*& 
      (4.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*((-4.5_8 + originFactor)*primitives(1).origin(component) + & 
         (4.5_8 - originFactor)*primitives(2).origin(component))*(primitives(1).origin(alpha) - & 
         primitiveAxB.origin(alpha)) - 2.0_8*(-3.5_8 + originFactor)*& 
        Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent) + & 
     Math_kroneckerDelta(alpha, kappa)*& 
      (4.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*((-4.5_8 + originFactor)*primitives(1).origin(component) + & 
         (4.5_8 - originFactor)*primitives(2).origin(component))*(primitives(1).origin(beta) - & 
         primitiveAxB.origin(beta)) - 2.0_8*(-3.5_8 + originFactor)*Math_kroneckerDelta(beta, & 
         component)*primitives(2).orbitalExponent) + primitiveAxB.orbitalExponent*(44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(1).origin(component)*& 
        primitives(2).origin(kappa)*primitiveAxB.origin(alpha) - 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*& 
        primitives(1).origin(component)*primitives(2).origin(kappa)*primitiveAxB.origin(alpha) - 44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*& 
        primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha) + 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
        primitives(1).origin(beta)*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha) - & 
       44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitives(2).origin(kappa)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + & 
       8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitives(2).origin(kappa)*primitiveAxB.origin(alpha)*& 
        primitiveAxB.origin(beta) + 44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
        primitiveAxB.origin(beta) - 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*& 
        primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) - 44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(1).origin(component)*& 
        primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*& 
        primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + 44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*& 
        primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) - 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
        primitives(1).origin(beta)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + & 
       44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - & 
       8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*& 
        primitiveAxB.origin(kappa) - 44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*& 
        primitiveAxB.origin(kappa) + 8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
        primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + 18.0_8*Math_kroneckerDelta(kappa, component)*& 
        primitives(1).origin(beta)*primitiveAxB.origin(alpha)*primitives(1).orbitalExponent - 4.0_8*originFactor*& 
        Math_kroneckerDelta(kappa, component)*primitives(1).origin(beta)*primitiveAxB.origin(alpha)*& 
        primitives(1).orbitalExponent - 18.0_8*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*& 
        primitiveAxB.origin(beta)*primitives(1).orbitalExponent + 4.0_8*originFactor*Math_kroneckerDelta(kappa, & 
         component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent - & 
       4.0_8*((4.5_8 - originFactor)*Math_kroneckerDelta(beta, component)*& 
          primitiveAxB.origin(alpha) + Math_kroneckerDelta(alpha, component)*& 
          ((-4.5_8 + originFactor)*primitives(1).origin(beta) + (4.5_8 - originFactor)*& 
            primitiveAxB.origin(beta)))*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa))*primitives(2).orbitalExponent + & 
       primitives(1).origin(alpha)*(-44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(beta) + & 
         8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(beta) + & 
         44.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - & 
         8.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + & 
         primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta)*((44.0_8 - 8.0_8*originFactor)*& 
            primitives(2).origin(kappa) + (-44.0_8 + 8.0_8*originFactor)*primitiveAxB.origin(kappa)) + & 
         18.0_8*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent - & 
         4.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*& 
          primitives(1).orbitalExponent + primitives(1).origin(beta)*(8.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
            (primitives(1).origin(component)*((-5.5_8 + originFactor)*primitives(2).origin(kappa) + & 
               (5.5_8 - originFactor)*primitiveAxB.origin(kappa)) + primitives(2).origin(component)*& 
              ((5.5_8 - originFactor)*primitives(2).origin(kappa) + (-5.5_8 + originFactor)*primitiveAxB.origin(kappa))) + 4.0_8*(-4.5_8 + originFactor)*& 
            Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent) + & 
         18.0_8*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*primitives(2).orbitalExponent - & 
         4.0_8*originFactor*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*& 
          primitives(2).orbitalExponent - 18.0_8*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(kappa)*& 
          primitives(2).orbitalExponent + 4.0_8*originFactor*Math_kroneckerDelta(beta, component)*& 
          primitiveAxB.origin(kappa)*primitives(2).orbitalExponent)))))*commonFactor/(primitiveAxB.orbitalExponent**2.0_8*primitives(1).orbitalExponent) 
				
	end function KineticIntegrals_dp

	!**
	! Retorna el valor de la derivada integral <i> d(d|d)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como parametro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function KineticIntegrals_dd() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 2 )
		
		!! Calcula la integral de overlap
		output= (reducedOrbExponent*(Math_kroneckerDelta(indxCenterA, center) - Math_kroneckerDelta(indxCenterB, center))*& 
  (((Math_PI/primitiveAxB.orbitalExponent)**1.5_8*Math_kroneckerDelta(lambda, component)*& 
     (((-7.0_8 + 2.0_8*originFactor)*Math_kroneckerDelta(beta, kappa)*& 
         (primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + (primitives(1).origin(beta) - primitiveAxB.origin(beta))*& 
         ((-7.0_8 + 2.0_8*originFactor)*Math_kroneckerDelta(alpha, kappa) + & 
          2.0_8*primitiveAxB.orbitalExponent*(-9.0_8 + 2.0_8*originFactor)*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*& 
           (primitives(2).origin(kappa) - primitiveAxB.origin(kappa))))*primitives(1).orbitalExponent + & 
      Math_kroneckerDelta(alpha, beta)*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa))*& 
       (2.0_8*primitiveAxB.orbitalExponent + (-7.0_8 + 2.0_8*originFactor)*primitives(1).orbitalExponent)))/primitiveAxB.orbitalExponent**2.0_8 + & 
   ((Math_PI/primitiveAxB.orbitalExponent)**1.5_8*(reducedOrbExponent*Math_kroneckerDelta(alpha, beta)*& 
       Math_kroneckerDelta(kappa, lambda)*(primitives(1).origin(component) - primitives(2).origin(component))*& 
       (2.0_8*primitiveAxB.orbitalExponent + (-7.0_8 + 2.0_8*originFactor)*primitives(1).orbitalExponent) + & 
      primitives(1).orbitalExponent*(-7.0_8*reducedOrbExponent*Math_kroneckerDelta(alpha, kappa)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(component) + & 
        2.0_8*originFactor*reducedOrbExponent*Math_kroneckerDelta(alpha, kappa)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(component) - & 
        18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*& 
         primitives(1).origin(beta)*primitives(1).origin(component) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(1).origin(beta)*& 
         primitives(1).origin(component) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(alpha)*primitives(1).origin(component)*primitives(2).origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(alpha)*primitives(1).origin(component)*& 
         primitives(2).origin(kappa) + 7.0_8*reducedOrbExponent*Math_kroneckerDelta(alpha, kappa)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(component) - & 
        2.0_8*originFactor*reducedOrbExponent*Math_kroneckerDelta(alpha, kappa)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(component) + & 
        18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*& 
         primitives(1).origin(beta)*primitives(2).origin(component) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(1).origin(beta)*& 
         primitives(2).origin(component) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(alpha)*primitives(2).origin(kappa)*primitives(2).origin(component) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(alpha)*primitives(2).origin(kappa)*& 
         primitives(2).origin(component) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(beta)*primitives(1).origin(component)*primitiveAxB.origin(alpha) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(beta)*primitives(1).origin(component)*& 
         primitiveAxB.origin(alpha) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(component)*primitives(2).origin(kappa)*primitiveAxB.origin(alpha) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(component)*primitives(2).origin(kappa)*& 
         primitiveAxB.origin(alpha) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(beta)*primitives(2).origin(component)*primitiveAxB.origin(alpha) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(beta)*primitives(2).origin(component)*& 
         primitiveAxB.origin(alpha) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(kappa)*primitives(2).origin(component)*& 
         primitiveAxB.origin(alpha) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(alpha)*primitives(1).origin(component)*primitiveAxB.origin(beta) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(1).origin(component)*& 
         primitiveAxB.origin(beta) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(alpha)*primitives(2).origin(component)*primitiveAxB.origin(beta) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(2).origin(component)*& 
         primitiveAxB.origin(beta) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(component)*primitiveAxB.origin(alpha)*& 
         primitiveAxB.origin(beta) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
         primitiveAxB.origin(beta) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(alpha)*primitives(1).origin(component)*primitiveAxB.origin(kappa) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(alpha)*primitives(1).origin(component)*& 
         primitiveAxB.origin(kappa) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(alpha)*primitives(2).origin(component)*primitiveAxB.origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(alpha)*primitives(2).origin(component)*& 
         primitiveAxB.origin(kappa) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(1).origin(component)*primitiveAxB.origin(alpha)*& 
         primitiveAxB.origin(kappa) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*Math_kroneckerDelta(beta, lambda)*& 
         primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
         primitiveAxB.origin(kappa) - 7.0_8*Math_kroneckerDelta(beta, lambda)*& 
         Math_kroneckerDelta(kappa, component)*primitives(1).origin(alpha)*primitives(1).orbitalExponent + & 
        2.0_8*originFactor*Math_kroneckerDelta(beta, lambda)*& 
         Math_kroneckerDelta(kappa, component)*primitives(1).origin(alpha)*primitives(1).orbitalExponent + & 
        7.0_8*Math_kroneckerDelta(beta, lambda)*Math_kroneckerDelta(kappa, component)*& 
         primitiveAxB.origin(alpha)*primitives(1).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(beta, lambda)*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*& 
         primitives(1).orbitalExponent + 7.0_8*Math_kroneckerDelta(beta, component)*& 
         Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(2).orbitalExponent - & 
        2.0_8*originFactor*Math_kroneckerDelta(beta, component)*Math_kroneckerDelta(kappa, lambda)*primitives(1).origin(alpha)*primitives(2).orbitalExponent + & 
        7.0_8*Math_kroneckerDelta(alpha, component)*Math_kroneckerDelta(kappa, & 
          lambda)*primitives(1).origin(beta)*primitives(2).orbitalExponent - 2.0_8*originFactor*& 
         Math_kroneckerDelta(alpha, component)*Math_kroneckerDelta(kappa, lambda)*& 
         primitives(1).origin(beta)*primitives(2).orbitalExponent + 7.0_8*Math_kroneckerDelta(alpha, component)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(kappa)*primitives(2).orbitalExponent - & 
        2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*& 
         Math_kroneckerDelta(beta, lambda)*primitives(2).origin(kappa)*primitives(2).orbitalExponent - & 
        7.0_8*Math_kroneckerDelta(beta, component)*Math_kroneckerDelta(kappa, lambda)*& 
         primitiveAxB.origin(alpha)*primitives(2).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(beta, component)*Math_kroneckerDelta(kappa, lambda)*primitiveAxB.origin(alpha)*& 
         primitives(2).orbitalExponent - 7.0_8*Math_kroneckerDelta(alpha, component)*& 
         Math_kroneckerDelta(kappa, lambda)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent + & 
        2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*& 
         Math_kroneckerDelta(kappa, lambda)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent - & 
        7.0_8*Math_kroneckerDelta(alpha, component)*Math_kroneckerDelta(beta, lambda)*& 
         primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*Math_kroneckerDelta(beta, lambda)*primitiveAxB.origin(kappa)*& 
         primitives(2).orbitalExponent + Math_kroneckerDelta(alpha, lambda)*& 
         (reducedOrbExponent*(-7.0_8 + 2.0_8*originFactor)*Math_kroneckerDelta(beta, kappa)*& 
           (primitives(1).origin(component) - primitives(2).origin(component)) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*& 
           primitives(2).origin(kappa)*primitiveAxB.origin(beta) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*& 
           primitives(2).origin(kappa)*primitiveAxB.origin(beta) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*& 
           primitives(2).origin(component)*primitiveAxB.origin(beta) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*& 
           primitives(2).origin(component)*primitiveAxB.origin(beta) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta)*& 
           primitiveAxB.origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta)*& 
           primitiveAxB.origin(kappa) + 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - & 
          4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + & 
          7.0_8*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent - & 
          2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*& 
           primitives(1).orbitalExponent + primitives(1).origin(beta)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(9.0_8 - 2.0_8*originFactor)*& 
             primitives(2).origin(kappa)*primitives(2).origin(component) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-9.0_8 + 2.0_8*originFactor)*& 
             primitives(1).origin(component)*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) - & 
            18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
             primitives(2).origin(component)*primitiveAxB.origin(kappa) - 7.0_8*Math_kroneckerDelta(kappa, component)*& 
             primitives(1).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*& 
             primitives(1).orbitalExponent) + 7.0_8*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*& 
           primitives(2).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(beta, component)*& 
           primitives(2).origin(kappa)*primitives(2).orbitalExponent - 7.0_8*Math_kroneckerDelta(beta, component)*& 
           primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(kappa)*primitives(2).orbitalExponent))))/(primitiveAxB.orbitalExponent**2.0_8*primitives(1).orbitalExponent) + & 
   2.0_8*((-1.0_8*(Math_PI/primitiveAxB.orbitalExponent)**1.5_8*Math_kroneckerDelta(lambda, component)*& 
       (Math_kroneckerDelta(beta, kappa)*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha)) + & 
        Math_kroneckerDelta(alpha, kappa)*(primitives(1).origin(beta) - primitiveAxB.origin(beta)) + & 
        (Math_kroneckerDelta(alpha, beta) + 2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - & 
            primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta)))*& 
         (primitives(2).origin(kappa) - primitiveAxB.origin(kappa)))*primitives(1).orbitalExponent)/primitiveAxB.orbitalExponent**2.0_8 + & 
     ((Math_PI/primitiveAxB.orbitalExponent)**2.5_8*Math_kroneckerDelta(kappa, lambda)*& 
       (reducedOrbExponent*Math_kroneckerDelta(alpha, beta)*(primitives(1).origin(component) - & 
          primitives(2).origin(component)) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(1).origin(component)*primitiveAxB.origin(alpha) + & 
        2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(2).origin(component)*primitiveAxB.origin(alpha) + & 
        2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) - & 
        2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) - & 
        Math_kroneckerDelta(alpha, component)*primitives(1).origin(beta)*primitives(2).orbitalExponent + & 
        Math_kroneckerDelta(beta, component)*primitiveAxB.origin(alpha)*primitives(2).orbitalExponent + & 
        Math_kroneckerDelta(alpha, component)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent + & 
        primitives(1).origin(alpha)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*(primitives(1).origin(component) - & 
            primitives(2).origin(component)) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta) - Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent)))/(Math_PI*primitives(2).orbitalExponent) + & 
     ((Math_PI/primitiveAxB.orbitalExponent)**1.5_8*(Math_kroneckerDelta(beta, lambda)*& 
         (reducedOrbExponent*Math_kroneckerDelta(alpha, kappa)*(-primitives(1).origin(component) + & 
            primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitives(2).origin(kappa)*& 
           primitiveAxB.origin(alpha) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*& 
           primitiveAxB.origin(alpha) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + & 
          Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*primitives(1).orbitalExponent - & 
          primitives(1).origin(alpha)*(-2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component) + & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa) + Math_kroneckerDelta(kappa, & 
              component)*primitives(1).orbitalExponent) + Math_kroneckerDelta(alpha, component)*& 
           primitives(2).origin(kappa)*primitives(2).orbitalExponent - Math_kroneckerDelta(alpha, component)*& 
           primitiveAxB.origin(kappa)*primitives(2).orbitalExponent) + Math_kroneckerDelta(alpha, lambda)*& 
         (reducedOrbExponent*Math_kroneckerDelta(beta, kappa)*(-primitives(1).origin(component) + & 
            primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitives(2).origin(kappa)*& 
           primitiveAxB.origin(beta) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(beta) - & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + & 
          Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent - & 
          primitives(1).origin(beta)*(-2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component) + & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa) + Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent) + Math_kroneckerDelta(beta, component)*& 
           primitives(2).origin(kappa)*primitives(2).orbitalExponent - Math_kroneckerDelta(beta, component)*& 
           primitiveAxB.origin(kappa)*primitives(2).orbitalExponent) + Math_kroneckerDelta(kappa, lambda)*& 
         (reducedOrbExponent*Math_kroneckerDelta(alpha, beta)*(-primitives(1).origin(component) + & 
            primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(1).origin(component)*& 
           primitiveAxB.origin(alpha) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(2).origin(component)*primitiveAxB.origin(alpha) - & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + & 
          Math_kroneckerDelta(alpha, component)*primitives(1).origin(beta)*primitives(2).orbitalExponent - & 
          Math_kroneckerDelta(beta, component)*primitiveAxB.origin(alpha)*primitives(2).orbitalExponent - & 
          Math_kroneckerDelta(alpha, component)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent + & 
          primitives(1).origin(alpha)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*(-primitives(1).origin(component) + & 
              primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta) - & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta) + Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent))))/primitiveAxB.orbitalExponent**2.0_8 + & 
     ((Math_PI/primitiveAxB.orbitalExponent)**1.5_8*(-primitives(2).origin(lambda) + primitiveAxB.origin(lambda))*& 
       (Math_kroneckerDelta(kappa, component)*(Math_kroneckerDelta(alpha, beta) + & 
          2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) - primitiveAxB.origin(alpha))*(primitives(1).origin(beta) - primitiveAxB.origin(beta)))*& 
         primitives(1).orbitalExponent + Math_kroneckerDelta(beta, kappa)*& 
         (2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(alpha)*(primitives(1).origin(component) - primitives(2).origin(component)) - & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*& 
           primitiveAxB.origin(alpha) - Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent) + & 
        Math_kroneckerDelta(alpha, kappa)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*& 
           (primitives(1).origin(component) - primitives(2).origin(component)) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*& 
           primitiveAxB.origin(beta) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta) - & 
          Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent) + & 
        2.0_8*primitiveAxB.orbitalExponent*(-primitives(2).origin(kappa) + primitiveAxB.origin(kappa))*& 
         (reducedOrbExponent*Math_kroneckerDelta(alpha, beta)*(-primitives(1).origin(component) + & 
            primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(1).origin(component)*& 
           primitiveAxB.origin(alpha) - 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*primitives(2).origin(component)*primitiveAxB.origin(alpha) - & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + & 
          Math_kroneckerDelta(alpha, component)*primitives(1).origin(beta)*primitives(2).orbitalExponent - & 
          Math_kroneckerDelta(beta, component)*primitiveAxB.origin(alpha)*primitives(2).orbitalExponent - & 
          Math_kroneckerDelta(alpha, component)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent + & 
          primitives(1).origin(alpha)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(beta)*(-primitives(1).origin(component) + & 
              primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(beta) - & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta) + Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent))))/primitiveAxB.orbitalExponent**2.0_8) - & 
   ((Math_PI/primitiveAxB.orbitalExponent)**1.5_8*(-primitives(2).origin(lambda) + primitiveAxB.origin(lambda))*& 
     (Math_kroneckerDelta(alpha, beta)*(4.0_8*primitiveAxB.orbitalExponent**2.0_8*reducedOrbExponent*primitives(2).origin(component)*& 
         primitiveAxB.origin(kappa) + 2.0_8*primitiveAxB.orbitalExponent*Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent - & 
        18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa)*primitives(1).orbitalExponent + & 
        4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa)*primitives(1).orbitalExponent - & 
        7.0_8*Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent**2.0_8 + & 
        2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent**2.0_8 - & 
        2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*(2.0_8*primitiveAxB.orbitalExponent + (-9.0_8 + 2.0_8*originFactor)*& 
           primitives(1).orbitalExponent) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*(primitives(2).origin(kappa) - & 
          primitiveAxB.origin(kappa))*(2.0_8*primitiveAxB.orbitalExponent + (-9.0_8 + 2.0_8*originFactor)*primitives(1).orbitalExponent)) + & 
      primitives(1).orbitalExponent*(Math_kroneckerDelta(beta, kappa)*& 
         (2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-9.0_8 + 2.0_8*originFactor)*primitives(1).origin(alpha)*(primitives(1).origin(component) - & 
            primitives(2).origin(component)) + 2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(9.0_8 - 2.0_8*originFactor)*primitives(1).origin(component)*& 
           primitiveAxB.origin(alpha) - 18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha) + & 
          4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha) + & 
          7.0_8*Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent - & 
          2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent) + & 
        Math_kroneckerDelta(alpha, kappa)*(2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-9.0_8 + 2.0_8*originFactor)*& 
           primitives(1).origin(beta)*(primitives(1).origin(component) - primitives(2).origin(component)) + & 
          2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(9.0_8 - 2.0_8*originFactor)*primitives(1).origin(component)*primitiveAxB.origin(beta) - & 
          18.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(beta) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
           primitives(2).origin(component)*primitiveAxB.origin(beta) + 7.0_8*Math_kroneckerDelta(beta, component)*& 
           primitives(2).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(beta, component)*& 
           primitives(2).orbitalExponent) + 2.0_8*primitiveAxB.orbitalExponent*(-22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitives(2).origin(kappa)*& 
           primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*& 
           primitives(2).origin(kappa)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) + 22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*& 
           primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta) - & 
          4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
           primitiveAxB.origin(beta) + 22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*& 
           primitiveAxB.origin(kappa) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(1).origin(component)*primitiveAxB.origin(alpha)*& 
           primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - 22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*& 
           primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*& 
           primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - & 
          9.0_8*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*& 
           primitives(1).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*& 
           primitiveAxB.origin(alpha)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent - 9.0_8*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*primitiveAxB.origin(alpha)*primitives(2).orbitalExponent + & 
          2.0_8*originFactor*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*& 
           primitiveAxB.origin(alpha)*primitives(2).orbitalExponent - 9.0_8*Math_kroneckerDelta(alpha, component)*& 
           primitives(2).origin(kappa)*primitiveAxB.origin(beta)*primitives(2).orbitalExponent + 2.0_8*originFactor*& 
           Math_kroneckerDelta(alpha, component)*primitives(2).origin(kappa)*primitiveAxB.origin(beta)*& 
           primitives(2).orbitalExponent + 9.0_8*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(alpha)*& 
           primitiveAxB.origin(kappa)*primitives(2).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + & 
          9.0_8*Math_kroneckerDelta(alpha, component)*primitiveAxB.origin(beta)*primitiveAxB.origin(kappa)*& 
           primitives(2).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*& 
           primitiveAxB.origin(beta)*primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + primitives(1).origin(alpha)*& 
           (-22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(beta) + & 
            4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(kappa)*primitives(2).origin(component)*primitiveAxB.origin(beta) - & 
            2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-11.0_8 + 2.0_8*originFactor)*primitives(1).origin(component)*primitiveAxB.origin(beta)*& 
             (primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + 22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*& 
             primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) - 4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*& 
             primitiveAxB.origin(beta)*primitiveAxB.origin(kappa) + 9.0_8*Math_kroneckerDelta(kappa, component)*& 
             primitiveAxB.origin(beta)*primitives(1).orbitalExponent - 2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(beta)*primitives(1).orbitalExponent + primitives(1).origin(beta)*& 
             (2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(11.0_8 - 2.0_8*originFactor)*primitives(2).origin(kappa)*primitives(2).origin(component) + & 
              2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-11.0_8 + 2.0_8*originFactor)*primitives(1).origin(component)*(primitives(2).origin(kappa) - & 
                primitiveAxB.origin(kappa)) - 22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa) + & 
              4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(kappa) - 9.0_8*Math_kroneckerDelta(kappa, component)*& 
             primitives(1).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitives(1).orbitalExponent) +& 
             9.0_8*Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*primitives(2).orbitalExponent - 2.0_8*originFactor*& 
             Math_kroneckerDelta(beta, component)*primitives(2).origin(kappa)*primitives(2).orbitalExponent - & 
            9.0_8*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + & 
            2.0_8*originFactor*Math_kroneckerDelta(beta, component)*primitiveAxB.origin(kappa)*& 
             primitives(2).orbitalExponent) + primitives(1).origin(beta)*(-2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-11.0_8 + 2.0_8*originFactor)*& 
             primitives(1).origin(component)*primitiveAxB.origin(alpha)*(primitives(2).origin(kappa) - primitiveAxB.origin(kappa)) + & 
            22.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) - & 
            4.0_8*originFactor*primitiveAxB.orbitalExponent*reducedOrbExponent*primitives(2).origin(component)*primitiveAxB.origin(alpha)*primitiveAxB.origin(kappa) + & 
            9.0_8*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*primitives(1).orbitalExponent - & 
            2.0_8*originFactor*Math_kroneckerDelta(kappa, component)*primitiveAxB.origin(alpha)*& 
             primitives(1).orbitalExponent - 9.0_8*Math_kroneckerDelta(alpha, component)*primitiveAxB.origin(kappa)*& 
             primitives(2).orbitalExponent + 2.0_8*originFactor*Math_kroneckerDelta(alpha, component)*& 
             primitiveAxB.origin(kappa)*primitives(2).orbitalExponent + primitives(2).origin(kappa)*& 
             (2.0_8*primitiveAxB.orbitalExponent*reducedOrbExponent*(-11.0_8 + 2.0_8*originFactor)*primitives(2).origin(component)*primitiveAxB.origin(alpha) + & 
              (9.0_8 - 2.0_8*originFactor)*Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent))))))/(primitiveAxB.orbitalExponent**2.0_8*primitives(1).orbitalExponent)))/(2.0_8*exp(originFactor))
	end function KineticIntegrals_dd
	
end module KineticDerivatives_
