!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords:                                                                    !
!!                                                                                 !
!!    thisPtr files is part of nonBOA                                                 !
!!                                                                                 !
!!    thisPtr program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    thisPtr program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with thisPtr program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para definicion de datos de elemetos atomicos.
! 
!  Este modulo define una seudoclase para manejo de datos atomicos, correspondientes
!  a elementos atomicos.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2008-08-05
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-20 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapto al estandar de codificacion propuesto.
!
! @see XMLParser_
!
!**
module AtomicElement_
	use APMO_

	implicit none
	
	type , public :: AtomicElement
		character(30) :: name
		character(30) :: symbol
		real(8) :: atomicNumber 
		real(8) :: massicNumber
		real(8) :: abundance
		real(8) :: meltingPoint
		real(8) :: boilingPoint
		real(8) :: density
		real(8) :: electronAffinity
		real(8) :: electronegativity
		real(8) :: ionizationEnergy1
		real(8) :: covalentRadius
		real(8) :: atomicRadio
		real(8) :: vanderWaalsRadio
		real(8) :: atomicWeight
		real(8) :: nuclearSpin

		!!< Variables con propositos de conveniencia
		logical :: isInstanced	
		logical :: isMassNumberPresent
		real(8) :: auxMassicNumber
		real(8) :: auxWeight
		real(8) :: auxAbundance
		character(20) :: blockName
		
	end type AtomicElement

	private ::
		type(AtomicElement), pointer :: thisPtr
		integer :: i
		integer :: j
		real(8) :: internalMassicNumber
		real(8) :: internalWeight
		real(8) :: auxAbundance
		character(20) :: bblockName
		character(20) :: currentBlockName
		character(20) :: currentBlockSymbol
		
	
	public :: &
		AtomicElement_constructor, &
		AtomicElement_destructor, &
		AtomicElement_show, &
		AtomicElement_XMLParser_startElement, &
		AtomicElement_XMLParser_endElement
	
contains
	
	
	!**
	! Define el constructor para la clase
	!
	!**
	subroutine AtomicElement_constructor( this)
		implicit none
		type(AtomicElement), intent(inout) :: this
		
		this.name			=	""
		this.symbol		=	""
		this.atomicNumber	=	0.0_8
		this.massicNumber	=	-1.0_8
		this.abundance 		=	0.0_8
		this.meltingPoint	=	0.0_8
		this.boilingPoint		=	0.0_8
		this.density		=	0.0_8
		this.electronAffinity	=	0.0_8
		this.electronegativity	=	0.0_8
		this.ionizationEnergy1=	0.0_8
		this.covalentRadius	=	0.0_8
		this.atomicRadio	=	0.0_8
		this.vanderWaalsRadio=	0.0_8
		this.atomicWeight	=	0.0_8
		this.nuclearSpin 	=	0.0_8
		this.isInstanced		=	.false.
		this.isMassNumberPresent = .false.
		this.auxMassicNumber=0.0_8
		this.auxWeight=0.0_8
		this.auxAbundance=-1.0_8
		this.blockName = " "	

	end subroutine AtomicElement_constructor
	
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine AtomicElement_destructor( this )
		implicit none
		type(AtomicElement), intent(inout) :: this

		
		if( associated(thisPtr) ) nullify(thisPtr)
		
	end subroutine AtomicElement_destructor
	
		
	!**
	! Define el destructor para clase
	!
	! @param thisPtr Funcion base
	!**
	subroutine AtomicElement_show( this )
		implicit none
		type(AtomicElement) , intent(in) :: this
		
		
		print *,""
		print *,"======================="
		print *,"     Element Properties "
		print *,"======================="
		print *,""
		write (6,"(T10,A22,A12,A10)")	"Name                = ",this.name,""
		write (6,"(T10,A22,A12,A10)")	"Symbol              = ",this.symbol,""
		write (6,"(T10,A22,F12.5,A10)")	"Atomic number       = ",this.atomicNumber,""
		write (6,"(T10,A22,F12.5,A10)")	"masic number        = ",this.massicNumber,""
		write (6,"(T10,A22,F12.5,A10)")	"Isotopic abundance  = ",this.abundance,""
		write (6,"(T10,A22,F12.5,A10)")	"Melting point       = ",this.meltingpoint," K"
		write (6,"(T10,A22,F12.5,A10)")	"Boiling point       = ",this.boilingpoint," K"	
		write (6,"(T10,A22,F12.5,A10)")	"Density             = ",this.density," g/cm^3"
		write (6,"(T10,A22,F12.5,A10)")	"Electron affinity   = ",this.electronaffinity," kJ/mol"
		write (6,"(T10,A22,F12.5,A10)")	"1.Ionization energy = ",this.ionizationenergy1," kJ/mol"
		write (6,"(T10,A22,F12.5,A10)")	"Electronegativity   = ",this.electronegativity," (pauling)"
		write (6,"(T10,A22,F12.5,A10)")	"Covalent radius     = ",this.covalentRadius," pm"
		write (6,"(T10,A22,F12.5,A10)")	"Atomic radius       = ",this.atomicRadio," pm" 
		write (6,"(T10,A22,F12.5,A10)")	"Van der Waals radius= ",this.vanderwaalsRadio," pm" 
		write (6,"(T10,A22,F12.5,A10)")	"Atomic weight       = ",this.atomicWeight,"u.m.a" 
		write (6,"(T10,A22,F12.5,A10)")	"Nuclear Spin        = ",this.nuclearSpin,""
		print *,""
		
	end subroutine AtomicElement_show

	
	subroutine AtomicElement_XMLParser_startElement ( this, tagName, dataName, dataValue, blockName )
		implicit none
		type(AtomicElement), target, intent(inout) :: this
		character(*) :: tagName
		character(*) :: dataName
		character(*) :: dataValue
		character(*) :: blockName
		
		if( associated( thisPtr) ) nullify(thisPtr)
		thisPtr => this
		thisPtr.blockName = trim( blockName )

		select case ( trim(tagName) ) 

			case ("element")
				
				select case ( trim(dataName) )
					
					case("name")
									
						currentBlockName = trim(dataValue)
						
					case("symbol")
									
						currentBlockSymbol = trim(dataValue)
						
						
				end select
		end select
		
		if ( ( trim( blockName ) == currentBlockName ) .or. ( trim( blockName ) == currentBlockSymbol ) ) then
		
			select case ( trim(tagName) )
			
				case ("element")
				
					
					select case ( trim(dataName) )
						
						
						case("name")
							
							thisPtr.name = trim(dataValue)
						
						case("symbol")
								
							thisPtr.symbol = trim(dataValue)
							thisPtr.name = currentBlockName
							
						case("atomicNumber")
										
							thisPtr.atomicNumber = DNUM( dataValue )
							
					end select
	
				case ("physicalproperties")
				
					select case ( trim(dataName) )
						
						case("meltingpoint")
										
							thisPtr.meltingPoint = DNUM(dataValue)
							
						case("boilingpoint")
										
							thisPtr.boilingPoint = DNUM(dataValue)
							
						case("density")
										
							thisPtr.density = DNUM( dataValue )
							
					end select
	
				case ("miscellaneous")
				
					select case ( trim(dataName) )
						
						case("electronegativity")
										
							thisPtr.electronegativity = DNUM(dataValue)
							
					end select
	
				case ("radius")
				
					select case ( trim(dataName) )
						
						case("covalent")
										
							thisPtr.covalentRadius = DNUM(dataValue)
						
						case("atomic")
										
							thisPtr.atomicRadio = DNUM(dataValue)
							
						case("vanderwaals")
										
							thisPtr.vanderWaalsRadio = DNUM(dataValue)
							
					end select
	
				case ("isotope")
				
					
				
					select case ( trim(dataName) )
						
						
						case("massicNumber")

							if ( thisPtr.massicNumber > 0.0 ) then
								if ( abs( thisPtr.massicNumber - DNUM(dataValue) ) <= APMO_instance.DOUBLE_ZERO_THRESHOLD ) then
									
									thisPtr.isMassNumberPresent=.true.
								
								end if
							end if
							
							this.auxMassicNumber = DNUM(dataValue)
						
						case("atomicWeight")


							if ( thisPtr.isMassNumberPresent == .true. )  then
								
								thisPtr.atomicWeight = DNUM(dataValue)
	
							end if

							
							this.auxWeight = DNUM(dataValue)
							
						case("abundance")
								
							if ( thisPtr.isMassNumberPresent==.true. )  then
								
								thisPtr.abundance = DNUM(dataValue)
							
							else 
								
								
								if ( ( thisPtr.massicNumber <= 0.0 ) .and. &
									( DNUM(dataValue) > thisPtr.auxAbundance ) ) then

									internalMassicNumber = thisPtr.auxMassicNumber
									internalWeight = thisPtr.auxWeight
									thisPtr.abundance = DNUM(dataValue)
									thisPtr.auxAbundance = DNUM(dataValue)

								end if
							
							end if
							
						case("nuclearSpin")
						
							if ( thisPtr.isMassNumberPresent==.true. )  then
								
								thisPtr.nuclearSpin = DNUM(dataValue)
								thisPtr.isMassNumberPresent = .false.

							else
								if ( ( thisPtr.massicNumber <= 0.0 ) .and. &
									( DNUM(dataValue) > thisPtr.auxAbundance ) ) then
									thisPtr.nuclearSpin = DNUM(dataValue)
								end if
								
							end if
							
					end select
	
	
			end select
		
		end if
		
	end subroutine AtomicElement_XMLParser_startElement
	
	subroutine AtomicElement_XMLParser_endElement( tagName)
		implicit none
		character(*) :: tagName
		
		select case ( trim(tagName) ) 

			case ("element")
				
				if ( abs( thisPtr.massicNumber) <= APMO_instance.DOUBLE_ZERO_THRESHOLD  ) then
					
					thisPtr.massicNumber = internalMassicNumber
					thisPtr.atomicWeight = internalWeight

				else if( thisPtr.massicNumber < 0.0) then
					
					if ( ( trim( thisPtr.blockName ) == currentBlockName ) .or. ( trim( thisPtr.blockName ) == currentBlockSymbol ) ) then
				
	 					thisPtr.massicNumber = internalMassicNumber
 						thisPtr.atomicWeight = internalWeight
				
					end if

				
				end if
				
				if ( thisPtr.name /= "")  then
					thisPtr.isInstanced = .true.
				end if

				currentBlockName = ""
                                currentBlockSymbol = ""

		end select
		
	end subroutine AtomicElement_XMLParser_endElement
	
end module AtomicElement_