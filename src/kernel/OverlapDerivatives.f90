!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief Modulo para calculo de derivadas de integrales de overlap 
! 
! Este modulo contiene los algoritmos necesarios para la evaluacion de derivadas
! de integrales de overlap entre pares de funciones gaussianas primitivas 
! (PrimitiveGaussian_), sin normalizar.
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-12-03
!
! @see gaussianProduct, PrimitiveGaussian_, OverlapIntegrals_ 
!
!**
module OverlapDerivatives_
	use PrimitiveGaussian_
	use Exception_

	implicit none
	
	private ::
		real(8) :: commonFactor
		real(8) :: reducedOrbExponent 
		integer :: component
		integer :: center
		integer :: indxCenterA
		integer :: indxCenterB
		type(PrimitiveGaussian) :: primitives(2)
		type(PrimitiveGaussian) :: primitiveAxB

	public :: &
		PrimitiveGaussian_overlapDerivative
		
	private :: &
		OverlapDerivatives_ss, &
		OverlapDerivatives_ps, &
		OverlapDerivatives_pp, &
		OverlapDerivatives_ds, &
		OverlapDerivatives_dp, &
		OverlapDerivatives_dd
		
contains

	!**
	! Retorna el valor de la integral de overlap entre un par de 
	! funciones gausianas sin normalizar, pasadas como par�metro.
	! Esta funci�n se encarga de utilizar el procedimineto adecuado
	! de acuerdo al momento angular de las funciones de entrada
	!
	! @param primitiveA Gausiana primitiva 
	! @param primitiveB Gausiana primitiva 
	!
	! @return Valor de la integral de overlap
	!**
	function PrimitiveGaussian_overlapDerivative( primitiveA , primitiveB , center_, component_) &
		result( output )
		implicit none
		type(PrimitiveGaussian), intent(in) :: primitiveA
		type(PrimitiveGaussian), intent(in) :: primitiveB
		integer, intent(in) :: center_
		integer, intent(in) :: component_
		real(8) :: output
		
		integer :: caseIntegral
		type(Exception) :: ex
		
		!!*******************************************************************
		!! Seleccion el metodo adecuado para el calculo de la integral
		!! de acuedo con el momento angular de las funciones de entrada.
		!!
		
		!! Ordena la gausianas de entrada de mayor a menor momento
		call  PrimitiveGaussian_sort( primitives,primitiveA, primitiveB )
		
		center = center_
		component = component_
		indxCenterA=primitives(1).owner
		indxCenterB=primitives(2).owner
		
		
		!! Obtiene el producto de la dos gausianas de entrada
		primitiveAxB = PrimitiveGaussian_product(primitiveA, primitiveB)
		reducedOrbExponent = PrimitiveGaussian_reducedOrbitalExponent( primitives(1) , primitives(2) )
		commonFactor = ( Math_PI/primitiveAxB.orbitalExponent )**1.5_8 * &
			PrimitiveGaussian_productConstant( primitives(1) , primitives(2) ) &
			/ primitiveAxB.orbitalExponent
		
		!! Determina la integral que se debe usar
		caseIntegral = 4*PrimitiveGaussian_getAngularMoment(primitives(1)) + PrimitiveGaussian_getAngularMoment(primitives(2))
		 
		select case (caseIntegral)
		
		case(0) 
			output = OverlapDerivatives_ss()
		
		case(4) 
			output = OverlapDerivatives_ps()
				
		case(5) 
			output = OverlapDerivatives_pp()
		
		case(8)
			output = OverlapDerivatives_ds()
			
		case(9) 
			output = OverlapDerivatives_dp()
			
		case(10) 
			output = OverlapDerivatives_dd()
		
		case default
			output = 0.0_8
			
			call Exception_constructor( ex , ERROR )
			call Exception_setDebugDescription( ex, "Class object PrimitiveGaussian in the overlap derivative function" )
			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
			call Exception_show( ex )
		
		end select
		!!*******************************************************************
		
	end function PrimitiveGaussian_overlapDerivative


	!**
	! Retorna el valor de la derivada integral <i> d(s|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_ss() result( output )
		implicit none
		real(8) :: output
		
		!! Calcula la integral de overlap
		output = (2.0_8 * reducedOrbExponent &
			*(-Math_kroneckerDelta(indxCenterA, center) &
			+Math_kroneckerDelta(indxCenterB, center)) &
			*(primitives(1).origin(component) &
			- primitives(2).origin(component))) &
			* commonFactor  * primitiveAxB.orbitalExponent
				
	end function OverlapDerivatives_ss

	!**
	! Retorna el valor de la derivada integral <i> d(p|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_ps() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output = ((Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) &
			* (2.0_8 * primitiveAxB.orbitalExponent &
			* reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			*(primitives(1).origin(alpha)-primitiveAxB.origin(alpha)) &
			- Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent)) &
			* commonFactor 
				
	end function OverlapDerivatives_ps
	
	!**
	! Retorna el valor de la derivada integral <i> d(p|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_pp() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getPMomentComponent( primitives(1).angularMomentIndex )
		beta  = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output = ((Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) &
			* (reducedOrbExponent*Math_kroneckerDelta(alpha, beta) &
			* (-primitives(1).origin(component) &
			+ primitives(2).origin(component)) &
			+ (primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			* (-2.0_8*primitiveAxB.orbitalExponent * reducedOrbExponent &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) & 
			* (primitives(2).origin(beta) &
			- primitiveAxB.origin(beta)) - Math_kroneckerDelta(beta, component) &
			* primitives(1).orbitalExponent) + Math_kroneckerDelta(alpha, component) &
			* (primitives(2).origin(beta) &
			- primitiveAxB.origin(beta)) *primitives(2).orbitalExponent)) &
			* commonFactor 
				
	end function OverlapDerivatives_pp
	
	!**
	! Retorna el valor de la derivada integral <i> d(d|s)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_ds() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		
		!! Calcula la integral de overlap
		output = ((Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) & 
			*(-(reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component))*(Math_kroneckerDelta(alpha, beta) &
			+     2.0_8 * primitiveAxB.orbitalExponent &
			* (primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha))*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta)))) &
			+ (Math_kroneckerDelta(beta, component)*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) & 
			+ Math_kroneckerDelta(alpha, component)*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta))) & 
			* primitives(2).orbitalExponent)) * commonFactor
				
	end function OverlapDerivatives_ds


	!**
	! Retorna el valor de la derivada integral <i> d(d|p)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_dp() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getPMomentComponent( primitives(2).angularMomentIndex )
		
		!! Calcula la integral de overlap
		output = ((Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) & 
			* (Math_kroneckerDelta(kappa, component)*(Math_kroneckerDelta(alpha, beta) &
			+ 2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha))*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta))) &
			* primitives(1).orbitalExponent + Math_kroneckerDelta(beta, kappa) &
			* (2.0_8 * primitiveAxB.orbitalExponent*reducedOrbExponent &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			- Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent) &
			+ Math_kroneckerDelta(alpha, kappa) &
			* ( 2.0_8 * primitiveAxB.orbitalExponent * reducedOrbExponent &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(1).origin(beta) - primitiveAxB.origin(beta)) &
			- Math_kroneckerDelta(beta, component)*primitives(2).orbitalExponent) &
			+ 2.0_8 * primitiveAxB.orbitalExponent*(primitives(2).origin(kappa)&
			- primitiveAxB.origin(kappa)) &
			*(reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (Math_kroneckerDelta(alpha, beta) + 2.0_8 * primitiveAxB.orbitalExponent &
			* (primitives(1).origin(alpha) -primitiveAxB.origin(alpha))&
			* (primitives(1).origin(beta) - primitiveAxB.origin(beta)))&
			- (Math_kroneckerDelta(beta, component)*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) + Math_kroneckerDelta(alpha, component) &
			* (primitives(1).origin(beta) - primitiveAxB.origin(beta)))&
			* primitives(2).orbitalExponent))) * commonFactor &
			/ (2.0_8*primitiveAxB.orbitalExponent) 
				
	end function OverlapDerivatives_dp

	!**
	! Retorna el valor de la derivada integral <i> d(d|d)/dRu </i> para un par de 
	! funciones gaussianas sin normalizar pasadas como par�metro.
	!
	! @return Valor de la derivada de la integral de overlap
	!
	!**
	function OverlapDerivatives_dd() result( output )
		implicit none
		real(8) :: output
		
		integer :: alpha
		integer :: beta
		integer :: kappa
		integer :: lambda
		
		!! Determina el indice de la componente de momento angular
		alpha = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 1 )
		beta  = PrimitiveGaussian_getDMomentComponent( primitives(1).angularMomentIndex, 2 )
		kappa = PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 1 )
		lambda= PrimitiveGaussian_getDMomentComponent( primitives(2).angularMomentIndex, 2 )
		
		!! Calcula la integral de overlap
		output = ((Math_kroneckerDelta(indxCenterA, center) &
			- Math_kroneckerDelta(indxCenterB, center)) &
			* (-1.*Math_kroneckerDelta(lambda, component) &
			* (Math_kroneckerDelta(beta, kappa)*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			+ Math_kroneckerDelta(alpha, kappa)*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta)) &
			+ (Math_kroneckerDelta(alpha, beta) + 2.0_8 * primitiveAxB.orbitalExponent &
			* (primitives(1).origin(alpha)-primitiveAxB.origin(alpha))&
			* (primitives(1).origin(beta) &
			- primitiveAxB.origin(beta)))*(primitives(2).origin(kappa)&
			- primitiveAxB.origin(kappa))) &
			* primitives(1).orbitalExponent + Math_kroneckerDelta(kappa, lambda) &
			* (-(reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component))*(Math_kroneckerDelta(alpha,beta) &
			+ 2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha)&
			- primitiveAxB.origin(alpha)) &
			* (primitives(1).origin(beta)-primitiveAxB.origin(beta))))&
			+ (Math_kroneckerDelta(beta, component)*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha)) &
			+ Math_kroneckerDelta(alpha, component)*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta))) * primitives(2).orbitalExponent) &
			+ Math_kroneckerDelta(beta, lambda) &
			* (reducedOrbExponent*Math_kroneckerDelta(alpha, kappa) &
			* (-primitives(1).origin(component) &
			+ primitives(2).origin(component)) &
			+ (primitives(1).origin(alpha)-primitiveAxB.origin(alpha))&
			* (-2.0_8 * primitiveAxB.orbitalExponent * reducedOrbExponent &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(2).origin(kappa)-primitiveAxB.origin(kappa))&
			- Math_kroneckerDelta(kappa, component) * primitives(1).orbitalExponent) &
			+ Math_kroneckerDelta(alpha, component)*(primitives(2).origin(kappa) &
			- primitiveAxB.origin(kappa)) &
			* primitives(2).orbitalExponent) + Math_kroneckerDelta(alpha, lambda) &
			* (reducedOrbExponent*Math_kroneckerDelta(beta,kappa) &
			*(-primitives(1).origin(component) &
			+ primitives(2).origin(component)) &
			+ (primitives(1).origin(beta) - primitiveAxB.origin(beta)) &
			* ( -2.0_8 * primitiveAxB.orbitalExponent * reducedOrbExponent &
			* (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(2).origin(kappa)-primitiveAxB.origin(kappa))&
			- Math_kroneckerDelta(kappa, component) * primitives(1).orbitalExponent) &
			+ Math_kroneckerDelta(beta, component) * (primitives(2).origin(kappa) &
			- primitiveAxB.origin(kappa))*primitives(2).orbitalExponent) &
			+ (-primitives(2).origin(lambda) &
			+ primitiveAxB.origin(lambda)) *(Math_kroneckerDelta(kappa, component) &
			* (Math_kroneckerDelta(alpha, beta) + 2.0_8 * primitiveAxB.orbitalExponent &
			* (primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha))*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta)))* primitives(1).orbitalExponent &
			+ Math_kroneckerDelta(beta, kappa) * ( 2.0_8 * primitiveAxB.orbitalExponent &
			* reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(1).origin(alpha)- primitiveAxB.origin(alpha))&
			- Math_kroneckerDelta(alpha, component)*primitives(2).orbitalExponent) &
			+ Math_kroneckerDelta(alpha, kappa) * (2.0_8 * primitiveAxB.orbitalExponent &
			* reducedOrbExponent * (primitives(1).origin(component) &
			- primitives(2).origin(component)) &
			* (primitives(1).origin(beta) &
			- primitiveAxB.origin(beta)) - Math_kroneckerDelta(beta,component) &
			* primitives(2).orbitalExponent) + 2.0_8 * primitiveAxB.orbitalExponent &
			* (primitives(2).origin(kappa)-primitiveAxB.origin(kappa))&
			* (reducedOrbExponent*(primitives(1).origin(component) &
			- primitives(2).origin(component)) * (Math_kroneckerDelta(alpha, beta) &
			+ 2.0_8*primitiveAxB.orbitalExponent*(primitives(1).origin(alpha) &
			- primitiveAxB.origin(alpha))*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta))) - (Math_kroneckerDelta(beta, component) &
			* (primitives(1).origin(alpha)-primitiveAxB.origin(alpha))&
			+ Math_kroneckerDelta(alpha, component)*(primitives(1).origin(beta) &
			- primitiveAxB.origin(beta))) &
			* primitives(2).orbitalExponent)))) * commonFactor &
			/ ( 2.0_8 * primitiveAxB.orbitalExponent ) 
				
	end function OverlapDerivatives_dd
	
end module OverlapDerivatives_
