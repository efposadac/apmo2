!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia
!!    http://www.unal.edu.co
!!
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>
!!    Keywords:
!!
!!    This files is part of nonBOA
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************

!** 
! @brief  Modulo para definicion y ajuste de parametros de control del programa
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion: </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de modulo y metodos basicos
!   - <tt> 2010-12-15 </tt>:  Edwin F. Posada C. ( efposadac@unal.edu.co )
!        -# Adicion de atributos a la clase
!**
module APMO_
	!use IFPORT
	
	implicit none
	
	type, public :: APMO
		
		!!***************************************************************************
		!! Parametros para control de umbrales en calculo de integrales de 
		!! contracciones gausianas y moleculares 
		!!
		real(8) :: TV	
		real(8) :: INTEGRAL_THRESHOLD 
		character(10) :: INTEGRAL_METHOD
		!!***************************************************************************
		
		
		!!***************************************************************************
		!! Parametros para control de proceso de minizacion de energia mediante 
		!! metodo SCF 
		!!
		real(8) :: SCF_NONELECTRONIC_ENERGY_TOLERANCE
		real(8) :: SCF_ELECTRONIC_ENERGY_TOLERANCE
		real(8) :: NONELECTRONIC_DENSITY_MATRIX_TOLERANCE
		real(8) :: ELECTRONIC_DENSITY_MATRIX_TOLERANCE
		real(8) :: TOTAL_ENERGY_TOLERANCE
		real(8) :: STRONG_ENERGY_TOLERANCE		!< Permite controlar la convergencia exaustiva de todas las especies
		real(8) :: DENSITY_FACTOR_THRESHOLD			!< define cuando recalcula un elemeto Gij de acuedo con el valor de Pij
		real(8) :: DOUBLE_ZERO_THRESHOLD
		real(8) :: DIIS_SWITCH_THRESHOLD
		real(8) :: DIIS_SWITCH_THRESHOLD_BKP
		real(8) :: STEP_OF_GRAPHS
		real(8) :: WAVE_FUNCTION_SCALE
		integer :: SCF_NONELECTRONIC_MAX_ITERATIONS
		integer :: SCF_ELECTRONIC_MAX_ITERATIONS
		integer :: SCF_MAX_ITERATIONS					!< Limita el numero global de itereaciones para cualquier especie
		integer :: SCF_INTERSPECIES_MAXIMUM_ITERATIONS
		integer :: LISTS_SIZE
		integer  :: CONVERGENCE_METHOD
		integer :: DIIS_DIMENSIONALITY
		integer :: ITERATION_SCHEME
		integer :: MAXIMUM_RANGE_OF_GRAPHS
		integer :: GRAPH_FOR_NUM_ORBITAL
		character(10)  :: SCF_ELECTRONIC_TYPE_GUESS
		character(10)  :: SCF_NONELECTRONIC_TYPE_GUESS
		character(10) ::  SCF_CONVERGENCE_CRITERIUM		!< Define si el criterio de convergencia esta dado por cambios de  energia o densidad
		character(50) :: EXTERNAL_POTENTIAL
		logical :: DIIS_ERROR_IN_DAMPING
		logical :: READ_COEFFICIENTS

		!!***************************************************************************
		
		!!***************************************************************************
		!! Parametros para control de proceso de minimizacion multidimensional
		!! 
		
		real(8) :: NUMERICAL_DERIVATIVE_DELTA
		real(8) :: MINIMIZATION_INITIAL_STEP_SIZE 
		real(8) :: MINIMIZATION_LINE_TOLERANCE
		real(8) :: MINIMIZATION_TOLERANCE_GRADIENT
		integer :: MINIMIZATION_MAX_ITERATION
		character(10)   ::  MINIMIZATION_METHOD
		character(10)   ::  MINIMIZATION_LIBRARY
		character(50)   ::  COORDINATES
		character(10)   ::  ENERGY_CALCULATOR
		logical :: ANALYTIC_GRADIENT
		logical :: MINIMIZATION_WITH_SINGLE_POINT !< Realiza proceso de minimizacion empleando parametros de un calculo de punto sencillo
		logical :: USE_SYMMETRY_IN_MATRICES
		logical :: RESTART_OPTIMIZATION
		logical :: OPTIMIZE_WITH_CP_CORRECTION
		logical :: CP_CORRECTION
		!!***************************************************************************
		
		!!***************************************************************************
		!! Criterios de conectividad atomica
		!! 
		real(8) :: BOND_DISTANCE_FACTOR
		real(8) :: BOND_ANGLE_THRESHOLD
		real(8) :: DIHEDRAL_ANGLE_THRESHOLD
		!!***************************************************************************


		!!***************************************************************************
		!! Parametros para control de correccion mediante teoria de perturbaciones MPn
		!! 
		integer :: MP_FROZEN_CORE_BOUNDARY
		logical :: MP_ONLY_ELECTRONIC_CORRECTION
		!!***************************************************************************
		
		
		!!***************************************************************************
		!! Variables de ambiente al sistema de archivos del programa 
		!! 
		character(255) :: HOME_DIRECTORY=""
		character(255) :: DATA_DIRECTORY=""
		character(255) :: EXTERNAL_COMMAND=""
		character(30)   :: EXTERNAL_SOFTWARE_NAME=""
		character(255) :: ATOMIC_ELEMENTS_DATABASE=""
		character(255) :: BASIS_SET_DATABASE=""
		character(255) :: POTENTIALS_DATABASE=""
		character(255) :: ELEMENTAL_PARTICLES_DATABASE=""
		character(100)  :: INPUT_FILE=""
		!!***************************************************************************
	
		!!***************************************************************************
		!! Control de formato de impresion y unidades
		!!
		integer :: FORMAT_NUMBER_OF_COLUMNS
		integer :: UNID_FOR_OUTPUT_FILE
		integer :: UNID_FOR_MP2_INTEGRALS_FILE
		integer :: UNID_FOR_MOLECULAR_ORBITALS_FILE
		character(50)  :: UNITS

		!!***************************************************************************
		
		!!***************************************************************************
		!! Tareas que deben realizarse
		!!
		integer :: MOLLER_PLESSET_CORRECTION
		character(50) :: METHOD
		character(20) :: FROZEN_PARTICLE(5)
		logical :: OPTIMIZE
		logical :: OPTIMIZE_WITH_MP
		logical :: ONLY_ELECTRONIC_EFFECT
		logical :: PROJECT_HESSIANE
		logical :: ELECTRONIC_WAVEFUNCTION_ANALYSIS
        !!***************************************************************************
		
		!!*****************************************************
		!! Impresion en proceso de depuracion
		!!
		logical :: DEBUG_SCFS
		!!
		!!*****************************************************

		!!*****************************************************
		!! Parametros de control general
		!!
		logical :: REMOVE_TRANSLATIONAL_CONTAMINATION
		logical :: TRANSFORM_TO_CENTER_OF_MASS
		logical :: ARE_THERE_DUMMY_ATOMS
		logical :: IS_THERE_EXTERNAL_POTENTIAL
		logical :: IS_THERE_FROZEN_PARTICLE
                logical :: APMOLPRO
		!!
		!!*****************************************************
	
	end type APMO

 	type(APMO), target :: APMO_instance
	logical, private :: instanced = .false.
	
	public  &
		APMO_constructor, &
		APMO_destructor, &
		APMO_showCredits, &
		APMO_getHomeDirectory, &
		APMO_getDataDirectory

		
contains

	!**
	! Constructor por omision
	!**
	subroutine APMO_constructor ()
		implicit none
		
		
		!!*****************************************************
		!! Variables para control de integrales
		!!
		APMO_instance%INTEGRAL_THRESHOLD = 1.0E-10
		APMO_instance%TV = 1.0E-6
		APMO_instance%INTEGRAL_METHOD = "RECURSIVE" !!valores permitidos "EXPLICIT" y "RECURSIVE"
		!!
		!!*****************************************************

		!!***************************************************************************
		!! Parametros para control de proceso de minizacion de energia mediante 
		!! metodo SCF 
		!!
		APMO_instance%SCF_NONELECTRONIC_ENERGY_TOLERANCE = 1.0E-5
		APMO_instance%SCF_ELECTRONIC_ENERGY_TOLERANCE = 1.0E-6
		APMO_instance%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE = 5.0E-4
		APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE = 1.0E-6
		APMO_instance%TOTAL_ENERGY_TOLERANCE = 1.0E-7
		APMO_instance%STRONG_ENERGY_TOLERANCE = 1.0E-9
		APMO_instance%DENSITY_FACTOR_THRESHOLD = 1.0E-8
		APMO_instance%SCF_NONELECTRONIC_MAX_ITERATIONS = 10000
		APMO_instance%SCF_ELECTRONIC_MAX_ITERATIONS = 20000
		APMO_instance%SCF_INTERSPECIES_MAXIMUM_ITERATIONS = 5000
		APMO_instance%SCF_MAX_ITERATIONS=20000
		APMO_instance%DOUBLE_ZERO_THRESHOLD  = 1.0E-12
		APMO_instance%CONVERGENCE_METHOD = 1 !!(0) NONE, (1) DAMPING, (2) DIIS, (3) LEVEL SHIFTING (4) DAMPING/DIIS
		APMO_instance%DIIS_SWITCH_THRESHOLD = 0.5
		APMO_instance%DIIS_SWITCH_THRESHOLD_BKP = 0.5
		APMO_instance%SCF_ELECTRONIC_TYPE_GUESS = "hcore"
		APMO_instance%SCF_NONELECTRONIC_TYPE_GUESS = "ones"
		APMO_instance%SCF_CONVERGENCE_CRITERIUM ="density"
		APMO_instance%DIIS_DIMENSIONALITY = 10
		APMO_instance%ITERATION_SCHEME = 1 !!(0) NONELECRONIC FULLY / e- (1) ELECTRONIC FULLY (2) CONVERGED INDIVIDIALLY (3) SCHEMESIMULTANEOUS
		APMO_instance%EXTERNAL_POTENTIAL="NONE"
		APMO_instance%DIIS_ERROR_IN_DAMPING = .false.
		APMO_instance%MAXIMUM_RANGE_OF_GRAPHS=100
		APMO_instance%STEP_OF_GRAPHS=0.01
		APMO_instance%WAVE_FUNCTION_SCALE= 1000.0
		APMO_instance%GRAPH_FOR_NUM_ORBITAL=1
		APMO_instance%READ_COEFFICIENTS = .true.
		APMO_instance%LISTS_SIZE = -20
		!!***************************************************************************

		!!***************************************************************************
		!! Parametros para control de proceso de minimizacion multidimensional
		!! 
		APMO_instance%NUMERICAL_DERIVATIVE_DELTA = 1.0E-3
		APMO_instance%MINIMIZATION_INITIAL_STEP_SIZE = 0.1_8
		APMO_instance%MINIMIZATION_LINE_TOLERANCE = 0.1_8
		APMO_instance%MINIMIZATION_TOLERANCE_GRADIENT = 1.0E-5
		APMO_instance%MINIMIZATION_MAX_ITERATION = 100
		APMO_instance%MINIMIZATION_LIBRARY = "GENERIC"
		APMO_instance%COORDINATES = "cartesian"
        APMO_instance%ENERGY_CALCULATOR = "internal"
		APMO_instance%MINIMIZATION_METHOD = "TR"
		APMO_instance%ANALYTIC_GRADIENT = .true.
		APMO_instance%MINIMIZATION_WITH_SINGLE_POINT = .true.
		APMO_instance%USE_SYMMETRY_IN_MATRICES =.false.
		APMO_instance%RESTART_OPTIMIZATION = .false.
        APMO_instance%OPTIMIZE_WITH_CP_CORRECTION = .false.
		APMO_instance%CP_CORRECTION = .false.
		!!***************************************************************************

		!!***************************************************************************
		!! Criterios de conectividad atomica
		!! 
		APMO_instance%BOND_DISTANCE_FACTOR = 1.3_8
		APMO_instance%BOND_ANGLE_THRESHOLD = 170.0_8
		APMO_instance%DIHEDRAL_ANGLE_THRESHOLD  = 170.0_8
		!!***************************************************************************


		!!*****************************************************
		!! Control de parametros de teoria de pertubaciones
		!!
		APMO_instance%MP_FROZEN_CORE_BOUNDARY = 1
		APMO_instance%MP_ONLY_ELECTRONIC_CORRECTION =.false.
		!!
		!!*****************************************************
		
		!!***************************************************************************
		!! Variables de ambiente al sistema de archivos del programa 
		!! 
		APMO_instance%HOME_DIRECTORY = APMO_getHomeDirectory()	!< Almacena directorio para raiz del programa
		APMO_instance%DATA_DIRECTORY = APMO_getDataDirectory()	!< Almacena directorio raiz de bases de datos
        APMO_instance%EXTERNAL_COMMAND = APMO_getExternalCommand()  !< Almacena el comando empleado para ejecutar gamess
        APMO_instance%EXTERNAL_SOFTWARE_NAME = APMO_getExternalSoftwareName()
		APMO_instance%ATOMIC_ELEMENTS_DATABASE = "/dataBases/atomicElements.xml"
		APMO_instance%BASIS_SET_DATABASE = "/basis/"
		APMO_instance%POTENTIALS_DATABASE= "/potentials/"
		APMO_instance%ELEMENTAL_PARTICLES_DATABASE = "/dataBases/elementalParticles.xml"
		APMO_instance%INPUT_FILE = ""
		!!***************************************************************************

		!!*****************************************************
		!! Control parametros de formato
		!!
		APMO_instance%FORMAT_NUMBER_OF_COLUMNS = 5
		APMO_instance%UNITS="ANGSTROMS"
		APMO_instance%UNID_FOR_OUTPUT_FILE = 6
		APMO_instance%UNID_FOR_MP2_INTEGRALS_FILE = 7
		APMO_instance%UNID_FOR_MOLECULAR_ORBITALS_FILE = 8
		!!
		!!*****************************************************
				
		!!*****************************************************
		!! Tareas que deben realizarse
		!!
		APMO_instance%METHOD = "undefined"
		APMO_instance%MOLLER_PLESSET_CORRECTION = 1
		APMO_instance%OPTIMIZE = .false.
		APMO_instance%OPTIMIZE_WITH_MP = .false.
		APMO_instance%ONLY_ELECTRONIC_EFFECT =.false.
		APMO_instance%PROJECT_HESSIANE = .true.
		APMO_instance%ELECTRONIC_WAVEFUNCTION_ANALYSIS = .false.
		APMO_instance%FROZEN_PARTICLE = ""
		!!
		!!*****************************************************
		
		!!*****************************************************
		!! Impresion en proceso de depuracion
		!!
		APMO_instance%DEBUG_SCFS = .false.
		!!
		!!*****************************************************

		!!*****************************************************
		!! Parametros de control general
		!!
		APMO_instance%REMOVE_TRANSLATIONAL_CONTAMINATION = .false.
		APMO_instance%TRANSFORM_TO_CENTER_OF_MASS = .true.
		APMO_instance%ARE_THERE_DUMMY_ATOMS = .false.
		APMO_instance%IS_THERE_EXTERNAL_POTENTIAL=.false.
		APMO_instance%IS_THERE_FROZEN_PARTICLE=.false.
 		APMO_instance%APMOLPRO=.false.
		!!
		!!*****************************************************
		
		instanced= .true.
		
	end subroutine APMO_constructor

	subroutine APMO_copyConstructor(this, otherThis )
		implicit none
		type(APMO) :: this
		type(APMO) :: otherThis

		this%TV = otherthis%TV
		this%INTEGRAL_THRESHOLD = otherthis%INTEGRAL_THRESHOLD
		this%INTEGRAL_METHOD = otherthis%INTEGRAL_METHOD
		this%SCF_NONELECTRONIC_ENERGY_TOLERANCE = otherthis%SCF_NONELECTRONIC_ENERGY_TOLERANCE
		this%SCF_ELECTRONIC_ENERGY_TOLERANCE = otherthis%SCF_ELECTRONIC_ENERGY_TOLERANCE
		this%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE = otherthis%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE
		this%ELECTRONIC_DENSITY_MATRIX_TOLERANCE = otherthis%ELECTRONIC_DENSITY_MATRIX_TOLERANCE
		this%TOTAL_ENERGY_TOLERANCE = otherthis%TOTAL_ENERGY_TOLERANCE
		this%STRONG_ENERGY_TOLERANCE = otherthis%STRONG_ENERGY_TOLERANCE
		this%DENSITY_FACTOR_THRESHOLD = otherthis%DENSITY_FACTOR_THRESHOLD
		this%SCF_NONELECTRONIC_MAX_ITERATIONS = otherthis%SCF_NONELECTRONIC_MAX_ITERATIONS
		this%SCF_ELECTRONIC_MAX_ITERATIONS = otherthis%SCF_ELECTRONIC_MAX_ITERATIONS
		this%SCF_MAX_ITERATIONS = otherthis%SCF_MAX_ITERATIONS
		this%SCF_INTERSPECIES_MAXIMUM_ITERATIONS = otherthis%SCF_INTERSPECIES_MAXIMUM_ITERATIONS
		this%LISTS_SIZE = otherthis%LISTS_SIZE
		this%DOUBLE_ZERO_THRESHOLD = otherthis%DOUBLE_ZERO_THRESHOLD
		this%DIIS_SWITCH_THRESHOLD = otherthis%DIIS_SWITCH_THRESHOLD
		this%DIIS_SWITCH_THRESHOLD_BKP = otherthis%DIIS_SWITCH_THRESHOLD_BKP
		this%CONVERGENCE_METHOD = otherthis%CONVERGENCE_METHOD
		this%SCF_ELECTRONIC_TYPE_GUESS = trim( otherthis%SCF_ELECTRONIC_TYPE_GUESS )
		this%SCF_NONELECTRONIC_TYPE_GUESS = trim ( otherthis%SCF_NONELECTRONIC_TYPE_GUESS )
		this%SCF_CONVERGENCE_CRITERIUM = trim( otherthis%SCF_CONVERGENCE_CRITERIUM )
		this%DIIS_DIMENSIONALITY = otherthis%DIIS_DIMENSIONALITY
		this%ITERATION_SCHEME = otherthis%ITERATION_SCHEME
		this%EXTERNAL_POTENTIAL= trim(otherthis%EXTERNAL_POTENTIAL)
		this%DIIS_ERROR_IN_DAMPING = otherthis%DIIS_ERROR_IN_DAMPING
		this%MAXIMUM_RANGE_OF_GRAPHS=otherthis%MAXIMUM_RANGE_OF_GRAPHS
		this%STEP_OF_GRAPHS=otherthis%STEP_OF_GRAPHS
		this%WAVE_FUNCTION_SCALE=otherthis%WAVE_FUNCTION_SCALE
		this%GRAPH_FOR_NUM_ORBITAL=otherthis%GRAPH_FOR_NUM_ORBITAL
		this%READ_COEFFICIENTS = otherthis%READ_COEFFICIENTS
		this%NUMERICAL_DERIVATIVE_DELTA = otherthis%NUMERICAL_DERIVATIVE_DELTA
		this%MINIMIZATION_INITIAL_STEP_SIZE = otherthis%MINIMIZATION_INITIAL_STEP_SIZE
		this%MINIMIZATION_LINE_TOLERANCE = otherthis%MINIMIZATION_LINE_TOLERANCE
		this%MINIMIZATION_TOLERANCE_GRADIENT = otherthis%MINIMIZATION_TOLERANCE_GRADIENT
		this%MINIMIZATION_MAX_ITERATION = otherthis%MINIMIZATION_MAX_ITERATION
		this%MINIMIZATION_METHOD = trim( otherthis%MINIMIZATION_METHOD )
		this%MINIMIZATION_LIBRARY = trim( otherthis%MINIMIZATION_LIBRARY )
		this%COORDINATES = trim( otherthis%COORDINATES )
        this%ENERGY_CALCULATOR = trim( otherthis%ENERGY_CALCULATOR)
		this%ANALYTIC_GRADIENT = otherthis%ANALYTIC_GRADIENT
		this%MINIMIZATION_WITH_SINGLE_POINT = otherthis%MINIMIZATION_WITH_SINGLE_POINT
		this%USE_SYMMETRY_IN_MATRICES = otherthis%USE_SYMMETRY_IN_MATRICES
		this%RESTART_OPTIMIZATION=otherthis%RESTART_OPTIMIZATION
        this%OPTIMIZE_WITH_CP_CORRECTION=otherthis%OPTIMIZE_WITH_CP_CORRECTION
		this%CP_CORRECTION=otherthis%CP_CORRECTION
		this%BOND_DISTANCE_FACTOR = otherthis%BOND_DISTANCE_FACTOR
		this%BOND_ANGLE_THRESHOLD = otherthis%BOND_ANGLE_THRESHOLD
		this%DIHEDRAL_ANGLE_THRESHOLD = otherthis%DIHEDRAL_ANGLE_THRESHOLD
		this%MP_FROZEN_CORE_BOUNDARY = otherthis%MP_FROZEN_CORE_BOUNDARY
		this%MP_ONLY_ELECTRONIC_CORRECTION = otherthis%MP_ONLY_ELECTRONIC_CORRECTION
		this%HOME_DIRECTORY = trim( otherthis%HOME_DIRECTORY )
		this%DATA_DIRECTORY = trim( otherthis%DATA_DIRECTORY )
        this%EXTERNAL_COMMAND = trim( otherthis%EXTERNAL_COMMAND )
		this%ATOMIC_ELEMENTS_DATABASE = trim( otherthis%ATOMIC_ELEMENTS_DATABASE )
		this%BASIS_SET_DATABASE = trim( otherthis%BASIS_SET_DATABASE )
		this%POTENTIALS_DATABASE = trim(otherthis%POTENTIALS_DATABASE)
		this%ELEMENTAL_PARTICLES_DATABASE = trim( otherthis%ELEMENTAL_PARTICLES_DATABASE )
		this%INPUT_FILE = trim( otherthis%INPUT_FILE )
		this%FORMAT_NUMBER_OF_COLUMNS = otherthis%FORMAT_NUMBER_OF_COLUMNS
		this%UNITS = trim( otherthis%UNITS )
		this%UNID_FOR_OUTPUT_FILE = otherthis%UNID_FOR_OUTPUT_FILE
		this%UNID_FOR_MP2_INTEGRALS_FILE = otherthis%UNID_FOR_MP2_INTEGRALS_FILE
		this%UNID_FOR_MOLECULAR_ORBITALS_FILE = otherthis%UNID_FOR_MOLECULAR_ORBITALS_FILE
		this%METHOD = trim( otherthis%METHOD )
		this%MOLLER_PLESSET_CORRECTION = otherthis%MOLLER_PLESSET_CORRECTION
		this%OPTIMIZE = otherthis%OPTIMIZE
		this%OPTIMIZE_WITH_MP = otherthis%OPTIMIZE_WITH_MP
		this%ONLY_ELECTRONIC_EFFECT = otherthis%ONLY_ELECTRONIC_EFFECT
		this%PROJECT_HESSIANE=otherthis%PROJECT_HESSIANE
		this%ELECTRONIC_WAVEFUNCTION_ANALYSIS=otherthis%ELECTRONIC_WAVEFUNCTION_ANALYSIS
		this%DEBUG_SCFS = otherthis%DEBUG_SCFS
		this%REMOVE_TRANSLATIONAL_CONTAMINATION = otherthis%REMOVE_TRANSLATIONAL_CONTAMINATION
		this%TRANSFORM_TO_CENTER_OF_MASS = otherthis%TRANSFORM_TO_CENTER_OF_MASS
		this%ARE_THERE_DUMMY_ATOMS = otherthis%ARE_THERE_DUMMY_ATOMS
		this%IS_THERE_EXTERNAL_POTENTIAL = otherthis%IS_THERE_EXTERNAL_POTENTIAL
		this%FROZEN_PARTICLE = otherthis%FROZEN_PARTICLE
		this%IS_THERE_FROZEN_PARTICLE=otherthis%IS_THERE_FROZEN_PARTICLE
		this%APMOLPRO = otherthis%APMOLPRO

	end subroutine
	
	!**
	!  Destructor
	!**
	subroutine APMO_destructor()
		implicit none
		
	end subroutine APMO_destructor

	!**
	! Imprime en consola creditos e informaci�n de la implementaci�n.
	!**
	subroutine APMO_showCredits()
		implicit none
		
		print *,"***************************************************************"
		print *,"*    APMO FORTRAN IMPLEMENTATION VERSION=1.5  January/2011    *"
		print *,"***************************************************************"
		print *,"	GQT-UNIVERSIDAD NACIONAL DE COLOMBIA"
		print *," 		S.A.GONZALEZ"
		print *," 		N.F.AGUIRRE"
		print *," 		E.F.POSADA (efposadac@unal.edu.co)"
		print *,"***************************************************************"
		print *,""
		
	end subroutine APMO_showCredits

	!>
	!! @brief Imprime los parametros empleados en ciclos SCF
	!<
	subroutine APMO_showParameters()
		implicit none

                integer :: i

		print *,""
		print *,"APMO IS RUNNING WITH NEXT PARAMETERS: "
		print *,"------------------------------------"
		print *,""
		write (*,"(T10,A)") "METHOD TYPE:  "//trim(APMO_instance%METHOD)
		if(APMO_instance%MOLLER_PLESSET_CORRECTION>=2) &
			write (*,"(T10,A,I)") "MOLLER PLESSET CORRECTION:  ",APMO_instance%MOLLER_PLESSET_CORRECTION
		if (APMO_instance%OPTIMIZE) then
			write (*,"(T10,A)") "GEOMETRY OPTIMIZATION:  T"
			write (*,"(T10,A)") "OPTIMIZATION METHOD: "//trim(APMO_instance%MINIMIZATION_METHOD)
			write (*,"(T10,A,ES15.5)") "GRADIENT THRESHOLD FOR THE MINIMIZATION: ",APMO_instance%MINIMIZATION_TOLERANCE_GRADIENT
			if(APMO_instance%MOLLER_PLESSET_CORRECTION>=2)APMO_instance%ANALYTIC_GRADIENT=.false.
			if( APMO_instance%ANALYTIC_GRADIENT) then
				write (*,"(T10,A)") "TYPE OF GRADIENT : ANALYTIC"
			else
				write (*,"(T10,A)") "TYPE OF GRADIENT : NUMERIC"
				write (*,"(T10,A,E15.5)") "NUMERICAL STEP: ",APMO_instance%NUMERICAL_DERIVATIVE_DELTA
			end if
		else
			write (*,"(T10,A,A)") "SINGLE POINT CALCULATION"
		end if
		write (*,"(T10,A,E15.5)") "NONELECTRONIC ENERGY TOLERANCE IN SCFs: ",APMO_instance%SCF_NONELECTRONIC_ENERGY_TOLERANCE
		write (*,"(T10,A,E15.5)") "NONELECTRONIC DENSITY MATRIX TOLERANCE IN SCFs: ",APMO_instance%NONELECTRONIC_DENSITY_MATRIX_TOLERANCE
		write (*,"(T10,A,E15.5)") "ELECTRONIC ENERGY TOLERANCE IN SCFs: ",APMO_instance%SCF_ELECTRONIC_ENERGY_TOLERANCE
		write (*,"(T10,A,E15.5)") "ELECTRONIC DENSITY MATRIX TOLERANCE IN SCFs: ",APMO_instance%ELECTRONIC_DENSITY_MATRIX_TOLERANCE
		write (*,"(T10,A,E15.5)") "TOTAL ENERGY TOLERANCE IN SCFs: ",APMO_instance%TOTAL_ENERGY_TOLERANCE
		write (*,"(T10,A,I)") "SCF MAX. ITERATIONS - NONELECTRONICS : ",APMO_instance%SCF_NONELECTRONIC_MAX_ITERATIONS
		write (*,"(T10,A,I)") "SCF MAX. ITERATIONS - ELECTRONICS : ",APMO_instance%SCF_ELECTRONIC_MAX_ITERATIONS
		write (*,"(T10,A,I)") "SCF MAX. ITERATIONS - INTERSPECIES : ",APMO_instance%SCF_INTERSPECIES_MAXIMUM_ITERATIONS
		write (*,"(T10,A)") "CRITERIUM OF CONVERGENCE: "//trim(APMO_instance%SCF_CONVERGENCE_CRITERIUM)
		write (*,"(T10,A)") "NONELECTRONIC DENSITY GUESS: "//trim(APMO_instance%SCF_NONELECTRONIC_TYPE_GUESS)
		write (*,"(T10,A)") "ELECTRONIC DENSITY GUESS: "//trim(APMO_instance%SCF_ELECTRONIC_TYPE_GUESS)
		write (*,"(T10,A,I)") "SCHEME OF ITERATION: ",APMO_instance%ITERATION_SCHEME
		write (*,"(T10,A)") "EXTERNAL POTENTIAL: "//trim(APMO_instance%EXTERNAL_POTENTIAL)
		
		select case(APMO_instance%CONVERGENCE_METHOD)

			case(1)
				write(*,"(T10,A)") "METHOD OF SCF CONVERGENCE: DAMPING"
			case(2)
				write(*,"(T10,A)") "METHOD OF SCF CONVERGENCE: DIIS"
				write(*,"(T10,A,E15.5)") "DIIS SWITCH THRESHOLD", APMO_instance%DIIS_SWITCH_THRESHOLD
				write(*,"(T10,A,I)") "DIIS DIMENSIONALITY: ", APMO_instance%DIIS_DIMENSIONALITY
			case(4)
				write(*,"(T10,A)") "METHOD OF SCF CONVERGENCE: DAMPING/DIIS"
				write(*,"(T10,A,E15.5)") "DIIS SWITCH THRESHOLD", APMO_instance%DIIS_SWITCH_THRESHOLD
				write(*,"(T10,A,I)") "DIIS DIMENSIONALITY: ", APMO_instance%DIIS_DIMENSIONALITY

		end select

	end subroutine APMO_showParameters

	
	!**
	!  @brief retorna la ruta al directorio raiz de APMO
	!**
	function APMO_getHomeDirectory() result ( directory )
		implicit none
		character(255) :: directory
		
		call getenv( "APMO_HOME", directory )
		
		directory = trim( directory )
		
	end function APMO_getHomeDirectory
	
	!**
	!  @brief retorna la ruta al directorio que contiene
	!         todos los archivos de configuraci'on y base
	!         de datos de APMO
	!**
	function APMO_getDataDirectory() result ( directory )
		implicit none
		
		character(255) :: directory
		
		call getenv( "APMO_DATA", directory )
		
		directory = trim( directory )
		
	end function APMO_getDataDirectory


        !**
	!  @brief retorna la ruta al directorio raiz de APMO
	!**
	function APMO_getExternalCommand() result ( output )
		implicit none
		character(255) :: output
		
		call getenv( "EXTERNAL_COMMAND", output )
		
		output = trim( output )
		
	end function APMO_getExternalCommand


        !**
	!  @brief retorna la ruta al directorio raiz de APMO
	!**
	function APMO_getExternalSoftwareName() result ( output )
		implicit none
		character(255) :: output
		
		call getenv( "EXTERNAL_SOFTWARE_NAME", output )
		
		output = trim( output )
		
	end function APMO_getExternalSoftwareName

	
end module APMO_
