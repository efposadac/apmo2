!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
module R12Integrals_
	use Exception_
	use, intrinsic :: iso_c_binding
	implicit none

	!>
	!! @brief Description
	!!
	!! @author fernando
	!!
	!! <b> Creation data : </b> 10-12-10
	!!
	!! <b> History change: </b>
	!!
	!!   - <tt> 10-12-10 </tt>:  fernando ( email@server )
	!!        -# description.
	!!   - <tt> MM-DD-YYYY </tt>:  authorOfChange ( email@server )
	!!        -# description
	!!
	!<
	type, public :: R12Integrals
		character(20) :: name
		logical :: isInstanced
	end type

	type, bind(c), public :: contr_data
		real(c_double)  :: AB(3)
		real(c_double)  :: CD(3)
		real(c_double)  :: AC(3)
		real(c_double)  :: ABdotAC, CDdotCA
	end type contr_data

	public :: &
		R12Integrals_constructor, &
		R12Integrals_destructor, &
		R12Integrals_show
		
private		
contains


	!>
	!! @brief Constructor por omision
	!!
	!! @param this
	!<
	subroutine R12Integrals_constructor(this)
		implicit none
		type(R12Integrals) :: this

		this.isInstanced = .true.

	end subroutine R12Integrals_constructor


	!>
	!! @brief Destructor por omision
	!!
	!! @param this
	!<
	subroutine R12Integrals_destructor(this)
		implicit none
		type(R12Integrals) :: this

		this.isInstanced = .false.

	end subroutine R12Integrals_destructor

	!>
	!! @brief Muestra informacion del objeto
	!!
	!! @param this 
	!<
	subroutine R12Integrals_show(this)
		implicit none
		type(R12Integrals) :: this
	end subroutine R12Integrals_show

	!!>
	!! @brief Indica si el objeto ha sido instanciado o no
	!!
	!<
	function R12Integrals_isInstanced( this ) result( output )
		implicit  none
		type(R12Integrals), intent(in) :: this
		logical :: output
		
		output = this.isInstanced
	
	end function R12Integrals_isInstanced

	!>
	!! @brief  Maneja excepciones de la clase
	!<
	subroutine R12Integrals_exception( typeMessage, description, debugDescription)
		implicit none
		integer :: typeMessage
		character(*) :: description
		character(*) :: debugDescription
	
		type(Exception) :: ex

		call Exception_constructor( ex , typeMessage )
		call Exception_setDebugDescription( ex, debugDescription )
		call Exception_setDescription( ex, description )
		call Exception_show( ex )
		call Exception_destructor( ex )
	
	end subroutine R12Integrals_exception

end module R12Integrals_
