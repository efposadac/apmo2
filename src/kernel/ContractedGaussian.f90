!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! @brief   Modulo para calculo de integrales entre contracciones de funciones gausianas 
!
! Este modulo define una seudoclase para la construccion de contracciones de funciones 
! gausianas primitivas. Ademas incluye los metodos necesarios para el calculos de 
! integrales moleculares entre las contracciones definidas. Las funciones contraidas
! tienen la siguiente forma:
!
! \f[ \chi(\bf{r-R_i}) = \sum_{i=1}^{L} {C_i{\phi (\bf{r;n_i},{\zeta}_i ,\bf{R_i})}} \f]
!
! y la integral de una particula asociada:
!
! \f[  \int_{TE}{ {\chi}^{*} \hat{O} \chi } \f]
! Donde:
!
! <table>
! <tr> <td> \f$ \chi \f$ : <td> <dfn> gausiana contraida. </dfn>
! <tr> <td> <b> L </b> : <td><dfn> Longitud de la contraccion. </dfn> 
! <tr> <td> \f$ C_i \f$ :<td> <dfn> Coeficiente de contraccion de la i-enesima primitiva.</dfn>
! <tr> <td> \f$ R_i \f$ :<td> <dfn> Origen de la  i-enesima gausiana primitiva.</dfn>
! <tr> <td> \f$ n_i \f$ :<td> <dfn> Indice de momento anngular de la  i-enesima primitiva.</dfn>
! <tr> <td> \f$ {\zeta}_i \f$ : <td> <dfn> Exponente orbital de la  i-enesima primitiva.</dfn>
! </table> 
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion : </b> 2007-02-06
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-01-06 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Propuso estandar de codificacion.
!   - <tt> 2007-07-12 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Se adapta al estandar de codificacion propuesto.
!	- <tt> 2010-09-25 </tt>: Edwin F. Posada C. (efposadac@unal.edu.co)
!		 -# Cambia de indices de momento angular a capas, amplía hasta momento angular "L".
!
! @see PrimitiveGaussian_
!**
module ContractedGaussian_
	use APMO_
	use Exception_
	use PrimitiveGaussian_
	use OverlapIntegrals_
	use KineticIntegrals_
	use MomentIntegrals_
	use AngularMomentIntegrals_
	use AttractionIntegrals_
	use RepulsionIntegrals_
	use LibintInterface_
	use OverlapDerivatives_
	use KineticDerivatives_
	use AttractionDerivatives_
	use RepulsionDerivatives_
	use String_
	

	implicit none
	
	type, public :: ContractedGaussian
		character(30) :: name
		type(PrimitiveGaussian) , allocatable :: primitives(:)
		real(8) , allocatable :: contractionCoefficients(:)
		real(8)    origin(3)
		integer(8) :: angularMomentIndex(3)
		integer(8) :: angularMoment
		real(8), allocatable :: normalizationConstant(:)
		integer :: numCartesianOrbital
		integer :: owner
		integer ::  length
		logical :: isVectorized								!< Indica si usa o no indice de momento angular
	end type ContractedGaussian
	
	public :: &
		ContractedGaussian_constructor,  	&
		ContractedGaussian_destructor,  	&
		ContractedGaussian_copyConstructor,	&
		ContractedGaussian_show,		&
		ContractedGaussian_showInCompactForm,	&
		ContractedGaussian_showInSimpleForm, 	&
		ContractedGaussian_showStructure,	&
		ContractedGaussian_getLength, 		&
		ContractedGaussian_getContraction,	&
		ContractedGaussian_getNumberOfCartesianOrbitals, &
		ContractedGaussian_getAngularMomentIndex,		&
		ContractedGaussian_getOwner,		&
		ContractedGaussian_getShellCode,	&
		ContractedGaussian_getName,		&
		ContractedGaussian_set,			&
		ContractedGaussian_getValueAt,		&
		ContractedGaussian_overlapIntegral,	&
		ContractedGaussian_overlapDerivative,	&
		ContractedGaussian_momentIntegral,	&
		ContractedGaussian_momentumIntegral,	&
		ContractedGaussian_kineticIntegral,	&
		ContractedGaussian_kineticDerivative,	&
		ContractedGaussian_attractionIntegral,	&
		ContractedGaussian_attractionDerivative,&
		ContractedGaussian_repulsionIntegral,	&
		ContractedGaussian_repulsionDerivative,	&
		ContractedGaussian_coulombOperator,     &
		ContractedGaussian_product
		
	private :: &
		ContractedGaussian_normalizeShell,&
		ContractedGaussian_normalizeContraction
	
contains

	!**
	! Constructor para la clase ContractedGaussian
	!	
	! @param this Gausiana contraida
	! @param orbitalsExponents Exponentes orbitales
	! @param contractionCoefficients Coeficientes de construccion
	! @param origin Origen de la contraccion
	! @param angularMomentIndex Indice de momento angular.
	!
	! @warning  El mumero de coeficientes de contraccion debe ser 
	!	    igual al Numero de exponentes orbitales.
	!**
	subroutine ContractedGaussian_constructor( this , orbitalsExponents , &
		contractionCoefficients , origin , angularMoment, angularMomentIndex, owner, name, ssize, noNormalize )
		implicit none
		
		type(ContractedGaussian) , intent(inout) :: this
		real(8), optional, intent(in) :: orbitalsExponents(:)
		real(8), optional , intent(in) :: contractionCoefficients(:)
		real(8), optional , intent(in) :: origin(3)
		integer(8),optional, intent(in) :: angularMoment
		integer(8),optional, intent(in) :: angularMomentIndex(3)
		integer, optional, intent(in) :: owner
		character(30), optional, intent(in) :: name
		integer, optional :: ssize
		logical, optional, intent(in) :: noNormalize
		
		integer :: i
		real(8) :: auxExponent
		
		!!******************************************************
		!! Genera e inicializa vector de primitivas
		!!
		if ( present(ssize) .and. .not. present(orbitalsExponents)) then

			this%length=ssize

		else if( present(orbitalsExponents) ) then

			this%length = size(orbitalsExponents)

		end if

		allocate( this%primitives( this%length ) )
		allocate( this%contractionCoefficients( this%length ) )
		
		this%isVectorized = .false.
		this%contractionCoefficients = 1.0_8
		this%angularMomentIndex=[0_8, 0_8, 0_8]
		this%angularMoment=0_8
		this%numCartesianOrbital=0
		this%owner = 0
		this%name =""
		this%origin = [0.0_8,0.0_8,0.0_8]
	
		if ( present( origin ) ) then
			this%origin = origin
		end if
		
		if ( present( angularMoment ) ) then
			this%angularMoment = angularMoment
		end if

		this%angularMomentIndex(1) = this%angularMoment

		if ( present( angularMomentIndex ) ) then
			this%angularMomentIndex = angularMomentIndex
			this%isVectorized = .true.
		end if
		
		if ( present( contractionCoefficients ) ) then
			this%contractionCoefficients = contractionCoefficients
		end if
		
		if (present( owner ) ) then
			this%owner = owner
		end if
		
		if (present( name ) ) then
			this%name = trim(name)
		end if
		
		this%numCartesianOrbital = ( ( this%angularMoment + 1_8 )*( this%angularMoment + 2_8 ) ) / 2_8

		allocate(this%normalizationConstant( this%numCartesianOrbital ))
		this%normalizationConstant = 1.0_8

		!! Inicializa cada primitiva de la contraccion.
		do i = 1 , this%length
			auxExponent=1.0_8
			if(present(orbitalsExponents)) auxExponent=orbitalsExponents(i)
				if(this%isVectorized) then
			        if(.not. present(noNormalize) ) then
				      call PrimitiveGaussian_constructor( this%primitives(i) , &
						this%origin, auxExponent , &
						angularMomentIndex=this%angularMomentIndex, numCartesianOrbital=this%numCartesianOrbital)
					else
				       call PrimitiveGaussian_constructor( this%primitives(i) , &
						this%origin, auxExponent , &
						angularMomentIndex=this%angularMomentIndex, numCartesianOrbital=this%numCartesianOrbital, noNormalize=.true. )
					end if
				else
			        if(.not. present(noNormalize) ) then
				      call PrimitiveGaussian_constructor( this%primitives(i) , &
						this%origin, auxExponent , &
						angularMoment=this%angularMoment, numCartesianOrbital=this%numCartesianOrbital)
					else
				       call PrimitiveGaussian_constructor( this%primitives(i) , &
						this%origin, auxExponent , &
						angularMoment=this%angularMoment, numCartesianOrbital=this%numCartesianOrbital, noNormalize=.true. )
					end if
				end if
		end do
		!!******************************************************
		!! Normaliza la contraccion. dependiendo de si se usa un indice de momento angular o un momento angular
		if(.not. present(noNormalize) ) then
			if (this%isVectorized) then
				call ContractedGaussian_normalizeContraction(this)
			else
				call ContractedGaussian_normalizeShell(this)
			end if
		end if

	end subroutine ContractedGaussian_constructor
	
	!**
	! @brief Define el constructor de copia de la clase.
	!**
	subroutine ContractedGaussian_copyConstructor( this , otherThis )
		implicit none
		type(ContractedGaussian) , intent(inout) :: this
		type(ContractedGaussian) , intent(in) :: otherThis
		
		integer :: i
		
		if ( allocated(otherthis%contractionCoefficients ) ) then
		
			this%origin = otherthis%origin
			this%angularMoment = otherthis%angularMoment
			this%angularMomentIndex = otherthis%angularMomentIndex
			this%normalizationConstant = otherthis%normalizationConstant
			this%numCartesianOrbital = otherThis%numCartesianOrbital
			this%owner = otherthis%owner
			this%length = otherthis%length
			this%name = trim(otherthis%name)
			this%isVectorized = otherThis%isVectorized
			
			if ( allocated( this%contractionCoefficients ) ) deallocate( this%contractionCoefficients)
				allocate( this%contractionCoefficients( size(otherthis%contractionCoefficients) ) )
			
			if ( allocated( this%primitives ) ) deallocate( this%primitives)
				allocate( this%primitives( size(otherthis%contractionCoefficients) ) )
			
			do i= 1, size(otherthis%contractionCoefficients)
				
				this%contractionCoefficients(i) = otherthis%contractionCoefficients(i)
				
				call PrimitiveGaussian_copyConstructor( this%primitives(i), otherthis%primitives(i) )
				
			end do
			
		end if
		
		
	end subroutine ContractedGaussian_copyConstructor
	
	
	!**
	! Define el destructor de la clase.
	!**
	subroutine ContractedGaussian_destructor( this )
		implicit none
		
		type(ContractedGaussian) , intent(inout) :: this
		
		if ( allocated( this%primitives ) ) then
			deallocate( this%primitives )
		end if
		
		if ( allocated(this%contractionCoefficients) ) then
			deallocate( this%contractionCoefficients )
		end if
		
		if ( allocated(this%normalizationConstant) ) then
			deallocate (this%normalizationConstant)
		end if

	end subroutine ContractedGaussian_destructor
	
	!**
	! Muestra en pantalla el valor de los atributos asociados a la gausiana
	! contraida solicitada, En caso de indicarse una de sus gausianas primitivas,
	! muestra informacion sobre dicha primitiva.
	!
	! @param this  Gausiana primitiva
	!
	! @warning Se asume que el origen de la contraci�n es igual para todas
	!	   las primitivas por los que solo se muestra el la primera.
	!**
	subroutine ContractedGaussian_show( this, primitiveNumber )
		implicit none
		
		type(ContractedGaussian) , intent(in) :: this
		integer , optional , intent(in) :: primitiveNumber
		
		integer :: i
		
		!! Mustra informacion sobre una primitiva.
		if ( present( primitiveNumber ) ) then
			call PrimitiveGaussian_show( this%primitives( primitiveNumber ) )
		else
			!!**************************************************************
			!! Muestra informacion de la gausiana especificada
			!!
			
			!! Imprime en la salida estandar
			print *,"=============================="
			print *," Contracted gaussian function:", trim( this%name )
			print *,"=============================="
			write (6,*) "Origin: ", this%origin( 1 ) , this%origin( 2 ) , this%origin( 3 )
			if (this%isVectorized) then
				write (6,200) this%angularMomentIndex(1), this%angularMomentIndex(2), this%angularMomentIndex(3)
200				FORMAT (' AngularMomentIndex(n):',I10.1,I10.1,I10.1)
			else
				write (6,201) this%angularMoment
201				FORMAT (' AngularMoment(n):',I10.1)
			end if
			print *,"Normalization Constant(N):",this%normalizationConstant
			print *,"Owner Center:          ",this%owner
			print *,"Length Contraction:",size(this%contractionCoefficients)
			print *,""
			print *,"Contraction Coefficients:"
			print *,"-------------------------"
			write (6,"(<this%length>F10.4)") (this%contractionCoefficients(i),i=1,this%length)
			print *,""
			
			!!**************************************************************
		end if
	end subroutine ContractedGaussian_show
	
		!**
	! Muestra en pantalla el valor de los atributos asociados a la gausiana
	! contraida solicitada, En caso de indicarse una de sus gausianas primitivas,
	! muestra informacion sobre dicha primitiva.
	!
	! @param this  Gausiana primitiva
	!
	! @warning Se asume que el origen de la contracion es igual para todas
	!	   las primitivas por los que solo se muestra el la primera.
	!**
	subroutine ContractedGaussian_showInCompactForm( this)
		implicit none
		
		type(ContractedGaussian) , intent(in) :: this
		
		integer :: i
		character(9) :: shellCode(this%numCartesianOrbital)
		
		shellCode=ContractedGaussian_getShellCode(this)

			do i=1,this%length

			write (6,"(T10,I5,A9,A1,A6,F20.8,F20.8)") i,"        ", trim(shellCode(1)),"      ",&
								this%primitives(i)%orbitalExponent ,this%contractionCoefficients(i)
			end do

			write (6,*) ""
		
	end subroutine ContractedGaussian_showInCompactForm


	subroutine ContractedGaussian_showInSimpleForm( this, unidOfOutput)
		implicit none
		type(ContractedGaussian) , intent(in) :: this
		integer :: unidOfOutput
		
		integer :: i
		character(9) :: shellCode(this%numCartesianOrbital)
		
		shellCode=ContractedGaussian_getShellCode(this)

			write (unidOfOutput,"(A1,I3,F5.2)") trim(String_getLowercase(shellCode(1)(1:1))),this%length,1.00

			do i=1,this%length
				write (unidOfOutput,"(ES19.10,ES19.10)") this%primitives(i)%orbitalExponent ,this%contractionCoefficients(i)
			end do
		
	end subroutine ContractedGaussian_showInSimpleForm
	
	!**
	! @ brief Imprime la estructura completa de la contraccion
	!**
	subroutine ContractedGaussian_showStructure( this )
		implicit none
		type(ContractedGaussian) , intent(in) :: this

		integer :: i
		
		call ContractedGaussian_show(this)
		
		do i=1, size(this%primitives)
			print *,""
			call PrimitiveGaussian_show( this%primitives( i ) )
		end do
	end subroutine ContractedGaussian_showStructure
	
	!**
	! Devuelve el tamano de la contraccion pasada por referencia.
	!
	! @param this  Gausiana contraida
	!
	! @return ContractedGaussian_getLength tamano de la contraccion
	!**
	function ContractedGaussian_getLength( this ) result( output )
		implicit none
		type(ContractedGaussian) :: this
		integer :: output
		
		output = size(this%primitives)
		
	end function ContractedGaussian_getLength
	
	!**
	! Permite obtener un puntero a la contraccion pasada como parametro, con el 
	! fin de lograrla  pasar como referencia a otras procedimientos sin necesidad
	! de duplicar el objeto.
	!
	! @param this Gausiana contraida
	!
	! @return contrationPointer Puntero a la gausiana contraida
	!**
	function ContractedGaussian_getContraction( this ) result( output )
		implicit none
		type(ContractedGaussian) , target :: this
		type(ContractedGaussian) , pointer :: output
		
		output => null()
		output => this
		
	end function ContractedGaussian_getContraction
	
	!!**
	! Permite obtener el numero de orbitales cartesianos de una contraccion
	! pasada como parametro
	!!**
	function ContractedGaussian_getNumberOfCartesianOrbitals( this ) result(output)
		implicit none
		type(ContractedGaussian) :: this
		integer :: output

		output = this%numCartesianOrbital

	end function ContractedGaussian_getNumberOfCartesianOrbitals

	!!**
	!! Permite obtener todas las posibles combinaciones de indice de momento angular
	!! a partir de un momento angular, por ende solo se debe usar para gaussianas no vectorizadas
	!!**
	function ContractedGaussian_getAngularMomentIndex(this) result(output)
		implicit none
		type(ContractedGaussian) :: this
		integer(8) :: output(3,this%numCartesianOrbital)

		integer :: counter
		integer :: x, y, z
		integer :: i, j
		counter = 1

	    do i = 0 , this%angularMoment
        	x = this%angularMoment - i
            do j = 0 , i
	            y = i - j
                z = j

                output(1:3, counter) = [x, y, z]
                counter = counter + 1
            end do
        end do

	end function ContractedGaussian_getAngularMomentIndex
	!**
	! Permite obtener el indice asociado al centro propietario de
	! de la contraccion.
	!
	! @param this Gausiana contraida
	!
	! @return contrationPointer Puntero a la gausiana contraida
	!**
	function ContractedGaussian_getOwner( this ) result( output )
		implicit none
		type(ContractedGaussian) , target :: this
		integer :: output
		
		output = this%owner
		
	end function ContractedGaussian_getOwner
	
	!**
	! @brief 	Retorna el codigo de capa y la direccion espacial
	!		de la contraccion especificada
	!**
	function ContractedGaussian_getShellCode( this ) result( output )
		implicit none
		type(ContractedGaussian) , intent(in) :: this
		character(9) :: output (this%numCartesianOrbital)
		
		type(Exception) :: ex
		
		character(1) :: indexCode(0:this%angularMoment)	!< Codigo para solo un indice de momento angular
  		character(1) :: shellCode(0:8)				    !< Codigo para una capa dada
  		character(1) :: coordCode(3)				    !< Codigo de las coordenadas cartesianas
  		integer :: nx, ny, nz	 					    !< Indices de momento angular
  		integer :: i, j, m, u, v					    !< Iteradores

		if(this%isVectorized) then

			nx = this%angularMomentIndex(1)
			ny = this%angularMomentIndex(2)
			nz = this%angularMomentIndex(3)

			!! nx
    		u = 0
    		do v = 1, nx
       			u = u + 1
       			indexCode(u) = trim(coordCode(1))
    		end do
    		!! ny
    		do v = 1, ny
       			u = u + 1
       			indexCode(u) = trim(coordCode(2))
    		end do
    		!! nz
    		do v = 1, nz
       			u = u + 1
       			indexCode(u) = trim(coordCode(3))
    		end do

    		output(1) = trim(indexCode(1)(0:this%angularMoment))

		else

			if ( this%angularMoment <= 8 ) then

		  		shellCode(0:8) = ["S", "P", "D", "F", "G", "H", "I", "J", "L"]
		  		coordCode(1:3) = ["x", "y", "z"]

		  		indexCode(0) = trim(shellCode(this%angularMoment))

		  		m = 0
		  		do i = 0 , this%angularMoment
		     		nx = this%angularMoment - i
		     		do j = 0 , i
		        		ny = i - j
		        		nz = j
		        		m = m + 1
		        		!! nx
		        		u = 0
		        		do v = 1, nx
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(1))
		        		end do
		        		!! ny
		        		do v = 1, ny
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(2))
		        		end do
		        		!! nz
		        		do v = 1, nz
		           			u = u + 1
		           			indexCode(u) = trim(coordCode(3))
		        		end do

		        		output(m) = trim(indexCode(1)(0:this%angularMoment))

		     		end do
		  		end do

			else

				call Exception_constructor( ex , ERROR )
	 			call Exception_setDebugDescription( ex, "Class object ContractedGaussian in the getShellCode function" )
	 			call Exception_setDescription( ex, "This angular moment  isn't implemented" )
	 			call Exception_show( ex )

	 		end if
 		end if
		
	end function ContractedGaussian_getShellCode
			
	!**
	! @brief 	Retorna el nombre asociado a la contraccion
	!**
	function ContractedGaussian_getName( this ) result( output )
		implicit none
		type(ContractedGaussian) , intent(in) :: this
		character(30) :: output
		
		output = trim(this%name)
		
	end function ContractedGaussian_getName
	
	!**
	! Permite ajustar los parametros de la gausiana contraida pasada como parametro
	! segun se especifique. Esta funcion incorpora autonormalizacion de la funcion
	! tras una modificacion.
	!
	! @param this  Gausiana contraida
	! @param [origin]  Origen la gausiana contraida
	! @param [contractionCoeff]  Vector con coeficientes de contraccion.
	! @param [orbitalExponet]  Exponente orbital
	! @param [angularMomentIndex] Indice de momento Angular
	! @param [gaussianNumber] Numero de Gausiana primitiva en la contraccion.
	!
	! @return this Gausiana modificada.
	!**
	subroutine ContractedGaussian_set( this , origin , contractionCoeff , orbitalsExponents ,&
		angularMomentIndex , angularMoment, gaussianID, owner)
		implicit none
		type(ContractedGaussian) , intent(inout) :: this
		real(8) , optional , intent(in) :: origin(3)
		real(8) , optional , intent(in) :: contractionCoeff(:)
		real(8) , optional , intent(in) :: orbitalsExponents(:)
		integer(8) , optional , intent(in) :: angularMomentIndex(3)
		integer(8) , optional , intent(in) :: angularMoment
		integer , optional :: gaussianID
		integer , optional :: owner
		
		integer :: i
		integer :: j
		integer :: limit

		!! Determina si se debe ajustar un primitiva de la contraccion
		if ( present(gaussianID) ) then
			j = gaussianID
			limit = gaussianID
		else
			j=1
			limit=this%length
		end if
	
		!! Ajusta el origen de cada gausiana especificada.
		if ( present( origin ) ) then 
			
			this%origin =origin
						
			do i = j , limit
				this%primitives(i)%origin = this%origin
			end do
			
		end if
	
		!! Ajusta el indice de momento angular de cada gausiana especificada
		if ( present( angularMoment ) ) then
			this%isVectorized = .false.
			this%angularMoment = angularMoment
			this%numCartesianOrbital = ( ( this%angularMoment + 1_8 )*( this%angularMoment + 2_8 ) ) / 2_8

			do i = j , limit
				call PrimitiveGaussian_set(this%primitives(i) , &
						angularMoment = this%angularMoment, &
						numCartesianOrbital = this%numCartesianOrbital)
			end do

			!! Renormaliza la contraccion
			if (allocated(this%normalizationConstant)) deallocate(this%normalizationConstant)
			allocate(this%normalizationConstant( this%numCartesianOrbital ))
			call ContractedGaussian_normalizeShell(this)

		end if

		!! Ajusta el momento angular de cada gausiana especificada
		if ( present( angularMomentIndex ) ) then
			this%isVectorized = .true.
			this%angularMomentIndex = angularMomentIndex

			do i = j , limit
				call PrimitiveGaussian_set(this%primitives(i) , &
						angularMomentIndex = this%angularMomentIndex )
			end do

			!! Renormaliza la contraccion
			call ContractedGaussian_normalizeContraction(this)

		end if
	
		!! Ajusta los exponentes orbitales de las primitivas de la contraccion
		if ( present( orbitalsExponents ) ) then 
			do i = j , limit  
				call PrimitiveGaussian_set( this%primitives(i) , orbitalExponent =  &
					orbitalsExponents(i) )
			end do
			
			!! Renormaliza la contraccion
			if(this%isVectorized) then
				call ContractedGaussian_normalizeContraction(this)
			else
				call ContractedGaussian_normalizeShell(this)
			end if

		end if
	
		!! Ajusta los coeficientes de contraccion de la funcion.
		if ( present(contractionCoeff) ) then

			this%contractionCoefficients = contractionCoeff
			
			!! Renormaliza la contraccion
			if(this%isVectorized) then
				call ContractedGaussian_normalizeContraction(this)
			else
				call ContractedGaussian_normalizeShell(this)
			end if
			
		end if
	
		!! Ajusta el centro propietario de cada funcion gausiana primitiva
		if ( present( owner ) ) then
			
			this%owner = owner
			do i = j , limit
				this%primitives(i)%owner = this%owner
			end do
			
		end if
	
	end subroutine ContractedGaussian_set
	
	
	!**
	! Permite calcular la constante de normalizacion de una capa de gausianas contraidas
	! de pasada como parametro.
	!
	! @param this Gausiana contraida
	!
	! @return  this Gausiana contraida normalizada.
	!**
	subroutine ContractedGaussian_normalizeShell( this )
		implicit none
		type(ContractedGaussian) , intent(inout) :: this

		real(8) :: integralValue(this%numCartesianOrbital * this%numCartesianOrbital)
		integer :: i, j, m, n

		this%normalizationConstant = 1.0_8

		 call ContractedGaussian_overlapIntegral( this , this, integralValue )

		m = 0
		do i=1, this%numCartesianOrbital
			do j = 1, this%numCartesianOrbital
				m = m + 1
				if (j == i) then
					this%normalizationConstant(i) = 1.0_8 / sqrt( integralValue(m))

				end if
			end do
		end do

	end subroutine ContractedGaussian_normalizeShell

	!**
	! Permite calcular la constante de normalizacion de la gausiana contraida
	! de pasada como parametro.
	!
	! @param this Gausiana contraida
	!
	! @return  this Gausiana contraida normalizada.
	!**
	subroutine ContractedGaussian_normalizeContraction( this )
		implicit none
		type(ContractedGaussian) , intent(inout) :: this
		real(8) :: integralValue(this%numCartesianOrbital * this%numCartesianOrbital)
		integer :: i, j, m, n

		this%normalizationConstant = 1.0_8

		call ContractedGaussian_overlapIntegral( this , this, integralValue )

		this%normalizationConstant(1) = 1.0_8 / sqrt( integralValue(1))

	end subroutine ContractedGaussian_normalizeContraction

	!>
	!! @brief Retorna el valor de la funcion en la coordenada especificada
	!<
	function ContractedGaussian_getValueAt( this, coordinate ) result(output)
	  	implicit none
	  	type(ContractedGaussian) , intent(in) :: this
	  	real(8) :: coordinate(3)
	  	real(8) :: output(this%numCartesianOrbital)
		integer :: i

	 	output = 0.0_8

	  	do i=1, this%length

			output = output &
		   		+ PrimitiveGaussian_getValueAt( this%primitives(i) , coordinate )&
			   	* this%contractionCoefficients(i)
	  	end do

	  	!!**************************************************************

		do i = 1, this%numCartesianOrbital
	 		output(i) = output(i) * this%normalizationConstant(i)
 		end do

	end function ContractedGaussian_getValueAt



	!**
	! Calcula la integral de overlap entre un par de contracciones especificadas.
	!
	! @param  contractedGaussianA Gausiana contraida A
	! @param  contractedGaussianB Gausiana contraida B
	!
	! @return  output Valor de la integral de traslapamiento.
	!**
	subroutine ContractedGaussian_overlapIntegral(contractedGaussianA, contractedGaussianB, integral)
		implicit none

		type(ContractedGaussian), intent(in) :: contractedGaussianA, contractedGaussianB
		real(8), intent(inout) :: integral(contractedGaussianA%numCartesianOrbital * contractedGaussianB%numCartesianOrbital)

		integer ::  am1(0:3)
		integer ::  am2(0:3)
		integer ::  max_am
		integer ::  nprim1
		integer ::  nprim2
		real(8) ::  A(0:3)
		real(8) ::  B(0:3)
		real(8) ::  exp1(0:contractedGaussianA%length)
		real(8) ::  exp2(0:contractedGaussianB%length)
		real(8) ::  coef1(0:contractedGaussianA%length)
		real(8) ::  coef2(0:contractedGaussianB%length)
		real(8) ::  nor1(0:contractedGaussianA%length)
		real(8) ::  nor2(0:contractedGaussianB%length)
		real(8) :: auxIntegral
		integer(8) :: angularMomentIndexA(3, contractedGaussianA%numcartesianOrbital)
		integer(8) :: angularMomentIndexB(3, contractedGaussianB%numcartesianOrbital)
		integer ::  i, m, p, q


		angularMomentIndexA = contractedGaussian_getAngularMomentIndex(contractedGaussianA)
		angularMomentIndexB = contractedGaussian_getAngularMomentIndex(contractedGaussianB)

		nprim1 = contractedGaussianA%length
		A(0) = contractedGaussianA%origin(1)
		A(1) = contractedGaussianA%origin(2)
		A(2) = contractedGaussianA%origin(3)
		coef1(0:nprim1-1) =  contractedGaussianA%contractionCoefficients(1:nprim1)


		nprim2 = contractedGaussianB%length
		B(0) = contractedGaussianB%origin(1)
		B(1) = contractedGaussianB%origin(2)
		B(2) = contractedGaussianB%origin(3)
		coef2(0:nprim2-1) =  contractedGaussianB%contractionCoefficients(1:nprim2)

		m = 0

		do p = 1, contractedGaussianA%numcartesianOrbital
			do q = 1, contractedGaussianB%numcartesianOrbital

				m = m + 1

				do i = 0, nprim1 - 1
					exp1(i) = contractedGaussianA%primitives(i+1)%orbitalExponent
				  	nor1(i) = contractedGaussianA%primitives(i+1)%normalizationConstant(p)
				end do

				do i = 0, nprim2 - 1
					exp2(i) = contractedGaussianB%primitives(i+1)%orbitalExponent
				  	nor2(i) = contractedGaussianB%primitives(i+1)%normalizationConstant(q)
				end do

				am1 = 0
				am2 = 0

				am1(0:2) = angularMomentIndexA(1:3, p)
				am2(0:2) = angularMomentIndexB(1:3, q)

				max_am = max(sum(am1), sum(am2)) + 1

				call OverlapIntegrals_compute(am1, am2, nprim1, nprim2, A, B, exp1, exp2, coef1, coef2, nor1, nor2, auxIntegral)

				auxIntegral = auxIntegral * contractedGaussianA%normalizationConstant(p) &
						    			  * contractedGaussianB%normalizationConstant(q)
			  	integral(m) = auxIntegral

		  	end do
	  	end do

	end subroutine ContractedGaussian_overlapIntegral


	! Calcula la derivada de la derivada de overlap entre un par de contracciones especificadas.
	!
	! @param  contractedGaussianA Gausiana contraida A
	! @param  contractedGaussianB Gausiana contraida B
	!
	! @return  Valor de la derivada de la integral de overlap
	!**
	function ContractedGaussian_overlapDerivative( contractedGaussianA , contractedGaussianB, nucleus, component ) &
		result( output )
		implicit none
		type(ContractedGaussian) , intent(in) :: contractedGaussianA
		type(ContractedGaussian) , intent(in) :: contractedGaussianB
		integer, intent(in) :: nucleus
		integer, intent(in) :: component
		real(8) :: output
		
		real(8) :: aux
		integer :: i
		integer :: j
		
		aux = Math_kroneckerDelta(contractedGaussianA%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianB%owner,nucleus)
		
			
		if ( aux > APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
				
			!!**************************************************************
			!! Calcula iterativamente derivada para cada gausiana de la
			!! contraccion.
			!!
			output = 0.0_8
!			do i = 1 , contractedGaussianA%length
!				do j = 1 , contractedGaussianB%length
!					output = output &
!							+( PrimitiveGaussian_overlapDerivative(contractedGaussianA%primitives(i) , &
!							contractedGaussianB%primitives(j), nucleus, component )&
!							* contractedGaussianA%contractionCoefficients(i) &
!							* contractedGaussianB%contractionCoefficients(j) &
!							* contractedGaussianA%primitives(i)%normalizationConstant &
!							* contractedGaussianB%primitives(j)%normalizationConstant )
!				end do
!			end do
!			!!**************************************************************
!
!			!! Obtiene la derivada normalizada.
!			output = output * contractedGaussianA%normalizationConstant &
!					* contractedGaussianB%normalizationConstant
		else
			output = 0.0_8
		end if 
				 
	end function ContractedGaussian_overlapDerivative

	!**
	! Calcula la integral de momento entre un par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param origin Origen alrededor del cual se calcula el momento.
!! 	! @param momentIndex Indice de momento
	!
	! @return  output Valor de la integral de momento.
	!**
	function ContractedGaussian_momentIntegral( contractedGaussianA , contractedGaussianB , origin , &
		component ) result(output)
		implicit none
		type(ContractedGaussian) :: contractedGaussianA
		type(ContractedGaussian) :: contractedGaussianB
		real(8) , intent( in ) :: origin( 3 )
		integer , intent( in ) :: component
		real(8) :: output(contractedGaussianA%numCartesianOrbital * contractedGaussianB%numCartesianOrbital)
		
		real(8) :: auxVal(contractedGaussianA%numCartesianOrbital * contractedGaussianB%numCartesianOrbital)
		integer :: a, b, m
		integer :: i, j
		integer :: k, l

		!!**************************************************************
		!! Calcula iterativamente el momento entre las primitivas de las
		!! contracciones dadas.
		!!
		output = 0.0_8
		do i = 1 , contractedGaussianA%length
			do j = 1 , contractedGaussianB%length

				auxVal = 0.0_8
				auxVal = PrimitiveGaussian_momentIntegral( contractedGaussianA%primitives(i) , &
							contractedGaussianB%primitives(j), origin, component )

				auxVal = auxVal  *  contractedGaussianA%contractionCoefficients(i) * &
									contractedGaussianB%contractionCoefficients(j)
				m = 0
				do k = 1, contractedGaussianA%numCartesianOrbital
					do l = 1, contractedGaussianB%numCartesianOrbital
						m = m + 1

						auxVal(m) = auxVal(m) * (contractedGaussianA%primitives(i)%normalizationConstant(k) &
											  *  contractedGaussianB%primitives(j)%normalizationConstant(l))

					end do
				end do

				output = output + auxVal

			end do
		end do


		!!**************************************************************
		!! Obtiene la integral de momento normalizada.
		m = 0
		do i = 1, contractedGaussianA%numCartesianOrbital
			do j = 1,  contractedGaussianB%numCartesianOrbital
				m = m + 1
				output(m) = output(m) * contractedGaussianA%normalizationConstant(i) &
						* contractedGaussianB%normalizationConstant(j)
			end do
		end do

	end function ContractedGaussian_momentIntegral
	
	!**
	! Calcula la integral de momentum entre un par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param origin Origen alrededor del cual se calcula el momento.
	! @param momentIndex Indice de momento
	!
	! @return  output Valor de la integral de momento.
	!**
	function ContractedGaussian_momentumIntegral( contractedGaussianA , contractedGaussianB , origin , &
		component ) result(output)
		implicit none
		type(ContractedGaussian) , intent(in) :: contractedGaussianA
		type(ContractedGaussian) , intent(in) :: contractedGaussianB
		real(8), intent( in ), optional :: origin( 3 )
		integer, intent( in ), optional :: component
		real(8) :: output
		
		integer :: i
		integer :: j
		
		!!**************************************************************
		!! Calcula iterativamente el momento entre las primitivas de las
		!! contracciones dadas.
		!!
		output = 0.0_8
!		do i = 1 , contractedGaussianA%length
!			do j = 1 , contractedGaussianB%length
!				output = output &
!						+ ( PrimitiveGaussian_angularMomentIntegral( contractedGaussianA%primitives(i) , &
!						contractedGaussianB%primitives(j) , origin , component ) &
!						* contractedGaussianA%contractionCoefficients(i) &
!						* contractedGaussianB%contractionCoefficients(j) &
!						* contractedGaussianA%primitives(i)%normalizationConstant &
!						* contractedGaussianB%primitives(j)%normalizationConstant )
!			end do
!		end do
!		!!**************************************************************
!
!		!! Obtiene la integral de momento normalizada.
!		output = output * contractedGaussianA%normalizationConstant &
!				* contractedGaussianB%normalizationConstant
		 
	end function ContractedGaussian_momentumIntegral

	
	!**
	! Calcula la integral de energia cinetica entre un par de contracciones especificadas.
	!
	! @param  contractedGaussianA Gausiana contraida A
	! @param  contractedGaussianB Gausiana contraida B
	!
	! @return  output Valor de la integral de energia cinetica.
	!**
	subroutine ContractedGaussian_kineticIntegral(contractedGaussianA, contractedGaussianB, integral)
		implicit none

		type(ContractedGaussian), intent(in) :: contractedGaussianA, contractedGaussianB
		real(8), allocatable, intent(inout) :: integral(:)

		integer ::  am1(0:3)
		integer ::  am2(0:3)
		integer ::  max_am
		integer ::  nprim1
		integer ::  nprim2
		real(8) ::  A(0:3)
		real(8) ::  B(0:3)
		real(8) ::  exp1(0:contractedGaussianA%length)
		real(8) ::  exp2(0:contractedGaussianB%length)
		real(8) ::  coef1(0:contractedGaussianA%length)
		real(8) ::  coef2(0:contractedGaussianB%length)
		real(8) ::  nor1(0:contractedGaussianA%length)
		real(8) ::  nor2(0:contractedGaussianB%length)
		real(8) :: auxIntegral
		integer(8) :: angularMomentIndexA(3, contractedGaussianA%numcartesianOrbital)
		integer(8) :: angularMomentIndexB(3, contractedGaussianB%numcartesianOrbital)
		integer ::  i, m, p, q

		angularMomentIndexA = contractedGaussian_getAngularMomentIndex(contractedGaussianA)
		angularMomentIndexB = contractedGaussian_getAngularMomentIndex(contractedGaussianB)

		nprim1 = contractedGaussianA%length
		A(0) = contractedGaussianA%origin(1)
		A(1) = contractedGaussianA%origin(2)
		A(2) = contractedGaussianA%origin(3)
		coef1(0:nprim1-1) =  contractedGaussianA%contractionCoefficients(1:nprim1)


		nprim2 = contractedGaussianB%length
		B(0) = contractedGaussianB%origin(1)
		B(1) = contractedGaussianB%origin(2)
		B(2) = contractedGaussianB%origin(3)
		coef2(0:nprim2-1) =  contractedGaussianB%contractionCoefficients(1:nprim2)

		m = 0

		do p = 1, contractedGaussianA%numcartesianOrbital
			do q = 1, contractedGaussianB%numcartesianOrbital

				m = m + 1

				do i = 0, nprim1 - 1
					exp1(i) = contractedGaussianA%primitives(i+1)%orbitalExponent
				  	nor1(i) = contractedGaussianA%primitives(i+1)%normalizationConstant(p)
				end do

				do i = 0, nprim2 - 1
					exp2(i) = contractedGaussianB%primitives(i+1)%orbitalExponent
				  	nor2(i) = contractedGaussianB%primitives(i+1)%normalizationConstant(q)
				end do

				am1 = 0
				am2 = 0

				am1(0:2) = angularMomentIndexA(1:3, p)
				am2(0:2) = angularMomentIndexB(1:3, q)

				max_am = max(sum(am1), sum(am2)) + 1

				call PrimitiveGaussian_kineticIntegral(am1, am2, nprim1, nprim2, A, B, exp1, exp2, coef1, coef2, nor1, nor2, auxIntegral)

				auxIntegral = auxIntegral * contractedGaussianA%normalizationConstant(p) &
						    			  * contractedGaussianB%normalizationConstant(q)
			  	integral(m) = auxIntegral
		  	end do
	  	end do

	end subroutine ContractedGaussian_kineticIntegral


	!**
	! Calcula la derivada de la integral de energia cinetica entre un par de
	! contracciones especificadas.
	!
	! @param  contractedGaussianA Gausiana contraida A
	! @param  contractedGaussianB Gausiana contraida B
	!
	! @return  Valor de la derivade de la integral de energia cinetica.
	!**
	function ContractedGaussian_kineticDerivative( contractedGaussianA , contractedGaussianB, nucleus, component ) &
		result( output )
		implicit none
		type(ContractedGaussian) , intent(in) :: contractedGaussianA
		type(ContractedGaussian) , intent(in) :: contractedGaussianB
		integer, intent(in) :: nucleus
		integer, intent(in) :: component
		real(8) output
		
		real(8), allocatable :: auxVal(:)
		real(8) :: aux
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		integer :: m
		
		aux = Math_kroneckerDelta(contractedGaussianA%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianB%owner,nucleus)
		
			
		if ( aux > APMO_instance%DOUBLE_ZERO_THRESHOLD ) then


			!!*******************************************************************
			!! Calcula iterativamente la derivada de la integral de energia cinetica
			!! entre las primitivas de las contracciones dadas.
			!!

!			do i = 1 , contractedGaussianA%length
!				do j = 1 , contractedGaussianB%length
!				output = output &
!						+ ( PrimitiveGaussian_kineticDerivative( contractedGaussianA%primitives(i) , &
!						contractedGaussianB%primitives(j) , nucleus, component ) &
!						* contractedGaussianA%contractionCoefficients(i) &
!						* contractedGaussianB%contractionCoefficients(j) &
!						* contractedGaussianA%primitives(i)%normalizationConstant &
!						* contractedGaussianB%primitives(j)%normalizationConstant )
!				end do
!			end do
!			!!*******************************************************************
!
!			!! Calcula el valor de la integral teniendo en cuenta las constantes
!			!! de normalizacion.
!			output = output * contractedGaussianA%normalizationConstant &
!					* contractedGaussianB%normalizationConstant

		else
			output = 0.0_8
		end if
		
	end function ContractedGaussian_kineticDerivative

	
	!**
	! Calcula la integral de interaccion clasica ( atraccion / repulsion) entre un 
	! par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param originPuntualCharge Origen de la carga puntual.
	!
	! @return  output Valor de la integral de interaccion clasica.
	!**
	subroutine ContractedGaussian_attractionIntegral(ContractedGaussianA, contractedGaussianB, particles, npunt, integralValue)
	  	implicit none

	  	type(ContractedGaussian), intent(in) :: ContractedGaussianA, contractedGaussianB
	  	type(puntualParticle), intent(in), allocatable :: particles(:)
	  	integer, intent(in) :: npunt

	  	real(8), allocatable, intent(inout) :: integralValue(:)

	  	integer :: am1(0:3)
	  	integer :: am2(0:3)
	  	integer :: max_am
	  	integer :: nprim1
	  	integer :: nprim2
	  	real(8) :: A(0:3)
	  	real(8) :: B(0:3)
	  	real(8) :: exp1(0:ContractedGaussianA%length)
	  	real(8) :: exp2(0:contractedGaussianB%length)
	  	real(8) :: coef1(0:ContractedGaussianA%length)
	  	real(8) :: coef2(0:contractedGaussianB%length)
	  	real(8) :: nor1(0:ContractedGaussianA%length)
	  	real(8) :: nor2(0:contractedGaussianB%length)
	  	real(8) :: auxIntegral
	  	integer(8) :: angularMomentIndexA(3, contractedGaussianA%numcartesianOrbital)
		integer(8) :: angularMomentIndexB(3, contractedGaussianB%numcartesianOrbital)
		integer :: i, m, p, q

	  	nprim1 = ContractedGaussianA%length
	  	nprim2 = contractedGaussianB%length

	  	A(0) = ContractedGaussianA%origin(1)
	  	A(1) = ContractedGaussianA%origin(2)
	  	A(2) = ContractedGaussianA%origin(3)

	  	B(0) = contractedGaussianB%origin(1)
	  	B(1) = contractedGaussianB%origin(2)
	  	B(2) = contractedGaussianB%origin(3)

	  	coef1(0:nprim1-1) =  ContractedGaussianA%contractionCoefficients(1:nprim1)
	  	coef2(0:nprim2-1) =  contractedGaussianB%contractionCoefficients(1:nprim2)

		angularMomentIndexA = contractedGaussian_getAngularMomentIndex(contractedGaussianA)
		angularMomentIndexB = contractedGaussian_getAngularMomentIndex(contractedGaussianB)

		m = 0

		do p = 1, contractedGaussianA%numcartesianOrbital
			do q = 1, contractedGaussianB%numcartesianOrbital

				m = m + 1

				do i = 0, nprim1 - 1
					exp1(i) = contractedGaussianA%primitives(i+1)%orbitalExponent
				  	nor1(i) = contractedGaussianA%primitives(i+1)%normalizationConstant(p)
				end do

				do i = 0, nprim2 - 1
					exp2(i) = contractedGaussianB%primitives(i+1)%orbitalExponent
				  	nor2(i) = contractedGaussianB%primitives(i+1)%normalizationConstant(q)
				end do

				am1 = 0
				am2 = 0

				am1(0:2) = angularMomentIndexA(1:3, p)
				am2(0:2) = angularMomentIndexB(1:3, q)

				call AttractionIntegrals_compute(am1, am2, nprim1, nprim2, npunt, A, B, exp1, exp2, coef1, coef2, nor1, nor2, particles, auxIntegral)

				auxIntegral = auxIntegral * contractedGaussianA%normalizationConstant(p) &
						    			  * contractedGaussianB%normalizationConstant(q)

			  	integralValue(m) = auxIntegral
!
   			end do
   		end do

	end subroutine ContractedGaussian_attractionIntegral

	!**
	! Devuelve los valores del operador de Coulomb para un rango discreto de la variable independiente r 
	! Igualmente devuelve los valores del operador clasico de interaccion iterparticula en el mismo dominio de r.
	!
	! @param this Gausiana contraida 
	! @param r_step paso de la variable independiente.
	! @param r_range intervalo abierto donde se define el dominio de la funcion
	!
	! @return  coulombValues
	!**
	subroutine ContractedGaussian_coulombOperator( this, r_step , r_range, charge) 
		implicit none
		type(ContractedGaussian) , intent(inout) :: this
		real(8) , intent(in) :: r_step
		real(8) , intent(in) :: r_range
		real(8),  intent(in) :: charge
		
		integer :: domainSize
		integer :: itr
		real(8) :: originCenter(3)
		real(8) :: auxVec(3)
		real(8) :: output
		real(8) :: aux
		integer :: i
		integer :: j
		
		auxVec=this%primitives(1)%origin
				
		do i = 1 , this%length
			call PrimitiveGaussian_set(this%primitives(i),origin = [0.0_8,0.0_8,0.0_8] )
		end do

		domainSize= int(  dabs( r_range ) / r_step, 4 )
		
		!!************************************************************
		!! Calcula iterativamente la integral de atraccion/repulsion 
		!! con una carga puntual.
		!!
		
		originCenter=0.0_8
		
		print *,"       <r>        ", "               <J>        ", "         <Z^2/r>        "
		
!		do itr=1, domainSize+1
!
!			originCenter(1)=   itr * r_step
!			output = 0.0_8
!
!			do i = 1 , this%length
!				do j = 1 , this%length
!					output = output &
!					+ ( PrimitiveGaussian_attractionIntegral( this%primitives(i) , &
!					this%primitives(j) , originCenter ) &
!					* this%contractionCoefficients(i) &
!					* this%contractionCoefficients(j) &
!					* this%primitives(i)%normalizationConstant &
!					* this%primitives(j)%normalizationConstant )
!				end do
!			end do
!
!			output = output * this%normalizationConstant &
!				* this%normalizationConstant  * (charge**2.0_8)
!
!				aux =1.0_8 / originCenter(1) * (charge**2.0_8)
!
!			print *,originCenter(1),output,aux
!
!
!		end do
		!!************************************************************
		
		do i = 1 , this%length
			call PrimitiveGaussian_set(this%primitives(i),origin = auxVec )
		end do

		
	end subroutine ContractedGaussian_coulombOperator
	
	!**
	! Calcula la derivada de la integral de interaccion clasica 
	! ( atraccion / repulsion) entre un par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param originPuntualCharge Origen de la carga puntual.
	!
	! @return  Valor de la derivada
	!**
	function ContractedGaussian_attractionDerivative( contractedGaussianA , contractedGaussianB , originPuntualCharge, &
		nucleus, component, puntualParticleOwner ) result(output)
		implicit none
		type(ContractedGaussian) , intent(in) :: contractedGaussianA
		type(ContractedGaussian) , intent(in) :: contractedGaussianB
		real(8) , intent(in) :: originPuntualCharge(3)
		integer, intent(in) :: nucleus
		integer, intent(in) :: component
		integer, intent(in), optional :: puntualParticleOwner
		
		real(8) :: output
		integer :: puntualParticleOwner_
		real(8) :: aux
		integer :: i
		integer :: j
		
		puntualParticleOwner_=0
		if ( present(puntualParticleOwner) ) then
			puntualParticleOwner_=puntualParticleOwner
		end if
		
		aux =Math_kroneckerDelta(contractedGaussianA%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianB%owner,nucleus) &
			+ Math_kroneckerDelta(puntualParticleOwner_,nucleus)
		
	
		if ( aux > APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
			
			!!************************************************************
			!! Calcula iterativamente la derivada para cada componente de 
			!! la contraccion
			!!
			output = 0.0_8
!			do i = 1 , contractedGaussianA%length
!				do j = 1 , contractedGaussianB%length
!
!					output = output &
!					+ ( PrimitiveGaussian_attractionDerivative( &
!					contractedGaussianA%primitives(i) , &
!					contractedGaussianB%primitives(j) , originPuntualCharge, &
!					nucleus, component, puntualParticleOwner_ ) &
!					* contractedGaussianA%contractionCoefficients(i) &
!					* contractedGaussianB%contractionCoefficients(j) &
!					* contractedGaussianA%primitives(i)%normalizationConstant &
!					* contractedGaussianB%primitives(j)%normalizationConstant )
!
!				end do
!			end do
			!!************************************************************
			
			!! Obtiene la el valor final de la derivada
!			output = output * contractedGaussianA%normalizationConstant &
!					* contractedGaussianB%normalizationConstant
		else
			output = 0.0_8
		end if
		
	end function ContractedGaussian_attractionDerivative
	
	
	!**
	! Calcula la integral de interaccion cuantica ( atraccion / repulsion) entre un 
	! par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param contractedGaussianC Gausiana contraida C
	! @param contractedGaussianD Gausiana contraida D
	!
	! @return  output Valor de la integral de interaccion cuantica.
	!**
	subroutine ContractedGaussian_repulsionIntegral( contractedGaussianA , contractedGaussianB , &
													 contractedGaussianC , contractedGaussianD, output )
		implicit none
		type(ContractedGaussian) , intent(in):: contractedGaussianA
		type(ContractedGaussian) , intent(in):: contractedGaussianB
		type(ContractedGaussian) , intent(in):: contractedGaussianC
		type(ContractedGaussian) , intent(in):: contractedGaussianD
		real(8), allocatable, intent(out) :: output(:)
		
		real(8), allocatable :: auxVal(:)
		integer :: i, j, k, l
		integer :: p, q, r, s
		integer :: m


		type(Exception) :: ex

		if(allocated(output)) deallocate(output)
		if(allocated(auxVal)) deallocate(auxVal)

		allocate(output(contractedGaussianA%numCartesianOrbital * contractedGaussianB%numCartesianOrbital * &
						contractedGaussianC%numCartesianOrbital * contractedGaussianD%numCartesianOrbital))
		allocate(auxVal(contractedGaussianA%numCartesianOrbital * contractedGaussianB%numCartesianOrbital * &
						contractedGaussianC%numCartesianOrbital * contractedGaussianD%numCartesianOrbital))

		output = 0.0_8
		auxVal = 0.0_8

		!!************************************
		!! Calcula iterativamente la integral de atraccion/repulsion de 
		!! contracciones gausianas.
		!!

		do l = 1 , contractedGaussianD%length
			do k = 1 , contractedGaussianC%length
				do j = 1 , contractedGaussianB%length
					do i = 1 , contractedGaussianA%length

						call LibintInterface_compute(contractedGaussianA%primitives(i) ,&
													 contractedGaussianB%primitives(j) ,&
													 contractedGaussianC%primitives(k) ,&
													 contractedGaussianD%primitives(l) , auxVal)

						auxVal = auxVal	* contractedGaussianA%contractionCoefficients(i) &
										* contractedGaussianB%contractionCoefficients(j) &
										* contractedGaussianC%contractionCoefficients(k) &
										* contractedGaussianD%contractionCoefficients(l)

						output = output + auxVal

					end do
				end do
			end do
		end do

		!!**************************************************************
		!! Obtiene la integral de repulsion normalizada.
		!!

		select case (order)
			case(0)
				!!(SS|SS)
				output(1) = output(1) * contractedGaussianA%normalizationConstant(1) &
									  * contractedGaussianB%normalizationConstant(1) &
									  * contractedGaussianC%normalizationConstant(1) &
									  * contractedGaussianD%normalizationConstant(1)
			case(1)
				!!(AB|CD)
				m = 0
				do i = 1, contractedGaussianA%numCartesianOrbital
					do j = 1,  contractedGaussianB%numCartesianOrbital
						do k = 1, contractedGaussianC%numCartesianOrbital
							do l = 1, contractedGaussianD%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianA%normalizationConstant(i) &
													  * contractedGaussianB%normalizationConstant(j) &
													  * contractedGaussianC%normalizationConstant(k) &
													  * contractedGaussianD%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(2)
				!!(BA|CD)
				m = 0

				do i = 1, contractedGaussianB%numCartesianOrbital
					do j = 1,  contractedGaussianA%numCartesianOrbital
						do k = 1, contractedGaussianC%numCartesianOrbital
							do l = 1, contractedGaussianD%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianB%normalizationConstant(i) &
													  * contractedGaussianA%normalizationConstant(j) &
													  * contractedGaussianC%normalizationConstant(k) &
													  * contractedGaussianD%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(3)
				!!(AB|DC)
				m = 0

				do i = 1, contractedGaussianA%numCartesianOrbital
					do j = 1,  contractedGaussianB%numCartesianOrbital
						do k = 1, contractedGaussianD%numCartesianOrbital
							do l = 1, contractedGaussianC%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianA%normalizationConstant(i) &
													  * contractedGaussianB%normalizationConstant(j) &
													  * contractedGaussianD%normalizationConstant(k) &
													  * contractedGaussianC%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(4)
				!!(BA|DC)
				m = 0

				do i = 1, contractedGaussianB%numCartesianOrbital
					do j = 1,  contractedGaussianA%numCartesianOrbital
						do k = 1, contractedGaussianD%numCartesianOrbital
							do l = 1, contractedGaussianC%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianB%normalizationConstant(i) &
													  * contractedGaussianA%normalizationConstant(j) &
													  * contractedGaussianD%normalizationConstant(k) &
													  * contractedGaussianC%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(5)
				!!(CD|AB)
				m = 0

				do i = 1, contractedGaussianC%numCartesianOrbital
					do j = 1,  contractedGaussianD%numCartesianOrbital
						do k = 1, contractedGaussianA%numCartesianOrbital
							do l = 1, contractedGaussianB%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianC%normalizationConstant(i) &
													  * contractedGaussianD%normalizationConstant(j) &
													  * contractedGaussianA%normalizationConstant(k) &
													  * contractedGaussianB%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(6)
				!!(DC|AB)
				m = 0

				do i = 1, contractedGaussianD%numCartesianOrbital
					do j = 1,  contractedGaussianC%numCartesianOrbital
						do k = 1, contractedGaussianA%numCartesianOrbital
							do l = 1, contractedGaussianB%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianD%normalizationConstant(i) &
													  * contractedGaussianC%normalizationConstant(j) &
													  * contractedGaussianA%normalizationConstant(k) &
													  * contractedGaussianB%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(7)
				!!(CD|BA)
				m = 0

				do i = 1, contractedGaussianC%numCartesianOrbital
					do j = 1,  contractedGaussianD%numCartesianOrbital
						do k = 1, contractedGaussianB%numCartesianOrbital
							do l = 1, contractedGaussianA%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianC%normalizationConstant(i) &
													  * contractedGaussianD%normalizationConstant(j) &
													  * contractedGaussianB%normalizationConstant(k) &
													  * contractedGaussianA%normalizationConstant(l)
							end do
						end do
					end do
				end do
			case(8)
				!!(DC|BA)
				m = 0

				do i = 1, contractedGaussianD%numCartesianOrbital
					do j = 1,  contractedGaussianC%numCartesianOrbital
						do k = 1, contractedGaussianB%numCartesianOrbital
							do l = 1, contractedGaussianA%numCartesianOrbital
								m = m + 1

								output(m) = output(m) * contractedGaussianD%normalizationConstant(i) &
													  * contractedGaussianC%normalizationConstant(j) &
													  * contractedGaussianB%normalizationConstant(k) &
													  * contractedGaussianA%normalizationConstant(l)
							end do
						end do
					end do
				end do
		end select

	end subroutine ContractedGaussian_repulsionIntegral

	!**
	! Calcula la derivada de laintegral de interaccion cuantica 
	! ( atraccion / repulsion) entre un par de contracciones especificadas.
	!
	! @param contractedGaussianA Gausiana contraida A
	! @param contractedGaussianB Gausiana contraida B
	! @param contractedGaussianC Gausiana contraida C
	! @param contractedGaussianD Gausiana contraida D
	!
	! @return  Valor de la derivada 
	!**
	function ContractedGaussian_repulsionDerivative( contractedGaussianA , contractedGaussianB , &
		contractedGaussianC , contractedGaussianD, nucleus, component ) result( output )
		implicit none
		type(ContractedGaussian) , intent(in):: contractedGaussianA
		type(ContractedGaussian) , intent(in):: contractedGaussianB
		type(ContractedGaussian) , intent(in):: contractedGaussianC
		type(ContractedGaussian) , intent(in):: contractedGaussianD
		integer, intent(in) :: nucleus
		integer, intent(in) :: component
		
		real(8) :: output
		real(8) :: aux
		integer :: i
		integer :: j
		integer :: k
		integer :: l
		
		aux =Math_kroneckerDelta(contractedGaussianA%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianB%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianC%owner,nucleus) &
			+ Math_kroneckerDelta(contractedGaussianD%owner,nucleus)
			
		if ( aux > APMO_instance%DOUBLE_ZERO_THRESHOLD ) then
		
			!!*************************************************************
			!! Calcula iterativamente la derivada para cada componente de
			!! la contraccion.
			!!
		
		
			output = 0.0_8
!			do l = 1 , contractedGaussianD%length
!				do k = 1 , contractedGaussianC%length
!					do j = 1 , contractedGaussianB%length
!						do i = 1 , contractedGaussianA%length
!							output = output &
!							+ ( PrimitiveGaussian_repulsionDerivative( contractedGaussianA%primitives(i) ,&
!							contractedGaussianB%primitives(j) , contractedGaussianC%primitives(k) , &
!							contractedGaussianD%primitives(l), nucleus, component ) &
!							* contractedGaussianA%contractionCoefficients(i) &
!							* contractedGaussianB%contractionCoefficients(j) &
!							* contractedGaussianC%contractionCoefficients(k) &
!							* contractedGaussianD%contractionCoefficients(l) &
!							* contractedGaussianA%primitives(i)%normalizationConstant &
!							* contractedGaussianB%primitives(j)%normalizationConstant &
!							* contractedGaussianC%primitives(k)%normalizationConstant&
!							* contractedGaussianD%primitives(l)%normalizationConstant )
!
!						end do
!					end do
!				end do
!			end do
			!!*************************************************************
			
	
			!! Obtiene el valor final de la derivada 
!			output = output * contractedGaussianA%normalizationConstant &
!					* contractedGaussianB%normalizationConstant * contractedGaussianC%normalizationConstant &
!					* contractedGaussianD%normalizationConstant
		else
			output = 0.0_8
		end if
	
	end function ContractedGaussian_repulsionDerivative
	
	
	
	!>
	!! @brief Calcula el producto de dos funciones gausianas contraidas
	!!            Actualmente el producto debe ser entre una funcion contraida 
	!!            generica (de cualquier momento angular) y una funcion tipo s. 
	!<
	subroutine ContractedGaussian_product(this, anotherThis, output) 
		implicit none
		type(ContractedGaussian), intent(in) :: this
		type(ContractedGaussian), intent(in) :: anotherThis
		type(ContractedGaussian), intent(inout) :: output

		integer :: i
		integer :: j
		integer :: k
		real(8) :: auxValue
 
               call ContractedGaussian_constructor(output, ssize=(this%length*anotherthis%length), &
			noNormalize=.true. )

               	k=1
		do i = 1, this%length
			do j = 1 , anotherthis%length
					
					call PrimitiveGaussian_copyConstructor(output%primitives(k), &
						PrimitiveGaussian_product( this%primitives(i) , &
						anotherthis%primitives(j), proportConstant=auxValue ) )
					output%primitives(k)%angularMomentIndex =  &
						this%primitives(i)%angularMomentIndex
					output%primitives(k)%normalizationConstant =  &
						this%primitives(i)%normalizationConstant
					output%primitives(k)%owner =  &
						this%primitives(i)%owner
					output%contractionCoefficients(k)=this%contractionCoefficients(i)* &
						anotherthis%contractionCoefficients(j) * auxValue
					k=k+1
			end do
		end do

		output%normalizationConstant=this%normalizationConstant
		output%owner= anotherthis%owner
			
	end subroutine ContractedGaussian_product

end module ContractedGaussian_
