/*!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************/
/**
 *  @brief Implentacion Basica de parser para lectura de archivos xml 
 *
 * 
 *  @author Sergio A. Gonz�lez
 *	
 *  <b> Fecha de creaci�n : </b> 2007-08-24
 *
 *  <b> Historial de modificaciones : </b>
 *    - <tt>2007-08-24</tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
 *         -# Creacion de archivo y metodos b�sicos	
 *    - <tt>2008-08-21</tt>: N�stor Aguirre ( nfaguirrec@unal.edu.co )
 *         -# Reestruro m�todos para uso en apmo
 **/
#include <cstdio>
#include <cstring>
#include <expat.h>

#define STRINGSIZE        30

extern "C"
{

	
#define FNAME(x,y) x##_mp_##y##_
	
#define XMLParser_startElement FNAME(xmlparser_,xmlparser_startelement)
#define XMLParser_endElement FNAME(xmlparser_,xmlparser_endelement)
#define  XMLParserBase_parseFile xmlparserbase_parsefile
	
	void XMLParser_startElement( char tagname[STRINGSIZE] , char  dataname[STRINGSIZE], char valuename[STRINGSIZE] ) ;
	void XMLParser_endElement( char tagname[STRINGSIZE] ) ;
	

}


#define BUFFSIZE        8192


void XMLParserBase_startElement( void* userdata, const char* name, const char** attr )
{
	int attrSize = 0 ;
	for( int i = 0; attr[i]; i += 2 )
		attrSize++ ;
	
	char aux[STRINGSIZE]  = "" ;
	char currentData[STRINGSIZE]  = "" ;
	char currentValue[STRINGSIZE]  = "" ;
	
	memset( aux, ' ', STRINGSIZE );
	memcpy( aux, name,strlen(name) ) ;
	
	for( int i = 0, j=0; attr[i]; i += 2, j++ ){
	
		memset( currentData, ' ', STRINGSIZE );
		memset( currentValue, ' ', STRINGSIZE );
	
		memcpy( currentData, attr[i], strlen(attr[i])) ;
		memcpy( currentValue, attr[i+1], strlen(attr[i+1])) ;
		
		XMLParser_startElement( aux , currentData, currentValue ) ;
	}
	

	
}

void XMLParserBase_endElement( void* userdata, const char* name )
{
	char aux[STRINGSIZE]  = "" ;
	
	memset( aux, ' ', STRINGSIZE );
	memcpy( aux, name,strlen(name)) ;

	XMLParser_endElement( aux ) ;
}

/// Esta funcion debe ser llamada desde fortran
extern "C" void XMLParserBase_parseFile(char* fileName)
{
	char* buff = (char*)malloc(sizeof(char)*BUFFSIZE) ;
		
	char * fileNameCorr;
	 fileNameCorr = strtok( fileName, "#" ) ;
	
	
	XML_Parser parser = XML_ParserCreate(NULL);
	if ( !parser ) {
		printf( "Couldn't allocate memory for parser" ) ;
// 		throw bad_alloc() ;
	}
		
	int done;
	int len ;
	
	XML_SetElementHandler( parser, XMLParserBase_startElement, XMLParserBase_endElement );
		
	FILE *file;
	if( ( file = fopen( fileNameCorr, "r" ) ) == NULL ){
		/// Aca hay que llamar a la funcion fortran de excepciones
		printf( "%s%s%s\n", "Error, ", fileNameCorr , ": no such file or directory" ) ;
		exit(-1);
// 		throw XMLParserExceptions::FileNotFound() ;
	}
	
	for (;;) {		
		len = (int)fread( buff, 1, BUFFSIZE, file );
		if ( ferror(file) ) {
			/// Aca hay que llamar a la funcion fortran de excepciones
			printf( "Read error" ) ;
			exit(-1);
// 			throw XMLParserExceptions::ReadError() ;
		}
		done = feof( file );
			
		if ( XML_Parse( parser, buff, len, done ) == XML_STATUS_ERROR ) {
			/// Aca hay que llamar a la funcion fortran de excepciones
			//void abort( void );
			printf( "%s%d %s %s %s\n", "Parse error at line ", XML_GetCurrentLineNumber(parser)-1, " of ",fileNameCorr , " xml file" ) ;
			//printf( "%s%d\n", "  Message: ", XML_ErrorString(XML_GetErrorCode(parser)) ) ;
			exit(-1);
			// 			throw XMLParserExceptions::ReadErrorAtLine( parser ) ;
		}
			
		if (done)
			break;
	}
	
	fclose(file) ;
	free(buff) ;
	XML_ParserFree( parser );
}
