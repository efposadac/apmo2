!!**********************************************************************************
!!
!!    Copyright (C) 2008-2009 by
!!
!!                       Universidad Nacional de Colombia
!!                          Grupo de Quimica Teorica
!!                        http://www.gqt-un.unal.edu.co
!!
!!                             Original Authors:
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)
!!                  Edwin F. Posada C. (efposadac@unal.edu.co)
!!
!!                              Contributors:
!!
!!    This program is free software; you can redistribute it and/or modify
!!    it under the terms of the GNU General Public License as published by
!!    the Free Software Foundation; either version 2 of the License, or
!!    (at your option) any later version.
!!
!!    This program is distributed in the hope that it will be useful,
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!    GNU General Public License for more details.
!!
!!    You should have received a copy of the GNU General Public License
!!    along with this program. If not, write to the Free Software Foundation,
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
!!
!!**********************************************************************************
!<
! @brief  Fuente del programa principal
!
! @author Sergio A. Gonzalez Monico
!
! <b> Fecha de creacion: </b> 2006-03-10
!
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2007-05-15 </tt>: Sergio A. Gonzalez M. ( sagonzalezm@unal.edu.co )
!        -# Creacion de archivo y metodos basicos
!>
program apmo
	 use APMO_
	 use MolecularSystem_
	 use Solver_
	 use MecanicProperties_
	 use GeometryOptimizer_
	 use InputOutput_
	 use FormatsConverter_
     use Stopwatch_
     use ExternalSoftware_
     use ExternalPotentialManager_
     implicit none

     call Stopwatch_constructor( apmo_stopwatch )
     call Stopwatch_start( apmo_stopwatch )



   	 write (6,*) "Begin APMO execution at : ", trim( Stopwatch_getCurretData( apmo_stopwatch ) )
   	 write (6,*) "---------------------------------------------------------------"
   	 write (6,*) ""

     call APMO_constructor()
     call APMO_showCredits()
     call MolecularSystem_constructor(MolecularSystem_instance)
     call MolecularSystem_loadOfInput()

     call APMO_showParameters()
     call MolecularSystem_showInformation()

     call ParticleManager_showParticlesInformation()

     !!***********************************************************************
     !!        Muestra la geometria inicial de sistema
     !!***********************************************************************
     write (6,"(T20,A30)") " INITIAL GEOMETRY: AMSTRONG"
     write (6,"(T18,A35)") "------------------------------------------"
     call MolecularSystem_showCartesianMatrix()
     call MecanicProperties_constructor( MolecularSystem_instance%mecProperties )
     if (APMO_instance%TRANSFORM_TO_CENTER_OF_MASS .and. .not.APMO_instance%ARE_THERE_DUMMY_ATOMS) then
          call MolecularSystem_moveToCenterOfMass( MolecularSystem_instance )
          call MolecularSystem_rotateOnPrincipalAxes( MolecularSystem_instance )
          write (6,"(T20,A30)") " GEOMETRY IN C.M. : AMSTRONG"
          write (6,"(T18,A35)") "------------------------------------------"
          call MolecularSystem_showCartesianMatrix()
     end if
     call MolecularSystem_showDistanceMatrix()
     call MolecularSystem_showZMatrix( MolecularSystem_instance )
     !!
     !!***********************************************************************


     call Solver_constructor( apmo_solver, trim(APMO_instance%METHOD) )

     if  (APMO_instance%OPTIMIZE .and. .not. APMO_instance%ELECTRONIC_WAVEFUNCTION_ANALYSIS &
          .and. .not.APMO_instance%ARE_THERE_DUMMY_ATOMS .and. .not. APMO_instance%CP_CORRECTION ) then

          call GeometryOptimizer_constructor( apmo_geometryOptimizer, ssolver = apmo_solver)
          call GeometryOptimizer_run( apmo_geometryOptimizer )
          call GeometryOptimizer_destructor( apmo_geometryOptimizer )

     else&
     if ( APMO_instance%CP_CORRECTION ) then

          call ExternalSoftware_constructor( external_instance )
          call ExternalSoftware_makeBSSEwithCP( external_instance )
     else

          call Solver_run( apmo_solver )

     end if

 	call Solver_show( apmo_solver )


     call FormatsConverter_writeMoldenFile( MolecularSystem_instance )

     if (APMO_instance%IS_THERE_EXTERNAL_POTENTIAL) &
	  call ExternalPotentialManager_draw2DPotential(externalPotentialManager_instance)
     
     call Solver_destructor( apmo_solver )
     call MolecularSystem_destructor(MolecularSystem_instance)
     call InputOutput_destructor()
     call APMO_destructor()

     call Stopwatch_stop( apmo_stopwatch )
     print *,""
     write (6,"(A16,ES10.2,A4)") "Enlapsed Time : ", apmo_stopwatch%enlapsetTime ," (s)"
     write (*,*) "APMO execution terminated normally at : ", trim( Stopwatch_getCurretData( apmo_stopwatch ) )
     call Stopwatch_destructor( apmo_stopwatch )

end program apmo
