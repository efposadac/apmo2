/***************************************************************************
 *   Copyright (C) 2007 by Universidad Nacional de Colombia                *
 *   http://www.unal.edu.co                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef CONTWAVE_H
#define CONTWAVE_H

#include <cstdlib>
#include <iostream>
#include <string>
using namespace std ;

/**
 *  @brief
 *  @author N�stor Aguirre
 *
 *  <b> Fecha de creaci�n : </b> 2007-07-12
 */
class APMO{
	static APMO* instance ;
	
	protected:
		APMO();
		~APMO();
		
	public:
		static APMO* Instance() ;
		
		uint getDebugLevel() const ;
		void setDebugLevel( uint debugLevel ) ;
		
		bool isIntegralsInMemory() ;
		void putIntegralsInMemory( bool integralsInMemory ) ;
		
		double getDoubleThresholdComparison() const ;
		void setDoubleThresholdComparison( double doubleThresholdComparison ) ;
		
		void setShowSymbols( bool showSymbols ) ;
		bool isShowSymbols() const ;
		
		string APMO_HOME() const ;
		string APMO_DATA() const ;
		
	private:
		uint debugLevel ;
		bool integralsInMemory ;
		double doubleThresholdComparison ;
		bool showSymbols ;
};

#endif
