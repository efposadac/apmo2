/***************************************************************************
 *   Copyright (C) 2007 by Universidad Nacional de Colombia                *
 *   http://www.unal.edu.co                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "APMO.h"

#ifdef __cplusplus
extern "C"
{
#endif
	#include <arch.h>

	#define finitializateNonBOA FNAME(fcontrol,initializatenonboa)
	
	void finitializateNonBOA() ;
	
#ifdef __cplusplus
}
#endif


APMO::APMO()
{
	debugLevel = 0 ;
	doubleThresholdComparison = 1e-6 ;
	integralsInMemory = false ;
	showSymbols = true ;
	
	finitializateNonBOA() ;
}

APMO::~APMO()
{
}

APMO* APMO::instance = NULL ;
APMO* APMO::Instance()
{
	if( instance == NULL )
		instance = new APMO() ;
	return instance ;
}

uint APMO::getDebugLevel() const
{
	return debugLevel ;
}

void APMO::setDebugLevel( uint debugLevel )
{
	this->debugLevel = debugLevel ;
}

bool APMO::isIntegralsInMemory()
{
	return integralsInMemory ;
}

/**
 * @deprecated
 * @param integralsInMemory 
 */
void APMO::putIntegralsInMemory( bool integralsInMemory )
{
	this->integralsInMemory = integralsInMemory ;
}

double APMO::getDoubleThresholdComparison() const
{
	return doubleThresholdComparison ;
}

void APMO::setDoubleThresholdComparison( double doubleThresholdComparison )
{
	this->doubleThresholdComparison = doubleThresholdComparison ;
}

void APMO::setShowSymbols( bool showSymbols )
{
	this->showSymbols = showSymbols ;
}

bool APMO::isShowSymbols() const
{
	return showSymbols ;
}

string APMO::APMO_HOME() const
{
	char* home = getenv("APMO_HOME") ;
	
	if( home )
		return string( home ) ;
	else
		cerr << "### ERROR ###: Environment variable APMO_HOME not defined" << endl ;
	
	return "." ;
}

string APMO::APMO_DATA() const
{
	char* data = getenv("APMO_DATA") ;
	
	if( data )
		return string( data ) ;
	else
		cerr << "### ERROR ###: Environment variable APMO_DATA not defined" << endl ;
	
	return "." ;
}

