#!/bin/bash

APMO_PATH=`grep apmovars.sh $HOME/.bashrc | gawk '{print $2}'`
source ${APMO_PATH}
PREFIX=${APMO_HOME}
if [ `whoami` != "root" ]
then
	echo -n "Uninstalling APMO Package from " $PREFIX" ..."
	cat $HOME/.bashrc | awk '( !($0~/apmovars.sh/) ){ print $0 }' > .profileTMP
	mv .profileTMP $HOME/.bashrc
	rm -rf $PREFIX

	echo "OK"
else
	echo -n "Uninstalling APMO Package from /opt/apmo ..."
	
	cat /etc/profile | awk '( !($0~/apmovars.sh/) ){ print $0 }' > .profileTMP
	mv .profileTMP /etc/profile
	rm -rf /opt/.apmo
		
	echo "OK"
fi
