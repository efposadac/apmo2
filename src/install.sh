#!/bin/bash

echo "====================================="
echo "APMO (Any Particle Molecular Orbital)"
echo "====================================="
echo ""
echo "In order to study nuclear quantum effects we have created"
echo "the Any Particle Molecular Orbital computer package (APMO)."
echo "The current version of the APMO code is an implementation"
echo "of the nuclear orbital and molecular orbital approaches (NMO)"
echo "at the Hartree-Fock level of theory. Currently we have applied"
echo "the APMO code to a variety of systems to elucidate the isotope"
echo "effects on electronic wave functions, geometries, dipole"
echo "moments and electron densities. Although our current version"
echo "of the code is slower than regular electronic structure packages,"
echo "our code is robust enough to treat any system containing any"
echo "combination of quantum particles (i.e. electrons, nuclei,"
echo "positrons, muons, etc) within a Hartree-Fock  scheme."
echo ""
echo "Complete instructions are in"
echo "      http://apmo.sourceforge.net"
echo ""
echo "Authors:"
echo "   Sergio A. Gonzalez, Nestor F. Aguirre, Edwin F. Posada, and Andres Reyes"
echo "   Universidad Nacional de Colombia"
echo ""

PREFIX=$HOME/
SCRATCH=/local/

if [ `whoami` != "root" ]
then
    cat apmovars.sh | sed 's/APMO_HOME=/APMO_HOME= /g' | grep -v "APMO_SCRT=" | gawk '{if($1=="APMO_HOME="){print "APMO_HOME='$PREFIX'/.apmo"; print "APMO_SCRT='$SCRATCH'"}else{print $0}}' > log
    mv log apmovars.sh
    
    
    echo -n "Installing APMO Package in " $PREFIX/.apmo
    
    mkdir $PREFIX/.apmo
    cp apmovars.sh $PREFIX/.apmo
    cp uninstall.sh $PREFIX/.apmo
    cp -r bin $PREFIX/.apmo
    cp -r data $PREFIX/.apmo
    
    EXIST_LINE=`grep "apmovars.sh " $HOME/.bashrc`
    
    if [ -z  "$EXIST_LINE" ]
    then
	echo "source $PREFIX/.apmo/apmovars.sh" >> $HOME/.bashrc
    fi
    echo " OK"
else
	echo -n "Installing APMO Package /opt/apmo ..."
	
	mkdir /opt/apmo
	cp -r bin /opt/apmo/
	cp -r data /opt/apmo/
	
	EXIST_LINE=`grep "apmovars.sh " /etc/profile`
	
	if [ -z  "$EXIST_LINE" ]
	then
		echo "source /opt/apmo/apmovars.sh" >> /etc/profile
	fi
	
	echo " OK"
fi

