#! /bin/sh

APMO_HOME=$HOME/.apmo
APMO_SCRT=/scratch/$USER

if [ -z "$APMO_HOME" ]
then
    export APMO_HOME
fi

APMO_DATA="$APMO_HOME/data"  #Debe eliminarse
export APMO_DATA             #Debe eliminarse

if [ -z "$APMO_DATA" ]
then
    APMO_DATA="$APMO_HOME/data"
    export APMO_DATA
fi

if [ -z "$APMO_SCRT" ]
then
    export APMO_SCRT
fi


if [ -z "$PATH" ]
then
    PATH="$APMO_HOME/bin:$APMO_HOME/utils:"
    export PATH
else
    PATH="${PATH}:$APMO_HOME/bin:$APMO_HOME/utils"
    export PATH
fi

