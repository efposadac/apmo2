!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief programa para listas de numeros reales 
!
! @author Lalita S. Uribe
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Lalita S. Uribe ( lsuribeo@unal.edu.co )
!        -# Creacion del archivo de prueba
! @verificar en funcionamiento de la rutina que genera las matrices para el
! calculo de la descomposición de energia
!**
program EnergyDecompositionTest
	use Matrix_
	use EnergyDecomposition_
	implicit none

	type(Matrix) :: input
	type(Matrix) :: output
	integer(8) :: numRowMatrix
	integer(8) :: numColMatrix
	integer(8) :: indexEs(12)
	integer(8) :: indexMon(4)

	numRowMatrix=18
	numColMatrix=18

	call Matrix_constructor(input,numRowMatrix,numColMatrix)
	call Matrix_constructor(output,numRowMatrix,numColMatrix)

	input%values=0.0_8
	output=EnergyDecomposition_electrostaticEnergy( input)

	indexEs=(/1,3,4,6,7,9,10,12,13,15,16,18/)
	output=EnergyDecomposition_polarizationEnergy( input, indexEs)
	output=EnergyDecomposition_exchangeEnergy( input, indexEs)
	indexMon=(/1,3,10,12/)
	output=EnergyDecomposition_chargeTransfer( input, indexEs, indexMon)


end program EnergyDecompositionTest
