!!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, atracci�n integrals, recursive integrals,     !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de atracci�n entre funciones gaussianas
! primitivas sin normalizar, determinando el tiempo requerido para el c�lculo.
!
program primitiveGaussian_attractionTest
	use PrimitiveGaussian_
	use LibintInterface_
	use AttractionIntegrals_
	implicit none
	type(PrimitiveGaussian) :: gA , gB, gC, gD
	real(8), allocatable :: out(:)
	real(8):: originPuntualCharge(3)
	real :: timeBegin , timeEnd
	integer :: i
	
	!!
	!! Definci�n de funciones gausianas y origen de la carga puntual. 
	!!
	call PrimitiveGaussian_constructor(gA,[2.0_8,1.0_8,-5.0_8],0.2_8, 1_8)
	call PrimitiveGaussian_constructor(gB,[-4.0_8,3.0_8,-2.0_8],0.2_8, 0_8)
	call PrimitiveGaussian_constructor(gC,[1.0_8,2.0_8,-3.0_8],0.2_8, 0_8)
	call PrimitiveGaussian_constructor(gD,[-5.0_8,1.0_8,-2.0_8],0.2_8, 0_8)
!	originPuntualCharge = [0.0_8,1.0_8,-0.5_8]
!
!	!! Imprime informaci�n sobre la gausianas construidas
	print *,"NV"
	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	
	!!***************************************************
	!! Inicio de calculo de integrales de atracci�n
	!!
	
	call cpu_time(timeBegin)
	!! (s|A(0)|s) 
!	call PrimitiveGaussian_attractionIntegralRecursive(gA,gB,originPuntualCharge, out)
	call LibintInterface_constructor(LibintInterface_instance, 3, 6, "ERIS")

	do i = 1, 3
		call LibintInterface_compute( gA, gB, gA, gB, out)
		print*, i
	end do


	do i = 1, size(out)
		print *, "integral", out(i)
	end do

	print*, cartesianOrbitalIndex
	out = 0.0_8
	!(p_x|A(0)|s)
	call PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge,out)
	print *,"(p_x|A(0)|s)=",out
!
!	!!(p_y|A(0)|s)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|s)=",out
!
!	!!(p_z|A(0)|s)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|s)=",out
!
!	!!(s|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|p_x)=",out
!
!	!!(s|A(0)|p_y)
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|p_y)=",out
!
!	!!(s|A(0)|p_z)
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|p_z)=",out
!
!	!!(d_xx|A(0)|s)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|s)=",out
!
!	!!(d_xy|A(0)|s)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|s)=",out
!
!	!!(d_yy|A(0)|s)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|s)=",out
!
!	!!(d_yz|A(0)|s)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|s)=",out
!
!	!!(d_xz|A(0)|s)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|s)=",out
!
!	!!(d_zz|A(0)|s)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|s)=",out
!
!	!!(s|A(0)|d_xx)
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_xx)=",out
!
!	!!(s|A(0)|d_xy)
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_xy)=",out
!
!	!!(s|A(0)|d_yy)
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_yy)=",out
!
!	!!(s|A(0)|d_yz)
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_yz)=",out
!
!	!!(s|A(0)|d_xz)
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_xz)=",out
!
!	!!(s|A(0)|d_zz)
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(s|A(0)|d_zz)=",out
!
!	!!(p_x|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|p_x)=",out
!
!	!!(p_x|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|p_y)=",out
!
!	!!(p_x|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|p_z)=",out
!
!	!!(p_y|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|p_x)=",out
!
!	!!(p_y|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|p_y)=",out
!
!	!!(p_y|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|p_z)=",out
!
!	!!(p_z|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|p_x)=",out
!
!	!!(p_z|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|p_y)=",out
!
!	!!(p_z|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|p_z)=",out
!
!	!!(d_xx|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_x)=",out
!
!
!	!!(d_xy|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_x)=",out
!
!	!!(d_yy|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|p_x)=",out
!
!	!!(d_yz|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|p_x)=",out
!
!	!!(d_xz|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|p_x)=",out
!
!	!!(d_zz|A(0)|p_x)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|p_x)=",out
!
!	!!(d_xx|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_y)=",out
!
!	!!(d_xy|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_y)=",out
!
!	!!(d_yy|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|p_y)=",out
!
!	!!(d_yz|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|p_y)=",out
!
!	!!(d_xz|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|p_y)=",out
!
!	!!(d_zz|A(0)|p_y)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|p_y)=",out
!
!	!!(d_xx|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_z)=",out
!
!	!!(d_xy|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|p_z)=",out
!
!	!!(d_yy|A(0)|p_z)-
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|p_z)=",out
!
!	!!(d_yz|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|p_z)=",out
!
!	!!(d_xz|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|p_z)=",out
!
!	!!(d_zz|A(0)|p_z)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|p_z)=",out
!
!	!!(p_x|d_xx)
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_xx)=",out
!
!	!!(p_x|d_xy)
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_xx)=",out
!
!	!!(p_x|d_yy)
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_yy)=",out
!
!	!!(p_x|d_yz)
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_yz)=",out
!
!	!!(p_x|d_xz)
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_xz)=",out
!
!	!!(p_x|d_zz)
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_x|A(0)|d_zz)=",out
!
!	!!(p_y|d_xx)
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|d_xx)=",out
!
!	!!(p_y|d_xy)
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|d_xx)=",out
!
!	!!(p_y|d_yy)
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|p_y)=",out
!
!	!!(p_y|d_yz)
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|d_yz)=",out
!
!	!!(p_y|d_z)
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|d_xz)=",out
!
!	!!(p_y|d_zz)
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_y|A(0)|d_zz)=",out
!
!	!!(p_z|d_xx)
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_xx)=",out
!
!	!!(p_z|d_xy)
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_xx)=",out
!
!	!!(p_z|d_yy)-
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_yy)=",out
!
!	!!(p_z|d_yz)
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_yz)=",out
!
!	!!(p_z|d_xz)
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_xz)=",out
!
!	!!(p_z|d_zz)
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(p_z|A(0)|d_zz)=",out
!
!	!!(d_xx|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_xx)=",out
!
!	!!(d_xy|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_xx)=",out
!
!	!!(d_yy|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_xx)=",out
!
!	!!(d_yz|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_xx)=",out
!
!	!!(d_xz|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_xx)=",out
!
!	!!(d_zz|A(0)|d_xx)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_xx)=",out
!
!	!!(d_xx|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_xy)=",out
!
!	!!(d_xy|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_xy)=",out
!
!	!!(d_yy|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_xy)=",out
!
!	!!(d_yz|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_xy)=",out
!
!	!!(d_xz|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_xy)=",out
!
!	!!(d_zz|A(0)|d_xy)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_xy)=",out
!
!	!!(d_xx|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_yy)=",out
!
!	!!(d_xy|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_yy)=",out
!
!	!!(d_yy|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_yy)=",out
!
!	!!(d_yz|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_yy)=",out
!
!	!!(d_xz|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_yy)=",out
!
!	!!(d_zz|A(0)|d_yy)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_yy)=",out
!
!	!!(d_xx|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_yz)=",out
!
!	!!(d_xy|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_yz)=",out
!
!	!!(d_yy|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_yz)=",out
!
!	!!(d_yz|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_yz)=",out
!
!	!!(d_xz|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_yz)=",out
!
!	!!(d_zz|A(0)|d_yz)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_yz)=",out
!
!	!!(d_xx|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_xz)=",out
!
!	!!(d_xy|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_xz)=",out
!
!	!!(d_yy|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_xz)=",out
!
!	!!(d_yz|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_xz)=",out
!
!	!!(d_xz|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_xz)=",out
!
!	!!(d_zz|A(0)|d_xz)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_xz)=",out
!
!	!!(d_xx|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xx|A(0)|d_zz)=",out
!
!	!!(d_xy|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xy|A(0)|d_zz)=",out
!
!	!!(d_yy|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yy|A(0)|d_zz)=",out
!
!	!!(d_yz|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_yz|A(0)|d_zz)=",out
!
!	!!(d_xz|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_xz|A(0)|d_zz)=",out
!
!	!!(d_zz|A(0)|d_zz)
!	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
!	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
!	out=PrimitiveGaussian_attractionIntegral(gA,gB,originPuntualCharge)
!	print *,"(d_zz|A(0)|d_zz)=",out
!
!	call cpu_time(timeEnd)
!	print *,"Time=", timeEnd - timeBegin , &
!	"sec - (Time by 100 Attraction integrals over s,p and d CGF)"
!	!!***************************************************
!
!	!!***************************************************
!	!! Calcula el tiempo requerido para 5000 integrales
!	!!
!
!	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
!	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
!	call cpu_time(timeBegin)
!
!	do i=1,5000
!		out=PrimitiveGaussian_attractionIntegral( gA , gB , originPuntualCharge)
!	end do
!
	call cpu_time(timeEnd)
	print *,"Time=", timeEnd - timeBegin , &
	"sec - (Time by 50 000 000 Attraction integrals over (dd|dd) CGF)"
!	!!***************************************************

end program primitiveGaussian_attractionTest
