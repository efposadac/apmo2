!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. González Mónico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de overlap entre funciones gaussianas
! primitivas sin normalizar, determinando el tiempo requerido para el calculo.
!
program PrimitiveGaussian_overlapTest
	use PrimitiveGaussian_
	use OverlapIntegrals_
	implicit none
	type(PrimitiveGaussian)::gA,gB
	real(8)::out
	integer::i
	real::timeBegin,timeEnd
	
	!!
	!! Definción de funciones gausianas. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8)
	
	!! Imprime información sobre la gausianas construidas

	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	
	!!***************************************************
	!! Inicio de calculo de integrales de overlap
	!!
	
	! call cpu_time(timeBegin)
	! !(s|s) 
	! out=PrimitiveGaussian_overlapIntegral(gA,gB) 
	! print *,"(s|s)=",out
	
	! !(p_x|s) 
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|s)=",out
	
	! !(p_y|s)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|s)=",out
	
	! !(p_z|s)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|s)=",out
	
	! !(s|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|p_x)=",out
	
	! !(s|p_y)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|p_y)=",out
	
	! !(s|p_z)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|p_z)=",out
	
	! !(d_xx|s)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|s)=",out
	
	! !(d_xy|s)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|s)=",out
	
	! !(d_yy|s)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|s)=",out
	
	! !(d_yz|s)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|s)=",out
	
	! !(d_xz|s)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|s)=",out
	
	! !(d_zz|s)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|s)=",out
	
	! !(s|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_xx)=",out
	
	! !(s|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_xy)=",out
	
	! !(s|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_yy)=",out
	
	! !(s|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_yz)=",out
	
	! !(s|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_xz)=",out
	
	! !(s|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(s|d_zz)=",out
	
	! !(p_x|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|p_x)=",out
	
	! !(p_x|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|p_y)=",out
	
	! !(p_x|p_z)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! gA%angularMomentIndex=[1_8,0_8,0_8]
	! gB%angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|p_z)=",out
	
	! !(p_y|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|p_x)=",out
	
	! !(p_y|p_y)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|p_y)=",out
	
	! !(p_y|p_z)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|p_z)=",out
	
	! !(p_z|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|p_x)=",out
	
	! !(p_z|p_y)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|p_y)=",out
	
	! !(p_z|p_z)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|p_z)=",out
	
	! !(d_xx|p_x)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_x)=",out
	
	! !(d_xy|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_x)=",out
	
	! !(d_yy|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|p_x)=",out
	
	! !(d_yz|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|p_x)=",out
	
	! !(d_xz|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|p_x)=",out
	
	! !(d_zz|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|p_x)=",out
	
	! !(d_xx|p_y)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_y)=",out
	
	! !(d_xy|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_y)=",out
	
	! !(d_yy|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|p_y)=",out
	
	! !(d_yz|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|p_y)=",out
	
	! !(d_xz|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|p_y)=",out
	
	! !(d_zz|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|p_y)=",out
	
	
	! !(d_xx|p_z)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_z)=",out
	
	! !(d_xy|p_z)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|p_z)=",out
	
	! !(d_yy|p_z)-
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|p_z)=",out
	
	! !(d_yz|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|p_z)=",out
	
	! !(d_xz|p_z)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|p_z)=",out
	
	! !(d_zz|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|p_z)=",out
	
	! !(p_x|d_xx)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_xx)=",out
	
	! !(p_x|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_xx)=",out
	
	! !(p_x|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_yy)=",out
	
	! !(p_x|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_yz)=",out
	
	! !(p_x|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_xz)=",out
	
	! !(p_x|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_x|d_zz)=",out
	
	! !(p_y|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|d_xx)=",out
	
	! !(p_y|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|d_xx)=",out
	
	! !(p_y|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|p_y)=",out
	
	! !(p_y|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|d_yz)=",out
	
	! !(p_y|d_z)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|d_xz)=",out
	
	! !(p_y|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_y|d_zz)=",out
	
	! !(p_z|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_xx)=",out
	
	! !(p_z|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_xx)=",out
	
	! !(p_z|d_yy)-
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_yy)=",out
	
	! !(p_z|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_yz)=",out
	
	! !(p_z|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_xz)=",out
	
	! !(p_z|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(p_z|d_zz)=",out
	
	! !(d_xx|d_xx)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_xx)=",out
	
	! !(d_xy|d_xx)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_xx)=",out
	
	! !(d_yy|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_xx)=",out
	
	! !(d_yz|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_xx)=",out
	
	! !(d_xz|d_xx)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_xx)=",out
	
	! !(d_zz|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_xx)=",out
	
	! !(d_xx|d_xy)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_xy)=",out
	
	! !(d_xy|d_xy)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_xy)=",out
	
	! !(d_yy|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_xy)=",out
	
	! !(d_yz|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_xy)=",out
	
	! !(d_xz|d_xy)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_xy)=",out
	
	! !(d_zz|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_xy)=",out
	
	! !(d_xx|d_yy)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_yy)=",out
	
	! !(d_xy|d_yy)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_yy)=",out
	
	! !(d_yy|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_yy)=",out
	
	! !(d_yz|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_yy)=",out
	
	! !(d_xz|d_yy)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_yy)=",out
	
	! !(d_zz|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_yy)=",out
	
	! !(d_xx|d_yz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_yz)=",out
	
	! !(d_xy|d_yz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_yz)=",out
	
	! !(d_yy|d_yz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_yz)=",out
	
	! !(d_yz|d_yz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_yz)=",out
	
	! !(d_xz|d_yz)
	
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_yz)=",out
	
	! !(d_zz|d_yz)
	
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_yz)=",out
	
	! !(d_xx|d_xz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_xz)=",out
	
	! !(d_xy|d_xz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_xz)=",out
	
	! !(d_yy|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_xz)=",out
	
	! !(d_yz|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_xz)=",out
	
	! !(d_xz|d_xz)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_xz)=",out
	
	! !(d_zz|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_xz)=",out
	
	! !(d_xx|d_zz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xx|d_zz)=",out
	
	! !(d_xy|d_zz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xy|d_zz)=",out
	
	! !(d_yy|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yy|d_zz)=",out
	
	! !(d_yz|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_yz|d_zz)=",out
	
	! !(d_xz|d_zz)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_xz|d_zz)=",out
	
	! !(d_zz|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_overlapIntegral(gA,gB)
	! print *,"(d_zz|d_zz)=",out
	
	! call cpu_time(timeEnd)
	! print *,"Time=",timeEnd-timeBegin,&
	! "sec - (Time by 100 integrals for overlap integrals over s,p and d CGF)"
	! !!***************************************************

	! print *,""
	! print *,"Change origin Tests"
	! print *,"=========================="
	! print *,""
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	
	! !(gA|ga) 
	! call PrimitiveGaussian_set(gA, origin=[1.0_8,2.3_8,10.5_8] )
	! call PrimitiveGaussian_set(gB, origin=[1.0_8,2.3_8,10.5_8] )
	! print *,"origin gA: 1.0,2.3,10.5"
	! print *,"origin gB: 1.0,2.3,10.5"
	! out=PrimitiveGaussian_overlapIntegral(gA,gA) 
	! print *,"(gA|gA)=",out
	! out=PrimitiveGaussian_overlapIntegral(gB,gB) 
	! print *,"(gB|gB)=",out
	! out=PrimitiveGaussian_overlapIntegral(gA,gB) 
	! print *,"(gA|gB)=",out
		
	! print *,"origin gA: 5.0,2.0,1.5"
	! print *,"origin gB: 5.0,2.0,1.5"
	
	! call PrimitiveGaussian_set(gA, origin=[5.0_8,2.69_8,1.5_8] )
	! call PrimitiveGaussian_set(gB, origin=[5.0_8,2.69_8,1.5_8] )
	! out=PrimitiveGaussian_overlapIntegral(gA,gA) 
	! print *,"(gA|gA)=",out
	! out=PrimitiveGaussian_overlapIntegral(gB,gB) 
	! print *,"(gB|gB)=",out
	! out=PrimitiveGaussian_overlapIntegral(gA,gB) 
	! print *,"(gA|gB)=",out
	
	! !!***************************************************
	! !! Calcula el tiempo requerido para 5000 integrales
	! !!
	
	! print *,""
	! print *,"Multiple Integrals Tests"
	! print *,"=========================="
	! print *,""
	
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	
	! call cpu_time(timeBegin)
	! do i=1,5000
	! 	out=( PrimitiveGaussian_overlapIntegral(gA,gB) ) 
	! end do
	! call cpu_time(timeEnd)
	! print *,"Time=",timeEnd-timeBegin, &
	! "sec - (Time by 5000 integrals for overlap integrals over (dyz|dxz) CGF)"
	!!*****************************************************
	
end program PrimitiveGaussian_overlapTest
