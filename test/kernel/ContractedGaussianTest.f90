!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de overlap, atracci�n repulsi�n, energ�a
! cin�tica y momento entre funciones gaussianas contraidas.
!**
! <b> Historial de modificaciones: </b>
!
!   - <tt> 2010-10-03 </tt>: Lalita Shaki Uribe ( lsuribeo@unal.edu.co )
!        -# Adecuación a la nueva forma de cálculo de integrales.
program ContractedGaussian_test
	use ContractedGaussian_
	use LibintInterface_
	
	implicit none
	type(ContractedGaussian) :: cA, cB
	type(ContractedGaussian) :: cC, cD
	type(ContractedGaussian), pointer :: cB_pointer
	type(puntualParticle), allocatable :: particle (:)
	real(8), allocatable :: integralValue(:)
	real(8) :: originPuntualCharge(3)
	integer, allocatable :: indexa(:)
	integer :: i

    allocate(particle(1))
    particle%X = 0.0_8
    particle%Y = 0.0_8
    particle%z = 0.0_8
    particle%charge = -1.0_8

        call APMO_constructor ()
        
        originPuntualCharge = [0.0_8,0.0_8,0.0_8]
        
        !!*********************************************                                                                                                      
        !! Prueba de contruccion de objetos                                                                                                                  

        !! Gausiana contraida A                                                                                                                              
        call ContractedGaussian_constructor( cA , [0.168856_8,0.623913_8,3.42525_8] ,&
                [0.444635_8,0.535328_8,0.154329_8], angularMoment=0_8 )
        call ContractedGaussian_show(cA)
        call ContractedGaussian_show(cA,2)
        allocate(integralValue(cA%numCartesianOrbital * cA%numCartesianOrbital))
        !call ContractedGaussian_set(cA,angularMomentIndex=[0_8,0_8,0_8])                                                                                     
        integralValue = ContractedGaussian_overlapIntegral( cA , cA)
        print *,"Overlap = ",integralValue

        !! Gausiana contraida B
        call ContractedGaussian_constructor( cB , [0.168856_8,0.623913_8,3.42525_8] ,&
                [0.444635_8,0.535328_8,0.154329_8],origin = [1.4_8,0.0_8,0.0_8], angularMoment=0_8 )

        call ContractedGaussian_show(cB)
        cB_pointer => ContractedGaussian_getContraction(cB)
        call ContractedGaussian_show(cB_pointer,3)
!
!        !! Gausiana contraida C
!        call ContractedGaussian_constructor( cC , [0.168856_8,0.623913_8,3.42525_8] ,&
!                [0.444635_8,0.535328_8,0.154329_8], angularMoment=0_8 )
!        call ContractedGaussian_show(cC)
!        call ContractedGaussian_show(cA,2)
!        call ContractedGaussian_set(cA,angularMomentIndex=[0_8,0_8,0_8])
!        !output = ContractedGaussian_overlapIntegral( cA , cA)
!        !print *,"Overlap = ",output


!        !! Gausiana contraida D
!        call ContractedGaussian_constructor( cD , [0.168856_8,0.623913_8,3.42525_8] ,&
!                [0.444635_8,0.535328_8,0.154329_8],origin = [1.4_8,0.0_8,0.0_8], angularMoment=0_8 )
!        call ContractedGaussian_show(cD)
!        !cB_pointer => ContractedGaussian_getContraction(cB)
!        !call ContractedGaussian_show(cB_pointer,3)

        !!*********************************************
	
	!!*********************************************
	!! Calculo de integrales
	!!
!	    call LibintInterface_initializeShell( cA%primitives(1) , cA%primitives(1) , cB%primitives(1), cB%primitives(1), &
!	    										1, 4, 5, 6, 6, indexa)
!       call LibintInterface_constructor(LibintInterface_instance, 1, 6, "ERIS")

!       	call  ContractedGaussian_repulsionIntegral( cA , cB , cC, cD, expli, method = "EXPLICIT")
!       	call  ContractedGaussian_repulsionIntegral( cA , cB , cC, cD, recur, method = "RECURSIVE")

 !      	call  ContractedGaussian_kineticIntegral( cA , cB)
 !      	call  ContractedGaussian_kineticIntegral( cA , cB)
   !  	        call  ContractedGaussian_kineticIntegral( cA , cB, expli)


 !     do i = 1, size(recur)
 !          write(*,"(A, F, A, F, A, F)") "   recursive    ", recur(i), "  explicit    ", expli(i), "    diff    ", recur(i)-expli(i)
 !     end do

!	call ContractedGaussian_overlapIntegral( cA , cB, output)
!	print *,"Overlap = ",output
!	output = ContractedGaussian_momentIntegral( cA , cB , [0.0_8,0.0_8,0.0_8] , [1_8,0_8,0_8])
!	print *,"Moment = ",output
!	output = ContractedGaussian_kineticIntegral( cA , cB)
!	print *,"Kinetic = ",output
!	output = ContractedGaussian_attractionIntegral( cA , cB , [0.0_8,0.0_8,0.0_8] )
!	print *,"Attraction = ",output
!	output = ContractedGaussian_repulsionIntegral( cA, cA , cB , cB )
!	print *,"Repulsion = ",output
				if(allocated(integralValue)) deallocate (integralValue)
				allocate(integralValue(cA%numCartesianOrbital * cB%numCartesianOrbital))
       		    integralValue = ContractedGaussian_overlapIntegral( cA , cB)
       			print *,"Overlap = ",integralValue

       			if(allocated(integralValue)) deallocate (integralValue)
       			allocate(integralValue(cA%numCartesianOrbital * cB%numCartesianOrbital))
       		    integralValue =  ContractedGaussian_momentIntegral( cA , cB , [0.0_8,0.0_8,0.0_8] , 1 )
       			print *,"Moment = ",integralValue

   				if(allocated(integralValue)) deallocate (integralValue)
   				allocate(integralValue(cA%numCartesianOrbital * cB%numCartesianOrbital))
       	        call  ContractedGaussian_kineticIntegral( cA , cB, integralValue)
       	        print *,"Kinetic = ",integralValue

       	        if(allocated(integralValue)) deallocate (integralValue)
   				allocate(integralValue(cA%numCartesianOrbital * cB%numCartesianOrbital))
       	        call  ContractedGaussian_attractionIntegral( cA , cB, particle, 1, integralValue)
       	        print *,"Attraction = ",integralValue

!	       	    if(allocated(integralValue)) deallocate (integralValue)
!   				allocate(integralValue(cA%numCartesianOrbital * cA%numCartesianOrbital * cB%numCartesianOrbital * cA%numCartesianOrbital))
!       	        call  ContractedGaussian_repulsionIntegral( cA , cA, cB, cB, integralValue)
!       	        print *,"Repulsion = ",integralValue
	             print *,"Falta arreglar la integral de repulsión"
	!!*********************************************
	
	!! Desconstrucci�n de objetos previamente creados 
	call ContractedGaussian_destructor(cA)
	call ContractedGaussian_destructor(cB)
	nullify(cB_Pointer)
	
end program ContractedGaussian_test
