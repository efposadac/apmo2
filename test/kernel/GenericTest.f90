!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
program GenericTest
     use BasisSet_
     use BasisSetManager_
     use APMO_
     use ExternalPotentialManager_
     use ExternalPotential_
     implicit none

     type(BasisSet), target :: bs
     call APMO_constructor()

     call BasisSetManager_loadBasisSet ( bs, "DZSPNB","H_1")

     call ExternalPotentialManager_constructor( externalPotentialManager_instance, ["fulerenos"],["H_1"] )

      call ExternalPotential_getInteractionMtx(externalPotentialManager_instance.externalsPots(1), bs.contractions)

      call ExternalPotentialManager_destructor( externalPotentialManager_instance )

end program GenericTest