!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Program de prueba para algoritmo de minimizacion SCF, empleando metodo RHF
!
!**
program SCFIntraspeciesTest
	use RHFWaveFunction_
	use SCFIntraspecies_
	use ParticleManager_
	use IntegralManager_
	use DensityMatrixSCFGuess_
	
	implicit none
	
	integer :: i
	integer :: status
	type(Matrix) :: auxDensity
! 	call APMO_constructor()
! 	
! 	call ParticleManager_constructor() 
! 	
! 	!! Ejemplo: 
! 	!!		Obtencion de integrales entre contracciones atómicas para molecula de hidrógeno
! 	!!		cuantica
! 	
! 	print *,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
! 	print *,"EJEMPLO DE CALCULO DE DE ESTRUCTURA ELECTRONICA PARA MOLECULA"
! 	print *,"        DE AGUA EMPLEANDO BASE MINIMA"
! 	print *,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
! 	print *,""
! 	
! 	call ParticleManager_addParticle( "O", baseName="sto-3g", origin=[0.0_8, 0.1557086589_8, 0.0117938483_8])	!< Cargando base electronica
! 	call ParticleManager_addParticle( "H", baseName="sto-3g", origin=[0.0_8, 1.3487883994_8, -1.4277817034_8])	!< Cargando base electronica
! 	call ParticleManager_addParticle( "H", baseName="sto-3g", origin=[0.0_8, 1.3656380988_8, 1.4372476747_8])	!< Cargando base electronica
! 	call ParticleManager_addParticle( "O", origin=[0.0_8, 0.1557086589_8, 0.0117938483_8])	!< Definiendo localizacion de nucleo puntual
! 	call ParticleManager_addParticle( "H", origin=[0.0_8, 1.3487883994_8, -1.4277817034_8])	!< Definiendo localizacion de nucleo puntual
! 	call ParticleManager_addParticle( "H", origin=[0.0_8, 1.3656380988_8, 1.4372476747_8])	!< Definiendo localizacion de nucleo puntual
! 
! 	!! Contruye Administrador de integrales
! 	call IntegralManager_constructor()
! 	
! 	!! Contruyendo objeto para realizacion de cilos SCFIntraspeciesTest
! 	call RHFWaveFunction_constructor()
! 	call SCFIntraspecies_constructor()
! 	
! 	!! Imprime informacion de las particulas en el sistema molecular
! 	call ParticleManager_showParticlesInformation()
! 	
! 	!! Obteniendo matrix de densidad inicial
! 	call DensityMatrixSCFGuess_ones(auxDensity,"e-")
! 	call RHFWaveFunction_setDensityMatrix( auxDensity, "e-" )
! 
! 	!! Construye matrix de overlap
! 	print *,"Matrix de Overlap:"
! 	print *,"------------------"
! 	call RHFWaveFunction_buildOverlapMatrix("e-")
! 	call Matrix_show(  RHFWaveFunction_getOverlapMatrix("e-") )
! 	
! 	!! Construye matrix de transformacion
! 	print *,"Matrix de Transformación:"
! 	print *,"-------------------------"
! 	call RHFWaveFunction_buildTransformationMatrix("e-",1)
! 	call Matrix_show(  RHFWaveFunction_getTransformationMatrix("e-") )
! 	
! 	
! 	!! Construye matrix de Hcore
! 	print *,"Matrix de Hcore:"
! 	print *,"----------------"
! 	call RHFWaveFunction_buildIndependentParticleMatrix("e-")
! 	call Matrix_show(  RHFWaveFunction_getIndependentParticleMatrix("e-") )
! 
! 		
! 	!!***************************************************************************************************
! 	!! Inicia proceso de iteracion SCF
! 	!!
! 		print *,"Realizando ciclo SCF:"
! 		print *,"---------------------"
! 		print *,""
! 		
! 		status =  SCF_INTRASPECIES_CONVERGENCE_CONTINUE
! 		
! 		write (6,"(A10,A12,A25)") "Iteracion", "Energia", " Cambio Densidad"
! 		
! 		do while ( ( status ==  SCF_INTRASPECIES_CONVERGENCE_CONTINUE ) .and. &
! 				(SCFIntraspecies_getNumberOfIterations("electron") <= APMO_instance.SCF_ELECTRONIC_MAX_ITERATIONS ) )
! 			
! 			!! Contruye la matriz de dos particulas (electrones) y la matriz de Fock
! 			call RHFWaveFunction_buildTwoParticlesMatrix("e-")
! 			call RHFWaveFunction_buildFockMatrix("e-")
! 			
! 			
! 			call SCFIntraspecies_iterate( "e-" )
! 			write (6,"(I5,F20.10,F20.10)") SCFIntraspecies_getNumberOfIterations("e-"), SCFIntraspecies_getLastEnergy("e-"), &
! 				SCFIntraspecies_getStandardDeviationOfDensistyMatrix("e-")
! 			 
! 			
! 			!! Prueba la convergencia de ciclo
! 			status = SCFIntraspecies_testDensityMatrixChange("e",1.0E-8_8)
! 			
! 		end do
! 		
! 		!! Indica el estado del proceso SCF al finalizar el ciclo
! 		call SCFIntraspecies_showIteratiosStatus( status, "e-" )
! 		
! 		print *,""
! 		print *,"Energia de Repulsion Nuclear: ", ParticleManager_puntualParticlesEnergy()
! 		print *,""
! 	
! 	!!
! 	!!***************************************************************************************************
! 	
! 	print *,"Matriz de coeficientes:"
! 	print *,"-----------------------"
! 	print *,""
! 	call Matrix_show( RHFWaveFunction_getWaveFunctionCoefficients("e-") )
! 	
! 	print *,"Energias de Orbitales moleculares:"
! 	print *,"----------------------------------"
! 	print *,""
! 	call Vector_show( RHFWaveFunction_getMolecularOrbitalsEnergy("e-") )
	
end program SCFIntraspeciesTest