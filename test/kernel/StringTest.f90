!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
program StringTest
     use String_
     implicit none

     character(24) :: str="aaasdhsdfioufsjkldhfsaljkfhasd"
     integer :: conc

     !! Imprime la cadena origina
     print *,trim(str)

     !! Convierte la cadena en mayusculas
     print *,trim(String_getUppercase(str))
     print *,trim(String_getUppercase(str))

     !! Convierte la cadena en minusculas
     print *,trim(String_getLowercase(str))
     print *,trim(String_getLowercase(str))

     !! Busca una coindincia en la cadena
     print *,'fious: ', String_findSubstring(str,'fious')

     str="holaholeholaholemolehole"
     print *,"Cedena original: "//str
     print *,"hole:",String_findSubstring(str,'holem',conc), conc
     print *,"mole:",String_findSubstring(str,'mole',conc), conc
     print *,"hola:",String_findSubstring(str,'hola',conc), conc
     print *,""
     str="holaholeho ole me ole "
     print *,"Cedena original: "//str
     print *," ole :",String_findSubstring(str,' ole ',conc), conc


end program StringTest