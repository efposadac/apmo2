!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. González Mónico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de energia cinetica entre funciones gaussianas
! primitivas sin normalizar, determinando el tiempo requerido para el calculo.
!
program PrimitiveGaussian_kineticTest
	use PrimitiveGaussian_
	use KineticIntegrals_
	implicit none
	type(PrimitiveGaussian) :: gA , gB
	real(8)::out
	integer::i
	real::timeBegin,timeEnd
	
	!!
	!! Definción de funciones gausianas. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8)
	
	!! Imprime información sobre la gausianas construidas

	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	
	!!***************************************************
	!! Inicio de calculo de integrales de energia cinetica
	!!
	
	call cpu_time(timeBegin)
!	call PrimitiveGaussian_kineticIntegral(gA,gB)
!	print *,"(s|T|s)=",out
	
	! !! (p_x|T|s) 
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|T|s)=",out
	
	! !! (p_y|T|s)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|T|s)=",out
	
	! !! (p_z|T|s)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|T|s)=",out
	
	! !! (s|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|p_x)=",out
	
	! !! (s|T|p_y)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|p_y)=",out
	
	! !! (s|T|p_z)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|p_z)=",out
	
	! !! (d_xx|T|s)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|s)=",out
	
	! !! (d_xy|T|s)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|s)=",out
	
	! !! (d_yy|T|s)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|s)=",out
	
	! !! (d_yz|T|s)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|s)=",out
	
	! !! (d_xz|T|s)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|s)=",out
	
	! !! (d_zz|T|s)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|s)=",out
	
	! !! (s|T|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_xx)=",out
	
	! !! (s|T|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_xy)=",out
	
	! !! (s|T|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_yy)=",out
	
	! !! (s|T|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_yz)=",out
	
	! !! (s|T|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_xz)=",out
	
	! !! (s|T|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(s|T|d_zz)=",out
	
	! !! (p_x|T|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|T|p_x)=",out
	
	! !! (p_x|T|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|T|p_y)=",out
	
	! !! (p_x|T|p_z)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|T|p_z)=",out
	
	! !! (p_y|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|T|p_x)=",out
	
	! !! (p_y|T|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|T|p_y)=",out
	
	! !! (p_y|T|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|T|p_z)=",out
	
	! !! (p_z|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|T|p_x)=",out
	
	! !! (p_z|T|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|T|p_y)=",out
	
	! !! (p_z|T|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|T|p_z)=",out
	
	! !! (d_xx|T|p_x)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_x)=",out
	
	! !! (d_xy|T|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_x)=",out
	
	! !! (d_yy|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|p_x)=",out
	
	! !! (d_yz|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|p_x)=",out
	
	! !! (d_xz|T|p_x)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|p_x)=",out
	
	! !! (d_zz|T|p_x)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|p_x)=",out
	
	
	! !! (d_xx|T|p_y)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_y)=",out
	
	! !! (d_xy|T|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_y)=",out
	
	! !! (d_yy|T|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|p_y)=",out
	
	! !! (d_yz|T|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|p_y)=",out
	
	! !! (d_xz|T|p_y)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|p_y)=",out
	
	! !! (d_zz|T|p_y)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|p_y)=",out
	
	! !! (d_xx|T|p_z)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_z)=",out
	
	! !! (d_xy|T|p_z)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|p_z)=",out
	
	! !! (d_yy|T|p_z)-
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|p_z)=",out
	
	! !! (d_yz|T|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|p_z)=",out
	
	! !! (d_xz|T|p_z)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|p_z)=",out
	
	! !! (d_zz|T|p_z)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|p_z)=",out
	
	! !! (p_x|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_xx)=",out
	
	! !! (p_x|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_xx)=",out
	
	! !! (p_x|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_yy)=",out
	
	! !! (p_x|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_yz)=",out
	
	! !! (p_x|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_xz)=",out
	
	! !! (p_x|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_x|d_zz)=",out
	
	! !! (p_y|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|d_xx)=",out
	
	! !! (p_y|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|d_xx)=",out
	
	! !! (p_y|d_yy)
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|p_y)=",out
	
	! !! (p_y|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|d_yz)=",out
	
	! !! (p_y|d_z)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|d_xz)=",out
	
	! !! (p_y|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_y|d_zz)=",out
	
	! !! (p_z|d_xx)
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_xx)=",out
	
	! !! (p_z|d_xy)
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_xx)=",out
	
	! !! (p_z|d_yy)-
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_yy)=",out
	
	! !! (p_z|d_yz)
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_yz)=",out
	
	! !! (p_z|d_xz)
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_xz)=",out
	
	! !! (p_z|d_zz)
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(p_z|d_zz)=",out
	
	! !! (d_xx|T|d_xx)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_xx)=",out
	
	! !! (d_xy|T|d_xx)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_xx)=",out
	
	! !! (d_yy|T|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_xx)=",out
	
	! !! (d_yz|T|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_xx)=",out
	
	! !! (d_xz|T|d_xx)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_xx)=",out
	
	! !! (d_zz|T|d_xx)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_xx)=",out
	
	! !! (d_xx|T|d_xy)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_xy)=",out
	
	! !! (d_xy|T|d_xy)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_xy)=",out
	
	! !! (d_yy|T|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_xy)=",out
	
	! !! (d_yz|T|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_xy)=",out
	
	! !! (d_xz|T|d_xy)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_xy)=",out
	
	! !! (d_zz|T|d_xy)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_xy)=",out
	
	! !! (d_xx|T|d_yy)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_yy)=",out
	
	! !! (d_xy|T|d_yy)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_yy)=",out
	
	! !! (d_yy|T|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_yy)=",out
	
	! !! (d_yz|T|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_yy)=",out
	
	! !! (d_xz|T|d_yy)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_yy)=",out
	
	! !! (d_zz|T|d_yy)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_yy)=",out
	
	! !! (d_xx|T|d_yz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_yz)=",out
	
	! !! (d_xy|T|d_yz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_yz)=",out
	
	! !! (d_yy|T|d_yz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_yz)=",out
	
	! !! (d_yz|T|d_yz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_yz)=",out
	
	! !! (d_xz|T|d_yz)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_yz)=",out
	
	! !! (d_zz|T|d_yz)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_yz)=",out
	
	! !! (d_xx|T|d_xz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_xz)=",out
	
	! !! (d_xy|T|d_xz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_xz)=",out
	
	! !! (d_yy|T|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_xz)=",out
	
	! !! (d_yz|T|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_xz)=",out
	
	! !! (d_xz|T|d_xz)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_xz)=",out
	
	! !! (d_zz|T|d_xz)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_xz)=",out
	
	! !! (d_xx|T|d_zz)
	! call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xx|T|d_zz)=",out
	
	! !! (d_xy|T|d_zz)
	! call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xy|T|d_zz)=",out
	
	! !! (d_yy|T|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yy|T|d_zz)=",out
	
	! !! (d_yz|T|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_yz|T|d_zz)=",out
	
	! !! (d_xz|T|d_zz)
	! call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_xz|T|d_zz)=",out
	
	! !! (d_zz|T|d_zz)
	! call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	! call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	! out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! print *,"(d_zz|T|d_zz)=",out
	
	
	call cpu_time(timeEnd)
	print *,"Time=",timeEnd-timeBegin , &
	"sec - (Time by 100 kinetic integrals over s,p and d CGF)"
	!!***************************************************
	
	!!***************************************************
	!! Calcula el tiempo requerido para 5000 integrales
	!!
	
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	
	! call cpu_time(timeBegin)
	! do i=1,5000
	! 	out=PrimitiveGaussian_kineticIntegral(gA,gB)
	! end do
	! call cpu_time(timeEnd)
	! print *,"Time=",timeEnd-timeBegin , &
	!"sec - (Time by 5000 kinetic integrals over (dyz|dxz) CGF)"
	!!***************************************************
	
end program PrimitiveGaussian_kineticTest
