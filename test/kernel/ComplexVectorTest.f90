!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! Este programa prueba la construccion y manipulacion de vectores
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-09-08
!   - <tt> 2007-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!   - <tt> 2008-09-08 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Prueba de las nuevas funciones (ComplexVector_plus,ComplexVector_dot,ComplexVector_norm,ComplexVector_getMax)
!**
program ComplexVectorTest
	use ComplexVector_
	use APMO_
	
	implicit none
	
	type(ComplexVector) :: A
	type(ComplexVector) :: B
	type(ComplexVector) :: C
	integer :: i
	integer :: j
	integer :: integerTmp
	complex(8) :: complexTmp
	character(5), allocatable :: myKeys(:)
	
	call APMO_constructor() ;
	
	!!*********************************************
	!! Probando el constructor
	!!**
	complexTmp = cmplx(2.45_8, 1.00_8)
	call ComplexVector_constructor( A, 8, complexTmp )
	
	print *, ""
	print *, " Vector A (HORIZONTAL)"
	print *, "----------------------"
	print *, ""
	call ComplexVector_show( A, flags=HORIZONTAL )
	
	print *, ""
	print *, " Vector A (VERTICAL)"
	print *, "---------------------"
	print *, ""
	call ComplexVector_show( A, flags=VERTICAL )
	
	call ComplexVector_destructor( A )
	!!*********************************************
	
	!!*********************************************
	!! Probando calcular valores propios y vectores propios
	!!**
	complexTmp = cmplx(2.0_8, 1.0_8)
	call ComplexVector_constructor( B, 3 )
	
	complexTmp = cmplx(1.4500_8, 1.1_8)
	call ComplexVector_setElement( B, 1, complexTmp )
	complexTmp = cmplx(3.1580_8, 1.2_8)
	call ComplexVector_setElement( B, 2, complexTmp )
	complexTmp = cmplx(5.1640_8, 1.3_8)
	call ComplexVector_setElement( B, 3, complexTmp )
	
	print *, ""
	print *, " Vector B"
	print *, "----------"
	allocate( myKeys(3) )
	myKeys = (/"    x","    y","    z"/)
	
	call ComplexVector_show( B, keys=myKeys, flags=WITH_KEYS )
	deallocate( myKeys )
	print *, ""
	
	call ComplexVector_destructor( B )
	!!*********************************************
	
	!!*********************************************
	!! Probando operaciones
	!!**
	print *, "---------------------------"
	print *, " Testing vector operations"
	print *, "---------------------------"
	
	complexTmp = cmplx(2.45_8, 1.00_8)
	call ComplexVector_constructor( A, 3, complexTmp )
	call ComplexVector_constructor( B, 3, values=([ complexTmp, 2.0_8*complexTmp, -1.0*complexTmp ]) )
	call ComplexVector_constructor( C, 3 )
	
	print *, ""
	print *, " A = "
	call ComplexVector_show( A )
	
	print *, ""
	print *, " B = "
	call ComplexVector_show( B )
	
	print *, ""
	print *, " A+B"
	C = ComplexVector_plus( A, B )
	call ComplexVector_show( C )
	
	print *, ""
	complexTmp = ComplexVector_dot( A, B )
	print *, " A\dotB = ", complexTmp
	print *, ""
	print *, " norm(A) = ", ComplexVector_norm( A )
	
!! 	allocate( myKeys(3) )
!! 	myKeys = (/"    x","    y","    z"/)
!! 	
!! 	call ComplexVector_swapElements( B, 1, 3 )
!! 	print *, ""
!! 	print *, " swap(B, 1<->3) = "
!! 	call ComplexVector_show( B, flags=VERTICAL+WITH_KEYS )
!! 	deallocate( myKeys )
	
	call ComplexVector_destructor( A )
	call ComplexVector_destructor( B )
	call ComplexVector_destructor( C )
	!!*********************************************
	
	call APMO_destructor() ;
	
end program ComplexVectorTest
