!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de overlap, atracci�n repulsi�n, energ�a
! cin�tica y momento entre funciones gaussianas contraidas.
!**
program NormalizeTest
	use ContractedGaussian_
	use AttractionIntegrals_
	
	implicit none
	type(ContractedGaussian) :: cA
	type(ContractedGaussian) :: cB
	type(ContractedGaussian) :: cC
	type(ContractedGaussian) :: cD
	type(ContractedGaussian) :: cE
	type(ContractedGaussian) :: cF
	type(ContractedGaussian) :: cG
	real(8), allocatable :: output(:)
	real(8):: origin(3)
	integer :: i
	
        call APMO_constructor ()

	!!*********************************************
	!! Prueba de contruccion de objetos

	!! Gausiana contraida A
	call ContractedGaussian_constructor( cA , [202699.0_8, 66030.0_8, 15140.0_8, 4317.0_8, &
			1414.0_8, 523.9_8] ,&
		[0.000812960630_8, 0.006284695643_8, 0.031918454243_8, 0.128793762716_8, &
			0.394580891131_8, 0.541273787047_8] )
	call ContractedGaussian_set(cA,angularMoment=0_8)
	call ContractedGaussian_show(cA)

	print *,"Old: ",cA.contractionCoefficients
	print *,"New: ",cA.contractionCoefficients*cA.normalizationConstant(1)


!	!! Gausiana contraida B
	call ContractedGaussian_constructor( cB , [523.9_8, 207.7_8, 86.54_8] ,&
		[0.183107647503_8, 0.617625795311_8, 0.253810600404_8] )
	call ContractedGaussian_set(cB,angularMoment=0_8)
	call ContractedGaussian_show(cB)

	!! Gausiana contraida C
	call ContractedGaussian_constructor( cC , [2957.000_8, 700.300_8, 224.600_8] ,&
		[0.002238274824_8, 0.181193676248_8, 0.867155529275_8] )
	call ContractedGaussian_set(cC,angularMoment=1_8)
	call ContractedGaussian_show(cC)


	!! Gausiana contraida D
	call ContractedGaussian_constructor( cD , [82.590_8, 33.190_8, 14.200_8] ,&
		[0.343987389211_8, 0.507081411211_8, 0.258990505859_8] )
	call ContractedGaussian_set(cD,angularMoment=1_8)
	call ContractedGaussian_show(cD)


	!! Gausiana contraida B
	call ContractedGaussian_constructor( cE , [14.290_8, 7.438_8, 3.526_8] ,&
		[0.079669487175_8, 0.373491356103_8, 0.605047994804_8] )
	call ContractedGaussian_set(cE,angularMoment=1_8)
	call ContractedGaussian_show(cE)

	!! Gausiana contraida C
	call ContractedGaussian_constructor( cF , [134.800_8, 36.390_8, 12.163_8, 4.341_8] ,&
		[0.176381298209_8, 0.130046287607_8, 0.410464615843_8, 0.582125715500_8] )
	call ContractedGaussian_set(cF,angularMoment=2_8)
	call ContractedGaussian_show(cF)

	
	!!*********************************************
	!! Calculo de integrales
	!!
!	origin = [0.0_8, 1.0_8, 0.0_8]
!	call ContractedGaussian_attractionIntegral( cE , cF, origin, output)
!        do i = 1, size(output)
!           print *, output(i)
!        end do
    if (allocated(output)) deallocate(output)
    allocate(output(cA%numCartesianOrbital * cA%numCartesianOrbital))
	output =  ContractedGaussian_overlapIntegral( cA , cA)
	print *,"Overlap = ",output

    if (allocated(output)) deallocate(output)
    allocate(output(cB%numCartesianOrbital * cB%numCartesianOrbital))
	output = ContractedGaussian_overlapIntegral( cB , cB)
	print *,"Overlap = ",output

    if (allocated(output)) deallocate(output)
    allocate(output(cC%numCartesianOrbital * cC%numCartesianOrbital))
	output = ContractedGaussian_overlapIntegral( cC , cC)
	print *,"Overlap = ",output

    if (allocated(output)) deallocate(output)
    allocate(output(cD%numCartesianOrbital * cD%numCartesianOrbital))
	output = ContractedGaussian_overlapIntegral( cD , cD)
	print *,"Overlap = ",output

    if (allocated(output)) deallocate(output)
    allocate(output(cE%numCartesianOrbital * cE%numCartesianOrbital))
	output = ContractedGaussian_overlapIntegral( cE , cE)
	print *,"Overlap = ",output

    if (allocated(output)) deallocate(output)
    allocate(output(cF%numCartesianOrbital * cF%numCartesianOrbital))
	output = ContractedGaussian_overlapIntegral( cF , cF)
	print *,"Overlap = ",output
!	origin = [0.0_8, 1.0_8, 0.0_8]
!	call ContractedGaussian_attractionIntegral( cE , cF, origin, output)
!        do i = 1, size(output)
!           print *, output(i)
!        end do

   



	!!*********************************************
	
	!! Desconstrucci�n de objetos previamente creados 
	call ContractedGaussian_destructor(cA)
	call ContractedGaussian_destructor(cB)
	call ContractedGaussian_destructor(cC)
	call ContractedGaussian_destructor(cD)
	call ContractedGaussian_destructor(cE)
	call ContractedGaussian_destructor(cF)
		
end program NormalizeTest
