!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. González Mónico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, kinetic integrals, recursive integrals,       !
!!              gaussian functions, analytic derivatives                           !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula derivadas de integrales de energia cinetica entre funciones
! gaussianas primitivas sin normalizar, determinando el tiempo requerido para el calculo.
!
program PrimitiveGaussian_kineticDerivTest
	use PrimitiveGaussian_
	use KineticDerivatives_
	
	implicit none
	
	type(PrimitiveGaussian)::gA,gB
	real(8)::out
	integer::i
	real::timeBegin,timeEnd
	
	!!
	!! Definción de funciones gausianas. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8,owner=1)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8,owner=2)
	
	!! Imprime información sobre la gausianas construidas
	print *,"Analytic"
	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	
	!!***************************************************
	!! Inicio de calculo derivadas de integrales de overlap
	!!
	
	call cpu_time(timeBegin)
	
	!(s|s) 
	print *,""
	print *,"d(Sa|T|Sb)/dRax=",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(Sa|T|Sb)/dRbx=",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(Sa|T|Sb)/dRbz=",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
!	print *,"d(Sa|T|Sa)/dRax=",PrimitiveGaussian_kineticDerivative(gA,gA,2,3)
	
	!!(p|A(0)|s) 
	
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	print *,"d(px|T|s)/dRax  = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(px|T|s)/dRbx  = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(px|T|s)/dRbz  = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
!	print *,"d(px|T|px)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gA,1,3)
	
	!!(p|A(0)|p) 
	
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	print *,"d(px|T|px)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(px|T|px)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(px|T|px)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)

	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	print *,"d(px|T|py)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(px|T|py)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(px|T|py)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
 
	!!(d|A(0)|s) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	print *,"d(dxx|T|s)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxx|T|s)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxx|T|s)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	print *,"d(dxy|T|s)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxy|T|s)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxy|T|s)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	!!(d|A(0)|p) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	print *,"d(dxx|T|px)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxx|T|px)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxx|T|px)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	print *,"d(dxy|T|py)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxy|T|py)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxy|T|py)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	print *,"d(dxy|T|pz)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxy|T|pz)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxy|T|pz)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	!!(d|A(0)|d) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	print *,"d(dxx|T|dxx)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxx|T|dxx)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxx|T|dxx)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	print *,"d(dxy|T|dzz)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxy|T|dzz)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxy|T|dzz)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	print *,"d(dxy|T|dyz)/dRax = ",PrimitiveGaussian_kineticDerivative(gA,gB,1,1)
	print *,"d(dxy|T|dyz)/dRbx = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,1)
	print *,"d(dxy|T|dyz)/dRbz = ",PrimitiveGaussian_kineticDerivative(gA,gB,2,3)
	
	call cpu_time(timeEnd)
	print *,"Time=",timeEnd-timeBegin,&
	"sec - (Time by 38 derivatives of kinetic energy integrals for CGF )"
	
	!!***************************************************
end program PrimitiveGaussian_kineticDerivTest
