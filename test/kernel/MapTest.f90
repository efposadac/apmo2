!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzlez Moniico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions, analytic derivatives                           !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa prueba para el modulo FMap
!
program MapTest 
	use Map_
	
	implicit none
		
	type(Map) ::  m
	
	call Map_constructor(m)
	
	call Map_insert(m, "electron", 1.0_8)
	call Map_insert(m, "proton", 2.0_8)
	call Map_insert(m, "muon", 3.0_8)
	call Map_insert(m, "oxygen", 4.0_8)
	call Map_insert(m, "hydrogen", 5.0_8)
	
	call Map_show(m)
	print *," Tama�o del mapa: ", Map_size(m)
	print *,"Iterador del muon", Map_find(m,"muon")
	
	call Map_insert(m, "protio", 7.0_8)
	
	call Map_erase(m, "oxygen")
	
	call Map_show(m)
	print *," Tama�o del mapa: ", Map_size(m)
	print *,"Iterador del proton", Map_find(m,"proton")
	
	call Map_swap(m,"muon", "electron")
	call Map_show(m)
	
	print *,"Buscado keys en el el mapa:"
	print *,""
	!! Busca un elemento existente en el mapa
	print *,"Como Key: hydrogen existe iterador > 0: ",Map_find(m,"hydrogen")
	
	!! Busca un elemento inexistente en el mapa
	print *,"Como Key pion no existe iterador = 0: ",Map_find(m,"pion")
	
	!! Trata de insertar un key ya existente
	call Map_insert(m, "hydrogen", 6.0_8)
	
	
	

end program MapTest
