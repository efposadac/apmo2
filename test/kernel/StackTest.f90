!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief programa para probar pilas de datos reales 
!
! @author Sergio A. Gonzalez
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo de prueba
! @todo verificar la liberacion de memoria al eliminar elementos de la pila
!**
program StackTest
	use Stack_
	implicit none

	type(Stack) :: stack_instance
	integer  i

	!! Construye una pila de numero reales
	call Stack_constructor(stack_instance,"testStack",20)


	!! Verifica el estado de la pila
	print *,"La pila est� libre: ", Stack_empty(stack_instance)

	!! Adiciona elementos a la pila
	print *,""
	print *,"Adicionando elementos a la pila"
	do i=1,20

		call Stack_push( stack_instance, real(i * 3.5_8,8) )
		print *, "Size: ", Stack_size(stack_instance),"Data: ",Stack_top(stack_instance)
	end do

	!! Elimina elementos a la pila
	print *,""
	print *,"Eliminado elemento de la pila"
	do i=1,22
		call Stack_pop(stack_instance)
	        print *, "Size: ", Stack_size(stack_instance), "Data: ",Stack_top(stack_instance)
	end do


	!! Verifica el estado de la pila
	print *,"La pila est� libre: ", Stack_empty(stack_instance)

	!! Adiciona elementos a la pila
	print *,""
	print *,"Adicionando nuevos elementos a la pila"
	do i=1,25

		call Stack_push( stack_instance, real(i * 1.5_8,8) )
		print *, "Size: ", Stack_size(stack_instance),"Data: ",Stack_top(stack_instance)
	end do

	!! Verifica el estado de la pila
	print *,"La pila est� libre: ", Stack_empty(stack_instance)


	!!Elimina la pila previamanete creada
	call Stack_destructor(stack_instance)


end program StackTest