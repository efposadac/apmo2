!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonzalez Monico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, atraccion integrals, recursive integrals,     !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa prueba la construccion de gausianas primitivas a igual que
! algunos metodos de la calse, ademas realiza el calculo de integrales de 
! translapamiento entre ellas.
!**
program PrimitiveGaussian_test
     use PrimitiveGaussian_
     use OverlapIntegrals_
     use AttractionIntegrals

     implicit none

     type(PrimitiveGaussian) :: gA,gB, gC
     type(PrimitiveGaussian) , pointer :: gA_Pointer
     real(8) :: out

     !!Contruye  gausianas primitivas
     call PrimitiveGaussian_constructor(gA,[0.0_8,0.0_8,-2.0_8],0.44_8,2_8)
     call PrimitiveGaussian_constructor(gB,[0.0_8,0.0_8,0.0_8],0.44_8,1_8) 

     gA_Pointer=>PrimitiveGaussian_getPrimitive(gA) 

     !! Muestra informacion de los objetos construidos.
     call PrimitiveGaussian_show(gA_Pointer)
     call PrimitiveGaussian_show(PrimitiveGaussian_getPrimitive(gB))

     !!***********************************************************
     !! Calcula integrales de overlap entre gausianas previamente
     !! construidas.
     !!

     out=PrimitiveGaussian_overlapIntegral (gA_Pointer,PrimitiveGaussian_getPrimitive(gB))
     print *,"(gA|gB) overlap integral unnormalized=",out

     out=PrimitiveGaussian_overlapIntegral (gA_Pointer,gA)
     print *,"(gA|gA)overlap integral unnormalized=",out

     out= PrimitiveGaussian_overlapIntegral ( gA_Pointer,gA) &
	  * PrimitiveGaussian_getNormalizationConstant(gA) &
	  * PrimitiveGaussian_getNormalizationConstant(gA) 
     
     print *,"(gA|gA) overlap integral normalized",out

     out= PrimitiveGaussian_overlapIntegral ( gA_Pointer,gB) &
	  * PrimitiveGaussian_getNormalizationConstant(gA) &
	  * PrimitiveGaussian_getNormalizationConstant(gB) 
     
     print *,"(gA|gB) overlap integral normalized",out

     out=PrimitiveGaussian_overlapIntegral ( gB,gB ) 
     print *,"(gB|gB) overlap integral unnormalized",out

     out= PrimitiveGaussian_overlapIntegral ( gB,gB ) &
	  * PrimitiveGaussian_getNormalizationConstant(gB) &
	  * PrimitiveGaussian_getNormalizationConstant(gB) 

     print *,"(gB|gB) overlap integral normalized",out

     nullify(gA_pointer)

     call PrimitiveGaussian_constructor(gC,[1.0_8,2.0_8,3.0_8],0.44_8,[1_8,2_8,0_8])
     

     print *,""
     print *,"PRUEBA DE FUNCION DE NORMALIZACION"
     print *,"==================================="
     print *,""

     call PrimitiveGaussian_show(gC)
     call PrimitiveGaussian_set( gC, [0_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(s): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(px): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(py): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(pz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(dxx): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,2_8,0_8] )
     write (*,"(A15,F25.12)") "N(dyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,0_8,2_8] )
     write (*,"(A15,F25.12)") "N(dzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(dxy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(dxz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,1_8,1_8] )
     write (*,"(A15,F25.12)") "N(dyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(fxxx): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,3_8,0_8] )
     write (*,"(A15,F25.12)") "N(fyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,0_8,3_8] )
     write (*,"(A15,F25.12)") "N(fzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [1_8,2_8,0_8] )
     write (*,"(A15,F25.12)") "N(fxyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(fxxz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,1_8,2_8] )
     write (*,"(A15,F25.12)") "N(fyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(fxxy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,2_8] )
     write (*,"(A15,F25.12)") "N(fxzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,2_8,1_8] )
     write (*,"(A15,F25.12)") "N(fyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)     
     
     call PrimitiveGaussian_set( gC, [1_8,1_8,1_8] )
     write (*,"(A15,F25.12)") "N(fxyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [4_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(gxxxx): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,4_8,0_8] )
     write (*,"(A15,F25.12)") "N(gyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,0_8,4_8] )
     write (*,"(A15,F25.12)") "N(gzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [3_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(gxxxy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,3_8] )
     write (*,"(A15,F25.12)") "N(gxzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,3_8,1_8] )
     write (*,"(A15,F25.12)") "N(gyyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)


     call PrimitiveGaussian_set( gC, [1_8,3_8,0_8] )
     write (*,"(A15,F25.12)") "N(gxyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(gxxxz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,1_8,3_8] )
     write (*,"(A15,F25.12)") "N(gyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,2_8,0_8] )
     write (*,"(A15,F25.12)") "N(gxxyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,0_8,2_8] )
     write (*,"(A15,F25.12)") "N(gxxzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,2_8,2_8] )
     write (*,"(A15,F25.12)") "N(gyyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,1_8,1_8] )
     write (*,"(A15,F25.12)") "N(gxxyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,1_8,2_8] )
     write (*,"(A15,F25.12)") "N(gxyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,2_8,1_8] )
     write (*,"(A15,F25.12)") "N(gxyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [5_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(hxxxxx): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,5_8,0_8] )
     write (*,"(A15,F25.12)") "N(hyyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,0_8,5_8] )
     write (*,"(A15,F25.12)") "N(hzzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [4_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(hxxxxy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,4_8] )
     write (*,"(A15,F25.12)") "N(hxzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,4_8,1_8] )
     write (*,"(A15,F25.12)") "N(hyyyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,4_8,0_8] )
     write (*,"(A15,F25.12)") "N(hxyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [4_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(hxxxxz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,1_8,4_8] )
     write (*,"(A15,F25.12)") "N(hyzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,2_8,0_8] )
     write (*,"(A15,F25.12)") "N(hxxxyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,0_8,3_8] )
     write (*,"(A15,F25.12)") "N(hxxzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,3_8,2_8] )
     write (*,"(A15,F25.12)") "N(hyyyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,3_8,0_8] )
     write (*,"(A15,F25.12)") "N(hxxyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,0_8,2_8] )
     write (*,"(A15,F25.12)") "N(hxxxzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,2_8,3_8] )
     write (*,"(A15,F25.12)") "N(hyyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,1_8,1_8] )
     write (*,"(A15,F25.12)") "N(hxxxyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,1_8,3_8] )
     write (*,"(A15,F25.12)") "N(hxyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,3_8,1_8] )
     write (*,"(A15,F25.12)") "N(hxyyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [2_8,2_8,1_8] )
     write (*,"(A15,F25.12)") "N(hxxyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,1_8,2_8] )
     write (*,"(A15,F25.12)") "N(hxxyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,2_8,2_8] )
     write (*,"(A15,F25.12)") "N(hxyyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)




     call PrimitiveGaussian_set( gC, [6_8,0_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixxxxxx): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,6_8,0_8] )
     write (*,"(A15,F25.12)") "N(iyyyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,0_8,6_8] )
     write (*,"(A15,F25.12)") "N(izzzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [5_8,1_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixxxxxy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,0_8,5_8] )
     write (*,"(A15,F25.12)") "N(ixzzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,5_8,1_8] )
     write (*,"(A15,F25.12)") "N(iyyyyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [1_8,5_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixyyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [5_8,0_8,1_8] )
     write (*,"(A15,F25.12)") "N(ixxxxxz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [0_8,1_8,5_8] )
     write (*,"(A15,F25.12)") "N(iyzzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [4_8,2_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixxxxyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [2_8,0_8,4_8] )
     write (*,"(A15,F25.12)") "N(ixxzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,4_8,2_8] )
     write (*,"(A15,F25.12)") "N(iyyyyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,4_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixxyyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)
     
     call PrimitiveGaussian_set( gC, [4_8,0_8,2_8] )
     write (*,"(A15,F25.12)") "N(ixxxxzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,2_8,4_8] )
     write (*,"(A15,F25.12)") "N(iyyzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [4_8,1_8,1_8] )
     write (*,"(A15,F25.12)") "N(ixxxxyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,1_8,4_8] )
     write (*,"(A15,F25.12)") "N(ixyzzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,4_8,1_8] )
     write (*,"(A15,F25.12)") "N(ixyyyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,3_8,0_8] )
     write (*,"(A15,F25.12)") "N(ixxxyyy): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,0_8,3_8] )
     write (*,"(A15,F25.12)") "N(ixxxzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [0_8,3_8,3_8] )
     write (*,"(A15,F25.12)") "N(iyyyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,2_8,1_8] )
     write (*,"(A15,F25.12)") "N(ixxxyyz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,1_8,3_8] )
     write (*,"(A15,F25.12)") "N(ixxyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,3_8,2_8] )
     write (*,"(A15,F25.12)") "N(ixyyzzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [3_8,1_8,2_8] )
     write (*,"(A15,F25.12)") "N(ixxxyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [1_8,2_8,3_8] )
     write (*,"(A15,F25.12)") "N(ixxxyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,3_8,1_8] )
     write (*,"(A15,F25.12)") "N(ixxxyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)

     call PrimitiveGaussian_set( gC, [2_8,2_8,2_8] )
     write (*,"(A15,F25.12)") "N(ixxyyzz): ", PrimitiveGaussian_getNormalizationConstant(gC)
     


     call PrimitiveGaussian_set( gC, [1_8,2_8,0_8] )
     out= PrimitiveGaussian_overlapIntegral( gC,gC ) &
	  * PrimitiveGaussian_getNormalizationConstant(gC) &
	  * PrimitiveGaussian_getNormalizationConstant(gC) 
     
     print *,"(gC|gC) overlap integral normalized",out

end program PrimitiveGaussian_test
