!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! Este programa prueba la construccion y manipulacion de matrices
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-19
!   - <tt> 2007-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!   - <tt> 2007-08-26 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Prueba de las nuevas funciones (Matrix_eigen,Matrix_factorizeLU)
!**

program MatrixTest
	use Matrix_
	use Vector_
	use APMO_
	
	implicit none
	
	type(Matrix) :: A
	type(Matrix) :: B
	type(Matrix) :: C
	type(Matrix) :: L
	type(Matrix) :: U
	type(Matrix) :: V
	type(Matrix) :: S
	
	type(Vector) :: tmpVector
	type(Matrix) :: tmpMatrix
	integer :: i
	integer :: j
	character(5), allocatable :: myColKeys(:)
	character(5), allocatable :: myRowKeys(:)
	integer, allocatable :: array1D(:)
	
	call APMO_constructor() ;
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing constructor methods "
	print *, "####################################################"
	print *, ""
	
	print *, ""
	print *, " Default Constructor"
	print *, "---------------------"
	print *, ""
	call Matrix_constructor( A, 3, 3, 2.45_8 )
	call Matrix_show( A )
	
	print *, ""
	print *, " Copy Constructor"
	print *, "------------------"
	print *, ""
	call Matrix_copyConstructor( B, A )
	call Matrix_show( B )
	call Matrix_destructor( A )
	call Matrix_destructor( B )
	
	print *, ""
	print *, " Diagonal Constructor"
	print *, "----------------------"
	print *, ""
	call Vector_constructor( tmpVector, 3, values=([ 2.45_8, 3.15_8, 5.456_8 ]) )
	call Matrix_diagonalConstructor( A, tmpVector )
	call Matrix_show( A )
	call Vector_destructor( tmpVector )
	call Matrix_destructor( A )
	
	print *, ""
	print *, " Random Elements Constructor"
	print *, "-----------------------------"
	print *, ""
	call Matrix_randomElementsConstructor( A, 3, 3 )
	call Matrix_show( A )
	call Matrix_destructor( A )
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing printing methods "
	print *, "####################################################"
	print *, ""
	call Matrix_constructor( A, 7, 7, 2.45_8 )
	
	allocate( myColKeys(7) )
	allocate( myRowKeys(7) )
	myColKeys = (/"   A1","   A2","   B2","   S1","   S3","   A1","   A1"/)
	myRowKeys = (/"    s","    s","    p","    p","    p","    s","    p"/)
	
	call Matrix_show( A )
	call Matrix_show( A, rowKeys=myRowKeys, flags=WITH_ROW_KEYS )
	call Matrix_show( A, rowKeys=myRowKeys, columnKeys=myColKeys, flags=WITH_BOTH_KEYS )
	
	deallocate( myRowKeys )
	deallocate( myColKeys )
	
	call Matrix_destructor( A )
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing calculate eigen vectors and eigenvalues "
	print *, "####################################################"
	print *, ""
	call Matrix_constructor( A, 3, 3, 0.0_8 )
	
	call Matrix_setElement( A, 1, 1, 1.4500_8 )
	call Matrix_setElement( A, 1, 2, 3.1580_8 )
	call Matrix_setElement( A, 1, 3, 5.1640_8 )
	call Matrix_setElement( A, 2, 1, 3.1580_8 )
	call Matrix_setElement( A, 2, 2, 6.1500_8 )
	call Matrix_setElement( A, 2, 3, 2.1580_8 )
	call Matrix_setElement( A, 3, 1, 5.1640_8 )
	call Matrix_setElement( A, 3, 2, 2.1580_8 )
	call Matrix_setElement( A, 3, 3, 4.1260_8 )
	
	print *, ""
	print *, " Matrix A"
	print *, "----------"
	print *, ""
	call Matrix_show( A )
	call Vector_constructor( tmpVector, Matrix_getNumberOfColumns( A ) )
	call Matrix_copyConstructor( tmpMatrix, A )
	
	call Matrix_eigen( A, tmpVector, eigenVectors=tmpMatrix, flags=SYMMETRIC )
	print *, ""
	print *, " Eigen values of B Matrix"
	print *, "--------------------------"
	print *, ""
	call Vector_show( tmpVector )
	
	print *, ""
	print *, " Eigen vectors of B Matrix"
	print *, "---------------------------"
	print *, ""
	call Matrix_show( tmpMatrix, flags=WITH_COLUMN_KEYS )
	
	call Matrix_destructor( A )
	call Vector_destructor( tmpVector )
	call Matrix_destructor( tmpMatrix )
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing LU descomposition"
	print *, "####################################################"
	print *, ""
	call Matrix_randomElementsConstructor( A, 3, 3, symmetric=.true. )
	
	print *, ""
	print *, " Matrix A"
	print *, "----------"
	print *, ""
	call Matrix_show( A )
	
	allocate( array1D( 3 )  )
	call Matrix_copyConstructor( B, A )
	call Matrix_constructor( L, 3, 3 )
	call Matrix_constructor( U, 3, 3 )
	
	B = Matrix_factorizeLU( A, L=L, U=U, pivotIndices=array1D )
	write (*,*) "Pivot indices = ", array1D
	print *, ""
	print *, " L Matrix"
	print *, "----------"
	print *, ""
	call Matrix_show( L, flags=WITHOUT_KEYS )
	
	print *, ""
	print *, " U Matrix"
	print *, "----------"
	print *, ""
	call Matrix_show( U, flags=WITHOUT_KEYS )
	
	call Matrix_destructor( A )
	call Matrix_destructor( B )
	call Matrix_destructor( L )
	call Matrix_destructor( U )
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing getting inverse matrix"
	print *, "####################################################"
	print *, ""
	call Matrix_randomElementsConstructor( A, 3, 3, symmetric=.true. )
	
	print *, ""
	print *, " Matrix A"
	print *, "----------"
	print *, ""
	call Matrix_show( A )
	
	call Matrix_constructor( B, 3, 3 )
	B = Matrix_inverse( A )
	
	print *, ""
	print *, " Matrix A^-1"
	print *, "-------------"
	print *, ""
	call Matrix_show( B )
	
	call Matrix_constructor( C, 3, 3 )
	C = Matrix_product( A, B )
	
	print *, ""
	print *, " Matrix A*A^-1"
	print *, "---------------"
	print *, ""
	call Matrix_show( C )
	
	call Matrix_destructor( A )
	call Matrix_destructor( B )
	call Matrix_destructor( C )
	
	print *, ""
	print *, "####################################################"
	print *, "# Testing functions matrix"
	print *, "####################################################"
	print *, ""
	call Matrix_constructor( A, 3, 3, 0.0_8 )
	call Matrix_constructor( B, 3, 3 )
	
	call Matrix_setElement( A, 1, 1,  0.0345_8 )
	call Matrix_setElement( A, 2, 2, -0.2500_8 )
	call Matrix_setElement( A, 3, 3,  0.5260_8 )
	
	call Matrix_setElement( A, 1, 2,  0.0800_8 )
	call Matrix_setElement( A, 2, 1,  0.0800_8 )
	
	call Matrix_setElement( A, 1, 3,  0.1640_8 )
	call Matrix_setElement( A, 3, 1,  0.1640_8 )
	
	call Matrix_setElement( A, 2, 3, -0.8581_8 )
	call Matrix_setElement( A, 3, 2, -0.8581_8 )
	
	print *, ""
	print *, " Matrix A"
	print *, "----------"
	print *, ""
	call Matrix_show( A )
	
	print *, ""
	print *, " A^2"
	print *, "-----"
	print *, ""
	B = Matrix_pow( A, 2.0_8 )
	call Matrix_show( B )
	
	print *, ""
	print *, " sqrt(A)"
	print *, "---------"
	print *, ""
	B = Matrix_sqrt( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " log(A)"
	print *, "---------"
	print *, ""
	B = Matrix_log( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " log10(A)"
	print *, "---------"
	print *, ""
	B = Matrix_log10( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " sin(A)"
	print *, "---------"
	print *, ""
	B = Matrix_sin( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " cos(A)"
	print *, "---------"
	print *, ""
	B = Matrix_cos( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " tan(A)"
	print *, "---------"
	print *, ""
	B = Matrix_tan( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " asin(A)"
	print *, "---------"
	print *, ""
	B = Matrix_asin( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " acos(A)"
	print *, "---------"
	print *, ""
	B = Matrix_acos( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " atan(A)"
	print *, "---------"
	print *, ""
	B = Matrix_atan( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " sinh(A)"
	print *, "---------"
	print *, ""
	B = Matrix_sinh( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " cosh(A)"
	print *, "---------"
	print *, ""
	B = Matrix_cosh( A )
	call Matrix_show( B )
	
	print *, ""
	print *, " tanh(A)"
	print *, "---------"
	print *, ""
	B = Matrix_tanh( A )
	call Matrix_show( B )

	print *, ""
	print *, " Elimina ultima fila de B"
	print *, "---------"
	print *, ""
	call Matrix_removeRow(B,3)
	call Matrix_show( B )

	print *, ""
	print *, " Elimina primera fila de A"
	print *, "---------"
	print *, ""
	call Matrix_removeRow(A,1)
	call Matrix_show( A )

	print *, ""
	print *, " Elimina columna central de B"
	print *, "---------"
	print *, ""
	call Matrix_removeColumn(B,2)
	call Matrix_show( B )
	
	call Matrix_destructor( A )
	call Matrix_destructor( B )

	call Matrix_constructor(A,6,4)
	 A.values(1, 1)= 2.27
	 A.values(1, 2)=-1.54
	 A.values(1, 3)= 1.15
	 A.values(1, 4)=-1.94
	 A.values(2, 1)= 0.28
	 A.values(2, 2)=-1.67
	 A.values(2, 3)= 0.94
	 A.values(2, 4)=-0.78
	 A.values(3, 1)=-0.48
	 A.values(3, 2)=-3.09
	 A.values(3, 3)= 0.99
	 A.values(3, 4)=-0.21
	 A.values(4, 1)= 1.07
	 A.values(4, 2)= 1.22
	 A.values(4, 3)= 0.79
	 A.values(4, 4)= 0.63
	 A.values(5, 1)=-2.35
	 A.values(5, 2)= 2.93
	 A.values(5, 3)=-1.45
	 A.values(5, 4)= 2.30
	 A.values(6, 1)= 0.62
	 A.values(6, 2)=-7.39
	 A.values(6, 3)= 1.03
	 A.values(6, 4)=-2.57
        
	print *, ""
	print *, " Realizando un SVD de A"
	print *, "---------"
	print *, ""
        
        
	call Matrix_svd(A,U,V,S)
        
	print *, ""
	print *, " MATRIZ A"
	print *, "---------"
	print *, ""
	call Matrix_show(A)
        
	print *, ""
	print *, " MATRIZ U"
	print *, "---------"
	print *, ""
	call Matrix_show(U)
        
	print *, ""
	print *, " MATRIZ V"
	print *, "---------"
	print *, ""
	call Matrix_show(V)

	print *, ""
	print *, " VALORES SINGULARES"
	print *, "---------"
	print *, ""
	call Matrix_show(S)

	call APMO_destructor()
	
end program MatrixTest
