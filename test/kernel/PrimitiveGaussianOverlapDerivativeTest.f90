!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. González Mónico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, overlap integrals, recursive integrals,       !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de overlap entre funciones gaussianas
! primitivas sin normalizar, determinando el tiempo requerido para el calculo.
!
program PrimitiveGaussian_overlapDerivTest
	use PrimitiveGaussian_
	use OverlapDerivatives_
	
	implicit none
	
	type(PrimitiveGaussian)::gA,gB
	real(8)::out
	integer::i
	real :: timeBegin
	real :: timeEnd
	
	!!
	!! Definción de funciones gausianas. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8,owner=1)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8,owner=2)
	
	!! Imprime información sobre la gausianas construidas
	print *,"NV"
	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	
	!!***************************************************
	!! Inicio de calculo derivadas de integrales de overlap
	!!
	
	call cpu_time(timeBegin)
	
	!(s|s) 
	print *,"d(Sa|Sb)/dRax=",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(Sa|Sb)/dRbx=",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(Sa|Sb)/dRbz=",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	print *,"d(Sa|Sa)/dRax=",PrimitiveGaussian_overlapDerivative(gA,gA,2,3)
	
	!!(p|A(0)|s) 
	
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	print *,"d(px|A|s)/dRax  = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(px|A|s)/dRbx  = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(px|A|s)/dRbz  = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	print *,"d(px|A|px)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gA,1,3)
	
	!!(p|A(0)|p) 
	
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	print *,"d(px|A|px)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(px|A|px)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(px|A|px)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)

	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	print *,"d(px|A|py)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(px|A|py)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(px|A|py)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
 
	!!(d|A(0)|s) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	print *,"d(dxx|A|s)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxx|A|s)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxx|A|s)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	print *,"d(dxy|A|s)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxy|A|s)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxy|A|s)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	
	!!(d|A(0)|p) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	print *,"d(dxx|A|px)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxx|A|px)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxx|A|px)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	print *,"d(dxy|A|py)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxy|A|py)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxy|A|py)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	print *,"d(dxy|A|pz)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxy|A|pz)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxy|A|pz)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	!!(d|A(0)|d) 
	
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	print *,"d(dxx|A|dxx)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxx|A|dxx)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxx|A|dxx)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	print *,"d(dxy|A|dzz)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxy|A|dzz)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxy|A|dzz)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	print *,"d(dxy|A|dyz)/dRax = ",PrimitiveGaussian_overlapDerivative(gA,gB,1,1)
	print *,"d(dxy|A|dyz)/dRbx = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,1)
	print *,"d(dxy|A|dyz)/dRbz = ",PrimitiveGaussian_overlapDerivative(gA,gB,2,3)
	
	call cpu_time(timeEnd)
	print *,"Time=",timeEnd-timeBegin,&
	"sec - (Time by 38 derivatives of overlap integrals for CGF )"
	
	!!***************************************************
end program PrimitiveGaussian_overlapDerivTest
