!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, repulsion integrals, recursive integrals,    !
!!              gaussian functions, analytic derivatives                           !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula derivadas de integrales de repulsion entre funciones
! gaussianas primitivas sin normalizar, determinando el tiempo requerido para el calculo.
!
program derivRepulsionTest
	use APMO_
	use RepulsionDerivatives_
	use PrimitiveGaussian_
	
	implicit none
	
	type(PrimitiveGaussian)::gA
	type(PrimitiveGaussian)::gB
	type(PrimitiveGaussian)::gC
	type(PrimitiveGaussian)::gD
	integer::i
	real(8):: out
	real :: timeBegin
	real :: timeEnd
	
	call  APMO_constructor()
	
	!!
	!! Definci�n de funciones gausianas y origen de la carga puntual. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8,owner=1)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8,owner=2)
	call PrimitiveGaussian_constructor(gC,[ 2.0_8,1.0_8,-5.0_8],0.3_8,0_8,owner=1)
	call PrimitiveGaussian_constructor(gD,[-1.0_8,3.5_8,-5.6_8],0.15_8,0_8,owner=4)
	
	
! 	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,[0_8,0_8,0_8])
! 	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,[0_8,0_8,0_8])
! 	call PrimitiveGaussian_constructor(gC,[ 1.0_8,2.5_8,-1.0_8],0.05_8,[0_8,0_8,0_8])
! 	call PrimitiveGaussian_constructor(gD,[-1.0_8,3.5_8,-5.6_8],0.15_8,[0_8,0_8,0_8])
	
	
	!!***************************************************
	!! Inicio de calculo de integrales de atracci�n
	!!
	
!	call cpu_time(timeBegin)
	
	!! (ss|ss)
	print *,""
	print *,"d(SaSb,ScSd)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(SaSb,ScSd)/dRbx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,2,1)
	print *,"d(SaSb,ScSd)/dRbz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,2,3)
	print *,"d(SaSb,ScSd)/dRcy = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(SaSb,ScSd)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	
	!! (PaSb,ScSd)
	print *,""
	gA.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(PxS,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxS,SS)/dRbx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,2,1)
	print *,"d(PxS,SS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxS,SS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(PxS,SS)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(SS,PzS)/dRbz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,2,3)
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(SS,SPy)/dRcy = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2) 
	
	!! (PaPb,ScSd)
	print *,""
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(PxPx,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxPx,SS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(PxPx,SS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(PxPx,SS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gB.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(PxPy,SS)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(SS,PxPx)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gD.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(SS,PxPy)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (PaSb,PcSd)
	print *,""
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(PxS,PxS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxS,PxS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(PxS,PxS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(PxS,PxS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(PyS,PxS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(SPz,PxS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)

	!! (PaPb,PcSd)
	print *,""
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(PxPx,PxS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxPx,PxS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(PxPx,PxS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(PxPx,PxS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[0_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(PyPz,PxS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(PyPz,SPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (PaPb,PcPd)
	print *,""
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(PxPx,PxPx)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(PxPx,PxPx)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(PxPx,PxPx)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(PxPx,PxPx)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[0_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(PyPz,PxPx)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(PyPz,PyPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)

	!! (DaSb,ScSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxS,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxS,SS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxS,SS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxS,SS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	print *,"d(DxyS,SS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(SS,SDyz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (DaPb,ScSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxPx,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,SS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxPx,SS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxPx,SS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(DxyPy,SS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[1_8,1_8,0_8]
	print *,"d(SS,PyDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)

	!! (DaSb,PcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxS,PxS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxS,PxS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxS,PxS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxS,PxS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(DxyS,PyS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,1_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,1_8,0_8]
	print *,"d(PzS,SDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	
	!! (DaDb,ScSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxDxx,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,SS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,SS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,SS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(DxyDyz,SS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	print *,"d(DxyDxy,SS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,1_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(SS,DxyDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	
	!! (DaSb,DcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxS,DxxS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxS,DxxS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxS,DxxS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxS,DxxS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(DxyS,DyzS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(SDxy,SDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	
	!! (DaPb,PcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxPx,PxS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,PxS)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxPx,PxS)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxPx,PxS)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(DxyPz,PyS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(SPx,PyDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)

	!! (DaSb,PcPd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxxS,PxPx)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxS,PxPx)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxS,PxPx)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxS,PxPx)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,1_8]
	gD.angularMomentIndex=[0_8,1_8,0_8]
	print *,"d(DxyS,PzPy)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[0_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,1_8,1_8]
	print *,"d(PyPx,SDyz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	
	!! (DaDb,PcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,PxS)/dRax = ", out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,PxS)/dRdx = ", out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,PxS)/dRdz = ", out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,PxS)/dRcx = ", out
	gA.angularMomentIndex=[0_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,1_8]
	gD.angularMomentIndex=[1_8,1_8,0_8]
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	print *,"d(PyS,DxzDxy)/dRaz = ", out
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyDxy,SPz)/dRax = ", out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyDxy,SPz)/dRay = ", out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	print *,"d(DxyDxy,SPz)/dRaz = ", out	

	!! (DaPb,PcPd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	out=PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,PxPx)/dRax = ",out
	out=PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxPx,PxPx)/dRdx = ",out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxPx,PxPx)/dRdz = ",out
	out=PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,PxPx)/dRcx = ",out
	gA.angularMomentIndex=[0_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	print *,"d(PyPz,PyDxz)/dRaz = ",out
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyPx,PyPz)/dRax = ",out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyPx,PyPz)/dRay = ",out
	out = PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	print *,"d(DxyPx,PyPz)/dRaz = ", out
	
	!! (DaPb,DcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxPx,Dxx,S)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,Dxx,S)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxPx,Dxx,S)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxPx,Dxx,S)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[0_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,1_8,1_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(S,Dyz,PyDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxyPx,DyzS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyPx,DyzS)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyPx,DyzS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (DaPb,DcPd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxxPx,Dxx,Px)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxPx,Dxx,Px)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxPx,Dxx,Px)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxPx,Dxx,Px)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,1_8,1_8]
	gC.angularMomentIndex=[0_8,1_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(Px,Dyz,PyDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxyPx,DyzPx)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyPx,DyzPx)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyPx,DyzPx)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (DaDb,PcPd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxxDxx,Px,Px)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,Px,Px)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,Px,Px)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,Px,Px)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,0_8,0_8]
	gB.angularMomentIndex=[0_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(Px,Py,DyzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxyDxy,PzPx)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyDxy,PzPx)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyDxy,PzPx)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(DxxDxx,PxPz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,PxPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(DxxDxx,PzPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
 	gB.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDzz,PzPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
 
	!! (DaDb,DcSd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxDxx,Dxx,S)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,Dxx,S)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,Dxx,S)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,Dxx,S)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,0_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(Dxy,S,DyzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(DxyDxy,SDxz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyDxy,SDxz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyDxy,SDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,1_8]
	gD.angularMomentIndex=[0_8,0_8,0_8]
	print *,"d(DxxDxx,DxzS)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,DxzS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDxx,DzzS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gB.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDzz,DzzS)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (DaDb,DcPd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[1_8,0_8,0_8]
	print *,"d(DxxDxx,Dxx,Px)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,Dxx,Px)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,Dxx,Px)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,Dxx,Px)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,1_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(Dxy,Pz,DyzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(DxyDxy,PzDxz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyDxy,PzDxz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyDxy,PzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,1_8]
	gD.angularMomentIndex=[0_8,0_8,1_8]
	print *,"d(DxxDxx,DxzPz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,DxzPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDxx,DzzPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gB.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDzz,DzzPz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	
	!! (DaDb,DcDd)
	print *,""
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[2_8,0_8,0_8]
	gD.angularMomentIndex=[2_8,0_8,0_8]
	print *,"d(DxxDxx,Dxx,Dxx)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,Dxx,Dxx)/dRdx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,1)
	print *,"d(DxxDxx,Dxx,Dxx)/dRdz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,4,3)
	print *,"d(DxxDxx,Dxx,Dxx)/dRcx = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[0_8,0_8,2_8]
	gC.angularMomentIndex=[0_8,1_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(Dxy,Dzz,DyzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[1_8,1_8,0_8]
	gB.angularMomentIndex=[1_8,1_8,0_8]
	gC.angularMomentIndex=[0_8,0_8,2_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(DxyDxy,DzzDxz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxyDxy,DzzDxz)/dRay = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,2)
	print *,"d(DxyDxy,DzzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gA.angularMomentIndex=[2_8,0_8,0_8]
	gB.angularMomentIndex=[2_8,0_8,0_8]
	gC.angularMomentIndex=[1_8,0_8,1_8]
	gD.angularMomentIndex=[1_8,0_8,1_8]
	print *,"d(DxxDxx,DxzDxz)/dRax = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,1)
	print *,"d(DxxDxx,DxzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gC.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDxx,DzzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)
	gB.angularMomentIndex=[0_8,0_8,2_8]
	print *,"d(DxxDzz,DzzDxz)/dRaz = ",PrimitiveGaussian_repulsionDerivative(gA,gB,gC,gD,1,3)

	
!	call cpu_time(timeEnd)
!	print *,"Time=",timeEnd-timeBegin , &
!	"sec - (Time by 143 repulsion integrals over s,p and d CGF)"
	!!***************************************************
 
end program derivRepulsionTest
             
