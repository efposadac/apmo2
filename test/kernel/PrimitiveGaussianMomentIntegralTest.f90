!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, atracci�n integrals, recursive integrals,     !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de momento entre funciones gaussianas
! primitivas sin normalizar, alrededor del punto C. Adem�s determina el tiempo
! requerido para el c�lculo.
!
	
program PrimitiveGaussian_momentTest
	use PrimitiveGaussian_
	use MomentIntegrals_
	implicit none
	type(PrimitiveGaussian)::gA,gB
	real(8), allocatable::out(:)
	real(8) ::R_c(3)
	integer::i
	integer::compMoment
	real::timeBegin,timeEnd
	
	!! ORBITALS ORIGIN AND ORBITALS EXPONENTS VECTORS TEST 
	
	!!
	!! Definci�n de funciones gausianas y centro de c�lculo. 
	!!
	R_c = [1.0_8,-3.5_8,2.99_8]
	compMoment = 1
	call primitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,angularMomentIndex=[0_8,0_8,0_8])
	call primitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,angularMomentIndex=[0_8,0_8,0_8])
	
	!! Imprime informaci�n sobre las gausianas construidas
	print *,"NV"
	call primitiveGaussian_show(gA)
	call primitiveGaussian_show(gB)
	
	!***************************************************
	!! Inicio de calculo de integrales de atracci�n
	!!
	call cpu_time(timeBegin)
	!!  (s|M|s) 
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment ) 
	print *,"(s|M|s)=",out
	

	stop

	!! (p_x|M|s) 
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|s)=",out
	
	!! (p_y|M|s)
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|s)=",out
	
	!! (p_z|M|s)
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|s)=",out
	
	!! (s|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|p_x)=",out
	
	!! (s|M|p_y)
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|p_y)=",out
	
	!! (s|M|p_z)
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|p_z)=",out
	
	
	!! (d_xx|M|s)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|s)=",out
	
	!! (d_xy|M|s)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|s)=",out
	
	!! (d_yy|M|s)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|s)=",out
	
	!! (d_yz|M|s)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|s)=",out
	
	!! (d_xz|M|s)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|s)=",out
	
	!! (d_zz|M|s)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|s)=",out
	
	!! (s|M|d_xx)
	call PrimitiveGaussian_set(gA,[0_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_xx)=",out
	
	!! (s|M|d_xy)
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_xy)=",out
	
	!! (s|M|d_yy)
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_yy)=",out
	
	!! (s|M|d_yz)
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_yz)=",out
	
	!! (s|M|d_xz)
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_xz)=",out
	
	!! (s|M|d_zz)
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(s|M|d_zz)=",out
	
	!! (p_x|M|p_x)
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|p_x)=",out
	
	!! (p_x|M|p_y)
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|p_y)=",out
	
	!! (p_x|M|p_z)
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	gA%angularMomentIndex=[1_8,0_8,0_8]
	gB%angularMomentIndex=[0_8,0_8,1_8]
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|p_z)=",out
	
	!! (p_y|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|p_x)=",out
	
	!! (p_y|M|p_y)
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|p_y)=",out
	
	!! (p_y|M|p_z)
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|p_z)=",out
	
	!! (p_z|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|p_x)=",out
	
	!! (p_z|M|p_y)
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|p_y)=",out
	
	!! (p_z|M|p_z)
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|p_z)=",out
	
	!! (d_xx|M|p_x)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_x)=",out
	
	!! (d_xy|M|p_x)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_x)=",out
	
	!! (d_yy|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|p_x)=",out
	
	!! (d_yz|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|p_x)=",out
	
	!! (d_xz|M|p_x)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|p_x)=",out
	
	!! (d_zz|M|p_x)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|p_x)=",out
	
	
	!! (d_xx|M|p_y)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_y)=",out
	
	!! (d_xy|M|p_y)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_y)=",out
	
	!! (d_yy|M|p_y)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|p_y)=",out
	
	!! (d_yz|M|p_y)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|p_y)=",out
	
	!! (d_xz|M|p_y)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|p_y)=",out
	
	!! (d_zz|M|p_y)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|p_y)=",out
	
	
	!! (d_xx|M|p_z)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_z)=",out
	
	!! (d_xy|M|p_z)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|p_z)=",out
	
	!! (d_yy|M|p_z)-
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|p_z)=",out
	
	!! (d_yz|M|p_z)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|p_z)=",out
	
	!! (d_xz|M|p_z)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|p_z)=",out
	
	!! (d_zz|M|p_z)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|p_z)=",out
	
	!! (p_x|M|d_xx)
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_xx)=",out
	
	!! (p_x|M|d_xy)
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_xx)=",out
	
	!! (p_x|M|d_yy)
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_yy)=",out
	
	!! (p_x|M|d_yz)
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_yz)=",out
	
	!! (p_x|M|d_xz)
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_xz)=",out
	
	!! (p_x|M|d_zz)
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gA,[1_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_x|M|d_zz)=",out
	
	!! (p_y|M|d_xx)
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|d_xx)=",out
	
	!! (p_y|M|d_xy)
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|d_xx)=",out
	
	!! (p_y|M|d_yy)
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|p_y)=",out
	
	!! (p_y|M|d_yz)
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|d_yz)=",out
	
	!! (p_y|M|d_z)
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|d_xz)=",out
	
	!! (p_y|M|d_zz)
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gA,[0_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_y|M|d_zz)=",out
	
	!! (p_z|M|d_xx)
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_xx)=",out
	
	!! (p_z|M|d_xy)
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_xx)=",out
	
	!! (p_z|M|d_yy)-
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_yy)=",out
	
	!! (p_z|M|d_yz)
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_yz)=",out
	
	!! (p_z|M|d_xz)
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_xz)=",out
	
	!! (p_z|M|d_zz)
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gA,[0_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(p_z|M|d_zz)=",out
	
	!! (d_xx|M|d_xx)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_xx)=",out
	
	!! (d_xy|M|d_xx)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_xx)=",out
	
	!(d_yy|M|d_xx)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_xx)=",out
	
	!! (d_yz|M|d_xx)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_xx)=",out
	
	!! (d_xz|M|d_xx)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_xx)=",out
	
	!! (d_zz|M|d_xx)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[2_8,0_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_xx)=",out
	
	!! (d_xx|M|d_xy)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_xy)=",out
	
	!! (d_xy|M|d_xy)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_xy)=",out
	
	!! (d_yy|M|d_xy)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_xy)=",out
	
	!! (d_yz|M|d_xy)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_xy)=",out
	
	!! (d_xz|M|d_xy)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_xy)=",out
	
	!! (d_zz|M|d_xy)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[1_8,1_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_xy)=",out
	
	!! (d_xx|M|d_yy)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_yy)=",out
	
	!! (d_xy|M|d_yy)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_yy)=",out
	
	!! (d_yy|M|d_yy)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_yy)=",out
	
	!! (d_yz|M|d_yy)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_yy)=",out
	
	!! (d_xz|M|d_yy)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_yy)=",out
	
	!! (d_zz|M|d_yy)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[0_8,2_8,0_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_yy)=",out
	
	!! (d_xx|M|d_yz)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_yz)=",out
	
	!! (d_xy|M|d_yz)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_yz)=",out
	
	!! (d_yy|M|d_yz)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_yz)=",out
	
	!! (d_yz|M|d_yz)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_yz)=",out
	
	!! (d_xz|M|d_yz)
	
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_yz)=",out
	
	!! (d_zz|M|d_yz)
	
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[0_8,1_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_yz)=",out
	
	!! (d_xx|M|d_xz)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_xz)=",out
	
	!! (d_xy|M|d_xz)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_xz)=",out
	
	!! (d_yy|M|d_xz)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_xz)=",out
	
	!! (d_yz|M|d_xz)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_xz)=",out
	
	!! (d_xz|M|d_xz)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_xz)=",out
	
	!! (d_zz|M|d_xz)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_xz)=",out
	
	!! (d_xx|M|d_zz)
	call PrimitiveGaussian_set(gA,[2_8,0_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xx|M|d_zz)=",out
	
	!! (d_xy|M|d_zz)
	call PrimitiveGaussian_set(gA,[1_8,1_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xy|M|d_zz)=",out
	
	!! (d_yy|M|d_zz)
	call PrimitiveGaussian_set(gA,[0_8,2_8,0_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yy|M|d_zz)=",out
	
	!! (d_yz|M|d_zz)
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_yz|M|d_zz)=",out
	
	!! (d_xz|M|d_zz)
	call PrimitiveGaussian_set(gA,[1_8,0_8,1_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_xz|M|d_zz)=",out
	
	!! (d_zz|M|d_zz)
	call PrimitiveGaussian_set(gA,[0_8,0_8,2_8])
	call PrimitiveGaussian_set(gB,[0_8,0_8,2_8])
	out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment )
	print *,"(d_zz|M|d_zz)=",out
	
	call cpu_time(timeEnd)
	print *,"Time=",timeEnd-timeBegin , &
	"sec - (Time by 100 integrals for moment integrals over s,p and d CGF)"
	!***************************************************
	
	!!***************************************************
	!! Calcula el tiempo requerido para 5000 integrales
	!!
	
	call PrimitiveGaussian_set(gA,[0_8,1_8,1_8])
	call PrimitiveGaussian_set(gB,[1_8,0_8,1_8])
	
	call cpu_time(timeBegin)
	do i=1,5000
		out=PrimitiveGaussian_momentIntegral( gA , gB , R_C , compMoment ) 
	end do
	call cpu_time(timeEnd)
	print *,"Time=",timeEnd-timeBegin , &
	"sec - (Time by 5000 integrals for moment integrals over (dyz|M|dxz) CGF)"
	!!***************************************************
	
end program PrimitiveGaussian_momentTest	
