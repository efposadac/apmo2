!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************

!**
! Este programa prueba la construccion y manipulacion de vectores
!
! @author Nestor Aguirre
!
! <b> Fecha de creacion : </b> 2008-08-19
!   - <tt> 2007-08-19 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Creacion del archivo y las funciones basicas
!   - <tt> 2007-08-26 </tt>: Nestor Aguirre ( nfaguirrec@unal.edu.co )
!        -# Prueba de las nuevas funciones (Vector_plus,Vector_dot,Vector_norm,Vector_getMax)
!**
program VectorTest
	use Vector_
	use APMO_
	
	implicit none
	
	type(Vector) :: A
	type(Vector) :: B
	type(Vector) :: C
	integer :: i
	integer :: j
	integer :: integerTmp
	real(8) :: doubleTmp
	character(5), allocatable :: myKeys(:)
	
	call APMO_constructor() ;
	
	!!*********************************************
	!! Probando el constructor
	!!**
	call Vector_constructor( A, 8, 2.45_8 )
	
	print *, ""
	print *, " Vector A (HORIZONTAL)"
	print *, "----------------------"
	print *, ""
	call Vector_show( A, flags=HORIZONTAL )
	
	print *, ""
	print *, " Vector A (VERTICAL)"
	print *, "---------------------"
	print *, ""
	call Vector_show( A, flags=VERTICAL )
	
	call Vector_destructor( A )
	!!*********************************************
	
	!!*********************************************
	!! Probando calcular valores propios y vectores propios
	!!**
	call Vector_constructor( B, 3, 0.0_8 )
	
	call Vector_setElement( B, 1, 1.4500_8 )
	call Vector_setElement( B, 2, 3.1580_8 )
	call Vector_setElement( B, 3, 5.1640_8 )
	
	print *, ""
	print *, " Vector B"
	print *, "----------"
	allocate( myKeys(3) )
	myKeys = (/"    x","    y","    z"/)
	
	call Vector_show( B, keys=myKeys, flags=WITH_KEYS )
	deallocate( myKeys )
	print *, ""
	
	call Vector_destructor( B )
	!!*********************************************
	
	!!*********************************************
	!! Probando operaciones
	!!**
	print *, "---------------------------"
	print *, " Testing vector operations"
	print *, "---------------------------"
	
	call Vector_constructor( A, 3, 2.45_8 )
	call Vector_constructor( B, 3, values=([ 1.45_8, 3.15_8, -4.45_8 ]) )
	call Vector_constructor( C, 3 )
	
	print *, ""
	print *, " A = "
	call Vector_show( A )
	
	print *, ""
	print *, " B = "
	call Vector_show( B )
	
	print *, ""
	print *, " A+B"
	C = Vector_plus( A, B )
	call Vector_show( C )
	
	print *, ""
	print *, " A\dotB = ", Vector_dot( A, B )
	print *, ""
	print *, " norm(A) = ", Vector_norm( A )
	
	doubleTmp = Vector_getMax( B, integerTmp )
	print *, ""
	print *, " max(B) = ", doubleTmp, " at position ", integerTmp
	
	doubleTmp = Vector_getMin( B, integerTmp )
	print *, ""
	print *, " min(B) = ", doubleTmp, " at position ", integerTmp
	
	allocate( myKeys(3) )
	myKeys = (/"    x","    y","    z"/)
	
	call Vector_swapElements( B, 1, 3 )
	print *, ""
	print *, " swap(B, 1<->3) = "
	call Vector_show( B, flags=VERTICAL+WITH_KEYS )
	deallocate( myKeys )

	print *, ""
	print *, " Remove the first element of B "
	call Vector_removeElement( B, 1 )
	call Vector_show(B)

	print *, ""
	print *, " Remove the last element of A"
	call Vector_removeElement( A, size(A.values) )
	call Vector_show(A)

	
	call Vector_destructor( A )
	call Vector_destructor( B )
	call Vector_destructor( C )
	!!*********************************************
	
	call APMO_destructor() ;
	
end program VectorTest
