!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
program CartesianCoordinateTest
     use CartesianCoordinate_
     use Matrix_
     implicit none

     type(CartesianCoordinate) :: cc
     type(Matrix) :: coordinates

     call Matrix_constructor( coordinates,3_8,3_8)
     coordinates.values(1,:)=[0.0,0.0,-0.7]
     coordinates.values(2,:)=[0.0,0.0,0.7]
     coordinates.values(3,:)=[0.0,0.0,0.0]

     call CartesianCoordinate_constructor(cc,coordinates,["O","H_1","H_2"],[8.0_8,1.0_8,1.0_8],[560000.0_8,1800.0_8,3600.0_8])

     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE+CARTESIAN_COORDINATE_WITH_MASS)

     print *,""
     call CartesianCoordinate_scaleCharge(cc, -1.0_8)
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)

     print *,""
     call CartesianCoordinate_removeStringToSymbols(cc, "_*")
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)

     print *,""
     call CartesianCoordinate_addSuffixToSymbols(cc, ") 6-311PPdp ")
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)

     print *,""
     call CartesianCoordinate_addPrefixToSymbols(cc, "e-(")
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)


     print *,""
     call CartesianCoordinate_restoreSymbols(cc)
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)

     print *,""
     call CartesianCoordinate_removeStringToSymbols(cc, "_2")
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)

     print *,""
     call CartesianCoordinate_changeSymbolOfAtom(cc, "H  dirac",1)
     call CartesianCoordinate_write(cc,6,CARTESIAN_COORDINATE_WITH_SYMBOL+CARTESIAN_COORDINATE_WITH_CHARGE)


end program CartesianCoordinateTest