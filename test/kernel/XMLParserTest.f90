!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�ez M�ico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, attraction integrals, recursive integrals,    !
!!              gaussian functions, analytic derivatives                           !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa prueba enl parser XML
!
program XMLParserTest
	use APMO_
	use BasisSetManager_
	use AtomicElementManager_
	use ElementalParticleManager_
	use ConstantsOfCoupling_
	
	
	implicit none
		
	type(BasisSet) :: basis
	type(AtomicElement) :: element
	type(ElementalParticle) :: pparticle
	type(ConstantsOfCoupling) :: couplingConstants
	
	call APMO_constructor()
	
	call BasisSetManager_loadBasisSet ( basis, "6-31", "Hydrogen" )
	call BasisSet_show(basis)
		
	call AtomicElementManager_loadElement ( element, "Br" )
	call AtomicElement_show( element )
	
	call ElementalParticleManager_loadParticle ( pparticle, "proton" )
	call ElementalParticle_show( pparticle )
	
	!!****************************************************************
	!! Obtencion de constantes de acoplamiento
	!!
	call ConstantsOfCoupling_constructor( couplingConstants )
	call XMLParser_constructor( trim( trim( APMO_instance.DATA_DIRECTORY ) // "/dataBases/constantsOfCoupling.xml"), &
			"H_1", couplingConstants=couplingConstants )
			
	call ConstantsOfCoupling_show( couplingConstants )
	!!****************************************************************
	      
	call APMO_destructor()

end program XMLParserTest
