!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief programa para listas de numeros reales 
!
! @author Sergio A. Gonzalez
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Sergio A. Gonz�lez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo de prueba
! @todo verificar la liberacion de memoria al eliminar elementos de la lista
!**
program ListTest
	use List_
	implicit none

	type(List) :: list_instance
	real(8), pointer :: valuePrt
	integer  i

	!! Construye una lista de numero reales
	call List_constructor(list_instance,"listTest",20)


	!! Verifica el estado de la lista
	print *,"La lista est� libre: ", List_empty(list_instance)

	!! Adiciona elementos a la lista
	print *,""
	print *,"Adicionando elementos a la lista"
	do i=1,20

		call List_push_back( list_instance, real(i * 3.5_8,8) )
		print *, "Size: ", List_size(list_instance),"Data: ",List_current(list_instance)
	end do

	!! Elimina elementos de la lista
	print *,""
	print *,"Eliminado elemento de la lista"
	do i=1,20
		print *, "Size: ", List_size(list_instance), "Data: ",List_current(list_instance)
		call List_pop_back(list_instance)
	end do

	!! Verifica el estado de la lista
	print *,"La lista est� libre: ", List_empty(list_instance)

	!! Adiciona elementos a la lista
	print *,""
	print *,"Adicionando nuevos elementos a la lista"
	do i=1,40

		call List_push_back( list_instance, real(i * 1.5_8,8) )
		print *, "Size: ", List_size(list_instance),"Data: ",List_current(list_instance)
	end do

	!! Verifica el estado de la lista
	print *,"La lista est� libre: ", List_empty(list_instance)


	print *,""
	print *,"Cambiando el iterador de la lista:"
	
	print *,""
	print *,"Diez posiciones antes"
	call List_iterate(list_instance,-10)
	print *,  "Data: ",List_current(list_instance)
	
	print *,""
	print *,"Cinco posiciones posiciones despues"
	call List_iterate(list_instance,+5)
	print *,  "Data: ",List_current(list_instance)
	
	
	
	print *,""
	print *,"Llevando al final de la lista"
	call List_end(list_instance)
	print *,  "Data: ",List_current(list_instance)

	print *,""
	print *,"Llevando al inicio de la lista"
	call List_begin(list_instance)
	print *,  "Data: ",List_current(list_instance)


	print *,""
	print *,"Retornando referencias a valor inicial y final de la lista:"
	
	print *,""
	print *,"referencia al primer elemento de la lista"
	print *,  "Data: ",List_front(list_instance)
	
	print *,""
	print *,"referencia al ultimo elemento de la lista"
	print *,  "Data: ",List_back(list_instance)

	!!Elimina la lista previamanete creada
	
	print *,""
	print *,"Llama el destructor de la lista:"
	call List_destructor(list_instance)


end program ListTest