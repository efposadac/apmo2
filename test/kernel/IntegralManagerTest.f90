!!**********************************************************************************
!!                                                                                 !
!!    Copyright (C) 2008-2009 by                                                   !
!!                                                                                 !
!!                       Universidad Nacional de Colombia                          !
!!                          Grupo de Quimica Teorica                               !
!!                        http://www.gqt-un.unal.edu.co                            !
!!                                                                                 !
!!                             Original Authors:                                   !
!!                Sergio A. Gonzalez M. (sagonzalesm@unal.edu.co)                  !
!!                 Nestor F. Aguirre C. (nfaguirrec@unal.edu.co)                   !
!!                                                                                 !
!!                              Contributors:                                      !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
!**
! @brief Programa para prueba del administrador de integrales
!
! @author Sergio Gonzalez
!
! <b> Fecha de creacion : </b> 2008-09-19
!   - <tt> 2007-08-19 </tt>: Sergio Gonzalez ( sagonzalezm@unal.edu.co )
!        -# Creacion del archivo 
!**
program IntegralManagerTest
	use IntegralManager_

	implicit none
	
	real(8) :: integralValue
        integer :: specieID
        integer :: numberOfContractions
	
! 	call APMO_constructor()
! 	call ParticleManager_constructor(4) 
! 	
! 	
! 	!! Ejemplo: 
! 	!!		Obtencion de integrales entre contracciones atómicas para molecula de hidrógeno
! 	!!		con nucleos como cargas puntuales
! 	
! 	print *,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
! 	print *,"EJEMPLO DE CALCULO DE INTEGRALES PARA MOLECULA "
! 	print *,"EMPLEANDO BASE MINIMA"
! 	print *,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
! 	
!  	call ParticleManager_addParticle( "H", baseName="sto-3g", origin=[0.0_8, 0.0_8, 0.0_8])	!< Cargando base electronica
!  	call ParticleManager_addParticle( "H", baseName="sto-3g", origin=[0.0_8, 0.0_8, 1.4_8])	!< Cargando base electronica
!   	call ParticleManager_addParticle( "H", origin=[0.0_8, 0.0_8, 0.0_8])		!< Definiendo localizacion de nucleo puntual
!   	call ParticleManager_addParticle( "H", origin=[0.0_8, 0.0_8, 1.4_8])		!< Definiendo localizacion de nucleo puntual
! 	
! 
!  	call IntegralManager_constructor()
!  	call ParticleManager_showParticlesInformation()
! ! 	call ContractedGaussian_show( ParticleManager_getContractionPtr( "e-", 1) )
! ! 	call ContractedGaussian_show( ParticleManager_getContractionPtr( "e-", 2) )
!         specieID=ParticleManager_getSpecieID( nameOfSpecie="e-" )
! 	numberOfContractions = ParticleManager_getNumberOfContractions( iteratorOfSpecie = specieID )
! 		
! 	!! Calculo de integrales de overlap
!  	integralValue = IntegralManager_getMatrixElement( OVERLAP_INTEGRALS, 1, 1, "e-"  )
! 	print *,"(1|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( OVERLAP_INTEGRALS, 1, 2, "e-"  )
! 	print *,"(1|2) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( OVERLAP_INTEGRALS, 2, 1, "e-"  )
! 	print *,"(2|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( OVERLAP_INTEGRALS, 2, 2, "e-"  )
! 	print *,"(2|2) =", integralValue
! 	
! 	!! Calculo de integrales de energia cinetica
!  	integralValue = IntegralManager_getMatrixElement( KINETIC_INTEGRALS, 1, 1, "e-"  )
! 	print *,"(1|T|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( KINETIC_INTEGRALS, 1, 2, "e-"  )
! 	print *,"(1|T|2) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( KINETIC_INTEGRALS, 2, 1, "e-"  )
! 	print *,"(2|T|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( KINETIC_INTEGRALS, 2, 2, "e-"  )
! 	print *,"(2|T|2) =", integralValue
! 	
! 	!! Calculo de integrales de energia atraccion con cargas puntuales
!  	integralValue = IntegralManager_getMatrixElement( ATTRACTION_INTEGRALS, 1, 1, "e-"  )
! 	print *,"(1|A|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( ATTRACTION_INTEGRALS, 1, 2, "e-"  )
! 	print *,"(1|A|2) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( ATTRACTION_INTEGRALS, 2, 1, "e-"  )
! 	print *,"(2|A|1) =", integralValue
! 	integralValue = IntegralManager_getMatrixElement( ATTRACTION_INTEGRALS, 2, 2, "e-"  )
! 	print *,"(2|A|2) =", integralValue
! 	
! 	!! Calculo de integrales de respulsion iter-electronica
!  	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 1, 1, 1, 1, specieID, numberOfContractions )
! 	print *,"(11|11) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 1, 1, 1, 2, specieID, numberOfContractions )
! 	print *,"(11|12) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 1, 1, 2, 2, specieID, numberOfContractions )
! 	print *,"(11|22) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 1, 2, 1, 2, specieID, numberOfContractions )
! 	print *,"(12|12) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 2, 2, 1, 1, specieID, numberOfContractions )
! 	print *,"(22|11) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 2, 2, 2, 2, specieID, numberOfContractions )
! 	print *,"(22|22) =", integralValue
! 	integralValue = IntegralManager_getIntraspecieRepulsionIntegral( 2, 2, 2, 1, specieID, numberOfContractions )
! 	print *,"(22|21) =", integralValue

end program IntegralManagerTest
