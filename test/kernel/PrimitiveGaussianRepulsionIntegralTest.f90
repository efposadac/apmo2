!***********************************************************************************
!!    Copyright (C) 2007 by Universidad Nacional de Colombia                       !
!!    http://www.unal.edu.co                                                       !
!!                                                                                 !
!!    Author: <Sergio A. Gonz�lez M�nico>  <sagonzalezm@unal.edu.co>               !
!!    Keywords: molecular integrals, atracci�n integrals, recursive integrals,     !
!!              gaussian functions.                                                !
!!                                                                                 !
!!    This files is part of nonBOA                                                 !
!!                                                                                 !
!!    This program is free software; you can redistribute it and/or modify         !
!!    it under the terms of the GNU General Public License as published by         !
!!    the Free Software Foundation; either version 2 of the License, or            !
!!    (at your option) any later version.                                          !
!!                                                                                 !
!!    This program is distributed in the hope that it will be useful,              !
!!    but WITHOUT ANY WARRANTY; without even the implied warranty of               !
!!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
!!    GNU General Public License for more details.                                 !
!!                                                                                 !
!!    You should have received a copy of the GNU General Public License            !
!!    along with this program. If not, write to the Free Software Foundation,      !
!!    Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              !
!!                                                                                 !
!!**********************************************************************************
	
!**
! Este programa calcula las integrales de repulsi�n de cuatro centros para 
! funciones gausianas, determinando el tiempo requerido para el calculo.
!
program PrimitiveGaussian_repulsionTest
	use APMO_
	use PrimitiveGaussian_
	use RepulsionIntegrals_
	
	implicit none
	
	type(PrimitiveGaussian)::gA,gB,gC,gD
	real(8), allocatable :: out(:)
	integer::i
	real :: timeBegin
	real :: timeEnd
	
	call APMO_constructor()
	
	!!
	!! Definci�n de funciones gausianas y origen de la carga puntual. 
	!!
	call PrimitiveGaussian_constructor(gA,[ 2.0_8,1.0_8,-5.0_8],0.1_8,0_8)
	call PrimitiveGaussian_constructor(gB,[-5.0_8,2.0_8,-1.0_8],0.2_8,0_8)
	call PrimitiveGaussian_constructor(gC,[ 1.0_8,2.5_8,-1.0_8],0.05_8,0_8)
	call PrimitiveGaussian_constructor(gD,[-1.0_8,3.5_8,-5.6_8],0.15_8,0_8)
	
	!! Imprime informaci�n sobre la gausianas construidas
	print *,"NV"
	call PrimitiveGaussian_show(gA)
	call PrimitiveGaussian_show(gB)
	call PrimitiveGaussian_show(gC)
	call PrimitiveGaussian_show(gD)
	
	!!***************************************************
	!! Inicio de calculo de integrales de atracci�n
	!!
	
	! call cpu_time(timeBegin)
	
	! !! (ss|ss)
  	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
  	! print *,"(ss|ss)=",out

	! !! (PaxSb,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxSb,ScSd)=",out
	
	! !! (ScSd,PaxSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PaxSb)=",out
	
	! !! (SdSv,PaxSb)
	! out=PrimitiveGaussian_repulsionIntegral(gD,gC,gA,gB)
	! print *,"(SdSc,PaxSb)=",out
	
	! !! (PaySb,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaySb,ScSd)=",out
	
	! !! (PazSb,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazSb,ScSd)=",out
	
	! !! (SbPax,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPax,ScSd)=",out
	
	! !! (SbPay,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPay,ScSd)=",out
	
	! !! (SbPaz,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPaz,ScSd)=",out
	
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	
	! !! (SbSa,PcxSd)
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbSa,PcxSd)=",out
	
	! !! (SbSa,PcySd)
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbSa,PcySd)=",out
	
	! !! (SbSa,PczSd)
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbSa,PczSd)=",out
	
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	
	! !! (SbSa,SdPcx)
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbSa,SdPcx)=",out
	
	! !! (SbSa,SdPcy)
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbSa,SdPcy)=",out
	
	! !! (SbSa,SdPcz)
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbSa,SdPcz)=",out
	
	! !! (PaxSb,PcxSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxSb,PcxSd)=",out
	
	! !! (PcxSd,PaxSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,PaxSb)=",out
	
	! !! (PaxSb,PcySd)
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxSb,PcySd)=",out
	
	! !! (PaxSb,PczSd)
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxSb,PczSd)=",out
	
	! !! (PaySb,PcxSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaySb,PcxSd)=",out
	
	! !! (PaySb,PcySd)
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaySb,PcySd)=",out
	
	! !! (PaySb,PczSd)
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaySb,PczSd)=",out
	
	! !! (PazSb,PcxSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazSb,PcxSd)=",out
	
	! !! (PazSb,PcySd)
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazSb,PcySd)=",out
	
	! !! (PazSb,PczSd)
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazSb,PczSd)=",out
	
	! !! (SbPaz,PcxSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPaz,PcxSd)=",out
	
	! !! (SbPay,PcySd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPay,PcySd)=",out
	
	! !! (SbPax,PczSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbPax,PczSd)=",out
	
	! !! (SbPaz,SdPcx)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbPaz,SdPcx)=",out
	
	! !! (SbPay,SdPcy)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbPay,SdPcy)=",out
	
	! !! (SbPax,SdPcz)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gD,gC)
	! print *,"(SbPax,SdPcz)=",out
	
	! !! (PaxPbx,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPbx,ScSd)=",out
	
	! !! (ScSd,PaxPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PaxPbx)=",out
	
	! !! (PaxPby,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPby,ScSd)=",out
	
	! !! (ScSd,PaxPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PaxPby)=",out
	
	! !! (PaxPbz,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPbz,ScSd)=",out
	
	! !! (ScSd,PaxPbz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PaxPbz)=",out
	
	! !! (PayPbx,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PayPbx,ScSd)=",out
	
	! !! (ScSd,PayPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PayPbx)=",out
	
	! !! (PayPby,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PayPby,ScSd)=",out
	
	! !! (ScSd,PaxPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PayPby)=",out
	
	! !! (PayPbz,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PayPbz,ScSd)=",out
	
	! !! (ScSd,PayPbz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PayPbz)=",out
	
	! !! (PazPbx,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazPbx,ScSd)=",out
	
	! !! (ScSd,PazPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PazPbx)=",out
	
	! !! (PazPby,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazPby,ScSd)=",out
	
	! !! (ScSd,PazPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PazPby)=",out
	
	! !! (PazPbz,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazPbz,ScSd)=",out
	
	! !! (ScSd,PazPbz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,PazPbz)=",out
	
	! !! (DaxxSb,ScSd)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxxSb,ScSd)=",out
	
	! !! (DaxySb,ScSd)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxySb,ScSd)=",out
	
	! !! (DaxzSb,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxzSb,ScSd)=",out
	
	! !! (DayySb,ScSd)
	! gA.angularMomentIndex=[0_8,2_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DayySb,ScSd)=",out
	
	! !! (DayzSb,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DayzSb,ScSd)=",out
	
	! !! (DazzSb,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DazzSb,ScSd)=",out
	
	
	! !! (SbDaxx,ScSd)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDaxx,ScSd)=",out
	
	! !! (SbDaxy,ScSd)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDaxy,ScSd)=",out
	
	! !! (SbDaxz,ScSd)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDaxz,ScSd)=",out
	
	! !! (SbDayy,ScSd)
	! gA.angularMomentIndex=[0_8,2_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDayy,ScSd)=",out
	
	! !! (SbDayzSb,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDayz,ScSd)=",out
	
	! !! (SbDazzSb,ScSd)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDazz,ScSd)=",out
	
	
	! !! (ScSd,DaxxSb)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DaxxSb)=",out
	
	! !! (ScSd,DaxySb)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DaxySb)=",out
	
	! !! (ScSd,DaxzSb)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DaxzSb)=",out
	
	! !! (ScSd,DayySb)
	! gA.angularMomentIndex=[0_8,2_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DayySb)=",out
	
	! !! (ScSd,DayzSb)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DayzSb)=",out
	
	! !! (ScSd,DazzSb)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DazzSb)=",out
	
	! !! (ScSd,SbDaxx)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDaxx)=",out
	
	! !! (ScSd,SbDaxy)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDaxy)=",out
	
	! !! (ScSd,SbDaxz)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDaxz)=",out
	
	! !! (ScSd,SbDayy)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDayy)=",out
	
	! !! (ScSd,SbDayz)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDayz)=",out
	
	! !! (ScSd,SbDazz)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,SbDazz)=",out
	
	! !! (PaxPbx,PcxSd)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPbx,PcxSd)=",out
	
	! !! (PcxSd,PaxPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,PaxPbx)=",out
	
	! !! (PaxPby,PcxSd)
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPby,PcxSd)=",out
	
	! !! (PcxSd,PaxPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,PaxPby)=",out
	
	! !! (PayPbz,PcxSd)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PayPbz,PcxSd)=",out
	
	! !! (PcxSd,PayPbz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,PayPbz)=",out
	
	! !! (PazPby,PcxSd)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PazPby,PcxSd)=",out
	
	! !! (PcxSd,PazPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,PazPby)=",out
	
	! !! (DaxyPbz,ScSd)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxyPbz,ScSd)=",out
	
	! !! (ScSd,DaxyPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DaxyPbz)=",out
	
	! !! (DaxxPbx,ScSd)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxxPbx,ScSd)=",out
	
	! !! (ScSd,DaxxPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DaxxPbx)=",out
	
	! !! (DayzPby,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DayzPby,ScSd)=",out
	
	! !! (ScSd,DayzPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(ScSd,DayzPby)=",out
	
	! !! (PbyDayz,ScSd)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(PbyDayz,ScSd)=",out
	
	! !! (ScSd,PbyDayz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(ScSd,PbyDayz)=",out
	
	! !! (DaxxSb,PcxSd)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxxSb,PcxSd)=",out
	
	! !! (PcxSd,DaxxSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DaxxSb)=",out
	
	! !! (DaxySb,PcxSd)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxySb,PcxSd)=",out
	
	! !! (PcxSd,DaxySb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DaxySb)=",out
	
	! !! (DaxzSb,PcxSd)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxzSb,PcxSd)=",out
	
	! !! (PcxSd,DaxzSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DaxzSb)=",out
	
	! !! (DayySb,PcxSd)
	! gA.angularMomentIndex=[0_8,2_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DayySb,PcxSd)=",out
	
	! !! (PcxSd,DayySb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DayySb)=",out
	
	! !! (DayzSb,PcxSd)
	! gA.angularMomentIndex=[0_8,1_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DayzSb,PcxSd)=",out
	
	! !! (PcxSd,DayzSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DayzSb)=",out
	
	! !! (DazzSb,PcxSd)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DazzSb,PcxSd)=",out
	
	! !! (PcxSd,DazzSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxSd,DazzSb)=",out
	
	! !! (DaxzSb,PcySd)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DaxzSb,PcySd)=",out
	
	! !! (PcySd,DaxzSb)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcySd,DaxzSb)=",out
	
	! !! (SbDaxz,PcySd)
	! gA.angularMomentIndex=[1_8,0_8,1_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(SbDaxz,PcySd)=",out
	
	! !! (PcySd,SbDaxz)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(PcySd,SbDaxz)=",out
	
	! !! (PaxPbx,PcxPdx)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPbx,PcxPdx)=",out
	
	! !! (PcxPdx,PaxPbx)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxPdx,PaxPbx)=",out
	
	! !! (PaxPby,PcxPdx)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPby,PcxPdx)=",out
	
	! !! (PcxPdx,PaxPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PcxPdx,PaxPby)=",out
	
	! !! (PaxPby,PczPdx)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PaxPby,PczPdx)=",out
	
	! !! (PczPdx,PaxPby)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gA,gB)
	! print *,"(PczPdx,PaxPby)=",out
	
	! !! (PbyPax,PczPdx)
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(PbyPax,PczPdx)=",out
	
	! !! (PczPdx,PbyPax)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(PczPdx,PbyPax)=",out
	
	! !! (PbyPax,PczPdx)
	! out=PrimitiveGaussian_repulsionIntegral(gB,gA,gC,gD)
	! print *,"(PbyPax,PdxPcz)=",out
	
	! !! (PczPdx,PbyPax)
	! out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! print *,"(PdxPcz,PbyPax)=",out
	
	! !! (Dxy S,S S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyS,SS)=",out
	
	! !! (S S,Dzz S )
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SS,DzzS)=",out
	
	! !! (Dxy S,Py S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyS,PyS)=",out
	
	! !! (S Pz,Dzz S )
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SPz,DzzS)=",out
	
	! !! (Dxy S,Px Py)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(Dxy S,Px Py)=",out
	
	! !! (Dxx S,Px Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(Dxx S,Px Px)=",out
	
	! !! (Dxy S,Px Py)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(Dxy S,Px Py)=",out
	
		
	! !! (Dxy S,S Dyz)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyS,S Dyz)=",out
	
	! !! (Dxx S,S Dxx)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxS,S Dxx)=",out
	
	! !! (Dxx S,Dzz S)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxS,DzzS)=",out
	
	! !! (Dxy Py,Pz S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyPy,PzS)=",out
	
	! !! (Dxx Px,Px S)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPx,PxS)=",out
	
	! !! (S Px ,Dzz Pz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SPx,DzzPz)=",out
	
	! !! (S Pz ,Dzz Pz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,1_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SPz,DzzPz)=",out
	
	! !! (Dxy Py,Pz Px)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyPy,PzPx)=",out
	
	! !! (Dyy Py,Py Py)
	! gA.angularMomentIndex=[0_8,2_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,1_8,0_8]
	! gD.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DyyPy,PyPy)=",out

	
	! !! (Pz Px ,Dzz Pz)
	! gA.angularMomentIndex=[0_8,0_8,1_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PzPx,DzzPz)=",out
	
	! !! (Dxy Py,Dxz S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,1_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyPy,DxzS)=",out
	
	! !! (Dxx Px,Dxx S)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPx,DxxS)=",out
	
		
	! !! (S Dxx ,Dzz Pz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"SDxx,DzzPz)=",out
	
	! !! (Dxy Py,Dxz Px)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,1_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyPy,DxzPx)=",out
	
	! !! (Dxx Py,Dzz Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPy,DzzPx)=",out
	
	! !! (Dzz Px,Dxx Py)
	! gA.angularMomentIndex=[0_8,0_8,2_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DzzPx,DxxPy)=",out
	
	! !! (Dxx Px,Dxx Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPx,DxxPx)=",out
	
	! !! (Dxx Px,Dyy Py)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,2_8,0_8]
	! gD.angularMomentIndex=[0_8,1_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPx,DyyPy)=",out
	
	! !! (Dxx Py,Dxx Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxPy,DxxPx)=",out
	
	
	! !! (Py Dxx ,Dzz Pz)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PyDxx,DzzPz)=",out
	
	! !! (Dxy Dxx,Pz Px)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,PzPx)=",out
	
	! !! (Dxx Dxx,Px Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxDxx,PxPx)=",out
	
	! !! (Py Px ,Dzz Dzz)
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PyPx,DzzDzz)=",out
	
	! !! (Dxy Dxx, S S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,SS)=",out
	
	! !! (S S ,Dzz Dzz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SS,DzzDzz)=",out
	
	! !! (Dxx Dxx, Px S)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[1_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxDxx,PxS)=",out
	
	! !! (Dxy Dxx, Pz S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,1_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,PzS)=",out
	
	! !! (S Px ,Dzz Dzz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SPx,DzzDzz)=",out
	
	! !! (Dxx Dxx, Dxx S)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxDxx,DxxS)=",out
	
	! !! (Dxy Dxx, Dzz S)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,DzzS)=",out
	
	! !! (S Dxy ,Dzz Dzz)
	! gA.angularMomentIndex=[0_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(SDxx,DzzDzz)=",out
	
	! !! (Dxy Dxx, Dzz Px)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,DzzPx)=",out
	
	! !! (Dxx Dxx, Dzz Px)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[1_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxDxx,DzzPx)=",out
	
	! !! (Px, Dzz Dxx, Dxx )
	! gA.angularMomentIndex=[1_8,0_8,0_8]
	! gB.angularMomentIndex=[0_8,0_8,2_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[2_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(Px,Dzz,Dxx,Dxx)=",out
	
	! !! (Py Dxy ,Dzz Dzz)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[1_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PyDxy,DzzDzz)=",out
	
	! !! (Py Dyy ,Dyy Dyy)
	! gA.angularMomentIndex=[0_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,2_8,0_8]
	! gC.angularMomentIndex=[0_8,2_8,0_8]
	! gD.angularMomentIndex=[0_8,2_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(PyDyy,DyyDyy)=",out
	
	! !! (Dxy Dxx, Dzz Dyz)
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,1_8,1_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxyDxx,DzzDyz)=",out
	
	! !! (Dxx Dxy ,Dzz Dzz)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[1_8,1_8,0_8]
	! gC.angularMomentIndex=[0_8,0_8,2_8]
	! gD.angularMomentIndex=[0_8,0_8,2_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DxxDxy,DzzDzz)=",out

	! !! (Dzz Dzz ,Dzz Dzz)
	! gA.angularMomentIndex=[2_8,0_8,0_8]
	! gB.angularMomentIndex=[2_8,0_8,0_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	! gD.angularMomentIndex=[2_8,0_8,0_8]
	! out=PrimitiveGaussian_repulsionIntegral(gA,gB,gC,gD)
	! print *,"(DzzDzz,DzzDzz)=",out



	! call cpu_time(timeEnd)
	! print *,"Time=",timeEnd-timeBegin , &
	! "sec - (Time by 143 repulsion integrals over s,p and d CGF)"
	! !!***************************************************
	
	! !!***************************************************
	! !! Calcula el tiempo requerido para 5000 integrales
	! !!
	! call cpu_time(timeBegin)
	
	! gA.angularMomentIndex=[1_8,1_8,0_8]
	! gB.angularMomentIndex=[0_8,1_8,1_8]
	! gC.angularMomentIndex=[1_8,0_8,1_8]
	! gC.angularMomentIndex=[2_8,0_8,0_8]
	
	! do i = 1,5000
	! 	out=PrimitiveGaussian_repulsionIntegral(gC,gD,gB,gA)
	! end do
	
	! call cpu_time(timeEnd)
	! print *,"Time=",timeEnd-timeBegin, &
	! "sec - (Time by 5000 repulsion integrals integrals (dd,dd) CGF)"
	!!***************************************************
	
	
end program PrimitiveGaussian_repulsionTest
