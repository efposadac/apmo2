* SYM4TR : title
  'Prueba de transformacion para molecula de hidrogeno'
* NAMEPT / name of Abel point group in upper-case letters
  'C1'
* number of SOs for each symmetry (a1,a2,b1,b2)
  25, 0, 0, 0
* number of active MOs for each symmetry (a1,a2,b1,b2)
  25, 0, 0, 0
* print flag (0=normal, 1=dump)
  1
* run flag (0=normal, 1=keep work files)
  0
* THRSOI (threshold for SO basis integrals)
  1.0D-10
* THRHLF (threshold for half-transformed integrals)
  1.0D-10
* THRMOI (threshold for MO basis integrals)
  1.0D-10
* END
