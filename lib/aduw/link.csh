#!/bin/csh -f
#
# static link original -> alias 
#
set echo
#
rm -f declar.h
rm -f prepar.h
rm -f wmupmo.f
ln -s PREPAR/declar.h declar.h
ln -s PREPAR/prepar.h prepar.h
ln -s PREPAR/wmupmo.f wmupmo.f

rm -f inf.dat
rm -f mocoef.dat
rm -f soint.dat
ln -s PREPAR/inf.dat inf.dat
ln -s PREPAR/mocoef.dat mocoef.dat
ln -s PREPAR/soint.dat soint.dat
