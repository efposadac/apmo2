      SUBROUTINE WMFINT(
     *                  DDOB73, 
     *                  IBUFF,  DBUFF
     *                 )
!----------------------------------------------------------------------
!  DESCRIPTION : This routine
!                reads SO basis integrals from text files and write 
!                them to a binary file.
!  RELEASE :     v.01  gen = sya 2003-03-29 at chukyo-u
!  CALLED BY :   WMFILE
!----------------------------------------------------------------------
!  record structure of binary SO integral file
!     KW, NW, DBUFF(1:LDOB73), IBUFF(1:4,1:LDOB73)
! 
!  meaning of variable
!     NW : number of SO integrals in a record
!     KW : = NW, 
!          = 0 or -NW for the last record
!     IBUFF(1,i) : p
!     IBUFF(2,i) : q
!     IBUFF(2,i) : r
!     IBUFF(2,i) : s
!     DBUFF(i) : integral value
!----------------------------------------------------------------------
!  N.B.
!     DDOB73 == 791
!----------------------------------------------------------------------
      IMPLICIT NONE
      INCLUDE 'declar.h'
      INCLUDE 'prepar.h'
!
!             ...declare variable(s) in argument list
!
      INTEGER,INTENT(IN) :: DDOB73
      REAL(KIND=LDREAL),DIMENSION(DDOB73),INTENT(INOUT) :: DBUFF
      INTEGER,DIMENSION(4,DDOB73),INTENT(INOUT) :: IBUFF
!
!             ...define variable(s) in code
!
      REAL(KIND=LDREAL) :: VAL
      INTEGER :: P,Q,R,S
      INTEGER :: NW
!----------------------------------------------------------------------
!
!      WRITE(*,*) 'WMFINT start'
!
!     ...read SO basis integrals
!

      OPEN(UNIT=NFT63,FILE=FN63,STATUS='OLD',ACCESS='SEQUENTIAL',
     *     FORM='FORMATTED')
      OPEN(UNIT=NFT73,FILE=FN73,STATUS='UNKNOWN',ACCESS='SEQUENTIAL',
     *     FORM='UNFORMATTED')
!
      NW = 0
      DO
        READ(UNIT=NFT63,FMT=*,END=109) P,Q,R,S,VAL
        NW = NW + 1
        IBUFF(1,NW) = P
        IBUFF(2,NW) = Q
        IBUFF(3,NW) = R
        IBUFF(4,NW) = S
        DBUFF(NW) = VAL

        IF( NW == DDOB73 ) THEN
          WRITE(NFT73) NW,NW,DBUFF,IBUFF
          NW = 0
        END IF
      END DO
  109 CONTINUE
      WRITE(NFT73) -NW,NW,DBUFF,IBUFF
!
      CLOSE(UNIT=NFT63)
      CLOSE(UNIT=NFT73)
!
!      WRITE(*,*) 'WMFINT ended'
!
      RETURN
      END SUBROUTINE
